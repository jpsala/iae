IfNotExist, autoPrint.ini
	goto, noExisteini

IniRead,reader,autoPrint.ini,general,reader
IniRead,nombre_pc,autoPrint.ini,general,nombre_pc
IniRead, viewer_exe, autoPrint.ini,general, viewer_exe
IfNotExist, %viewer_exe%
	goto, noExisteViewer
IniRead, path_pdfs, autoPrint.ini,general, path_pdfs
IfNotExist, %path_pdfs%
	goto, noExistePathPdf
path_pdfs=%path_pdfs%%nombre_pc%\
IfNotExist, %path_pdfs%
	goto, noExistePathPdf
#Persistent
SetTimer, PrintMon, 500
PrintMon:
	Loop, %path_pdfs%*.pdf {
		f = %A_LoopFileName%
		StringSplit, partes, f, @
		imprimeAuto = %partes1%
		impresora = %partes2%
		;MsgBox, %A_LoopFileLongPath%
		if( imprimeAuto == "P" ){
			MsgBox, 4, Impresión, Quiere imprimir el comprobante?
			IfMsgBox Yes
				gosub, print
		}else{
			gosub, print
		}
		FileDelete, %A_LoopFileLongPath%
	}
return

print:
;MsgBox, "%path_pdfs%%A_LoopFileName%"
if( reader == "sumatra"){
	runwait, "%viewer_exe%" -print-to "%impresora%" "%path_pdfs%%A_LoopFileName%"
}else{
	runwait, "%viewer_exe%" /t "%path_pdfs%%A_LoopFileName%" "%impresora%"
	;MsgBox, "%viewer_exe%" /t "%path_pdfs%%A_LoopFileName%" "%impresora%"
}
return

noExisteViewer:
if( reader == "sumatra"){
	MsgBox, el path al sumatra reader es incorrecto:`r%viewer_exe%
}else{
	MsgBox, el path al foxit reader es incorrecto:`r%viewer_exe%
}
	ExitApp
return

noExistePathPdf:
	MsgBox, No existe el path a los pdf`r%path_pdfs%
	ExitApp
	
noExisteIni:
	MsgBox, No existe el archivo autoPrint.ini
	ExitApp
