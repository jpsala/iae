cd /prg/iae
echo estoy en $(pwd)
echo rm
rm iae-nuevo.dump > /dev/null 2>&1
echo "SET GLOBAL log_bin_trust_function_creators = 1;" > iae-nuevo.dump
echo dump
mysqldump -u root --complete-insert --single-transaction --routines --triggers iae-nuevo >> iae-nuevo.dump
echo bzip2
rm -f iae-nuevo.dump.bz2
bzip2 iae-nuevo.dump
echo listo
