requirejs.config({
	"baseUrl": "js/lib",
	"paths": {
		app: "../app",
		ko: 'knockout-3.1.0',
		bootstrap: 'bootstrap.min',
		jquery:"jquery-2.1.0.min"
	},
	"shim": {
		"knockout-projections.min" : ["ko"]
	}
});

//requirejs(["app/main"]);
