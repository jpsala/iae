define(['ko', "app/models/person"], function(ko, person) {
	return function docViewModel() {
		this.firstName = ko.observable('Bert');
		this.firstNameCaps = ko.computed(function() {
			return this.firstName().toUpperCase();
		}, this);
	};
});