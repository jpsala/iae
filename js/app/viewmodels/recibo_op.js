vm = (function () {
  var
   vm = {
     tipo: ko.observable(null),
     muestraRetencion: ko.observable(true),
     retencion: ko.observable(null),
     totalRetencion: ko.observable(),
     cuit: ko.observable(),
     socio_nombre: ko.observable(),
     obteniendoRetencion: ko.observable(),
     urlRetencion: '',
     muestra_socio_nombre: ko.observable(),
     init: function () {
       vm.tipo(doc.tipo);
     },
     socioChange: function (data) {
       vm.retencion(Number(data ? data.retencion : null));
     },
     valoresTotalesChange: function () {
       retencionChange();
     },
     quitaRetencion: function () {
       vm.muestraRetencion(false);
       vm.totalRetencion(0);
     }
   };
  vm.muestra_socio_nombre(false);

  vm.retencion.subscribe(function (v) {
    retencionChange();
    setTimeout(() => {valores.refresh()
  },
    100
    )
    ;
  });
  vm.cuit.subscribe(function (v) {
    cuitChange();
  });

  function retencionChange() {
    if (vm.muestraRetencion()) {
      vm.totalRetencion(calculaRetencion(vm.retencion()));
    }
  }

  function cuitChange() {
    vm.muestraRetencion(false);
    vm.obteniendoRetencion(true);
    var timeout = setTimeout(function () {
      vm.obteniendoRetencion(false);
      alert('error obteniendo la retención');
    }, 10000)
    $.ajax({
      type: "GET",
      dataType: "json",
      data: {cuit: vm.cuit()},
      url: vm.urlRetencion,
      success: function (data) {
        data.error && alert('ARBA dice: ' + data.error)
        vm.retencion(data.retencion);
        vm.obteniendoRetencion(false);
        vm.muestraRetencion(true);
        clearTimeout(timeout);
        valores && valores.refresh();
      },
      error: function (data, status) {
        alert('ARBA dice: ' + data)
        vm.obteniendoRetencion(false);
        clearTimeout(timeout);
      }
    });
  }

  function calculaRetencion(v) {
    var retencion = Math.round(doc.total * (v / 100) * 100) / 100;

    return isNaN(retencion) ? "" : retencion;
  }

  return vm;

}());
