jQuery.fn.extend({
    clearinput: function(fn) {
        $(this).wrap('<span class="divclearable"></span>')
        .parent()
        .append('<a href="javascript:" tabIndex="-1"></a>')
        .find('a')
        .click(function() {
            $(this).prev().focus().val('').change().keyup().select();
            if(fn){
                fn($(this));
            }
        });
    }
});
