	String.prototype.lPad = function(n, c) {
		var i;
		var a = this.split('');
		for (i = 0; i < n - this.length; i++) {
			a.unshift(c)
		}
		;
		return a.join('')
	};
	String.prototype.rPad = function(n, c) {
		var i;
		var a = this.split('');
		for (i = 0; i < n - this.length; i++) {
			a.push(c);
		}
		return a.join('');
	};
	function round(num, dec) {
		dec = dec ? dec : 2;
		var result = (Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec)).toFixed(dec);
		return Number(result);
	}
	function number_format(number, decimals, dec_point, thousands_sep) {
		number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
		var n = !isFinite(+number) ? 0 : +number,
				prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
				sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
				dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
				s = '',
				toFixedFix = function(n, prec) {
					var k = Math.pow(10, prec);
					return '' + Math.round(n * k) / k;
				};
		// Fix for IE parseFloat(0.55).toFixed(0) = 0;
		s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
		if (s[0].length > 3) {
			s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
		}
		if ((s[1] || '').length < prec) {
			s[1] = s[1] || '';
			s[1] += new Array(prec - s[1].length + 1).join('0');
		}
		return s.join(dec);
	}
