spinInit();

function muestraErrores(errorObj) {
    var errores = "";
    for (i in errorObj) {
        errores += (errorObj[i] + "\n");
    }
    alert(errores);

}

function round(num, dec) {
    dec = dec ? dec : 2;
    var result = (Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec)).toFixed(dec);
    return Number(result);
}

function GUID() {
    var S4 = function()
    {
        return Math.floor(
                Math.random() * 0x10000 /* 65536 */
                ).toString(16);
    };

    return (
            S4() + S4() + "-" +
            S4() + "-" +
            S4() + "-" +
            S4() + "-" +
            S4() + S4() + S4()
            );
}

function setFocus($e, t) {
    if (!t)
        t = 55;
    setTimeout(function() {
        $e.focus();
    }, t, $e);

}

function setFocusAndSelect($e, t) {
    if (!t)
        t = 100;
    setTimeout(function() {
        $e.focus().select();
    }, t, $e);

}

function pad(number, length) {

    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }

    return str;

}

function colorize($e, color, weigth) {
    color = color ? color : "red";
    weigth = weigth ? weigth : "900";
    var colorAnt = $e.css("color"), weightAnt = $e.css("font-weight");
    $e.animate({color: color, "font-weight": weigth}, function() {
        $(this).animate({color: colorAnt, "font-weight": weightAnt}, function() {
            $(this).animate({color: color, "font-weight": weigth}, function() {
                $(this).animate({color: colorAnt, "font-weight": weightAnt});
            });
        });
    });
}

function validaNumeroComprob($this) {
    var numero = $this.val();
    numAr = numero.split("-");
    if (numAr.length !== 2) {
        alert("Compruebe el número de comprobante");
        setTimeout(function() {
            $this.focus().select()
        }, 10);
        return false;
    }
    ;
    $this.val(pad(numAr[0], 4) + "-" + pad(numAr[1], 8));
}

function muestraErroresDoc(errores) {
    $dlg = $("#errores-dialog");
    if (!$dlg.length) {
        $dlg = $("body").append("<div id='errores-dialog'><ul class='errores-seccion'></ul></div>").find("#errores-dialog");
    } else {
        $dlg.find(".errores-seccion").html("");
    }
    if (!$('#errores-dialog').is('dialog')) {
        $dlg.dialog({
            position: ["center", "center"],
            autoOpen: false,
            width: "auto",
            title: "Errores en la grabación",
            buttons: {
                Ok: function() {
                    $dlg.dialog("close");
                }
            }
        });
    }
    $ul = $dlg.find(".errores-seccion");
    entro = false;
    for (var seccion in errores) {
        if (errores[seccion]) {
            $ul.append("<li class='errores-seccion-sub'>" + seccion + "</li>");
            $sub = $ul.find(".errores-seccion-sub").last();
            itemEsArray = typeof errores[seccion] === "object";
            entro = true;
            if (itemEsArray) {
                for (var key in errores[seccion]) {
                    itemEsArray = typeof errores[seccion][key] === "object";
                    if (itemEsArray) {
                        $sub.append("<li class='errores-seccion-sub'>" + key + "</li>");
                    }
                    item = errores[seccion][key];
                    if (itemEsArray) {
                        for (var propiedad in item) {
                            $ul.find(".errores-seccion-sub").last()
                                    .append("<li>" + item[propiedad] + "</li>");
                        }
                    } else {
                        $ul.find(".errores-seccion-sub").last()
                                .append("<li>" + item + "</li>");

                    }
                }
            } else {
                $sub.append("<li class='errores-seccion-sub'>" + errores[seccion] + "</li>");
            }

        }
    }
    if (entro) {
        $dlg.dialog("open");
    }
    return entro;
}

function alerta(txt, title, buttons) {
    title = title ? title : "Alerta!";
    buttons = buttons ? buttons : {
        ok: function() {
            $(this).dialog("close");
        }
    };
    var $alerta = $("#common-alerta");
    if ($alerta.length === 0) {
        $("body").append("<div style='display:none' class='ui-widget ui-widget-content' id='common-alerta'></div>");
        $alerta = $("#common-alerta");
    }
    $alerta.html(txt/*txt.substr(0, 1200)*/);
    $commonDlg = $alerta.dialog({
        position: {
            my: "center, center+200",
            at: "top"
        },
        title: title,
        width: "auto",
        heigth: "auto",
        buttons: buttons,
        close: function() {
            $alerta.dialog("close");
            $alerta.dialog("destroy");
            jQuery("[aria-describedby='common-alerta']").remove();
        }
    });
}

function number_format(number, decimals, dec_point, thousands_sep) {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function(n, prec) {
                var k = Math.pow(10, prec);
                return '' + Math.round(n * k) / k;
            };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

function socioAC(fn, url) {
    $('#socio').focus().autocomplete({
        autoSelect: true, autoFocus: true,
        minLength: 2, source: url,
        select: function(event, ui) {
            fn(ui.item);
        }
    });
}

function spinInit() {
    muestraSpin = true;
    jQuery(document).ajaxStart(function() {
        //$('body').css('cursor', 'wait');
        if (muestraSpin) {
            $('#spin').show();
        }
    });
    jQuery(document).ajaxStop(function() {
            $('#spin').hide();
    });

}