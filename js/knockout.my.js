ko.bindingHandlers.select2 = {
    init: function(el, valueAccessor, allBindingsAccessor) {
        //console.log(el, valueAccessor, allBindingsAccessor, viewModel);
        var allBindings = allBindingsAccessor();
        ko.utils.domNodeDisposal.addDisposeCallback(el, function() {
            $(el).select2('destroy');
        });
        allBindings.select2.initSelection = function(element, callback) {
            var data = [];
            $(element.val().split(",")).each(function() {
                data.push({id: this, text: this});
            });
            callback(data);
        };
        ko.utils.registerEventHandler(el, "select2-selected", function(data) {
            if ('obj' in allBindingsAccessor()) {
                allBindingsAccessor().obj(data.choice);
            }
        });
        var select2 = ko.utils.unwrapObservable(allBindings.select2);
        $(el).select2(select2);
    }
};
/*

 editableHTML

 <h4> edit </h4>
 <ul class="list" data-bind="foreach:titles">
 <li class="title" data-bind="editableHTML:$data" contenteditable="true"></li>
 </ul>

 <h4> view </h4>
 <ul class="" data-bind="foreach: titles">
 <li class="title" data-bind="text:$data"></li>
 </ul>
 */
ko.bindingHandlers.editableHTML = {
    init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
        var $element = $(element);
        var initialValue = ko.utils.unwrapObservable(valueAccessor());
        var $origObservable = bindingContext.$rawData;

        $element.html(initialValue);

        $element.on('keyup', function() {
            var curVal = valueAccessor();
            var newVal = $element.html();

            if (ko.isObservable($origObservable))
                $origObservable(newVal);
        });
    }
};

ko.observable.fn.toString = function() {
    return "observable: " + ko.toJSON(this(), null, 2);
};

ko.computed.fn.toString = function() {
    return "computed: " + ko.toJSON(this(), null, 2);
};

ko.observableArray.fn.find = function(fld, value){
    return ko.utils.arrayFirst(this(), function(o) {
        return o[fld]() === value;
    });
}
