ko.bindingHandlers['anchorenable'] = {
    'update': function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        if (value && element.disabled) {
            element.disabled = false;
            element.removeAttribute('disabled');
            var hrefBackObject = element.getAttribute('href_back');
            if (hrefBackObject && hrefBackObject != '') {
                element.setAttribute('href', hrefBackObject);
                element.removeAttribute('href_back');
            }
        }
        else if ((!value) && (!element.disabled)) {
            element.disabled = true;
            element.setAttribute("disabled", true);
            var hrefObject = element.getAttribute('href');
            if (hrefObject && hrefObject != '') {
                element.setAttribute('href_back', hrefObject);
            }
            element.removeAttribute("href");
        }
    }
};

ko.bindingHandlers.instantValue = {
    init: function (element, valueAccessor, allBindings) {
        var newAllBindings = function () {
            // for backwards compatibility w/ knockout  < 3.0
            return ko.utils.extend(allBindings(), {valueUpdate: 'afterkeydown'});
        };
        newAllBindings.get = function (a) {
            return a === 'valueUpdate' ? 'input' : allBindings.get(a);
        };
        newAllBindings.has = function (a) {
            return a === 'valueUpdate' || allBindings.has(a);
        };
        ko.bindingHandlers.value.init(element, valueAccessor, newAllBindings);
    },
    update: ko.bindingHandlers.value.update
};
ko.bindingHandlers.toJSON = {
    update: function (element, valueAccessor) {
        return ko.bindingHandlers.text.update(element, function () {
            return ko.toJSON(valueAccessor(), null, 2);
        });
    }
};
ko.bindingHandlers.currency = {
    symbol: ko.observable('$'),
    update: function (element, valueAccessor, allBindingsAccessor) {
        return ko.bindingHandlers.text.update(element, function () {
            var value = +(ko.utils.unwrapObservable(valueAccessor()) || 0),
                symbol = ko.utils.unwrapObservable(allBindingsAccessor().symbol === undefined
                    ? ko.bindingHandlers.currency.symbol
                    : allBindingsAccessor().symbol);
            return symbol + value.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
        });
    }
};
ko.bindingHandlers.select2 = {
    init: function (el, valueAccessor, allBindingsAccessor) {
        //console.log(el, valueAccessor, allBindingsAccessor, viewModel);
        var allBindings = allBindingsAccessor();
        ko.utils.domNodeDisposal.addDisposeCallback(el, function () {
            $(el).select2('destroy');
        });
        allBindings.select2.initSelection = function (element, callback) {
            var data = [];
            $(element.val().split(",")).each(function () {
                data.push({id: this, text: this});
            });
            callback(data);
        };
        ko.utils.registerEventHandler(el, "select2-selected", function (data) {
            if ('obj' in allBindingsAccessor()) {
                allBindingsAccessor().obj(data.choice);
            }
        });
        var select2 = ko.utils.unwrapObservable(allBindings.select2);
        $(el).select2(select2);
    }
};
/*

 editableHTML

 <h4> edit </h4>
 <ul class="list" data-bind="foreach:titles">
 <li class="title" data-bind="editableHTML:$data" contenteditable="true"></li>
 </ul>

 <h4> view </h4>
 <ul class="" data-bind="foreach: titles">
 <li class="title" data-bind="text:$data"></li>
 </ul>
 */
ko.bindingHandlers.editableHTML = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        var $element = $(element);
        var initialValue = ko.utils.unwrapObservable(valueAccessor());
        var $origObservable = bindingContext.$rawData;

        $element.html(initialValue);

        $element.on('keyup', function () {
            var curVal = valueAccessor();
            var newVal = $element.html();

            if (ko.isObservable($origObservable))
                $origObservable(newVal);
        });
    }
};
ko.bindingHandlers.editableText = {
    init: function (element, valueAccessor) {
        $(element).on('blur', function () {
            var observable = valueAccessor();
            observable($(this).text());
        });
    },
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).text(value);
    }
};
ko.observable.fn.toString = function () {
    return "observable: " + ko.toJSON(this(), null, 2);
};

ko.computed.fn.toString = function () {
    return "computed: " + ko.toJSON(this(), null, 2);
};
