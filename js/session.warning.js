var controlSession = (function() {
	var url, urlIndex, timer, timerMuestraSegundos, warningBegin, activeDialog = false;
	var cerrarSesion = function() {
		logOut();
	};
	var pingServer = function(urlKeep) {
		muestraSpin = false;
		$.post(urlKeep, function() {
			muestraSpin = false;
		});
	};
	function initSessionMonitor(params) {
		$("#menubar").append("<li id='idle-minutes' style='float:right;font-size:9px' class='ui-menubar-item'></li>");
		url = params.urlKeep;
		urlIndex = params.urlIndex;

		sessionTimeLeftAjax = $.get(params.urlSessionSecondsLeft);
		muestraSpin = false;
		$.when(sessionTimeLeftAjax).then(function(sessionSecondsLeft) {
			muestraSpin = true;
			params.expirationSeconds = sessionSecondsLeft;
			initSessionMonitorDataReady(params);
		});

	}

	function initSessionMonitorDataReady(params) {
		initDialog();
//		console.log(params.expirationSeconds/60 + " Minutos es la duración de la sesión");
		$.idleTimer(1000 * params.expirationSeconds);

		muestraTiempoParaIdle();
		setInterval(muestraTiempoParaIdle, 5000);

		setInterval(function() {
			pingServer(params.urlKeep);
		}, (params.expirationSeconds - 10) * 1000);

		$(document).bind("active.idleTimer", function() {
			console.log("active");
		});

		$(document).bind("idle.idleTimer", function() {
			console.log("idle");
			if (jQuery.ui && !activeDialog) {
				activeDialog = true;
				warningBegin = new Date().getTime();
				$("#dialog-sesion").dialog("open");
				timer = setTimeout(function() {
					$("#dialog-sesion").dialog("close");
					cerrarSesion();
				}, params.warningSeconds * 1000);

				actualizaSegundosRestantes(params.warningSeconds);
				timerMuestraSegundos = setInterval(function() {
					actualizaSegundosRestantes(params.warningSeconds);
				}, 1000);
			}
		});
	}
	function muestraTiempoParaIdle() {
		$("#idle-minutes").html(Math.ceil(round($.idleTimer("getRemainingTime") / 1000 / 60, 0)));
	}
	function actualizaSegundosRestantes(warningSeconds) {
		var diff = Math.abs((warningBegin - new Date().getTime()));
		diff = Math.round(((warningSeconds * 1000) - diff) / 1000, 0);
		$("#dialog-tiempo-cierre").html(diff + " Segundos");
	}
	function initDialog() {
		if (jQuery.ui) {
			$("body").append("<div id='dialog-sesion'>\n\
				<div style='text-align:center;margin-top:10px'>\n\
				<span style='font-color:red;font-size:20px'>Tiempo para cierre: </h1>\n\
				<span style='font-color:red;font-size:20px' id='dialog-tiempo-cierre'></span>\n\
				</div>\n\
				</div>");

			$("#dialog-sesion").dialog({
				close: function() {
					clearTimeout(timerMuestraSegundos);
					clearTimeout(timer);
					activeDialog = false;
				},
				autoOpen: false,
				position: {
					my: "center",
					at: "center",
					of: window
				},
				width: 'auto',
				title: "La sesión está por finalizar",
				buttons: {
					"Me quedo": function() {
						$(this).dialog("close");
					},
					"Cerrar sesión": function() {
						$(this).dialog("close");
						cerrarSesion();
					}
				}
			});
		}
	}
	function logOut() {
		window.location.href = urlIndex;
	}

	return {init: initSessionMonitor};

})();