

var docViewModel = (function() {
    var self = this;
	self.debug = ko.observableArray([]);
	self.data = ko.observable({});
	self.options = ko.observable({});
	self.apps = ko.observableArray([]);
	self.valores = ko.observableArray([]);
	self.valorVacio;
	self.cant = ko.observable(0);

	var init = function(options) {
		var deferred = $.Deferred();
		self.timerInit = (new Date()).getTime();
		initOptions(options);
		console.log("init:", "socio_id:" + self.options.socio_id, "doc_id:" + self.options.doc_id, "doc_tipo_id:" + self.options.doc_tipo_id, "doc_tipo_str:" + self.options.doc_tipo_str);
		getAllData().done(function() {
			dataReady();
			deferred.resolve("ok");
		});
		return deferred.promise();
	};

	function getAllData() {
		var appData = getDataAjax();
		var valorVacioAjax = getValorVacioAjax();
		var valoresAjax = self.options.doc_id ? getItemsAjax(self.options.valoresUrl, self.valores) : [];
		var appsAjax = self.options.socio_id ? getItemsAjax(self.options.appsUrl, self.apps) : [];
		return $.when(appData, appsAjax, valoresAjax, valorVacioAjax).done();
	}

	function getDataAjax() {
		return $.getJSON(self.options.dataUrl, {doc_id: self.options.doc_id, doc_tipo_id: self.options.doc_tipo_id}, function(data) {
			for (var name in data) {
				if (self.data()[name]) {
					if (self.data()[name]() !== data[name]) {
						self.data()[name](data[name]);
						//self.data()[name].valueHasMutated();
					}
				} else {
					self.data()[name] = ko.observable(data[name]);
				}
			}
			self.data().pago_a_cuenta_detalle(self.data().pago_a_cuenta_detalle() ===
					self.data().pago_a_cuenta_detalle() ? "A cuenta" : "");
			self.data().pago_a_cuenta_importe(self.data().pago_a_cuenta_importe() ===
					self.data().pago_a_cuenta_importe() ? 0 : 0);
			self.data().numeracion_manual = false;
		});
	}

	function valoresAdd() {
		var valor = ko.observable({});
		for (var name in self.valorVacio) {
			valor()[name] = ko.observable(data[name]);
		}
		return valor;
	}

	function getValorVacioAjax() {
		self.valorVacio = ko.observable({});
		return $.getJSON(self.options.valorVacioUrl, null, function(data) {
			self.valorVacio = data;
			return;
			for (var name in data) {
				self.valorVacio()[name] = ko.observable(data[name]);
			}
		});
	}

	function getItemsAjax(url, target) {
		return $.getJSON(url, {doc_id: self.options.doc_id, socio_id: self.options.socio_id}, function(data) {
			target.removeAll();
			$.each(data, function(i, o) {
				var tr = {};
				for (var name in o) {
					tr[name] = ko.observable(o[name]);
				}
				target.push(tr);
			});
		});
	}

	function initOptions(options) {
		self.options.submitUrl = options.submitUrl;
		self.options.valorVacioUrl = options.valorVacioUrl;
		self.options.dataUrl = options.dataUrl;
		self.options.valoresUrl = options.valoresUrl;
		self.options.appsUrl = options.appsUrl;
		self.options = $.extend({
			doc_id: null,
			doc_tipo_id: null,
			doc_tipo_str: "Pasar doc_tipo_str",
			socio_id: null
		}, options);
	}

	function dataReady() {
		self.numeroCompleto = ko.computed(function() {
	//		self.cant(self.cant() + 1);
			console.log("numeroCompleto");
			return self.data().sucursal().lPad(4, "0") + "-" + self.data().numero().lPad(8, "0");
		});

		self.appsTotal = ko.computed(function() {
			self.cant(self.cant() + 1);
			console.log("apps total");
			var total = 0;
			for (var p = 0; p < self.apps().length; ++p)
			{
				total += Number(self.apps()[p].importe());
			}
			return round(total, 2);
		});

		self.valoresTotal = ko.computed(function() {
			//self.cant(self.cant()+1);
			console.log("valores total");
			var total = 0;
			for (var p = 0; p < self.valores().length; ++p)
			{
				total += Number(self.valores()[p].importe());
			}
			return round(total, 2);
		});

		self.data().pago_a_cuenta_importe.subscribe(function() {
//			self.cant(self.cant()+1);
			console.log("pago a cta subsc");
			if (typeof self.data().pago_a_cuenta_importe() !== "number") {
				var valor = Number(self.data().pago_a_cuenta_importe());
				self.data().pago_a_cuenta_importe(isNaN(valor) ? 0 : valor);
			}
		});

		self.total = ko.computed(function() {
//			self.cant(self.cant() + 1);
			console.log("total");
			return pagoTotal() - self.valoresTotal();
		});

		self.totalCero = ko.computed(function() {
			//self.cant(self.cant() + 1);
			console.log("totalCero");
			return (round(pagoTotal() - self.valoresTotal(), 2)) === 0;
		});

		self.valoresVacios = self.valores.filter(function(a, b) {
			console.log("valoresVacios");
			return Number(a.importe()) === 0.00;
		});

//			Agrega nuevo valor cuando hace falta
			ko.computed(function() {
//			self.cant(self.cant() + 1);
			console.log("nuevo valor?");
			if ((pagoTotal() > self.valoresTotal()) && (self.valoresVacios().length === 0)) {
				var nuevoValor = valoresAdd();
				nuevoValor().tipo_nombre("Efectivo");
				nuevoValor().importe(0);
				self.valores.push(nuevoValor());
			} else {
				borraValoresVacios();
			}
		});

		self.selectedValor = function(e) {
			if (e) {
				self.activeValor = e;
				borraValoresVacios();
			}
			return self.activeValor;
		};

		ko.bindingHandlers.numeroValido = {
			init: function(element) {
				$(element).blur(function() {
					var importe = ko.dataFor(element).importe;
					importe(round(importe(), 2) || 0);
				});
			}
		};

		function pagoTotal() {
			return self.appsTotal() + self.data().pago_a_cuenta_importe();
		}

		function borraValoresVacios() {
			var recs = (pagoTotal() > self.valoresTotal()) ?
					self.valoresVacios().length - 1 :
					self.valoresVacios().length;
			$.each(self.valoresVacios(), function(i, a) {
				if (a && self.valores.indexOf(a) && i < recs && self.activeValor !== a) {
					self.valores.remove(a);
				}
			});
		}


		console.log("init: listo...", (new Date()).getTime() - self.timerInit, "Milliseconds");

	}

	self.fmt = function(number) {
		return "$ " + number_format(number);
	};

	self.submit = function() {
		var data = {
			data: self.data,
			apps: self.apps,
			valores: self.valores,
			opciones: self.options
		};
		$.post(self.submitUrl, {
			data: ko.toJS(data),
			contentType: 'application/json',
			type: 'POST'
		}, function(a) {
			console.log(a);
			$("#log").html(a);
		});
	};

	self.cancel = function() {
		getAllData();
	};

	return {init: init, debug: self.debug, root: {data: self.data, apps: self.apps, valores: self.valores}};

})();

