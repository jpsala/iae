docViewModel = (function() {
	var self = this;
	var pub = {};
	var initReady = $.Deferred();
	pub.data = {};
	pub.data.valores = ko.observableArray([]);
	pub.init = function(url, doc_id) {
		console.log("init begin");
		$.getJSON(url, {doc_id: doc_id}, function(data) {
			parseData(data);
			initReady.resolve(this);
		});
		return initReady.promise();
	};
	function parseData(data){
		pub.data.socio = ko.observable(data.socio);
		pub.data.numero = ko.observable(data.numero);
		pub.data.fecha_valor = ko.observable(data.fecha_valor);
		pub.data.concepto_id = ko.observable(data.concepto_id);
		$.each(data.valores,function(i, o){
			pub.data.valores.push(new Valor(o));
		});
		
	}
	function Valor(o){
		this.id = ko.observable(o.id);
		this.tipo = ko.observable(o.abreviacion);
		this.fecha = ko.observable(o.fecha);
		this.destino_id = ko.observable(o.destino_id);
		//this. = ko.observable(o.id);
	}
	return pub;
})();


