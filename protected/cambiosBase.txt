> tabla comentario

delimiter $$

CREATE TABLE `comentario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comentario` text,
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `Comentario_id` int(10) unsigned DEFAULT NULL,
  `contexto` varchar(35) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Comentario_user1` (`user_id`),
  KEY `fk_Comentario_Comentario1` (`Comentario_id`),
  CONSTRAINT `fk_Comentario_Comentario1` FOREIGN KEY (`Comentario_id`) REFERENCES `comentario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Comentario_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1$$


