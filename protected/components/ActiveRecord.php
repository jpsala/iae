<?php

abstract class ActiveRecord extends GxCActiveRecord {

    public function afterSave() {
        parent::afterSave();
        $this->logCambio($this->isNewRecord ? 'C' : 'U');
    }

    public function afterDelete() {
        parent::afterDelete();
        $this->logcambio('R');
    }

    private function logCambio($tipoCambio) {
        if ($this->tableName() !== 'cambio') {
            $c = new Cambio();
            $c->attributes = array(
                'clase' => $this->tableName(),
                'fecha' => new CDbExpression('NOW()'),
                'user_id' => Yii::app()->user->id,
                'tipo' => $tipoCambio,
            );
            $c->insert();
        }
    }

}