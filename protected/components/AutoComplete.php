<?php
/**
 * hay que sobreescribir el getSelect, las propiedades a tener en cuenta son
 * 
 * @var termIsNum Si el termino a buscar es numerico (simplemente is_numeric($this->term))
   @var term  El termino a buscar (viene del post)
   @var ci Si es case insensitive (viene del post)
 */
class AutoComplete {

  private $label, $value;
  private $return;
  public $termIsNum, $ci, $term, $_select, $otros;

  public function __construct($GET) {
    $this->term = $GET['term'];

    $this->label = isset($GET['label']) ? $GET['label'] : 'NOMBRE';
    $this->value = isset($GET['value']) ? $GET['value'] : 'ID';
    $this->otros = isset($GET['otros']) ? $GET['otros'] : null;

    $this->ci = isset($GET['ci']);


    $this->termIsNum = is_numeric($this->term);

  }

  public function render() {


    $result = Yii::app()->db->createCommand($this->getSelect())->query();
    $this->return = array();
    foreach ($result as $row) {
      $this->return[] = $this->fillRow($row);
    }
    echo json_encode($this->return);
  }

  public function fillRow($row) {
    $ret = array('value' => $row[$this->value], 'label' => $row[$this->label]);
    if ($this->otros) {
      foreach ($this->otros as $otro) {
        $ret['otros'][$otro] = $row[$otro];
      }
    }
    return $ret;
  }

  public function getSelect() {
    throw new Exception('No se puede usar directamente la clase AutoComplete');

// ejemplo (alumnos)
// 
//    if ($this->termIsNum) {
//      $this->select = sprintf("select * from te_alumnos a where a.matricula = '%s'", $this->term);
//    } else {
//      if (!$this->ci) { //Case sensitive
//        $this->select = sprintf("select * from te_alumnos a where a.nombre like '%s'", '%' . $this->term . '%');
//      } else { //Case insensitive
//        $this->select = sprintf(
//                "select * from te_alumnos a where a.nombre like '%s'  or a.nombre like '%s'", '%' . $this->term . '%', '%' . strtoupper($this->term) . '%'
//        );
//      }
//    }
  }

}

?>
