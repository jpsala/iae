<?php

class FB extends CApplicationComponent {

  public $host;
  public $ibase_charset;
  public $usuario;
  public $pass;
  private $_db;
  private $_club = false;
  private $baseClub = 'c:\iae_club.fdb';

//    private $baseClub = '192.168.2.254:/export/sistema/data/iae_club_vacia.gdb';

  public function getConnection() {
    if (isset($this->_db)) {
      return $this->_db;
    } else {
      $this->_db = $this->connect();
      return $this->_db;
    }
  }

  public function getClub() {
    return $this->_club;
  }

  public function setClub($value) {
    if ($this->_club !== $value) {
      $this->_club = $value;
      $this->_db = $this->connect();
    }
  }

  public function qry($qry) {
    $this->getConnection();
    return ibase_query($qry);
  }

  public function qryAll($qry) {
    $this->getConnection();
    $q = ibase_query($qry);
    while ($r = ibase_fetch_object($q)) {
      $ret[] = $r;
    }
    return $ret;
  }

  public function qryRow($qry) {
    $this->getConnection();
    return ibase_fetch_object(ibase_query($qry));
  }

  public function qryRowObj($qry) {
    $this->getConnection();
    $r = ibase_query($qry);
    return ibase_fetch_object($r);
  }

  public function exec($qry) {
    $this->getConnection();
    return ibase_execute(ibase_prepare($qry));
  }

  public function init() {
    parent::init();
    $this->getConnection();
  }

  public function connect() {
    if (!isset($this->ibase_charset))
      throw new Exception("Falta definir \"ibase_charset\" en config/main, en firebird ej. \"UNICODE_FSS\"");;
    return ibase_connect($this->_club ? $this->baseClub : $this->host, $this->usuario, $this->pass, $this->ibase_charset   );
  }

}

?>