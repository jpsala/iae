<?php

/**
 * Ejemplo:
 * <pre>
 * $jqGrid = new <b>JqGrid()</b>;
 * $jqGrid->fields = array("nombre", "importe");
 * $jqGrid->selectCount = "SELECT count(*) as cant FROM articulo a";
 * $jqGrid->filter = "DocController::movBancoArticulosfilter";
 * $jqGrid->select = "
 * &nbsp;Select a.id, a.nombre, a.precio_neto as importe
 * &nbsp;&nbsp; from articulo a
 * &nbsp;&nbsp; inner join articulo_tipo t on t.id = a.articulo_tipo_id
 * &nbsp;&nbsp; where t.novedad_tipo = 3";
 * $jqGrid->render();
 * </pre>
 * @abstract
 */
class JqGrid extends CComponent {

  public $count, $total_pages, $totalrows, $start;
  public $responce;
  private $_select;
  public $attributes;

  /**
   * @var ID necesario para devolver al jqgrid
   */
  public $id;

  /**
   * @var array son los campos que se van a devolver al jqgrid, deben estar en select
   */
  public $fields;

  /**
   * Ejemplo:
   * <pre>
   * <code>
   *   $jqGrid->filter = "DocController::movBancoArticulosfilter"
   * </code>
   *   y después:
   * <code>
   *   public static function movBancoArticulosfilter($field, $row) {
   *   &nbsp;return $row[$field];
   *   }* </pre>
   * </code>
   * @var function
   */
  public $filter = null;

  public function __construct() {
    $this->id = 'id';
    foreach ($_REQUEST as $key => $value) {
      $this->attributes[$key] = $value;
    }
    if (!$this->attributes["sidx"])
      $this->attributes["sidx"] = 1;
    $this->totalrows = isset($this->attributes["totalrows"]) ? $this->attributes["totalrows"] : false;
    if ($this->totalrows) {
      $this->rows = $this->totalrows;
    }
  }

  public function setSelectCount($value) {
    $row = Yii::app()->db->createCommand($value)->queryRow();

    $this->count = $row['cant'];

    if ($this->count > 0 && $this->rows > 0) {
      $this->total_pages = ceil($this->count / $this->rows);
    } else {
      $this->total_pages = 0;
    }

    if ($this->page > $this->total_pages)
      $this->page = $this->total_pages;

    $this->start = $this->rows * $this->page - $this->rows;

    if ($this->start < 0)
      $this->start = 0;
  }

  public function getSelect() {
    return $this->_select;
  }

  public function setSelect($value) {
    $this->_select = $value;
    $responce->page = $this->attributes["page"];
    $responce->total = $this->total_pages;
    $responce->records = $this->count;
    $result = Yii::app()->db->createCommand(
                    $this->_select .
                    " ORDER BY " . $this->__get('sidx') . " " . $this->__get('sord') .
                    " LIMIT " . $this->start . "," . $this->rows
            )->query();

    $i = 0;
    foreach ($result as $row) {
      $responce->rows[$i]["id"] = $row[$this->id];
      foreach ($this->fields as $field) {
        if ($this->filter) {
          $responce->rows[$i]['cell'][] = call_user_func($this->filter, $field, $row);
        } else {
          $responce->rows[$i]['cell'][] = $row[$field];
        }
      }
      $i++;
    }
    echo json_encode($responce);
    die;
    $this->responce = json_encode($responce);
  }

  public function render() {
    echo $this->responce;
  }

  public function __get($name) {
    if (key_exists($name, $this->attributes)) {
      return $this->attributes[$name];
    }
  }

}

?>
