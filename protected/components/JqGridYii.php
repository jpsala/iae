<?php
/*
 * 
 */
class JqGridYii extends CComponent {

  public $page, $limit, $count, $total_pages;
  public $sidx, $start;
  public $sord;
  public $sidex;
  public $totalrows;
  public $responce;
  private $_select;
  /**
   * @var ID necesario para devolver al jqgrid
   */
  public $id;
  /**
   * @var array son los campos que se van a devolver al jqgrid, deben estar en select
   */
  public $fields;

  public function __construct() {
    $this->id = 'id';
    $this->page = $_REQUEST['page'];
    $this->limit = $_REQUEST['rows'];
    $this->sidx = $_REQUEST['sidx'];
    $this->sord = $_REQUEST['sord'];
    if (!$this->sidx)
      $sthis->idx = 1;
    $this->totalrows = isset($_REQUEST['totalrows']) ? $_REQUEST['totalrows'] : false;
    if ($this->totalrows) {
      $this->limit = $this->totalrows;
    }
  }

  public function setSelectCount($value) {
    $row = Yii::app()->db->createCommand($value)->queryRow();
    
    $this->count = $row['cant'];

    if ($this->count > 0 && $this->limit > 0) {
      $this->total_pages = ceil($this->count / $this->limit);
    } else {
      $this->total_pages = 0;
    }

    if ($this->page > $this->total_pages)
      $this->page = $this->total_pages;

    $this->start = $this->limit * $this->page - $this->limit;

    if ($this->start < 0)
      $this->start = 0;
  }

  public function getSelect() {
    return $this->_select;
  }
  public function setSelect($value) {
    $this->_select = $value;
    $responce->page = $this->page;
    $responce->total = $this->total_pages;
    $responce->records = $this->count;
    $result = Yii::app()->db->createCommand($value)->query();
    
    $i = 0;
    foreach ($result as $row) {
      $responce->rows[$i][] = $row[$this->id];
      foreach($this->fields as $field){
        $responce->rows[$i]['cell'][] = $row[$field];
      }
      $i++;
    }
    $this->responce = json_encode($responce);
  }
  
  public function render(){
    echo $this->responce;
  }

}

?>
