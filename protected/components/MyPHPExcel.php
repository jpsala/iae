<?php

$phpExcelPath = Yii::getPathOfAlias('ext');
spl_autoload_unregister(array('YiiBase', 'autoload'));
include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
spl_autoload_register(array('YiiBase', 'autoload'));

class MyPHPExcel extends PHPExcel {

    public function __construct() {
        setlocale(LC_ALL,'es');
        parent::__construct();
        $this->setActiveSheetIndex(0);
        $validLocale = PHPExcel_Settings::setLocale('es_ar');
        if (!$validLocale) {
	echo 'Unable to set locale to '.$locale." - reverting to en_us<br />\n";
}

        $this->getActiveSheet()->setTitle("hoja1");
    }

    public function output($nombre) {
       header('Content-Type: application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment;filename=$nombre.xlsx");
        header('Cache-Control: max-age=0');
        // header("Content-type: application/pdf",true,200);	
        $objWriter = PHPExcel_IOFactory::createWriter($this, 'Excel2007');
        try{
          $objWriter->save('php://output');
        } catch (Exception $e) {
          ve2('error en output de myphpexcel');
          vd2($e);
        }
    }

}

?>