<?php
// include_once("formulas/" . Ciclo::getCicloNombreParaCargaDeNotas() . ".php");
class Notas extends stdClass {
  /**
  * @param {array} options 
  *  array(
  *      'ciclo_id' => Ciclo::getCicloIdParaCargaDeNotas(),
  *      'alumno_id'=> null,
  *      // 'division_id'=>false,
  *      // 'asignatura_id'=>false
  *    );
  */
  public static function getNotas($_options = []) {
    $defaults = array(
      'ciclo_id' => Ciclo::getCicloIdParaCargaDeNotas(),
      'alumno_id'=> null,
      // 'division_id'=>false,
      // 'asignatura_id'=>false
    );
    $options = (object) array_merge($defaults, $_options);
    
    // if(!isset($options->division_id)) {
    //   throw new Exception("getNotas, necesito una division", 1);
    // }

    $where = $options->alumno_id ? 'a.id = '.$options->alumno_id : 'true';
    $where = $where . (isset($options->division_id) ? ' and ad.division_id = '.$options->division_id : '');
    $where = $where . (isset($options->ciclo_id) ? ' and c1.id = '.$options->ciclo_id : '');
    $where = $where . (isset($options->asignatura_id) ? ' and a1.id = '.$options->asignatura_id : '');
    $selectNotas = "
      SELECT a.id, lp.id as periodo_id, lp.nombre as periodo, li.nombre AS sub_periodo, 
            a1.id as asignatura_id, a1.nombre AS asignatura, n.nota, li.tipo_nota, li.estado, 
            li.final, li.nota_del_periodo, c.*
        FROM nota n
          INNER JOIN alumno_division ad ON n.Alumno_Division_id = ad.id
          INNER JOIN asignatura a1 ON n.Asignatura_id = a1.id
          INNER JOIN cursos_view c ON c.division_id = ad.Division_id
          INNER JOIN alumno a ON ad.Alumno_id = a.id
          INNER JOIN logica_item li ON n.Logica_item_id = li.id
          INNER JOIN logica_periodo lp ON li.Logica_Periodo_id = lp.id
          INNER JOIN ciclo c1 ON ad.Ciclo_id = c1.id
      WHERE $where
      ORDER BY c.anio_orden, c.division_nombre, lp.orden, li.orden, a.id
    ";
    $qry = Helpers::qryAll($selectNotas);
    // vd2($qry);
    return $qry;
  }

  public static function adapter($nombre, $notas) {
    if( $nombre === 'asignatura') {
      return self::adapterAsignatura($notas);
    }
    return $notas;
  }

  private static function adapterAsignatura($notasRaw) {
    $periodos = [];
    $asignaturas = [];
    $notas = [];
    foreach ($notasRaw as $nota) {
      // vd2($nota);
      if($nota['nota']) {
        $periodos[$nota['periodo_id']] = $nota['periodo'];
        $asignaturas[$nota['asignatura_id']] = $nota['asignatura'];
        // $res->asignaturas[$nota['periodo']][$nota['asignatura']] = $nota['nota'];
        // $notas[$periodos[$nota['periodo_id']]][$asignaturas[$nota['asignatura']]] = $nota['nota'];
        $notas[$nota['periodo_id']][$nota['asignatura_id']] = $nota['nota'];
      }else{
        $periodos[$nota['periodo_id']] = $nota['periodo'];
        $asignaturas[$nota['asignatura_id']] = $nota['asignatura'];
        // $res->asignaturas[$nota['periodo']][$nota['asignatura']] = $nota['nota'];
        // $notas[$periodos[$nota['periodo_id']]][$asignaturas[$nota['asignatura']]] = $nota['nota'];
        $notas[$nota['periodo_id']][$nota['asignatura_id']] = '';

      }
    }
    // vd2($periodos);
    return ['periodos' => $periodos, 'asignaturas' => $asignaturas, 'notas' => $notas];
  }
}