<?php

include_once("formulas/" . Ciclo::getCicloNombreParaCargaDeNotas() . ".php");

class NotasAdmin extends stdClass {

  public static $logica_id, $nivel_id, $items, $destino, $notasCache,
	   $initialized = false, $division_asignaturas, $ciclo_id;

  public static function init($division_id) {
	self::$initialized = true;
	self::$items = array();

	$ciclo_id = self::$ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
	list(self::$logica_id, self::$nivel_id) = Helpers::qryDataRow("
            select l.id, n.id
                from logica l
                    inner join logica_ciclo lc on lc.Logica_id = l.id and lc.Ciclo_id = $ciclo_id
                    inner join asignatura_tipo t on t.id = lc.Asignatura_Tipo_id
					inner join nivel n on n.id = t.Nivel_id
					inner join anio a on a.Nivel_id = n.id
					inner join division d on d.Anio_id = a.id and d.id = $division_id
         ");
	$items = Yii::app()->db->createCommand("
			select lp.id as logica_periodo_id, li.condicion, lp.orden as logica_periodo_orden,
				   li.nombre_unico as logica_item_nombre_unico, li.logica_periodo_id,
				   li.orden as logica_item_orden, li.manual, li.id, li.nombre, li.tipo_nota, li.estado,
				   li.tiene_conducta, li.imprime_en_boletin, li.imprime_en_planilla,
				   lp.nombre_unico as logica_periodo_nombre_unico, li.nota_del_periodo
			  from logica_item li
				inner join logica_periodo lp on lp.id = li.logica_periodo_id
			  where li.logica_id = :logica_id
			  order by lp.orden, li.orden
	")->queryAll(true, array("logica_id" => self::$logica_id));
	foreach ($items as $item) {
	  self::$items[$item["logica_item_nombre_unico"]] = $item;
	}
	self::$division_asignaturas = Helpers::qryAll("
            select da.asignatura_id as id, a.nombre as materia, da.boletin
                from division_asignatura  da
                    inner join asignatura a on a.id = da.asignatura_id
                where da.Division_id = $division_id  and da.boletin = 1
                order by case when da.orden = 0 or da.orden is null then a.orden else da.orden end
          ");
  $selectNotas = "
            select ad.id as alumno_division_id, n.asignatura_id, nota, li.manual, logica_periodo_id, li.id as logica_item_id, \"\" as error,
                        li.nombre, li.nombre_unico as logica_item_nombre_unico, /*tipo_nota,*/ estado, li.tipo_nota,
                        li.imprime_en_boletin
                        /*, li.abrev as logica_item_abrev, li.imprime_en_planilla, lp.abrev as logica_periodo_abrev  */
                from nota n
                    inner join alumno_division ad on ad.id = n.Alumno_Division_id
                    inner join division d on d.id = ad.Division_id
                    inner join logica_item li on li.id = n.logica_item_id
                    inner join logica_periodo lp on lp.id = li.logica_periodo_id
                where d.id = $division_id and ciclo_id = $ciclo_id
        ";
  try {
  	$notasCache = Helpers::qryAll($selectNotas);
  } catch(Exception $e) {
    vd2($e);
  }
  foreach ($notasCache as $nota) {
	  self::$notasCache[$nota["alumno_division_id"]][$nota["asignatura_id"]][$nota["logica_item_id"]] = $nota;
	}
  }

  public static function getNotasDivision($division_id) {
	$notas = array();
	self::init($division_id);
	$alumno_divisiones = Helpers::qryAll("
            select ad.id, concat(a.apellido, \", \", a.nombre) as alumno,
            concat(asi.nombre, \" \", ns.nombre) as siguiente_division,
                        d.nombre as division, an.nombre as anio, n.nombre as nivel,
                        a.matricula
                from alumno_division ad
                    inner join division d on d.id = ad.division_id
                    inner join anio an on an.id = d.anio_id
                    inner join nivel n on n.id = an.nivel_id
                    left join division ds on ds.id = d.division_id_siguiente
                    inner join alumno a on a.id = ad.Alumno_id
                    inner join alumno_estado ae on ae.id = a.estado_id
                            and (ae.activo_edu = 1 or ae.activo_edu =0)
                    left join anio asi on asi.id = ds.Anio_id
                    left join nivel ns on ns.id = asi.Nivel_id
                where ad.Division_id = :division_id and ad.ciclo_id = :ciclo_id
                            and (ad.activo or ad.promocionado or ad.egresado)
                            and ae.activo_edu
                      and a.activo = 1 and not ad.borrado /*  and a.matricula in (2971 )*/
                 order by a.sexo desc, a.apellido, a.nombre
          ", array("division_id" => $division_id, "ciclo_id" => self::$ciclo_id));
  // vd2($alumno_divisiones);
	foreach ($alumno_divisiones as $ad) {
	  //self::$alumno_division_id = $ad["id"];
	  $ns = NotasAdmin::getNotasAlumnoDivision($ad["id"]);
    // vd2($ns);
	  $notas[$ad["alumno"]]["notasItems"] = $ns["notasItems"];
      $notas[$ad["alumno"]]["notasPeriodos"] = $ns["notasPeriodos"];
      $notas[$ad["alumno"]]["final"] = $ns["final"];
	  $notas[$ad["alumno"]]["conducta"] = NotasAdmin::getConductaAlumnoDivision($ad["id"]);
	  $notas[$ad["alumno"]]["inasistencia"] = NotasAdmin::getInasistenciasAlumnoDivision($ad["id"]);
	  $notas[$ad["alumno"]]["datos"] = array(
		   "nivel" => $ad["nivel"],
		   "anio" => $ad["anio"],
		   "division" => $ad["division"],
		   "matricula" => $ad["matricula"],
		   "siguiente_division" => $ad["siguiente_division"]
	  );
    // vd2($notas);
	}

	return $notas;
  }

  public static function getNotasAlumnoDivision($alumno_division_id) {
	if (!self::$initialized) {
	  throw new Exception("Falta llamar a NotasAdmin::init()");
	}

	foreach (self::$division_asignaturas as $da) {
	  $notas = NotasAdmin::getNotasAlumnoDivisionAsignatura($da["id"], $alumno_division_id);
	  $notasItems[$da["materia"]] = $notas["items"];
	  $notasPeriodos[$da["materia"]] = $notas["periodos"];
	}
	return array(
		 "notasItems" => $notasItems,
		 "notasPeriodos" => $notasPeriodos,
		 "final" => Formulas::getNotaFinal(self::$nivel_id, $alumno_division_id, $notasItems));
  }

  public static function getNotasAlumnoDivisionAsignatura($asignatura_id, $alumno_division_id) {
	if (!self::$initialized) {
	  throw new Exception("Falta llamar a NotasAdmin::init()");
	}
	$logica_id = self::$logica_id;
	$nivel_id = self::$nivel_id;
	$integradora = $nivel_id == 4 ? Helpers::qryScalar("
            select da.integradora
            from division_asignatura da
                inner join alumno_division ad on ad.Division_id = da.Division_id
            where da.Asignatura_id = $asignatura_id and ad.id = $alumno_division_id
        ") : 0;

	$notas = array();
	$notasRet = array();
	$notasRet['integradora'] = $integradora;
	foreach (self::$items as $item) {
	  $nota = null;
	  $item_id = $item["id"];
	  if ($item["manual"]) {
		if (self::$notasCache) {
		  $nota = isset(self::$notasCache[$alumno_division_id][$asignatura_id][$item_id]) ?
			   self::$notasCache[$alumno_division_id][$asignatura_id][$item_id] :
			   null;
		} else {
		  $select = "
            select nota, li.manual, logica_periodo_id, li.id as logica_item_id, \"\" as error,
                        li.nombre, li.nombre_unico as logica_item_nombre_unico, tipo_nota, estado, li.tipo_nota,
                        li.imprime_en_boletin, li.imprime_en_planilla, lp.abrev as logica_periodo_abrev,
                        li.abrev as logica_item_abrev
              from nota n
                inner join logica_item li on li.id = n.logica_item_id
                inner join logica_periodo lp on lp.id = li.logica_periodo_id
              where Alumno_Division_id = $alumno_division_id and
                Asignatura_id = $asignatura_id and
                Logica_Item_id = $item_id
                  ";
		  $nota = Yii::app()->db->createCommand($select)->queryRow();
		}
	  }
	  if (!$nota) {
		$nota["manual"] = $item["manual"];
		$nota["nota"] = "";
		$nota["error"] = "";
		$nota["estado"] = "";
		$nota["imprime_en_boletin"] = $item["imprime_en_boletin"];
	  }
	  $notaVal = Formulas::getNota($nivel_id, $nota, $item, $notas, $integradora);
	  $notas[$item["logica_item_nombre_unico"]] = $notaVal;
	  if ($item["tipo_nota"] == 100 and $integradora !== 1) {
		continue;
	  }
	  if ($item["nota_del_periodo"]) {
		$notasRet["periodos"][$item["logica_periodo_nombre_unico"]] = $notaVal;
	  }
	  $notasRet["items"][$item["logica_item_nombre_unico"]] = $notaVal;
	}
	return $notasRet;
  }

  private static function getInasistenciasAlumnoDivision($alumno_division_id) {
	if (!self::$initialized) {
	  throw new Exception("Falta llamar a NotasAdmin::init()");
	}
	$logica_id = Logica::getLogicaIdPorAlumnoDivison($alumno_division_id);
	$nivel_id = Nivel::getNivelIdDesdeAlumnoDivision($alumno_division_id);
	$itemsSelect = "
                select lp.id as logica_periodo_id, li.condicion, lp.orden as logica_periodo_orden, li.nombre_unico as logica_item_nombre_unico, li.logica_periodo_id,
                       li.orden as logica_item_orden, li.manual, li.id, li.formula, li.nombre, li.tipo_nota, li.estado, li.tiene_conducta,
                       n.inasistencia_detallada, lp.fecha_inicio_ci, lp.fecha_fin_ci
                  from logica_item li
                    inner join logica_periodo lp on lp.id = li.logica_periodo_id
                    inner join nivel n on n.id = $nivel_id
                  where li.logica_id = $logica_id
                  order by lp.orden, li.orden
        ";
	$items = Yii::app()->db->createCommand($itemsSelect)->queryAll();
	$totalInasistencias = 0;
	foreach ($items as $item) {
	  if ($item["tiene_conducta"] == 1) {
		$fecha_desde = $item["fecha_inicio_ci"];
		$fecha_hasta = $item["fecha_fin_ci"];
		$logica_periodo_id = $item["logica_periodo_id"];
		if ($item["inasistencia_detallada"] == 1) {
//                    $totalInasistencias = Helpers::qryScalar("
//                        select sum(t.valor) as inasistencias
//                        from inasistencia i
//                                inner join inasistencia_detalle d on d.inasistencia_id = i.id
//                                inner join inasistencia_tipo t on t.id = d.inasistencia_tipo_id
//                        where i.Alumno_Division_id = $alumno_division_id
//                ");
		  $inasistencias = Helpers::qryScalar("
                        select sum(t.valor) as inasistencias
                        from inasistencia i
                                inner join inasistencia_detalle d on d.inasistencia_id = i.id
                                inner join inasistencia_tipo t on t.id = d.inasistencia_tipo_id
                        where i.Alumno_Division_id = $alumno_division_id and d.fecha between \"$fecha_desde\" and \"$fecha_hasta\"
                ");
		  $inasistenciasRet[$item["logica_periodo_id"]] = $inasistencias;
		} else {
		  $inasistencias = Helpers::qryScalar("
                                select cantidad as inasistencias
                                  from inasistencia i
                                  where i.Alumno_Division_id = $alumno_division_id and
                                        i.logica_periodo_id = $logica_periodo_id
                            ");
		  $inasistenciasRet[$item["logica_periodo_id"]] = $inasistencias;
		}
		$totalInasistencias+=$inasistencias;
	  }
	}

	return array("inasistencias" => $inasistenciasRet, "totalInasistencias" => $totalInasistencias);
  }

  private static function getConductaAlumnoDivision($alumno_division_id) {
	$ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
	if (!self::$initialized) {
	  throw new Exception("Falta llamar a NotasAdmin::init()");
	}

	$alumnoDivision = Yii::app()->db->createCommand("
            select ad.id, ad.ciclo_id, n.conducta_detallada, coalesce(de.desempenio,\"\") as desempenio, n.id as nivel_id, ad.ciclo_id
                from alumno_division ad
                    inner join alumno a on a.id = ad.`Alumno_id` and a.activo = 1
						  inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_edu = 1
                    inner join division d on d.id = ad.division_id
                    inner join anio an on an.id = d.anio_id
                    inner join nivel n on n.id = an.nivel_id
                    left join desempenio de on de.alumno_division_id = ad.id
                where ad.id = $alumno_division_id and ad.ciclo_id = $ciclo_id and (ad.activo or ad.promocionado or ad.egresado)
                order by a.sexo desc, a.apellido, a.nombre
            ")->queryRow();
	//ve($alumnoDivision);
	$logica_id = self::$logica_id;

	$AmonestacionesFinal = 0;
	foreach (self::$items as $item) {
	  if ($item["tiene_conducta"] == 1) {
		$logica_periodo_id = $item["logica_periodo_id"];
		if ($alumnoDivision["conducta_detallada"] == "0") {
		  $amonestaciones = Helpers::qryScalar("
                                select c.conducta as sanciones
                                  from conducta c
                                  where c.Alumno_Division_id = $alumno_division_id and
                                        c.logica_periodo_id = $logica_periodo_id
                            ");
		  $totalAmonestaciones[$item["logica_item_nombre_unico"]] = $amonestaciones;
		  $AmonestacionesFinal = $amonestaciones;
		  //ve('bien', $AmonestacionesFinal);
		} else {
		  $amonestaciones = Helpers::qryScalar("
                                select sum(d.cantidad) as sanciones
                                  from conducta c
                                    inner join conducta_detalle d on d.conducta_id = c.id
                                  where c.Alumno_Division_id = $alumno_division_id and
                                        c.logica_periodo_id = $logica_periodo_id
                            ");
		  $totalAmonestaciones[$item["logica_item_nombre_unico"]] = $amonestaciones;
		  $AmonestacionesFinal += $amonestaciones;
//		  ve('mal', $amonestaciones);
		}
	  }
	}
	return array("amonestaciones" => $totalAmonestaciones, "amonestacionesFinal" => $AmonestacionesFinal, "desempenio" => $alumnoDivision["desempenio"]);
  }

  public static function getPeriodos() {
	if (!self::$initialized) {
	  throw new Exception("Falta llamar a NotasAdmin::init()");
	}
	$logica_id = self::$logica_id;
	foreach (Helpers::qryAll("
                            select lp.abrev, lp.nombre_unico, lp.nombre, lp.imprime_en_boletin
                                from logica_periodo lp
                                    where lp.Logica_id = $logica_id /* and lp.imprime_en_boletin = 1*/
                                    order by lp.orden
           ") as $periodo) {
	  $periodos[$periodo["nombre_unico"]] = $periodo;
	}
	return $periodos;
  }

  public static function getItems() {
	if (!self::$initialized) {
	  throw new Exception("Falta llamar a NotasAdmin::init()");
	}
	return self::$items;
  }

}

?>
