<?php

class Param extends CComponent {

    public $controller;
    public $onchange;
    public $showLabel = false;
    public $label;
    public $seleccionaAuto;
    public $prompt;

    public function __construct($options) {
        $this->controller = $options["controller"];
        $this->seleccionaAuto = (isset($options["seleccionaAuto"]) and $options["seleccionaAuto"]) ? $options["seleccionaAuto"] : true;
        $this->onchange = isset($options["onchange"]) ? $options["onchange"] : null;
    }

    public function render() {
        $this->style();
        $this->html();
        $this->js();
    }

    public function html() {
        echo "crear la function html";
    }

    public function js() {
        echo "crear la function js";
    }

    public function style() {
        ?>
        <style type="text/css">
            .param-label{vertical-align: super;}
            .chzn-container{vertical-align: text-bottom;}
        </style>
        <?php

    }

}
?>
