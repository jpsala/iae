<?php

class ParamAlumno extends Param {

    public $todos = false;
    public $activos = true;

    public function __construct($options) {
        parent::__construct($options);
        $this->todos = isset($options["todos"]) ? $options["todos"] : false;
        $this->activos = isset($options["activos"]) ? $options["activos"] : false;
        $this->label = "Alumno";
    }

    public function html() {
        $x = array('prompt' => '');
        ?>
        <style type="text/css">
            #alumno-select{width:250px}
        </style>
        <?php if ($this->showLabel): ?>
            <span class="param-label"><?php echo $this->label; ?></span>
        <?php endif; ?>    
        <select id="alumno-select" data-placeholder="Alumno" class="chzn-select">
            <?php
            if ($alumnos = $this->activos) {
                $alumnos = Alumno::model()->findAll(array("condition"=>"activo = 1", "order"=>"apellido , nombre"));
            } elseif ($alumnos = $this->todos) {
                $alumnos = Alumno::model()->findAll(array("order"=>"apellido , nombre"));
            } else {
                $alumnos = array();
            }
            $listData = CHtml::listData($alumnos, 'id', 'nombre');
            if ($this->todos) {
                $listData = "";
//                $alumnos = Alumno::model()->findAll("");
                foreach ($alumnos as $a) {
                    $listData[$a->id] = $a->nombreCompleto;
                }
            } else {
                $listData = CHtml::listData($alumnos, 'id', 'nombre');
            }
            echo CHtml::listOptions(null, $listData, $x)
            ?>

        </select>
        <?php
    }

    public function js() {
        ?>
        <script type="text/javascript">
            $("#alumno-select").chosen().change(function() {
                paramOnChangeAlumno($(this));
            });

            function paramOnChangeAlumno($this) {
                alumno_id = $this.val();
        <?php if ($this->onchange): ?>
            <?php echo $this->onchange . "(\$this);"; ?>
        <?php endif; ?>
                setTimeout(function() {
                    $("#periodo_select_chzn").mousedown();
                }, 200);
                return false;
            }

        </script>
        <?php
    }

}
?>
