<?php

class ParamAnio extends Param {

    public function __construct($options) {
        parent::__construct($options);
        $this->label = "Año";
    }

    public function html() {
        $x = array('prompt' => 'Año');
        ?>
        <style type="text/css">
            #anio-select{width:100px}
        </style>
        <?php if ($this->showLabel): ?>
            <span class="param-label"><?php echo $this->label; ?></span>
        <?php endif; ?>    
        <select id="anio-select" data-placeholder="Año" class="chzn-select">
            <?php
            echo CHtml::listOptions(null, CHtml::listData(Anio::model()->findAll("id=-1"), 'id', 'nombre'), $x)
            ?>
        </select>
        <?php
    }

    public function js() {
        ?>
        <script type="text/javascript">
            anio_id = -1;
            var seleccionaAuto = <?php echo $this->seleccionaAuto ? "true" : "false"; ?>;
            $("#anio-select").chosen().change(function() {
                paramOnChangeAnio($(this));
            });
            function paramOnChangeAnio($this) {
                anio_id = $this.val();
                division_id = -1;
                if ($("#division-select").length > 0) {
                    $.ajax({
                        type: "GET",
                        //dataType: "json",
                        data: {anio_id: anio_id},
                        url: "<?php echo $this->controller->createUrl("division/options"); ?>",
                        success: function(data) {
        <?php if ($this->onchange): ?>
            <?php echo $this->onchange . "(\$this,data);"; ?>
        <?php endif; ?>
                            //orientacion_id = data.Orientacion.id;
                            if ($("#division-select").length > 0) {
                                $("#division-select").html(data + "<option value=\"-1\">Nueva División</option>");
                                $("#division-select").trigger("liszt:updated");
                                if (seleccionaAuto) {
                                    $("#division_select_chzn").mousedown();
                                }
                            }
                        },
                        error: function(data, status) {
                        }
                    });
                }
                return false;
            }

        </script>
        <?php
    }

}
?>
