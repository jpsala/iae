<?php

class ParamAsignatura extends Param {

  public function __construct($options) {
    parent::__construct($options);
    $this->label = "Asignaturas";
  }

  public function html() {
    $x = array('prompt' => 'Asignatura');
    ?>
    <style type="text/css">
      #asignatura-select{width:240px}
    </style>
    <?php if ($this->showLabel): ?>
      <span class="param-label"><?php echo $this->label; ?></span>
    <?php endif; ?>    
    <select id="asignatura-select" data-placeholder="Asignatura" class="chzn-select">
      <?php
      echo CHtml::listOptions(null, CHtml::listData(Asignatura::model()->findAll("id=-1"), 'id', 'nombre'), $x)
      ?>
    </select>
    <?php
  }

  public function js() {
    ?>
    <script type="text/javascript">
      $("#asignatura-select").chosen().change(function() {
        paramOnChangeAsignatura($(this));
      });

      function paramOnChangeAsignatura($this) {
        asignatura_id = $this.val();
        $alumnoSelect = $("#alumno-select");
        $periodoSelect = $("#periodo-select");
        if ($periodoSelect.length > 0) {
          division_asignatura_id = $this.val();
          $.ajax({
            data: {division_asignatura_id: division_asignatura_id},
            type: "GET",
            url: "<?php echo $this->controller->createUrl("logicaPeriodo/options"); ?>",
            success: function(data) {
              $("#periodo-select").html(data);
              $("#periodo-select").trigger("liszt:updated");
              $("#div-para-notas").html("");
              //$("#periodo_select_chzn").mousedown();
              $("#periodo-select").change();
            },
            error: function(data, status) {
              division_asignatura_id = null;
            }
          });
        }
        if ($alumnoSelect.length > 0) {
          $.ajax({
            type: "GET",
            //dataType: "json",
            data: {asignatura_id: asignatura_id},
            url: "<?php echo $this->controller->createUrl("alumno/options"); ?>",
            success: function(data) {
              $alumnoSelect = $("#alumno-select");
              if ($alumnoSelect.length > 0) {
                $alumnoSelect.html(data);
                $alumnoSelect.trigger("liszt:updated");
                $("#alumno_select_chzn").mousedown();
              }
              $periodoSelect = $("#periodo-select");
              if ($periodoSelect.length > 0) {
                $periodoSelect.html(data);
                $periodoSelect.trigger("liszt:updated");
                $("#periodo_select_chzn").mousedown();
              }
            },
            error: function(data, status) {
            }
          });
          return false;
        }
    <?php if ($this->onchange): ?>
      <?php echo $this->onchange . "(\$this);"; ?>
    <?php endif; ?>
      }
    </script>
    <?php
  }

}
?>
