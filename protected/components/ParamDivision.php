<?php

class ParamDivision extends Param {

    public function __construct($options) {
        parent::__construct($options);
        $this->seleccionaAuto = isset($options["selecciona-auto"]) ? $options["selecciona-auto"] : true;
        $this->label = "División";
    }

    public function html() {
        $x = array('prompt' => 'División');
        ?>
        <style type="text/css">
            #division-select{width:120px}
        </style>
        <?php if ($this->showLabel): ?>
            <span class="param-label"><?php echo $this->label; ?></span>
        <?php endif; ?>    
        <select id="division-select" data-placeholder="División" class="chzn-select">
            <?php
            echo CHtml::listOptions(null, CHtml::listData(Division::model()->findAll("id=-1"), 'id', 'nombre'), $x)
            ?>
        </select>
        <?php
    }

    public function js() {
        ?>
        <script type="text/javascript">
            division_id = -1;
            var seleccionaAuto = <?php echo $this->seleccionaAuto ? "true" : "false"; ?>;
            $("#division-select").chosen().change(function() {
                paramOnChangeDivision($(this));
            });

            function paramOnChangeDivision($this) {
                division_id = $this.val();
                $periodoSelect = $("#periodo-select");
                $alumnoSelect = $("#alumno-select");
                $asignaturaSelect = $("#asignatura-select");
                $divisionSelect = $("#division-select");
                if ($asignaturaSelect.length > 0) {
                    $.ajax({
                        type: "GET",
                        //dataType: "json",
                        data: {division_id: $this.val()},
                        url: "<?php echo $this->controller->createUrl("asignatura/options"); ?>",
                        success: function(data) {
                            $("#asignatura-select").html(data);
                            $("#asignatura-select").trigger("liszt:updated");
                            if (seleccionaAuto) {
                                $("#asignatura_select_chzn").mousedown();
                            }
        <?php if ($this->onchange): ?>
            <?php echo $this->onchange . "(\$this);"; ?>
        <?php endif; ?>
                        },
                        error: function(data, status) {
                            division_asignatura_id = null;
                        }
                    });
                    return false;
                } else if ($alumnoSelect.length > 0) {
                    $.ajax({
                        type: "GET",
                        //dataType: "json",
                        data: {division_id: division_id},
                        url: "<?php echo $this->controller->createUrl("alumno/options"); ?>",
                        success: function(data) {
                            $alumnoSelect.html(data);
                            $alumnoSelect.trigger("liszt:updated");
                            $alumnoSelect.val("");
                            if (seleccionaAuto) {
                                $("#alumno_select_chzn").mousedown();
                            }
        <?php if ($this->onchange): ?>
            <?php echo $this->onchange . "(\$this,data);"; ?>
        <?php endif; ?>
                        },
                        error: function(data, status) {
                        }
                    });
                    return false;
                } else if ($periodoSelect.length > 0) {
                    setTimeout(function() {
                        $("#periodo_select_chzn").mousedown();
                    }, 100);
                    return false;
                } else {
        <?php if ($this->onchange): ?>
            <?php echo $this->onchange . "(\$this);"; ?>
        <?php endif; ?>
                }
            }
        </script>
        <?php
    }

}
?>
