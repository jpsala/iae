<?php

class ParamNivel extends Param {

    public $traePeriodosConConducta = false;
    public $urlPeriodos;

    public function __construct($options) {
        parent::__construct($options);
        $this->label = "Nivel";
        $this->traePeriodosConConducta = isset($options["trae_periodos_con_conducta"]) ?
                $options["trae_periodos_con_conducta"] :
                false;
        $this->urlPeriodos = $this->traePeriodosConConducta ?
                $this->controller->createUrl('logicaPeriodo/optionsConductaDesdeNivel') :
                $this->controller->createUrl('logicaPeriodo/optionsDesdeNivel');
    }

    public function html() {

        $x = array('prompt' => '');
        ?>
        <style type="text/css">
            #nivel-select{width:180px}
        </style>
        <?php if ($this->showLabel): ?>
            <span class="param-label"><?php echo $this->label; ?></span>
        <?php endif; ?>
        <select id="nivel-select" data-placeholder="Nivel" class="chzn-select">
            <?php
            echo CHtml::listOptions(null, CHtml::listData(Nivel::model()->findAll("activo = 1"), 'id', 'nombre'), $x)
            ?>
        </select>
        <?php
    }

    public function js() {
        ?>
        <script type="text/javascript">
            nivel_id = -1;
            var seleccionaAuto = <?php echo  $this->seleccionaAuto ? "true" : "false"; ?>;
            $("#nivel-select").chosen().change(function() {
                paramOnChangeNivel($(this));
            });
            if (seleccionaAuto) {
                $("#nivel_select_chzn").mousedown();
            }
            var urlPeriodos = "<?php echo $this->urlPeriodos; ?>";
            function paramOnChangeNivel($this) {
                nivel_id = $this.val();
                anio_id = -1;
                division_id = -1;
                $anioSelect = $("#anio-select");
                if ($anioSelect.length > 0) {
                    $.ajax({
                        type: "GET",
                        //dataType: "json",
                        data: {nivel_id: $this.val()},
                        url: "<?php echo $this->controller->createUrl("anio/options"); ?>",
                        success: function(data) {
                            $anioSelect = $("#anio-select");
                            if ($anioSelect.length > 0) {
                                $anioSelect.html(data);
                                $anioSelect.trigger("liszt:updated");
                                $("#anio_select_chzn").mousedown();
                            }
                        },
                        error: function(data, status) {
                        }
                    });
                }
                $divisionSelect = $("#division-select");
                if ($divisionSelect.length > 0) {
                    $.ajax({
                        type: "GET",
                        //dataType: "json",
                        data: {anio_id: -1},
                        url: "<?php echo $this->controller->createUrl("division/options"); ?>",
                        success: function(data) {
                            $divisionSelect = $("#division-select");
                            if ($divisionSelect.length > 0) {
                                $divisionSelect.html(data);
                                $divisionSelect.trigger("liszt:updated");
                            }
                        },
                        error: function(data, status) {
                        }
                    });
                }

                $periodoSelect = $("#periodo-select");
                if ($periodoSelect.length > 0) {
                    $.ajax({
                        type: "GET",
                        url: urlPeriodos + "&nivel_id=" + nivel_id,
                        success: function(data) {
                            $("#periodo-select").html(data);
                            $("#periodo-select").trigger("liszt:updated");
                        },
                        error: function(data, status) {
                        }
                    });

                }
        <?php if ($this->onchange): ?>
            <?php echo $this->onchange . "(\$this);"; ?>
        <?php endif; ?>

                return false;
            }

        </script>
        <?php
    }

}
?>
