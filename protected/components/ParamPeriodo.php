<?php

class ParamPeriodo extends Param {

  public function __construct($options) {
    parent::__construct($options);
    $this->label = "División";
  }

  public function html() {
    $x = array('prompt' => 'Período');
    ?>
    <style type="text/css">
      #periodo-select{width:150px}
    </style>
    <?php if ($this->showLabel): ?>
      <span class="param-label"><?php echo $this->label; ?></span>
    <?php endif; ?>    
    <select id="periodo-select" data-placeholder="Período" class="chzn-select">
      <?php
      echo CHtml::listOptions(null, CHtml::listData(LogicaPeriodo::model()->findAll("id=-1"), 'id', 'nombre'), $x)
      ?>
    </select>
    <?php
  }

  public function js() {
    ?>
    <script type="text/javascript">
      
      $("#periodo-select").chosen().change(function() {
        paramOnChangePeriodo($(this));
      });

      function paramOnChangePeriodo($this) {
        logica_periodo_id = $this.val();
        $alumnoSelect = $("#alumno-select");
        if ($alumnoSelect.length > 0) {
          $.ajax({
            type: "GET",
            //dataType: "json",
            data: {periodo_id: periodo_id},
            url: "<?php echo $this->controller->createUrl("alumno/options"); ?>",
            success: function(data) {
              $alumnoSelect = $("#alumno-select");
              if ($alumnoSelect.length > 0) {
                $alumnoSelect.html(data);
                $alumnoSelect.trigger("liszt:updated");
                $("#alumno_select_chzn").mousedown();
              }
              
            },
            error: function(data, status) {
            }
          });
          return false;
        }
    <?php if ($this->onchange): ?>
      <?php echo $this->onchange . "(\$this);"; ?>
    <?php endif; ?>
      }
    </script>
    <?php
  }

}
?>
