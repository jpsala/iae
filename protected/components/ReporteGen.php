<?php

class ReporteGen extends CComponent {

  public $params, $query, $nombre, $reporte, $controller, $fields = array(), $html = "", $salida;

  public function __construct($controller = null, $output = "pdf") {
    $this->controller = $controller;
    $this->nombre = $_REQUEST["reporte"];
    $this->reporte = Reporte::model()->find("nombre = \"$this->nombre\"");
    if (!$this->reporte) {
      throw new Exception("Reporte $this->nombre no fué encontrado");
    }
    if (!isset($_REQUEST["param_submit"])) {
      $params = array();
      foreach ($this->reporte->reporteParams as $param) {
        $params[$param->nombre] = array(
            "tipo" => $param->tipo,
            "label" => $param->label,
            "id" => $param->nombre,
            "default" => $param->default,
        );
      }
      $this->params = new Params(array(
                  "items" => $params,
                  "action" => $this->controller->createUrl("/reporte/index", array("reporte" => $this->nombre, "output" => $output)),
//                "default" => "nombre",
//                "style" => "label{width:120px !important}",
                      )
      );
    }
  }

  public function render() {
    if (!isset($_REQUEST["param_submit"])) {
      $this->renderParams();
    } else {
      $db = Yii::app()->db;
      $cmd = $db->createCommand($this->reporte->query);
      $cmd->prepare();
      foreach ($_REQUEST["params"] as $nombre => $value) {
        $cmd->bindValue($nombre, $value);
      }
      $this->renderReport($cmd, $_REQUEST["output"]);
    }
  }

  public function renderParams() {
    $this->controller->render("params", array("html" => $this->params->htmlFull(), "reporte" => $this->nombre));
  }

  public function renderReport($cmd, $output) {
    if ($output == "pdf") {
      $pdf = new Pdf();
      $pdf->domPdf->set_paper("a4","portrait");
      $html = $this->controller->renderPartial("pdf", array(
          "cmd" => $cmd,
          "reporte_id" => $this->reporte->id,
          /*"pdf" => $pdf->domPdf*/), true
      );
      $pdf->render($html);
    } else {
      $this->controller->renderPartial("pdf", array(
          "cmd" => $cmd,
          "reporte_id" => $this->reporte->id)
      );
    }
  }

}

?>
