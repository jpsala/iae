<?php

	class WebUser extends CWebUser {

		private $_model;

		public function getModel() {
			if ($this->isGuest) {
				return new User();
			} else {
				if (!isset($this->_model)) {
					$this->_model = User::model()->findByPk($this->id);
				}
				return $this->_model;
			}
		}

		public function getKeepSession() {
			return  Helpers::qryScalar("select keep_session from  user where id = $this->id");
		}

		public function hasCookie($name) {
			return !empty(Yii::app()->request->cookies[$name]->value);
		}

		public function getCookie($name) {
			return Yii::app()->request->cookies[$name]->value;
		}

		public function setCookie($name, $value) {
			$cookie = new CHttpCookie($name, $value);
			$cookie->expire = time() + 60 * 60 * 24 * 180;
			Yii::app()->request->cookies[$name] = $cookie;
		}

		public function removeCookie($name) {
			unset(Yii::app()->request->cookies[$name]);
		}

	}
	