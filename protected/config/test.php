<?php

return CMap::mergeArray(
                require(dirname(__FILE__) . '/main.php'), array(
            'components' => array(
                'fixture' => array(
                    'class' => 'system.test.CDbFixtureManager',
                ),
                'db' => array(
                    'class' => 'CDbConnection',
                    'connectionString' => 'mysql:host=localhost;dbname=iae-nuevo-dev',
                    'charset' => 'UTF8',
                    //'initSQLs'=>array("SET session time_zone = '+0:00';"),
                    'username' => 'root',
                    'password' => 'root',
                    'enableParamLogging' => true,
                ),
            ),
                )
);
