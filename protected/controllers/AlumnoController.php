<?php

class AlumnoController extends Controller
{
	// borrar desde acá
	public function actionFixAlumnosGraba()
	{
		$alumnoDivisiones = $_POST['divisiones'];
		foreach ($alumnoDivisiones as $key => $alumno_division_id) {
			$s =
				/** @lang text */
				"select concat(a.apellido,', ',a.nombre) as alumno, n.nombre as nivel, an.nombre as anio, d.nombre as division
					from alumno_division ad
 						inner join division d on d.id = ad.division_id
 						inner join anio an on an.id = d.anio_id
 						inner join nivel n on n.id = an.nivel_id
 						inner join alumno a on a.id = ad.alumno_id
					where ad.id = $alumno_division_id";
			$q = Helpers::qry($s);
			Helpers::qryExec("
				delete from alumno_division where id = $alumno_division_id
			");
			echo '<div>Borrando: ' . $q['alumno'] . ' -> ' . $q['nivel'] . ' -> ' . $q['anio'] . ' -> ' . $q['division'] . '</div>';
		}
	}

	public function actionFixAlumnos()
	{
		$this->layout = "nuevo";
		$ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
		$niveles = 'SELECT * FROM nivel n WHERE n.activo /*AND n.id <> 6*/ ORDER BY n.orden ';
		$niveles = Helpers::qryAll($niveles);
		foreach ($niveles as $nivel_key => $nivel) {
			$nivel_id = $nivel['id'];
			$anios = /** @lang text */
				"SELECT id, nombre, orden
						FROM anio a
						WHERE a.nivel_id = $nivel_id and  a.activo ORDER BY a.orden
				";
			$anios = Helpers::qryAll($anios);
			foreach ($anios as $key => $anio) {
				$anio_id = $anio['id'];
				$alumnos = [];
				$alumnosConDivRepetidasSelect = /** @lang text */
					"SELECT a.id
							FROM alumno a
								WHERE  (SELECT COUNT(*)
													FROM alumno_division ad
														 INNER JOIN division d on d.id = ad.division_id
														 INNER JOIN anio an on an.id = d.anio_id
														 INNER JOIN alumno a2 ON ad.Alumno_id = a2.id
														 INNER JOIN alumno_estado ae ON a2.estado_id = ae.id
														 INNER JOIN alumno_division_estado ade ON ad.alumno_division_estado_id = ade.id
												 	WHERE (SELECT COUNT(*) FROM nota n WHERE n.Alumno_Division_id = ad.id) = 0
																 AND ae.activo_edu and an.id = $anio_id
																 AND ad.alumno_id = a.id AND ade.muestra_edu
																 AND ! ad.borrado
																 AND ad.ciclo_id = $ciclo_id
												) > 1
							ORDER BY a.apellido, a.nombre";
				$alumnosConDivRepetidas = Helpers::qryAll($alumnosConDivRepetidasSelect);
				foreach ($alumnosConDivRepetidas as $alumno) {
					$alumno_id = $alumno['id'];
					$divisionesRepetidasSelect = /** @lang SQL */
						"SELECT distinct GROUP_CONCAT(ad.id) as divisiones FROM alumno_division ad
                         INNER JOIN alumno a2 ON ad.Alumno_id = a2.id
                         INNER JOIN alumno_estado ae ON a2.estado_id = ae.id
                         INNER JOIN alumno_division_estado ade ON ad.alumno_division_estado_id = ade.id
                         INNER JOIN division d ON ad.Division_id = d.id
                         INNER JOIN anio a1 ON d.Anio_id = a1.id
                         INNER JOIN ciclo c ON ad.Ciclo_id = c.id
                         INNER JOIN nivel n ON a1.Nivel_id = n.id
                     WHERE (SELECT COUNT(*) FROM nota n WHERE n.Alumno_Division_id = ad.id) = 0
                             AND ae.activo_edu
                             AND ad.alumno_id = $alumno_id
                             AND a1.id = $anio_id
                             AND ad.ciclo_id = $ciclo_id
                             AND ade.muestra_edu
					";
					$divisionesRepetidas = Helpers::qryScalar($divisionesRepetidasSelect);
					$datosDivisionesSelect = /** @lang SQL */
						"SELECT a.id, a.nombre, a.apellido, a.activo, a.egresado, a.ingresante,
										ae.nombre as estado
							FROM alumno a
								inner join alumno_estado ae on ae.id = a.estado_id
              WHERE a.id = " . $alumno_id;
					$datosDivisiones = Helpers::qry($datosDivisionesSelect);
					$alumnoData = [
						'nombre' => $datosDivisiones['nombre'] . ' ' . $datosDivisiones['apellido'],
						'ids' => $divisionesRepetidas,
						'otros' => 'activo:'.$datosDivisiones['activo'].'/'.
											 'estado:'.$datosDivisiones['estado'].'/'
					];

					$alumnoDivisionAllSelect = /** @lang SQL */
						"SELECT ad.id, d.nombre AS division,
										ae.nombre AS estado
						FROM alumno a
								INNER JOIN alumno_division ad ON a.id = ad.Alumno_id
								INNER JOIN alumno_division_estado ade on ade.id = ad.alumno_division_estado_id
								INNER JOIN alumno_estado ae ON a.estado_id = ae.id
								INNER JOIN division d ON ad.Division_id = d.id
								INNER JOIN anio a1 ON d.Anio_id = a1.id
								INNER JOIN ciclo c ON ad.Ciclo_id = c.id
								INNER JOIN nivel n ON a1.Nivel_id = n.id
						WHERE ad.id IN (" . $alumnoData['ids'] . ")
									AND ad.ciclo_id = $ciclo_id
									AND ae.activo_edu and ade.muestra_edu
						ORDER BY n.orden, a.apellido, a.nombre";
					unset($alumnoData['ids']);
					$alumnoDivisionAll = Helpers::qryAll($alumnoDivisionAllSelect);
					if (count($alumnoDivisionAll) > 0) {
						$alumnoData['divisiones'] = $alumnoDivisionAll;
					}
					$alumnos[] = $alumnoData;
				}
				if (count($alumnos) > 0) {
					$anios[$key]['alumnos'] = $alumnos;
				} else {
					unset($anios[$key]);
				}
			}
			$niveles[$nivel_key]['anios'] = $anios;
		}
		$this->render('fix_divisiones', ['niveles' => $niveles]);
	}

	// borrar hasta acá

	public $notjstree;

	public function filters()
	{
		return array(
//            'accessControl', // perform access control for CRUD operations
			//'rights',
		);
	}

	public function accessRules()
	{
		return array(
			array('allow', // all users
				'actions' => array("olvidePassword", 'familiaGridAjax', "autocomplete", "adminForm", "adminGraba", "test"),
				'users' => array('*'),
			),
			array('allow', // authenticated user
				'actions' => array(""),
				'users' => array('@'),
			),
			array('allow', // roles
				'actions' => array("admin"),
				'roles' => array('Alumno.*'),
			),
			array('deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	public function actions()
	{
		return array();
	}

	public function actionArreglaAlumnosDivisiones()
	{
		$select = "UPDATE alumno_division ad SET ad.borrado = 1 WHERE ad.id IN
        (
          SELECT id FROM
          (
            SELECT ad.id
              FROM alumno a
                         INNER JOIN alumno_division ad ON a.id = ad.Alumno_id
                         INNER JOIN alumno_estado ae ON a.estado_id = ae.id
                         INNER JOIN division d ON ad.Division_id = d.id
                         INNER JOIN anio a1 ON d.Anio_id = a1.id
                         INNER JOIN ciclo c ON ad.Ciclo_id = c.id
                         INNER JOIN nivel n ON a1.Nivel_id = n.id
                     WHERE (SELECT COUNT(*) FROM nota n WHERE n.Alumno_Division_id = ad.id) = 0
                             AND (SELECT COUNT(*) FROM alumno_division ad1 INNER JOIN division d1 ON ad1.Division_id = d1.id INNER JOIN anio a2 ON d1.Anio_id = a2.id WHERE ad1.Alumno_id = a.id AND a2.id = a1.id AND ad1.Ciclo_id = ad.Ciclo_id) > 1
                             AND a1.nivel_id != 1 AND (ad.activo = 0 AND ae.activo_edu)
                     ORDER BY n.nombre, a1.nombre, a.apellido, a.nombre
                ) AS arbitraryTableName
        )
        ";
		echo 'arreglados: ' . Helpers::qryExec($select);
	}

	public function actionAdmin()
	{
		$alumno = new Alumno('search');
		$alumno->matricula = Alumno::getSiguienteMatricula();
		$this->render('admin', array(
			'alumno' => $alumno,
		));
	}
	public function actionAdminTraeMatriculaIngresante(){
		$ret = new stdClass();
		$ret->matricula = Alumno::getSiguienteMatriculaIngresante();
		// if($ret->matricula < 90000){
		// 	$ret->matricula+=90000;
		// }
		echo json_encode($ret);
	}
	public function actionAdminGraba()
	{
		//var_dump($_POST['fecha_nacimiento']);die;
		//setlocale(LC_ALL, "sp");
		//date_default_timezone_set('America/Argentina/Buenos_Aires');
		$ret = new stdClass();
		$familiaObs = isset($_POST["obs_familia"]) ? $_POST["obs_familia"] : "";
		$nuevo = false;
		$alumno = Alumno::model()->findByPk($_POST["alumno_id"]);
		if (!$alumno) {
			$alumno = new Alumno();
			$nuevo = true;
		} else {
			if ($alumno->Familia_id <> $_POST["Familia_id"]) {
				$alumno->Familia_id = $_POST["Familia_id"];
			}
		}

		$alumno->attributes = $_POST;
//    $alumno->calle = ucwords(strtolower($_POST['calle']));
//    $alumno->numero = $_POST['numero'];
//    $alumno->piso = $_POST['piso'];
//    $alumno->departamento = strtoupper($_POST['departamento']);
		$alumno->vive_con_id = $_POST['vive_con_id'];
		$alumno->fecha_nacimiento = Helpers::date("Y/m/d", $_POST['fecha_nacimiento']);
		$alumno->fecha_ingreso = Helpers::date("Y/m/d", $_POST['fecha_ingreso']);
		$alumno->fecha_ingreso_estab = ($_POST['fecha_ingreso_estab'] !== "") ? Helpers::date("Y/m/d", $_POST['fecha_ingreso_estab']) : NULL;
		$alumno->fecha_egreso = ($_POST['fecha_egreso'] !== "") ? Helpers::date("Y/m/d", $_POST['fecha_egreso']) : NULL;
		$alumno->Tipo_Documento_id = $_POST['Documento_Tipo_id'];
		$alumno->numero_documento = $_POST['numero_documento'];
		$alumno->localidad_id = $_POST['Localidad'];

		$alumno->obra_social = $_POST['obra_social'];
		$alumno->numero_afiliado = $_POST['numero_afiliado'];
		$alumno->grupo_sanguineo = $_POST['grupo_sanguineo'];

		$alumno->telefono_1 = $_POST['telefono_1'];
		$alumno->telefono_2 = $_POST['telefono_2'];
		$alumno->email = $_POST['email'];
		$alumno->sexo = $_POST['sexo'];
		$alumno->obs_beca = $_POST['obs_beca'];
		$alumno->nacionalidad_opcion = $_POST['nacionalidad-select'];

//     $alumno->porc_beca = $_POST['porc_beca'];
		//$alumno->activo = isset($_POST['activo']) ? 1 : 0;
		//$alumno->ingresante = isset($_POST['ingresante']) ? 1 : 0;
		$alumno->setEstado($_POST["estado_id"]);
		$ad = $alumno->alumnoDivisionActiva;
		$tr = Yii::app()->db->beginTransaction();
		if (!isset($_POST["Familia_id"])) {
			$f = new Familia();
			$f->save();
			$alumno->Familia_id = $f->id;
			$old_obs = null;
		} else {
			$f = Familia::model()->findByPk($alumno->Familia_id);
			$old_obs = $f->obs;
		}
		$f->obs = strtoupper($_POST['obs_cobranza']) !== "NULL" ? $_POST['obs_cobranza'] : "";
		if (!$f->save()) {
			$ret->errors = $f->errors;
			echo json_encode($ret);
			$tr->rollback();
			die;
		}
		if ($f->obs !== $old_obs) {
			$log = new Log();
			$log->anterior = $old_obs;
			$log->nuevo = $f->obs;
			$log->clave = "familia-obs";
			$log->texto = "Cambio de observaciones";
			if (!$log->save()) {
				vd($log->errors);
			}
			//	$texto = json_encode(array("obs_ant" => $old_obs, "user_id"));
		}
		if (!$alumno->save()) {
			$ret->errors = $alumno->errors;
			echo json_encode($ret);
			$tr->rollback();
			die;
		}
		//$alumno->familia->obs = $familiaObs;
		//$alumno->familia->save();
		$ret->alumno_id = $alumno->id;
		$ret->matricula = $alumno->matricula;
		$ret->nuevo = $nuevo;
		$ret->familia_id = $alumno->Familia_id;
		//da de alta el socio
		if ($nuevo) {
			$as = new Socio();
			$as->tipo = 'A';
			$as->Alumno_id = $alumno->id;
			$as->save();
		}
		if ($ad and $ad->Division_id <> $_POST["division_id"] and $_POST["division_id"]) {
			$alumno->asignaDivision(Division::model()->findByPk($_POST["division_id"]));
		} elseif (!$ad) {
			$ad = new AlumnoDivision();
			$ad->Alumno_id = $alumno->id;
			$ad->Ciclo_id = Ciclo::getActivo()->id;
			$ad->Division_id = $_POST["division_id"];
			$ad->activo = 1;
			if (!$ad->save()) {
				$ret->errors = $ad->errors;
				echo json_encode($ret);
				$tr->rollback();
				die;
			}
		}
		$tr->commit();
		echo json_encode($ret);
	}

	public function actionAdminForm($alumno_id)
	{
		$alumno = Alumno::model()->findByPk($alumno_id);
		if (!$alumno) {
			$alumno = new Alumno();
			$alumno->matricula = Alumno::getSiguienteMatricula();
			$alumnoDivision = null;
		} else {
			$alumnoDivision = $alumno->alumnoDivisionActiva;
		}
		$this->renderPartial("_adminForm", array("alumno" => $alumno, "alumnoDivision" => $alumnoDivision));
	}

	public function actionAutoComplete($activo = 1)
	{
		$term = $_GET['term'];
    if (substr($term, 0, 1) == "#") {
      $dni = substr($term, 1);
      // $socio = Helpers::qryScalar('select')
    }

		$criteria = new CDbCriteria();
		$criteria->limit = 40;
    if (isset($dni)) {
      if(strlen($dni) < 4){
        $dni = 99999999999999;
      }
      $criteria->addCondition("numero_documento like  \"%$dni%\"");
    }elseif (is_numeric($term)) {
			$criteria->addCondition("matricula = \"$term\"");
		} else {
			$nombres = explode(",", $term);
			$apellido = trim($nombres[0]);
			$criteria->addCondition("apellido like \"%$apellido%\"");
			if (count($nombres) > 1) {
				$nombre = trim($nombres[1]);
				$criteria->addCondition("nombre like \"%$nombre%\"");
			}
		}
		if ($activo) {
			$criteria->addCondition("activo = $activo");
		}
		$alumnos = Alumno::model()->findAll($criteria);
    $ret = array();

		foreach ($alumnos as $alumno) {
      $labelDni = isset($dni) ? ' / dni:'.$alumno->numero_documento:'';
			$ret[] = array(
				"id" => $alumno->id,
				'label' => "(" . $alumno->matricula . ") " . $alumno->nombreCompleto . $labelDni,
				'value' => $alumno->nombreCompleto);
		}
		echo json_encode($ret);
	}

	public function actionFamiliaGridAjax()
	{
		$responce = new stdClass();
		$familia_id = $_GET["familia_id"];
		$alumno_id = $_GET["alumno_id"];
		if (!$familia_id) return;
		$page = $_GET['page'];
		$limit = $_GET['rows'];
		$sord = $_GET['sord'];
		$sidx = $_GET['sidx'];
		if (!$sidx) $sidx = 1;
		$row = Yii::app()->db->createCommand("
            select (
                (select count(*)-1 from alumno a where a.Familia_id=$familia_id)+
                (select count(*)-1 from pariente p inner join familia f on f.id = p.Familia_id inner join pariente_tipo pt on pt.id = p.Pariente_tipo_id where p.Familia_id=$familia_id)
             ) as count"
		)->queryRow();
		$count = $row['count'];
		if ($count > 0) {
			$total_pages = ceil($count / $limit);
		} else {
			$total_pages = 0;
		}
		if ($page > $total_pages) $page = $total_pages;

		$start = $limit * $page - $limit;

		// do not put $limit*($page - 1) $SQL = "SELECT a.id, a.invdate, b.name, a.amount,a.tax,a.total,a.note FROM invheader a, clients b WHERE a.client_id=b.client_id ORDER BY $sidx $sord LIMIT $start , $limit";
		$sql
			= <<<EOD
            select concat("par_",p.id) as id, "par" as tipo, concat(p.apellido,", ",p.nombre) as nombre, pt.nombre as relacion
                from pariente p
                    inner join familia f on f.id = p.Familia_id
                    inner join pariente_tipo pt on pt.id = p.Pariente_tipo_id
                where p.Familia_id=$familia_id
            UNION
            select concat("alu_",a.id) as id, "alu" as tipo, concat(a.apellido,", ",a.nombre) as nombre, "hermano" as relacion
                from alumno a
                where a.Familia_id=$familia_id and a.id <> $alumno_id
            order by nombre
EOD;
		$rows = Yii::app()->db->createCommand($sql)->queryAll();
		$responce->page = $page;
		$responce->total = $total_pages;
		$responce->records = $count;
		$i = 0;
		foreach ($rows as $row) {
			$responce->rows[$i]['id'] = $row["id"];
			$responce->rows[$i]['cell'] = array($row["nombre"], $row["relacion"]);
			$i++;
		}
		echo json_encode($responce);
	}

	public function actionAdminTraePariente($pariente_id)
	{
		$p = Pariente::model()->findByPk($pariente_id);
		if (!$p) {
			$p = new Pariente();
		}
		$this->renderPartial("_adminParienteForm", array("pariente" => $p));
	}

	public function actionAdminParienteGraba()
	{
		$ret = new stdClass();
		$p = Pariente::model()->findByPk($_POST["Pariente_id"]);
		if (!$p) {
			$p = new Pariente();
		}
		$p->nombre = $_POST['Nombre_pariente'];
		$p->apellido = $_POST['Apellido_pariente'];
		$p->calle = $_POST['calle_pariente'];
		$p->departamento = $_POST['departamento_pariente'];
		$p->piso = $_POST['piso_pariente'];
		$p->numero = $_POST['numero_pariente'];
		$p->numero_documento = $_POST['numero_documento_pariente'];
		$p->fecha_nacimiento = Helpers::date("Y/m/d", $_POST['fecha_nacimiento_par']);
		$p->tipo_documento_id = $_POST['Documento_Tipo_id_pariente'];
		$p->email = $_POST['email1'];
		$p->email1 = $_POST['email2'];
		$p->lugar_nacimiento = $_POST['localidad-pariente'];
		// $p->lugar_nacimiento = $_POST['pais-pariente-select'];
		$p->localidad_id1 = $_POST['localidad-pariente'];
		if (isset($_POST['vive'])) $p->vive = 1;
		else $p->vive = 0;

		$p->Familia_id = Alumno::model()->findByPk($_POST["alumno_id"])->Familia_id;
		$p->attributes = $_POST;
		$p->save();
		if (!$p->save()) {
			$ret->errors = $p->errors;
			echo json_encode($ret);
			die;
		}
		$ret->Familia_id = $p->Familia_id;
		echo json_encode($ret);
	}

	public function actionAdminParienteElimina($pariente_id)
	{
		$p = Pariente::model()->findByPk($pariente_id);
		$p->delete();
	}

	public function actionObservaciones()
	{
		/* @var $user User */
		$this->comentarios = false;
		$this->render("observaciones");
	}

	public function actionTraeObservaciones($socio_id, $observaciones_tipo, $soloPropias)
	{
		$whereTipo = $observaciones_tipo != "todas" ? " observaciones_tipo_id = $observaciones_tipo" : " true ";
		$wherePublicas = $soloPropias == "checked" ? " false " : " publica=1 ";
		$user = Yii::app()->user->model;
		$socio = Socio::model()->findByPk($socio_id);
		$alumno = Alumno::model()->findByPk($socio->Alumno_id);
		$select
			= "
      ((user_id = $user->id or $wherePublicas) and Alumno_id = $socio->Alumno_id)
        and $whereTipo";
		$observaciones = ObservacionesAlumno::model()->findAll(array("condition" => "$select", "order" => "fecha asc")
		);
		$obsNueva = new ObservacionesAlumno();
		$obsNueva->Alumno_id = $socio->Alumno_id;
		$obsNueva->user_id = $user->id;
		$obsNueva->fecha = date("Y-m-d H:i:s");
		//var_dump($obsNueva->fecha);die;
		$this->renderPartial("_observaciones", array("observaciones" => $observaciones, "obsNueva" => $obsNueva, "matricula" => $alumno->matricula));
	}

	public function actionGrabaObservacion()
	{
		$texto = html_entity_decode($_POST["texto"]);
		$observacion_id = $_POST["observacion_id"];
		$alumno_id = $_POST["alumno_id"];
		$alumno = Alumno::model()->findByPk($alumno_id);
		$observaciones_tipo_id = $_POST["observaciones_tipo_id"];
		if ($observacion_id != "nuevo") {
			$c = ObservacionesAlumno::model()->find("id =$observacion_id");
		} else {
			$c = new ObservacionesAlumno();
			$c->user_id = Yii::app()->user->id;
			$c->Alumno_id = $alumno_id;
			$c->Ciclo_id = Ciclo::getActivo()->id;
			$c->fecha = date("Y/m/d");
			$c->publica = $_POST["publica"] == "true" ? 1 : 0;
		}
		$c->texto = $texto;
		$c->observaciones_tipo_id = $observaciones_tipo_id;
		if (!$c->save()) {
			json_encode(var_dump($c->errors));
		} elseif ($observacion_id == "nuevo") {
			$obsNueva = new ObservacionesAlumno();
			$obsNueva->Alumno_id = $alumno_id;
			$obsNueva->user_id = Yii::app()->user->id;
			$obsNueva->fecha = date("Y/m/d");
			$ret->fecha = "Creado el " . date("d/m/Y \a \l\a\s H:i", time());
			$ret->nuevo = $this->renderPartial("_observaciones", array(
				"observaciones" => array(),
				"obsNueva" => $obsNueva,
				"matricula" => $alumno->matricula
			), true
			);
			$ret->id = $c->id;
			echo json_encode($ret);
		}
	}

	public function actionTestFecha()
	{
		$c = new ObservacionesAlumno();
		$c->user_id = Yii::app()->user->id;
		$c->Alumno_id = 25865;
		$c->Ciclo_id = Ciclo::getActivo()->id;
		$value = date("d/m/Y H:i:s");
		//$value = date("d/m/Y");
		var_dump($value);
//    $c->fecha = Helpers::dateFormat($value);
		$c->fecha = date("Y/m/d", mystrtotime(str_replace("/", ".", $value)));

		//$c->fecha = date("Y/m/d H:i:s");
		$c->texto = date("d/m/Y H:i:s", mystrtotime($c->fecha));
		$c->observaciones_tipo_id = 1;
		$c->save();
		var_dump($c->fecha);
	}

	public function actionEliminaObservacion()
	{
		$observacion_id = $_POST["observacion_id"];
		$o = ObservacionesAlumno::model()->findByPk($observacion_id);
		$o->delete();
	}

	public function actionDatosAlumnoDialog($matricula)
	{
		$alumno = Alumno::model()->find("matricula = $matricula");
		$this->renderPartial("_datosAlumno", array("alumno" => $alumno));
	}

	public function actionOptions($division_id)
	{
		$ciclo_id = Ciclo::getActivo()->id;
		$ads = AlumnoDivision::model()->with("alumno")->findAll(array("condition" => "Division_id = $division_id and alumno.activo=1 and t.activo=1 and Ciclo_id=$ciclo_id", "order" => "apellido,nombre"));
		$listData = array();
		foreach ($ads as $ad) {
			$listData[$ad->alumno->id] = $ad->alumno->nombreCompleto;
		}
		$x = array();
		echo CHtml::listOptions(null, $listData, $x);
	}

	public function actionBecas($ajax = false, $filtro = null, $solo_con_beca = "true")
	{
		if (!Yii::app()->user->checkAccess('Carga de Becas')) {
			Helpers::error("Error de accesos", "No tiene acceso a la carga/modificación de becas");
			die;
		}
		$ciclo_id = Ciclo::getCicloIdParaInformesAdmin();
		$this->notjstree = true;
		$whereAlumno = $filtro ? " and ( a.apellido like :alumno or
            a.nombre like :alumno or a.obs_beca like :alumno) " : "";
		$whereSoloBeca = $solo_con_beca == "true" ? " and (a.beca is not null and a.beca > 0) " : "";
		$select
			= "
				select a.id, concat(a.apellido, ', ', a.nombre) as alumno, a.matricula, a.beca, a.obs_beca,
					concat( nivel.nombre,' ', anio.nombre, ' ', d.nombre) as asignacion, concat(p.apellido, ', ', p.nombre) as pariente,
					concat(p.calle, ' ', COALESCE(p.numero, ''), ' ', COALESCE(p.departamento, '')) as pariente_domicilio,
					art.precio_neto as cuota, saldo(s.id,null) as saldo, a.familia_id as familia_id
				from alumno a
					inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_admin = 1
					inner join alumno_division ad on ad.Alumno_id = a.id and ad.activo /*((ad.activo or ad.promocionado or ad.egresado)*/
					inner join socio s on s.alumno_id = a.id
					inner join division d on d.id = ad.Division_id
					inner join anio on anio.id = d.Anio_id
					inner join nivel on nivel.id = anio.Nivel_id
					inner join familia f on f.id = a.Familia_id
					left join pariente p on p.id = a.vive_con_id
					left join articulo art on art.id = case when d.articulo_id is not null then d.articulo_id else anio.articulo_id end
				where (a.activo = 1 or a.ingresante = 1) $whereAlumno $whereSoloBeca
							and ad.Ciclo_id = $ciclo_id
				order by a.apellido, a.nombre";
		//vd($select);
		$data = Helpers::qryAll($select, array("alumno" => "%$filtro%"));
		$render = $ajax ? "renderPartial" : "render";
		$this->$render("becas", array("data" => $data,
				"solo_con_beca" => $solo_con_beca,
				"filtro" => $filtro)
		);
	}

	public function actionBecasGraba()
	{
		$a = Alumno::model()->findByPk($_POST["alumno_id"]);
		$a->beca = $_POST["beca"];
		$a->obs_beca = $_POST["obs_beca"];
		$a->save();
	}

	public function actionAdmin2($socio_id = -1)
	{
		$this->render("admin2", array("div" => $this->actionAdmin2Ajax($socio_id, true)));
	}

	public function actionAdmin2Ajax($socio_id, $return = false)
	{
		$s
			= "
            select a.id, a.apellido, a.nombre, 0 as alumnofset, f.obs, 0 as obsfs,
                        0 as visafs, a.visa_numero, a.visa_activo, d.cbu as debito_cbu, d.activo as debito_activo, d.id as debito_id, 0 as debitofs,
                        visa_titular, a.estado_id
                        /*, a.fecha_nacimiento*/
            from alumno a
					inner join familia f on f.id = a.familia_id
                        inner join socio on socio.alumno_id = a.id and socio.id = $socio_id
                        left join alumno_debito d on d.alumno_id = a.id";
		$alumno = Helpers::qryOrEmpty($s);
		$listDataEstados = CHtml::listData(AlumnoEstado::model()->findAll(), "id", "nombre");
		$cols = array(
			"id" => array("tipo" => "hidden"),
			"debito_id" => array("tipo" => "hidden"),
			"nombre" => array("label" => "", "placeholder" => "Nombre", "style" => "width:35%"),
			"apellido" => array("label" => "", "placeholder" => "Apellido", "style" => "width:35%"),
			"obs" => array("tipo" => "textarea", "label" => "Observaciones Cobranza"),
			"debito_activo" => array("tipo" => "checkbox", "label" => "Activo", "style" => "width:10px"),
			"visa_activo" => array("tipo" => "checkbox", "label" => "Activo", "style" => "width:10px"),
			"visa_numero" => array("label" => "NÃºmero", "style" => "width:143px"),
			"visa_titular" => array("label" => "Titular"),
			"estado_id" => array("tipo" => "dropDown", "data" => $listDataEstados, "label" => "Estado", "placeholder" => "Nombre"),
			//"fecha_nacimiento" => array("label" => "Fecha de nacimiento", "date" => true, "hid"),
			"debito_cbu" => array("label" => "CBU"),
		);
		$fieldSets = array(
			"alumnofset" => array("legend" => "Nombres", "campos" => array("nombre", "apellido", "estado_id")),
			"visafs" => array("legend" => "Visa", "campos" => array("visa_numero", "visa_activo", "visa_titular")),
			"obsfs" => array("legend" => "", "campos" => array("obs")),
			"debitofs" => array("legend" => "Débito", "campos" => array("debito_cbu", "debito_activo")),
		);

		$div = Helpers::getDiv("alumno", $alumno, $cols, $fieldSets, true);

		if ($return) {
			return $div;
		} else {
			echo $div;
		}
	}

	public function actionAdmin2Graba()
	{
		$alumno = Alumno::model()->findByPk($_GET["alumno"]["id"]);
		$alumno->setAttributes($_GET["alumno"], false);
		$debito_id = $_GET["alumno"]["debito_id"];
		$d = AlumnoDebito::model()->findByPk($debito_id);
		$tr = Yii::app()->db->beginTransaction();
		if (!$d) {
			$d = new AlumnoDebito();
			$d->alumno_id = $alumno->id;
		}
		$d->cbu = $_GET["alumno"]["debito_cbu"];
		$d->activo = $_GET["alumno"]["debito_activo"];
		$alumno->fecha_ingreso = "";
		if (!$alumno->save()) {
			$tr->rollback();
			echo json_encode($alumno->errors);
			die;
		} else {
			if (!$d->save()) {
				$tr->rollback();
				echo json_encode($d->errors);
				die;
			}
		}
		$alumno->familia->obs = $_GET["alumno"]["obs"];
		$alumno->familia->save();
		$tr->commit();
		echo json_encode(null);
	}

	public function actionChkCta()
	{
		$select
			= "
            SELECT d.anulado, d.socio_id, a.matricula, concat(a.apellido,' ', a.nombre) AS Alumno,
                d.detalle,
                saldo(d.Socio_id,NULL) AS Saldo,
                sum(d.saldo) AS saldoDocsPendientes,
                (sum(d.saldo) - saldo(d.Socio_id,NULL)) AS Diferencia,
                totalPorAplicar(d.Socio_id) AS porAplicar,
                (sum(d.saldo) - saldo(d.Socio_id,NULL) - totalPorAplicar(d.Socio_id)) AS difError
            FROM doc d
					INNER JOIN socio s ON s.id = d.Socio_id
					INNER JOIN alumno a ON a.id = s.Alumno_id
					INNER JOIN alumno_estado ae ON ae.id = a.estado_id AND ae.activo_admin = 1
					INNER JOIN talonario t ON t.id = d.talonario_id
					INNER JOIN comprob c ON c.id =t.comprob_id
            WHERE a.matricula IS NOT NULL AND d.saldo > 0 AND
                                    c.signo_cc = 1 AND
                                    /*d.anulado <> 1  and */
                                    d.activo = 1 AND saldo(d.Socio_id,NULL) > 0
            GROUP BY d.socio_id, a.matricula, a.apellido, a.nombre
            HAVING (sum(d.saldo) - saldo(d.Socio_id,NULL)) = porAplicar
            ORDER BY diferencia DESC
         ";
		$rows = Helpers::qryAllOrEmpty($select);
		foreach ($rows as $key => $row) {
			$socio_id = $row["socio_id"];
			$docs
				= "
                SELECT d.anulado, d.detalle, d.fecha_creacion, c.nombre, d.numero, d.total, d.saldo, d.pago_a_cuenta_importe FROM doc d
                    LEFT JOIN talonario t ON t.id = d.talonario_id
                    LEFT JOIN comprob c ON c.id = t.comprob_id
                    WHERE d.Socio_id = 1";
			$rows[$key]["detailData"]["docs"] = Helpers::qryAll($docs);
		}
		$cols["alumno"] = array(
			"socio_id" => array("visible" => false),
			"Saldo" => array("currency" => true),
		);
		$cols["docs"] = array(
			"total" => array("total" => true),
		);
		$div = Helpers::getTable("alumno", $rows, $cols, true);
		$this->renderPartial("/layouts/main", array("content" => $div));
	}

	public function actionParientes()
	{
		$this->layout = "nuevo";
		$this->render("parientes");
	}

	public function actionParientesGetTipoDocs()
	{
		$tSql
			= "
        SELECT * FROM tipo_documento
      ";
		echo json_encode(Helpers::qryAll($tSql));
	}

	public function actionParientesGetNiveles()
	{
		$nSql
			= "
        SELECT * FROM nivel
      ";
		echo json_encode(Helpers::qryAll($nSql));
	}

	public function actionParientesGetAlumnos($filtro, $filtroNivel = null)
	{
		$ciclo_id = Ciclo::getCicloIdParaInformesAdmin();
		if (!$filtro) {
			$where = "(a.id = -1)";
//      $where = "(a.apellido like '%bedoya%')";
		} elseif ($filtro === "*") {
			$where = " true ";
		} else {
			$where = "(a.apellido like '%$filtro%' or a.nombre like '%$filtro%' or a.matricula like '%$filtro%' )";
		}
		if ($filtroNivel) {
			$where .= " and an.nivel_id = $filtroNivel ";
		}
		$alumnosSql
			= "
        SELECT a.id as alumno_id, concat(a.apellido, ', ', a.nombre) as nombre,
               a.matricula, a.encargado_pago_id, a.encargado_visto, a.familia_id,
               a.encargado_visto, a.encargado_cambiado
         FROM alumno a
          INNER JOIN alumno_estado ae ON ae.id = a.estado_id AND (ae.activo_edu OR ae.activo_admin)
          inner join alumno_division ad on ad.Alumno_id = a.id and ad.Ciclo_id = $ciclo_id
          inner join alumno_division_estado ade on ade.id = ad.alumno_division_estado_id and ade.muestra_admin or ade.muestra_edu
          inner join division d on d.id = ad.Division_id
          inner join anio an on an.id = d.Anio_id
        where $where
        ORDER BY 3
      ";
		//vd($alumnosSql);
//    $alumnosSql
//                   = "
//        SELECT a.familia_id, a.id as alumno_id, concat(a.apellido, ', ', a.nombre) as nombre,
//               a.matricula, p.id as vive_con_id, p.Pariente_Tipo_id,
//               concat(p.apellido, ', ', p.nombre) as vive_con_nombre, t.nombre as vive_con_tipo,
//               tdv.id as tipo_doc_vive_con_id, tdv.nombre as tipo_doc_vive_con, p.numero_documento as doc_vive_con,
//               pp.id as encargado_pago_id,
//               concat(pp.apellido, ', ', pp.nombre) as encargado_pago_nombre,
//               tde.id as tipo_doc_encargado_pago_id, tde.nombre as tipo_doc_encargado_pago, pp.numero_documento as doc_encargado_pago
//         FROM alumno a
//          INNER JOIN alumno_estado ae ON ae.id = a.estado_id AND (ae.activo_edu OR ae.activo_admin)
//          INNER JOIN pariente p on p.id = a.vive_con_id
//          INNER JOIN pariente pp on pp.id = a.encargado_pago_id
//          inner join pariente_tipo t on t.id = p.Pariente_Tipo_id
//          inner join tipo_documento tdv on tdv.id = a.Tipo_Documento_id
//          inner join tipo_documento tde on tdv.id = p.tipo_documento_id
//        where $where
//        ORDER BY 3
//      ";

		$alumnosRows = Helpers::qryAll($alumnosSql);
		foreach ($alumnosRows as $key => $p) {
			$familia_id = $p["familia_id"];
			$parientesSql
				= "
        select p.id, concat(p.apellido, ', ', p.nombre) as nombre, t.nombre as tipo,
          p.tipo_documento_id, p.numero_documento, td.nombre as tipo_documento_nombre
          from pariente p
           inner join pariente_tipo t on t.id = p.Pariente_Tipo_id
           inner join tipo_documento td on td.id = p.tipo_documento_id
          where p.Familia_id = $familia_id
      ";
			$alumnosRows[$key]["parientes"] = Helpers::qryAll($parientesSql);
		}
		echo json_encode($alumnosRows);
	}

	public function actionParientesGrabaAlumno()
	{
		$pariente = json_decode($_GET['pariente']);
		$alumno_id = $_GET["alumno_id"];
		$actual = Helpers::qryScalar("select encargado_pago_id from alumno where id = $alumno_id");
		if ($actual !== $pariente->id) {
			Helpers::qryExec("update alumno set encargado_pago_id = $pariente->id, encargado_cambiado = 1 where id = $alumno_id");
		}
		Helpers::qryExec("
      update pariente
        set tipo_documento_id=$pariente->tipo_documento_id,
            numero_documento = '$pariente->numero_documento'
        where id = $pariente->id");
	}

	public function actionParientesGrabaEncargadoVisto()
	{
		$visto = (isset($_GET['visto']) and $_GET['visto'] === 'true') ? 1 : 0;
		$alumno_id = $_GET["alumno_id"];
		Helpers::qryExec("update alumno set encargado_visto = $visto where id = $alumno_id");
	}

	public function actionParientesNoVistos()
	{
		$ciclo_id = Ciclo::getCicloIdParaInformesAdmin();
		$s
			= "
      SELECT CONCAT(a.apellido, ', ', a.nombre) AS alumno, CONCAT(p.apellido, ', ', p.nombre) AS encargado,
             td.nombre, p.numero_documento, ad.id
        FROM alumno a
          INNER JOIN alumno_estado ae ON ae.id = a.estado_id AND (ae.activo_edu OR ae.activo_admin)
          left join alumno_division ad on ad.Alumno_id = a.id and ad.Ciclo_id = $ciclo_id and ad.activo
  				left join alumno_division_estado ade on ade.id = ad.alumno_division_estado_id and ade.muestra_admin
          inner join pariente p on p.id = a.encargado_pago_id
          inner join tipo_documento td on td.id = p.tipo_documento_id
          inner join division d on d.id = ad.Division_id
          inner join anio an on an.id = d.Anio_id
        where !a.encargado_visto
      order by 1,2
    ";
		$noVistos = Helpers::qryAll($s);
		$this->render("noVistos", array("noVistos" => $noVistos));
	}

	public function actionTest()
	{
		echo 'jsonp(' . json_encode(Helpers::qryAll("SELECT * FROM alumno WHERE apellido LIKE '%sala%'")) . ')';
	}
}
