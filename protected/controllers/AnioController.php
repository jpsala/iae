<?php

	class AnioController extends Controller {

		public function actionIndex() {
			// renders the view file 'protected/views/site/index.php'
			// using the default layout 'protected/views/layouts/main.php'
			//$this->render('index');
			var_dump(Yii::app()->user);
		}

		public function actionAdmin() {
			$this->pageTitle = "Administración de años";
			$this->render("admin");
		}

		public function actionAdminAsignaturas($nivel_id, $anio_id, $division_id = null) {
			$division_id = ($division_id == -2) ? null : $division_id;
			if ($division_id) {
			  $s = "
					SELECT da.division_id, a.orden as orden_div, da.orden as orden_asig, at.tipo as tipo,
						a.nombre, a.abrev, a.id,  at.id as asignatura_tipo_id, at.nombre as asignatura_tipo_nombre, 
							case when da.integradora is not null then da.integradora else a.integradora end as integradora/*, 
							case when da.integradora is not null then da.integradora else a.integradora end as integradoras*/, da.boletin,
							case when da.orden = 0 or da.orden is null then a.orden else da.orden end as orden
							FROM `asignatura` a
									left join division_asignatura da on da.Asignatura_id = a.id and da.Division_id = $division_id
									inner join asignatura_tipo at on at.id = a.asignatura_tipo_id 
							where Nivel_id = $nivel_id and Anio_id = $anio_id and a.activa = 1 
							order by case when da.orden = 0 or da.orden is null then a.orden else da.orden end            
			";
				$asignaturas = Helpers::qryAll($s);
				//ve($s);
			} else {
				$select = "
					SELECT at.tipo as tipo, 
						a.nombre, a.abrev, a.id,  at.id as asignatura_tipo_id, at.nombre as asignatura_tipo_nombre, a.integradora/*,
							case when a.integradora = 1 then 1 else 
									case when 
											(select count(da.integradora)
													from anio a2
															inner join division d on d.Anio_id = a2.id
															inner join division_asignatura da on da.Division_id = d.id
															inner join asignatura asi on asi.id = da.asignatura_id and asi.anio_id = 117
													where d.anio_id = 117) > 1 then 1 else 0 
									end
							end as integradoras*/, boletin
							FROM asignatura a 
									inner join asignatura_tipo at on at.id = a.asignatura_tipo_id 
							where Nivel_id = $nivel_id and Anio_id = $anio_id and a.activa = 1 
							order by  a.orden
			";
				//vd($select);
				$asignaturas = Helpers::qryAll($select);
			}
			$this->renderPartial("_adminAsignaturas", array("nivel_id" => $nivel_id, "anio_id" => $anio_id, "asignaturas" => $asignaturas));
		}

		public function actionAdminAsignaturasGrabaDivision() {
			$division_id = $_POST["division_id"];
			$nombre = $_POST["nombre"];
			$anio_id = $_POST["anio_id"];
			$orden = isset($_POST["orden"]) ? $_POST["orden"] : 0;
			$orientacion = $_POST["orientacion"];
			$division = Division::model()->findByPk($division_id);
			if (!$division) {
				$division = new Division();
				$division->Anio_id = $anio_id;
			}
			$division->nombre = $nombre;
			$division->orden = $orden;
			$division->Orientacion_id = $orientacion == "undefined" ? null : $orientacion;
			if (!$division->save()) {
				echo json_encode($division->errors);
			}
		}

		public function actionAsignaDesasignaAsignatura($division_id, $asignatura_id) {
			$da = DivisionAsignatura::model()->find("Asignatura_id = $asignatura_id and Division_id=$division_id");
			if (!$da) {
				$da = new DivisionAsignatura();
				$da->Division_id = $division_id;
				$da->Asignatura_id = $asignatura_id;
				if (!$da->save()) {
					echo json_encode($da->errors);
				} else {
					echo "Agregada DivisionAsignatura id:$da->id";
				}
			} else {
				if ($da->delete() != 1) {
					echo "Error borrando division_id:$division_id, asignatura_id:$asignatura_id";
				}
			}
		}

		public function actionAdminAsignaturasGraba() {
			$_POST["asignatura_id"] = $_POST["asignatura_id"] == "undefined" ? null : $_POST["asignatura_id"];
			$ret = new stdClass();
			$ret->nuevo = false;
			$asignatura = Asignatura::model()->findByPk($_POST["asignatura_id"]);
			if (!$asignatura) {
				$asignatura = new Asignatura();
				$ret->nuevo = true;
			}
			$asignatura->attributes = $_POST;
			$asignatura->desde_ciclo = Ciclo::getActivo()->id;
			if (!$asignatura->save()) {
				$ret->error = $asignatura->errors;
			} else {
				$ret->asignatura_id = $asignatura->id;
			}
			echo json_encode($ret);
		}

		public function actionAdminAsignaturasBorra($asignatura_id) {
			$asignatura = Asignatura::model()->findByPk($asignatura_id);
			$asignatura->activa = 0;
			if (!$asignatura->save()) {
				echo json_encode($asignatura->errors);
			}
		}

		public function actionOptions($nivel_id) {
			$x = array('prompt' => '');
			echo CHtml::listOptions(null, CHtml::listData(Anio::model()->findAll("nivel_id=$nivel_id"), 'id', 'nombre'), $x);
		}

		public function actionAdminOrientaciones($nivel_id, $anio_id) {
			$orientaciones = Orientacion::model()->findAll("Anio_id = $anio_id");
			$this->renderPartial("/orientacion/_adminOrientaciones", array("nivel_id" => $nivel_id, "anio_id" => $anio_id, "orientaciones" => $orientaciones));
		}

		public function actionOrientaciones($anio_id) {
			$aors = Orientacion::model()->findAll("Anio_id = $anio_id");
			$ret = array();
			/* @var $da DivisionAsignatura */
			foreach ($aors as $ao) {
				$ret[] = $ao->id;
			}
			echo json_encode($ret);
		}

		public function actionAdminOrientacionesGraba() {
			$ret = null;
			$ret->nuevo = false;
			$orientacion = Orientacion::model()->findByPk($_POST["orientacion_id"]);
			if (!$orientacion) {
				$orientacion = new Orientacion();
				$ret->nuevo = true;
			}
			$orientacion->attributes = $_POST;

			if (!$orientacion->save()) {
				$ret->error = $orientacion->errors;
			} else {
				$ret->orientacion_id = $orientacion->id;
			}
			echo json_encode($ret);
		}

		public function actionAdminOrientacionesBorra($orientacion_id) {
			$oenuso = Division::model()->count("Orientacion_id = $orientacion_id");
			if ($oenuso > 0) {
				echo('La orientación se encuentra en uso. No es posible eliminarla');
			} else {
				$orientacion = Orientacion::model()->findByPk($orientacion_id);
				if (!$orientacion->delete()) {
					echo json_encode($orientacion->errors);
				}
			}
		}

		public function actionGrabaAnio($nivel_id, $anio_id, $anio_nombre) {
			if ($anio_id == -1) {
				$anio = new Anio;
				$anio->Nivel_id = $nivel_id;
				$anio->activo = 1;
			} else {
				$anio = Anio::model()->findByPk($anio_id);
			}
			$anio->nombre = $anio_nombre;
			if (!$anio->save()) {
				var_dump($anio->errors);
				die;
			}
			echo $anio->id;
		}

		public function actionAdminGrabaOrdenAsignaturas() {
			$division_id = $_POST["division_id"] == "-2" ? null : $_POST["division_id"];
			$tr = Yii::app()->db->beginTransaction();
			$orden = 10;
			if ($division_id) {
				foreach (explode(",", $_POST["asignaturas"]) as $asignatura_id) {
					if ($asignatura_id and $asignatura_id !== "") {
						//ve("update division_asignatura set orden = $orden 
						//	where asignatura_id = $asignatura_id and division_id = $division_id");
						Yii::app()->db->createCommand("
							update division_asignatura set orden = $orden 
							where asignatura_id = $asignatura_id and division_id = $division_id")->execute();
						$orden+=10;
					}
				}
			} else {
				foreach (explode(",", $_POST["asignaturas"]) as $asignatura_id) {
					if ($asignatura_id and $asignatura_id !== "") {
						Yii::app()->db->createCommand("update asignatura set orden = $orden where id = $asignatura_id")->execute();
						$orden+=10;
					}
				}
			}
			$tr->commit();
		}

		public function actionGrabaIntegradora() {
			$division_id = $_GET["division_id"] == "" ? null : $_GET["division_id"];
			$anio_id = $_GET["anio_id"] == "" ? null : $_GET["anio_id"];
			$asignatura_id = $_GET["asignatura_id"] == "" ? null : $_GET["asignatura_id"];
			$integradora = $_GET["integradora"] == "true" ? 1 : 0;
			$tr = Yii::app()->db->beginTransaction();
			if ($division_id) {
				$a = Asignatura::model()->find("anio_id = $anio_id and id = $asignatura_id");
				$a->integradora = 0;
				$a->save();
				$division_asignaturas = DivisionAsignatura::model()->findAll("asignatura_id = $asignatura_id and division_id = $division_id");
				foreach ($division_asignaturas as $da) {
					$da->integradora = $integradora == 1 ? 1 : 0;
					$da->save();
				}
				foreach ($division_asignaturas as $da) {
					ve($da->attributes);
				}
			} else {
				$a = Asignatura::model()->find("anio_id = $anio_id and id = $asignatura_id");
				$a->integradora = $integradora;
				$a->save();
				$division_asignaturas = DivisionAsignatura::model()->findAll("asignatura_id = $asignatura_id");
				foreach ($division_asignaturas as $da) {
					$da->integradora = $integradora == 1 ? 1 : null;
					$da->save();
				}
			}
			$tr->commit();
		}

		public function actionGrabaBoletin() {
			$division_id = $_GET["division_id"] == "" ? null : $_GET["division_id"];
			$anio_id = $_GET["anio_id"] == "" ? null : $_GET["anio_id"];
			$asignatura_id = $_GET["asignatura_id"] == "" ? null : $_GET["asignatura_id"];
			$boletin = $_GET["boletin"] == "true" ? 1 : 0;
			$tr = Yii::app()->db->beginTransaction();
			if ($division_id) {
				$a = Asignatura::model()->find("anio_id = $anio_id and id = $asignatura_id");
				$a->boletin = 0;
				$a->save();
				$division_asignaturas = DivisionAsignatura::model()->findAll("asignatura_id = $asignatura_id and division_id = $division_id");
				foreach ($division_asignaturas as $da) {
					$da->boletin = $boletin == 1 ? 1 : 0;
					$da->save();
				}
				foreach ($division_asignaturas as $da) {
					ve($da->attributes);
				}
			} else {
				$a = Asignatura::model()->find("anio_id = $anio_id and id = $asignatura_id");
				$a->boletin = $boletin;
				$a->save();
				$division_asignaturas = DivisionAsignatura::model()->findAll("asignatura_id = $asignatura_id");
				foreach ($division_asignaturas as $da) {
					$da->boletin = $boletin == 1 ? 1 : null;
					$da->save();
				}
			}
			$tr->commit();
		}

		public function actionLimpiaOrdenDivision($division_id) {
			Yii::app()->db->createCommand("update division_asignatura set orden = 0
                 where division_id = $division_id")->execute();
		}

		public function actionAsignaturaSubForm($asignatura_sub_id) {
			$this->render("asignaturaSubForm", array("asignatura_sub_id" => $asignatura_sub_id));
		}

	}
	