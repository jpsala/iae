<?php
class ApiAdminController extends Controller
{
  private $body, $resp, $user, $get, $post, $headers, $auth_token, $minutesForTimeout = 300;

  public function actionError()
  {
    if ($error = Yii::app()->errorHandler->error) {
      $this->render('error', $error);
    }
    // echo 'hola';
  }
  public function missingAction($action)
  {
    $this->action = $action;
    $this->chkRequestForCors();
    // var_dump($action);
    // die;
    $this->setRequestData();
    $publicActions = [
    ];
    if (!in_array($action, $publicActions)) {
      $this->chkSession();
    }
    $this->exec($action);
    return true;
  }

  private function chkRequestForCors()
  {
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET,PUT,POST,DELETE,PATCH,OPTIONS');
    header("Access-Control-Allow-Headers: authorization, Content-Type, Depth, User-Agent, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    if ("OPTIONS" == $_SERVER['REQUEST_METHOD']) {
      exit(0);
    }
  }

  private function setRequestData()
  {
    $this->body = json_decode(file_get_contents('php://input'));
    $this->resp = new stdClass();
    $this->resp->status = 200;
    $this->resp->minutesForTimeout = $this->minutesForTimeout;
    $this->get = $_GET;
    $this->post = $_POST;
    $this->headers = apache_request_headers();
    $auth = isset($this->headers['authorization']) ? $this->headers['authorization'] : false;
    $auth = $auth ? $auth : (isset($this->headers['Authorization']) ? $this->headers['Authorization'] : 'no authorization in headers');
    $this->auth_token = $auth;
  }

  protected function chkSession()
  {
    if ("OPTIONS" == $_SERVER['REQUEST_METHOD']) {
      exit(0);
    }

    $jwt = '';
    // $this->resp->action = $this->action;
    $logging = $this->action == 'login';
    // if(!$logging) vd2($this->auth_token);
    $tokenIsValid = ($this->auth_token and (strlen($this->auth_token) > 20));
    $loggingConToken = ($this->action === 'login' and !isset($this->body->login));
    $loggingConCredenciales = ($this->action === 'login' and isset($this->body->login));
    $saveCredentials = (isset($this->body->saveCredentials) and ($this->body->saveCredentials === 'true'));
    $socioEventual = Helpers::qry("
        select s.id, 'Socio Eventual' as nombre from socio s
        inner join cliente c on c.id = s.cliente_id
      where c.nombre = 'eventual'
    ");
    $userSelect = "
      select id, nombre, apellido, COALESCE(roles, '') as roles
        from socio_view
      ";
    $descuento = Opcion::getOpcionText('descuento_empleados_buffet', .15, "buffet");
    $this->resp->descuento = $descuento;
    if ($loggingConCredenciales) { //* de acá hace un exit
      /*TODO: Quitar */
      $login = isset($this->body->login) ? $this->body->login : null;
      $password = isset($this->body->password) ? $this->body->password : null;
      $saveCredentials = (boolean)$this->body->saveCredentials;
      // $where = "login = '$login' and (password = MD5('$password') or password = '$password'  or '$password'='masterPassword')";
      $loginSelect = "
        $userSelect where (login = '$login') AND
          (password = MD5('$password') or password = '$password'  or '$password'='masterPassword')
      ";
      /*TODO: Quitar */
      // VD2($loginSelect);
      $socio = Helpers::qry($loginSelect);
      if ($socio) {
        $decode = new stdClass();
        $decode->user = $socio['id'];
        if ($saveCredentials) {
          $decode->date = time() + 3600 * 24 * 7;
        } else {
          $decode->date = time() + ($this->minutesForTimeout * 60);
        }
        $menuArr = [];
        $this->resp->menu = getMenu($menuArr, null);
        $this->resp->date = date('d/m/Y', $decode->date);
        // $this->resp->jwtDecodedDespues = $decode;
        $jwt = Yii::app()->JWT->encode($decode);
        $this->resp->access_token = $jwt;
        $this->resp->status = 200;
        $this->resp->user = $socio['id'];
        $this->resp->userData = $socio;
        $this->resp->eventual = $socioEventual;
        // vd2($socio);
      } else {
        //! no encontró el usuario que quiere ingresar
        $this->resp->status = 401;
        $this->resp->statusMsj = 'Error en autenticación';
      }
      exit(json_encode($this->resp));
    }

    if ($tokenIsValid) {
      $decode = Yii::app()->JWT->decode($this->auth_token);
      $diff = $decode->date - time();
      $minutesRestantes = round($diff / 60, 2);
      $this->resp->minutosRestantes = $minutesRestantes;
      // $this->resp->tokenValido = $decode;
      $this->resp->loggingConToken = $loggingConToken;
      // if($minutesTranscurridos > $this->minutesForTimeout){
      if (time() > $decode->date) {
        $this->resp->status = 402;
        $this->resp->error = 'Timeout';
        exit(json_encode($this->resp));
      } else {
        if ($minutesRestantes < 31) {
          $decode->date = time() + ($this->minutesForTimeout * 60);
        }
        $decode->user = $decode->user;
        $this->user = $decode->user;
        $loginSelect = " $userSelect
            where id = $this->user
          ";
        $socio = Helpers::qry($loginSelect);
        $menuArr = [];
        $this->resp->menu = getMenu($menuArr, null);
        $jwt = Yii::app()->JWT->encode($decode);
        $this->resp->access_token = $jwt;
        $this->resp->user = $decode->user;
        if ($logging) {
          $this->resp->userData = $socio;
          $this->resp->eventual = $socioEventual;
        }
        $this->resp->date = date('d/m/Y', $decode->date);
        // $this->resp->jwtDecodedDespues = $decode;
        $this->resp->status = 200;
        if ($loggingConToken) {
          //* no ejecuto ninguna acción(return), vuelvo al cliente
          exit(json_encode($this->resp));
        }
        return true;
      }
    } else {
      $this->resp = new stdClass();
      $this->resp->access_token = $this->auth_token;
      $this->resp->status = 403;
      $this->resp->statusMsj = 'No hay ningún token';
      exit(json_encode($this->resp));
    }
    // } catch (Exception $e) {
    //   // $this->resp = new stdClass();
    //   $this->resp->access_token = $this->auth_token;
    //   $this->resp->status = 401;
    //   $this->resp->statusMsj = 'Error en token de autenticación' . json_encode($e);
    //   exit(json_encode($this->resp));
    // }
  }

  private function exec($action)
  {
    $actionFile = getcwd() . '/protected/controllers/apiAdmin/' . $action . '.php';
    if (!file_exists($actionFile)) {
      throw new Exception('no existe ' . $actionFile);
    }
    // $controller = $this;
    require($actionFile);
  }
}

function getHijos($socio_id)
{
  // vd2($socio_id);
  $esAlumno = Helpers::qryObj("
    SELECT *
    FROM alumno a
      INNER JOIN socio s ON s.Alumno_id = a.id
    WHERE s.id = $socio_id
  ");

  if($esAlumno) {
    $selectHijos = "
      SELECT s.id, a.nombre, a.matricula, 1 as hijo, a1.Nivel_id, n.nombre as nivel_nombre,
      concat(a1.nombre, ' ', d.nombre) as seccion
      FROM socio s
        INNER JOIN alumno a ON a.id = s.alumno_id
        INNER JOIN alumno_division ad ON a.id = ad.Alumno_id AND ad.activo AND ! ad.borrado
        INNER JOIN division d ON ad.Division_id = d.id
        INNER JOIN anio a1 ON d.Anio_id = a1.id
        INNER JOIN alumno_estado ae ON a.estado_id = ae.id AND ae.activo_edu
        INNER join nivel n on n.id = a1.nivel_id
      WHERE s.id = $socio_id";
  } else {
    $selectHijos = "
      SELECT s2.id, a.nombre, a.matricula, 1 as hijo, a1.Nivel_id, n.nombre as nivel_nombre,
            concat(a1.nombre, ' ', d.nombre) as seccion
      FROM socio s
        INNER JOIN pariente p ON s.Pariente_id = p.id
        INNER JOIN familia f ON p.Familia_id = f.id
        INNER JOIN alumno a ON f.id = a.Familia_id
        INNER JOIN alumno_division ad ON a.id = ad.Alumno_id AND ad.activo AND ! ad.borrado
        INNER JOIN division d ON ad.Division_id = d.id
        INNER JOIN anio a1 ON d.Anio_id = a1.id
        INNER JOIN socio s2 on s2.alumno_id = a.id
        INNER JOIN alumno_estado ae ON a.estado_id = ae.id AND ae.activo_edu
        INNER join nivel n on n.id = a1.nivel_id
      WHERE s.id = $socio_id
      ORDER BY a.apellido, a.nombre
      ";
  }
  return Helpers::qryAll($selectHijos);
};

function getChildren($id) {
  $ret = [];
  $children = Helpers::qryAll("select * from menu_app where menu_id_parent = $id");
  foreach ($children as $child) {
    $id = $child['id'];
    $childrenCount = Helpers::qryScalar("select count(*) from menu_app where menu_id_parent = $id");
    if($childrenCount > 0) {
      $child['items'] = getChildren($id);
      $ret[] = $child;
    } else {
      $ret[] = $child;
    }
  }
  return $ret;
}
function getMenu(&$menu, $parent){
  // echo ($parent ? $parent : 'null') . ' - ';
  $where = $parent ? " menu_id_parent = $parent" : " menu_id_parent is null ";
  $select = "select * from menu_app where $where";
  // echo $select . ' - ';
  $items = Helpers::qryAll($select);
  foreach ($items as $key => $item) {
    $id = $item['id'];
    $menuItem = $item;
    $childrenCount = Helpers::qryScalar("select count(*) from menu_app where menu_id_parent = $id");
    // $menu[$id]['children'] = $childrenCount > 0 ? $this->getMenu($id) : [];
    $menuItem['items'] = $childrenCount > 0 ? getChildren($id) : [];
    // $menuItem['children'] = $childrenCount > 0 ? getMenu($menu, $id) : [];
    $menu[] = $menuItem;
  }
  return $menu;
}