<?php

class ApiController extends Controller {
	public $request;
	private $classes = array(
		'alumnos' => 'alumno',
		'parientes' => 'pariente',
		'familias' => 'familia',
		'profesores' => 'profesor',
		'notas' => 'notas'
	);

	public function actions() {
		$request = $this->parseRequest();
		$actions = array();
		foreach ($this->classes as $plural => $class) {
			$actions[$plural] = array(
				"class" => "application.controllers.api." . $plural . "Action",
				"request" => $request
			);
		}

		return $actions;
	}

	private function echoRest($r) {
		ve($r);
		//$select = "select "
	}

	public function actionColors() {
		$ret = new stdClass();
		$ret->colors = array();
		$ret->colors[] = array('id' => 1, 'color' => 'red');
		$ret->colors[] = array('id' => 2, 'color' => 'blue');
		$ret->colors[] = array('id' => 3, 'color' => 'yellow');
		echo json_encode($ret);
	}
	private function chkRequest2()
	{
		header('Access-Control-Allow-Origin: http://iae.dyndns.org');
		header('host:'.$_SERVER['HTTP_HOST']);
		if($_SERVER['HTTP_HOST'] === 'localhost'){
			header('Access-Control-Allow-Origin: http://localhost:3000');
//			var_export($_SERVER);
		}
		header('Access-Control-Allow-Origin: '.$_SERVER['HTTP_HOST']);
		header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
		header('Access-Control-Max-Age: 1000');
		header("Access-Control-Allow-Credentials: true");
		header("Access-Control-Allow-Headers: Content-Type, Depth, User-Agent, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control");
		if (array_key_exists('HTTP_ACCESS_CONTROL_REQUEST_HEADERS', $_SERVER)) {
			header('Access-Control-Allow-Headers: '
				. $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']);
		} else {
//			header('Access-Control-Allow-Headers: *');
		}
		$port = $_SERVER['HTTP_HOST'] === 'localhost'?':3000':'';
		//vd('Access-Cont$this->bodyrol-Allow-Origin: http://'.$_SERVER['HTTP_HOST'].$port);
//		$host = substr($_SERVER['REMOTE_HOST']);
		if(isset($_SERVER['HTTP_ORIGIN'])){
			$host = $_SERVER['HTTP_ORIGIN'];
//		$host = $_SERVER['HTTP_ORIGIN'];
		}else{
			$host = 'http://localhost';
		}
		header('Access-Control-Allow-Origin: '.$host);

		if ("OPTIONS" == $_SERVER['REQUEST_METHOD']) {
			exit(0);
		}
	}
	private function chkRequest() {
		$ret = new stdClass();
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
		header('Access-Control-Max-Age: 1000');
		if (array_key_exists('HTTP_ACCESS_CONTROL_REQUEST_HEADERS', $_SERVER)) {
			header('Access-Control-Allow-Headers: '
				. $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']);
		} else {
			header('Access-Control-Allow-Headers: *');
		}
		if ("OPTIONS" == $_SERVER['REQUEST_METHOD']) {
			exit(0);
		}
	}

	public function actionLogout() {
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');
		$ret = new stdClass();
		$ret->status = 200;
//			Yii::app()->user->logout();
		$ret->access_token = '12345';
		exit(json_encode($ret));
		//echo json($ret, $_GET['callback']);
	}

	public function actionGrabaDomicilios() {
		$this->chkRequest2();

		$ret = new stdClass();
		$ret->status = 200;

		$locations = json_decode(file_get_contents('php://input'));
		foreach($locations as $l){
			$location = $l->location->lat . ','. $l->location->lng;
			$id = $l->id;
			Helpers::qryExec("
				update alumno set location = \"$location\" where id = $id
			");
		}
		exit(json_encode($ret));
//		var_dump($_POST);
	}

	public function actionAlumnosConDomicilio() {
		$this->chkRequest2();
		$select = "
			SELECT a.id, concat(a.apellido, ', ', a.nombre) AS nombre,
             concat('Mar del plata, ', p.calle, ' ', p.numero) AS domicilio,
				a.location
			FROM alumno a
				inner join alumno_estado ae on ae.id = a.estado_id
				inner join alumno_division ad on ad.Alumno_id = a.id
				inner join alumno_division_estado ade on ade.id = ad.alumno_division_estado_id
				inner join pariente p on p.id = a.vive_con_id
				inner join ciclo c on c.id = ad.Ciclo_id
			where a.activo and ae.activo_admin and ad.activo
					  and ade.muestra_admin and ad.Ciclo_id = 4
		";
		$alumnos = Helpers::qryAll($select);
//		var_dump($alumnos);die;
		$ret = new stdClass();
		$ret->status = 200;
		$ret->access_token = session_id();
		$ret->data = $alumnos;
		exit(json_encode($ret));
//		exit(json($alumnos, $_GET['callback']));
	}

		public function actionBuffet_articulos() {
		$this->chkRequest();
		$ret = new stdClass();
		$entityBody = json_decode(file_get_contents('php://input'));
		$select = "SELECT id, nombre, precio_venta, imagen, comentarios, borrado FROM buffet_articulo";
		$arts = Helpers::qryAll($select);
		foreach ($arts as $key => $art) {
			$selectSub = "SELECT id, nombre, precio_venta
        FROM buffet_articulo_sub
        WHERE articulo_id = " . $art["id"];
			$artsSub = Helpers::qryAll($selectSub);
			if (!$artsSub) {
				$artsSub = $art;
				$artsSub['nombre'] = '';
			}
			$arts[$key]['subs'] = $artsSub;
		}
		$ret->status = 200;
		$ret->access_token = '12345';
		$ret->data = $arts;
		exit(json_encode($ret));
	}

	public function actionBuffet_cierra() {
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');
		$ret = new stdClass();
//		vd($_REQUEST);
		$doc = $_REQUEST['doc'];
		$items = $_REQUEST['items'];
		$tr = Yii::app()->db->beginTransaction();

		Helpers::qryExec(
			"
				INSERT INTO buffet_doc(socio_id, total) VALUES(:socio_id, :total)
			", ['socio_id' => $doc['socio_id'], 'total' => $doc['total']]);
		$doc_id = helpers::qryScalar('select LAST_INSERT_ID();');
		foreach ($items as $key => $item) {
			if (!isset($item['subId'])) {
				$item['subId'] = Null;
				$item['subNombre'] = '';
			}
			Helpers::qryExec(
				"
				insert into buffet_doc_det(doc_id, articulo_id, detalle, importe, sub_articulo_id)
							 values($doc_id, :articulo_id, :detalle, :importe, :subId)",
				array(
					'articulo_id' => $item['id'],
					'detalle' => $item['nombre'] . ' ' . $item['subNombre'],
					'importe' => $item['precio_venta'],
					'subId' => $item['subId']));
		}
		//([{"id":"1","nombre":"Hamburguesa","precio_venta":"50.00","imagen":"hamburguesa_completa.jpg","comentarios":null,"borrado":"0","subs":[{"id":"1","nombre":"con jam\u00f3n y queso","precio_venta":"65.00"},{"id":"3","nombre":"con queso","precio_venta":"60.00"},{"id":"4","nombre":"con queso y tomate","precio_venta":"65.00"},{"id":"5","nombre":"con tomate y lechuga","precio_venta":"65.00"},{"id":"6","nombre":"completa (huevo, jamon y queso)","precio_venta":"75.00"},{"id":"7","nombre":"sola","precio_venta":"60.00"}]},{"id":"2","nombre":"Pancho","precio_venta":"40.00","imagen":"pancho.jpg","comentarios":null,"borrado":"0","subs":{"id":"2","nombre":"","precio_venta":"40.00","imagen":"pancho.jpg","comentarios":null,"borrado":"0"}},{"id":"3","nombre":"Milanesa","precio_venta":"60.00","imagen":"milanesa.jpg","comentarios":null,"borrado":"0","subs":[{"id":"8","nombre":"sola","precio_venta":"60.00"},{"id":"9","nombre":"completa","precio_venta":"80.00"}]},{"id":"4","nombre":"Gaseosa","precio_venta":"15.00","imagen":"gaseosa.jpg","comentarios":null,"borrado":"0","subs":[{"id":"10","nombre":"Coca Cola","precio_venta":"50.00"},{"id":"11","nombre":"Sprite","precio_venta":"50.00"},{"id":"12","nombre":"Fanta","precio_venta":"50.00"}]},{"id":"5","nombre":"Tarta","precio_venta":"45.00","imagen":"tarta.jpg","comentarios":null,"borrado":"0","subs":[{"id":"13","nombre":"Jam\u00f3n y queso","precio_venta":"60.00"},{"id":"14","nombre":"Verdura","precio_venta":"60.00"},{"id":"15","nombre":"Choclo","precio_venta":"60.00"}]},{"id":"6","nombre":"Papas Fritas","precio_venta":"40.00","imagen":"fritas.jpg","comentarios":null,"borrado":"0","subs":{"id":"6","nombre":"","precio_venta":"40.00","imagen":"fritas.jpg","comentarios":null,"borrado":"0"}},{"id":"8","nombre":"Pizza","precio_venta":"70.00","imagen":"pizza.jpg","comentarios":null,"borrado":"0","subs":[{"id":"16","nombre":"Muzarella","precio_venta":"60.00"},{"id":"17","nombre":"Napolitana","precio_venta":"90.00"},{"id":"18","nombre":"Cebolla","precio_venta":"85.00"}]},{"id":"9","nombre":"Sandwich Completo","precio_venta":"0.00","imagen":"sandwich_completo.jpg","comentarios":null,"borrado":"0","subs":{"id":"9","nombre":"","precio_venta":"0.00","imagen":"sandwich_completo.jpg","comentarios":null,"borrado":"0"}},{"id":"10","nombre":"Canelones","precio_venta":"90.00","imagen":"canelones.jpg","comentarios":null,"borrado":"0","subs":{"id":"10","nombre":"","precio_venta":"90.00","imagen":"canelones.jpg","comentarios":null,"borrado":"0"}},{"id":"12","nombre":"Tortilla","precio_venta":"60.00","imagen":"tortilla.jpg","comentarios":null,"borrado":"0","subs":{"id":"12","nombre":"","precio_venta":"60.00","imagen":"tortilla.jpg","comentarios":null,"borrado":"0"}}]);
		$tr->commit();
		$ret->status = 'ok';
//		$ret->data = $_REQUEST;
		exit(json_encode($ret));
//		exit(json($ret, $_GET['callback']));
	}

	public function actionAlumnosConVariasDivisionesPorAnio() {
		$ciclo_id = Helpers::qryScalar("SELECT id FROM ciclo WHERE activo = 1");
		$select = "
      SELECT n.nombre, c.nombre, a.id, a1.nombre, d.nombre, a.apellido, a.nombre, ad.*,
        (SELECT COUNT(*) FROM nota n WHERE n.Alumno_Division_id = ad.id) AS notas,
        (SELECT COUNT(*) FROM alumno_division ad1 INNER JOIN division d1 ON ad1.Division_id = d1.id INNER JOIN anio a2 ON d1.Anio_id = a2.id WHERE ad1.Alumno_id = a.id AND a2.id = a1.id AND ad1.Ciclo_id = ad.Ciclo_id) AS ad,
        (SELECT GROUP_CONCAT(d1.nombre) FROM alumno_division ad1 INNER JOIN division d1 ON ad1.Division_id = d1.id INNER JOIN anio a2 ON d1.Anio_id = a2.id WHERE ad1.Alumno_id = a.id AND a2.id = a1.id AND ad1.Ciclo_id = ad.Ciclo_id) AS divisiones
      FROM alumno a
          INNER JOIN alumno_division ad ON a.id = ad.Alumno_id
          INNER JOIN alumno_estado ae ON a.estado_id = ae.id
          INNER JOIN division d ON ad.Division_id = d.id
          INNER JOIN anio a1 ON d.Anio_id = a1.id
          INNER JOIN ciclo c ON ad.Ciclo_id = c.id
          INNER JOIN nivel n ON a1.Nivel_id = n.id
      WHERE (SELECT COUNT(*) FROM nota n WHERE n.Alumno_Division_id = ad.id) = 0
              AND (SELECT COUNT(*) FROM alumno_division ad1 INNER JOIN division d1 ON ad1.Division_id = d1.id INNER JOIN anio a2 ON d1.Anio_id = a2.id WHERE ad1.Alumno_id = a.id AND a2.id = a1.id AND ad1.Ciclo_id = ad.Ciclo_id) > 1
              AND a1.nivel_id != 1
      ORDER BY n.nombre, a1.nombre, a.apellido, a.nombre";
		$qry = Helpers::qryAll($select);
		exit(json($qry, $_GET['callback']));
	}

	private function parseRequest() {
		$request = $_REQUEST;
		$return = new stdClass();
		$return->id = null;
		if (isset($request["id"])) {
			$return->id = $request["id"];
		}
		$return->resources = array();
		$return->fields = '*';
		$return->where = " ";
		$return->order = " ";
		foreach ($_REQUEST as $key => $value) {
			if ($key == 'fields') {
				$return->fields = $request["fields"];
			} elseif ($key == 'filtro') {
				$return->where = $value;
			} elseif ($key == 'order') {
				$return->order = $value;
			} else {
				$return->$key = $value;
			}
		}
		if (isset($request["resource"])) {
			$resources = explode(',', $request["resource"]);
			foreach ($resources as $resource) {
				$parts = explode(':', $resource);
				//      ve($parts);
				//$return->resources[$parts[0]] = $parts[1];
				//for($i=2; $i<count($parts); $i++){
				//                        ve($i);
				//                    }
			};
		}

		return $return;
	}

}

function json($data, $callback = "callback") {
	$json = CJSON::encode($data);
	return $callback . ' (' . $json . ');';
}
