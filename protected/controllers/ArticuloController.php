<?php

class ArticuloController extends Controller {

    public $layout = "column1";

    public function actionArticulo($id = null) {
        $model = Articulo::model()->findByPk($id);
        if (!$model) {
            $model = new Articulo();
        }
        $this->render('articuloIndex', array('model' => $model));
    }

    public function actionArticuloGraba() {

        $tran = Yii::app()->db->beginTransaction();

        $model = Articulo::model()->findByPk($_POST['id']);

        if (!$model) {
            $model = new Articulo();
        }

        $model->attributes = $_POST;
        $model->nombre = ucwords(strtolower($_POST['nombre']));


        if ($model->isNewRecord)
            $model->fecha_creacion = new CDbExpression('NOW()');
        else
            $model->fecha_modificacion = new CDbExpression('NOW()');
        if (!$model->save()) {
            $tran->rollback();
            echo json_encode($model->getErrors());
            Yii::app()->end();
        }
        echo json_encode("");
        $tran->commit();
    }

    public function actionArticuloForm($id) {
        $model = Articulo::model()->findByPk($id);
        if (!$model) {
            $model = new Articulo();
        }
        $this->renderPartial('_articuloForm', array('model' => $model));
    }

    public function actionArticuloBorra($id) {
        Articulo::model()->deleteAll("id=$id");
    }

    public function actionTipoArticulo($id = null) {

        $model = ArticuloTipo::model()->findByPk($id);
        if (!$model) {
            $model = new ArticuloTipo();
        }
        $this->render('articulotipoIndex', array('model' => $model));
    }

    public function actionTipoArticuloGraba() {

        $tran = Yii::app()->db->beginTransaction();

        $model = ArticuloTipo::model()->findByPk($_POST['id']);

        if (!$model) {
            $model = new ArticuloTipo();
        }

        $model->attributes = $_POST;
        $model->nombre = ucwords(strtolower($_POST['nombre']));
        $model->novedad_tipo = $_POST['tipo'];
        if (isset($_POST['maneja_stock']))
            $model->maneja_stock = 1;
        else
            $model->maneja_stock = 0;

        if (isset($_POST['maneja_orden']))
            $model->maneja_orden = 1;
        else
            $model->maneja_orden = 0;


        if (!$model->save()) {
            $tran->rollback();
            echo json_encode($model->getErrors());
            Yii::app()->end();
        }
        $tran->commit();
    }

    public function actionTipoArticuloForm($id) {
        $model = ArticuloTipo::model()->findByPk($id);
        if (!$model) {
            $model = new ArticuloTipo();
        }
        $this->renderPartial('_articulotipoForm', array('model' => $model));
    }

    public function actionTipoArticuloBorra($id) {
        $artenuso = Articulo::model()->count("articulo_tipo_id = $id");
        if ($artenuso > 0) {
            echo('El Tipo de Articulo se encuentra en uso. No es posible eliminarla');
        } else {
            ArticuloTipo::model()->deleteAll("id=$id");
        }
    }

    public function articuloTipoOptions() {
        $html = '<option value=""></option>';
        foreach (ArticuloTipo::model()->findAll() as $at) {
            $html.= "<option maneja_orden=\"$at->maneja_orden\" value=\"$at->id\">$at->nombre</option>";
        }
        return $html;
    }

    public function actionarticuloManejaOrden($id) {
        if ($id != null) {
            $art = ArticuloTipo::model()->FindbySql("select maneja_orden from articulo_tipo where id = $id");
            echo $art['maneja_orden'];
        } else {
            echo '1';
        }
//      if (!$art) {
//          
//          echo "error";
//      }
//     else{
//        echo $art['maneja_orden'];
//      }
    }

    public function actionArticulosAc() {
        $term = $_GET['term'];
        $articulos = Yii::app()->db->createCommand("
        select a.id, a.nombre, a.precio_neto as importe
              from articulo a
                inner join articulo_tipo t on t.id = a.articulo_tipo_id
              where t.novedad_tipo = 3 and a.nombre like \"%$term%\"")->queryAll();
        $ret = array();
        foreach ($articulos as $a) {
            $ret[] = array(
                    "id" => $a["id"],
                    'label' => $a["nombre"],
                    'value' => $a["nombre"]);
        }
        echo json_encode($ret);
    }

}

?>
