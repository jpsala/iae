<?php

class AsignaturaController extends Controller {

    public function actionIndex() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        //$this->render('index');
        var_dump(Yii::app()->user);
    }
    
    public function actionAdmin(){
        $this->render("admin");
    }

    public function actionOptions($division_id) {
        $x = array('prompt' => '');
        echo CHtml::listOptions(null, CHtml::listData(DivisionAsignatura::model()->with("asignatura")->findAll("Division_id=$division_id"), "id", 'asignatura.abrev'), $x);
    }

}