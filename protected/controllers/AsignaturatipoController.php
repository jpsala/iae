<?php

class AsignaturaTipoController extends Controller {
    
    public function actionAdmin() {
        $this->pageTitle = "Administración de tipos de asignaturas";
        $this->render("/tipo_asignatura/tipoasig");
    }

    public function actionOptions($nivel_id) {
        //$x = array('prompt' => '');
        //echo CHtml::listOptions(null, CHtml::listData(Anio::model()->findAll("nivel_id=$nivel_id"), 'id', 'nombre'), $x);
        $asignaturas_tipo = AsignaturaTipo::model()->findAll("Nivel_id = $nivel_id");
        $this->renderPartial("/tipo_asignatura/_admintipoasig", array("nivel_id"=>$nivel_id, "$asignaturas_tipo" => $asignaturas_tipo));
    }
   
    public function actionAdminAsigTipos($nivel_id) {
        $asignaturas_tipo = AsignaturaTipo::model()->findAll("Nivel_id = $nivel_id");
        $this->renderPartial("/tipo_asignatura/_admintipoasig", array("nivel_id"=>$nivel_id, "$asignaturas_tipo" => $asignaturas_tipo));
    }
    
    public function actionAsigTipos($anio_id) {
        $atrs = AsignaturaTipo::model()->findAll("Anio_id = $anio_id");
        $ret=array();
        /* @var $da DivisionAsignatura */
        foreach ($atrs as $at) {
            $ret[]=$at->id;
        }
        echo json_encode($ret);
    }
    
    public function actionAdminAsigTiposGraba() {
        $ret = null;
        $ret->nuevo = false;
        $asignatura_tipo = AsignaturaTipo::model()->findByPk($_POST["asignatura_tipo_id"]);
        if (!$asignatura_tipo) {
            $asignatura_tipo = new AsignaturaTipo();
            $ret->nuevo = true;
        }
        $asignatura_tipo->attributes = $_POST;
        
        if (!$asignatura_tipo->save()) {
            $ret->error = $asignatura_tipo->errors;
        } else {
            $ret->asignatura_tipo_id = $asignatura_tipo->id;
        }
        echo json_encode($ret);
    }
    
    public function actionAdminAsigTiposBorra($asignatura_tipo_id) {
        $atenuso = Asignatura::model()->count("Asignatura_Tipo_id = $asignatura_tipo_id");
        if ($atenuso > 0) {
            echo('El Tipo de Asignatura se encuentra en uso. No es posible eliminarla');
        } else {
            $asignatura_tipo = AsignaturaTipo::model()->findByPk($asignatura_tipo_id);
            if (!$asignatura_tipo->delete()) {
                echo json_encode($asignatura_tipo->errors);
            }
        }
    }
    
    

}