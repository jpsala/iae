<?php

class AsrController extends Controller {

  public $layout = "nuevo";

  public function actionRetenciones() {
    $this->render("retenciones");
  }

  public function actionImportaSueldos() {
	  $this->render("importaSueldos");
  }

  public function actionSubeArchivo() {
    if (!isset($_FILES['archivo_sueldos']["name"]) or ! $_FILES['archivo_sueldos']["name"]) {
      echo "Especifique el nombre del archivo";
      return;
    }
    $destino = 'archivos_sueldos\\' . $_FILES['archivo_sueldos']['name'];
    if (!file_exists('archivos_sueldos')) {
      mkdir('archivos_sueldos');
    }
    if (file_exists($destino)) {
      unlink($destino);
    }
    copy($_FILES["archivo_sueldos"]['tmp_name'], $destino);
    $this->procesaArchivo($destino);
    }

    private function procesaArchivo($archivo) {
    set_time_limit(0);
    $dbh = new PDO('mysql:host=localhost;dbname=iae-nuevo', 'root', 'lani0363', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $this->actualizaCamposSueldos($_POST['ids'], $dbh);
    spl_autoload_unregister(array('YiiBase', 'autoload'));
    $phpExcelPath = Yii::getPathOfAlias('ext.PHPExcel.Classes');
    include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
    $fileType = 'Excel5';
    spl_autoload_register(array('YiiBase', 'autoload'));

    $objReader = PHPExcel_IOFactory::createReader($fileType);
    $objPHPExcel = $objReader->load($archivo);

    $as = $objPHPExcel->getActiveSheet();
    $val = $objPHPExcel->getActiveSheet()->getCell("A1")->getValue();
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $fileType);
    $lastColumn = $as->getHighestColumn();
    $lastColumn++;
    for ($column = 'A'; $column != $lastColumn; $column++) {
      $cell = $as->getCell($column . 1);
      $val = $cell->getValue();
      $nombre_nuevo = $this->nombreNuevo($val, $dbh);
      if(trim($nombre_nuevo)){
      $cell->setValue($nombre_nuevo);
      }else{
      }
    }
    $nombreArchivo = $_FILES['archivo_sueldos']["name"];
    header('Content-type: application/vnd.ms-excel');
    header("Content-Disposition: attachment; filename=\"empl.xls\"");
    $objWriter->save('php://output');
    }

    private function actualizaCamposSueldos($ids, $dbh){
    foreach ($ids as $nombre_viejo => $nombre_nuevo) {
      $h = $dbh->query("select count(*) from sueldos_campos where nombre_viejo = \"$nombre_viejo\"");
      $existe = $h->fetchColumn();
      if (!$existe) {
      $dbh->exec("insert into sueldos_campos(nombre_viejo,nombre_nuevo) values(\"$nombre_viejo\",\"$nombre_nuevo\")");
      }else{
      $dbh->exec("update sueldos_campos set nombre_nuevo = \"$nombre_nuevo\" where nombre_viejo = \"$nombre_viejo\"");
      }
      
    }
    
    }
    private function nombreNuevo($nombreViejo, $dbh) {
      $v = $dbh->query("select nombre_nuevo from sueldos_campos where nombre_viejo = \"$nombreViejo\"")->fetch();
    return $v["nombre_nuevo"];
  }

}
