<?php

class auditController extends CController {

    public function actionInit() {
        
        $tables = array("doc","doc_valor","alumno", "novedad","logica","logica_item");
        
        echo "Create database<br/>";
        Yii::app()->db->createCommand("
            CREATE DATABASE IF NOT EXISTS iae_audit")->execute(array());
        foreach ($tables as $t) {
            echo "<br/>$t<br/><br/>";
            $existe_audit_id = Yii::app()->db->createCommand("
                SELECT count(*)
                    FROM INFORMATION_SCHEMA.COLUMNS
                    WHERE   TABLE_NAME = '$t' AND 
                                    COLUMN_NAME = 'audit_id' and
                                    table_schema = \"iae-nuevo\"")->queryScalar(array());
            if (!$existe_audit_id) {
                echo "Create $t<br/>";
                Yii::app()->db->createCommand("
               ALTER TABLE $t ADD audit_id INT UNSIGNED NULL")->execute(array());
            }

            echo "create $t audit<br/>";
            $s = "
                SET FOREIGN_KEY_CHECKS=0;   # (1)
                drop table if exists `iae_audit`.$t;
                CREATE TABLE `iae_audit`.$t LIKE `iae-nuevo`.$t;
                ALTER TABLE `iae_audit`.$t
                   DROP PRIMARY KEY,
                   MODIFY id SMALLINT UNSIGNED NOT NULL,  # (1)
                   ADD audit_tipo CHAR(1), 
                   ADD audit_creacion datetime, 
                   MODIFY audit_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY;";
            Yii::app()->dba->createCommand($s)->execute(array());

            $s = "
                drop view if exists `iae-nuevo`." . $t ."_history;
                CREATE VIEW `iae-nuevo`." . $t ."_history AS SELECT * FROM `iae_audit`.$t;";
            //vd($s);
            echo "Create $t view<br/>";
            Yii::app()->db->createCommand($s)->execute(array());
            $s = "
                USE `iae-nuevo`;
                DELIMITER $$
                DROP TRIGGER IF EXISTS " . $t . "_ari$$
                CREATE TRIGGER " . $t . "_ari
                AFTER INSERT ON " . $t . "
                FOR EACH ROW
                BEGIN
                  INSERT INTO iae_audit." . $t . "
                  SELECT $t.*, 'I' as audit_tipo, now() as audit_creacion FROM " . $t . "
                  WHERE  id = NEW.id;
                END;
                $$
                DROP TRIGGER IF EXISTS " . $t . "_aru$$
                CREATE TRIGGER " . $t . "_aru
                AFTER UPDATE ON " . $t . "
                FOR EACH ROW
                BEGIN
                  INSERT INTO iae_audit." . $t . "
                      SELECT $t.*,'U' as audit_tipo, now() as audit_creacion FROM " . $t . "
                  WHERE  id = NEW.id;
                END;
                $$
                DELIMITER ;$$";
            echo "Create $t trigger<br/>";
            ve($s);
            Yii::app()->db->createCommand("$s")->execute(array());
        }
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
