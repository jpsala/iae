<?php

    class BancosController extends CController
    {
        public $layout = "bootstrap";
//        private $dir = __DIR__."/bancos/";
//        private $procesadosDir = __DIR__."/bancos/procesados/";
        private $procesadosDir = "/mnt/shares/zoot/bancos/procesados/";
        private $dir = "/mnt/shares/zoot/bancos/";

        //private $procesadosDir = "e:\\bancos\\procesados\\";
        //private $dir = "e:\\bancos\\";

        public function actionArchivos() {
            if(!file_exists($this->dir)) {
                throw new Exception("No existe la carpeta $this->dir");
            }
            if(!file_exists($this->procesadosDir)) {
                throw new Exception("No existe la carpeta $this->procesadosDir");
            }

            $files = array_diff(scandir($this->dir), array('..', '.', 'procesados'));
            foreach ($files as $key => $file) {
                $f                        = array();
                $tipo                     = $this->tipoBanco($file);
                $f['id']                  = uniqid();
                $f['nombre']              = $file;
                $f['tipo']                = $tipo;
                $f['summary']             = $this->summary($file, $tipo);
                $recs                     = $this->recs($file, $tipo);
                $banco = $f['summary']['banco'];
                if(!isset($recs['recs'])){
                  vd($recs);
                }
                $f['recs']                = $recs['recs'];
                $f['totalRecs']           = $recs['total'];
                $f['totalRecsOk']         = $recs['totalOk'];
                $f['totalRecsRechazados'] = $recs['totalRechazado'];
                $files[$key]              = $f;
            }
            $this->render('elijeArchivo', array('files' => $files));
        }

        private function tipoBanco($nombre) {
            if(substr($nombre, 0, 9) == 'RDEBLIQC_') {
                return 'VISA';
            } else {
                return 'Desconocido';
            }
        }

        private function summary($nombre, $tipo) {
            if($tipo == 'VISA') {
                return $this->summaryVisa($nombre);
            } else {
                return array();
            }
        }

        private function recs($nombre, $tipo) {
            if($tipo == 'VISA') {
                return $this->recsVisa($nombre);
            } else {
                return array();
            }
        }

        private function summaryVisa($nombre) {
            $ret          = array();
            $lines        = file($this->dir . $nombre);
            $header       = $lines[0];
            $banco = substr($header, 21, 8);
            $bancoNombre = ($banco == '31552136') ? 'Superville' : 'Frances';
            $footer       = $lines[count($lines) - 1];
            $totalRaw     = substr($footer, 52, 11);
            $total        = substr($totalRaw, 0, 9) . '.' . substr($totalRaw, -2);
            $totalFmt     = number_format($total, 2);
            $ret['total'] = $totalFmt;
            $fechaRaw     = substr($header, 29, 8);
            $fecha        = date('d/m/Y'/*, strtotime($fechaRaw)*/);
            $ret['fecha'] = $fecha;
            $ret['cant']  = count($lines) - 2;
            $ret['banco'] = $bancoNombre;

            return $ret;
        }

        private function recsVisa($nombre) {
            $ret   = array();
            $lines = file($this->dir . $nombre);
            array_shift($lines);
            array_pop($lines);
            $total          = 0;
            $totalRechazado = 0;
            foreach ($lines as $line) {
                $id                    = substr($line, 95, 15);
                $motivoRechazo         = substr($line, 132, 29);
                $importeRaw            = substr($line, 62, 15);
                $importe               = (substr($importeRaw, 0, 13) * 1) . '.' . substr($importeRaw, -2);
                $importeFmt            = number_format($importe, 2);
                $rec                   = Helpers::qry("
                    select d.detalle, a.matricula,
                           concat(a.apellido, ', ', a.nombre) as nombre
                      from doc d
                        inner join socio s on s.id = d.Socio_id
                        inner join alumno a on a.id = s.Alumno_id
                        where d.id = $id
                ");
                $rec['id_archivo']     = (int)$id * 1;
                $rec['motivo_rechazo'] = $motivoRechazo;
                $total += trim($motivoRechazo) == "" ? $importe : 0;
                $totalRechazado += trim($motivoRechazo) !== "" ? $importe : 0;
                $rec['importe'] = $importeFmt;
                $ret[$rec["nombre"]]          = $rec;
            }
            $totalOkFmt        = number_format($total, 2);
            $totalRechazadoFmt = number_format($totalRechazado, 2);
            $totalTotal        = $total + $totalRechazado;
            $totalFmt          = number_format($totalTotal, 2);
	$ret = record_sort($ret,"nombre");
            return array(
                'recs'           => $ret, 'totalOk' => $totalOkFmt,
                'totalRechazado' => $totalRechazadoFmt, 'total' => $totalFmt
            );
        }


        private function procesaArchivo($nombre) {
            //            if(substr($nombre,0,9) == 'RDEBLIQC_'){
            //                procesaVisa
            //            }
            //$file = readfile('/bancos/'.$nombre);
        }

        public function actionGenerarRecibos($archivo, $excluir = array(), $banco = '') {
            $tipo = $this->tipoBanco($archivo);
            if($tipo == 'VISA') {
                $this->generarRecibosVisa($archivo, $banco);
            }
        }

        private function generarRecibosVisa($archivo, $banco) {
            $excluir   = isset($_GET['excluir']) ? json_decode($_GET['excluir']) : null;
            $excluidos = array();
            foreach ($excluir as $cada) {
                $excluidos[] = $cada->id;
            }
            $retErrores   = array();
            $user_id      = Yii::app()->user->id;
//            $talonario_id = Helpers::qryScalar("
//              select t.id
//              from user u
//                  inner join puesto_trabajo p on p.id = u.puesto_trabajo_id
//                  inner join talonario t on t.punto_venta_numero = p.punto_venta_numero and t.comprob_id = 17
//              where u.id =  $user_id");

			$talonario_id = 41;
            $talonario    = Talonario::model()->findByPk($talonario_id);
            $lines        = file($this->dir . $archivo);
            $header       = $lines[0];
            $fechaRaw     = substr($header, 29, 8);
            $fecha        = date('d/m/Y'/*, strtotime($fechaRaw)*/);
            array_shift($lines);
            array_pop($lines);
            //$lines = array_slice($lines,0,1);
            $tr = Yii::app()->db->beginTransaction();
            $excluidosTexto = '';
            foreach ($lines as $line) {
                $id = (int)substr($line, 95, 15) * 1;
                if(!in_array($id, $excluidos)) {
                    $motivoRechazo = substr($line, 132, 29);
                    $importeRaw    = substr($line, 62, 15);
                    $codigoRechazo = trim(substr($line, 130, 2));
                    if($codigoRechazo == '00') {
                        $importe = (substr($importeRaw, 0, 13) * 1) . '.' . substr($importeRaw, -2);
                        $rec     = Helpers::qry("
                            select d.id, d.detalle, a.matricula, s.id as socio_id,
                                   concat(a.apellido, ', ', a.nombre) as nombre
                              from doc d
                                inner join socio s on s.id = d.Socio_id
                                inner join alumno a on a.id = s.Alumno_id
                                where d.id = $id
                              ");

                        $numero  = $talonario->numero + 1;
                        $_POST   = array(
                            'doc'          => array
                            (
                                'comprob_id'            => '17',
                                'id'                    => '',
                                'numero'                => '0001-' . $numero,
                                'fecha_valor'           => $fecha,
                                'pago-a-cuenta-detalle' => 'Generación Autom.',
                                'pago-a-cuenta-importe' => $importe,
                                'tipo'                  => '17',
                                'total'                 => $importe,
                                'saldo'                 => $importe,
                                'Socio_id'              => $rec['socio_id'],
                            ),
                            'aplicacionesAuto' => true,
                            'aplicaciones' => array
                            (
                            ),
                            'valor-id'     => '5',
                            'tipo'         => '4',
                            'fecha'        => $fecha,
                            'numero'       => '111',
                            'obs'          => 'Generado Auto.',
                            'valores'      => array
                            (
                                0 => array
                                (
                                    'Rec. Bancaria' => array
                                    (
                                        'importe'    => $importe,
                                        'tipo'       => '4',
                                        'Destino_id' => $banco == "Frances"?'9':'10',
                                        'fecha'      => $fecha,
                                        'numero'     => '111',
                                        'obs'        => '',
                                    )
                                )
                            )
                        );
                        $errores = array();
                        Doc::altaDoc($_POST, $errores, false);
                        $talonario->numero++;
                        $talonario->save();
                        if(count($errores) > 0) {
                            $retErrores[] = $errores;
                            var_export($errores);
                            var_export($rec);
                            var_export($_POST);
                            $tr->rollback();
                            die;
                        }
                    }
                }else{
                    $excluidosTexto .= $id . ',';
                }
            }
            if(count($retErrores) == 0) {
                $this->borraArchivo($archivo);
            }
            echo json_encode($retErrores);
            $tr->commit();
        }

        private function borraArchivo($archivo) {
            rename($this->dir . $archivo, $this->procesadosDir . $archivo);
        }
    }
function record_sort($records, $field, $reverse=false)
{
    $hash = array();

    foreach($records as $record)
    {
        $hash[$record[$field]] = $record;
    }

    ($reverse)? krsort($hash) : ksort($hash);

    $records = array();

    foreach($hash as $record)
    {
        $records []= $record;
    }

    return $records;
}
