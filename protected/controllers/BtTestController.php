<?php

class BtTestController extends Controller {

	public $layout = "bootstrap";
	
	public function actionAjaxMock(){
		$this->render("ajaxMock");
	}
	
	public function actionTest1() {
		$this->render("test1");
	}
	
	public function actionTodo() {
		Yii::app()->libs->add("knockout-debug");
		$this->render("todo");
	}
	
	public function actionDialogo1(){
		$this->render("dialog1");
	}
	
	public function actionFontAwesome(){
		$this->render("font-awesome");
	}
	
	public function actionJsTest1(){
		Yii::app()->libs->add("knockout");
		$this->render("jsTest1");
	}

}
