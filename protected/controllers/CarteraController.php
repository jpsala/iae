<?php

class CarteraController extends Controller
{

  public $layout = "column1";

  public function actionIndex()
  {
    $this->render("index");
  }

  public function actionTraeDatos($fecha_desde, $fecha_hasta, $en_cartera = null, $entregados = null, $agrupados = null)
  {
    $data        = new stdClass();
    $movimientos = array();
    $fecha_desde = date("Y/m/d", mystrtotime($fecha_desde));
    $fecha_hasta = date("Y/m/d", mystrtotime($fecha_hasta)) . " 23:59:59";

    $total_en_cartera = Helpers::qryScalar(" select sum(importe) as total from doc_valor dv
                              where dv.doc_valor_id is null and dv.chq_estado is null
                              and dv.fecha BETWEEN \"$fecha_desde\" and \"$fecha_hasta\"");

    if (isset($agrupados)) {

      $sql
        = "SELECT Sum(dv.importe) AS importe, dv.fecha, dv.numero, '' AS chq_origen,
            dv.chq_entregado_a, b.nombre 
            FROM doc_valor dv
            INNER JOIN banco b ON b.id = dv.banco_id
            WHERE tipo = 1 ";
      if (isset($en_cartera) && isset($entregados)) {
        $sql1 = " and (chq_estado is null or chq_estado = 1) and dv.fecha BETWEEN \"$fecha_desde\" and \"$fecha_hasta\"
                group by fecha, numero,dv.banco_id, dv.chq_entregado_a, b.nombre     
                order by fecha, numero, importe    
           ";
      } else
        if (isset($en_cartera)) {
          $sql1 = " and dv.doc_valor_id is null and dv.chq_estado is null
            and dv.fecha BETWEEN \"$fecha_desde\" and \"$fecha_hasta\"
            group by fecha, numero, dv.banco_id, dv.chq_entregado_a, b.nombre 
            order by fecha, numero, importe    
           ";
        } else
          if (isset($entregados)) {
            $sql1 = " and dv.doc_valor_id is not null and dv.chq_estado = 1
            and dv.fecha BETWEEN \"$fecha_desde\" and \"$fecha_hasta\"
            group by fecha, numero, dv.banco_id, dv.chq_entregado_a, b.nombre             
            order by fecha, numero, importe    
           ";
          }
      if (isset($sql1)) {
        $movimientos = Helpers::qryAll($sql . $sql1);
      } else {
        $sql
                     = "SELECT dv.importe, dv.fecha, dv.numero, dv.chq_origen,
            dv.chq_entregado_a, b.nombre 
            FROM doc_valor dv
            INNER JOIN banco b ON b.id = dv.banco_id
            WHERE tipo = -1 ";
        $movimientos = Helpers::qryAll($sql);
      }
    } else {
      $sql
        = "SELECT dv.importe, dv.fecha, dv.numero, dv.chq_origen,
            dv.chq_entregado_a, b.nombre 
            FROM doc_valor dv
            INNER JOIN banco b ON b.id = dv.banco_id
            WHERE tipo = 1 ";
      if (isset($en_cartera) && isset($entregados)) {
        $sql1 = " and (chq_estado is null or chq_estado = 1) and dv.fecha BETWEEN \"$fecha_desde\" and \"$fecha_hasta\"
            order by fecha, numero, importe    
           ";
      } else
        if (isset($en_cartera)) {
          $sql1 = " and dv.doc_valor_id is null and dv.chq_estado is null
            and dv.fecha BETWEEN \"$fecha_desde\" and \"$fecha_hasta\"
            order by fecha, numero, importe    
           ";
        } else
          if (isset($entregados)) {
            $sql1 = " and dv.doc_valor_id is not null and dv.chq_estado = 1
            and dv.fecha BETWEEN \"$fecha_desde\" and \"$fecha_hasta\"
            order by fecha, numero, importe    
           ";
          }
      if (isset($sql1)) {
        $movimientos = Helpers::qryAll($sql . $sql1);
      } else {
        $sql
                     = "SELECT dv.importe, dv.fecha, dv.numero, dv.chq_origen,
            dv.chq_entregado_a, b.nombre 
            FROM doc_valor dv
            INNER JOIN banco b ON b.id = dv.banco_id
            WHERE tipo = -1 ";
        $movimientos = Helpers::qryAll($sql);
      }

    }
    $movimientosTable  = $this->renderPartial("movimientos", array("movimientos" => $movimientos, "total_en_cartera" => $total_en_cartera), true);
    $data->movimientos = $movimientosTable;
    echo json_encode($data);
  }

}

?>
