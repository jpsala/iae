<?php

class ChequeraController extends Controller {

  public $layout = "column1";

  public function filters() {
    return array(
        'accessControl', // perform access control for CRUD operations
        'rights',
    );
  }


  public function actionChequera($id = null) {
    $model = Chequera::model()->findByPk($id);
    if (!$model) {
      $model = new Chequera();
    }
    $this->render('chequeraIndex', array('model' => $model));
  }

  public function actionChequeraGraba() {

    $tran = Yii::app()->db->beginTransaction();

    $model = Chequera::model()->findByPk($_POST['id']);

    if (!$model) {
      $model = new Chequera();
      $model->numero = Chequera::getNumeroChequera($_POST['destino_id']);
      $operacion = "Generada";
    }else{
      $operacion = "Modificada";  
    }
    
    $model->attributes = $_POST;
    $model->descripcion = ucwords(strtolower($_POST['descripcion']));
    $model->primer_numero = $_POST['p_numero'];
    $model->siguiente_numero = $_POST['s_numero'];
    $model->ultimo_numero = $_POST['u_numero'];
    
    if (isset($_POST['diferido']))
      $model->diferido = 1;
    else
      $model->diferido = 0;
    if (!$model->save()) {
      $tran->rollback();
      $arr['err'] = $model->getErrors();
      echo json_encode($arr);
      Yii::app()->end();
    }
    $tran->commit();

    $arr['obs'] = "Chequera $model->numero $operacion";
     echo json_encode($arr);
  }

  public function actionChequeraForm($id) {
    $model = Chequera::model()->findByPk($id);
    if (!$model) {
      $model = new Chequera();
    }
    $this->renderPartial('_chequeraForm', array('model' => $model));
  }

  public function actionChequeraBorra($id) {
    $cenuso = DocValor::model()->count("chequera_id = $id");
    if ($cenuso > 0) {
      echo('La chequera se encuentra en uso. No es posible eliminarla');
    } else {
      $tran = Yii::app()->db->beginTransaction();
      try {
        Chequera::model()->deleteAll("id=$id");
        $tran->commit();
      } catch (Exception $e) {
        echo('Error al eliminar la Chequera. Probablemente se encuentre en uso ' . $e->getMessage());
        $tran->rollBack();
      }
    }
  }


}

?>
