<?php

class CicloController extends Controller {

  public $layout = "nuevo";

	public function actionIndex(){
		$this->pageTitle = "Ciclos por defecto";
		$this->render("index");
	}
	
	public function actionGraba(){
		$ret = new stdClass();
		$user_id = Yii::app()->user->id;
		$ciclo_id_para_informes_educativo = $_POST['ciclo_id_para_informes_educativo'];
		$ciclo_id_para_informes_admin = $_POST['ciclo_id_para_informes_admin'];
		$ciclo_id_actual = Ciclo::getActivoId();
//		if($ciclo_id_actual === $ciclo_id_para_informes){
//			Opcion::deleteOpcion('ciclo_id', "educativoInformes");
//		}else{
//		}
			Opcion::saveOpcion('ciclo_id', $ciclo_id_para_informes_educativo, "educativoInformes");
			Opcion::saveOpcion('ciclo_id', $ciclo_id_para_informes_admin, "adminInformes");
		$ret->status = 'ok';
		echo json_encode($ret);
	}

}

?>
