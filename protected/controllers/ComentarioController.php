<?php

class ComentarioController extends Controller {

  public $indent = -1;
  public $comments = "";

  public function actionGeneral() {
      $this->render("general");
  }
  
  public function actionIndex($partial = false, $contexto = null) {
    $this->getComentarios(null, $contexto);
    if ($partial) {
      $this->renderPartial("comments", array("comentarios" => $this->comments, "contexto" => $contexto));
    } else {
      $this->render("comments", array("comentarios" => $this->comments, "contexto" => $contexto));
    }
  }

  public function actionElimina() {
    $comentario_id = $_POST["comentario_id"];
    $c = Comentario::model()->findByPk($comentario_id);
    $c->delete();
  }

  public function actionGraba() {
    $texto = html_entity_decode($_POST["texto"]);
    $comentario_id = $_POST["comentario_id"];
    $comentario_parent_id = $_POST["comentario_parent_id"] ? $_POST["comentario_parent_id"] : null;
    $contexto = $_POST["contexto"];
    $estado_id = $_POST["estado_id"] == "null" ? null : $_POST["estado_id"];
    if ($comentario_id != "nuevo") {
      $c = Comentario::model()->find("id =$comentario_id");
    } else {
      $c = new Comentario();
      $c->user_id = Yii::app()->user->id;
    }
    $c->comentario = $texto;
    $c->Comentario_id = $comentario_parent_id;
    $c->contexto = $contexto;
    $c->estado_id = $estado_id;
    if(!$c->save()){
      Helpers::error(json_encode($c->errors));
    }
    $this->renderPartial("comment", array("comentario" => $c));
    
//    Helpers::envia_mail('smtp', 'mail.iae.esc.edu.ar', null, 'jpsala', 'lani0363', 'Nuevo comentario publicado',  "Comentario publicado por ". Yii::app()->user->name. ". \n  $texto", 'sistemas@gralifer.com', 'octaviomassone@gmail.com');
    Helpers::envia_mail('sendmail', '/usr/sbin/sendmail -t', null, 'jpsala', 'lani0363', 'Nuevo comentario publicado',  "Comentario publicado por ". Yii::app()->user->name. ". \n  $texto", 'jpsala@localhost',"jpsalamdq@gmail.com");//, 'octaviomassone@gmail.com');
 }  //Helpers::envia_mail('smtp', 'mail.gralifer.com', '25', '', '', 'Nuevo comentario publicado',  "Comentario publicado por ". Yii::app()->user->name. ". \n  $texto", 'sistemas@gralifer.com', 'octaviomassone@gmail.com');

  public function getComentarios($comentario_id = null, $contexto = null) {
    $condition = $comentario_id ? "t.Comentario_id = $comentario_id" : "Comentario_id is null";
    $condition .= $contexto ? " and contexto = \"$contexto\"" : " and true";
    $order = $comentario_id ? "fecha_modificacion" : "fecha_modificacion desc";
    $comentarios = Comentario::model()->with(array("estado", "user"))->findAll(array("condition" => $condition, "order" => $order));
    $this->comments.="<div class='indent'>";
    foreach ($comentarios as $comentario) {
      $this->comments .= $this->renderPartial("comment", array("comentario" => $comentario), true);
      $this->getComentarios($comentario->id, $contexto);
    }
    $this->comments.="</div>";
  }

  public function indentSpan($cant = 0) {
    $ret = "";
    for ($index = 0; $index < $cant; $index++) {
      $ret.="<span class=\"indent\">x</span>";
    }
    return $ret;
  }

}