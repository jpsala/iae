<?php

class ConceptoController extends Controller {

  public $layout = "column1";

  public function actionChgNode() {
    $r = new stdClass();
    switch ($_POST["operacion"]) {
      case "move_node":
        $co = Concepto::model()->findByPk($_POST["origen_id"]);
        $co->concepto_id = $_POST["destino_id"] == -1 ? null : $_POST["destino_id"];
        $co->orden = $_POST["orden"];
        if ($co->save()) {
          echo "ok";
        } else {
          ve($co->errors);
        }
        break;

      case "rename_node":
        $co = Concepto::model()->findByPk($_POST["origen_id"]);
        $co->nombre = $_POST["nombre"];
        if ($co->save()) {
          echo "ok";
        } else {
          ve($co->errors);
        }
        break;


      case "remove_node":
        $cant = Doc::model()->count("concepto_id = " . $_POST["concepto_id"]);
        if ($cant > 0) {
          $r->errors = "Hay documentos con ese concepto, no se puede borrar!";
          echo json_encode($r);
        } else {
          $co = Concepto::model()->findByPk($_POST["concepto_id"]);
          if ($co->delete()) {
            echo "ok";
          } else {
            $r->errors = $co->errors;
            json_encode($r);
          }
        }
        break;

      case "create_node":
        $co = new Concepto();
        $co->concepto_id = $_POST["destino_id"];
        $co->nombre = $_POST["nombre"];
        if ($co->save()) {
          $r->id = $co->id;
          echo json_encode($r);
        } else {
          $r->errors = $co->errors;
          echo json_encode($r);
        }
        break;

      default:
        break;
    }
  }

	public function actionMover(){
		$this->layout = "nuevo";
		$this->render("mover");
	}
	
	public function actionMoverAjax($src_id, $tgt_id){
		//$sBck = "update doc d set d.concepto_id_ant = d.concepto_id where d.concepto_id = $src_id";
		//Helpers::qryExec($sBck);
		$s = "update doc d set d.concepto_id = $tgt_id where d.concepto_id = $src_id";
		$ret = Helpers::qryExec($s);
		$c = "delete from concepto where concepto_id = $src_id; delete from concepto where id = $src_id;";
        Helpers::qryExec($c);
        $cant = "select count(*) from doc where concepto_id = $tgt_id";
        $ret = Helpers::qryScalar($cant);
		echo $ret;
	}
}
