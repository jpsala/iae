<?php

	class ConciliacionController extends Controller {

		public $layout = "column1";

	public function actionIndex() {
			$cajaActivaId = Destino::getCajaInstanciaActiva()->Destino_id;
			$destinos = Destino::model()->findAll("tipo = 2 and id <> $cajaActivaId");
			$destinoListData = "";
			foreach ($destinos as $d) {
				$destinoListData .= "<option value=\"$d->id\">$d->nombre</option>";
			}
			$this->render("index", array("destinoListData" => $destinoListData));
		}

		public function actionTraeDatos($destino_id) {
			$saldoAnt = Helpers::qryScalar("
					select total
							from conciliacion c
							where c.destino_id = $destino_id and c.estado = 1
							 order by numero desc
							 limit 1
			");
			$conciliaciones = Helpers::qryAll("
					select *
							from conciliacion c
							where c.destino_id = $destino_id and c.estado = 0
			");
			//vd($destino_id);
			$movimientos = array();

			$total = "";
			$conciliacion_id = "";
			if (count($conciliaciones) > 1) {
				throw new Exception("Hay mas de una conciliación abierta para esa cuenta");
			} elseif (count($conciliaciones) == 1) {
				$conciliacion_id = $conciliaciones[0]["id"];
				$select = "
					select IF((dv.tipo=3 or dv.tipo=6),dv.fecha,d.fecha_creacion) as fecha1,  d.fecha_creacion, d.detalle, c.signo_banco,
							concat(LPAD(d.sucursal, 4, '0'),'-',LPAD(d.numero, 8, '0'))  as docnumero, c.abreviacion,  dv.*
					from doc_valor dv
							inner join doc d on d.id = dv.Doc_id
							inner join talonario t on t.id = d.talonario_id
							inner join comprob c on c.id = t.comprob_id
					where d.anulado = 0 and ( dv.conciliacion_id = $conciliacion_id or dv.conciliacion_id is null or dv.conciliacion_id = '') and dv.destino_id = $destino_id
					order by fecha1
				";
				//vd($select);
				$movimientos = Helpers::qryAll($select);
				$numero = $conciliaciones[0]["numero"];
				$total = $conciliaciones[0]["total"];
			} else {
				$numero = $this->actionNumeroConciliacion($destino_id);
				$select = "
					select IF((dv.tipo=3 or dv.tipo=6),dv.fecha,d.fecha_creacion) as fecha1, d.fecha_creacion, d.detalle, c.signo_banco,
							concat(LPAD(d.sucursal, 4, '0'),'-',LPAD(d.numero, 8, '0')) as docnumero, c.abreviacion, dv.*
					from doc_valor dv
						inner join doc d on d.id = dv.Doc_id
						inner join talonario t on t.id = d.talonario_id
						inner join comprob c on c.id = t.comprob_id
					where d.anulado = 0 and dv.conciliacion_id is null and dv.destino_id = $destino_id
					order by fecha1
				";
				//vd($select);
				$movimientos = Helpers::qryAll($select);
			}
			$movimientosTable = $this->renderPartial("movimientos", array("movimientos" => $movimientos), true);
			$data = new stdClass();
			$data->movimientos = $movimientosTable;
			$data->numero = $numero;
			$data->total = $total;
			$data->fecha = date("d/m/Y", time());
			$data->conciliacion_id = $conciliacion_id;
			$data->saldo = $saldoAnt ? $saldoAnt : "0.00";
			echo json_encode($data);
		}

		public function actionNumeroConciliacion($destino_id) {
			$numero = Helpers::qryScalar("
            select max(numero)
                from conciliacion c
                where c.destino_id = $destino_id


        ");

			return $numero + 1;
		}

		public function actionGraba() {
			$ret = new stdClass();
			$ret->errores = array();
			$tr = Yii::app()->db->beginTransaction();
			$concilia = $_POST["concilia"] === 'true' ? 1 : 0;
			$doc = $_POST["doc"];
			$conciliacion_id = $doc["conciliacion_id"];
			$movimientos = $_POST["movimientos"];
//			$tr->rollback();
//			var_dump($movimientos);die;
			if ($conciliacion_id) {
				$conciliacion = Conciliacion::model()->findByPk($conciliacion_id);
			} else {
				$conciliacion = new Conciliacion();
				$conciliacion->fecha = Helpers::fechaParaGrabar(date("d/m/Y", time()));
				$conciliacion->user_id = Yii::app()->user->id;
			}
			$conciliacion->setAttributes($doc);
			$conciliacion->estado = $concilia;
			if (!$conciliacion->save()) {
				$ret->errorres["Conciliacion"] = $conciliacion->errors;
			} else {
				$conciliacion_id = $conciliacion->id;
			}

			if (count($ret->errores) == 0) {
				foreach ($movimientos as $id => $checked) {
					$row = DocValor::model()->findByPk($id);
					if (!$row) {
						$ret->errorres["Busca Doc"] = "Doc #$row->id No encontrado";
					}
					$row->conciliacion_id = ($checked ? $conciliacion_id : null);

					if (!$row->save()) {
						vd($row->errors,$row);
						$ret->errorres["Graba Doc"] = $row->errors;
					}
					if($id == 74490){
//						$tr->commit();
//						var_dump($row->attributes);
//						die;
					}
				}
			}

			if (count($ret->errores) == 0) {
				$tr->commit();
			}else{
				$tr->rollback();
			}
			echo json_encode($ret);
		}

	}

?>
