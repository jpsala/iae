<?php

class ConductaController extends Controller {

    public function actionIndex() {
        $this->render('admin', array("fecha_hoy" => date("d/m/Y", time())));
    }

    public function actionTraeItems($alumno_id, $logica_periodo_id) {
        /* @var $alumno Alumno */
        $alumno = Alumno::model()->findByPk($alumno_id);
        $alumno_division_id = $alumno->alumnoDivisionActiva->id;
        $conducta = Conducta::model()->find(
                "logica_periodo_id=\"$logica_periodo_id\" and alumno_division_id = \"$alumno_division_id\""
        );
        $items = (isset($conducta)) ? $conducta->conductaDetalles : array();
        $this->renderPartial("_items", array("items" => $items));
    }

    public function actionGrabaItem() {
        /*
          alumno_id:alumno_id,
          logica_periodo_id:logica_periodo_id,
          item_id:item_id,
          fecha:$("#fecha-item").val(),
          cantidad:$("#cantidad-item").val(),
          detalle:$("#detalle-item").val()
         */ if ($_POST[
                "item_id"]) {
            $conductaDetalle = ConductaDetalle::model()->findByPk($_POST["item_id"]);
        }
        if (!$conductaDetalle) {
            $alumno = Alumno::model()->findByPk($_POST["alumno_id"]);
            $alumno_division_id = $alumno->alumnoDivisionActivaId;
            $logica_periodo_id = $_POST["logica_periodo_id"];
            $conducta = Conducta::model()->find(
                    "logica_periodo_id=\"$logica_periodo_id\" and alumno_division_id = \"$alumno_division_id\"");
            if (!$conducta) {
                $conducta = new Conducta();
                $conducta->logica_periodo_id = $logica_periodo_id;
                $conducta->Alumno_Division_id = $alumno_division_id;
                $conducta->save();
            }
            $conductaDetalle = new ConductaDetalle();
            $conductaDetalle->conducta_id = $conducta->id;
        }
        $conductaDetalle->fecha = Helpers::fechaParaGrabar($_POST["fecha"]);
        $conductaDetalle->cantidad = $_POST["cantidad"];
        $conductaDetalle->detalle = $_POST["detalle"];
        $ret = new stdClass();
        if (!$conductaDetalle->save()) {
            $ret->errors = $conductaDetalle->errors;
            echo json_encode($ret);
            die;
        }
        $ret->id = $conductaDetalle->id;

        echo json_encode($ret);
    }

    public function actionBorraItem() {
        $cd = ConductaDetalle::model()->findByPk($_POST["item_id"]);
        $cd->delete();
    }

    public function actionCargaConducta() {
        $this->pageTitle = "Carga de conducta";
        $this->render("cargaConducta"); //, array("notas" => $notas, "logicaItems" => $logicaItems, "fechaPeriodo" => date("2012-01-02"), "divisionAsignatura" => $divisionAsignatura, "asignatura"=>$asignatura,"logicaPeriodo"=>$logicaPeriodo));
    }

    public function actionCargaConductaAjax($division_id, $logica_periodo_id = null) {
        $conductaParaCarga = Conducta::getConductaParaCarga($division_id);
        $this->renderPartial("cargaConductaAjax", array(
                "conductas" => $conductaParaCarga,
                "logica_periodo_id" => $logica_periodo_id,
                "trae_periodos_con_conducta" => false,
                )
        );
    }

    public function actionCargaConductaGraba() {
        if (isset($_POST["conducta"])) {
            $ret = new stdClass();
            $errors = null;
            $division_id = $_POST["division_id"];
            $ciclo_id = Ciclo::getActivoId();
            $tr = Yii::app()->db->beginTransaction();
            foreach ($_POST["conducta"] as $alumno_id => $conducta) {
                if ($conducta){
                    foreach ($conducta as $key => $val) {
                        $parts = explode(":", $key);
                        $logica_periodo_id = $parts[0];
                        $conducta_id = $parts[1];
                        if ($conducta_id) {
                            $conducta = Conducta::model()->findByPk($conducta_id);
                            if (!$val) {
                                $conducta->delete();
                                continue;
                            }
                        } else {
                            $alumno_division_id = Helpers::qryScalar("
							  select ad.id
								from alumno_division ad
								where ad.Division_id = $division_id and
									  ad.Ciclo_id = $ciclo_id and
									  ad.alumno_id = $alumno_id and 
									 (ad.activo or ad.promocionado or ad.egresado)
							");
                            $conducta = Conducta::model()->find(
											"alumno_division_id = $alumno_division_id and
							   logica_periodo_id = $logica_periodo_id
							  "
                            );
                            if ($conducta) {
                                $ret->encontrados[] = $conducta->attributes;
                                if (!$val) {
                                    $conducta->delete();
                                    continue;
                                }
                            } else if (!$val) {
                                continue;
                            } else {
                                $conducta = new Conducta();
                                $conducta->Alumno_Division_id = $alumno_division_id;
                                $conducta->logica_periodo_id = $logica_periodo_id;
                            }
                        }
                        $conducta->conducta = $val;
                        if (!$conducta->save()) {
                            $errors = $conducta->errors;
                            exit;
                        }
                    }
				}
            }
            if ($errors) {
                $ret->errors = $errors;
                $tr->rollback();
            } else {
                $tr->commit();
            }
            echo json_encode($ret);
        }
    }

    public function actionDesempenio() {
        $this->render("/conducta/genNiAnDi"
                , array(
                "fechas" => false,
                "reporteNombre" => "desempenioAjax",
                "ajax" => true,
                "print" => false,
        ));
    }

    public function actionDesempenioAjax($nivel_id,  $anio_id,  $division_id) {
        $this->renderPartial("desempenioAjax", $_GET);
    }
    
    public function actionDesempenioGraba(){
        foreach ($_POST["desempenio"] as $key => $value) {
            $d = Desempenio::model()->find("alumno_division_id=$key");
            if(!$d){
                $d = new Desempenio();
                $d->alumno_division_id = $key;
            }
            $d->desempenio = strtoupper($value);
            if(!$d->save()){
                vd($d->errors);
            }
        }
    }

}