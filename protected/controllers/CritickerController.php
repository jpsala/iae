<?php

class CritickerController extends Controller {

    function actionGetScore() {
        $score = Yii::app()->ck->createCommand("select valor from opcion where opcion = \"last_score\"")->queryScalar();
        echo $_GET['callback'] . '(' . json_encode(array("score" => $score)) . ")";
    }

    function actionDelPeli($film_id, $login) {
        echo jsonp(array("del"=>Yii::app()->ck->createCommand(
                                "delete from pelicula where criticker_id = $film_id and login = \"$login\"")
                        ->execute()));
    }

    function actionSetScore() {
        echo jsonp(array("chg" => Yii::app()->ck->createCommand(
                        "update opcion 
                            set valor=:score 
                            where opcion = :opcion
        ")->execute(array("opcion" => "last_score", "score" => $_GET["score"]))));
    }

    function actionPelis() {
//         echo jsonp(array("hola"=>"chau"));
//         die;
        $user = $_GET["user"];
        $login = $_GET["login"];
        $tr = Yii::app()->ck->beginTransaction();
        $cantAct = 0;
        $cantIns = 0;
        foreach ($_GET["peli"] as $key => $peli) {
            $key = trim($key);
            $id = Yii::app()->ck
                    ->createCommand("select id from pelicula where criticker_id = $key and user = \"$user\"")
                    ->queryScalar();
            if ($id) {
                if ($peli["borra"] == 0) {
                    $cantAct += Yii::app()->ck
                            ->createCommand("update pelicula set criticker_id = :criticker_id, nombre = :nombre , score = :score, login = :login where id = :id")
                            ->execute(array("criticker_id" => $key, "nombre" => utf8_encode($peli["nombre"]), "score" => $peli["score"], "login" => $login, "id" => $id));
                } else {
                    $cantAct += Yii::app()->ck
                            ->createCommand("delete from pelicula where id = :id")
                            ->execute(array("id" => $id));
                }
            } else {
                $cantIns += Yii::app()->ck
                        ->createCommand("insert into pelicula(criticker_id,nombre,score,user,login) values (:criticker_id,:nombre,:score,:user, :login)")
                        ->execute(array("criticker_id" => $key, "nombre" => utf8_encode($peli["nombre"]), "score" => $peli["score"], "user" => $user, "login" => $login));
            }
        }
        $tr->commit();
        echo jsonp(array("Upd" => $cantAct, "Ins" => $cantIns));
    }

    function actionGetPelis() {
        $pelis = Yii::app()->ck
                ->createCommand("select * from pelicula")
                ->queryAll();
        echo jsonp($pelis);
    }

    public function actionGetDist($login) {
        $pelis = Yii::app()->ck
                ->createCommand(
                        "SELECT CAST(score AS UNSIGNED) score, count(*) as cant FROM pelicula 
                          where  login = \"$login\" group by score order by 1 desc")
                ->queryAll();
        $data = Yii::app()->ck
                ->createCommand(
                        "SELECT sum(score) as total, count(*) as cant FROM pelicula 
                          where  login = \"$login\"")
                ->queryAll();
        $cant = 0;
        $total = 0;
        $html = "<span id=\"dist\" style=\"color:rgb(254,100,100);font-size: 12px;float:left;background-color:black;padding:2px 2px 2px 3px\">";
        foreach ($pelis as $p) {
            $cant ++;
            $total += $p["cant"];
            $score = $p["score"];
            $html .="<span style=\";background-color: rgb(80,80,80);float: left;margin-right: 1px;padding-right: 2px;text-align: center\"><a style=\"cursor:pointer\" onclick=\"return scoreChangeAbs($score)\">". $p["score"] . "</a><br/>" . $p["cant"] . "</span>";
        }
        $html .= "</span>";
        $ret["html"] = $html;
        $ret["promedio"] = $cant > 0 ? (round($total / $cant, 0)) : 0;
        echo jsonp($ret);
    }

}

function jsonp($data) {
    //return $_GET['callback'] . '(' . json_encode($data) . ")";
    return (isset($_GET['callback']) ? $_GET['callback'] : "noHayCallback") . '(' . json_encode($data) . ")";
}
