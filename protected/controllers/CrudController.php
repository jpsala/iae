<?php

	class CrudController extends Controller {

		public $layout = "column1";

		public function filters() {
			return array(
					'accessControl', // perform access control for CRUD operations
					'rights',
			);
		}

		public function actionDestino($id = null) {
			$model = Destino::model()->findByPk($id);
			if (!$model) {
				$model = new Destino();
			}
			$this->render('destinoIndex', array('model' => $model));
		}

		public function actionDestinoGraba() {

			$tran = Yii::app()->db->beginTransaction();

			$model = Destino::model()->findByPk($_POST['id']);

			if (!$model) {
				$model = new Destino();
			}

			$model->attributes = $_POST;

			if (!$model->save()) {
				$tran->rollback();
				echo json_encode($model->getErrors());
				Yii::app()->end();
			}
			$tran->commit();
		}

		public function actionDestinoForm($id) {
			$model = Destino::model()->findByPk($id);
			if (!$model) {
				$model = new Destino();
			}
			$this->renderPartial('_destinoForm', array('model' => $model));
		}

		public function actionDestinoBorra($id) {
			Destino::model()->deleteAll("id=$id");
		}

		public function actionBanco($id = null) {
			$model = Banco::model()->findByPk($id);
			if (!$model) {
				$model = new Banco();
			}
			$this->render('bancoIndex', array('model' => $model));
		}

		public function actionBancoGraba() {

			$tran = Yii::app()->db->beginTransaction();

			$model = Banco::model()->findByPk($_POST['id']);

			if (!$model) {
				$model = new Banco();
			}

			$model->attributes = $_POST;
			//$model->validate();
			//var_dump($model->errors);die;
			$model->nombre = ucwords(strtolower($_POST['nombre']));
			//Si no lo asigno explicitamente no lo graba ¿?
			$model->codigo = $_POST['codigo'];
			if (!$model->save()) {
				$tran->rollback();
				echo json_encode($model->getErrors());
				Yii::app()->end();
			}
			$tran->commit();
		}

		public function actionBancoForm($id) {
			$model = Banco::model()->findByPk($id);
			if (!$model) {
				$model = new Banco();
			}
			$this->renderPartial('_bancoForm', array('model' => $model));
		}

		public function actionBancoBorra($id) {
			Banco::model()->deleteAll("id=$id");
		}

		public function actionTraeLocalidadesPorPais($pais_id) {
			$x = array('prompt' => '');
			echo CHtml::listOptions(null, CHtml::listData(Localidad::model()->findAll("Pais_id=$pais_id"), 'id', 'nombre'), $x);
		}

		public function actionTraeProvinciasPorPais($pais_id) {
			$x = array('prompt' => '');
			echo CHtml::listOptions(null, CHtml::listData(Provincia::model()->findAll("Pais_id=$pais_id"), 'id', 'nombre'), $x);
		}

		public function actionTraeLocalidadesPorProvincia($provincia_id) {
			$x = array('prompt' => '');
			echo CHtml::listOptions(null, CHtml::listData(Localidad::model()->findAll("provincia_id=$provincia_id"), 'id', 'nombre'), $x);
		}

		public function actionComprob($id = null) {
			$model = Comprob::model()->findByPk($id);
			if (!$model) {
				$model = new Comprob();
			}
			$this->render('comprobIndex', array('model' => $model));
		}

		public function actionComprobGraba() {

			$tran = Yii::app()->db->beginTransaction();

			$model = Comprob::model()->findByPk($_POST['id']);

			if (!$model) {
				$model = new Comprob();
			}

			$model->attributes = $_POST;
			$model->nombre = ucwords(strtolower($_POST['nombre']));

			if (!$model->save()) {
				$tran->rollback();
				echo json_encode($model->getErrors());
				Yii::app()->end();
			}
			$tran->commit();
		}

		public function actionComprobForm($id) {
			$model = Comprob::model()->findByPk($id);
			if (!$model) {
				$model = new Comprob();
			}
			$this->renderPartial('_comprobForm', array('model' => $model));
		}

		public function actionComprobBorra($id) {
			$cenuso = ComprobPuntoVenta::model()->count("Comprob_id = $id");
			if ($cenuso > 0) {
				echo('El Comprobante se encuentra en uso. No es posible eliminarlo');
			} else {
				$tran = Yii::app()->db->beginTransaction();
				try {
					Comprob::model()->deleteAll("id=$id");
					$tran->commit();
				} catch (Exception $e) {
					echo('Error al eliminar el Comprobante. Probablemente se encuentre en uso ' . $e->getMessage());
					$tran->rollBack();
				}
			}
		}

		public function actionTalonario($id = null) {
			$model = Talonario::model()->findByPk($id);
			if (!$model) {
				$model = new Talonario();
			}
			$this->render('talonarioIndex', array('model' => $model));
		}

		public function actionTalonarioGraba() {

			$tran = Yii::app()->db->beginTransaction();
			$model = Talonario::model()->findByPk($_POST['id']);

			if (!$model) {
				$model = new Talonario();
			}

			$model->attributes = $_POST;
			$letra = strtoupper($_POST['letra']);
			$model->letra = $letra;
			if (isset($_POST['manual']))
				$model->numeracion_manual = 1;
			else
				$model->numeracion_manual = 0;
			if (isset($_POST['activo']))
				$model->activo = 1;
			else
				$model->activo = 0;
			$model->comprob_id = $_POST['Comprob_id'];
			$model->punto_venta_numero = $_POST['punto_venta'];
			if (!$model->save()) {
				$tran->rollback();
				echo json_encode($model->getErrors());
				Yii::app()->end();
			}
			$tran->commit();
		}

		public function actionTalonarioForm($id) {
			$model = Talonario::model()->findByPk($id);
			if (!$model) {
				$model = new Talonario();
			}
			$this->renderPartial('_talonarioForm', array('model' => $model));
		}

		public function actionTalonarioBorra($id) {
			$talonarioenuso = Doc::model()->count("talonario_id = $id");
			if ($talonarioenuso > 0) {
				echo('El Talonario se encuentra en uso. No es posible eliminarlo');
			} else {
				$tran = Yii::app()->db->beginTransaction();
				try {
					Talonario::model()->deleteAll("id=$id");
					$tran->commit();
				} catch (Exception $e) {
					echo('Error al eliminar el Talonario. Probablemente se encuentre en uso ' . $e->getMessage());
					$tran->rollBack();
				}
			}
		}

		public function actionNivel($id = null) {
			$model = Nivel::model()->findByPk($id);
			if (!$model) {
				$model = new Nivel();
				vd($model->attributes);
			}
			$this->render('nivelIndex', array('model' => $model));
		}

		public function actionNivelGraba() {
			$tran = Yii::app()->db->beginTransaction();

			$model = Nivel::model()->findByPk($_POST['id']);

			if (!$model) {
				$model = new Nivel();
			}

			$model->attributes = $_POST;
			$model->Logica_id = $_POST['logica_id'];
			if (!$model->save()) {
				$tran->rollback();
				echo json_encode($model->getErrors());
				Yii::app()->end();
			}
			$tran->commit();
		}

		public function actionNivelForm($id) {

			$model = Nivel::model()->findByPk($id);
			if (!$model) {
				$model = new Nivel();
			}
			$this->renderPartial('_nivelForm', array('model' => $model));
		}

		public function actionNivelBorra($id) {
			Nivel::model()->deleteAll("id=$id");
		}

		public function actionTraeDivisionesPorAnio() {
			// $division = Division::model()->findAll("Anio_id = $id");
			//$this->actionNivelForm(2); 
			$this->renderPartial('_nivelDivision');
		}

		public function actionAnio($id = null, $idNivel) {
			$model = Anio::model()->findByPk($id);
			if (!$model) {
				$model = new Anio();
			}
			//$niveles = Anio::model()->findAll();
			$this->render('anioIndex', array('anios' => $model, 'idNivel' => $idNivel));
		}

		public function actionAnioGraba($idanio = null, $idnivel, $nombre_anio, $orden, $activo) {
			$r = null;
			$r->nuevo = false;
			$tran = Yii::app()->db->beginTransaction();
			$model = Anio::model()->findByPk($idanio);
			if (!$model) {
				$model = new Anio();
				$r->nuevo = true;
			}
			$model->Nivel_id = $idnivel;
			$model->nombre = $nombre_anio;
			$model->orden = $orden;
			$model->activo = $activo;
			if (!$model->save()) {
				$tran->rollback();
				echo json_encode($model->getErrors());
				Yii::app()->end();
			}
			$tran->commit();
			$r->id = $model->id;
			echo json_encode($r);
		}

		public function actionAniosporNivel($id = null) {
			$anios = Anio::model()->findAll(array("condition" => "Nivel_id=$id", "order" => "orden"));
			$model = Anio::model()->findByPk($anios[0]->id);
			if (!$model) {
				$model = new Anio();
			}

			// $this->renderPartial('_anioForm', array('model' => $model));
			//var_dump($anios[0]->attributes);die();
			$this->renderPartial('_anioForm', array('model' => $model, 'anios' => $anios));
		}

		public function actionAnioForm1($id) {

			$model = Anio::model()->findByPk($id);
			if (!$model) {
				$model = new Anio();
			}

			$division = Division::model()->findAll("Anio_id = $id ");


			$this->renderPartial('_anioForm', array('anios' => $model, 'division' => $division));
		}

		public function actionAnioBorra($id) {
			$tanioenuso = Division::model()->count("Anio_id = $id");
			if ($tanioenuso > 0) {
				echo('El Año tiene divisiones vinculadas. No es posible eliminarlo');
			} else {
				$tran = Yii::app()->db->beginTransaction();

				try {
					Anio::model()->deleteAll("id=$id");
					$tran->commit();
				} catch (Exception $e) {
					echo('Error al eliminar el Año. Probablemente se encuentre en uso ' . $e->getMessage());
					$tran->rollBack();
				}
			}
		}

		public function actionTarjeta($id = null) {
			$model = Tarjeta::model()->findByPk($id);
			if (!$model) {
				$model = new Tarjeta();
			}
			$this->render('tarjetaIndex', array('model' => $model));
		}

		public function actionTarjetaGraba() {

			$tran = Yii::app()->db->beginTransaction();

			$model = Tarjeta::model()->findByPk($_POST['id']);

			if (!$model) {
				$model = new Tarjeta();
			}

			$model->attributes = $_POST;
			$model->nombre = ucwords(strtolower($_POST['nombre']));



			if (!$model->save()) {
				$tran->rollback();
				echo json_encode($model->getErrors());
				Yii::app()->end();
			}
			$tran->commit();
		}

		public function actionTarjetaForm($id) {
			$model = Tarjeta::model()->findByPk($id);
			if (!$model) {
				$model = new Tarjeta();
			}
			$this->renderPartial('_tarjetaForm', array('model' => $model));
		}

		public function actionTarjetaBorra($id) {
			$tarjenuso = TarjetaPlan::model()->count("tarjeta_id = $id");
			if ($tarjenuso > 0) {
				echo('La tarjeta tiene planes vinculados. No es posible eliminarla');
			} else {
				$tran = Yii::app()->db->beginTransaction();
				try {
					Tarjeta::model()->deleteAll("id=$id");
					$tran->commit();
				} catch (Exception $e) {
					echo('Error al eliminar la Tarjeta. Probablemente se encuentre en uso ' . $e->getMessage());
					$tran->rollBack();
				}
			}
		}

		public function actionAdminPlanes($tarjeta_id) {
			$planes = TarjetaPlan::model()->findAll("tarjeta_id = $tarjeta_id");
			$this->renderPartial("/plantarjeta/_adminPlanes", array("tarjeta_id" => $tarjeta_id, "planes" => $planes));
		}

		public function actionAdminPlan() {
			$this->pageTitle = "Administración de planes de tarjetas de crédito";
			$this->render("/plantarjeta/planes");
		}

		public function actionAdminPlanesGraba() {
			$ret = null;
			$ret->nuevo = false;
			$plan = TarjetaPlan::model()->findByPk($_POST["plan_id"]);
			if (!$plan) {
				$plan = new TarjetaPlan();
				$ret->nuevo = true;
			}
			$plan->attributes = $_POST;
			$plan->cuotas = $_POST['cuota'];
			$plan->activo = 1;
			if (!$plan->save()) {
				$ret->error = $plan->errors;
			} else {
				$ret->plan_id = $plan->id;
			}
			echo json_encode($ret);
		}

		public function actionAdminPlanesBorra($plan_id) {
			$planenuso = DocValorTarjeta::model()->count("tarjeta_plan_id = $plan_id");
			if ($planenuso > 0) {
				echo('El plan se encuentra en uso. No es posible eliminarlo');
			} else {
				$plan = TarjetaPlan::model()->findByPk($plan_id);
				if (!$plan->delete()) {
					echo json_encode($plan->errors);
				}
			}
		}

		public function actionInasisTipo($id = null) {
			$model = InasistenciaTipo::model()->findByPk($id);
			if (!$model) {
				$model = new InasistenciaTipo();
			}
			$this->render('inasistipoIndex', array('model' => $model));
		}

		public function actionInasisTipoGraba() {

			$tran = Yii::app()->db->beginTransaction();

			$model = InasistenciaTipo::model()->findByPk($_POST['id']);

			if (!$model) {
				$model = new InasistenciaTipo();
			}

			$model->attributes = $_POST;
			$model->nombre = ucwords(strtolower($_POST['nombre']));



			if (!$model->save()) {
				$tran->rollback();
				echo json_encode($model->getErrors());
				Yii::app()->end();
			}
			$tran->commit();
		}

		public function actionInasisTipoForm($id) {
			$model = InasistenciaTipo::model()->findByPk($id);
			if (!$model) {
				$model = new InasistenciaTipo();
			}
			$this->renderPartial('_inasistipoForm', array('model' => $model));
		}

		public function actionInasisTipoBorra($id) {
			$tienuso = InasistenciaDetalle::model()->count("inasistencia_tipo_id = $id");
			if ($tienuso > 0) {
				echo('El tipo de inasistencia tiene inasistencias vinculadas. No es posible eliminarla');
			} else {
				$tran = Yii::app()->db->beginTransaction();
				try {
					InasistenciaTipo::model()->deleteAll("id=$id");
					$tran->commit();
				} catch (Exception $e) {
					echo('Error al eliminar el Tipo de Inasistencia. Probablemente se encuentre en uso ' . $e->getMessage());
					$tran->rollBack();
				}
			}
		}

		public function actionTipoDoc($id = null) {
			$model = TipoDocumento::model()->findByPk($id);
			if (!$model) {
				$model = new TipoDocumento();
			}
			$this->render('tipodocIndex', array('model' => $model));
		}

		public function actionTipoDocGraba() {

			$tran = Yii::app()->db->beginTransaction();

			$model = TipoDocumento::model()->findByPk($_POST['id']);

			if (!$model) {
				$model = new TipoDocumento();
			}

			$model->attributes = $_POST;
			$model->nombre = ucwords(strtolower($_POST['nombre']));
			$model->nombre_corto = strtoupper($_POST['nombre_corto']);



			if (!$model->save()) {
				$tran->rollback();
				echo json_encode($model->getErrors());
				Yii::app()->end();
			}
			$tran->commit();
		}

		public function actionTipoDocForm($id) {
			$model = TipoDocumento::model()->findByPk($id);
			if (!$model) {
				$model = new TipoDocumento();
			}
			$this->renderPartial('_tipodocForm', array('model' => $model));
		}

		public function actionTipoDocBorra($id) {
			/*
			 * @todo control de tipodoc en Proveedor.(que será esto??)
			 */

			$tdenuso = Alumno::model()->count("tipo_documento_id = $id");
			if ($tdenuso > 0) {
				echo('El tipo de documento tiene alumnos vinculadas. No es posible eliminarlo');
			} else {
				$tran = Yii::app()->db->beginTransaction();
				try {
					TipoDocumento::model()->deleteAll("id=$id");
					$tran->commit();
				} catch (Exception $e) {
					echo('Error al eliminar el Tipo de Documento. Probablemente se encuentre en uso ' . $e->getMessage());
					$tran->rollBack();
				}
			}
		}

		public function actionCiclo($id = null) {
			$model = Ciclo::model()->findByPk($id);
			if (!$model) {
				$model = new Ciclo();
			}
			$this->render('cicloIndex', array('model' => $model));
		}

		public function actionCicloGraba() {

			$tran = Yii::app()->db->beginTransaction();
			$id = $_POST['id'];
			$model = Ciclo::model()->findByPk($id);

			if (!$model) {
				$model = new Ciclo();
			}

			$model->attributes = $_POST;
			$model->fecha_inicio = Helpers::date("Y/m/d", $_POST['fecha_inicio']);
			$model->fecha_fin = Helpers::date("Y/m/d", $_POST['fecha_fin']);
			if (isset($_POST['activo']))
				$model->activo = 1;
			else
				$model->activo = 0;
			if (isset($_POST['bloqueado']))
				$model->bloqueado = 1;
			else
				$model->bloqueado = 0;


			$cicloActivo = Ciclo::model()->findBySql('select id from ciclo where activo = true');

			if (isset($cicloActivo)) {
				if ($cicloActivo->id == $id)
					$grabar = true;
				else
					$grabar = false;
			}else {
				$grabar = true;
			}

			if ($grabar) {
				if (!$model->save()) {
					$tran->rollback();
					echo json_encode($model->getErrors());
					Yii::app()->end();
				}
				$tran->commit();
			} else {
				throw new CHttpException(404, 'Ya hay un ciclo activo');
			}
		}

		public function actionCicloForm($id) {
			$model = Ciclo::model()->findByPk($id);
			if (!$model) {
				$model = new Ciclo();
			}
			$this->renderPartial('_cicloForm', array('model' => $model));
		}

		public function actionCicloBorra($id) {

			$cenuso = AlumnoDivision::model()->count("ciclo_id = $id");
			if ($cenuso > 0) {
				echo('El ciclo tiene alumnos vinculadas. No es posible eliminarlo');
			} else {
				$tran = Yii::app()->db->beginTransaction();
				try {
					ciclo::model()->deleteAll("id=$id");
					$tran->commit();
				} catch (Exception $e) {
					echo('Error al eliminar el Ciclo. Probablemente se encuentre en uso ' . $e->getMessage());
					$tran->rollBack();
				}
			}
		}

		public function actionParienteTipo($id = null) {
			$model = ParienteTipo::model()->findByPk($id);
			if (!$model) {
				$model = new ParienteTipo();
			}
			$this->render('parientetipoIndex', array('model' => $model));
		}

		public function actionParienteTipoGraba() {

			$tran = Yii::app()->db->beginTransaction();

			$model = ParienteTipo::model()->findByPk($_POST['id']);

			if (!$model) {
				$model = new ParienteTipo();
			}

			$model->attributes = $_POST;
			$model->Nombre = ucwords(strtolower($_POST['nombre']));

			if (!$model->save()) {
				$tran->rollback();
				echo json_encode($model->getErrors());
				Yii::app()->end();
			}
			$tran->commit();
		}

		public function actionParienteTipoForm($id) {
			$model = ParienteTipo::model()->findByPk($id);
			if (!$model) {
				$model = new ParienteTipo();
			}
			$this->renderPartial('_parientetipoForm', array('model' => $model));
		}

		public function actionParienteTipoBorra($id) {
			$tpenuso = Pariente::model()->count("Pariente_Tipo_id = $id");
			if ($tpenuso > 0) {
				echo('El tipo de pariente tiene familias vinculadas. No es posible eliminarlo');
			} else {
				$tran = Yii::app()->db->beginTransaction();
				try {
					ParienteTipo::model()->deleteAll("id=$id");
					$tran->commit();
				} catch (Exception $e) {
					echo('Error al eliminar el Tipo de Pariente. Probablemente se encuentre en uso ' . $e->getMessage());
					$tran->rollBack();
				}
			}
		}

		public function actionIeu() {
			$this->layout = "nuevo";
			$this->render("ieu");
		}

		public function actionIeuAjax(){
			$qry = Helpers::qryAll("select * from ieu");
			echo json_encode($qry);
		}
		public function actionIeuEmptyAjax(){
			$qry = Helpers::qryAllOrEmpty("select * from ieu where id=-1");
			echo json_encode($qry);
		}
	}

?>
