<?php

class DivisionController extends Controller {

    public function actionIndex() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        //$this->render('index');
        $this->render("index");
    }

    public function actionOptions($anio_id) {
        //$x = array('prompt' => '');
        //echo CHtml::listOptions(null, CHtml::listData(Division::model()->findAll("anio_id=$anio_id"), 'id', 'nombre'), $x);
        echo "<option value=\"\"></option>";
        foreach (Division::model()->findAll(array("condition" => "anio_id=$anio_id", "order" => "orden")) as $d) {
            echo "<option orientacion_id=\"$d->Orientacion_id\" orden=\"$d->orden\" value=\"$d->id\">$d->nombre</option>";
        }
    }

    public function actionAsignaturas($division_id) {
        $das = DivisionAsignatura::model()->with()->findAll("Division_id = $division_id ");
        $ret = array();
        /* @var $da DivisionAsignatura */
        foreach ($das as $da) {
            $ret[] = $da->Asignatura_id;
        }
        echo json_encode($ret);
    }

    public function actionGrabaSiguiente() {
        list($division_id, $siguiente_division_id) = array($_POST["division_id"], $_POST["siguiente_division_id"]);
        $d = Division::model()->findByPk($division_id);
        if ($siguiente_division_id == "-1") {
            $d->division_id_siguiente = null;
            $ret = array("division" => null, "anio" => null, "nivel" => Null);
        } else {
            $d->division_id_siguiente = $siguiente_division_id;
            $ret = Helpers::qry("
            select d.nombre as division, a.nombre as anio, n.nombre as nivel
                from division d
                    inner join anio a on a.id = d.anio_id
                    inner join nivel n on n.id = a.nivel_id
               where d.id = $siguiente_division_id
        ");
        }
        $d->save();
        echo json_encode($ret);
    }

}