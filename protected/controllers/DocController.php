<?php

class DocController extends Controller {
  /*
   *              DOCS
   */
  /*
   * Filtro de recibos para ASR
   */
// arianalu@hotmail.com

  public function actionFiltroASR() {
    $this->layout = "nuevo";
    $this->render("filtroASR");
  }

  public function actionFiltroRecibosTiposComprob() {
    $comprobs =
      Comprob::RECIBO_VENTAS .','. Comprob::RECIBO_ELECTRONICO
      /* .','. Comprob::NC_ELECTRONICO .','. Comprob::ND_ELECTRONICO */
      /* .','. Comprob::NC_VENTAS .','. Comprob::ND_VENTAS */ 
      .','. Comprob::RECIBO_BANCO .','. Comprob::NC_VENTAS .','. Comprob::ND_VENTAS;
    $options = Helpers::qryAll("
                SELECT t.id, c.nombre
                    FROM talonario t
                      INNER JOIN comprob c ON c.id = t.comprob_id
                WHERE c.id IN ($comprobs)
            ");
    //$options[] = array('id'=>-1,'nombre'=>'todos');
    echo json_encode($options);
  }

  public function actionFiltroRecibos($filtro = null, $desde = null, $hasta = null, $talonarios = null) {
    $desde = $desde ? date("Y/m/d", strtotime(str_replace("/", ".", $desde))) : null;
    $hasta = $hasta ? date("Y/m/d", strtotime(str_replace("/", ".", $hasta))) : null;
    $whereFecha = $desde ? " fecha_valor between \"$desde\"  and \"$hasta\"" : " true ";
    $whereFiltro = $filtro ?
      "( d.numero like \"%$filtro\" or a.apellido like \"%$filtro%\" or d.fecha_valor like \"%$filtro%\" )" :
      true;
    $talonarios = $talonarios ? $talonarios : '25,41,61';
    $resp = new stdClass();
    $limit = 30000;
    if ($filtro) {
      $where = "banco LIKE \"$filtro%\" OR destino LIKE \"$filtro%\"";
    } else {
      $where = true;
    }
    $selectCount = "
				select count(*)
				from doc d
					inner join talonario t on t.id = d.talonario_id
					inner join comprob c on c.id = t.comprob_id
					LEFT JOIN socio s ON d.Socio_id = s.id
					LEFT JOIN alumno a ON s.Alumno_id = a.id
					left join concepto c2 on c2.id = d.concepto_id
				where d.talonario_id in ($talonarios) and $whereFiltro and $whereFecha and d.anulado = 0 and d.activo = 1
				";
    $select = "
      SELECT doc_id, comprob, fecha_valor, total, alumno, concepto, filtrado, destino, banco FROM(
        select d.id as doc_id,
          concat(RPAD(c.abreviacion,5,' '), '#', LPAD(d.sucursal,4,'0'),'-', LPAD(d.numero,6,'0')) AS comprob,
          DATE_FORMAT(d.fecha_valor,'%d/%m/%Y') as fecha_valor, d.total,
          CONCAT(a.matricula, ':', a.apellido, ', ', a.nombre) as alumno,
          concat(case when c3.nombre is not null then c3.nombre else '' end,'/',c2.nombre) as concepto,
          case when filtrado then 'true' else 'false' end as filtrado,
          (
            SELECT d1.nombre FROM doc_valor dv
              INNER JOIN destino d1 ON dv.Destino_id = d1.id
              WHERE dv.Doc_id = d.id LIMIT 1
          ) AS destino,
          (
                SELECT b.nombre FROM doc_valor dv
                  INNER JOIN banco b ON dv.banco_id = b.id
                  where dv.doc_id = d.id LIMIT 1
          ) AS banco
      from doc d
          inner join talonario t on t.id = d.talonario_id
          inner join comprob c on c.id = t.comprob_id
          LEFT JOIN socio s ON d.Socio_id = s.id
          LEFT JOIN alumno a ON s.Alumno_id = a.id
          left join concepto c2 on c2.id = d.concepto_id
          left join concepto c3 on c3.id = c2.concepto_id
      where d.talonario_id in ($talonarios) /*and $whereFiltro*/ and $whereFecha and d.activo and d.anulado = 0
      order by d.talonario_id, d.fecha_valor
      ) valores
      WHERE $where
      limit $limit
				";
    $resp->recibos = Helpers::qryAll($select);
    $cant = count($resp->recibos);
    $resp->cant = $cant;
    if ($cant > $limit) {
      $resp = array();
    }
    echo json_encode($resp);
  }

  public function actionFiltroGraba($doc_id, $filtrado) {
    $filtrado = $filtrado == 'true' ? 1 : 0;
    helpers::qryExec("update doc set filtrado = $filtrado where id = $doc_id");
  }

  /*
   * 			DOC Mod
   */

  public function actionMod($doc_id) {
    $this->layout = "nuevo";
    $this->render("/doc/mod/index");
  }

  public function actionModData($doc_id) {
    $data = Helpers::qry("
			select d.id,case when s.alumno_id then concat(a.apellido, ', ', a.nombre) else '' end as socio,
					  d.numero, date_format(d.fecha_creacion,'%Y') as fecha_creacion,
						date_format(d.fecha_valor,'%d/%m/%Y') as fecha_valor, d.detalle, d.total, d.concepto_id
				from doc d
					left join socio s on d.socio_id = s.id
					left join alumno a on a.id = s.alumno_id
				where d.id = $doc_id");
    $data["valores"] = Helpers::qryAll("
			select dv.*, date_format(dv.fecha,'%d/%m/%Y') as fecha, t.abreviacion
				from doc_valor dv
					inner join doc_valor_tipo t on t.id = dv.tipo
				where dv.doc_id = $doc_id
			");
    echo json_encode($data);
  }

  /*
   * Fin DOC Mod
   */

  public function actionDocImpresion($doc_id) {
    $s = "select d.total, d.fecha_creacion as fecha, d.detalle, d.letra, d.sucursal, d.numero, d.pago_a_cuenta_importe, d.pago_a_cuenta_detalle,
            case when s.Alumno_id is not null then a.nombre_completo else p.razon_social end as nombre_completo,
            case when s.Alumno_id is not null then \"Alumno\" else \"Proveedor\" end as cliente_tipo,
            case when p.id is null then d.cuit else p.cuit end as cuit,
            case when s.Alumno_id is not null then '' else p.nro_ingresos_brutos end as nro_ingresos_brutos,
            case when s.Alumno_id is not null then '' else concat(p.calle, ' ', coalesce(p.calle_numero,''), ' ', coalesce(p.calle_piso,''), ' ', coalesce(p.calle_departamento,'')) end as domicilio,
            a.nivel_nombre, a.anio_nombre,
            a.division_nombre, a.matricula, c.id as comprob_id, c.nombre as comprob_nombre, l.nombre as localidad, l.codigo_postal,
            df.doc, a.documento_tipo, a.documento_numero, a.nombre_encargado, a.documento_tipo_encargado, a.documento_numero_encargado,
            d.socio_nombre
           from doc d
            inner join talonario t on t.id = d.talonario_id
            inner join comprob c on c.id = t.comprob_id
            left join socio s on s.id = d.Socio_id
            left join alumnos_datos a on a.id = s.Alumno_id
            left join proveedor p on p.id = s.Proveedor_id
            left join localidad as l on p.localidad_id = l.id
            left join doc_afip df on df.doc_id = d.id
          where d.id = $doc_id";
    //vd($s);
    $doc = Helpers::qry($s);
    
//			vd($doc);
    $comprob_id = $doc["comprob_id"];
    $doc["val"] = Helpers::qryAll("
				select v.importe, t.nombre, v.tipo, v.numero,  d.nombre as destino,
						concat(p.nombre,'  ', ta.nombre ) as tarjeta,
						b.nombre as banco, DATE_FORMAT(v.fecha, '%d/%m/%Y') as fecha
					from doc_valor v
						inner join doc_valor_tipo t on t.id = v.tipo
						left join destino d on d.id = v.Destino_id
						left join tarjeta_plan p on p.id = v.tarjeta_plan_id
						left join tarjeta ta on ta.id = p.tarjeta_id
						left join banco b on b.id = v.banco_id
					where v.doc_id = $doc_id
				");
    $doc["apl"] = Helpers::qryAll("
            select d.sucursal, d.numero, da.importe, c.nombre as comprob, d.detalle
                from doc_apl da
                    inner join doc d on d.id = da.doc_id_destino
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
                where da.Doc_id_origen = $doc_id
        ");
    $doc["retencion"] = Helpers::qry("
      select v.importe, t.nombre, v.tipo, v.numero,  d.nombre as destino,
        concat(p.nombre,'  ', ta.nombre ) as tarjeta,
        b.nombre as banco, DATE_FORMAT(v.fecha, '%d/%m/%Y') as fecha
       from doc_valor v
        inner join doc_valor_tipo t on t.id = v.tipo
        left join destino d on d.id = v.Destino_id
        left join tarjeta_plan p on p.id = v.tarjeta_plan_id
        left join tarjeta ta on ta.id = p.tarjeta_id
        left join banco b on b.id = v.banco_id
       where v.doc_id = $doc_id and v.tipo = 7
				");
    //		vd($doc["apl"]);
    $talonarioData = Talonario::getTalonarioData($comprob_id);
    $imprimeAuto = $talonarioData["imprimeAuto"];
    $impresora = $talonarioData["impresora"];
    $nombre_pc = $talonarioData["nombre_pc"];
    $talonarioDataRetencion = Talonario::getTalonarioData(Comprob::RETENCION);
    //vd($talonarioDataRetencion);
    // Numero de retención
    //vd($comprob_id);
    //		vd($comprob_id);
    if ($doc["retencion"]) {
//      $doc['cuit'] = Helpers::qryScalar("select dv.cuit from doc d inner join doc_valor dv on dv.doc_id = d.id where dv.cuit is not null and dv.cuit != ''");
      $this->renderPartial("/doc/docImpresionRetencion", array(
        "doc" => $doc,
        "imprimeAuto" => $talonarioDataRetencion["imprimeAuto"],
        "nombre_pc" => $talonarioDataRetencion["nombre_pc"],
        "impresora" => $talonarioDataRetencion["impresora"],
        "numero" => $talonarioDataRetencion["numero"]
      ));
      $this->renderPartial("/doc/docImpresionGenerico", array(
        "doc" => $doc,
        "imprimeAuto" => $imprimeAuto,
        "nombre_pc" => $nombre_pc,
        "impresora" => $impresora
        )
      );
//				$this->renderPartial("/doc/docImpresionRetencion", array(
//						"doc" => $doc,
//						"imprimeAuto" => 'S',
//						"nombre_pc" => $nombre_pc,
//						"impresora" => "laserGrande"
//				));
    } elseif (in_array($comprob_id, array(Comprob::RECIBO_VENTAS, Comprob::RECIBO_BANCO, Comprob::NC_VENTAS, Comprob::FACTURA_COMPRAS))) {
      $this->renderPartial("/doc/docImpresionRecibo", array(
        "doc" => $doc,
        "imprimeAuto" => $imprimeAuto,
        "nombre_pc" => $nombre_pc,
        "impresora" => $impresora
        )
      );
    } elseif (in_array($comprob_id, array(Comprob::RECIBO_ELECTRONICO))) {
      $this->renderPartial("/doc/docImpresionReciboElectronico", array(
        "doc" => $doc,
        "imprimeAuto" => $imprimeAuto,
        "nombre_pc" => $nombre_pc,
        "impresora" => $impresora
        )
      );
    } elseif (in_array($comprob_id, array(Comprob::NC_ELECTRONICO))) {
      $this->renderPartial("/doc/docImpresionNCElectronico", array(
        "doc" => $doc,
        "imprimeAuto" => $imprimeAuto,
        "nombre_pc" => $nombre_pc,
        "impresora" => $impresora
        )
      );
      //echo json_encode(array("status" => "ok"));
    } elseif (in_array($comprob_id, array(Comprob::AJUSTE_CAJA_SALIDA, Comprob::AJUSTE_CAJA_ENTRADA,
        Comprob::NC_COMPRAS, Comprob::ND_COMPRAS, Comprob::DEPOSITO_BANCARIO_ENTRADA,
        Comprob::DEPOSITO_BANCARIO_SALIDA, Comprob::MOVIMIENTO_BANCARIO_D, Comprob::MOVIMIENTO_BANCARIO_H,
        Comprob::ND_VENTAS, Comprob::OP, Comprob::NC_ELECTRONICO, Comprob::ND_ELECTRONICO))) {
      $this->renderPartial("/doc/docImpresionGenerico", array(
        "doc" => $doc,
        "imprimeAuto" => $imprimeAuto,
        "nombre_pc" => $nombre_pc,
        "impresora" => $impresora
      ));
    } elseif (in_array($comprob_id, array(Comprob::RETENCION))) {
      $this->renderPartial("/doc/docImpresionRetencion", array(
        "doc" => $doc,
        "imprimeAuto" => $imprimeAuto,
        "nombre_pc" => $nombre_pc,
        "impresora" => $impresora
      ));
    } else {
      throw new Exception("No hay plantilla de impresión para el comprobante $comprob_id");
    }
  }

  public function actionDocModificacion($doc_id) {
    $doc = Helpers::qry("
            select d.id, d.total, d.fecha_creacion, d.fecha_valor, d.detalle, d.letra, d.sucursal, d.numero, d.pago_a_cuenta_importe, d.pago_a_cuenta_detalle,
                        case when s.Alumno_id is not null then a.nombre_completo else p.razon_social end as nombre_completo,
                        d.concepto_id, co.nombre as concepto_nombre
                        , a.nivel_nombre, a.anio_nombre,
                        a.division_nombre, a.matricula, c.id as comprob_id, c.nombre as comprob_nombre, dv.destino_id,
                        des.nombre as destino_nombre
                from doc d
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
                    left join socio s on s.id = d.Socio_id
                    left join alumnos_datos a on a.id = s.Alumno_id
                    left join proveedor p on p.id = s.Proveedor_id
                    left join concepto co on co.id = d.concepto_id
                    left join doc_valor dv on dv.doc_id = d.id
                    left join destino des on des.id = dv.Destino_id
                where d.id = $doc_id
        ");

    $comprob_id = $doc["comprob_id"];

    $doc["val"] = Helpers::qryAll("
            select v.id, v.importe, t.nombre, p.nombre as plan , v.tipo, v.numero,  d.id as destino_id, d.nombre as destino, ta.nombre as tarjeta,
                        b.nombre as banco, v.chq_origen, v.lote, v.chq_cuit_endosante, v.chq_entregado_a, ch.descripcion as chequera,
                        v.chequera_id, v.tarjeta_plan_id, v.banco_id, v.fecha
                from doc_valor v
                    inner join doc_valor_tipo t on t.id = v.tipo
                    left join destino d on d.id = v.Destino_id
                    left join tarjeta_plan p on p.id = v.tarjeta_plan_id
                    left join tarjeta ta on ta.id = p.tarjeta_id
                    left join banco b on b.id = v.banco_id
                    left join chequera ch on ch.id = v.chequera_id
                where v.doc_id = $doc_id
        ");
    $doc["apl"] = Helpers::qryAll("
            select d.sucursal, da.importe, d.total, d.detalle,
                        concat(c.nombre,\"-\", LPAD(d.numero,8,\"0\"), \" / \", substr(d.detalle,1,80)) as comprob,
                        d.fecha_valor, d.fecha_creacion
                from doc_apl da
                    inner join doc d on d.id = da.doc_id_destino
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
                where da.Doc_id_origen = $doc_id
        ");

    $this->renderPartial("modificaDoc", array(
      "comprob_id" => $comprob_id,
      "doc" => $doc,
    ));
  }

  public function actionDocModificacionGraba() {
    $tr = Yii::app()->db->beginTransaction();
    $doc = Doc::model()->findByPk($_POST["doc_id"]);
    $doc->setAttributes($_POST["cab"]);
    $doc->fecha_creacion = Helpers::fechaParaGrabar($_POST["cab"]["fecha_creacion"]);
    if (isset($_POST["cab"]["fecha_valor"])) {
      $doc->fecha_valor = Helpers::fechaParaGrabar($_POST["cab"]["fecha_valor"]);
    }
    $totalValores = 0;
    if (isset($_POST["doc-det"])) {
      foreach ($_POST["doc-det"] as $doc_valor_id => $valor) {
        $doc_valor = DocValor::model()->findByPk($doc_valor_id);
        $doc_valor->numero = $valor["numero"];
        $doc_valor->importe = str_replace(",", "", $valor["importe"]);
        if ($valor["banco_id"]) {
          $doc_valor->banco_id = $valor["banco_id"];
        }
        if ($valor["chq_cuit_endosante"]) {
          $doc_valor->chq_cuit_endosante = $valor["chq_cuit_endosante"];
        }
        if ($valor["chq_entregado_a"]) {
          $doc_valor->chq_entregado_a = $valor["chq_entregado_a"];
        }
        if ($valor["chq_origen"]) {
          $doc_valor->chq_origen = $valor["chq_origen"];
        }
        if (isset($_POST["cab"]["destino_id"])) {
          $doc_valor->Destino_id = $_POST["cab"]["destino_id"];
        }
        $totalValores += $doc_valor->importe;
        if (!$doc_valor->save()) {
          $tr->rollback();
          vd($doc_valor->errors);
        }
      }
      $totalValores = round($totalValores, 2);
    }
    $doc->total = round($doc->total, 2);
    if (isset($_POST["doc-det"]) and ( $totalValores !== $doc->total)) {
      echo "El total de los valores ($totalValores) es diferente que el total del documento ($doc->total) dif=" . ($totalValores - $doc->total);
      //vd($doc->total+0, $total, $total - ($doc->total+0));
      $tr->rollback();
      Yii::app()->end();
    }
    if (!$doc->save()) {
      $tr->rollback();
      vd($doc->errors);
    }
    //vd($doc->attributes);
    $tr->commit();
  }

  public function actionModificaValor() {
    $data = reset($_POST["doc-det"]);
    $data["doc_valor_id"] = "";
    $this->renderPartial("modificaDocValor", array("data" => $data));
  }

  /*
   * Documentos a aplicar
   */

  public function actionDocsAAplicar($socio_id, $pagoTotal = null, $doc_id = null) {
    /* @var $comprob Comprob */
    $socio = Socio::model()->with(array("alumno", "alumno.familia"))
      ->findByPk($socio_id);
    $obsCobranza = $socio->Alumno_id ? $socio->alumno->familia->obs : "";
    //    $tipo_doc = $socio->Alumno_id ? Comprob::RECIBO_VENTAS : Comprob::OP;
    $ret = array();
    /* @var $cta DocApl */
    /* @var $comprob Comprob */
    // no traigo los comp. que tienen liquid_conf_id=0 pq son los de
    // una liquidaciÃƒÂ³n a medias
    if ($doc_id) {
      $qry = "
                select dd.id, c.nombre, dd.sucursal,dd.numero, dd.detalle,
			 								 dd.fecha_creacion , dd.fecha_valor, date_format(lc.fecha_venc_1, '%d/%m/%y') as fecha_vto,
			 								 dd.total, dd.saldo+a.importe as saldo, a.importe, c.nombre as comprob
                    from doc d
                        inner join doc_apl a on a.Doc_id_origen = d.id
                        inner join doc dd on dd.id = a.doc_id_destino
                        inner join liquid_conf lc on lc.id = dd.doc_liquid_id
                        inner join talonario t on t.id = dd.talonario_id
                        inner join comprob c on c.id = t.comprob_id
                    where d.id = $doc_id and a.doc_apl_cab_id is null
                    order by d.fecha_valor
            ";
    } else {
      // hola
      $qry = "
				select d.id as id, c.nombre as comprob, d.detalle, d.fecha_creacion, d.fecha_valor, date_format(l.fecha_venc_1, '%d/%m/%y') as fecha_vto,
										d.sucursal, d.numero, d.total, d.saldo, dl.liquid_conf_id, l.fecha_confirmado
						from doc d
								left join doc_liquid dl on dl.id = d.doc_liquid_id
								left join liquid_conf l on l.id = dl.liquid_conf_id
								inner join talonario t on t.id = d.talonario_id
								inner join comprob c on c.id = t.comprob_id
						where d.socio_id = $socio_id and
												d.saldo > 0 and
												c.signo_cc = 1 and
												d.anulado <> 1  and
												d.activo = 1
						order by d.fecha_valor";
    }
    $ctas = Yii::app()->db->createCommand($qry)->queryAll();
    foreach ($ctas as $cta) { // hola
      //$cta["total"] = round($cta["total"],2);
      if (!$cta["detalle"]) {
        $ddoc_id = $cta["id"];
        foreach (Yii::app()->db->createCommand("
                            select a.nombre
                              from doc_det dd
                                inner join articulo a on a.id = dd.articulo_id
                              where dd.Doc_id = $ddoc_id")->queryAll() as $det) {
          $cta["detalle"] .= substr($det["nombre"], 0, 15) . "/";
        }
        $cta["detalle"] = substr($cta["detalle"], 0, -1);
        if (strlen($cta["detalle"]) > 20) {
          $cta["detalle"] = substr($cta["detalle"], 0, 20) . "...";
        }
      }
      $ddoc_id = $cta["id"];
      $cantDetalles = Yii::app()->db->createCommand("
                select count(*) from doc_det where doc_id = $ddoc_id
            ")->queryScalar();
      if ($doc_id) {
        $pago = $cta["importe"];
      } else {
        if (!$pagoTotal) {
          $pago = abs($cta["saldo"]);
        } else {
          if ($pagoTotal > $cta["saldo"]) {
            $pago = $cta["saldo"];
          } else {
            $pago = $pagoTotal;
          }
          $pagoTotal -= $pago;
        }
      }
      $ret[] = array(
        "id" => $cta["id"],
        "comprob" => $cta["comprob"],
        "fecha_creacion" => $cta["fecha_creacion"],
        "fecha_vto" => $cta["fecha_vto"],
        "sucursal" => $cta["sucursal"],
        "numero" => $cta["numero"],
        "detalle" => $cta["detalle"],
        "cantDetalles" => $cantDetalles,
        "total" => $cta["total"],
        "saldo" => $cta["saldo"],
        "aplicacion" => $pago,
      );
    }
    $this->renderPartial("_docsAAplicar", array(
      "ctas" => $ret, "resto" => $pagoTotal ? $pagoTotal : 0,
      "doc_id" => $doc_id,
      "matricula" => $socio->alumno ? $socio->alumno->matricula : "",
      "obsCobranza" => $obsCobranza,
      )
    );
  }

  /*
   *                                              RECIBO - OP
   */

  public function filters() {
    return array(
      'accessControl', // perform access control for CRUD operations
      'rights',
    );
  }

  public function actionReciboBanco($doc_id = null, $socio_id = null) {
    $this->actionReciboOp($doc_id, "reciboBanco", $socio_id);
  }

  public function actionReciboOp($doc_id = null, $tipo = null, $socio_id = null) {
    if ($doc_id) {
      $doc = Doc::model()->with("talonario")->findByPk($doc_id);
      $socio_id = $doc->Socio_id;
      $comprob_id = $doc->talonario->comprob_id;
      $socio = $doc->socio;
    } else {
      $socio = $socio_id ? Socio::model()->findByPk($socio_id) : null;
      switch ($tipo) {
        case "recibo":
          $this->pageTitle = "Recibo";
          $comprob_id = Comprob::RECIBO_VENTAS;
          break;
        case "reciboElectronico":
          $this->pageTitle = "Recibo Electrónico";
          $comprob_id = Comprob::RECIBO_ELECTRONICO;
          break;
        case "reciboBanco":
          $this->pageTitle = "Recibo Banco";
          $comprob_id = Comprob::RECIBO_BANCO;
          break;
        case "op":
          $this->pageTitle = "Orden de Pago";
          $comprob_id = Comprob::OP;
          break;
        default:
          break;
      }
      $doc = new Doc("insert", $comprob_id);
    }

    if ($socio) {
      $data = Helpers::qry("
                select a.matricula, nivel.nombre as nivel,
                            concat(nivel.nombre, ' ', anio.nombre, ' ', d.nombre) as curso,
                             a.beca, saldo(s.id,null) as saldo,
                             concat(p.apellido, ', ', p.nombre) as encargado_pago,
                             td.nombre as encargado_tipo_doc, p.numero_documento as encargado_numero_doc
                    from alumno a
                        left join socio s on s.Alumno_id = a.id and s.id = $socio_id
                        left join alumno_division ad on ad.Alumno_id = a.id and ad.activo = 1
                        left join pariente p on p.id = a.encargado_pago_id
                        left join tipo_documento td on td.id = p.tipo_documento_id
                        left join division d on d.id = ad.Division_id
                        left join anio on anio.id = d.Anio_id
                        left join nivel on nivel.id = anio.Nivel_id
                where s.id = $socio_id");
    } else {
      $data = null;
    }

    $talonarioData = Talonario::getTalonarioData($comprob_id);
    $imprimeAuto = $talonarioData["imprimeAuto"];

    if (!Destino::getCajaInstanciaActivaId()) {
      $cajaCerrada = true;
    } else {
      $cajaCerrada = false;
    }
    return $this->render("reciboOp/recibo_op", array(
        "doc" => $doc,
        "doc_id" => $doc_id,
        "comprob_id" => $comprob_id,
        "ctas" => array(),
        "socio" => $socio,
        "cajaCerrada" => $cajaCerrada,
        "action" => $tipo,
        "imprimeAuto" => $imprimeAuto,
        "data" => $data,
        )
    );
  }

  public function actionReciboElectronico($doc_id = null, $socio_id = null) {
    $this->actionReciboOp($doc_id, "reciboElectronico", $socio_id);
  }

  public function actionRecibo($doc_id = null, $socio_id = null) {
    $this->actionReciboOp($doc_id, "recibo", $socio_id);
  }

  public function actionOp($doc_id = null, $socio_id = null) {
    $this->actionReciboOp($doc_id, "op", $socio_id);
  }

  public function actionDocGraba() {
    $json = new stdClass();
    $doc = Doc::altaDoc($_POST, $json->errores);
    if ($doc) {
      $json->doc_id = $doc->id;
      if (isset($doc->afip)) {
        $json->afip = $doc->afip;
      }
    }
    echo json_encode($json);
  }

  public function actionReciboCtaCte($socio_id) {
    $this->renderPartial("ctaCte", array("socio_id" => $socio_id));
  }

  public function actionReciboCtaCteViejo($socio_id) {
    $matricula = Yii::app()->db->createCommand("
            select a.matricula
              from socio s
                inner join alumno a on a.id = s.alumno_id
              where s.id = $socio_id
          ")->queryScalar();
    $db = Yii::app()->fb->connection;
    $select = "
          select * from te_trae_cc_sin_det($matricula,Null,0)";
    $sth = ibase_query($db, $select);
    $rows = array();
    while ($row = ibase_fetch_assoc($sth)) {
      //vd($row);
      $rows[] = $row;
    }
    //    print_r($rows);die;
    $this->renderPartial("ctaCteViejo", array("rows" => $rows));
  }

  public function actionGetDetalle($doc_id) {
    $dets = DocDet::model()->with("articulo")->findAll("doc_id=$doc_id");
    $this->renderPartial("_doc_det", array("dets" => $dets));
  }

  public function actionReciboGrabaNotaDeCredito() {
    $ret = new stdClass();
    $ret->errores = array();
    $tr = Yii::app()->db->beginTransaction();
    $doc = new Doc("insert", Comprob::NC_VENTAS);
    $doc->Socio_id = $_POST["socio_id"];
    $doc->total = $_POST["importe"];
    $doc->saldo = 0;
    $doc->detalle = $_POST["obs"];
    $doc->concepto_id = isset($_POST["concepto_id"]) ? $_POST["concepto_id"] : null;
    if (!$doc->save()) {
      $ret->errores = $doc->errors;
      echo json_encode($ret);
      die;
    }

    $da = new DocApl();
    $da->Doc_id_origen = $doc->id;
    $da->importe = $doc->total;
    $da->doc_id_destino = $_POST["doc_id"];
    if (!$da->save()) {
      echo json_encode($ret);
      die;
    }
    $tr->commit();
    $ret->saldo = Doc::model()->findByPk($_POST["doc_id"])->saldo;


    echo json_encode($ret);
  }

  /*
   *                                              FACTURA VENTA - FACTURA COMPRA
   */

  public function actionFacturaCompra() {
    $this->actionFactura(Comprob::FACTURA_COMPRAS);
  }

  public function actionFactura($tipo) {
    $doc = new Doc("insert", $tipo);
    $this->render('factura/factura', array(
      "tipo" => $tipo,
      "doc" => $doc
      )
    );
  }

  public function actionFacturaVenta() {
    $this->actionFactura(Comprob::FACTURA_VENTAS);
  }

  public function actionFacturaGraba() {
    $json = new stdClass();
    $doc = Doc::altaDoc($_POST, $json->errores);
    if (isset($doc->afip) and $doc->afip) {
      $json->afip = $doc->afip;
    }
    echo json_encode($json);
  }

  public function actionGetDocDetVacia($id = "nuevo") {
    $docDet = new DocDet();
    $this->renderPartial("factura/_factura_detalle", array(
      "docDet" => $docDet, "docDetID" => $id
    ));
  }

  public function actionArticuloAC() {
    $term = $_GET['term'];
    //    $artTipo = $_GET["artTipo"];

    $criteria = new CDbCriteria();
    $criteria->limit = 40;
    if (is_numeric($term)) {
      $criteria->addCondition("id = \"$term\"");
    } else {
      $criteria->addCondition("nombre like \"%$term%\"");
      $criteria->order = "nombre";
    }
    $arts = Articulo::model()->findAll($criteria);
    $ret = array();
    /* @var $art Articulo */
    foreach ($arts as $art) {
      $ret[] = array(
        "id" => $art->id,
        'label' => "(" . $art->id . ") " . $art->nombre,
        "precio" => $art->precio_neto,
        'value' => $art->nombre
      );
    }
    echo json_encode($ret);
  }

  /*
   *                                              VALORES
   */

  public function actionTraeValores($doc_id) {
    $valores = Helpers::qryAll("
            select *
                from doc_valor v
                where v.doc_id = $doc_id
        ");
    $cab = $this->renderPartial("valores/_traeValoresCab", array("valores" => $valores), true);
    $det = $this->renderPartial("valores/_traeValoresDet", array("valores" => $valores), true);
    echo json_encode(array("cab" => $cab, "det" => $det));
  }

  public function actionValoresTraeValorForm($tipo_valor, $id, $cheque_origen = null) {
    //        $valor = Yii::app()->db->createCommand("
    //            select v.id, v.Doc_id, v.importe, v.banco_id, v.chq_cuit_endosante, v.fecha,
    //                   v.numero, v.tarjeta_plan_id, v.obs, v.Destino_id,
    //                   t.id as tarjeta_id, p.id as plan_id, v.chequera_id
    //              from doc_valor v
    //                left join tarjeta_plan p on p.id = v.tarjeta_plan_id
    //                left join tarjeta t on t.id = p.tarjeta_id")->queryRow();
    $this->renderPartial("valores/valoresTraeValorForm", array(
      "tipo_valor" => $tipo_valor,
      "id" => $id,
      //        "valor" => $valor,
      "cheque_origen" => $cheque_origen
      )
    );
  }

  public function actionGetChequeraSiguienteNumero($chequera_id) {
    $chequera = Chequera::model()->findByPk($chequera_id);
    echo $chequera->siguienteNumero;
  }

  public function actionGetChequesDiv() {
    $this->renderPartial("valores/_chequesDiv");
  }

  public function actionGetCuponesDiv() {
    $this->renderPartial("valores/_cuponesDiv");
  }

  public function actionGetRetencionesDiv() {
    $this->renderPartial("valores/_retencionesDiv");
  }

  public function actionRecuperaPlanes($tarjeta_id) {
    $x = array('prompt' => '');
    echo CHtml::listOptions(null, CHtml::listData(TarjetaPlan::model()->findAll("tarjeta_id=$tarjeta_id"), 'id', 'nombre'), $x);
  }

  /*
   *                                              CAJA/FONDOS
   */

  public function actionMovCajaEntrada($doc_id = null) {
    $this->actionMovCaja($doc_id, Comprob::AJUSTE_CAJA_ENTRADA);
  }

  public function actionMovCaja($doc_id = null, $comprob_id) {
    if ($doc_id) {
      $model = Doc::model()->findByPk($doc_id);
    } else {
      $model = new Doc("insert", $comprob_id);
    }
    $this->render("movCaja/movCaja", array(
      "model" => $model,
      "comprob_id" => $comprob_id,
      "doc_id" => $doc_id
      )
    );
  }

  public function actionMovCajaSalida($doc_id = null) {
    $this->actionMovCaja($doc_id, Comprob::AJUSTE_CAJA_SALIDA);
  }

  public function actionMovCajaGraba() {
    $json = new stdClass();
    $doc = Doc::altaDoc($_POST, $json->errores);
    if ($doc) {
      $json->doc_id = $doc->id;
    }
    echo json_encode($json);
  }

  /*
   *                                                TRANSFERENCIAS
   *
   */

  public function actionTransferencia() {
    $this->actionTransferenciaDeposito("T");
  }

  public function actionTransferenciaDeposito($tipo) {
    if (!$tipo) {
      throw new Exception("Debe indicar si es Transferencia (T) o Deposito (D) como parametro");
    }
    $tipoDestino = $tipo == "T" ? 1 : 2;
    $cajaActivaId = Destino::getCajaInstanciaActiva()->Destino_id;
    $cajaActiva = Destino::model()->findByPk($cajaActivaId);
    if (!$cajaActivaId) {
      Helpers::error("La caja no estÃ¡ abierta");
      die;
    }
    $destinos = Destino::model()->findAll("tipo = 2 and id <> $cajaActivaId");
    $destinoListData = "";
    foreach ($destinos as $d) {
      $destinoListData .= "<option value=\"$d->id\">$d->nombre</option>";
    }

    if ($tipoDestino == 1) {
      $entrada = new Doc("insert", Comprob::TRANSFERENCIA_ENTRADA);
      $salida = new Doc("insert", Comprob::TRANSFERENCIA_SALIDA);
    } else {
      $entrada = new Doc("insert", Comprob::DEPOSITO_BANCARIO_SALIDA);
      $salida = new Doc("insert", Comprob::DEPOSITO_BANCARIO_ENTRADA);
    }

    $this->render("transferenciaDeposito", array(
      "entrada" => $entrada,
      "salida" => $salida,
      "destinoListData" => $destinoListData,
      "cajaActiva" => $cajaActiva,
      "tipoDestino" => $tipo,
      )
    );
  }

  public function actionDeposito() {
    $this->actionTransferenciaDeposito("D");
  }

  //@todo:Falta grabar la transferencia con altaDoc

  public function actionTransferenciaDepositoGraba() {
    Doc::altaTransferenciaDeposito($_POST);
  }

  /*
   *                                                Movimiento Bancario
   *
   */

  public function actionMovBanco() {
    $entrada = new Doc("insert", Comprob::MOVIMIENTO_BANCARIO_D);
    $destinos = Destino::model()->with(array("banco"))->findAll("tipo=2");
    $destinoListData = "";
    foreach ($destinos as $d) {
      $banco = $d->banco->nombre;
      $destinoListData .= "<option value=\"$d->id\" banco=\"$banco\">$d->numero_cuenta</option>";
    }
    $this->render("movBanco/movBanco", array(
      "entrada" => $entrada,
      "destinoListData" => $destinoListData,
      )
    );
  }

  public function actionMovBancoGraba() {

    $json = new stdClass();
    if ($_POST["doc"]["total"] < 0) {
      $_POST["doc"]["total"] = $_POST["doc"]["total"] * -1;
      $_POST["doc"]["comprob_id"] = Comprob::MOVIMIENTO_BANCARIO_H;
      $doc = new Doc("insert", Comprob::MOVIMIENTO_BANCARIO_H);
    } else {
      $doc = new Doc("insert", Comprob::MOVIMIENTO_BANCARIO_D);
    }
    $_POST["doc"]["numero"] = $doc->numero;
    //vd($_POST);


    $_POST["valores"][]["Efectivo"] = array(
      "importe" => $_POST["doc"]["total"],
    );

    Doc::altaDoc($_POST, $json->errores);
    echo json_encode($json);
  }

  public function actionMovBancoArticulos() {
    $jqGrid = new JqGrid();
    $jqGrid->fields = array("nombre", "importe");
    $jqGrid->selectCount = "
      SELECT count(*) AS cant FROM articulo a
        INNER JOIN articulo_tipo t ON t.id = a.articulo_tipo_id
        WHERE t.novedad_tipo = 3
    ";
    //$jqGrid->filter = "DocController::movBancoArticulosfilter";
    $jqGrid->select = "
          SELECT a.id, a.nombre, a.precio_neto AS importe
              FROM articulo a
                INNER JOIN articulo_tipo t ON t.id = a.articulo_tipo_id
              WHERE t.novedad_tipo = 3";
    $jqGrid->render();
  }

  public function actionMovBancoGetTr() {
    $this->renderPartial("movBanco/movBanco_articulo_tr");
  }

  /*
   *                                                APLICACIONES
   */

  public function actionAplicacionAlumno($socio_id = null, $todo = null) {
    $this->actionAplicacion($socio_id, $todo, "alumno");
  }

  public function actionAplicacion($socio_id = null, $todo = null, $socioTipo = null, $comprob_id = null) {
    $socio = Socio::model()->with(array(
        "alumno", "proveedor"
      ))->findByPk($socio_id);
    if ($comprob_id) {
      switch ($comprob_id) {
        case Comprob::RECIBO_ELECTRONICO:
          $comprobTipo = "reciboElectronico";
          break;
        case Comprob::RECIBO_VENTAS:
          $comprobTipo = "recibo";
          break;
        case Comprob::RECIBO_BANCO:
          $comprobTipo = "reciboBanco";
          break;
        case Comprob::OP:
          $comprobTipo = "op";
          break;
        case Comprob::NC_VENTAS:
          $comprobTipo = "ncVentas";
          break;
        case Comprob::NC_COMPRAS:
          $comprobTipo = "ncVentas";
          break;
        case Comprob::ND_VENTAS:
          $comprobTipo = "ndVentas";
          break;
        case Comprob::NC_ELECTRONICO:
          $comprobTipo = "ncVentasElectronico";
          break;
        case Comprob::ND_ELECTRONICO:
          $comprobTipo = "ndVentasElectronico";
          break;
        default:
          //                    Helpers::error("error","Aplicacion, no se puede volver al comprob_id $comprob_id");
          //                    die;
          $comprobTipo = "";
          break;
      }
    } else {
      $comprobTipo = "";
    }
    if ($socio) {
      $socioTipo = ($socio->Alumno_id ? "alumno" : "proveedor");
    } else {
      if (!$socioTipo) {
        throw new Exception('$_GET["socioTipo"] debe ser alumno o proveedor');
      }
    }
    if (!$socioTipo) {
      throw new Exception("No está definido socioTipo");
    }
    /* @var $socio Socio */
    if ($socio_id and ! $socio) {
      Helpers::error("Socio no encontrado");
      die;
    }
    if (!$todo) {
      $todoAnd = " and d.saldo > 0";
    } else {
      $todoAnd = " ";
    }
    /*
     * Documentos pendientes
     */
    if (!$socio) {
      $docsPendientes = array();
      $docs = array();
    } else {
      $qry = "
                select d.id as doc_id, c.nombre as comprob, d.detalle, d.fecha_creacion, d.total, d.saldo,
                       d.sucursal, d.numero
                    from socio s
                        inner join doc d on d.Socio_id = s.id
                        inner join talonario t on t.id = d.talonario_id
                        inner join comprob c on c.id = t.comprob_id
                    where s.id = $socio_id and c.signo_cc = 1 $todoAnd and
                                 d.anulado <> 1 and d.activo = 1
                    order by d.fecha_valor";
      //vd($qry);
      $docsPendientes = Yii::app()->db->createCommand($qry)->queryAll();
      /*
       * Aplicaciones
       */
      foreach ($docsPendientes as $key => $doc) {
        $doc_id = $doc["doc_id"];
        $docs = Yii::app()->db->createCommand("
                    select da.id as doc_apl_id, doc_orig.id as doc_orig_id, c.nombre as comprob,
                    doc_orig.detalle,
                           doc_orig.fecha_creacion, da.importe, da.doc_id_destino,
                           doc_orig.sucursal, doc_orig.numero
                            from doc_apl da
                                inner join doc doc_orig on doc_orig.id = da.doc_id_origen
                                inner join talonario tdo on tdo.id = doc_orig.talonario_id
                                inner join comprob c on c.id = tdo.comprob_id
                            where da.doc_id_destino = $doc_id
                                  and doc_orig.anulado <> 1
                  ")->queryAll();
        $docsPendientes[$key]["docsAplicados"] = array();
        foreach ($docs as $d) {
          $docsPendientes[$key]["docsAplicados"][] = $d;
        }
      }
      /*
       *  Documentos de crédito
       */
      $docs = Yii::app()->db->createCommand("
                select d.id as doc_id, c.nombre as comprob, d.detalle, d.fecha_creacion, d.total, d.saldo,
                       d.sucursal, d.numero
                  from socio s
                      inner join doc d on d.Socio_id = s.id
                      inner join talonario t on t.id = d.talonario_id
                      inner join comprob c on c.id = t.comprob_id
                  where s.id = $socio_id and c.signo_cc = -1 $todoAnd and d.anulado <> 1 and d.activo = 1
                  order by d.fecha_valor
                ")->queryAll();
    }

    $this->pageTitle = $socioTipo == "alumno" ? "Aplicaciones Alumnos" : "Aplicaciones Proveedores";
    $this->render("aplicacion/index", array(
      "docsPendientes" => $docsPendientes,
      "docs" => $docs,
      "socio" => $socio,
      "todo" => $todo,
      "socioTipo" => $socioTipo,
      "comprobTipo" => $comprobTipo,
      "vieneDeDoc" => $socioTipo
      )
    );
  }

  public function actionAplicacionProveedor($socio_id = null, $todo = null) {
    $this->actionAplicacion($socio_id, $todo, "proveedor");
  }

  public function actionAplicacionGraba() {

    /* @var $apl DocApl */
    if (isset($_POST["borrar"])) {
      foreach ($_POST["borrar"] as $app_id) {
        DocApl::Borra($app_id);
      }
    }

    if (isset($_POST["importe"])) {
      $dac = new DocAplCab();
      $dac->user_id = Yii::app()->user->model->id;
      $dac->fecha = Helpers::date("Y/m/d", time());
      $dac->save();
      $tr = Yii::app()->db->beginTransaction();
      foreach ($_POST["importe"] as $origen => $dest) {
        foreach ($dest as $app => $importe) {
          $error = DocApl::Nueva(array(
              "origen" => $origen,
              "destino" => $app,
              "importe" => $importe,
              "docAplCabId" => $dac->id,
          ));
          if ($error) {
            $tr->rollback();
            var_dump(json_encode(array(
              "docs" => array("origen: $origen, destino: $app, importe: $importe"),
              "error" => $error
            )));
            die;
          }
        }
      }
      $tr->commit();
    }
  }

  /*
   *                                                Anulaciones
   */

  public function actionAnulaDoc($doc_id = null, $force = false) {
    $tr = Yii::app()->db->beginTransaction();
    if (!$doc_id) {
      $this->render("anulaDoc");
    } else {
      $data = new stdClass();
      $data->error = "";
      $doc = Doc::model()->findByPk($doc_id);
      $doc_dependientes = Doc::model()->findAll("doc_id = $doc->id");
      if (!$force) {
        $data->aplicaciones = $doc->tieneAplicaciones;
        $data->cajaCerrada = $doc->cajaCerrada;
      } else {
        foreach ($doc_dependientes as $doc_dependiente) {
          $doc_dep = Doc::model()->findByPk($doc_dependiente['id']);
          $doc_dep->anula();
        }
      }
    }

    $doc->anula();

    //$doc =
    //$destinoInstancia =
    $data->doc_id = $doc_id;
    $tr->commit();
    echo json_encode($data);
  }

  public function actionAnulaDocGetDocs() {
    $comprobTipo = $_POST["comprob_tipo_id"];
    $fechaDesde = date("Y/m/d", mystrtotime($_POST["fecha_desde"]));
    $fechaHasta = date("Y/m/d", mystrtotime($_POST["fecha_hasta"]) + 24 * 3600);
    $selectComprobTipoNombre = "select nombre from comprob where id = $comprobTipo";
    $comprobTipoNombre = Yii::app()->db->createCommand($selectComprobTipoNombre)->queryScalar();
    $select = "
      select d.id, a.matricula, d.detalle,
             case when s.Alumno_id is not null then concat(a.apellido,', ', a.nombre) else p.razon_social end as nombre ,
             d.numero, d.fecha_creacion, d.fecha_modificacion, d.total, d.saldo, d.pago_a_cuenta_importe
        from doc d
          inner join talonario t on t.id = d.talonario_id
          left join socio s on s.id = d.Socio_id
          left join alumno a on a.id = s.Alumno_id
          left join proveedor p on p.id = s.Proveedor_id
        where t.comprob_id = $comprobTipo
              and d.fecha_creacion between \"$fechaDesde\" and  \"$fechaHasta\"
              and d.anulado <> 1 and d.activo = 1
        order by fecha_creacion desc, d.numero asc";
    //echo $select;die;
    $rows = Yii::app()->db->createCommand($select)->queryAll();
    $this->renderPartial("_anulaDoc_docs", array(
      "comprobTipoNombre" => $comprobTipoNombre,
      "comprobTipo" => $comprobTipo,
      "rows" => $rows,
    ));
  }

  public function actionImprimeDoc($doc_id) {
    echo $doc_id;
  }

  /*
   *                                                Nota de crédito/débito
   */

  public function actionNcVentas($socio_id = null) {
    $this->pageTitle = "Nota de Crédito";
    $this->actionNcNd(Comprob::NC_VENTAS, $socio_id);
  }

  public function actionNcNd($comprob_id, $socio_id = null) {
    $socio = $socio_id ? Socio::model()->findByPk($socio_id) : null;

    $tipoSocio = in_array($comprob_id, array(
        Comprob::NC_COMPRAS, Comprob::ND_COMPRAS
      )) ? "proveedor" : "cliente";
    $model = new Doc("insert", $comprob_id);
    $this->render("ncnd/ncnd", array(
      "comprob_id" => $comprob_id,
      "socio" => $socio,
      "model" => $model,
      "tipoSocio" => $tipoSocio
      )
    );
  }

  public function actionNdVentas($socio_id = null) {
    $this->pageTitle = "Nota de Débito";
    $this->actionNcNd(Comprob::ND_VENTAS, $socio_id);
  }

  public function actionNcVentasElectronico($socio_id = null) {
    $this->pageTitle = "Nota de Crédito Electrónica";
    $this->actionNcNd(Comprob::NC_ELECTRONICO, $socio_id);
  }

  public function actionNdVentasElectronico($socio_id = null) {
    $this->pageTitle = "Nota de Débito Electrónica";
    $this->actionNcNd(Comprob::ND_ELECTRONICO, $socio_id);
  }

  public function actionNcCompras() {
    $this->pageTitle = "Nota de Crédito";
    $this->actionFactura(Comprob::NC_COMPRAS);
  }

  public function actionNdCompras() {
    $this->pageTitle = "Nota de Débito";
    $this->actionFactura(Comprob::ND_COMPRAS);
  }

  public function actionNcndGraba() {
    $json = new stdClass();
//      vd($_POST);
    Doc::altaDoc($_POST, $json->errores);
    echo json_encode($json);
  }

  /*
   *                                                OTROS
   */

  public function actionSocioChkSaldo($socio_id) {
    $socio = Socio::model()->findByPk($socio_id);
    $saldo = $socio->saldoParaAplicar;
    if ($saldo) {
      echo $saldo;
    }
  }

}
