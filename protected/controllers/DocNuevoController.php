<?php

	class DocNuevoController extends Controller {

		public $layout = "bootstrap";

		public function actionIndex() {
			Yii::app()->libs->add(array("knockout", "jquery-sortable", "setImmediate", "font-awesome"));
			$this->render("index");
		}

		public function actionAppsAjax($doc_id = null, $socio_id = null) {
			if ($doc_id) {
				$sApp = "
				select dd.id, CONCAT(RPAD(c.abreviacion,10,' '),LPAD(dd.sucursal,4,'0'),'-',LPAD(dd.numero,8,'0')) AS comprob,
						  DATE_FORMAT(d.fecha_valor,'%d/%m/%y') as fecha, dd.total, dd.saldo+a.importe as saldo, a.importe, 
						  dd.detalle
				from doc d
						inner join doc_apl a on a.Doc_id_origen = d.id
						inner join doc dd on dd.id = a.doc_id_destino
						inner join talonario t on t.id = dd.talonario_id
						inner join comprob c on c.id = t.comprob_id
				where d.id = $doc_id and a.doc_apl_cab_id is null";
			} else {
				$sApp = "
				select d.id, CONCAT(RPAD(c.abreviacion,10,' '),LPAD(d.sucursal,4,'0'),'-',LPAD(d.numero,8,'0')) AS comprob,
						  d.fecha_creacion as fecha, d.total, d.saldo as saldo, 0 as importe, 
				d.detalle
				from doc d
						left join doc_liquid dl on dl.id = d.doc_liquid_id
						left join liquid_conf l on l.id = dl.liquid_conf_id
						inner join talonario t on t.id = d.talonario_id
						inner join comprob c on c.id = t.comprob_id
				where d.socio_id = $socio_id and
										d.saldo > 0 and 
										c.signo_cc = 1 and
										d.anulado <> 1  and 
										d.activo = 1
				order by d.fecha_valor";
			}
			$qryApp = Helpers::qryAll($sApp);
			echo json_encode($qryApp);
		}

		public function actionDataAjax($doc_id = null, $doc_tipo_id = null) {
			$doc_id = $doc_id ? $doc_id : -1;
//			$s = "
//				SELECT d.id AS doc_id, d.anulado AS anu, DATE_FORMAT(d.fecha_valor,'%d.%m.%y') AS fecha_valor, c.nombre AS comprob, 
//							 d.sucursal, d.numero, d.total AS total_comprob, d.pago_a_cuenta_importe, d.pago_a_cuenta_detalle,
//							 1 AS numero_completo
//					FROM doc d
//						INNER JOIN talonario t ON d.talonario_id = t.id
//						INNER JOIN comprob c ON t.comprob_id = c.id
//				where d.id = $doc_id
//				";
//			$qry = Helpers::qry($s);
			$qry = Doc::model()->findByPk($doc_id);
			if (!$qry) {
				$qry = new Doc("insert", $doc_tipo_id);
			}else{
				$qry->fecha_valor = date("d/m/Y",  strtotime($qry->fecha_valor));
			}
			echo json_encode($qry->attributes);
		}

		public function actionValorVacioAjax() {
			$s = "
				select v.id, v.tipo, vt.nombre as tipo_nombre, v.importe
					from doc_valor v
						inner join doc_valor_tipo vt on vt.id = v.tipo
					where v.doc_id = -1
				";
			$qry = Helpers::qryFields($s);
			echo json_encode($qry);
		}
		public function actionValoresAjax($doc_id) {
			$s = "
				select v.id, v.tipo, vt.nombre as tipo_nombre, v.importe
					from doc_valor v
						inner join doc_valor_tipo vt on vt.id = v.tipo
					where v.doc_id = $doc_id
				";
			$qry = Helpers::qryAll($s);
			echo json_encode($qry);
		}

		public function actionSubmit(){
			
		}
	}
	