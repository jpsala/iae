<?php

class ExecController extends Controller {

    function actionIndex($action = "", $exec=0){
        $render = ($exec == 0) ? "render" : "renderPartial";
        $this->$render("exec",array("action"=>$action,"exec"=>$exec));
    }

}
