<?php

class FamiliaController extends Controller {

    public function actionAutoComplete() {
        $term = $_GET['term'];

        $criteria = new CDbCriteria();
        $criteria->limit = 20;
        $nombres = explode(",", $term);
        $apellido = trim($nombres[0]);
        $criteria->addCondition("apellido like \"%$apellido%\"");
        if (count($nombres) > 1) {
            $nombre = trim($nombres[1]);
            $criteria->addCondition("nombre like \"%$nombre%\"");
        }
        $alumnos = Alumno::model()->findAll($criteria);
        $ret = array();
        foreach ($alumnos as $alumno) {
            $ret[] = array(
                "id" => $alumno->Familia_id,
                'label' => "(" . $alumno->matricula . ") " . $alumno->nombreCompleto,
                'value' => $alumno->nombreCompleto);
        }
        echo json_encode($ret);
    }

}