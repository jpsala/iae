<?php

class FondosController extends Controller {

    //public static $layout = "column1";

    public function filters() {
        return array(
                'accessControl', // perform access control for CRUD operations
                'rights',
        );
    }

    public function accessRules() {
        return array(
                array('allow', // all users
                        'actions' => array(),
                        'users' => array('*'),
                ),
                array('allow', // authenticated user
                        'actions' => array(),
                        'users' => array('@'),
                ),
                array('allow', // roles
                        'actions' => array(),
                        'roles' => array('AdminEducativo'),
                ),
                array('deny', // deny all users
                        'users' => array('*'),
                ),
        );
    }

    public function actionCierraCaja() {
        //$this
        if (isset($_POST["cierra-caja"])) {
            Destino::getCajaUsuario()->cierra();
            $this->render("abreCierraCaja", array("abierta" => false, "confirmado" => true));
        } else {
            $this->render("abreCierraCaja", array("abierta" => false, "confirmado" => false));
        }
    }

    public function actionAbreCaja() {
        Destino::getCajaUsuario()->abre();
        $this->render("abreCierraCaja", array("abierta" => true));
    }

    public function actionMovimientoCaja($tipo) {

        $tipoComprob = ($tipo == "E" ? Comprob::AJUSTE_CAJA_ENTRADA : Comprob::AJUSTE_CAJA_SALIDA);
        $movCaja = new Doc("insert", $tipoComprob);
        $instancia = Destino::getCajaInstanciaActiva();
        $this->pageTitle = " " . ($tipo == "E" ? "Entrada" : "Salida") . " de caja ";
        $this->render("movimientoCaja", array(
                "instancia" => $instancia,
                "tipo" => $tipo,
                "movCaja" => $movCaja,
                )
        );
    }

    public function actionGetConceptosPaths($term) {
        echo Concepto::getPathsForAutoComplete($term);
    }

    public function actionConsultaCaja($todas = false) {
        $cajas = Destino::getCajas($todas);
        $this->render("consultaCaja", array("cajas" => $cajas));
    }

    public function actionTraeCajaInstancias($fecha, $destino_id) {
        $fecha = date("Y/m/d", mystrtotime($fecha));
        $tipoRetencion = DocValor::TIPO_RETENCION;
        $sql = "
          select di.*,
             (select sum(v.importe)
              from doc_valor v
                inner join doc d on v.doc_id = d.id
              where
              v.destino_instancia_id = di.id
                and d.anulado <> 1
                /*and v.tipo != $tipoRetencion*/
                and d.activo = 1) as total
          from destino_instancia di
              inner join destino d on d.id = di.Destino_id
          where ((DATE(di.fecha_apertura) = \"$fecha\") and
              destino_id = $destino_id)
          order by fecha_apertura
    ";
        $instancias = Helpers::qryAll($sql);
        $fechasQry = "select DATE(i.fecha_apertura) as fecha
        from destino_instancia i
          where i.destino_id = $destino_id
                /*and i.fecha_cierre is not null*/
      group by DATE(i.fecha_apertura)
    ";
        $fechasRows = Helpers::qryAll($fechasQry);
        $fechas = "[";
        foreach ($fechasRows as $fecha) {
            $fechas.= "\"" . date("Y-m-d", mystrtotime($fecha["fecha"])) . "\",";
        }
        $fechas.="]";
        $this->renderPartial("_consultaInstancias", array(
                "instancias" => $instancias,
                "fechas" => $fechas
        ));
    }

    public function actionTraeCajaResumida($instancia_id) {
        $instancia = DestinoInstancia::model()->findByPk($instancia_id);
        if (!$instancia)
            return;
        $fechas = date("d/m/y", mystrtotime($instancia->fecha_apertura))
                . " - " . ($instancia->fecha_cierre ? date("d/m/y", mystrtotime($instancia->fecha_cierre)) : "actual");
        $destino_id = $instancia->Destino_id;
        $wheres = array(
                "Destino_Instancia_id < $instancia_id",
                "Destino_Instancia_id = $instancia_id",
                "true"
        );
        //$tipoDocValorTarjeta = DocValor::TIPO_TARJETA;
        $rows = array();
        $tipoRetencion = DocValor::TIPO_RETENCION;
        foreach ($wheres as $where) {
            $qry = "
                SELECT v.tipo, t.nombre, sum(importe * comprob.signo_caja) as importe
                  FROM doc_valor v
                    inner join doc_valor_tipo t on t.id = v.tipo
                    inner join destino_instancia di on di.id = v.Destino_Instancia_id and di.Destino_id = $destino_id
                    inner join doc on doc.id = v.Doc_id
                    inner join talonario on talonario.id = doc.talonario_id
                    inner join comprob on comprob.id = talonario.comprob_id
                  where $where and  doc.anulado <> 1
                      /*and v.tipo != $tipoRetencion*/
                      and ((di.destino_id = v.destino_id) or v.destino_id is null)
                      and case when v.tipo = $tipoRetencion then v.retencion_estado is null else true end
                  group by t.nombre
                ";

            $rows[] = Helpers::qryAll($qry);
            // ve2($qry);
        }
        // vd2($rows[2]);
        //http://localhost/iae/index.php?r=fondos/ResumenCaja&instancia_id=2088
        $this->renderPartial("_consultaCajaResumida", array(
                "instancia" => $rows[1],
                "rowsSaldo" => $rows[0],
                "rowsSaldoActual" => $rows[2],
                "fechas" => $fechas
        ));
    }

    function actionCajaDetallada($instancia_id, $desglosa) {
        if (!$instancia_id)
            return;
        $tipoRetencion = DocValor::TIPO_RETENCION;

        foreach (DocValorTipo::model()->findAll("id <> " . DocValor::TIPO_CHEQUE_PROPIO ." and id <> " . DocValor::TIPO_RETENCION) as $dvt) {
            $selectSaldoAnt = "
                select sum(dv.importe * cs.signo_caja) as total
                  from doc_valor dv
                      inner join doc on doc.id = dv.Doc_id
                      inner join talonario ts on ts.id = doc.talonario_id
                      left join destino_instancia di on di.id = dv.Destino_Instancia_id
                      inner join comprob cs on cs.id = ts.comprob_id
                  where dv.Destino_Instancia_id < $instancia_id
                    and ((di.destino_id = dv.destino_id) or dv.destino_id is null)
                    /*and dv.tipo != $tipoRetencion*/
                    and doc.anulado = 0 and dv.tipo = $dvt->id";
            $saldosAnt[$dvt->nombre] = Helpers::qryScalar($selectSaldoAnt);

            $select = "select dv.destino_id as dest1, di.destino_id as dest2,
                       sum(dv.importe * c.signo_caja) as importe,
                       d.fecha_creacion, dv.obs, d.numero,
                       d.pago_a_cuenta_detalle, d.pago_a_cuenta_importe, d.saldo, d.total,
                       c.id as comprob_id, c.nombre as comprob_nombre,
                       c.signo_caja as comprob_signo, t.punto_venta_numero, d.detalle as doc_detalle,
                       dv.numero as valor_numero, dv.tipo as valor_tipo,
                       case
                        when socio.alumno_id
                          then concat(a.apellido,\", \",a.nombre)
                        else
                          p.razon_social
                        end as socio_nombre
                  from doc_valor dv
                      inner join doc d on d.id = dv.Doc_id
                      inner join talonario t on t.id = d.talonario_id
                      inner join comprob c on c.id = t.comprob_id
                      left join destino_instancia di on di.id = dv.Destino_Instancia_id
                      left join socio on socio.id = d.socio_id
                      left join alumno a on socio.alumno_id = a.id
                      left join proveedor p on socio.proveedor_id = p.id
                  where dv.Destino_Instancia_id = $instancia_id
                        and dv.tipo = $dvt->id
                        and ((di.destino_id = dv.destino_id) or dv.destino_id is null)
                        /*and dv.tipo != $tipoRetencion*/
                        and (d.anulado = 0)
                  group by  dv.destino_id , di.destino_id ,d.fecha_creacion, dv.obs, d.numero, d.pago_a_cuenta_detalle, d.pago_a_cuenta_importe, d.saldo, d.total,
                    c.id, c.nombre , c.signo_caja , t.punto_venta_numero,
                    d.detalle,  dv.tipo
                  order by dv.tipo, fecha_creacion, d.numero
            ";

            $qrys[$dvt->nombre] = Helpers::qryAll($select);
        }
        if ($desglosa == 'true') {
            $this->renderPartial("_cajaDetallada_1", array(
                    "instancia_id" => $instancia_id,
                    "qrys" => $qrys,
                    "saldosAnt" => $saldosAnt,
            ));
        } else {

            $this->renderPartial("_cajaDetallada", array(
                    "instancia_id" => $instancia_id,
                    "qrys" => $qrys,
                    "saldosAnt" => $saldosAnt,
            ));
        }
    }

    public function actionResumenCaja($instancia_id) {
        $this->renderPartial("/fondos/planillaResumenCajaAjax", array(
                "instancia_id" => $instancia_id));
    }

}

?>
