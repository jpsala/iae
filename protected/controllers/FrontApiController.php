<?php

class FrontApiController extends Controller
{
	private $body, $resp, $socio_id, $get, $post, $headers, $auth_token, $minutesForTimeout = 10;
	public function missingAction($action){
    $this->chkRequestForCors();
		$this->setRequestData();
    $publicActions = [
      'auth', 
      'logout', 
      'login', 
      'activate', 
      'register',
      'emisionComprobantes'
    ];
		if (!in_array($action, $publicActions)) {
			$this->chkSession();
		}
		$this->exec($action);
		return true;
	}

	private function chkRequestForCors()	{
    header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET,PUT,POST,DELETE,PATCH,OPTIONS');
    header("Access-Control-Allow-Headers: authorization, Content-Type, Depth, User-Agent, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control");
		if ("OPTIONS" == $_SERVER['REQUEST_METHOD']) {
			exit(0);
    }
  }
  
	private function setRequestData()	{ 
		$this->body = json_decode(file_get_contents('php://input'));
		$this->resp = new stdClass();
    $this->resp->status = 200;
    $this->resp->minutesForTimeout = $this->minutesForTimeout;
		$this->get = $_GET;
		$this->post = $_POST;
		$this->headers = apache_request_headers();
		$auth = isset($this->headers['authorization']) ? $this->headers['authorization'] : false;
		$auth = $auth ? $auth : (isset($this->headers['Authorization']) ? $this->headers['Authorization'] : 'no authorization in headers');
		$this->auth_token = $auth;
  }
  
  protected function chkSession(){
    $jwt = '';
    try{
      $decode = Yii::app()->JWT->decode($this->auth_token);
      $date = date('d/m/Y', $decode->date);
      $diff = time() - $decode->date;
      $minutes = round(abs($diff) / 60, 2);
      if($minutes > $this->minutesForTimeout){
        $this->resp->status = 402;
        $this->resp->error = 'Timeout';
        exit(json_encode($this->resp));
      }
      $decode->date = time();
      $jwt = Yii::app()->JWT->encode($decode);
      $this->resp->access_token = $jwt;
      $this->socio_id = $decode->socio_id;
      $this->resp->minutes = $minutes;
			$this->resp->status = 200;

    } catch(Exception $e) {
      $this->resp = new stdClass();
      $this->resp->access_token = $this->auth_token;
			$this->resp->status = 401;
      $this->resp->statusMsj = 'Error en token de autenticación' .json_encode($e);
			exit(json_encode($this->resp));
    }
		if ("OPTIONS" == $_SERVER['REQUEST_METHOD']) {
			exit(0);
		}
	}


	private function exec($action)	{
		$actionFile = getcwd() . '/protected/controllers/apiFront/' . $action . '.php';
		if (!file_exists($actionFile)) {
			throw new Exception('no existe ' . $actionFile);
		}
    // $controller = $this;
		require($actionFile);
	}

}
