<?php
/*
  Genera unas notas de crédito para claudia por errores de haydee
*/
class GeneraController extends CController {

  public function actionGeneraNotas(){
    $matriculasComma = "4907,4387,4615,4513,5351,4594,4758,4494,4682,4612,5341,4611,4578,4606,4521,4598,4473,4776,4764,4457,4569,4866,5049,4917,4586,4568,4588,4566,5048,4583,4585,4325,4160,4790,4175,4341,4285,4340,4891,4426,4924,4650,4415,4419,4416,4407,4410,4421,4425,4411,4412,4878,4879,4260,4478,4251,4318,4497,4268,4477,4300,4337,4284,4262,4874,4558,4592,4881,4280,4368,4361";
    $importe = 80;
    // $matriculasComma = "4263,4449,4530,4408,4785,4476,4617,4464,4463,4618,4632,4634,4789,4633,5155,4557,4629,4550,4474,4483,4466,4840,4548,4546,5156,4602,4591,4471";
    // $importe = 40;
    $detalle = "NC por error en carga de transportes";
    $this->generaNotas($matriculasComma, 3, 1, $importe, null, 1936, $detalle);
  }

  public function actionGeneraNotasDebito(){
    $matriculasComma = "5335,5134,5133,4613,4614,4600,4647,4392,4756,4757,5336,4386,4772,5334,4994,4860,4911,4593,4531,4599,4909,4910,5342,4605,4603,4662,4698,4763,4954,4919,4610,4908,4596,4601,4587,4658,4759,4608,4609,4660,4770,4762,5337,4769,5012,4768,4752,4771,4854,4448,4947,5205,4760,4717,4765,4455,4590,4773,5338,5339,4775,5013,4584,5363,4921,4567,4511,4659,4472,4571,4890,4544,4570,4579,5356,4577,5343,4261,4973,4246,4552,4286,4264,4275,4255,4201,4862,4249,4235,4277,4529,4643,4326,5346,4250,4243,4258,4306,4257,4198,4640,4265,4295,4259,4293,4642,4339,4417,4639,4420,4402,4254,4274,4279,4342,4492,4276,4880,5173,4199,4405,4649,4404,4561,4894,4974,4414,4423,4424,4422,4406,4486,4413,4403,4644,4490,4892,4539,4248,4755,4278,4694,4955,4864,4495,4281";
    $importe = 80;
    // $matriculasComma = "4624,4253,4305,4394,4683,4520,4393,4409,5146,4389,4485,4266,4779,5145,4777,4778,4396,4522,4914,4395,5144,4397,4519,4625,4781,4783,4388,5345,4447,4391,4995,4496,4784,4782,4469,5203,4468,4870,5148,5159,4283,4453,4913,5151,5192,4637,4631,4780,5161,4616,5150,4623,4788,4622,4621,4627,4749,4626,4620,5152,4636,5208,4912,4635,4648,5154,5183,4507,4547,4549,4451,5359,4922,4551,4475,4545,5153,4369,4470,5157,4559";
    // $importe = 40;
    $detalle = "";
    $this->generaNotas($matriculasComma, 2, 1, $importe, null, 1948, $detalle, true);
  }

  private function generaNotas($matriculasComma, $comprob_id, $punto_venta_id, $importe, $fecha, $concepto_id, $detalle, $aplicaAuto){
    $fecha = $fecha ? $fecha : date('d/m/Y'/*, strtotime($fechaRaw)*/);
    $detalle = $detalle ? $detalle : '';
    $aplicaAuto = $aplicaAuto ? 'true' : 'false';
    $matriculas = split(',', $matriculasComma);
    $talonario    = Talonario::model()->findBySql("
      select t.* 
        from talonario t
          inner join comprob c on c.id = t.comprob_id AND c.id = $comprob_id AND t.punto_venta_numero = $punto_venta_id
    ");
    $tr = Yii::app()->db->beginTransaction();
    foreach($matriculas as $matricula){
      $socio_id = Helpers::qryScalar("
        select s.id from socio s
          inner join alumno a on a.id = s.alumno_id
        where a.matricula = $matricula
      ");

      $numero  = $talonario->numero + 1; 
      $doc["numero"] = $punto_venta_id.'-'.$numero;
      $doc["comprob_id"] = $comprob_id;
      $doc["fecha_valor"] = $fecha;
      $doc["total"]=$importe;
      $doc["concepto_id"]=$concepto_id;
      $doc["detalle"]=$detalle;
      $doc["comprob_id"]=$talonario->comprob_id;
      $doc["Socio_id"]=$socio_id;
      
      $data   = array();
      $data['doc'] = $doc; 
      $data['aplicacionesAuto'] = $aplicaAuto;
      // vd2($data);
      $errores = array();
      Doc::altaDoc($data, $errores, false);
      $talonario->numero++;
      $talonario->save();
      if(count($errores) > 0) {
          $retErrores[] = $errores;
          echo 'errores';
          var_export($errores);
          echo 'post';
          var_export($data);
          $tr->rollback();
          die;
      }

    }
    $tr->commit();
  }  
}