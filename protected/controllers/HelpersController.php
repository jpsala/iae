<?php

class HelpersController extends Controller {

  public function actionError() {
    $params = $this->getActionParams();
    $data = isset($_GET["data"]) ? $_GET["data"] : "";
    $this->render("/helpers/error", array("error" => $_GET["mensaje"], "data"=>$data));
  }

}