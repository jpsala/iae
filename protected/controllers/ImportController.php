<?PHP

set_time_limit(1800);

class ImportController extends RController {

    public function filters() {
        return array(
                'rights',
        );
    }

    public function actionBorraTodo() {
        $this->actionBorraDocs();
        $this->actionBorraTodoMenosDocs();
    }

    public function actionBorraDocs() {
        $tablas = array(
                "doc_valor",
                "doc_det",
                "doc_apl",
                "doc_apl_cab",
                "doc",
        );
        $tr = Yii::app()->db->beginTransaction();
        Yii::app()->db->createCommand("
            update doc_valor set doc_valor_id = null
      ")->execute();
        foreach ($tablas as $tabla) {
            echo Yii::app()->db->createCommand("
        delete from $tabla
      ")->execute() . " registros borrados de $tabla<br/>";
        }
        $tr->commit();
    }

    public function actionBorraTodoMenosDocs() {
        $tablas = array(
                //"conducta",
                //"concepto",
                "novedad",
                "novedad_cab",
                //"inasistencia_detalle",
                //"inasistencia",
                //"observaciones_alumno",
                "liquid_conf",
                //"nota",
                //"alumno_division",
                //"alumno_grupo",
                //"asignatura",
                //"orientacion",
                //"division",
                //"anio",
                //"nivel",
                //"articulo",
//        "socio",
//        "alumno",
//        "proveedor",
        );
        $tr = Yii::app()->db->beginTransaction();
        foreach ($tablas as $tabla) {
            echo Yii::app()->db->createCommand("
        delete from $tabla
      ")->execute() . " registros borrados de $tabla<br/>";
        }
        $tr->commit();
    }

    public function allowedActions() {
        return '';
    }

    public function actionSaldosClientes2($fechaParaSaldo = null) {
        $i = 0;
        $db = Yii::app()->fb->connection;
        $fechaParaSaldo = $fechaParaSaldo ? $fechaParaSaldo : date("Y/m/d", time());
        $sth = ibase_query($db, "
            select a.nombre,a.matricula, 
                    (select importe from te_saldo_no_cc(a.matricula, '$fechaParaSaldo')) as saldo,
                    (select saldo from calcula_saldo(a.oid_cliente, '$fechaParaSaldo')) as saldo_viejo
            from te_alumnos a
            where (select saldo from calcula_saldo(a.oid_cliente, '$fechaParaSaldo')) <> 0 or
                         (select importe from te_saldo_no_cc(a.matricula, '$fechaParaSaldo')) <> 0
            order by a.apellido, a.nombre
        ");
        $this->render("saldosComparacion", array("sth" => $sth, "fechaParaSaldo" => $fechaParaSaldo));
    }

    public function actionSaldosClientes() {
        // 3516 = 0 // 101
        //$fechaParaSaldo = date("Y/m/d", time());
        $fechaParaSaldo = date("Y/m/d", time()); //"2013/06/28";
        $db = Yii::app()->fb->connection;
        $sth = ibase_query($db, "
            select a.nombre,a.matricula, (select importe from te_saldo_no_cc(a.matricula, '$fechaParaSaldo')) as saldo
                            from te_alumnos a
                            where (select saldo from calcula_saldo(a.oid_cliente, '$fechaParaSaldo')) <> 0
        ");
        $nuevos = 0;
        $total = 0;
        $encero = 0;
        /* @var $d Doc */
        /* @var $a Alumno */
        $tr = Yii::app()->db->beginTransaction();
        while ($row = ibase_fetch_object($sth)) {
            $total++;
            $a = Alumno::model()->find("matricula='$row->MATRICULA'", array("matricula" => $row->MATRICULA));
            if (!$a) {
                echo("Alumno $row->NOMBRE no fué encontrado<br/>");
                continue;
                ;
            }
            $saldo = round($row->SALDO, 2);
            if ($saldo == 0) {
                $encero++;
                continue;
            }
            $comprob_id = $saldo < 0 ? Comprob::NC_VENTAS : Comprob::ND_VENTAS;
            $c = Comprob::model()->findByPk($comprob_id);
            $socio_id = $a->socio->id;
            $doc = Doc::model()
                    ->with("talonario", "talonario.comprob")
                    ->find("Socio_id = $socio_id and detalle = \"Saldo inicial\"");
            if (!$doc) {
                $doc = new Doc("insert", $comprob_id);
                $doc->Socio_id = $a->socio->id;
                $doc->detalle = "Saldo inicial";
                $nuevos++;
            } else {
                if ($saldo < 0) {
                    $doc->talonario_id = $doc->getTalonarioUsuario(Comprob::NC_VENTAS);
                } else {
                    $doc->talonario_id = $doc->getTalonarioUsuario(Comprob::ND_VENTAS);
                }
            }
            $doc->concepto_id = 1773;
            $doc->total = abs($saldo);
            $doc->saldo = abs($saldo);
            if (!$doc->save()) {
                echo("No se pudo grabar el saldo de $a->nombreCompleto<br/>");
                var_dump($doc->errors);
            }
        }
        $tr->commit();
        echo "Total: $total<br/>Insertados: $nuevos<br/> En cero(no importados): $encero";
    }

    public function actionSaldosProveedores() {
        $db = Yii::app()->fb->connection;
        $sth = ibase_query($db, "
            select p.*, (select sum(c.debe) - sum(c.haber) from t_cc_prove c where c.oid_prove = p.oid_prove and c.anulado = 0) AS saldo
              from t_proveedores p
        ");

        $nuevos = 0;
        $total = 0;
        $encero = 0;

        $tr = Yii::app()->db->beginTransaction();
        while ($row = ibase_fetch_object($sth)) {
            $total++;
            $id_prove = trim($row->ID_PROVE);
            $p = Proveedor::model()->find("codigo='$id_prove'");
            if (!$p) {
                $p = Proveedor::model()->find("id='$id_prove'");
            }
            if (!$p) {
                ve($id_prove);
                $p = new Proveedor();
                $this->importaProveedor($p, $row);
                if (!$p->save()) {
                    var_dump($row);
                    var_dump($p->attributes);
                    var_dump($p->errors);
                    die;
                } else {
                    $s = new Socio();
                    $s->Proveedor_id = trim($p->id);
                    $s->tipo = "P";
                    var_dump($s->attributes);
                    if (!$s->save()) {
                        var_dump($row);
                        var_dump($s->errors);
                        die;
                    }
                }
            }
            $saldo = $row->SALDO;
            if ($saldo == 0) {
                $encero++;
                continue;
            }
            //$comprob_id = $saldo < 0 ? Comprob::NC_VENTAS : Comprob::ND_VENTAS;
            $comprob_id = $saldo > 0 ? Comprob::NC_COMPRAS : Comprob::ND_COMPRAS;
            echo "$p->nombre: $saldo<br/>";
            $socio_id = Socio::model()->find("Proveedor_id=$p->id")->id;
            $doc = Doc::model()
                    ->with("talonario", "talonario.comprob")
                    ->find("Socio_id = $socio_id and detalle = \"Saldo inicial\"");
            if ($doc) {
                $doc->delete();
            }
            $docNuevo = new Doc("insert", $comprob_id);
            $docNuevo->Socio_id = $socio_id;
            $docNuevo->total = abs(round($saldo, 2));

            $docNuevo->saldo = $comprob_id == Comprob::NC_COMPRAS ? 0 : abs(round($saldo, 2));
            $docNuevo->detalle = "Saldo inicial";
            if (!$docNuevo->save()) {
                echo("No se pudo grabar el saldo de $p->nombreCompleto<br/>");
                var_dump($docNuevo->errors);
            }
            $docNuevo->saldo = $comprob_id == Comprob::NC_COMPRAS ? 0 : abs(round($saldo, 2));
            $docNuevo->save();
            $nuevos++;
        }
        $tr->commit();
        echo "Total: $total<br/>Insertados: $nuevos<br/> En cero(no importados): $encero";
    }

    public function actionDocs() {

        $db = Yii::app()->fb->connection;
        Yii::app()->fb->connection;

        $data = array();
        $i = 0;
        $data[$i]["ct"] = array(
                "nombre" => "factura-venta",
                "afectacion" => "H",
                "compra_venta" => "V"
        );
        $data[$i]["c"] = array(
                "nombre" => "factura",
                "abreviacion" => "fac",
                "numeracion_manual" => 0
        );
        $data[$i]["pv"] = array(
                "terminal" => "caja1",
                "numero" => 1,
                "activo" => 1
        );

        $i++;
        $data[$i]["ct"] = array(
                "nombre" => "recibo",
                "afectacion" => "H",
                "compra_venta" => "V"
        );
        $data[$i]["c"] = array(
                "nombre" => "recibo",
                "abreviacion" => "fac",
                "numeracion_manual" => 0
        );
        $data[$i]["pv"] = array(
                "terminal" => "caja1",
                "numero" => 1,
                "activo" => 1
        );

        foreach ($data as $d) {
            $nombre = $d["ct"]["nombre"];
            $ct = ComprobTipo::model()->find("nombre = '$nombre'");

            if (!$ct) {
                $ct = new ComprobTipo();
                $ct->nombre = $d["ct"]["nombre"];
                $ct->tipo = "what";
                $ct->afectacion = $d["ct"]["afectacion"];
                $ct->compra_venta = $d["ct"]["compra_venta"];
                if (!$ct->save()) {
                    print_r($ct->errors);
                    return;
                    throw new Exception("Error grabando ComprobTipo");
                }
            }
            $c = Comprob::model()->find("nombre = 'factura'");
            if (!$c) {
                $c = new Comprob();
                $c->Comprob_Tipo_id = $ct->id;
                $c->nombre = $d["c"]["nombre"];
                $c->abreviacion = $d["c"]["abreviacion"];
                $c->numeracion_manual = $d["c"]["numeracion_manual"];
                if (!$c->save()) {
                    throw new Exception("Error grabando Comprob");
                }
            }

            $pv = PuntoVenta::model()->find("terminal = 'caja1'");
            if (!$pv) {
                $pv = new PuntoVenta();
                $pv->terminal = $d["pv"]["terminal"];
                $pv->numero = $d["pv"]["numero"];
                $pv->activo = $d["pv"]["activo"];
                if (!$pv->save()) {
                    throw new Exception("Error grabando PuntoDeVenta");
                }
            }

            $cpv = ComprobPuntoVenta::model()->find("Comprob_id = $c->id");
            if (!$cpv) {
                $cpv = new ComprobPuntoVenta();
                $cpv->Comprob_id = $c->id;
                $cpv->numero = 1;
                $cpv->Punto_Venta_id = $pv->id;
                if (!$cpv->save()) {
                    throw new Exception("Error grabando ComprobPuntoVenta");
                }
            }
        }
    }

    public function borraTabla($tabla) {
        Yii::app()->db->createCommand("delete from $tabla")->execute();
    }

    public function actionFamilias() {
        $this->borraTabla("familia");
        $db = Yii::app()->fb->connection;
        $sth = ibase_query($db, "select * from te_familias");
        $tr = Yii::app()->db->beginTransaction();
        while ($row = ibase_fetch_object($sth)) {
            $f = new Familia();
            $f->id = $row->ID_FAMILIA;
            $f->save();
        }
        $tr->commit();
    }

    public function actionInitCuidado() {
        $this->actionBorraTodoMenosDocs();
//    $this->actionAlumnos();
//    $this->actionParientes();
        $this->actionArticulos();
        $this->actionAsignaciones();
        $this->actionMaterias();
        $this->actionActualizaAnios();
//    $this->actualizaAlumnos();
        $this->actionConceptos();
//    $this->saldosClientes();
//    $this->saldosProveedores();
        $this->actionAsignaAsignaturasADivisiones();
    }

    public function actionAsignaAsignaturasADivisiones() {
        $tr = Yii::app()->db->beginTransaction();
        foreach (Anio::model()->findAll() as $anio) {
            foreach (Division::model()->findAll("Anio_id=$anio->id") as $division) {
                foreach (Asignatura::model()->findAll("Anio_id = $anio->id") as $asignatura) {
                    if (DivisionAsignatura::model()->find("asignatura_id = $asignatura->id and division_id = $division->id")) {
                        ve("Ya existe divisionAsignatura con division_id = $division->id y asignatura_id = $asignatura->id");
                    } else {
                        $da = new DivisionAsignatura();
                        $da->Asignatura_id = $asignatura->id;
                        $da->Division_id = $division->id;
                        if (!$da->save()) {
                            ve("error grabando DivisionAsignatura");
                            ve($da->errors);
                            ve($da->attributes);
                        }
                    }
                }
            }
        }
        $tr->commit();
    }

    public function actionAlumnos() {
        $db = Yii::app()->fb->connection;
        $sth = ibase_query($db, "select STRLEN(a.obs) || ' ' as lenobs,
          a.matricula, a.nombre, a.sexo, a.calle_nombre, a.calle_numero, a.calle_piso,
          a.calle_departamento, a.NRO_DOC, a.FECHA_NAC, a.EMAIL, a.TE_PARTICULAR, a.TE_MENSAJE,
          a.ID_TIPO_DOC,a.ID_FAMILIA, a.LUGAR_NAC, a.porc_beca, activo, f.obs_cobranza
        from te_alumnos a 
        inner join te_familias f on a.id_familia = f.id_familia
        order by matricula");
        $nuevos = 0;
        $total = 0;
        /* @var $a Alumno */
        $tr = Yii::app()->db->beginTransaction();
        while ($row = ibase_fetch_object($sth)) {
            echo $row->LENOBS . "," . $row->MATRICULA . "," . $row->NOMBRE . "<br/>";
            $total++;
            $a = Alumno::model()->find("matricula='$row->MATRICULA'", array(
                    "matricula" => $row->MATRICULA)
            );
            if (!$a) {
                $nuevos++;
                $a = new Alumno("import");
                $this->importaAlumno($a, $row);
                $this->parseNombre(substr($row->NOMBRE, 0, 45), $a);
                echo $a->apellido;
                if (!isset($row->ID_FAMILIA)) {
                    ve($row);
                    die;
                }
                $f = Familia::model()->findByPk($row->ID_FAMILIA);
                if (!$f) {
                    $f = new Familia();
                    $f->id = $row->ID_FAMILIA;
                    if (!$f->save()) {
                        $tr->rollback();
                        throw new Exception("No se pudo grabar la familia $f->id");
                    }
                }
                $a->Familia_id = $f->id;
                if (!$a->validate()) {
                    print_r($a->attributes);
                    echo "<br/><br/>";
                    print_r($a->errors);
                    $tr->rollback();
                    die;
                }
                if (!$a->save()) {
                    $tr->rollback();
                    //throw new Exception("No se pudo grabar el alumno $a->matricula");
                    throw new Exception("No se pudo dar de alta al alumno $a->matricula");
                } else {
                    $s = Socio::model()->find("Alumno_id=$a->id");
                    if ($s) {
                        $tr->rollback();
                        throw new Exception("Socio encontrado $s->Alumno_id, Mal!?");
                    } else {
                        $s = new Socio();
                        $s->Alumno_id = $a->id;
                        $s->tipo = "alumno";
                        $s->save();

                        $s->tipo = "A";
                        if (!$s->save()) {
                            throw new Exception("No se pudo crear el socio \r" . json_encode($s->errors));
                        }
                    }
                }
            }
            $this->importaAlumno($a, $row);
            $this->parseNombre(substr($row->NOMBRE, 0, 45), $a);
            $a->obs_cobranza = $row->OBS_COBRANZA;
            $a->calle = trim($row->CALLE_NOMBRE) ? trim($row->CALLE_NOMBRE) : "ingresar";
            $a->sexo = trim($row->SEXO);
            $a->beca = $row->PORC_BECA;
            $a->activo = $row->ACTIVO;
            $a->numero = trim($row->CALLE_NUMERO);
            $a->piso = trim($row->CALLE_PISO);
            $a->departamento = trim($row->CALLE_DEPARTAMENTO);
            $a->numero_documento = trim($row->NRO_DOC);
            $a->fecha_nacimiento = $row->FECHA_NAC ?
                    Helpers::date("Y/m/d", mystrtotime($row->FECHA_NAC)) :
                    Helpers::date("Y/m/d", mystrtotime("01/01/1900"));
            $a->email = trim($row->EMAIL) ? trim($row->EMAIL) : "ingresar@ingresar.com";
            $a->telefono_1 = trim($row->TE_PARTICULAR) ? trim($row->TE_PARTICULAR) : "ingresar";
            $a->telefono_2 = trim($row->TE_MENSAJE);
            if (!$a->save()) {
                print_r($a->errors);
                die;
            }
            $s = Socio::model()->find("Alumno_id=$a->id");
            if (!$s) {
                $s = new Socio();
                $s->Alumno_id = $a->id;
                $s->tipo = "a";
                if (!$s->save()) {
                    $tr->rollback();
                    throw new Exception("No se pude agregar el socio con Alumno_id = $a->id");
                }
            }
            $a->fecha_nacimiento = Helpers::date("Y/m/d", $row->FECHA_NAC);
//        $a->beca = $row->
            if (!$a->save()) {
                var_dump($a->errors);
                die;
            }
//        continue;
//        $this->importaAlumno($a, $row);
//
//        if (!$a->save()) {
//          print_r(trim($row->EMAIL));
//          die;
//          print_r($a->errors);
//          die;
//          throw new Exception("mal");
//        }
//        $s = Socio::model()->find("Alumno_id=$a->id");
//        if (!$s) {
//          $s = new Socio();
//          $s->Alumno_id = $a->id;
//          $s->tipo = "a";
//          if (!$s->save()) {
//            $tr->rollback();
//            throw new Exception("No se pude agregar el socio con Alumno_id = $a->id");
//          }
//        }
        }
        $tr->commit();
        echo "Total: $total<br/>Nuevos:$nuevos<br/>";
    }

    function importaAlumno(Alumno $a, $row) {
        $a->matricula = $row->MATRICULA;
        $a->sexo = trim($row->SEXO);
        $a->calle = trim($row->CALLE_NOMBRE) ? trim($row->CALLE_NOMBRE) : "ingresar";
        $a->numero = trim($row->CALLE_NUMERO);
        $a->numero_documento = trim($row->NRO_DOC);
        $a->localidad_id = trim($row->LUGAR_NAC) == 1 ? "2" : "790237";
        $a->fecha_nacimiento = Helpers::date("Y/m/d", $row->FECHA_NAC);
        $a->email = trim($row->EMAIL) ? trim($row->EMAIL) : "ingresar@ingresar.com";
        $a->telefono_1 = trim($row->TE_PARTICULAR) ? trim($row->TE_PARTICULAR) : "ingresar";
        $a->telefono_2 = trim($row->TE_MENSAJE);
        if ($row->ID_TIPO_DOC == "C") {
            $a->Tipo_Documento_id = 1;
        } elseif ($row->ID_TIPO_DOC == "D") {
            $a->Tipo_Documento_id = 2;
        } elseif ($row->ID_TIPO_DOC == "E") {
            $a->Tipo_Documento_id = 3;
        } elseif ($row->ID_TIPO_DOC == "P") {
            $a->Tipo_Documento_id = 7;
        } elseif ($row->ID_TIPO_DOC == "Z") {
            $a->Tipo_Documento_id = 2;
        } else {
            $a->Tipo_Documento_id = 1;
        }
    }

    public function actionParientes() {
        $db = Yii::app()->fb->connection;


//    $sth = ibase_query($db, "
//        ");

        $sth = ibase_query($db, "
            select  a.matricula, p.nombre as PARIENTE_NOMBRE, z.nombre as parentezco,
                p.fecha_nac, p.profesion_actividad, p.te_particular, p.te_trabajo,
                p.te_celular, p.id_tipo_doc, p.nro_doc, p.domicilio_computado,
                p.vive, p.email, p.email2, p.lugar_nac, p.id_localidad, p.cod_pos,
                a.vive_con, p.id_parentezco,
                p.calle_nombre, p.calle_numero, p.calle_piso, p.calle_departamento,
                p.id_localidad, p.te_particular, p.te_trabajo, p.te_celular, p.nacionalidad
            from te_alumnos a
                inner join te_padres p on p.id_familia = a.id_familia
                inner join te_parentezco z on z.id_parentezco = p.id_parentezco
            
            /*where a.oid_,c,liente = 9290*/
            order by p.calle_nombre asc/*a.id_familia*/"
        );
        $nuevos = 0;
        $total = 0;
        /* @var $a Alumno */
        $tr = Yii::app()->db->beginTransaction();
//echo "cuidado, borra todo antes";die;
        Yii::app()->db->createCommand("update alumno set vive_con_id = null")->execute();
        Yii::app()->db->createCommand("delete from pariente")->execute();

        while ($row = ibase_fetch_object($sth)) {
            $total++;
            $a = Alumno::model()->find("matricula='$row->MATRICULA'", array(
                    "matricula" => $row->MATRICULA,
                    )
            );
            $row->PARIENTE_NOMBRE = str_replace(",", " ", $row->PARIENTE_NOMBRE);
            if (!$a) {
                $nuevos++;
                echo "Primero hay que importar los alumnos ($row->MATRICULA)<br/>";
                die;
            } else {
                $pariente_tipo_nombre = mb_convert_case($row->PARENTEZCO, MB_CASE_TITLE, "UTF-8");
                $pt = ParienteTipo::model()->find("nombre = \"$pariente_tipo_nombre\"");
                if (!$pt) {
                    $pt = new ParienteTipo();
                    $pt->nombre = $pariente_tipo_nombre;
                    $pt->save();
                }
                $f = Familia::model()->findByPk($a->Familia_id);
                if (!$f) {
                    $tr->rollback();
                    throw new Exception("Primere se deben importar las familias");
                }
                $ptr = new Pariente();
                $this->parseNombre(substr($row->PARIENTE_NOMBRE, 0, 45), $ptr);
                $p = Pariente::model()->find("Familia_id = $a->Familia_id and
            Pariente_Tipo_id = $pt->id and
            nombre = \"$ptr->nombre\" and apellido = \"$ptr->apellido\"");
                $ptr = new Pariente();
                $this->parseNombre($row->PARIENTE_NOMBRE, $ptr);
                $p = Pariente::model()->find("Familia_id = $a->Familia_id and
             Pariente_Tipo_id = $pt->id and
             nombre = \"$ptr->nombre\" and apellido = \"$ptr->apellido\"");
                if (!$p) {
                    $p = new Pariente();
                    $this->parseNombre($row->PARIENTE_NOMBRE, $p);
                    $p->Familia_id = $a->Familia_id;
                    $p->Pariente_Tipo_id = $pt->id;
                    $p->nombre = $ptr->nombre;
                    $p->apellido = $ptr->apellido;
//          $p->save();
//                    p.calle_nombre, p.calle_numero, p.calle_piso, p.calle_departamento, p.id_localidad,
//                    p.te_particular, p.te_trabajo, p.te_celular,
                    $p->calle = trim($row->CALLE_NOMBRE);
                    $p->numero = $row->CALLE_NUMERO;
                    $p->departamento = trim($row->CALLE_DEPARTAMENTO);
                    $p->piso = $row->CALLE_PISO;
                    $p->localidad_id1 = trim($row->ID_LOCALIDAD) == "1" ? "2" : "790237";
                    $p->lugar_nacimiento = trim($row->LUGAR_NAC) == "1" ? "2" : "790237";
                    $nacionalidad = (int) trim($row->NACIONALIDAD);
                    if ($nacionalidad != 2) {
                        $p->nacionalidad = 247;
                        if (is_numeric($nacionalidad) and $nacionalidad > 0) {
                            $sthPais = ibase_query($db, "
                select id_pais,nombre from t_paises p where p.id_pais = $nacionalidad
              ");
//echo $nacionalidad."<br/>"
                            if ($sthPais and $sthPais > 0) {
//$nacionalidadRow = ibase_fetch_row($sthPais);
                                $p->nacionalidad_texto = "$nacionalidad";
                            } else {
                                var_dump("no encontrado $sthPais");
                                $p->nacionalidad_texto = "$nacionalidad";
                            }
                        } else {
                            $p->nacionalidad_texto = "$nacionalidad";
                        }
                    } else {
                        $p->nacionalidad = "2";
                    }
                    $p->fecha_nacimiento = $row->FECHA_NAC;
                    $p->numero_documento = $row->NRO_DOC;
                    $p->email = $row->EMAIL;
                    $p->email1 = $row->EMAIL2;
                    $p->telefono_casa = trim($row->TE_PARTICULAR);
                    $p->telefono_celular = trim($row->TE_CELULAR);
                    $p->telefono_trabajo = trim($row->TE_TRABAJO);
                    $p->vive = $row->VIVE;
                    if ($row->ID_TIPO_DOC == "C") {
                        $p->tipo_documento_id = 1;
                    } elseif ($row->ID_TIPO_DOC == "D") {
                        $p->tipo_documento_id = 2;
                    } elseif ($row->ID_TIPO_DOC == "E") {
                        $p->tipo_documento_id = 3;
                    } elseif ($row->ID_TIPO_DOC == "P") {
                        $p->tipo_documento_id = 7;
                    } elseif ($row->ID_TIPO_DOC == "Z") {
                        $p->tipo_documento_id = 2;
                    } else {
                        $p->tipo_documento_id = 1;
                    }
                    if (!$p->save()) {
                        print_r($p->attributes);
                        die;
                        throw new Exception("Error grabando el pariente:");
                    }
                }
                if ($row->ID_PARENTEZCO == $row->VIVE_CON) {
                    $a->vive_con_id = $p->id;
                    $a->save();
                }
            }
        }
        $tr->commit();
        Yii::app()->db->createCommand("update localidad set provincia_id=2 where provincia_id = 1818")->execute();
        return;
    }

    public function actionAsignaciones() {
        $db = Yii::app()->fb->connection;
        $sth = ibase_query($db, "
        select a.matricula, n.id_nivel, n.nombre as nombre_nivel, c.id_curso_division, c.nombre as nombre_curso, a.activo
            from te_alumnos a
                inner join te_niveles n on n.id_nivel = a.id_nivel
                inner join te_cursos_divisiones c on c.id_nivel = a.id_nivel and c.id_curso_division = a.id_curso_division
            where a.id_nivel not in (6,8) /*and a.nombre = ?*/
            order by n.orden, c.nombre
    ");
        $total = 0;
        $articulo_id = Articulo::model()->find()->id;
        /* @var $a Alumno */
        $tr = Yii::app()->db->beginTransaction();
        while ($row = ibase_fetch_object($sth)) {
            $total++;
            $a = Alumno::model()->find("matricula='$row->MATRICULA'", array("matricula" => $row->MATRICULA));
            if (!$a) {
                echo "no está $row->MATRICULA</br>";
            } else {
                $nombreNivel = mb_convert_case($row->NOMBRE_NIVEL, MB_CASE_TITLE, "UTF-8");
                $nivel = Nivel::model()->find("nombre = \"$nombreNivel\"");
                if (!$nivel) {
                    $nivel = new Nivel();
                    $nivel->nombre = $nombreNivel;
                    $nivel->Logica_id = Logica::model()->find()->id;
                    $nivel->save();
                }
                $nombres = explode(" ", $row->NOMBRE_CURSO);
                if (count($nombres) == 2) {
                    $nombreAnio = $nombres[0];
                    $nombreDiv = $nombres[1];
                } else if (count($nombres) == 3) {
                    $nombreAnio = $nombres[0] . $nombres[1];
                    $nombreDiv = $nombres[2];
                } else if (count($nombres) == 4) {
                    $nombreAnio = $nombres[0] . $nombres[1] . $nombres[2];
                    $nombreDiv = $nombres[3];
                }
                $anio = Anio::model()->find("nombre = \"$nombreAnio\" and Nivel_id = $nivel->id");
                if (!$anio) {
                    $anio = new Anio();
                    $anio->nombre = $nombreAnio;
                    $anio->Nivel_id = $nivel->id;
                    $anio->articulo_id = $articulo_id;
                    if (!$anio->save()) {
                        ve($anio->attributes);
                        ve($anio->errors);
                        die;
                        continue;
                    }
                }

                $div = Division::model()->find("nombre = \"$nombreDiv\" and Anio_id = $anio->id");
                if (!$div) {
                    $div = new Division();
                    $div->nombre = $nombreDiv;
                    $div->Anio_id = $anio->id;
                    $div->save();
                }
                $ad = AlumnoDivision::model()->find("Alumno_id = $a->id and Division_id = $div->id");
                if (!$ad) {
                    $ad = new AlumnoDivision();
                    $ad->Alumno_id = $a->id;
                    $ad->Division_id = $div->id;
                    $ad->activo = 1;
                    $ad->Ciclo_id = Ciclo::getActivo()->id;
                    $ad->fecha_alta = date("Y-m-d");
                    if (!$ad->save()) {
                        ve($ad->errors);
                        die;
                    }
                }
                echo "$a->matricula / $nivel->id - $nivel->nombre / $anio->id - $anio->nombre - $div->id - $div->nombre </br>";
            }
//            if($total > 30) die;
        }
        $tr->commit();
    }

    public function actionMaterias() {
        echo '<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">';
        $cicloActivo = Ciclo::getActivo();
        $db = Yii::app()->fb->connection;
        $sth = ibase_query($db, "
        select v.nombre as nivel, c.nombre as curso, n.id_nombre_materia, n.descripcion as materia, m.oid_materias
          from te_materias m
              inner join te_nombre_materias n on n.id_nombre_materia = m.id_nombre_materia
              inner join te_niveles v on v.id_nivel = m.id_nivel
              inner join te_cursos_divisiones c on c.id_nivel = m.id_nivel and c.id_curso_division = m.id_curso_division
          where m.id_nivel in (2,3,4)
          order by m.oid_materias
    ");
        $tr = Yii::app()->db->beginTransaction();
        $i = 0;
        while ($row = ibase_fetch_object($sth)) {
            $nombreNivel = mb_convert_case($row->NIVEL, MB_CASE_TITLE, "UTF-8");
            $nivel = Nivel::model()->find("nombre = \"$nombreNivel\"");
            if (!$nivel) {
                throw new Exception("Primero debe ejecutar /importar/asignaciones");
            }
            $asignatura_tipo = AsignaturaTipo::model()->find("Nivel_id=$nivel->id");
            if (!$asignatura_tipo) {
                $asignatura_tipo = new AsignaturaTipo();
                $asignatura_tipo->Nivel_id = $nivel->id;
                $asignatura_tipo->nombre = "Regular $nivel->nombre";
                $asignatura_tipo->save();
            }

            $nombres = explode(" ", $row->CURSO);
            if (count($nombres) == 2) {
                $nombreAnio = $nombres[0];
                $nombreDiv = $nombres[1];
            } else if (count($nombres) == 3) {
                $nombreAnio = $nombres[0] . $nombres[1];
                $nombreDiv = $nombres[2];
            } else if (count($nombres) == 4) {
                $nombreAnio = $nombres[0] . $nombres[1] . $nombres[2];
                $nombreDiv = $nombres[3];
            }


            $anio = Anio::model()->find("nombre = \"$nombreAnio\" and Nivel_id = $nivel->id");
            if (!$anio) {
                throw new Exception("Primero debe ejecutar /importar/asignaciones");
            }
            $div = Division::model()->find("nombre = \"$nombreDiv\" and Anio_id = $anio->id");
            if (!$div) {
                ve("No existe la division: con nombre = \"$nombreDiv\", Anio = $nombreAnio y nivel=$nivel->nombre");
                continue;
            }
            //echo $nivel . "/" . $nombreAnio . "/" . $nombreDiv . "/" . "$row->MATERIA" . "<br/>";
            //$materia = mb_convert_case($row->MATERIA, MB_CASE_TITLE, "UTF-8");
            $materia = mb_convert_case($row->MATERIA, MB_CASE_TITLE, "UTF-8");
            $a = Asignatura::model()->findBySql("
        select a.*
            from asignatura a
            where a.Anio_id = $anio->id and a.nombre = \"$materia\"
      ");
            if (!$a) {
                $a = new Asignatura();
                $a->desde_ciclo = $cicloActivo->id;
                $a->nombre = $materia;
                $a->Anio_id = $anio->id;
                $a->Asignatura_Tipo_id = $asignatura_tipo->id;
                $a->abrev = $a->nombre;
                if (!$a->save()) {
                    var_dump(json_encode($a->errors));
                    die;
                }
            }
            ve($row->MATERIA, $a->nombre);
        }
        $tr->commit();
    }

    public function actionArticulos() {
        $db = Yii::app()->fb->connection;
        $sth = ibase_query($db, "select * from te_novedades");
        $nuevos = 0;
        $total = 0;
        /* @var $a Alumno */
        $articulo_tipo_novedad = Opcion::getOpcionText("articulo_tipo_novedad", Null, "liquidacion");
        $tr = Yii::app()->db->beginTransaction();
        while ($row = ibase_fetch_object($sth)) {
            $total++;
            $a = new Articulo();
            $a->nombre = trim($row->NOMBRE);
            $a->articulo_tipo_id = $articulo_tipo_novedad;
//      $a->concepto = "1";
            $a->fecha_creacion = Helpers::date("Y/m/d", time());
            $a->orden = $row->NRO_ORDEN;
            $a->precio_neto = $row->IMPORTE;
            $a->afectacion = trim($row->AFECTACION);
            if (!$a->save()) {
                print_r($a->errors);
                die;
            }
        }

        $tr->commit();
        echo "Total: $total<br/>Nuevos:$nuevos<br/>";
    }

    public function actionActualizaAnios() {
        $db = Yii::app()->fb->connection;
        $sth = ibase_query($db, "
      select LOWER(ni.nombre) as nivel, LOWER(cd.nombre) as curso, LOWER(n.nombre) as novedad, n.importe
         from te_cursos_divisiones c
             inner join te_novedades n on n.id_novedad = c.id_novedad
             inner join te_niveles ni on ni.id_nivel = c.id_nivel
             inner join te_cursos_divisiones cd on cd.id_nivel = ni.id_nivel and cd.id_curso_division = c.id_curso_division");
        /* @var $a Alumno */
        while ($row = ibase_fetch_object($sth)) {
            $nivel = trim(strtolower($row->NIVEL));
            if ($nivel == "es")
                $nivel = "Educación Secundaria";
            $n = Nivel::model()->find("nombre like \"$nivel\"");
            if (!$n) {
                echo $nivel . " no encontrado<br/>";
                continue;
            } else {
                $nombres = explode(" ", $row->CURSO);
                if (count($nombres) == 2) {
                    $nombreAnio = $nombres[0];
                    $nombreDiv = $nombres[1];
                } else if (count($nombres) == 3) {
                    $nombreAnio = $nombres[0] . $nombres[1];
                    $nombreDiv = $nombres[2];
                } else if (count($nombres) == 4) {
                    $nombreAnio = $nombres[0] . $nombres[1] . $nombres[2];
                    $nombreDiv = $nombres[3];
                }
//var_dump($nombreAnio,$nombreDiv);die;
                $anio = strtolower(trim($nombreAnio));
                $div = strtolower(trim($nombreDiv));
                $a = Anio::model()->find("Nivel_id = $n->id and nombre like \"$anio\"");
                if (!$a) {
                    echo "Anio:" . $nivel . "-" . $anio . " no encontrado " . "id = $n->id and nombre like \"$anio\"<br/>";
                    continue;
                } else {
                    $novedad = trim($row->NOVEDAD);
                    $art = Articulo::model()->find("nombre like \"$novedad\"");
                    if (!$art) {
                        echo "Articulo $novedad No encontrado<br/>";
                    } else {
                        $a->articulo_id = $art->id;
                        if (!$a->save()) {
                            var_dump($a->errors);
                            die;
                        } else {
                            var_dump($a->attributes);
                        }
                    }
                    $c = Division::model()->find("Anio_id = $a->id and nombre like \"$div\"");
                    if (!$c) {
                        echo "Division:" . $anio . "-" . $anio . " - $div no encontrado<br/>";
                    } else {
                        
                    }
                }
            }
        }
    }

    function actionTest() {
        $db = Yii::app()->fb->connection;
        //mysql_query("SET NAMES 'win1252'");
        $sth = ibase_query($db, "select matricula,nombre,activo from te_alumnos where matricula = 1107");
        /* @var $a Alumno */
        $row = ibase_fetch_object($sth);
        $a = Alumno::model()->find("matricula='$row->MATRICULA'", array("matricula" => $row->MATRICULA));
        $this->parseNombre($row->NOMBRE, $a);
        $a->save();
        $a = Alumno::model()->find("matricula='1107'");
        echo '<meta content="text/html; charset=utf-8" http-equiv="Content-type">';
        vd($a->apellido);
    }

    function parseNombre($nombre, $obj) {
        $partes = explode(" ", mb_convert_case(trim($nombre), MB_CASE_TITLE, "UTF-8"));
        if ($nombre == "") {
            $obj->apellido = "(falta apellido)";
            $obj->nombre = "(falta nombre)";
        } elseif (count($partes) == 1) {
            $obj->apellido = trim($partes[0]);

            $obj->nombre = "(falta nombre)";
        } elseif (count($partes) == 2) {
            $obj->apellido = trim($partes[0]);
            $obj->nombre = trim($partes[1]);
        } elseif (count($partes) == 3) {
            $obj->apellido = trim($partes[0]);
            $obj->nombre = trim($partes[1]) . " " . trim($partes[2]);
        } elseif (count($partes) == 4) {
            $obj->apellido = trim($partes[0]) . " " . trim($partes[1]);
            $obj->nombre = trim($partes[2]) . " " . trim($partes[3]);
        } elseif (count($partes) == 5) {
            $obj->apellido = trim($partes[0]) . " " . trim($partes[1]);
            $obj->nombre = trim($partes[2]) . " " . trim($partes[3]) . " " . trim($partes[4]);
        } elseif (count($partes) == 6) {
            $obj->apellido = trim($partes[0]) . " " . trim($partes[1]);
            $obj->nombre = trim($partes[2]) . " " . trim($partes[3]) . " " . trim($partes[4]) . " " . trim($partes[5]);
        } elseif (count($partes) == 7) {
            $obj->apellido = trim($partes[0]) . " " . trim($partes[1]);
            $obj->nombre = trim($partes[2]) . " " . trim($partes[3]) . " " . trim($partes[4]) . " " . trim($partes[5]) . " " . trim($partes[6]);
        } else {
            var_dump($partes);
            die;
        }
    }

    function actionActualizaAlumnos() {
        $db = Yii::app()->fb->connection;
        $s = "select f.obs, a.matricula
            from te_alumnos a
                inner join te_niveles n on n.id_nivel = a.id_nivel
                inner join te_cursos_divisiones cd on cd.id_nivel = n.id_nivel and cd.id_curso_division = a.id_curso_division
                inner join te_familias f on f.id_familia = a.id_familia
            where f.obs_cobranza like  '%VISA%' and f.obs is not null and f.obs <> ''";
//        vd($s);
        $sth = ibase_query($db, $s);
        $total = 0;
        /* @var $a Alumno */
        $tr = Yii::app()->db->beginTransaction();
        while ($row = ibase_fetch_object($sth)) {
            $total++;
            $a = Alumno::model()->find("matricula='$row->MATRICULA'", array("matricula" => $row->MATRICULA));
            if (!$a) {
                echo $row->MATRICULA . "/";
            } else {
                $a->visa_numero = substr($row->OBS, 0,19);
                if (!$a->save()) {
                    var_dump($a->errors);
                    echo $row->PORC_BECA;
                    die;
                }
            }
        }
        $tr->commit();
    }

    function actionActualizaParientes() {
        $db = Yii::app()->fb->connection;
        $sth = ibase_query($db, ".
            select a.matricula, a.nombre, p.nombre as padre, z.nombre as tipo, 
                        p.profesion_actividad as profesion, v.oid_padre
                from te_alumnos a
                    inner join te_padres p on p.id_familia = a.id_familia
                    left join te_parentezco z on z.id_parentezco = p.id_parentezco
                    left join te_padres v on v.id_familia = a.id_familia and v.id_parentezco = p.id_parentezco and a.vive_con = v.id_parentezco
                where p.profesion_actividad is not null and p.profesion_actividad <> '' 
                order by a.nombre, p.nombre,v.nombre");
        $tr = Yii::app()->db->beginTransaction();
        while ($row = ibase_fetch_object($sth)) {
            $pariente = Helpers::qry("
                    select a.matricula, p.id, p.profesion
                    from pariente p
                        inner join familia f on f.id = p.Familia_id
                        inner join alumno a on a.Familia_id = f.id
                        inner join pariente_tipo pt on pt.id = p.Pariente_Tipo_id
                    where a.matricula = :matricula and pt.nombre = :tipo
                    order by a.apellido", array("matricula" => $row->MATRICULA, "tipo" => $row->TIPO));
            if ($pariente) {
                echo $pariente["matricula"] . "-" . $pariente["id"] . ": " . $pariente["profesion"] . "<br/>";
                echo $exec = Helpers::qryExec(
                        "update pariente 
                                set profesion=:profesion 
                                where id=:id", array(
                        "profesion" => $row->PROFESION,
                        "id" => $pariente["id"]));
            }
        }
        $tr->commit();
    }

    public function actionConceptos() {
        $nuevos = 0;
        $total = 0;

        $db = Yii::app()->fb->connection;

        $sth = ibase_query($db, "
      select * from t_conceptos order by oid_nodo_padre asc"
        );

        $tr = Yii::app()->db->beginTransaction();

        while ($row = ibase_fetch_object($sth)) {
            $total++;
            $id = $row->OID_NODO;
            $concepto_id = Yii::app()->db->createCommand("
        select id from concepto where id=$id
      ")->queryScalar();
            if (!$concepto_id) {
                $nuevos++;
                $c = new Concepto();
                $c->id = $row->OID_NODO;
                $c->concepto_id = $row->OID_NODO_PADRE == 0 ? null : $row->OID_NODO_PADRE;
                $c->nombre = $row->NODO;
                $c->alias = $row->ALIAS;
                $c->ingreso_egreso = "E";
                if (!$c->save()) {
                    var_dump($c->errors);
                    die;
                }
            } else {
                
            }
        }
        $tr->commit();
        echo "Nuevos: $nuevos";
        echo "Total: $total";
    }

    public function actionChequesVieja() {
        $total = 0;

        $db = Yii::app()->fb->connection;

        $sth = ibase_query($db, "
      select first 1 * 
        from t_chq_terceros 
        where estado = 0 
          and anulado = 0 
          and fec_salida is null"
        );

        $tr = Yii::app()->db->beginTransaction();

        while ($row = ibase_fetch_object($sth)) {
            $total++;
        }
        $tr->commit();
        echo "Total: $total";
    }

    function importaProveedor(Proveedor $proveedor, $row) {
        /* @var $proveedor Proveedor */
        $s = new Socio();
        if (!is_numeric(trim($row->ID_PROVE))) {
            $proveedor->id = time();
        } else {
            $proveedor->id = $row->ID_PROVE;
        }
        $proveedor->razon_social = trim($row->RAZON_SOCIAL) ? trim($row->RAZON_SOCIAL) : trim($row->NOM_FANTASIA);
        $proveedor->nombre_fantasia = trim($row->NOM_FANTASIA);
        $proveedor->cuit = trim($row->CUIT);
        $proveedor->nro_ingresos_brutos = trim($row->NRO_ING_BRUTOS);
        $proveedor->observaciones = trim($row->OBS);
        $db = Yii::app()->fb->connection;
        $sth = ibase_query($db, "
      select * from t_domicilios d where d.oid_prove = $row->OID_PROVE;
        ");
        $dom = ibase_fetch_object($sth);
        if ($dom) {
            $proveedor->calle = trim($dom->CALLE_NOMBRE) ? trim($dom->CALLE_NOMBRE) : "ingresar";
            $proveedor->calle_numero = trim($dom->CALLE_NUMERO);
            $proveedor->calle_piso = trim($dom->CALLE_PISO);
            $proveedor->calle_departamento = trim($dom->CALLE_DEPARTAMENTO);
//    $proveedor->correo_electronico1 = trim($dom->CORREO_ELECTRONICO);
            $proveedor->telefono1 = trim($dom->TELEFONO1);
            $proveedor->telefono2 = trim($dom->TELEFONO2);
            $proveedor->telefono3 = trim($dom->TELEFONO3);
            $proveedor->localidad_id = trim($dom->ID_LOCALIDAD) == 1 ? "2" : "790237";
        }
        $proveedor->contacto = trim($row->CONTACTO);
        $proveedor->fecha_ultima_compra = $row->FEC_ULT_COMPRA;
    }

    public function actionCheques() {
        //189 int lujan
        if (!$_POST) {
            $db = Yii::app()->fb->connection;
            $sth = ibase_query($db, "
                select * 
                  from t_chq_terceros 
                  where /*(estado = 0 or (estado is null))
                    and entregado_a is null
                    and*/ anulado = 0 and fec_acred >= ? order by numero"
                    , "2013.07.01"
            );
            // 4135
            // 189 lujan
            $this->render("cheques", array("rows" => $sth));
        } else {
            $di = Destino::getCajaInstanciaActiva();
            $db = Yii::app()->fb->connection;
            $tr = Yii::app()->db->beginTransaction();
            $doc = new Doc("insert", Comprob::AJUSTE_CAJA_ENTRADA);
            $doc->total = 0;
            $doc->saldo = 0;
            $doc->detalle = "Importacion Cheques";
            $doc->concepto_id = 1775;
            if (!$doc->save()) {
                echo(json_encode($doc->errors));
                die;
            }
            $total_cheques = 0;
            foreach (array_keys($_POST["sel"]) as $ID_CHQ_TERCERO) {
                $qryChq = ibase_query($db, "
                select * 
                  from t_chq_terceros 
                  where ID_CHQ_TERCERO = $ID_CHQ_TERCERO"
                );
                $chqIB = ibase_fetch_object($qryChq);
                $banco_id_ib = trim($chqIB->ID_BANCO);
                $banco_id = Yii::app()->db->createCommand("
                    select id from banco where codigo = \"$banco_id_ib\"
                  ")->queryScalar();
                if (!$banco_id) {
                    throw new Exception("No existe el banco con id $banco_id_ib, $banco_id en la base de datos");
                }
                $dv = new DocValor();
                $dv->tipo = 1;
                $dv->Doc_id = $doc->id;
                $dv->importe = $chqIB->IMPORTE;
                $dv->Destino_Instancia_id = $di->id;
                $dv->Destino_id = $di->Destino_id;
                $dv->banco_id = $banco_id;
                $dv->chq_origen = $chqIB->FIRMANTE;
                $dv->numero = $chqIB->NUMERO;
                $dv->fecha = date("Y-m-d", mystrtotime($chqIB->FEC_ACRED));
                $total_cheques += $dv->importe;
                if (!$dv->save()) {
                    var_dump($dv->errors);
                    die;
                }
                $doc->total = $total_cheques;
            }
            $doc->save();
            $tr->commit();
            $this->redirect($this->createUrl("/"));
        }
    }

    public function actionBancosNombre() {
        $db = Yii::app()->fb->connection;
        $sth = ibase_query($db, "
      select * from T_BANCOS
        ");

        $total = 0;
        /* @var $b Banco */
        $tr = Yii::app()->db->beginTransaction();
        while ($row = ibase_fetch_object($sth)) {
            $id_banco = trim($row->ID_BANCO);
            $b = Banco::model()->find("codigo = $id_banco");
            if (!$b) {
                $total++;
                $b = New Banco();
            }
            $b->nombre = trim($row->NOMBRE);
            $b->codigo = trim($row->ID_BANCO);
            if (!$b->save()) {
                var_dump($b->errors);
                echo("Error <br/>");
                die;
            }
            $d = Destino::model()->find("Banco_id = $b->id");
            if (!$d) {
                $d = new Destino();
            }
            $d->nombre = trim($row->NOMBRE);
            $d->Destino_Tipo_id = 2;
            $d->Banco_id = $b->id;
            if (!$d->save()) {
                var_dump($d->errors);
                die;
            }
        }
        $tr->commit();
        echo "Total: $total <br/>";
    }

    public function actionBancos() {
        $di = Destino::getCajaInstanciaActiva();
//    $dv->Destino_id = $di->Destino_id;
        $db = Yii::app()->fb->connection;
        $sth = ibase_query($db, "
      select * from T_BANCOS
        ");

        $total = 0;
        /* @var $b Banco */
        $tr = Yii::app()->db->beginTransaction();
        while ($row = ibase_fetch_object($sth)) {
            $b = Banco::model()->find("nombre = \"" . trim($row->NOMBRE) . "\"");
            if (!$b) {
                $total++;
                $b = New Banco();
            }
            $b->nombre = trim($row->NOMBRE);
            $b->codigo = trim($row->ID_BANCO);
            if (!$b->save()) {
                var_dump($b->errors);
                echo("Error <br/>");
                die;
            }
            $d = Destino::model()->find("Banco_id = $b->id");
            if (!$d) {
                $d = new Destino();
            }
            $d->nombre = trim($row->NOMBRE);
            $d->Destino_Tipo_id = 2;
            $d->Banco_id = $b->id;
            if (!$d->save()) {
                var_dump($d->errors);
                die;
            }
        }
        $tr->commit();
        echo "Total: $total <br/>";
    }

}
