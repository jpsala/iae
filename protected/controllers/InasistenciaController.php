<?php

	class InasistenciaController extends Controller {

		public function actionIndex() {
			$this->render('admin', array("fecha_hoy" => date("d/m/Y", time())));
		}

		public function actionTraeItems($fecha, $division_id, $ciclo_id = null) {
//    $ads = AlumnoDivision::model()->with("alumno", "division")->findAll(array(
//        "condition" => "Division_id = $division_id and t.activo = 1 and ciclo_id = $ciclo_id",
//        "order" => "alumno.apellido,alumno.nombre")
//    );

			$inasistencias = Inasistencia::getInasistencias($division_id, $fecha, $ciclo_id);

			$this->renderPartial("_items", array("inasistencias" => $inasistencias, "division_id" => $division_id, "fecha" => $fecha));
		}

		public function actionGraba($inasistencia_detalle_id, $tipo_id, $detalle, $alumno_id, $division_id, $fecha) {
			$ciclo_id = Ciclo::getActivoId();
			$id = InasistenciaDetalle::model()->findByPk($inasistencia_detalle_id);
			$ret = new stdClass();
			$ret->estado = null;
			$ret->error = null;
			if (!$id) {
				$alumno = Alumno::model()->findByPk($alumno_id);
				$alumno_division_id = $alumno->alumnoDivisionActiva->id;
				$fecha_qry = Helpers::date("Y/m/d", $fecha);
				$nivel_id = Helpers::qryScalar("select a.nivel_id 
					from anio a 
						inner join division d on d.anio_id = a.id and d.id = $division_id");
				echo "
					select lc.id
					from logica_periodo lc
						inner join logica_ciclo c on c.Logica_id = lc.Logica_id and c.Ciclo_id = $ciclo_id
						inner join asignatura_tipo a on a.id = c.Asignatura_Tipo_id and a.Nivel_id = $nivel_id
					where \"$fecha_qry\" >= lc.fecha_inicio_ci and \"$fecha_qry\" <= lc.fecha_fin_ci";

				$lps = Helpers::qryAll("
					select lc.id
					from logica_periodo lc
						inner join logica_ciclo c on c.Logica_id = lc.Logica_id and c.Ciclo_id = $ciclo_id
						inner join asignatura_tipo a on a.id = c.Asignatura_Tipo_id and a.Nivel_id = $nivel_id
					where \"$fecha_qry\" >= lc.fecha_inicio_ci and \"$fecha_qry\" <= lc.fecha_fin_ci"
				);
				if (count($lps) != 1) {
					$ret->error = "LogicaPeriodo no existe o hay mas de una para esta fecha";
					$ret->estado = "error";
				} else {
					$logica_periodo_id = $lps[0]["id"];
					$inasistencia = Inasistencia::model()->find(
							"logica_periodo_id=\"$logica_periodo_id\" and alumno_division_id = \"$alumno_division_id\""
					);
					if (!$inasistencia) {
						$inasistencia = new Inasistencia();
						$inasistencia->logica_periodo_id = $logica_periodo_id;
						$inasistencia->Alumno_Division_id = $alumno_division_id;
						$inasistencia->save();
					}
					$id = new InasistenciaDetalle();
					$id->inasistencia_id = $inasistencia->id;
				}
			}
			if (!$ret->error) {
				$id->detalle = $detalle;
				$id->inasistencia_tipo_id = $tipo_id;
				$id->fecha = Helpers::date("Y/m/d", $fecha);
				if (!$id->save()) {
					print_r($id->errors);
					$ret->error = json_encode($id->errors);
					$ret->estado = "error";
				} else {
					$ret->id = $id->id;
					$ret->estado = "ok";
				}
			}
			echo json_encode($ret);
		}

		public function actionReporte($division_id = 34, $fecha = "19/03/2012") {
			$this->layout = "impresion1";
			$this->pageTitle = "Informe de inasistencias";
			$html = $this->render("reporte", array("inasistencias" => Inasistencia::getInasistencias($division_id, $fecha)), true);
			//echo $html;die;
			$tmp = Yii::app()->basePath . "/data/" . uniqid() . ".html";
			$pdf = Yii::app()->basePath . "/data/" . uniqid() . ".pdf";
			$footerPage = Yii::app()->basePath . "/data/footer.html";
			file_put_contents($tmp, $html);
			if (isset($_SERVER["SERVER_NAME"]) and $_SERVER["SERVER_NAME"] == "localhost") {
				$output = shell_exec("\"\\Program Files\\wkhtmltopdf\\wkhtmltopdf.exe \" --footer-html $footerPage $tmp $pdf 2>&1");
			} else {
				$output = shell_exec("/usr/bin/wkhtmltopdf --footer-html $footerPage $tmp $pdf 2>&1");
			}
			//echo "<pre>$output</pre>";die;
			header('Content-Type: application/pdf');
			header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
			header('Pragma: public');
			header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
			//header('Content-Length: ' . strlen($pdf));
			header('Content-Disposition: inline; filename="' . basename($pdf) . '";');
			readfile($pdf);
			unlink($tmp);
			unlink($pdf);
		}

		public function actionOctavioGay() {
			echo phpinfo();
		}

		public function actionCargaInasistencia() {
			$this->pageTitle = "Carga de inasistencia";
			$this->render("cargaInasistencia"); //, array("notas" => $notas, "logicaItems" => $logicaItems, "fechaPeriodo" => date("2012-01-02"), "divisionAsignatura" => $divisionAsignatura, "asignatura"=>$asignatura,"logicaPeriodo"=>$logicaPeriodo));
		}

		public function actionCargaInasistenciaAjax($division_id, $logica_periodo_id = null) {
			$inasistenciaParaCarga = Inasistencia::getInasistenciaParaCarga($division_id);
			$this->renderPartial("cargaInasistenciaAjax", array(
					"inasistencias" => $inasistenciaParaCarga,
					"logica_periodo_id" => $logica_periodo_id,
					"trae_periodos_con_inasistencia" => false,
					)
			);
		}

		public function actionCargaInasistenciaGraba() {
			if (isset($_POST["inasistencia"])) {
				$ret = new stdClass();
				$errors = null;
				$division_id = $_POST["division_id"];
				$ciclo_id = Ciclo::getActivoId();
				$tr = Yii::app()->db->beginTransaction();
				foreach ($_POST["inasistencia"] as $alumno_id => $inasistencia) {
					if ($inasistencia)
						foreach ($inasistencia as $key => $val) {
							$parts = explode(":", $key);
							$logica_periodo_id = $parts[0];
							$inasistencia_id = $parts[1];
							if ($inasistencia_id) {
								$inasistencia = Inasistencia::model()->findByPk($inasistencia_id);
								if (!$val) {
									$inasistencia->delete();
									continue;
								}
							} else {
								$alumno_division_id = Helpers::qryScalar("
                select ad.id
                  from alumno_division ad
                  where ad.Division_id = $division_id and
                        ad.Ciclo_id = $ciclo_id and
                        ad.alumno_id = $alumno_id
              ");
								$inasistencia = Inasistencia::model()->find(
										"alumno_division_id = $alumno_division_id and
                       logica_periodo_id = $logica_periodo_id
                      "
								);
								if ($inasistencia) {
									$ret->encontrados[] = $inasistencia->attributes;
									if (!$val) {
										$inasistencia->delete();
										continue;
									}
								} else if (!$val) {
									continue;
								} else {
									$inasistencia = new Inasistencia();
									$inasistencia->Alumno_Division_id = $alumno_division_id;
									$inasistencia->logica_periodo_id = $logica_periodo_id;
								}
							}
							$inasistencia->cantidad = $val;
							if (!$inasistencia->save()) {
								$errors = $inasistencia->errors;
								exit;
							}
						}
				}
				if ($errors) {
					$ret->errors = $errors;
					$tr->rollback();
				} else {
					$tr->commit();
				}
				echo json_encode($ret);
			}
		}

		public function actionVerInasistencias($division_id) {
			$ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
			//vd("select ad.id from alumno_division ad where ad.`Division_id` = $division_id and ad.`Ciclo_id` =$ciclo_id");
			$ad = Yii::app()->db->createCommand(
					"select ad.id from alumno_division ad where ad.`Division_id` = $division_id and ad.`Ciclo_id` =$ciclo_id"
			);
			$inasistencias = Yii::app()->db->createCommand(
							"select concat(a.apellido,', ',a.nombre) as alumno, d.fecha, d.detalle, t.nombre as tipo, t.valor
                from inasistencia i
                        inner join inasistencia_detalle d on d.inasistencia_id = i.id
                        inner join alumno_division ad on ad.id = i.Alumno_Division_id and ad.ciclo_id = $ciclo_id and ad.activo /*((ad.activo or ad.promocionado or ad.egresado)*/
                        inner join division di on di.id = ad.Division_id and di.id = $division_id
                        inner join inasistencia_tipo t on t.id = d.inasistencia_tipo_id
                        inner join alumno a on a.id = ad.Alumno_id
                order by a.apellido, a.nombre, d.fecha "
					)->queryAll();
			$this->render("verInasistencias", array("division_id" => $division_id, "inasistencias" => $inasistencias));
		}

	}

?>
