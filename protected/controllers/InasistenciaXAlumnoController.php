<?php

class InasistenciaXAlumnoController extends Controller {


    public function actionIndex() {
    $this->render('admin', array("fecha_hoy" => date("d/m/Y", time())));
  }

  public function actionTraeItems($alumno_id) {
    /* @var $alumno Alumno */
    
    $alumno = Alumno::model()->findByPk($alumno_id);

    $alumno_division_id = $alumno->alumnoDivisionActiva;
    //var_dump($alumno_division_id);die;
    $lps = LogicaPeriodo::model()->GetActiva();
    
    if (count($lps) != 1) {
        throw new Exception("LogicaPeriodo no existe o hay mas de una para esta fecha");
      }
    $logica_periodo_id = $lps->id;
 
    $inasistencia = Inasistencia::model()->find(
            "logica_periodo_id=\"$logica_periodo_id\" and alumno_division_id = \"$alumno_division_id->id\""
    );
    if (!$inasistencia) {
      $inasistencia = new Inasistencia();
      $inasistencia->logica_periodo_id = $logica_periodo_id;
      $inasistencia->Alumno_Division_id = $alumno_division_id->id;
      $inasistencia->save();
    }
    $items = InasistenciaDetalle::model()
            ->with("inasistenciaTipo")
            ->findAll("inasistencia_id = $inasistencia->id");
    $this->renderPartial("_items", array("items" => $items));
  }

  public function actionGrabaItem() {
    if ($_POST["item_id"]) {
      $inasistenciaDetalle = InasistenciaDetalle::model()->findByPk($_POST["item_id"]);
    }
    if (!$inasistenciaDetalle) {
      $alumno = Alumno::model()->findByPk($_POST["alumno_id"]);
      $alumno_division_id = $alumno->alumnoDivisionActiva->id;
      $lps = LogicaPeriodo::model()->GetActiva();
    
     if (count($lps) != 1) {
         throw new Exception("LogicaPeriodo no existe o hay mas de una para esta fecha  "   );
       }
     $logica_periodo_id = $lps->id;
     
      $inasistencia = Inasistencia::model()->find(
              "logica_periodo_id=\"$logica_periodo_id\" and alumno_division_id = \"$alumno_division_id\""
      );
      $inasistenciaDetalle = new InasistenciaDetalle();
      $inasistenciaDetalle->inasistencia_id = $inasistencia->id;
    }
    $inasistenciaDetalle->fecha = Helpers::fechaParaGrabar($_POST["fecha"]);
    $inasistenciaDetalle->inasistencia_tipo_id = $_POST["inasistencia_tipo_id"];
    $inasistenciaDetalle->detalle = $_POST["detalle"];
    $ret = null;
    if (!$inasistenciaDetalle->save()) {
      $ret->errors = $inasistenciaDetalle->errors;
      echo json_encode($ret);
      die;
    }
    $ret->id = $inasistenciaDetalle->id;
    echo json_encode($ret);
  }

  public function actionBorraItem() {
    $id = InasistenciaDetalle::model()->findByPk($_POST["item_id"]);
    echo $id->delete();
  }
  
  public static function getInasistenciasDetalladasPorAlumno($alumno_id) {
    $alumno = Alumno::model()->findByPk($alumno_id);
    $alumno_division_id = $alumno->alumnoDivisionActiva->id;  
    $inasistencias = Yii::app()->db->createCommand("  select i.id, i.logica_periodo_id, id.fecha, id.detalle, it.nombre, it.valor,
                    a.nombre as nomalumno, a.apellido as apealumno 
                    from inasistencia i
                    inner join inasistencia_detalle id 
                    on id.inasistencia_id = i.id
                    inner join inasistencia_tipo it on it.id = id.inasistencia_tipo_id
                    inner join alumno_division ad on ad.id = i.Alumno_Division_id
                    inner join alumno a on a.id = ad.Alumno_id        
                    where  fecha BETWEEN (select c.fecha_inicio from ciclo c where c.activo = 1)and (select c.fecha_fin from ciclo c where c.activo = 1)
                    and i.Alumno_Division_id = $alumno_division_id")->query()->readAll();

    return $inasistencias;
  }

  public function actionReporteDetallado() {
    $this->layout = "impresion1";
    $this->pageTitle = "Informe de inasistencias por alumno. Ciclo: ". Ciclo::getActivo();
    $html = $this->render("reporteDetallado", array("inasistencias" => $this->getInasistenciasDetalladasPorAlumno()), true);

    $tmp = Yii::app()->basePath . "/data/" . uniqid() . ".html";
    $pdf = Yii::app()->basePath . "/data/" . uniqid() . ".pdf";
    $footerPage = Yii::app()->basePath . "/data/footer.html";
    file_put_contents($tmp, $html);
    if (isset($_SERVER["SERVER_NAME"]) and $_SERVER["SERVER_NAME"] == "localhost") {
      $output = shell_exec("\"\\Program Files\\wkhtmltopdf\\wkhtmltopdf.exe \" --footer-html $footerPage $tmp $pdf 2>&1");
    } else {
      $output = shell_exec("/usr/bin/wkhtmltopdf --footer-html $footerPage $tmp $pdf 2>&1");
    }
    //echo "<pre>$output</pre>";die;
    header('Content-Type: application/pdf');
    header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
    header('Pragma: public');
    header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    //header('Content-Length: ' . strlen($pdf));
    header('Content-Disposition: inline; filename="' . basename($pdf) . '";');
    readfile($pdf);
    unlink($tmp);
    unlink($pdf);
  }


}