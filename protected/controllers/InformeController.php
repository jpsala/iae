<?php

include_once("formulas/LogicaActiva.php");

class InformeController extends Controller
{

    public $layout = "column1";

    public function actionLujanEntreFechas()
    {
        $this->render("/informe/genEntreFechas", array(
            "reporteNombre" => "lujanReporte",
            "excel" => true,
            "titulo" => "Lujan, dar nombre al reporte"));
    }

    public function actionLujanReporte($output, $fecha_desde, $fecha_hasta)
    {
        $this->renderPartial("/informe/lujanEntreFechas", array(
            "fecha_desde" => $fecha_desde, "fecha_hasta" => $fecha_hasta
        ));
    }


    public function actionAlumnosSelCols()
    {
        $this->render("/informe/excelSelCols", array(
        "reporteNombre" => "alumnosSelColsExcel",
        "excel" => true,
        "cols" => [
          "nivel" => array("title" => "Nivel"),
          "curso" => array("title" => "Curso"),
          "division" => array("title" => "Division"),
          "matricula" => array("title" => "Matricula", "format" => "text"),
          "Nombre" => array("title" => "Nombre Alumno"),
          "fecha_nacimiento" => array("title" => "Fec. Nac."),
          "p1mail" => array("title" => "p1mail"),
          "p2mail" => array("title" => "p2mail"),
          "DNI" => array("title" => "DNI"),
          // "beca" => array("title" => "Beca"),
          "domicilio" => array("title" => "Domicilio"),
          "padre" => array("title" => "Vive Con"),
          "p1tc" => array("title" => "Tel. Fijo", "align" => "right"),
          "p1tt" => array("title" => "Tel. Trabajo", "align" => "right"),
          "p1tce" => array("title" => "Tel. Cel.", "align" => "right"),
          "nombre2" => array("title" => "Nombre Madre"),
          "p2tc" => array("title" => "Tel. Fijo", "align" => "right"),
          "p2tt" => array("title" => "Tel. Trabajo", "align" => "right"),
          "p2tce" => array("title" => "Tel. Cel.", "align" => "right"),
        ]
        ));
    }

    public function actionAlumnosSelColsExcel($nivel_id, $anio_id, $division_id, $cols = [])
    {
        $this->renderPartial("excelSelColsExcel", array(
        "nivel_id" => $nivel_id,
        "anio_id" => $anio_id,
        "division_id" => $division_id,
        "cols" => $cols
        ));
    }


    public function actionBarCode()
    {
        $this->renderPartial("barCode");
    }

    public function actionHorariosEntreFechas()
    {
        $this->render("/informe/genEntreFechas", array(
        "reporteNombre" => "horariosReporte",
        "excel" => true,
        "titulo" => "Horarios entre fechas"));
    }

    public function actionHorariosReporte($output, $fecha_desde, $fecha_hasta)
    {
        $this->renderPartial("/informe/horarios", array(
        "fecha_desde" => $fecha_desde, "fecha_hasta" => $fecha_hasta
        ));
    }

    public function actionHorariosEntreFechasDetalle()
    {
        $this->render("/informe/genEntreFechas", array(
        "reporteNombre" => "horariosReporteDetalle",
        "excel" => true,
        "titulo" => "Horarios entre fechas con detalle"
        ));
    }


    public function actionHorariosReporteDetalle($output, $fecha_desde, $fecha_hasta)
    {
        $this->renderPartial("/informe/horariosDetalle", array(
        "fecha_desde" => $fecha_desde, "fecha_hasta" => $fecha_hasta
        ));
    }

    private function procesaArchivoHorarios()
    {
        echo '<h4><a href="index.php">Volver</a></h4>';
        $tr = Yii::app()->db->beginTransaction();

        $dir = "/mnt/shares/zoot/horarios/";
        $lines = file($dir . 'horarios.txt');
        foreach ($lines as $line) {
            $campos = preg_split('/\s+/', $line);
            $documento = $campos[1];
            $fecha = $campos[2];
            $hora = $campos[3];
            $fh = date('Y-m-d H:i:s', strtotime($fecha . ' ' . $hora));
            $tipo = $campos[4]; //=== '4' ? 'E': 'S';
            $empleado_id = Helpers::qryScalar("select id from empleado where documento = $documento");
            if (!$empleado_id) {
                Helpers::qryExec("insert into empleado(documento) values($documento)");
                $empleado_id = Helpers::qryScalar('select LAST_INSERT_ID();');
            }
            $existeFecha = Helpers::qryScalar("select count(*) from empleado_registro_raw where empleado_id = $empleado_id and fecha = '$fh'");
            if (!$existeFecha) {
                Helpers::qryExec("insert into empleado_registro_raw(empleado_id,fecha,tipo) values($empleado_id, '$fh','$tipo')");
            }
        }
        $tr->commit();
    }

    private function procesaHorarios()
    {

        function convertToHoursMins($time, $format = '%02d:%02d')
        {
            if ($time < 1) {
                return;
            }
            $hours = floor($time / 60);
            $minutes = ($time % 60);
            return sprintf($format, $hours, $minutes);
        }
        $tr = Yii::app()->db->beginTransaction();
        $empleados = Helpers::qryAll("select distinct id, documento from empleado");
        echo '<div style="max-width:200px"><span>';
        foreach ($empleados as $empleado) {
            $empleado_id = $empleado['id'];
            $fechas = Helpers::qryAll("select distinct date_format(fecha, '%Y/%m/%d') as fecha from empleado_registro_raw where empleado_id = $empleado_id order by fecha");
            foreach ($fechas as $fecha) {
                $fechaBusqueda = $fecha['fecha'];
                $recs = Helpers::qryAll(
                    "select * from empleado_registro_raw
            where empleado_id = $empleado_id and
                  \"$fechaBusqueda\" = date_format(fecha, '%Y/%m/%d')"
                );
                if ($recs) {
                      $primero = $recs[0];
                      $ultimo = $recs[count($recs) - 1];
                      $fecha_inicio = $primero['fecha'];
                      $fecha_ultimo = $ultimo['fecha'];
                      $minutos = intval(abs(strtotime($fecha_inicio) - strtotime($fecha_ultimo)) / 60);
                      $fechaParaGrabar = date('Y/m/d', strtotime($fecha_inicio));
                      $tiempo = convertToHoursMins($minutos, '%02d horas %02d minutos');
                      $dias = $minutos / 60 / 8;
                      $existeFecha = Helpers::qryScalar("select count(*) from empleado_registro where empleado_id = $empleado_id and date_format(fecha, '%Y/%m/%d') = '$fechaParaGrabar'");
                  //          ve($existeFecha, $existeFecha === 0, $minutos, $minutos >0);
                  //          echo '</br>';
                    if ($existeFecha === '0' && $minutos > 0) {
                        echo '<div><span style="color:red">'.$empleado['documento'] .'</span><span style="color:blue">'. $tiempo.'</span></div>';
                        Helpers::qryExec("insert into empleado_registro(empleado_id,fecha,minutos,tiempo) values($empleado_id, '$fechaParaGrabar',$minutos, \"$tiempo\")");
                    }
                }
            }
        }
        echo '</span></div>';
        $tr->commit();
    }

    public function actionHorarios()
    {
        Helpers::qryExec("delete from empleado_registro");
        Helpers::qryExec("delete from empleado_registro_raw");
        $this->procesaArchivoHorarios();
        $this->procesaHorarios();
    }

    public function actionNotas1()
    {
        $this->render("/informe/genNiAnDiAsViejo", array("reporteNombre" => "notas1Ajax"));
    }

    public function actionInasistencias1()
    {
        $this->render("/informe/inasistencias1", array("reporteNombre" => "inasistencias1Ajax"));
    }

    public function actionNotas1Ajax($division_asignatura_id, $impresion = null)
    {
        $divisionAsignatura = DivisionAsignatura::model()
            ->with(array("asignatura", "division", "division.anio", "division.anio.nivel"))
            ->findByPk($division_asignatura_id);
        $logica_id = $divisionAsignatura->asignatura->logica->id;
        $logicaItems = LogicaItem::model()->with("logicaPeriodo")
            ->findAll(array("condition" => "t.Logica_id=$logica_id", "order" => "logicaPeriodo.orden, t.orden"));
        $notas = $divisionAsignatura->getNotas();
      //echo "<pre>";
      //var_export($notas);die;
        $this->renderPartial("notas1Ajax", array(
        "notas" => $notas,
        "logicaItems" => $logicaItems,
        "impresion" => $impresion,
        "asignaturaNombre" => $divisionAsignatura->asignatura->nombre,
        "profesorNombre" => Yii::app()->user->model->nombre,
        "divisionAsignatura" => $divisionAsignatura));
    }

    public function actionInasistencias1Ajax($division_id, $impresion = null)
    {
        $divisionAsignatura = DivisionAsignatura::model()
            ->with(array("asignatura", "division", "division.anio", "division.anio.nivel"))
            ->findByPk($division_asignatura_id);
        $logica_id = $divisionAsignatura->asignatura->logica->id;
        $logicaItems = LogicaItem::model()->with("logicaPeriodo")
            ->findAll(array("condition" => "t.Logica_id=$logica_id", "order" => "logicaPeriodo.orden, t.orden"));
        $notas = $divisionAsignatura->getNotas();
        $this->renderPartial("inasistencias1Ajax", array(
        "notas" => $notas,
        "logicaItems" => $logicaItems,
        "impresion" => $impresion,
        "asignaturaNombre" => $divisionAsignatura->asignatura->nombre,
        "profesorNombre" => Yii::app()->user->model->nombre,
        "divisionAsignatura" => $divisionAsignatura));
    }

    public function actionPlanillaResumenFinal()
    {
        $this->render("/informe/genNiAnDiPeViejo", array(
        "reporteNombre" => "planillaResumenFinalAjax",
        "esPdf" => true,
        ));
    }

    public function actionPlanillaResumenFinalAjax($division_id, $logica_periodo_id, $paperSize = "legal")
    {
        $this->renderPartial("planillaResumenFinalAjax", array(
        "division_id" => $division_id,
        "logica_periodo_id" => $logica_periodo_id,
        "paperSize" => $paperSize
        ));
    }

    public function actionStickers()
    {
        $this->render("/informe/genNiAnDiPeViejo", array(
        "reporteNombre" => "stickersAjax",
        "esPdf" => true,
        ));
    }

    public function actionStickersAjax($division_id, $logica_periodo_id)
    {
        $this->renderPartial("stickersAjax", array(
        "division_id" => $division_id,
        "logica_periodo_id" => $logica_periodo_id,
        ));
    }

    public function actionStickersPatricia()
    {
        $this->render("/informe/genNiAnDiPeViejo", array(
        "reporteNombre" => "stickersPatriciaAjax",
        "esPdf" => true,
        ));
    }

    public function actionStickersPatriciaAjax($division_id, $logica_periodo_id)
    {
        $this->renderPartial("stickersAjaxPatri", array(
        "division_id" => $division_id,
        "logica_periodo_id" => $logica_periodo_id,
        ));
    }

    public function actionPlanillaNotasIntegradora()
    {
        $this->render("/informe/genNiAnDiAsViejo", array("reporteNombre" => "planillaNotasAjaxIntegradora",
        "esPdf" => true,
        ));
    }

    public function actionPlanillaNotas()
    {
        $this->render("/informe/genNiAnDiAsViejo", array("reporteNombre" => "planillaNotasAjax",
        "esPdf" => true,
        ));
    }

    public function actionPlanillaNotasAjax($division_asignatura_id, $paperSize = "legal")
    {
        $row = Helpers::qry("
				select division_id, da.asignatura_id, a.nombre
						from division_asignatura  da
								inner join asignatura a on a.id = da.asignatura_id
						where da.id=$division_asignatura_id ");
      // try{
        $notas = Division::getNotasPlanillaNotas($row["division_id"], $row["asignatura_id"], true);
      // }catch(Exception $e){
        // ve2($e);

      // }
      //vd2($notas);
  //        vd($notas);
        $this->renderPartial("planillaNotasAjax", array(
        "notas" => $notas,
        "paperSize" => $paperSize,
        "asignaturaNombre" => $row["nombre"],
        ));
    }

    public function actionPlanillaNotasPablo()
    {
        $this->render("/informe/genNiAnDiAsViejo", array("reporteNombre" => "planillaNotasAjaxPablo",
            "esPdf" => true,
        ));
    }
    public function actionPlanillaNotasAjaxPablo($division_asignatura_id, $paperSize = "legal")
    {
        $row = Helpers::qry("
				select division_id, da.asignatura_id, a.nombre
						from division_asignatura  da
								inner join asignatura a on a.id = da.asignatura_id
						where da.id=$division_asignatura_id ");
        $notas = Division::getNotasPlanillaNotas($row["division_id"], $row["asignatura_id"], true);
//        vd2($notas);
//        vd($notas);
        $this->renderPartial("planillaNotasAjaxPablo", array(
            "notas" => $notas,
            "paperSize" => $paperSize,
            "asignaturaNombre" => $row["nombre"],
        ));
    }

    public function actionPlanillaNotasAjaxIntegradora($division_asignatura_id, $paperSize = "legal")
    {
        $row = Helpers::qry("
            select da.division_id, da.asignatura_id, a.nombre
                from division_asignatura  da
                    inner join asignatura a on a.id = da.asignatura_id
                where da.id=$division_asignatura_id");
        $notas = Division::getNotasPlanillaNotasConIntegradora($row["division_id"], $row["asignatura_id"]);
  //        vd($notas);
        $this->renderPartial("planillaNotasAjaxIntegradora", array(
        "notas" => $notas,
        "paperSize" => $paperSize,
        "asignaturaNombre" => $row["nombre"],
        ));
    }

    public function actionDocs1()
    {
      //$comprobs = Comprob::model()->findAll("compra_venta in ('V','F')");
        $comprobs = Comprob::model()->findAll();
        $this->render("/informe/genEntreFechasViejo", array("reporteNombre" => "docs1Ajax", "comprobs" => $comprobs));
    }

    public function actionDocs1Ajax($fecha_desde, $fecha_hasta, $comprob_id = Comprob::RECIBO_VENTAS, $paperSize = "legal", $output = 'P')
    {
        $fecha_desde = date("Y/m/d", mystrtotime($fecha_desde));
        $fecha_hasta = date("Y/m/d", mystrtotime($fecha_hasta)) . " 23:59:59";

        $fecha_dv = "case when dv.tipo = " . DocValor::TIPO_CHEQUE_PROPIO . "  then dv.fecha  when dv.tipo = " . DocValor::TIPO_CHEQUE . " then dv.fecha else d.fecha_creacion end  as fechadv";
        $select = "
            select
                    d.id, d.anulado, d.activo, date_format(d.fecha_valor, '%d/%m/%Y') as fecha, d.sucursal, d.numero, d.detalle, d.total,
                    d.fecha_valor, d.concepto_id, co.nombre as concepto_nombre, c.nombre as comprob,
                    a.matricula, d.fecha_valor,t.comprob_id, dv.fecha as fechadv,
                    case when s.alumno_id is not null then
                            concat(a.apellido, ', ', a.nombre)
                    else
                            p.razon_social
                    end as nombre ,
                    (select GROUP_CONCAT(b.nombre)
                                     from doc_valor dv
                                              inner join destino dest on dest.id = dv.destino_id
                                              inner join banco b on b.id = dest.Banco_id
                                     where dv.doc_id = d.id
                    ) as banco, co.nombre as concepto
            from doc d
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
                    left join socio s on s.id = d.socio_id
                    left join alumno a on a.id = s.alumno_id
                    left join proveedor p on p.id = s.proveedor_id
                    left join concepto co on co.id = d.concepto_id
                    left join doc_valor dv on dv.doc_id = d.id
            where d.fecha_valor BETWEEN \"$fecha_desde\" and \"$fecha_hasta\" and c.id = $comprob_id
            group  by d.id, d.anulado, d.activo, d.fecha_valor, d.sucursal, d.numero, d.detalle, d.total,
                      d.fecha_valor, d.concepto_id, co.nombre ,
                      c.nombre , a.matricula, d.fecha_valor ,t.comprob_id
            order by d.fecha_valor, d.numero
	  ";
  //    vd2($select);
      //echo($select); die;
        $rows = Helpers::qryAll($select);

        $this->renderPartial($output === 'P'?"docs1Ajax":'docs1Excel', array(
        "rows" => $rows,
        "paperSize" => $paperSize,
        "comprob_id" => $comprob_id,
        ));
    }
//----
    public function actionNovedadesClaudia($pdf = false)
    {
        $fechaDesde = Helpers::qryScalar("select max(fecha_confirmado) from liquid_conf lc where lc.confirmada = 0");
      // $fechaDesde = Helpers::qryScalar("select max(fecha_confirmado) from liquid_conf lc /* where lc.confirmada = 0 */");
        if (!$fechaDesde) {
            $fechaDesde = date("d/m/Y", time() - (1 * 24 * 60 * 60));
        }
        $this->render(
            "/informe/genNiAnDi",
            array(
            "fechas" => true,
            "fecha_desde" => $fechaDesde,
            "reporteNombre" => "novedadesClaudiaInforme",
            "ajax" => false,
            "excel" => true,
            )
        );
    }

    public function actionNovedadesClaudiaInforme()
    {
        $this->novedadesClaudiaExcel();
    }

    public function novedadesClaudiaExcel()
    {
        $this->renderPartial("informeNovedadesClaudiaEXCEL");
    }

//----

    public function actionNovedades1($pdf = false)
    {
        $fechaDesde = Helpers::qryScalar("select max(fecha_confirmado) from liquid_conf lc where lc.confirmada = 0");
        if (!$fechaDesde) {
            $fechaDesde = date("d/m/Y", time() - (1 * 24 * 60 * 60));
        }
        $this->render("/informe/genNiAnDi", array(
        "fechas" => true,
        "fecha_desde" => $fechaDesde,
        "reporteNombre" => "novedades1Informe",
        "ajax" => true,
        "excel" => true));
    }

    public function actionNovedades1Informe()
    {
      // �cto
        if ($_GET["output"] == "ajax") {
            $this->novedades1Ajax();
        } elseif ($_GET["output"] == "excel") {
            $this->novedades1Excel();
        }
    }

    public function novedades1Excel()
    {
        $this->renderPartial("informeNovedadesEXCEL");
    }

    public function novedades1Ajax()
    {
        $ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
        $paperSize = isset($_GET["paperSize"]) ? $_GET["paperSize"] : "A4";
        $fecha_desde = Helpers::fechaParaGrabar($_GET["fecha_desde"], "Y/m/d");
        $fecha_hasta = Helpers::fechaUltimoSegundo($_GET["fecha_hasta"], "Y/m/d");
        $where = "c.fecha_carga between \"$fecha_desde\" and \"$fecha_hasta\" /*and a.activo = 1*/";
        $where .= in_array($_GET["usuario_id"], array("0", "")) ? " " : " and u.id = " . $_GET["usuario_id"] . " ";
        $where .= in_array($_GET["articulo_id"], array("0", "")) ? " " : " and art.id = " . $_GET["articulo_id"] . " ";
      //$where .= $_GET["anio_id"] !== "-1" ? " and anio.id = \"" . $_GET["anio_id"] . "\" " : " ";
      //$where .= $_GET["division_id"] !== "-1" ? " and d.id = " . $_GET["division_id"] : " ";
        $nivel_id = in_array($_GET["nivel_id"], array("-1", "")) ? "-1" : $_GET["nivel_id"];
        $anio_id = in_array($_GET["anio_id"], array("-1", "")) ? "-1" : $_GET["anio_id"];
        $division_id = in_array($_GET["division_id"], array("-1", "")) ? "-1" : $_GET["division_id"];
        $whereNivel = " where activo = 1 " . (($nivel_id !== "-1") ? " and id = " . $nivel_id : " ");
        $selectNivel = "
					select id,nombre from nivel $whereNivel
			";
        if ($anio_id !== "-1") {
          //$where .= " anio.id = " . $anio_id;
            $whereAnio = " where id =  " . $anio_id;
        } else {
            $whereAnio = " where 1=1 ";
        }

        if ($division_id !== "-1") {
          //$where .= " d.id = " . $division_id;
            $whereDivision = " where id =  " . $division_id;
        } else {
            $whereDivision = " where 1=1 ";
        }

        $rows = array();
        $rowsNivel = Helpers::qryAll($selectNivel);
        foreach ($rowsNivel as $rowNivel) {
            $selectAnio = "
                select id,nombre from anio $whereAnio and nivel_id = " . $rowNivel["id"];
            $rowsAnio = Helpers::qryAll($selectAnio);
            foreach ($rowsAnio as $rowAnio) {
                $selectDivision = "
                    select id,nombre from division $whereDivision and anio_id=" . $rowAnio["id"];
                $rowsDivision = Helpers::qryAll($selectDivision);
                foreach ($rowsDivision as $rowDivision) {
                    $division_id = $rowDivision["id"];

                    $selectTotales = "
                        select art.id, art.nombre
                        from novedad_cab c
                            inner join novedad n on n.novedad_cab_id = c.id
                            inner join alumno a on a.id = n.alumno_id and a.activo = 1
                            inner join alumno_division ad on ad.Alumno_id = a.id and ad.activo = true
                            inner join division d on d.id = ad.Division_id and ad.division_id = $division_id
                            inner join anio on anio.id = d.Anio_id
                            inner join nivel on nivel.id = anio.Nivel_id
                            left join user u on u.id = c.user_id
                            left join articulo art on art.id = n.articulo_id
                        where $where
                        group by art.id, art.nombre";
                    $novsTotales = Helpers::qryAll($selectTotales);
                    foreach ($novsTotales as $rowNov) {
                        $art_id = $rowNov["id"];
                        $artNombre = $rowNov["nombre"];
                        $select = "
                        select n.id, nivel.nombre as nivel, anio.nombre as anio, d.nombre as division,
                                    concat(a.apellido, ', ', a.nombre) as alumno, a.matricula, c.fecha_carga, c.fecha, u.nombre as usuario,
                                    n.detalle, n.importe, art.nombre as articulo
                        from novedad_cab c
                            inner join novedad n on n.novedad_cab_id = c.id
                            inner join alumno a on a.id = n.alumno_id and a.activo = 1
                            inner join alumno_division ad on ad.Alumno_id = a.id and ad.activo = true
                            inner join division d on d.id = ad.Division_id and ad.division_id = $division_id
                            inner join anio on anio.id = d.Anio_id
                            inner join nivel on nivel.id = anio.Nivel_id
                            left join user u on u.id = c.user_id
                            left join articulo art on art.id = n.articulo_id
                        where $where  and art.id = $art_id
                        order by nivel.orden, anio.nombre, d.nombre, n.detalle, a.apellido, a.nombre";
                        //ve($select);
                        $novs = Helpers::qryAll($select);
                        if (count($novs) > 0) {
                            $rows[$rowNivel["nombre"]][$rowAnio["nombre"]][$rowDivision["nombre"]][$artNombre] = $novs; ///$rowNivel[ "nombre"].$rowAnio["nombre"].$rowDivision["nombre"];
                        }
                    }
                }
            }
        }
        if ($_GET["output"] == "pdf") {
            $this->renderPartial("novedades1PDF", array(
            "rows" => $rows,
            "paperSize" => $paperSize,
            ));
        } else {
            $render = $_GET["output"] == "print" ? "render" : "renderPartial";
            $this->$render("novedades1Ajax", array(
            "rows" => $rows,
            "paperSize" => $paperSize,
            "output" => $_GET["output"],
            ));
        }
    }

    public function actionGetDocDet($doc_id)
    {

        $dataValores = Helpers::qryAll("
           select v.importe, v.numero, v.fecha, v.obs, t.nombre as tipo_valor, b.nombre as banco, d.nombre as destino,
                        ch.descripcion as chequera, v.Destino_id
                from doc_valor v
                    inner join doc_valor_tipo t on t.id = v.tipo
                    left join banco b on b.id = v.banco_id
                    left join destino d on d.id = v.Destino_id
                    left join chequera ch on ch.id = v.chequera_id
                where v.Doc_id =  $doc_id
        ");
        $dataArticulos = Helpers::qryAll("
            select a.id as articulo_id, a.nombre, d.cantidad, d.importe, d.total
                from doc_det d
            inner join articulo a on a.id = d.articulo_id
                where d.Doc_id =  $doc_id
        ");
        $ret = new stdClass;
        $ret->doc_id = $doc_id;
        if ($dataValores) {
            $ret->renderValores = $this->renderPartial("docs1AjaxDetalleValores", array("data" => $dataValores, "doc_id" => $doc_id), true);
        } else {
            $ret->renderValores = null;
        }

        if ($dataArticulos) {
            $ret->renderArticulos = $this->renderPartial("docs1AjaxDetalleArticulos", array("data" => $dataArticulos, "doc_id" => $doc_id), true);
        } else {
            $ret->renderArticulos = null;
        }
        echo json_encode($ret);
    }

    public function actionAlumnoConFormaDePagoViejo()
    {
        $this->render("/informe/genAlumnoEntreFechasViejo", array("reporteNombre" => "alumnoConFormaDePagoPDF", "titulo" => "Alumno con Forma de Pago"));
    }

    public function actionAlumnoConFormaDePagoPDF2($socio_id, $fecha_desde, $fecha_hasta)
    {
        $this->renderPartial("/informe/alumnoConFormaDePagoPDF2", array(
        "socio_id" => $socio_id, "fecha_desde" => $fecha_desde, "fecha_hasta" => $fecha_hasta
        ));
    }

    public function actionAlumnoConFormaDePagoPDF($socio_id, $fecha_desde, $fecha_hasta)
    {
        $this->renderPartial("/informe/alumnoConFormaDePagoPDF", array(
        "socio_id" => $socio_id, "fecha_desde" => $fecha_desde, "fecha_hasta" => $fecha_hasta
        ));
    }

    public function actionProveedorRetencionesArchivo()
    {
        $this->render("/informe/genEntreFechas", array(
        "reporteNombre" => "proveedorRetencionesArchivoTxt",
        "txt" => true,
        "titulo" => "Retenciones-Archivo"));
    }

    public function actionProveedorRetencionesArchivoTxt($output, $fecha_desde, $fecha_hasta)
    {
        $this->renderPartial("/informe/proveedorRetencionesArchivo", array(
        "fecha_desde" => $fecha_desde, "fecha_hasta" => $fecha_hasta));
    }

    public function actionProveedorRetenciones($output, $fecha_desde, $fecha_hasta)
    {
        $this->render("/informe/genEntreFechas", array(
        "reporteNombre" => "proveedorConRetencionesPDF",
        "fecha_desde" => $fecha_desde, "fecha_hasta" => $fecha_hasta,
        "titulo" => "Retenciones"));
    }

    public function actionProveedorRetencionesPDF($socio_id, $fecha_desde, $fecha_hasta)
    {
        $this->renderPartial("/informe/proveedorConRetencionesPDF", array(
        "socio_id" => $socio_id, "fecha_desde" => $fecha_desde, "fecha_hasta" => $fecha_hasta
        ));
    }

    public function actionProveedorConFormaDePago()
    {
        $this->render("/informe/genProveedorEntreFechas", array("reporteNombre" => "proveedorConFormaDePagoPDF", "titulo" => "Proveedor Con Cuenta Corriente"));
    }

    public function actionProveedorConFormaDePagoPDF($socio_id, $fecha_desde, $fecha_hasta)
    {
        $this->renderPartial("/informe/proveedorConFormaDePagoPDF", array(
        "socio_id" => $socio_id, "fecha_desde" => $fecha_desde, "fecha_hasta" => $fecha_hasta
        ));
    }

    public function actionAlumnoCuentaCorriente()
    {
        $this->render("/informe/genAlumnoEntreFechasViejo", array("reporteNombre" => "alumnoCtaPDF", "titulo" => "Alumno Cuenta Corriente"));
    }

    public function actionAlumnoCtaPDF($socio_id, $fecha_desde, $fecha_hasta)
    {
        $this->renderPartial("/informe/alumnoCtaPDF", array(
        "socio_id" => $socio_id, "fecha_desde" => $fecha_desde, "fecha_hasta" => $fecha_hasta
        ));
    }

    public function actionProveedoresSaldo()
    {
        $this->render("/informe/genProveedoresSaldos", array("reporteNombre" => "ProveedoresSaldoPDF", "titulo" => "Saldos de Proveedores "));
    }

    public function actionProveedoresSaldoPDF($fecha_desde, $fecha_hasta, $solosaldo)
    {
        $this->renderPartial("/informe/proveedoresSaldoPDF", array(
        "fecha_desde" => $fecha_desde, "fecha_hasta" => $fecha_hasta, "solo_saldo" => $solosaldo
        ));
    }

    public function actionConceptoResumen()
    {
        $this->render("/informe/genConceptoResumen", array("reporteNombre" => "conceptoResumenPDF", "titulo" => "Resumen de Conceptos"));
    }

    public function actionConceptoResumenPDF($fecha_desde, $fecha_hasta, $concepto_id)
    {
        $this->renderPartial("/informe/conceptoResumenPDF", array(
        "fecha_desde" => $fecha_desde, "fecha_hasta" => $fecha_hasta, "concepto_id" => $concepto_id
        ));
    }

    public function actionInformeRecibos()
    {
        $this->render("/informe/genEntreFechas", array(
        "reporteNombre" => "informeRecibosAjax",
        "pdf" => true,
        "excel" => true,
        "ajax" => false,
        "titulo" => "Recibos entre Fechas",
        ));
    }

    public function actionInformeRecibosAjax($fecha_desde, $fecha_hasta, $output)
    {
        if ($output == "excel") {
            $this->renderPartial("/informe/informeRecibosEXCEL", array(
            "fecha_desde" => $fecha_desde,
            "fecha_hasta" => $fecha_hasta,
              ));
        } elseif ($output == "pdf") {
            $this->renderPartial("/informe/informeRecibosPDF", array(
            "fecha_desde" => $fecha_desde,
            "fecha_hasta" => $fecha_hasta,
              ));
        }
    }

    public function actioninformeBanco()
    {
      //No cambiar al informe genérico!!!!!!!!!!!!!!!!!!!
        $this->render("/informe/genEntreFechasBanco", array("reporteNombre" => "informeBancoPDF"));
    }

    public function actioninformeBancoPDF($destino_id, $fecha_desde, $fecha_hasta)
    {
      //No cambiar al informe genérico!!!!!!!!!!!!!!!!!!!
        $this->renderPartial("/informe/informeBancoPDF", array(
        "destino_id" => $destino_id, "fecha_desde" => $fecha_desde, "fecha_hasta" => $fecha_hasta
        ));
    }

    public function actionAlumnosConSaldo()
    {
        $this->renderPartial("/informe/informeAlumnosConSaldoPDF", array());
    }

    public function actionAlumnosConSaldoExcel()
    {
        $this->renderPartial("/informe/informeAlumnosConSaldoExcel", array());
    }

    public function actioninformeCobranza()
    {
        $this->render("/informe/genInformeCobranza", array("reporteNombre" => "informeCobranzaExcel"));
    }

    public function actioninformeCobranzaExcel($nivel_id, $nro_debito = 0)
    {
        $this->renderPartial("/informe/informeCobranzaExcel", array(
        "nivel_id" => $nivel_id,
        "nro_debito" => $nro_debito));
    }

    public function actionPlanDeCuentas()
    {
        $plan = $this->renderPartial("planDeCuentasHTML", true, true);
        $this->renderPartial("planDeCuentas", array("plan" => $plan));
    }

    public function actionAlumnos1()
    {
        $this->render("/informe/genNiAnDi", array(
        "reporteNombre" => "alumnos1Ajax",
        "pdf" => true,
        "excel" => true,
        "ajax" => false,
        ));
    }

    public function actionAlumnos1Ajax($nivel_id, $anio_id, $division_id, $output = "ajax")
    {
        if ($output == "excel") {
            $this->renderPartial("alumnos1Excel", array(
            "nivel_id" => $nivel_id,
            "anio_id" => $anio_id,
            "division_id" => $division_id,
              ));
        } elseif ($output == "pdf") {
            $this->renderPartial("alumnos1PDF", array(
            "nivel_id" => $nivel_id,
            "anio_id" => $anio_id,
            "division_id" => $division_id,
              ));
        }
    }

    public function actionAlumnosFede1()
    {
        $this->render("/informe/genNiAnDi", array(
        "reporteNombre" => "alumnosFede1Ajax",
        "pdf" => false,
        "excel" => true,
        "ajax" => false,
        ));
    }

    public function actionAlumnosFede1Ajax($nivel_id, $anio_id, $division_id, $output = "excel")
    {
      // Lo pidiÃ³ haydee, es la planilla de buffet del sistema viejo
        if ($output == "excel") {
            $this->renderPartial("/informe/alumnosFede1EXCEL", array(
            "nivel_id" => $nivel_id,
            "anio_id" => $anio_id,
            "division_id" => $division_id,
              ));
        }
    }

    public function actionPdftest()
    {
        $this->renderPartial("/informe/pdftest");
    }

    public function actionCtaVieja()
    {
        $matricula = 2690;
        $db = Yii::app()->fb->connection;
        $select = "
          select * from te_trae_cc($matricula,Null)";
        $sth = ibase_query($db, $select);
        $rows = array();
        while ($row = ibase_fetch_assoc($sth)) {
            $rows[] = $row;
        }
        $this->render("ctaVieja", array("rows" => $rows));
    }

    public function actionAlumnoConFormaDePago()
    {
        $this->render("/informe/genAlumnoEntreFechas", array(
        "reporteNombre" => "alumnoConFormaDePagoInforme",
        "pdf" => true,
        "excel" => false,
        "ajax" => true,
        ));
    }

    public function actionAlumnoConFormaDePagoInforme($output, $fecha_desde, $fecha_hasta, $socio_id)
    {

        if ($output == "pdf") {
            $this->renderPartial("/informe/ctaCteConFormaDePagoPDF", array(
            "socio_id" => $socio_id,
            "fecha_desde" => $fecha_desde,
            "fecha_hasta" => $fecha_hasta,
              ));
        } elseif ($output == "ajax") {
            $this->renderPartial("/informe/ctaCteConFormaDePagoAJAX", array(
            "socio_id" => $socio_id,
            "fecha_desde" => $fecha_desde,
            "fecha_hasta" => $fecha_hasta,
              ));
        }
    }

    public function actionCtaCteNuevo()
    {
        $this->render("/informe/genAlumnoEntreFechas", array(
        "reporteNombre" => "ctaCteNuevoInforme",
        "pdf" => true,
        "excel" => false,
        "ajax" => true,
        ));
    }

    public function actionCtaCteNuevoInforme($output, $fecha_desde, $fecha_hasta, $socio_id)
    {
        if ($output == "pdf") {
            $this->renderPartial("/informe/informeCtaCteNuevoPDF", array(
            "socio_id" => $socio_id,
            "fecha_desde" => $fecha_desde,
            "fecha_hasta" => $fecha_hasta,
              ));
        } elseif ($output == "ajax") {
            $this->renderPartial("/informe/informeCtaCteNuevoAJAX", array(
            "socio_id" => $socio_id,
            "fecha_desde" => $fecha_desde,
            "fecha_hasta" => $fecha_hasta,
              ));
        }
    }
    public function actionTelefonos()
    {
        $this->renderPartial("/informe/telefonosExcel");
    }

    public function actionTelefonosInforme($output, $fecha_desde, $fecha_hasta, $socio_id)
    {
        $this->renderPartial("/informe/alumnosFede1EXCEL", array(
          "nivel_id" => $nivel_id,
          "anio_id" => $anio_id,
          "division_id" => $division_id,));
    }

    public function actionAlumnosConRiesgoAcademico()
    {
        $this->render("/informe/genNiAnDi", array(
        "reporteNombre" => "alumnosConRiesgoAcademicoInforme",
        "pdf" => false,
        "excel" => true,
        "ajax" => false,
        ));
    }

    public function actionAlumnosConRiesgoAcademicoInforme($nivel_id, $anio_id, $division_id, $output = "ajax")
    {
        if ($output == "excel") {
            if ($nivel_id == -1 or $anio_id == -1 or $division_id == -1) {
                Helpers::error("Error!", "Debe especificar Nivel, AÃ±o y DivisiÃ³n");
                die;
            }
            $this->renderPartial("/informe/alumnosConRiesgoAcademicoEXCEL", array(
            "nivel_id" => $nivel_id,
            "anio_id" => $anio_id,
            "division_id" => $division_id,
              ));
        }
    }

    public function actionConciliacionInforme($conciliacion, $destino_id, $output = "ajax")
    {

        $this->renderPartial("/informe/conciliacionPDF", array(
        "conciliacion" => $conciliacion,
        "destino_id" => $destino_id,
            ));
    }

    public function actionEmails()
    {
        $s = "select p.apellido, p.nombre, p.email
                from alumno a
                    inner join pariente p on p.id = a.vive_con_id and p.email is not null
                where a.activo = 1
                order by p.apellido, p.nombre";
        $rows = Helpers::qryAll($s);
        $cols = array(
        "nombre" => array("title" => "Alumno"),
        "email" => array("title" => "Correo"),
        );
        $this->renderPartial("emailsExcel", array("rows" => $rows, "cols" => $cols));
        ;
    }

    public function actionInformeAlumnos2()
    {
        $this->render("/informe/genNiAnDi", array(
        "reporteNombre" => "informeAlumnos2Ajax",
        "pdf" => false,
        "excel" => true,
        "ajax" => false,
        ));
    }

    public function actionInformeAlumnos2Ajax($nivel_id, $anio_id, $division_id, $output = "excel")
    {
      // Lo pidiÃ³ haydee, es la planilla de buffet del sistema viejo
        if ($output == "excel") {
            $this->renderPartial("/informe/informeAlumnos2Excel", array(
            "nivel_id" => $nivel_id,
            "anio_id" => $anio_id,
            "division_id" => $division_id,
              ));
        }
    }

    public function actionAlumnosDNI()
    {
        $this->render("/informe/genNiAnDi", array(
        "reporteNombre" => "alumnosDNIInforme",
        "pdf" => true,
        "excel" => true,
        "ajax" => false,
        "sexo" => true,
        ));
    }

    public function actionAlumnosDNIInforme($nivel_id, $anio_id, $division_id, $sexo, $output = "ajax")
    {
        if ($output == "excel") {
            if ($nivel_id == -1 or $anio_id == -1 or $division_id == -1) {
                Helpers::error("Error!", "Debe especificar Nivel, Año y División");
                die;
            }
            $this->renderPartial("/informe/alumnosDNIEXCEL", array(
            "nivel_id" => $nivel_id,
            "anio_id" => $anio_id,
            "division_id" => $division_id,
            "sexo" => $sexo,
              ));
        } elseif ($output == "pdf") {
            $this->renderPartial("/informe/alumnosDNIPDF", array(
            "nivel_id" => $nivel_id,
            "anio_id" => $anio_id,
            "division_id" => $division_id,
            "sexo" => $sexo,
            ));
        }
    }

    public function actionAlumnoInasistencias()
    {
        $this->render("/informe/genNiAnDiAl", array(
        "reporteNombre" => "alumnoInasistenciasInforme",
        "pdf" => false,
        "excel" => true,
        "ajax" => false,
        "fechas" => true,
        "fecha_desde" => date("d/m/Y", strtotime("01/01/2013"))
        ));
    }

    public function actionAlumnoInasistenciasInforme($alumno_id, $fecha_desde, $fecha_hasta, $output = "ajax")
    {
        if ($output == "excel") {
            $this->renderPartial("/informe/alumnosInasistenciasEXCEL", array(
            "alumno_id" => $alumno_id,
            "fecha_desde" => $fecha_desde,
            "fecha_hasta" => $fecha_hasta,
              ));
        }
    }

    public function actionInformeAlumnos1()
    {
        $this->render("/informe/genNiAnDi", array(
        "reporteNombre" => "informelumnos1Ajax",
        "pdf" => false,
        "excel" => true,
        "ajax" => false,
        ));
    }

    public function actionInformelumnos1Ajax($nivel_id, $anio_id, $division_id, $output = "ajax")
    {
        if ($output == "excel") {
            $this->renderPartial("/informe/informePrimariaEXCEL", array(
            "nivel_id" => $nivel_id,
            "anio_id" => $anio_id,
            "division_id" => $division_id,
              ));
        }
    }

    public function actionInformeAlumnosASR1()
    {
        $this->render("/informe/genNiAnDi", array(
        "reporteNombre" => "informelumnosASR1Ajax",
        "pdf" => false,
        "excel" => true,
        "ajax" => false,
        ));
    }

    public function actionInformelumnosASR1Ajax($nivel_id, $anio_id, $division_id, $output = "ajax")
    {
        if ($output == "excel") {
            $this->renderPartial("/informe/informelumnosASR1", array(
            "nivel_id" => $nivel_id,
            "anio_id" => $anio_id,
            "division_id" => $division_id,
              ));
        }
    }

    public function actionNC()
    {
        $this->pageTitle = "Notas de Crédito";
        $this->render("/informe/genEntreFechas", array(
        "reporteNombre" => "NCInforme",
        "pdf" => false,
        "excel" => true,
        "ajax" => false,
        "titulo" => "Notas de Crédito"
        ));
    }

    function actionNCInforme($fecha_desde, $fecha_hasta, $output = "ajax")
    {
        if ($output == "excel") {
            $fecha_desde = date("Y/m/d", mystrtotime($fecha_desde));
            $fecha_hasta = date("Y/m/d", mystrtotime($fecha_hasta)) . " 23:59:59";
            $this->renderPartial("/informe/NCexcel", array(
            "fecha_desde" => $fecha_desde,
            "fecha_hasta" => $fecha_hasta
              ));
        } elseif ($output == "ajax") {
            $fecha_desde = date("Y/m/d", mystrtotime($fecha_desde));
            $fecha_hasta = date("Y/m/d", mystrtotime($fecha_hasta)) . " 23:59:59";
            $this->renderPartial("/informe/NCajax", array(
            "fecha_desde" => $fecha_desde,
            "fecha_hasta" => $fecha_hasta
              ));
        }
    }

    public function actionInformeIngresantes1()
    {
        $this->render("/informe/genNiAnDi", array(
        "reporteNombre" => "alumnosIngresantesAjax",
        "pdf" => true,
        "excel" => true,
        "ajax" => false,
        ));
    }

    function actionAlumnosIngresantesAjax($nivel_id = null, $anio_id = null, $division_id = null, $output = "ajax")
    {
        if ($output == "excel" and false) { // no existe
            $this->renderPartial("/informe/informeIngresantesEXCEL", array(
            "nivel_id" => $nivel_id,
            "anio_id" => $anio_id,
            "division_id" => $division_id,
              ));
        } elseif ($output == "pdf") {
            $this->renderPartial("/informe/informeIngresantesPDF", array(
            "nivel_id" => $nivel_id,
            "anio_id" => $anio_id,
            "division_id" => $division_id,
            ));
        }
    }

    public function actionAlumnosCompleto1()
    {
        $this->render("/informe/genNiAnDi", array(
        "reporteNombre" => "AlumnosCompleto1Ajax",
        "pdf" => true,
        "excel" => true,
        "ajax" => false,
        "sexo" => true,
        ));
    }

    function actionAlumnosCompleto1Ajax($nivel_id, $anio_id, $division_id, $sexo = null, $output = "ajax")
    {

        if ($output == "excel") {
            $this->renderPartial("/informe/informealumnoscompletoEXCEL", array(
            "division_id" => $division_id,
              ));
        } elseif ($output == "pdf") {
            $this->renderPartial("/informe/informealumnoscompletoPDF", array(
            "division_id" => $division_id,
            ));
        }
    }

    public function actionTest()
    {
        $this->render("test");
    }

    public function actionActasVolantes()
    {
        $this->render("/informe/genNiAnDiAs", array(
        "reporteNombre" => "actasVolantesInforme",
        "pdf" => true,
        "excel" => false,
        "ajax" => false,
        ));
    }

    public function actionActasVolantesInforme($output = "ajax")
    {
        $this->renderPartial("/informe/actasVolantesInformePDF", array(
            ));
    }

    public function actionBoletines()
    {
        $this->render("/informe/genNiAnDi", array(
        "reporteNombre" => "boletinesAjax",
        "pdf" => true,
        ));
    }

    public function actionBoletinesAjax($nivel_id, $anio_id, $output, $division_id, $fecha)
    {
        $this->renderPartial("boletinesNuevoAjax", array(
        "division_id" => $division_id,
        "fecha" => $fecha,
        ));
    }

    public function actionBoletinesPrimaria()
    {
        $this->render("/informe/genNiAnDi", array(
        "reporteNombre" => "boletinesPrimariaAjax",
        "pdf" => true,
        ));
    }

    public function actionBoletinesPrimariaAjax($nivel_id, $anio_id, $output, $division_id, $fecha)
    {
        $this->renderPartial("boletinesNuevoAjax", array(
        "division_id" => $division_id,
        "fecha" => $fecha,
        ));
    }

    public function actionTestNotasAdmin()
    {
      //$notas = NotasAdmin::getNotas(1283);
        vd(NotasAdmin::getNotasDivision(367));
    }

    public function actionVtoParaBorrar()
    {
        $s = "select concat(a.apellido, \", \", a.nombre) as alumno, saldo(s.id,null) as saldo
                from doc d
                    inner join socio s on s.id = d.socio_id
                    inner join alumno a on a.id = s.Alumno_id
                    inner join doc_liquid l on l.id = d.doc_liquid_id
                    inner join liquid_conf c on c.id = l.liquid_conf_id
                where d.total = 40 and d.detalle = \"Recargo 2do. Vto.\" and c.id = 5 and d.fecha_modificacion is null and d.anulado = 0 and d.saldo = d.total and saldo(d.socio_id,null) <= 2400
                order by a.apellido, a.nombre
        ";
        $rows = Helpers::qryAll($s);
        $cols = array("saldos" => array(
        ""
        ));
        $t = Helpers::getTable("saldos", $rows);
        $t .= "
            <script>
                $(function(){
                    var texto = \"<div style='margin:4px'>\" + $(\".tablegen-table-tr-search\").length + \" registros \" + \"</div>\";
                    $(\"#tablegenquickfilter-div\").append(texto);
                });
            </script>
        ";
        $this->renderPartial("/layouts/main", array("content" => $t));
    }

    public function actionChkSaldo()
    {
        $this->render("/informe/genProveedorEntreFechas", array("reporteNombre" => "chkSaldoHtml", "fechas" => false, "titulo" => "Chequea los mivimientos"));
    }

    public function actionChkSaldoHtml($socio_id)
    {
        $this->render("chkSaldoHtml", array("socio_id" => $socio_id));
    }

    public function actionEmision()
    {
        $this->render("emision", array(
        ));
    }

    public function actionAlumnosConSusFamilias()
    {
        $this->render("/informe/genNiAnDi", array(
        "reporteNombre" => "alumnosConSusFamiliasInforme",
        "pdf" => true,
        "excel" => true,
        "ajax" => false,
        "sexo" => true,
        ));
    }

    public function actionAlumnosConSusFamiliasInforme($nivel_id, $anio_id, $division_id, $sexo, $output = "ajax")
    {
        if ($output == "excel") {
            if ($nivel_id == -1 or $anio_id == -1 or $division_id == -1) {
                Helpers::error("Error!", "Debe especificar Nivel, Año y División");
                die;
            }
            $this->renderPartial("/informe/alumnosConSusFamilias", array(
            "nivel_id" => $nivel_id,
            "anio_id" => $anio_id,
            "division_id" => $division_id,
            "sexo" => $sexo,
              ));
        } elseif ($output == "pdfxxxxx") {
            $this->renderPartial("/informe/alumnosDNIPDF", array(
            "nivel_id" => $nivel_id,
            "anio_id" => $anio_id,
            "division_id" => $division_id,
            "sexo" => $sexo,
            ));
        }
    }

  /*
   * Hola Juan, necesito por favor, visualizar e imprimir listados de alumnos
   * del nivel primario en donde figure en una sola planilla los siguientes
   * datos: nombre y apellido del alumno, Nº de documento, fecha de nacimiento,
   * dirección, teléfono fijo y ambos celulares de la mamá y papá.
   *                                                         GRACIAS !!!!!
   */

    public function actionAlumnos2()
    {
        $this->render("/informe/genNiAnDi", array(
        "reporteNombre" => "alumnos2Informe",
        "pdf" => false,
        "excel" => true,
        "ajax" => false,
        ));
    }

    public function actionAlumnos2Informe($nivel_id, $anio_id, $division_id, $output)
    {
        if ($output == "excel") {
            if ($nivel_id == -1) {
                Helpers::error("Error!", "Debe especificar al menos el Nivel");
                die;
            }
            $this->renderPartial("/informe/alumnos2EXCEL", array(
            "nivel_id" => $nivel_id,
            "anio_id" => $anio_id,
            "division_id" => $division_id
              ));
        } elseif ($output == "pdfxxxxx") {
            $this->renderPartial("/informe/alumnosDNIPDF", array(
            "nivel_id" => $nivel_id,
            "anio_id" => $anio_id,
            "division_id" => $division_id,
            "sexo" => $sexo,
            ));
        }
    }

    public function actionAlumnos3()
    {
        $this->render("/informe/genNiAnDi", array(
        "reporteNombre" => "alumnos3Informe",
        "pdf" => false,
        "excel" => true,
        "ajax" => false,
        ));
    }

    public function actionAlumnosParientesDomicilio()
    {
        $this->renderPartial("/informe/informeAlumnosParienteDomicilio");
    }

    public function actionAlumnos3Informe($nivel_id, $anio_id, $division_id, $output)
    {
        if ($output == "excel") {
            if ($nivel_id == -1) {
                Helpers::error("Error!", "Debe especificar al menos el Nivel");
                die;
            }
            $data = Helpers::qry("
              select n.nombre as nivel, a.nombre as curso, d.nombre as division
               from division d
                inner join anio a on a.id = d.anio_id
                inner join nivel n on n.id = a.nivel_id
               where d.id = $division_id
              ");
            $this->renderPartial("/informe/informeAlumnos3Excel", array(
            "nivel_id" => $nivel_id,
            "anio_id" => $anio_id,
            "division_id" => $division_id,
            "data" => $data,
              ));
        } elseif ($output == "pdfxxxxx") {
            $this->renderPartial("/informe/alumnosDNIPDF", array(
            "nivel_id" => $nivel_id,
            "anio_id" => $anio_id,
            "division_id" => $division_id,
            "sexo" => $sexo,
            ));
        }
    }

    public function actionAlumnosCompletoExcel()
    {
        $this->render("/informe/genNiAnDi", array(
        "reporteNombre" => "alumnosCompletoInforme",
        "pdf" => false,
        "excel" => true,
        "ajax" => false,
        ));
    }

    public function actionAlumnosCompletoInforme($nivel_id, $anio_id, $division_id, $output)
    {
        if ($output == "excel") {
            if ($nivel_id == -1) {
        //        Helpers::error("Error!", "Debe especificar al menos el Nivel");
        //        die;
            }
            $this->renderPartial("/informe/alumnosCompletoEXCEL", array(
            "nivel_id" => $nivel_id,
            "anio_id" => $anio_id,
            "division_id" => $division_id
              ));
        } elseif ($output == "pdfxxxxx") {
            $this->renderPartial("/informe/alumnosDNIPDF", array(
            "nivel_id" => $nivel_id,
            "anio_id" => $anio_id,
            "division_id" => $division_id,
            "sexo" => $sexo,
            ));
        }
    }

    public function actionRegistroMatricula($pdf = false)
    {
        $this->render(
            "/informe/genNiAnDi",
            array(
            "reporteNombre" => "registroMatriculaInforme",
            "pdf" => true,
            "excel" => true,
            )
        );
    }

    public function actionRegistroMatriculaInforme($output)
    {
        $division_id = filter_input(INPUT_GET, "division_id");
        $select = "
				SELECT
						date_format(a.fecha_ingreso,'%d/%m/%y') as FecIngr, a.matricula,
						CONCAT(a.apellido , ', ', a.nombre) AS nombres, date_format(a.fecha_nacimiento,'%d/%m/%y') AS fecha_nacimiento,
						DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(a.fecha_nacimiento)), '%Y')+0 AS edad,
						a.sexo, p2.nombre AS nacionalidad, a.numero_documento,
						'' AS vacuna_fecha, '' AS vacuna_result, '' AS buco_fecha, '' AS buco_result,
						CONCAT(p1.apellido, ', ', p1.nombre) AS pariente_nombre, p.nombre AS nacionalidad_pariente,
						CAP_FIRST(p1.profesion) AS pariente_profesion, CAP_FIRST(pariente_domicilio(p1.id)) as pariente_domicilio,
						p1.telefono_casa AS telefono, '' AS egreso, '' AS observaciones
				 FROM alumno a
					INNER JOIN alumno_division ad ON a.id = ad.Alumno_id AND ad.activo
					INNER JOIN alumno_estado ae ON a.estado_id = ae.id AND ae.activo_edu
					INNER JOIN pariente p1 ON a.vive_con_id = p1.id
					left JOIN localidad l ON p1.lugar_nacimiento = l.id
					left JOIN pais p ON l.Pais_id = p.id
					left JOIN localidad l1 ON a.localidad_id = l1.id
					left JOIN pais p2 ON l1.Pais_id = p2.id
				where ad.division_id = $division_id
				order by a.apellido, a.nombre
			";
        $data = Helpers::qryAll($select);
  //          vd($output);
        if ($output == "excel") {
            $this->renderPartial("registroMatriculaExcel", array("data" => $data));
        } else {
            $division_id = filter_input(INPUT_GET, "division_id");
            $data = $this->renderPartial("registroMatriculaPDF_rows", array("division_id" => $division_id), true);
            $this->renderPartial("registroMatriculaPDF", array("data" => $data));
        }
    }

    public function actionRegistroMatriculaExcel()
    {
        $division_id = filter_input(INPUT_GET, "division_id");
        $this->renderPartial("registroMatriculaExcel", array("data" => $data));
    }
}
