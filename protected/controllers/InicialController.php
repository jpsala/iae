<?php

class InicialController extends CController
{

  public $layout = "nuevo";

  public function actionUsuariosInicial(){
    $this->pageTitle = "Usuarios Inicial";
    $this->layout    = "nuevo";
    $select = "
      SELECT p.apellido, p.nombre, p.email, p.password, GROUP_CONCAT(concat(a.matricula, '-',substr(a.nombre,1,10))) AS hijos
      FROM socio s 
        INNER JOIN pariente p ON s.Pariente_id = p.id
        INNER JOIN familia f2 ON p.Familia_id = f2.id
        INNER JOIN alumno a ON f2.id = a.Familia_id
      WHERE p.email IS NOT NULL AND p.password IS NOT NULL AND LENGTH(p.password) < 10
            AND 
        (SELECT COUNT(*) 
          FROM familia f 
            INNER JOIN alumno a ON f.id = a.Familia_id 
            INNER JOIN alumno_division ad ON a.id = ad.Alumno_id AND ad.activo
            INNER JOIN division d ON ad.Division_id = d.id
            INNER JOIN anio a1 ON d.Anio_id = a1.id
          WHERE f.id = f2.id AND a1.Nivel_id = 1 ) > 0
      GROUP BY p.id
      ORDER BY p.apellido, p.nombre
    ";
    // var_dump($selectCta);die;
    $data = Helpers::qryAll($select);
    $this->render('usuariosInicial', ['data'=>$data]);
  }

  public function actionTestMap()
  {
    $this->render("testMap");
  }

  public function actionTestMap2()
  {
    $this->render("testMap2");
  }

  public function actionTestGetAreas()
  {
    echo json_encode(Helpers::qryAll("SELECT * FROM inicial_area"));
  }

  public function actionCargaIndex()
  {
    $this->pageTitle = "Evaluación narrada";
    $this->layout    = "nuevo";
    $this->render("cargaIndex");
  }

  public function actionDivisionesAjax($anio_id)
  {
    $anio_id           = $anio_id ? $anio_id : -1;
    $divisiones        = Division::model()->findAll("anio_id = $anio_id");
    $divisionesOptions = array("class" => "form-control", "empty" => "");
    $divisionesData    = CHtml::listData($divisiones, "id", "nombre");
    $selDivisiones     = CHtml::dropDownList("division-select", null, $divisionesData, $divisionesOptions);
    echo $selDivisiones;
  }

  public function actionNotasAjax($division_id, $area_id)
  {
    $this->renderPartial("cargaAjax", array(
      "division_id" => $division_id, "area_id" => $area_id
    ));
  }

  public function actionGrabaNotas()
  {
    $ciclo_id = Ciclo::getActivoId();
    $cant     = 0;
//        $especial = $_POST['especial'];
    //$ciclo_id = Ciclo::getActivoId();
    foreach ($_POST["notas"] as $nota) {
      $tipo               = $nota["tipo"];
      $nota_id            = $nota["nota_id"];
      $obs_id             = $nota["obs_id"];
      $area_id            = $nota["area_id"];
      $alumno_division_id = $nota["alumno_division_id"];
      $periodo_id         = $nota["periodo_id"];
      $sub_periodo_id     = $nota["sub_periodo_id"];
      $materia_id         = $nota["materia_id"];
      $tildado            = $nota["nota"];
      $obs                = $nota["obs"];
      $texto              = $nota["texto"];
      $firma              = isset($nota["firma"]) ? $nota["firma"] : "";
      $firma_id           = isset($nota["firma_id"]) ? $nota["firma_id"] : "";
      if ($tipo == "nota") {
        if ($nota_id > 0) {
//		  if (trim($obs) !== "") {
          $cant += Helpers::qryExec("
							update inicial_nota set tildado = \"$tildado\", texto = \"$texto\" where id = $nota_id
						");
//		  } else {
//			$cant += Helpers::qryExec("
//							delete from  inicial_nota  where id = $nota_id
//						");
//		  }
        } else {
          if (trim($tildado) !== "" or texto !== "") {
            $cant += Helpers::qryExec("
							INSERT INTO inicial_nota (texto, tildado, periodo_id, sub_periodo_id, materia_id,  alumno_division_id)
								VALUES (\"$texto\", \"$tildado\", $periodo_id , $sub_periodo_id, $materia_id,  $alumno_division_id)
						");
          }
        }
      } else {
        if ($tipo == "obs") {
          if ($obs_id > 0) {
            if (trim($obs) !== "") {
              $cant += Helpers::qryExec("
							update inicial_observacion set texto = \"$obs\" where id = $obs_id
						");
            } else {
              $cant += Helpers::qryExec("
							delete from  inicial_observacion  where id = $obs_id
						");
            }
          } else {
            $cant += Helpers::qryExec("
							INSERT INTO inicial_observacion (texto, periodo_id, area_id,  alumno_division_id)
								VALUES (\"$obs\", $periodo_id , $area_id, $alumno_division_id)
						");
          }
        } else {
          if ($tipo == "firma") {
            if ($firma_id > 0) {
              $cant += Helpers::qryExec("
						              update inicial_firma
						              set firmado=$tildado,docente=\"$firma\"
						              where id = $firma_id
						            ");
            } else {
              $cant += Helpers::qryExec("
                                    INSERT INTO inicial_firma (ciclo_id, periodo_id, area_id,  alumno_division_id, docente, firmado)
                                        VALUES ($ciclo_id,  $periodo_id , $area_id, $alumno_division_id, \"$firma\", $tildado)
                                    ");
            }
          }
        }
      }
    }
    echo $cant;
  }

  public function actionAreaIndex()
  {
    $this->pageTitle = "Áreas y contenidos";
    $this->render("areaIndex");
  }

  public function actionAreasAjax($anio_id = null)
  {
    if (!$anio_id) {
      echo json_encode(Helpers::qryAll("SELECT ia.id, ia.especial, concat(ia.nombre/*, '(', coalesce(a.nombre,'S/A'), ')'*/) AS nombre, ia.abreviacion, ia.docente, ia.seccion_id
        FROM inicial_area ia
                left JOIN anio a ON a.id = ia.seccion_id
              ORDER BY ia.nombre, a.nombre
        "));
    } else {
      echo json_encode(Helpers::qryAll("
        select ia.id, ia.especial, concat(ia.nombre/*, '(', a.nombre, ')'*/) as nombre, ia.abreviacion, ia.docente
        from inicial_area ia
                inner join anio a on a.id = ia.seccion_id
        where seccion_id is null or seccion_id = $anio_id"));
    }
  }

  public function actionAreasSelectAjax($anio_id = null)
  {
    $anio_id      = $anio_id ? $anio_id : -1;
    $cond         = !$anio_id ? "seccion_id is null" : "seccion_id = $anio_id or seccion_id is null";
    $areas        = InicialArea::model()->findAll($cond);
    $areasOptions = array("class" => "form-control", "empty" => "");
    $areasData    = CHtml::listData($areas, "id", "nombre");
    $selAreas     = CHtml::dropDownList("area-select", null, $areasData, $areasOptions);
    echo $selAreas;
  }

  public function actionSeccionesAjax()
  {
    $nivel_inicial_id = Nivel::NIVEL_INICIAL;
    echo json_encode(Helpers::qryAll("select * from anio where nivel_id=$nivel_inicial_id"));
  }

  public function actionContenidosAjax($area_id)
  {
    if ($area_id) {
      echo json_encode(Helpers::qryAll("select im.*, (SELECT count(*) FROM inicial_nota in1  WHERE in1.materia_id = im.id) AS notas
		from inicial_materia im
		where im.area_id = $area_id"));
    }
  }

  public function actionGrabaContenidosAjax()
  {
    $post       = $_POST["data"];
    $data       = $post["data"];
    $contenidos = isset($post["contenidos"]) ? $post["contenidos"] : array();
    $area_id    = $data["area_id"];
    $seccion_id = $data["seccion_id"] ? $data["seccion_id"] : null;
    //$docente_id = $data["docente_id"] ? $data["docente_id"] : null;
    $docente    = $data["docente"] ? $data["docente"] : null;
    $areaNombre = $data["areaNombre"];
    $especial   = ($data["especial"] === 'true') ? 1 : 0;
    if ($area_id == -1) {
      Helpers::qryExec("INSERT INTO inicial_area(nombre, seccion_id, docente, especial) VALUES(:nombre, :seccion_id, :docente, :especial)", array(
        "nombre" => $areaNombre, "seccion_id" => $seccion_id,
        "docente" => $docente, "especial" => $especial
        //                    "docente_id" => $docente_id
      ));
      $area_id = Helpers::lastInsertedId();
    } else {
//      vd2($seccion_id);
//      vd2($areaNombre);
      Helpers::qryExec("UPDATE inicial_area SET nombre=:nombre,
                                    seccion_id = :seccion_id, docente = :docente, especial = :especial
                                  WHERE id = :area_id", array(
          "nombre" => $areaNombre, "seccion_id" => $seccion_id,
          "area_id" => $area_id, "docente" => $docente, "especial" => $especial
          //                        "area_id" => $area_id, "docente_id" => $docente_id
        )
      );
    }
    foreach ($contenidos as $contenido) {
      if ($contenido["id"]) {
        Helpers::qryExec("UPDATE inicial_materia SET nombre=:nombre, texto=:texto WHERE id = :id"
          , array(
            "nombre" => $contenido["nombre"],
            "texto" => $contenido["texto"],
            "id" => $contenido["id"]
          )
        );
      } else {
        Helpers::qryExec("INSERT INTO inicial_materia(area_id, nombre,texto) VALUES(:area_id, :nombre, :texto)"
          , array(
            "area_id" => $area_id,
            "nombre" => $contenido["nombre"],
            "texto" => $contenido["texto"]
          )
        );
      }
    }
  }

  public function actionBorraContenidosAjax($contenido_id, $force = false)
  {
    if ($force) {
      echo Helpers::qryExec("
		delete in1.* from inicial_nota in1
		  INNER JOIN inicial_materia im ON im.id = in1.materia_id AND im.id = $contenido_id
	  ");
    }
    echo Helpers::qryExec("
				delete from inicial_materia where id = $contenido_id
			");
  }

  public function actionBorraAreaAjax($area_id)
  {
    Helpers::qryExec("
				delete from inicial_firma where area_id = $area_id
			");
    Helpers::qryExec("
				delete from inicial_observacion where area_id = $area_id
			");
    echo Helpers::qryExec("
				delete from inicial_area where id = $area_id
			");
  }

  public function actionDocentesIndex()
  {
    $this->pageTitle = "Docentes";
    $this->render("docentesIndex");
  }

  public function actionDocentesJardinAjax()
  {
    $s
              = "SELECT u.id, u.nombre, a.itemname AS jardin
                          FROM user u
                            INNER JOIN authassignment a ON a.userid = u.id AND a.itemname = 'jardin'";
    $usuarios = Helpers::qryAll($s);
    echo json_encode($usuarios);
  }

  public function actionDocentesAjax()
  {
    $s
              = "SELECT u.id, u.nombre, u.login, a.itemname AS jardin
                          FROM user u
                            LEFT JOIN authassignment a ON a.userid = u.id AND a.itemname = 'jardin'";
    $usuarios = Helpers::qryAll($s);
    echo json_encode($usuarios);
  }

  public function actionDocenteCambiaAjax($docente_id, $val)
  {
    /* @var $auth CAuthManager */
    //$auth = Yii::app()->authManager;
    if ($val == "true") {
      $auth->assign('jardin', $docente_id);
    } else {
      $auth->revoke("jardin", $docente_id);
    }
  }

}
