<?php


class LiquidacionController extends Controller
{

  public $layout = '//layouts/column1';
  public $liquid_conf;
  public $liquid_conf_id = null;
  public $liquid_conf_detalle = null;
  public $articulo_beca = null;
  public $articulo_intereses = null;
  public $valor_cuota = null, $errors = null;
  public $conceptoLiquidacion; //hola
  public $esMatricula = null;

  public function actionLiquida($conel100 = false, $nivel_id = null)
  {
    $cnf = LiquidConf::model()->findAll("confirmada = 0");
    if (count($cnf) !== 1) {
      Helpers::error("Error en liquid conf, mas de una configuraciÃ³n sin confirmar");
      die;
    }
    $whereNivel = ($nivel_id and $nivel_id !== "") ? " and n.id=$nivel_id " : "";
    $where100 = (($conel100 === "true")) ? "" : " and coalesce(al.beca,0) < 100 ";

    $cicloActivo = Ciclo::getActivo()->id;

    $this->liquid_conf = $cnf[0];
    $this->liquid_conf_id = $this->liquid_conf->id;
    $this->liquid_conf_detalle = $this->liquid_conf->descripcion;
    $this->articulo_beca = Articulo::model()->findByPk(Opcion::getOpcionText("articulo-beca", null, "liquidacion"));
    $this->articulo_intereses = Opcion::getOpcionText("articulo_intereses", null, "liquidacion");
    $this->conceptoLiquidacion = Opcion::getOpcionText("concepto", null, "liquidacion");
    $this->esMatricula = $this->liquid_conf["es_matricula"];

    $comprob_factura = Comprob::FACTURA_VENTAS;

    $tr = Yii::app()->db->beginTransaction();
    if ($this->esMatricula) {
      $caseArt = "case when dmat.articulo_id <> 0 then dmat.articulo_id else amat.articulo_id end as articulo_id";
    } else {
      $caseArt = "case when d.articulo_id <> 0 then d.articulo_id else a.articulo_id end as articulo_id";
    }

    /*
         *   Asigno la siguiente division
         */
    /*
          if ($this->esMatricula) {
          $b = Yii::app()->db->createCommand("
          update alumno_division ad
          inner join alumno a on a.id = ad.Alumno_id
          set ad.siguiente_division_id = null
          where ad.Ciclo_id = $cicloActivo and ad.activo = 1 and a.activo = 1 and a.ingresante = 0
          ")->execute();
          $u1 = Yii::app()->db->createCommand("
          update alumno_division ad
          inner join alumno a on a.id = ad.Alumno_id
          set ad.siguiente_division_id = (select d.division_id_siguiente from division d where d.id=ad.division_id)
          where ad.Ciclo_id = $cicloActivo and ad.activo = 1 and a.activo = 1 and a.ingresante = 0
          ")->execute();
          $u2 = Yii::app()->db->createCommand("
          update alumno_division ad
          inner join alumno a on a.id = ad.Alumno_id
          set ad.siguiente_division_id = ad.division_id
          where ad.Ciclo_id = $cicloActivo and ad.activo = 1 and a.activo = 0 and a.ingresante = 1
          ")->execute();
          //            $tr->commit();
          //            vd($b,$u1,$u2);
          }
         */

    $sDelete = "
            delete doc.*, l.*
                from doc
                    inner join doc_liquid l on l.id = doc.doc_liquid_id
                    inner join socio s on s.id = doc.socio_id
                    inner join alumno a on s.alumno_id = a.id
                    inner join alumno_division ad on ad.alumno_id = a.id and ad.activo = 1
                    inner join division d on (d.id = ad.siguiente_division_id or d.id = ad.division_id)
                    inner join anio on anio.id = d.Anio_id
                    inner join nivel n on n.id = anio.nivel_id
                    inner join talonario t on t.id = doc.talonario_id
                where l.liquid_conf_id = $this->liquid_conf_id and t.comprob_id = $comprob_factura $whereNivel";

    //vd($sDelete);
    Yii::app()->db->createCommand($sDelete)->execute();
    $whereNivel = "";
    $sAlumnos = "
                      select s.id as socio_id, al.matricula, coalesce(al.beca, 0) as beca,
      				d.division_id_siguiente,
      				$caseArt,
      				saldo(s.id, null) as saldo_anterior,
      				concat(al.nombre, ', ', al.apellido) as alumno_nombre,
      				n.nombre as nivel, a.nombre as anio,d.nombre as division
      			from alumno_division ad
      				inner join division d on d.id = ad.Division_id and d.activo = 1
      				left join division dmat on dmat.id = d.division_id_siguiente
      				inner join anio a on a.id = d.Anio_id
      				left join anio amat on amat.id = dmat.Anio_id
      				inner join nivel n on n.id = a.Nivel_id
      				inner join alumno al on al.id = ad.Alumno_id
      				inner join alumno_estado ae on ae.id = al.estado_id and ae.activo_admin = 1
      				inner join socio s on s.Alumno_id = al.id
      			where ad.Ciclo_id = $cicloActivo and
      					ad.activo = 1 and al.activo = 1 $whereNivel $where100 and not al.ingresante and not ae.ingresante
      					/*and al.id = 43504*/ /*and d.id = 423*/
      			order by n.orden, a.nombre, d.nombre, al.apellido, al.nombre
            ";

    //vd($sAlumnos);
    $alumnos = Yii::app()->db->createCommand($sAlumnos)->queryAll();
    foreach ($alumnos as $alumno) {
      if (!$this->liquidaAlumno($alumno)) {
        $this->errors[] = "Alumno $alumno";
        var_dump($this->errors);
        die;

        return false;
      }
    }
    $this->liquid_conf->fecha_liquidacion = Helpers::fechaParaGrabar(date("d/m/Y", time()));
    $this->liquid_conf->save();


    // cambiar acá !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //
    //
    //
    $tr->commit();
    echo "ok";
  }

  function liquidaAlumno($alumno)
  {
    $socio_id = $alumno["socio_id"];
    $articuloCuota_id = $alumno["articulo_id"];
    if (!$articuloCuota_id) {
      return true;
    }
    $articulo = Yii::app()->db->createCommand("select nombre, precio_neto from articulo where id = $articuloCuota_id")->queryRow();
    $articuloCuotaImporte = $articulo["precio_neto"];
    $articuloCuotaNombre = $articulo["nombre"];
    $beca = $alumno["beca"];
    $saldoAnterior = $this->esMatricula ? 0 : $alumno["saldo_anterior"];
    $total = 0;

    $d = new Doc("insert", Comprob::FACTURA_VENTAS);
    if (count($d->errors) > 0) {
      $this->errors = $d->errors;

      return false;
    }

    $d->activo = 0;
    $d->Socio_id = $socio_id;
    $d->detalle = $this->liquid_conf_detalle;
    $d->concepto_id = $this->conceptoLiquidacion;
    $d->fecha_vto1 = Helpers::fechaParaGrabar($this->liquid_conf->fecha_venc_1);
    $d->fecha_vto2 = Helpers::fechaParaGrabar($this->liquid_conf->fecha_venc_2);
    if (!$d->save()) {
      $this->errors = $d->errors;

      return false;
    }

    // Cuota
    $det = new DocDet();
    $det->Doc_id = $d->id;
    $det->articulo_id = $articuloCuota_id;
    $det->importe = $articuloCuotaImporte;
    $det->total = $articuloCuotaImporte;
    $det->cantidad = 1;
    //$anio = date("Y",time()) + 1;
    //$det->detalle = (!$this->esMatricula ? "Cuota " : "Reserva de Vacante $anio ") . $articuloCuotaNombre;
    $det->detalle = $this->liquid_conf_detalle . " " . $articuloCuotaNombre;
    if (!$det->save()) {
      $this->errors = $d->errors;

      return false;
    }

    $total += $articuloCuotaImporte;
    if (!$this->esMatricula) {
      /*
             *  Novedades
             */
      $totalNovedades = 0;
      $novedades = Yii::app()->db->createCommand(
        "
                    select CONCAT(n.detalle/*,'(', count(n.articulo_id),')'*/) as detalle, SUM(n.importe) as importe, n.articulo_id, nc.fecha
                        from novedad n
                            inner join novedad_cab nc on nc.id = n.novedad_cab_id
                            inner join articulo a on a.id = n.articulo_id
                            inner join socio s on s.alumno_id = n.alumno_id
                        where s.id = $socio_id AND nc.liquid_conf_id = $this->liquid_conf_id
                        GROUP BY n.articulo_id"
      )->queryAll();
      foreach ($novedades as $n) {
        $totalNovedades += $n["importe"];
        $det = new DocDet();
        $det->Doc_id = $d->id;
        $det->articulo_id = $n["articulo_id"];
        $det->detalle = $n["detalle"]; //Helpers::qryScalar("select nombre from articulo where id = $det->articulo_id");
        $det->importe = round($n["importe"], 2);
        $det->total = round($n["importe"], 2);
        $det->cantidad = 1;
        if (!$det->save()) {
          $this->errors = $det->errors;
          return false;
        }
      }

      $total += $totalNovedades;

      /*
             *      Interes
             */

      $totalInteres = 0;
      if ($saldoAnterior >= ($articuloCuotaImporte + 50)) {
        $det = new DocDet();
        $det->Doc_id = $d->id;
        $det->articulo_id = $this->articulo_intereses;
        $det->detalle = Helpers::qryScalar("select nombre from articulo where id = $det->articulo_id");
        $det->importe = round(($saldoAnterior * 0.03), 2);
        $det->total = $det->importe;
        $totalInteres += $det->total;
        $det->cantidad = 1;
        if (!$det->save()) {
          $this->errors = $det->errors;

          return false;
        }
      }

      $total += $totalInteres;

      // Beca
      // calculo el importe de la beca para luego grabarla en el detalle
      if ($beca > 0 /* and ($saldoAnterior < $articuloCuotaImporte) */) {
        $det = new DocDet();
        $totalBeca = round(($articuloCuotaImporte * $beca) / 100, 2);
        $det->Doc_id = $d->id;
        $det->articulo_id = $this->articulo_beca->id;
        $det->detalle = Helpers::qryScalar("select nombre from articulo where id = $det->articulo_id") . " $beca%";
        $det->importe = $totalBeca * -1;
        $det->total = $totalBeca * -1;
        $det->cantidad = 1;
        if (!$det->save()) {
          $this->errors = $det->errors;

          return false;
        }
        //vd($totalBeca);
      } else {
        $totalBeca = 0;
      }
      if (!$det->save()) {
        $this->errors = $det->errors;

        return false;
      }
      $total -= $totalBeca;
    }

    // asigno total y saldo al documento
    $d->total = round($total, 2);
    $d->saldo = round($total, 2);
    //preguntar a claudia como es la condiciÃ³n
    //if (($d->total +$saldoAnterior)> 0) {
    if ($d->total > 0 or true) {
      $dl = new DocLiquid();
      $dl->liquid_conf_id = $this->liquid_conf_id;
      $dl->interes = $this->esMatricula ? 0 : round($totalInteres, 2);
      $dl->cuota = $articuloCuotaImporte;
      $dl->beca = $this->esMatricula ? 0 : $totalBeca;
      $dl->saldo_anterior = $this->esMatricula ? 0 : ($saldoAnterior ? $saldoAnterior : 0);
      $dl->novedades = $this->esMatricula ? 0 : round($totalNovedades, 2);
      if (!$dl->save()) {
        vd("Error grabando en doc_liquid", $dl->errors);
      }

      $d->doc_liquid_id = $dl->id;

      if (!$d->save()) {
        $this->errors = $d->errors;

        return false;
      }
    } else {
      //echo $alumno["alumno_nombre"] . "<br/>";
      $d->delete();
    }

    return true;
  }

  /**
   * @param null $liquid_conf_id
   * @param bool $impresion
   * @throws CException
   * @throws Exception
   */
  public function actionResumen($liquid_conf_id = null, $impresion = false)
  {
    $whereSaldoPositivo = "";
    //			$whereSaldoPositivo = "and dl.saldo_anterior + d.total > 0";
    $whereSaldoNegativo = "and dl.saldo_anterior < 0";
    if ($liquid_conf_id) {
      $lc = LiquidConf::model()->findByPk($liquid_conf_id);
    } else {
      $lc = $liquid_conf_id = LiquidConf::getActive();
      $liquid_conf_id = $lc->id;
    }
    /*
         *          Cuotas
         */
    //$sb = "
    //   select max(id) from liquid_confddd
    //";
    //vd($selectCuotas);
    //$b = Yii::app()->db->createCommand($sb)->queryAll();
    //vd($b);
    $comprob_factura = Comprob::FACTURA_VENTAS;

    $selectCuotas = "
  				select ni.orden,
  					concat(ni.nombre, ' ', group_concat(distinct an.abrev order by an.orden,'/'), ' (', group_concat(distinct art.precio_neto),')')  as detalle,
  					sum(dl.cuota) as total, count(*) as cant
  				from doc d
  					inner join doc_liquid dl on dl.id = d.doc_liquid_id
  					inner join socio s on s.id = d.Socio_id
  					inner join alumno al on al.id = s.Alumno_id
  					inner join alumno_estado ae on ae.id = al.estado_id and ae.activo_admin = 1
  					inner join alumno_division ad on ad.Alumno_id = al.id and ad.activo = 1
  				inner join division di on di.id = ad.Division_id
                  /*inner join division di on di.id = ad.Division_id*/
                  inner join anio an on an.id = di.Anio_id
                  inner join nivel ni on ni.id = an.Nivel_id
                  inner join talonario t on t.id = d.talonario_id
                 inner join articulo art on art.id = an.articulo_id
          where dl.liquid_conf_id = $liquid_conf_id and t.comprob_id = $comprob_factura $whereSaldoPositivo
          group by art.precio_neto, ni.nombre
          ";
    //vd($selectCuotas);
    $cuotas = Yii::app()->db->createCommand($selectCuotas)->queryAll();

    //vd($s);

    /*
         *              Saldos
         */
    $s = "
            select ni.nombre as detalle, sum(dl.saldo_anterior) as total, count(*) as cant
              from doc d
          			inner join doc_liquid dl on dl.id = d.doc_liquid_id
          			inner join socio s on s.id = d.Socio_id
          			inner join alumno al on al.id = s.Alumno_id
          			inner join alumno_estado ae on ae.id = al.estado_id and ae.activo_admin = 1
          			inner join alumno_division ad on ad.Alumno_id = al.id and ad.activo = 1
          			inner join division di on di.id = ad.Division_id
          			inner join anio an on an.id = di.Anio_id
                INNER JOIN division d1 ON di.division_id_siguiente = d1.id
                INNER JOIN anio a ON d1.Anio_id = a.id
          			inner join nivel ni on ni.id = an.Nivel_id
          			inner join talonario t on t.id = d.talonario_id
              where dl.liquid_conf_id = $liquid_conf_id and t.comprob_id = $comprob_factura $whereSaldoPositivo
              group by ni.nombre
              order by ni.orden
          ";
    //vd($s);
    $saldos = Yii::app()->db->createCommand($s)->queryAll();

    /*
         *              Saldos Negativos
         */
    $sn = "
            select ni.nombre as detalle, sum(dl.saldo_anterior) as total, count(*) as cant
              from doc d
          			inner join doc_liquid dl on dl.id = d.doc_liquid_id
          			inner join socio s on s.id = d.Socio_id
          			inner join alumno al on al.id = s.Alumno_id
          			inner join alumno_estado ae on ae.id = al.estado_id and ae.activo_admin = 1
          			inner join alumno_division ad on ad.Alumno_id = al.id and ad.activo = 1
          			inner join division di on di.id = ad.Division_id
          			inner join anio an on an.id = di.Anio_id
          			inner join nivel ni on ni.id = an.Nivel_id
          			inner join talonario t on t.id = d.talonario_id
                        where dl.liquid_conf_id = $liquid_conf_id and t.comprob_id = $comprob_factura $whereSaldoNegativo
              group by ni.nombre
              order by ni.orden
          ";
    //vd($s);
    $saldosNegativos = Yii::app()->db->createCommand($sn)->queryAll();

    /*
         *              Becas
         */
    $becas = Yii::app()->db->createCommand("
      			select concat(\"Beca \",al.beca,\"%\") as detalle , sum(dl.beca) as total, count(dl.beca) as cant
      				from doc d
      				inner join doc_liquid dl on dl.id = d.doc_liquid_id
      				inner join socio s on s.id = d.Socio_id
      				inner join alumno al on al.id = s.Alumno_id
      				inner join alumno_estado ae on ae.id = al.estado_id and ae.activo_admin = 1
      			where dl.liquid_conf_id = $liquid_conf_id and al.beca $whereSaldoPositivo
      			group by al.beca
      			order by al.beca
      		")->queryAll();
    $becasDetalle = Yii::app()->db->createCommand("
      			select concat(a.apellido,', ', a.nombre) as alumno , a.beca, dl.beca as importeBeca
      				from doc_liquid dl
      					inner join doc d on d.doc_liquid_id = dl.id
      					inner join socio s on d.Socio_id = s.id
      					inner join alumno a on a.id = s.Alumno_id
      					inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_admin = 1
      				where dl.beca <> 0 $whereSaldoPositivo
      				order by a.beca, a.apellido, a.nombre
      		")->queryAll();

    /*
         *          Novedades
         */
    $articulo_tipo_novedad = Opcion::getOpcionText("articulo_tipo_novedad", null, "liquidacion");

    $novedades = Yii::app()->db->createCommand("
    				select a.nombre as detalle, sum(det.total) as total, count(*) as cant
    						from doc d
    								inner join doc_liquid dl on dl.id = d.doc_liquid_id
    								inner join doc_det det on det.Doc_id = d.id
    								inner join articulo a on a.id = det.articulo_id
    								inner join socio s on s.id = d.Socio_id
    								inner join alumno al on al.id = s.Alumno_id
    								inner join alumno_estado ae on ae.id = al.estado_id and ae.activo_admin = 1
    								inner join alumno_division ad on ad.Alumno_id = al.id and ad.activo = 1
    								inner join division di on di.id = ad.Division_id
    								inner join anio an on an.id = di.Anio_id
    								inner join nivel n on n.id = an.Nivel_id
    						 where dl.liquid_conf_id = $liquid_conf_id  and al.activo = 1 and
    								 a.articulo_tipo_id = $articulo_tipo_novedad $whereSaldoPositivo
    						group by a.nombre
        	")->queryAll();
    /*
         *          Intereses
         */
    $articulo_intereses = Opcion::getOpcionText("articulo_intereses", null, "liquidacion");
    $intereses = Yii::app()->db->createCommand("
          select sum(dd.importe)
              from doc d
                    inner join doc_liquid dl on dl.id = d.doc_liquid_id
                    inner join doc_det dd on dd.Doc_id = d.id
              where dd.articulo_id = $articulo_intereses and dl.liquid_conf_id = $liquid_conf_id $whereSaldoPositivo
        ")->queryScalar();

    $novedadesPorNivel = array();
    foreach (Helpers::qryAll("select id,nombre from nivel n where n.activo = 1 and n.egresado = 0 order by orden") as $nivel) {
      $articulo_tipo_novedad = Opcion::getOpcionText("articulo_tipo_novedad", null, "liquidacion");
      $nivel_id = $nivel["id"];
      $novedadesPorNivel[$nivel["nombre"]] = Helpers::qryAll("
              select n.id, n.nombre as nivel, a.nombre as detalle, sum(det.total) as total, count(*) as cant
                  from doc d
                      inner join doc_liquid dl on dl.id = d.doc_liquid_id
                      inner join doc_det det on det.Doc_id = d.id
                      inner join articulo a on a.id = det.articulo_id
                      inner join socio s on s.id = d.Socio_id
                      inner join alumno al on al.id = s.Alumno_id
					  inner join alumno_estado ae on ae.id = al.estado_id and ae.activo_admin = 1
                      inner join alumno_division ad on ad.Alumno_id = al.id and ad.activo = 1
                      inner join division divi on divi.id = ad.Division_id
                      inner join anio an on an.id = divi.Anio_id
                      inner join nivel n on n.id = an.Nivel_id
                   where dl.liquid_conf_id = $liquid_conf_id and an.nivel_id = $nivel_id and a.articulo_tipo_id = $articulo_tipo_novedad
                               $whereSaldoPositivo
                  group by n.nombre, a.nombre
              ");
    }
    $this->renderPartial("resumen", array(
      "cuotas" => $cuotas,
      "becas" => $becas,
      "becasDetalle" => $becasDetalle,
      "intereses" => $intereses,
      "novedades" => $novedades,
      "saldos" => $saldos,
      "saldosNegativos" => $saldosNegativos,
      "novedadesPorNivel" => $novedadesPorNivel,
      "impresion" => $impresion,
      "descripcion" => $lc->descripcion
    ));
  }

  public function actionEmisionComprobantes($liquid_conf_id = null, $nivel_id = "", $alumno_id = null)
  {
    $comprobFactura = Comprob::FACTURA_VENTAS;

    $where = $nivel_id ? " where n.id = $nivel_id " : " ";
    $where = $where . ($alumno_id ? " and a.id = $alumno_id " : "");
    if ($liquid_conf_id) {
      $lc = LiquidConf::model()->findByPk($liquid_conf_id);
    } else {
      $lc = $liquid_conf_id = LiquidConf::getActive();
      $liquid_conf_id = $lc->id;
    }
    $alumnosSelect = "
    			select n.nombre as nivel, anio.nombre as anio, d.nombre as division, lc.fecha_liquidacion,
    				concat(a.apellido, ', ', a.nombre) as alumno, a.matricula,
    				s.id as socio_id, p.calle, p.numero, p.piso, p.departamento,
    				a.beca, a.visa_activo as visa,
    				deb.activo as debito_activo, lc.descripcion
    			from alumno_division ad
    				inner join division d on d.id = ad.division_id and d.activo = 1
    				inner join anio on anio.id = d.anio_id and anio.activo = 1
    				inner join nivel n on n.id = anio.Nivel_id
    				inner join alumno a on a.id = ad.Alumno_id  and a.activo = 1 /*and a.id = 43504*/
    				inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_admin = 1
    				left  join alumno_debito deb on deb.alumno_id = a.id
    				inner join socio s on s.alumno_id = a.id
    				inner join doc doc on doc.socio_id = s.id
    				inner join doc_liquid dl on dl.id = doc.doc_liquid_id and dl.liquid_conf_id = $liquid_conf_id
    				inner join liquid_conf lc on lc.id = dl.liquid_conf_id
    				left join pariente p on p.id = a.vive_con_id
    				inner join talonario t on t.id = doc.talonario_id and t.comprob_id = $comprobFactura
    			$where  and ad.activo = 1 /*and a.matricula = 5125*/
    			order by n.orden, anio.nombre, d.nombre, a.sexo, a.apellido, a.nombre";
    //vd($alumnosSelect);
    $alumnos = Helpers::qryAll($alumnosSelect);
    //vd($alumnos);
    foreach ($alumnos as $key => $a) {
      $alumnos[$key]["domicilio"] = $a["calle"] . " " . $a["numero"] . " " . $a["piso"] . " " . $a["departamento"];
      $socio_id = $a["socio_id"];
      $alumnos[$key]["doc"] = Helpers::qry("
            select d.id, d.fecha_creacion, d.total, d.detalle,
                        d.fecha_vto1, d.fecha_vto2, dl.saldo_anterior
            from doc d
                inner join talonario t on t.id = d.talonario_id and t.comprob_id = $comprobFactura
                inner join doc_liquid dl on dl.id = d.doc_liquid_id and dl.liquid_conf_id = $liquid_conf_id
            where d.socio_id = $socio_id");
      $doc = $alumnos[$key]["doc"];
      $doc_id = $doc["id"];
      if (!$doc_id) {
        vd("error!!!, no tiene ninguna factura!", $doc);
      }
      $alumnos[$key]["doc"]["det"] = Helpers::qryAll("
              select det.detalle as nombre, total, afectacion
              from doc_det det
                  left join articulo a on a.id = det.articulo_id
              where det.Doc_id = $doc_id
              order by orden");
    }

    //vd($alumnos);
    $this->renderPartial("emisionComprobantes", array("alumnos" => $alumnos));
  }

  public function actionEmisionComprobantesIngresantes($cant = 120, $primero = 101)
  {
    $comprobFactura = Comprob::FACTURA_VENTAS;

    $where = " ";
    $where = $where . ("");
    $lc = $liquid_conf_id = LiquidConf::getActive();
    $liquid_conf_id = $lc->id;
    $alumnosSelect = "
        			select n.nombre as nivel, anio.nombre as anio, d.nombre as division, lc.fecha_liquidacion,
        				concat(a.apellido, ', ', a.nombre) as alumno, a.matricula,
        				s.id as socio_id, p.calle, p.numero, p.piso, p.departamento,
        				a.beca, a.visa_activo as visa,
        				deb.activo as debito_activo, lc.descripcion
        			from alumno_division ad
        				inner join division d on d.id = ad.division_id and d.activo = 1
        				inner join anio on anio.id = d.anio_id and anio.activo = 1
        				inner join nivel n on n.id = anio.Nivel_id
        				inner join alumno a on a.id = ad.Alumno_id  and a.activo = 1 /*and a.id = 43504*/
        				inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_admin = 1
        				left  join alumno_debito deb on deb.alumno_id = a.id
        				inner join socio s on s.alumno_id = a.id
        				left join doc doc on doc.socio_id = s.id
        				left join doc_liquid dl on dl.id = doc.doc_liquid_id and dl.liquid_conf_id = $liquid_conf_id
        				left join liquid_conf lc on lc.id = dl.liquid_conf_id
        				left join pariente p on p.id = a.vive_con_id
        				left join talonario t on t.id = doc.talonario_id and t.comprob_id = $comprobFactura
        			where ad.activo = 1 /*and a.matricula = 5125*/
        			order by n.orden, anio.nombre, d.nombre, a.sexo, a.apellido, a.nombre
              limit $cant";
    //vd($alumnosSelect);
    $alumnos = Helpers::qryAll($alumnosSelect);
    //vd($alumnos);
    foreach ($alumnos as $key => $a) {
      $alumnos[$key]["domicilio"] = $a["calle"] . " " . $a["numero"] . " " . $a["piso"] . " " . $a["departamento"];
      $socio_id = $a["socio_id"];
      $alumnos[$key]["doc"] = Helpers::qry("
                select d.id, d.fecha_creacion, /*d.total*/ 4700 as total, d.detalle,
                            d.fecha_vto1, d.fecha_vto2, /*dl.saldo_anterior*/ 4700 as saldo_anterior
                from doc d
                    inner join talonario t on t.id = d.talonario_id and t.comprob_id = $comprobFactura
                    inner join doc_liquid dl on dl.id = d.doc_liquid_id and dl.liquid_conf_id = $liquid_conf_id
                where d.socio_id = $socio_id");
      $doc = $alumnos[$key]["doc"];
      $doc_id = $doc["id"];
      if (!$doc_id) {
        //                    vd("error!!!, no tiene ninguna factura!", $doc);
      }
      //     $alumnos[$key]["doc"]["det"] = Helpers::qryAll("
      //         select det.detalle as nombre, total, afectacion
      //             from doc_det det
      //                 left join articulo a on a.id = det.articulo_id
      //             where det.Doc_id = $doc_id
      //             order by orden");
    }

    //vd($alumnos);
    $this->renderPartial("emisionComprobantesIngresantes", array("alumnos" => $alumnos, "primero" => $primero));
  }

  public function actionLiquidacion()
  {
    $cnf = LiquidConf::model()->findAll("fecha_confirmado is null");
    $nroCuota = Yii::app()->db->createCommand("select nro_cuota from liquid_conf lc order by id desc limit 1 ")->queryScalar() + 1;
    $niveles = Nivel::model()->findAll(array(
      "order" => "orden",
      "condition" => "activo = 1"
    ));
    if (count($cnf) == 0) {
      $lc = new LiquidConf();
      $nroCuota = $nroCuota > 11 ? 1 : $nroCuota;
      $lc->nro_cuota = $nroCuota;
      $lc->descripcion = (!$this->esMatricula ? "Cuota " : "Reserva vacante ") . $nroCuota;
      $lc->es_matricula = 0;
      $lc->id = -1;
      $lc->conel100 = 0;
    } else {
      $lc = LiquidConf::model()->findByPk($cnf[0]->id);
    }
    $this->render("liquidacion", array("lc" => $lc, "niveles" => $niveles));
  }

  public function actionGraba()
  {
    $liquid_conf_id = $_POST["id"];
    $lc = LiquidConf::model()->findByPk($liquid_conf_id);
    if (!$lc) {
      $lc = new LiquidConf();
    }
    $lc->attributes = $_POST;
    $lc->fecha_venc_1 = Helpers::fechaParaGrabar($_POST["fecha_venc_1"]);
    $lc->fecha_venc_2 = Helpers::fechaParaGrabar($_POST["fecha_venc_2"]);
    $lc->es_matricula = isset($_POST["matricula"]) ? 1 : 0;
    $lc->conel100 = isset($_POST["conel100"]) ? 1 : 0;
    if (!$lc->save()) {
      echo json_encode($lc->errors);
      die;
    } else {
      echo json_encode("");
    }
  }

  public function actionVerLiq()
  {
    $this->render("verLiq");
  }

  public function actionCierraLiq()
  {
    $cnf = LiquidConf::model()->findAll("fecha_confirmado is null");
    if (count($cnf) !== 1) {
      Helpers::error("Error en liquid conf");
      die;
    }
    $fecha = date("Y/m/d", time());
    $tr = Yii::app()->db->beginTransaction();
    $lc = LiquidConf::model()->findByPk($cnf[0]->id);
    $lc->fecha_confirmado = $fecha;
    $lc->confirmada = 1;
    $comprob_factura = Comprob::FACTURA_VENTAS;
    $recsCant = Helpers::qryExec("
            update doc d
                inner join doc_liquid dl on dl.id = d.doc_liquid_id
                inner join talonario t on t.id = d.talonario_id
            set d.activo = 1
            where dl.liquid_conf_id = $lc->id and t.comprob_id = $comprob_factura");
    if (!$lc->save()) {
      $tr->rollback();
      var_dump($lc->errors);
    } else {
      $tr->commit();
      $ret = new stdClass;
      $ret->status = "ok";
      $ret->registros = $recsCant;
      echo json_encode($ret);
    }
  }

  public function actionRecargos()
  {
    $this->render("recargos", array("alumnos" => alumnosParaRecargos()));
  }

  public function actionRecargosGraba()
  {
    //        echo Opcion::getOpcionText("leyenda recargo", "Recargo 2do. Vto.", "liquidacion");
    //        die;
    $ultimaLC = Helpers::qryScalar("select max(id) as id from liquid_conf");
    if (!$ultimaLC) {
      echo "Error buscando la última liquidación";
      die;
    }
    $lc = Helpers::qry("select recargos from liquid_conf where id = $ultimaLC");
    if ($lc["recargos"] == 1) {
      $conceptoRecargo_id = Opcion::getOpcionText("concepto recargo", null, "liquidacion");

      echo "Los recargos ya fueron realizados<br/>";
      echo "delete from doc where concepto_id = $conceptoRecargo_id and liquid_conf_id = $ultimaLC";
      die;
    }

    $alumnos = alumnosParaRecargos();

    $tr = Yii::app()->db->beginTransaction();
    $importeRecargo = Opcion::getOpcionText("recargo", null, "liquidacion");
    $conceptoRecargo = Opcion::getOpcionText("concepto recargo", null, "liquidacion");
    $leyendaRecargo = Opcion::getOpcionText("leyenda recargo", "Recargo 2do. Vto.", "liquidacion");
    foreach ($alumnos as $a) {
      $dl = new DocLiquid();
      $dl->liquid_conf_id = $ultimaLC;
      if (!$dl->save()) {
        vd($dl->errors);
      }
      $doc = new Doc("insert", Comprob::ND_VENTAS);
      $doc->total = $importeRecargo;
      $doc->saldo = $importeRecargo;
      $doc->Socio_id = $a["socio_id"];
      $doc->concepto_id = $conceptoRecargo;
      $doc->detalle = $leyendaRecargo;
      $doc->user_id = Yii::app()->user->id;
      $doc->doc_liquid_id = $dl->id;
      if (!$doc->save()) {
        vd($doc->errors);
      }
    }
    $liquid = LiquidConf::model()->findByPk($ultimaLC);
    $liquid->recargos = 1;
    $liquid->save();
    $tr->commit();
    echo "ok";
  }

  public function actionRendicionDebitosFrances($preview = true, $banco = null)
  {
    $this->renderPartial("rendicionDebitosFrances", array("preview" => $preview, 'banco' => $banco));
  }

  public function actionRendicionDebitosVisa($preview = true, $banco = null)
  {
    $this->renderPartial("rendicionDebitosVisa", array("preview" => $preview, 'banco' => $banco));
  }

  public function actionRendicionBanelco()
  {
    $this->actionRendicionBanelcoVisa("Banelco");
  }

  public function actionRendicionVisa()
  {
    $this->actionRendicionBanelcoVisa("Visa");
  }

  public function actionRendicionDebitoVisa()
  {
    $this->actionRendicionDebitoVisa("DebitoVisa");
  }

  public function actionRendicionBanelcoVisa($banco, $preview = true)
  {
    //   vd($preview);
    //        $rows = Alumno::getLiquidacionData();
    //        $div = Helpers::getTable("main", $rows, $cols);

    $this->renderPartial("rendicionBanelcoVisa", array(
      "banco" => $banco, "preview" => $preview
    ));
  }

  public function actionRendicionLink($accion = "")
  {
    if ($accion == "cabecera") {
      $row = Alumno::getLiquidacionDataTotales();
      $this->renderPartial("rendicionLink", array(
        "row" => $row, "accion" => $accion
      ));
    } elseif ($accion == "detalle") {
      $rows = Alumno::getLiquidacionData();
      $this->renderPartial("rendicionLink", array(
        "rows" => $rows, "accion" => $accion
      ));
    } else {
      $this->render("rendicionLink", array("accion" => $accion));
    }
  }

  public function actionDebitos($soloConDebito = true)
  {
    $lc = $liquid_conf_id = LiquidConf::getActive();
    $liquid_conf_id = $lc->id;
    if (!$liquid_conf_id or $liquid_conf_id == 0) {
      Helpers::error("Error en Debito", "Error recuperando el id de la última liquidación confirmada");
      die;
    }
    $whereDebito = $soloConDebito ? " and  deb.cbu is not null " : "";
    $comprob_factura = Comprob::FACTURA_VENTAS;
    $alumnosSelect = "
            select n.nombre as nivel, anio.nombre as anio, d.nombre as division,
									concat(a.apellido, ', ', a.nombre) as alumno, a.matricula,
									s.id as socio_id, p.calle, p.numero, p.piso, p.departamento,
									saldo(s.id,null) as total, deb.activo, deb.cbu,
									lc.fecha_venc_1, lc.fecha_venc_2
            from nivel n
                   inner join anio on anio.Nivel_id = n.id and anio.activo = 1
                   inner join division d on d.Anio_id = anio.id and d.activo = 1
                   inner join alumno_division ad on ad.Division_id = d.id and ad.activo = 1
                   inner join alumno a on a.id = ad.Alumno_id and a.activo = 1 /*and a.matricula = 4391*/
                    inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_admin = 1
                   left  join alumno_debito deb on deb.alumno_id = a.id
                   inner join socio s on s.alumno_id = a.id
                   inner join doc doc on doc.socio_id = s.id
                   inner join doc_liquid dl on dl.id = doc.doc_liquid_id and dl.liquid_conf_id = $liquid_conf_id
                   inner join liquid_conf lc on lc.id = dl.liquid_conf_id
                   left join pariente p on p.id = a.vive_con_id
                    inner join talonario t on t.id = doc.talonario_id and t.comprob_id = $comprob_factura
            where  (doc.total + dl.saldo_anterior) > 0 $whereDebito
            order by a.apellido, a.nombre";
    //        vd($alumnosSelect);
    $dataDebito = Helpers::qryAll($alumnosSelect);
    $this->render("debitos", array("data" => $dataDebito));
  }

  public function actionDebitosArchivo($soloConDebito = true, $banco = null)
  {
    $liquid_conf_id = Helpers::qryScalar("select max(id) from liquid_conf where confirmada = 1");
    if (!$liquid_conf_id or $liquid_conf_id == 0) {
      Helpers::error("Error en Debito", "Error recuperando el id de la última liquidación confirmada");
      die;
    }
    $comprob_factura = Comprob::FACTURA_VENTAS;
    $alumnosSelect = "
                    select doc.id as doc_id, a.matricula, REPLACE(deb.cbu,'-','') as cbu, /*doc.total + dl.saldo_anterior as total, */ saldo(s.id,null) as total,
                                lc.fecha_venc_1, lc.fecha_venc_2,  lc.descripcion
                    from alumno a
                            inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_admin = 1
                            inner join alumno_division ad on ad.alumno_id = a.id and ad.activo = 1
                            inner join division d on d.id = ad.division_id and d.activo = 1
                            inner join anio on anio.id = d.anio_id and anio.activo = 1
                            left  join alumno_debito deb on deb.alumno_id = a.id
                            inner join socio s on s.alumno_id = a.id
                            inner join doc doc on doc.socio_id = s.id
                            inner join doc_liquid dl on dl.id = doc.doc_liquid_id and dl.liquid_conf_id = $liquid_conf_id
                            inner join liquid_conf lc on lc.id = dl.liquid_conf_id
                            left join pariente p on p.id = a.vive_con_id
                            inner join talonario t on t.id = doc.talonario_id and t.comprob_id = $comprob_factura
                    where  (doc.total + dl.saldo_anterior) > 0
                                 and deb.activo = 1 and deb.cbu is not null
                                 and a.activo = 1 /*and a.matricula = 3496*/";
    $dataDebito = Helpers::qryAll($alumnosSelect);
    $this->renderPartial("debitosArchivo", array("data" => $dataDebito, 'banco' => $banco));
  }

  public function actionTestSaldo()
  { }

  public function actionControl()
  {
    $this->render("control");
  }

  public function actionControlAjax($liquid_conf_id, $liquid_conf_ant_id)
  {
    $lcs = array($liquid_conf_id, $liquid_conf_ant_id);
    $articulo_tipo_novedad = Opcion::getOpcionText("articulo_tipo_novedad", null, "liquidacion");
    foreach ($lcs as $lc_id) {
      $selectGral = "
          			select lc.descripcion,n.nombre as nivel, an.nombre as anio, di.nombre,
          				sum(dl.cuota) as cuota, sum(dl.beca) as beca, sum(dl.saldo_anterior) as saldo,
          				count(*) as cant
          			from doc d
          				inner join socio s on s.id = d.Socio_id
          				inner join alumno a on a.id = s.Alumno_id
          				inner join doc_liquid dl on dl.id = d.doc_liquid_id and dl.liquid_conf_id = $lc_id
          				inner join alumno_division ad on ad.Alumno_id = a.id and ad.activo = 1
          				inner join division di on di.id = ad.Division_id
          				inner join anio an on an.id = di.Anio_id /*and an.nombre = 4*/
          				inner join nivel n on n.id = an.Nivel_id
          				inner join alumno_estado ae on ae.id = a.estado_id and ae.egresado  = 0
          				inner join liquid_conf lc on lc.id=dl.liquid_conf_id
          			where  a.egresado = 0 and cuota is not null
          			group by lc.id, n.orden";
      $general[] = Helpers::qryAllObj($selectGral);
    }
    foreach ($lcs as $lc_id) {
      $selectNovedades = "
        			select n.nombre as nivel, a.nombre as detalle, sum(det.total) as total, count(*) as cant
        			from doc d
        				inner join doc_liquid dl on dl.id = d.doc_liquid_id
        				inner join doc_det det on det.Doc_id = d.id
        				inner join articulo a on a.id = det.articulo_id
        				inner join socio s on s.id = d.Socio_id
        				inner join alumno al on al.id = s.Alumno_id
        				inner join alumno_estado ae on ae.id = al.estado_id and ae.activo_admin = 1
        				inner join alumno_division ad on ad.Alumno_id = al.id and ad.activo = 1
        				inner join division divi on divi.id = ad.Division_id
        				inner join anio an on an.id = divi.Anio_id
        				inner join nivel n on n.id = an.Nivel_id
        			 where dl.liquid_conf_id = $lc_id and a.articulo_tipo_id = $articulo_tipo_novedad
        			group by a.nombre
        			order by 1,2";
      $novedades[] = Helpers::qryAllObj($selectNovedades);
    }
    $this->renderPartial("controlAjax", array(
      "general" => $general, "novedades" => $novedades
    ));
  }
}

function alumnosParaRecargos()
{
  return Helpers::qryAll("
            select s.id as socio_id, a.beca, concat(a.apellido, ', ', a.nombre) as nombre,
                    saldo(s.id,Null) as saldo, ar.precio_neto as cuota
               from alumno a
                    inner join socio s on s.Alumno_id = a.id
                    left join alumno_division ad on ad.Alumno_id = a.id and ad.activo = 1
                    left join division d on d.id = ad.Division_id
                    left join anio on anio.id = d.Anio_id
                    left  join articulo ar on ar.id = anio.articulo_id
               where  a.activo = 1 and saldo(s.id, null) >1000 /*ar.precio_neto*/
               order by a.apellido, a.nombre");
}
