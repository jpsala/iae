<?php

class LogicaPeriodoController extends Controller {

  public function actionIndex() {
    // renders the view file 'protected/views/site/index.php'
    // using the default layout 'protected/views/layouts/main.php'
    //$this->render('index');
    var_dump(Yii::app()->user);
  }

  public function actionOptionsConductaDesdeAlumno($alumno_id) {
    if (!$alumno = Alumno::model()->findByPk("$alumno_id")) {
      return;
    }
    echo $alumno->nivel_id;
    die;
    //echo actionOptionsConductaDesdeNivel(Alumno::model()->findByPk("$alumno_id")->nivel_id);
  }

  public function actionOptionsConductaDesdeNivel($nivel_id) {
    return $this->actionOptionsDesdeNivel($nivel_id, true);
  }
  
    public function actionOptionsInasistenciaDesdeNivel($nivel_id) {
    return $this->actionOptionsDesdeNivel($nivel_id, true);
  }

  public function actionOptionsDesdeNivel($nivel_id, $conConducta = false) {

    if (!$nivel_id or $nivel_id == "null") {
      return;
    }
    $whereConducta = $conConducta ? " and tiene_conducta = 1" : "";
    $ciclo_id = Ciclo::getCicloParaCargaDeNotas()->id;
    $qry = "
      select lc.Logica_id
        from nivel n
          inner join asignatura_tipo at  on at.Nivel_id = n.id and at.tipo = 1
          inner join logica_ciclo lc on lc.Asignatura_Tipo_id = at.id and lc.ciclo_id = $ciclo_id
            where n.id = $nivel_id 
      ";
    $logica_id = Yii::app()->db->createCommand($qry)->queryScalar();
    //$log_periodo_act = LogicaPeriodo::getActiva($logica_id);
    $x = array('prompt' => '');
    $lis = LogicaPeriodo::model()->findAllBySql(
            "select lp.id, lp.nombre from logica_periodo lp
                  where lp.fecha_inicio_ci is not null and lp.logica_id = $logica_id and
                        (select count(\"*\") from logica_item li where
                            li.logica_periodo_id = lp.id $whereConducta)
                  order by lp.orden
            ");
    echo CHtml::listOptions(
            null//$log_periodo_act ? $log_periodo_act->id : null
            , CHtml::listData($lis, 'id', 'nombre'
            )
            , $x
    );
  }

  public function actionOptions($division_asignatura_id, $seleccionaActiva = true) {
    $log_periodo_act = null;
    $ciclo_id = Ciclo::getCicloParaCargaDeNotas()->id;
    $x = array('prompt' => '');
    $qry = "
      select lc.Logica_id
          from asignatura a
              inner join division_asignatura da on da.Asignatura_id = a.id
              inner join asignatura_tipo t on t.id = a.Asignatura_Tipo_id
              inner join logica_ciclo lc on lc.Asignatura_Tipo_id = t.id and lc.Ciclo_id = $ciclo_id
          where da.id = $division_asignatura_id
      ";
    if ($seleccionaActiva) {
      $logica_id = Yii::app()->db->createCommand($qry)->queryScalar();
      $qry = "
      select lc.Logica_id
          from asignatura a
              inner join division_asignatura da on da.Asignatura_id = a.id
              inner join asignatura_tipo t on t.id = a.Asignatura_Tipo_id
              inner join logica_ciclo lc on lc.Asignatura_Tipo_id = t.id and lc.Ciclo_id = $ciclo_id
          where da.id = $division_asignatura_id
      ";
			//vd($qry);
      if (!($log_periodo_act = LogicaPeriodo::getActiva($logica_id))) {
//        throw new Exception("No hay ningún período activo para esta fecha");
      }
    }
    echo CHtml::listOptions(
            $log_periodo_act ? $log_periodo_act->id : null
            , CHtml::listData(LogicaPeriodo::model()->findAll(
                            array(
                                "condition" => "fecha_inicio_ci is not null and logica_id = $logica_id",
                                "order" => "orden")
                    ), 'id', 'nombre'
            )
            , $x
    );
  }

  public function actionOptionsDesdeAlumno($socio_id) {
    $nivel_id = 
    $ciclo_id = Ciclo::getCicloParaCargaDeNotas()->id;
    $qry = "
      select lc.Logica_id
        from nivel n
          inner join asignatura_tipo at  on at.Nivel_id = n.id and at.tipo = 1
          inner join logica_ciclo lc on lc.Asignatura_Tipo_id = at.id and lc.ciclo_id = $ciclo_id
            where n.id = $nivel_id 
      ";
    $logica_id = Yii::app()->db->createCommand($qry)->queryScalar();
    //$log_periodo_act = LogicaPeriodo::getActiva($logica_id);
    $x = array('prompt' => '');
    $lis = LogicaPeriodo::model()->findAllBySql(
            "select lp.id, lp.nombre from logica_periodo lp
                  where lp.fecha_inicio_ci is not null and lp.logica_id = $logica_id and
                        (select count(\"*\") from logica_item li where
                            li.logica_periodo_id = lp.id $whereConducta)
                  order by lp.orden
            ");
    echo CHtml::listOptions(
            null//$log_periodo_act ? $log_periodo_act->id : null
            , CHtml::listData($lis, 'id', 'nombre'
            )
            , $x
    );
  }
  
}