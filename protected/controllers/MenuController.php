<?php

	class MenuController extends Controller {

		public $layout = "bootstrap";

		public function actionIndex() {
			$tree = $this->getItems(Null, array());
			$menuItems = Helpers::qryAll("
				SELECT mi.id, case when (mi.detalle=\"\") then mi.texto else mi.detalle end as texto, mi.activo
					FROM menuitem mi
			");
			Yii::app()->libs->add(array("knockout", "jquery-sortable", "setImmediate", "ajaxMock"));
			$this->render("index", array("tree" => $tree, "menuItems" => $menuItems));
		}

		public function actionGuardaMenuItem() {
			list($menuitem_id, $parent_id) = array($_POST["menuitem_id"], $_POST["parent_id"]);
			$qry = "
				insert into menu(menu_id,menuitem_id) values($parent_id,$menuitem_id)
			";
			echo Helpers::qryExec($qry);
		}
		
		public function actionAddItem($texto, $detalle, $url) {
			$qry = "
				insert into menuitem (texto, detalle, url) values(\"$texto\", \"$detalle\", \"$url\")
			";
			Helpers::qryExec($qry);
			echo Helpers::lastInsertedId();
		}
		
		public function actionAddMenu($texto) {
			$qry = "
				insert into menu (texto) values(\"$texto\")
			";
			Helpers::qryExec($qry);
			echo Helpers::lastInsertedId();
		}

		public function actionGuardaMenu() {
			$parent_id = filter_input(INPUT_GET, "parent_id");
			$ids = ($_GET["id"]);
			$orden = 0;
			foreach ($ids as $id) {
				$qry = "
					update menu set menu_id = $parent_id, orden = $orden where id = $id
				";
				$orden += 10;
				echo Helpers::qryExec($qry);
			}
			die;
		}

		public function actionBorraMenuItem($id){
			echo Helpers::qryExec("
				delete from menu where menuitem_id = $id;
				delete from menuitem where id = $id;
			");
		}
		
		public function actionBorraMenu($id){
			echo Helpers::qryExec("
				delete from menu where id = $id;
			");
		}
		
		private function getItems($parent_id, $tree) {
			$where = $parent_id ? "m.menu_id = $parent_id" : "m.menu_id IS null";
			$select = "SELECT m.id, m.menu_id, m.activo, m.orden,
					case when (mi.id is NULL) then m.texto else 
						case when mi.detalle != '' then mi.detalle else mi.texto end
					end as texto,
					case when (mi.id is NULL) 
						then m.texto 
						else case when mi.detalle != '' THEN mi.detalle ELSE mi.texto end
						end as detalle,
					(SELECT COUNT(*) FROM menu m1 WHERE m1.menu_id = m.id) AS children_tot
				FROM menu m
					 LEFT JOIN menuitem mi ON mi.id = m.menuitem_id
				WHERE $where and m.activo = 1
				order by orden";
			$qry = Helpers::qryAll($select);
			$id = 0;
			foreach ($qry as $item) {
				$tree[$id] = $item;
				if ($item["children_tot"] > 0) {
					$children = $this->getItems($item["id"], array());
					$tree[$id]["children"] = $children;
				} else {
					$tree[$id]["children"] = array();
				}
				$id++;
			}
			return $tree;
		}

	}
	