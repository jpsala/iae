<?php

class
NotaController extends Controller
{

  public function filters()
  {
    return array(
//            'accessControl', // perform access control for CRUD operations
     'rights',
    );
  }

  public function actionCargaXAsignatura($cuali = false)
  {
    $this->bootstrap = false;
    $this->pageTitle = "Carga de notas";
    // alumno_id de prueba = 15034
//        $ciclo_activo = Ciclo::getActivo();
    /* @var $division Division */
//        $asignatura = Asignatura::model()->find();
//        $division = Division::model()->find();
//        $divisionAsignatura = DivisionAsignatura::model()->find("Division_id=$division->id and Asignatura_id =$asignatura->id");
//        $logica_id = $asignatura->logica->id;
//        $logicaItems = LogicaItem::model()->findAll(array("condition" => "Logica_id=$logica_id", "order" => "orden"));
//        $logicaPeriodo = LogicaPeriodo::model()->findByPk(1);
//        $notas = $divisionAsignatura->getNotas($logicaPeriodo->id);
    $this->render("cargaXAsignatura", array('cuali' => $cuali)); //, array("notas" => $notas, "logicaItems" => $logicaItems, "fechaPeriodo" => date("2012-01-02"), "divisionAsignatura" => $divisionAsignatura, "asignatura"=>$asignatura,"logicaPeriodo"=>$logicaPeriodo));
  }

  public function actionTraeNotasParaCargaXAsignatura($division_asignatura_id, $logica_periodo_id, $cuali = false)
  {
    if (!$logica_periodo_id) {
      return null;
    }
    $conIntegradora = Helpers::qryScalar("
      select da.integradora FROM division_asignatura da
        INNER JOIN division d ON da.Division_id = d.id
        INNER JOIN anio a ON d.Anio_id = a.id
      WHERE da.id = $division_asignatura_id
    ");
    $divisionAsignatura = DivisionAsignatura::model()->findByPk($division_asignatura_id);
    $asignatura = $divisionAsignatura->asignatura;
    $asignaturaTipo_input = $asignatura->asignaturaTipo->tipo_input;
    $logica_id = $asignatura->logica->id;
    $integradora = $divisionAsignatura->integradora ? $divisionAsignatura->integradora : 0;
    $logicaItems = LogicaItem::model()->with("logicaPeriodo")->findAll(array(
      "condition" => "t.Logica_id = $logica_id and not (tipo_nota=100 and $integradora <> 1)",
      "order" => "logicaPeriodo.orden, t.orden"
     )
    );
    $logicaPeriodoSeleccionada_id = Helpers::qryScalar("select id from logica_periodo where id = $logica_periodo_id");
    $notas = $divisionAsignatura->getNotas($logicaPeriodoSeleccionada_id, $cuali);
    // ve2($cuali);
    $this->renderPartial("_notas", array(
      "notas" => $notas,
      "logicaItems" => $logicaItems,
      "divisionAsignatura" => $divisionAsignatura,
      "asignatura" => $asignatura,
      "asignaturaTipo_input" => $asignaturaTipo_input,
      "logicaPeriodoSeleccionada_id" => $logicaPeriodoSeleccionada_id,
      "integradora" => $conIntegradora,
      "cuali" => $cuali
     )
    );
  }

  public function actionCargaXAsignaturaGraba()
  {
    $cuali = (isset($_POST['cuali']) and $_POST['cuali'] == 1) ? true:false;
    $notas = array();
    foreach ($_POST["alumnos"] as $alumno_id) {
      $notas[$alumno_id] = $_POST["notas"][$alumno_id];
    }
    /* @var $divisionAsignatura DivisionAsignatura */
    $divisionAsignatura = DivisionAsignatura::model()->findByPk($_POST['division_asignatura_id']);
    $divisionAsignatura->grabaNotasSinValidar($notas, $cuali);
  }

  public function actionCargaXAsignaturaProcesa()
  {
    $notasAProcesar = reset($_POST["notas"]); // uso reset porque la primer dimensiÃ³n el array es el id del alumno, sirve para actionCargaXAsignaturaGuarda pero acÃ¡ no hace falta
    $alumnoDivision = AlumnoDivision::model()->findByPk($_POST["alumno_division_id"]);
    $asignatura_id = DivisionAsignatura::model()->findByPk($_POST["division_asignatura_id"])->Asignatura_id;
    $asignatura = Asignatura::model()->findByPk($asignatura_id);
    $logica_periodo_id = $_POST["logica_periodo_id"];
//	vd(Asignatura::model()->findByPk($asignatura->id)->logica->procesaNotas($alumnoDivision, $asignatura, $notasAProcesar, $logica_periodo_id));
    echo json_encode(Asignatura::model()->findByPk($asignatura->id)->logica->procesaNotas($alumnoDivision, $asignatura, $notasAProcesar, $logica_periodo_id));
  }

  public function actionLogica()
  {
    $pn = new ParamNivel(array(
     "controller" => $this,
     "onchange" => "changeNivel",
    ));
    $this->render("logica", array(
      "paramNivel" => $pn)
    );
  }

  public function actionTraeAsignaturaTipos($nivel_id)
  {
    ?>
    <select id="asignatura-tipos-select" data-placeholder="Tipos de asignatura"
            class="chzn-select">
      <?php
      $x = array('prompt' => '');
      echo CHtml::listOptions(null, CHtml::listData(
       AsignaturaTipo::model()->findAll("nivel_id=$nivel_id"), 'id', 'nombre')
       , $x
      );
      echo "<option value='-1'> - Nuevo Tipo de Asignatura - </option>";
      ?>
    </select>

    <?php
  }

  public function actionTraeLogicas($asignatura_tipo_id)
  {
    $data = Helpers::qryAll("select l.id, concat(t.nombre,'.', c.nombre) as nombre
            from asignatura_tipo t
              inner join logica_ciclo lc on lc.Asignatura_Tipo_id = t.id
              inner join logica l on lc.Logica_id = l.id
              inner join ciclo c on c.id = lc.Ciclo_id
            where t.id = $asignatura_tipo_id");
    echo '<select id="logica-select" data-placeholder="Lógicas" class="chzn-select">';
    $x = array('prompt' => '');
    try{
      echo CHtml::listOptions(null, CHtml::listData($data, 'id', 'nombre'), $x );
    } catch(Exception $e){
      echo 'Caught exception: ',  $e->getMessage(), "\n";
      die;
    }
    echo "<option value='nueva'> - Nueva LÃ³gica -</option>";
    echo '</select>';

  }

  public function actionTraeLogicaData($logica_id, $asignatura_tipo_id)
  {
    $logica = Logica::model()->findByPk($logica_id);
    if (!$logica) {
      $logica = new Logica();
      //$logica->
    }
    $liNuevo = new LogicaItem();
    $liNuevo->Logica_id = $logica->id;
    $lpNuevo = new LogicaPeriodo();
    $lpNuevo->Logica_id = $logica->id;
    $lpNuevo->fecha_inicio = date("Y/m/d");
    $lpNuevo->fecha_inicio_ci = date("Y/m/d");
    $lpNuevo->fecha_fin = date("Y/m/d");
    $lpNuevo->fecha_fin_ci = date("Y/m/d");
    $this->renderPartial("_logicaData", array(
     "logica" => $logica,
     "liNuevo" => $liNuevo,
     "lpNuevo" => $lpNuevo,
    ));
  }

  public function actionGrabaLogica()
  {
    $logica_id = $_POST["logica_id"];
    $asignatura_tipo_id = $_POST["asignatura_tipo_id"];
    /* @var $l Logica */
    $l = Logica::model()->findByPk($logica_id);
    $ta = AsignaturaTipo::model()->findByPk($_POST["asignatura_tipo_id"]);
    if (!$ta) {
      $ta = new AsignaturaTipo();
      $ta->Nivel_id = $_POST["nivel-id"];
    }
    $ta->nombre = $_POST["asignatura-tipo-nombre"];
    $ta->tipo_input = $_POST["asignatura-tipo-tipo-input"];
    if (!$ta->save()) {
      echo "AsignaturaTipo ";
      var_dump($ta->errors);
      die;
    }
    if (!$l) {
      $l = new Logica();
      $l->nombre = $_POST["nombre"];
      $l->formula = $_POST["formula"];
      if (!$l->save()) {
        echo "Logica nueva";
        var_dump($l->errors);
        die;
      } else {
        $logica_id = $l->id;
        $lc = new LogicaCiclo();
        $lc->Logica_id = $logica_id;
        $lc->Asignatura_Tipo_id = $asignatura_tipo_id;
        $lc->Ciclo_id = Ciclo::getCicloParaCargaDeNotas()->id;
        if (!$lc->save()) {
          echo "LogicaCiclo nueva";
          var_dump($lc->errors);
          die;
        }
      }
    } else {
      $l->nombre = $_POST["nombre"];
      $l->formula = $_POST["formula"];
    }
    if (!$l->save()) {
      echo "Logica $l->id";
      var_dump($l->errors);
      die;
    }

    foreach ($_POST["logica-item"] as $id => $item) {
      $li = LogicaItem::model()->findByPk($id);
      if (!$li) {
        if (!$item["abrev"] and !$item["manual"] and !$item["formula"] and !$item["nombre"] and !$item["requerida"]) {
          continue;
        }
        $li = new LogicaItem();
        $li->Logica_id = $logica_id;
      }
      $li->tiene_conducta = isset($item["tiene-conducta"]) ? 1 : 0;
      $li->abrev = $item["abrev"];
      $li->manual = $item["manual"];
      $li->formula = $item["formula"];
      $li->nombre = $item["nombre"];
      $li->nombre_unico = $item["nombre-unico"];
      $li->requerida = $item["requerida"];
      $li->tipo_nota = $item["tipo-nota"];
      $li->Logica_Periodo_id = $item["periodo_id"];
      $li->imprime_en_boletin = $item["imprime-en-boletin"];
      $li->orden = $item["orden"];
      $li->formula = $item["formula"];
      if (!$li->save()) {
        echo "Item $li->id";
        var_dump($item);
        var_dump($li->errors);
        die;
      }
    }
    foreach ($_POST["logica-periodo"] as $id => $item) {
      $lp = LogicaPeriodo::model()->findByPk($id);
      if (!$lp) {
        if (!$item["nombre"] and !$item["abreviatura"] and !$item["orden"]) {
          continue;
        }
        $lp = new LogicaPeriodo();
        $lp->Logica_id = $logica_id;
      }
      $lp->nombre = $item["nombre"];
      $lp->orden = $item["orden"];
      $lp->abrev = $item["abreviatura"];
      $lp->fecha_inicio = date("Y-m-d", mystrtotime($item["fecha_inicio"]));
      $lp->fecha_fin = date("Y-m-d", mystrtotime($item["fecha_fin"]));
      $lp->fecha_inicio_ci = date("Y-m-d", mystrtotime($item["fecha_inicio_ci"]));
      $lp->fecha_fin_ci = date("Y-m-d", mystrtotime($item["fecha_fin_ci"]));
      if (!$lp->save()) {
        echo "Periodo $lp->id";
        var_dump($item);
        var_dump($lp->errors);
      }
      $lp = LogicaPeriodo::model()->findByPk($id);
    }
  }

  public function actionLogicaItemBorra($item_id)
  {
    LogicaItem::model()->deleteByPk($item_id);
  }

  public function actionLogicaPeriodoBorra($periodo_id)
  {
    LogicaPeriodo::model()->deleteByPk($periodo_id);
  }

  public function actionLogicaBorra($logica_id)
  {
    Logica::model()->deleteByPk($logica_id);
  }

  public function actionTest()
  {
    $this->renderPartial("test");
  }

  public function actionSelect()
  {
    $this->render('select', array('ejemplo' => 'ejemplo1', 'users' => User::model()->findAll()));
  }

  public function actionTraeTipoAsignatura($asignatura_tipo_id)
  {
    $ta = AsignaturaTipo::model()->findByPk($asignatura_tipo_id);
    $this->renderPartial("_tipoAsignatura", array("ta" => $ta));
  }

  public function actionborraAsignaturaTipo($asignatura_tipo_id)
  {
    $ta = AsignaturaTipo::model()->findByPk($asignatura_tipo_id);
    if ($ta->delete()) {
      echo "ok";
    }
  }

  public function actionTraeItemsData()
  {

  }

  public function actionTraeNotasParaImpresion1($division_asignatura_id)
  {
    $divisionAsignatura = DivisionAsignatura::model()->findByPk($division_asignatura_id);
    $asignatura = $divisionAsignatura->asignatura;
    $asignaturaTipo = $asignatura->asignaturaTipo;
    $logica_id = $asignatura->logica->id;
    $logicaItems = LogicaItem::model()->with("logicaPeriodo")
     ->findAll(array("condition" => "t.Logica_id=$logica_id", "order" => "logicaPeriodo.orden, t.orden"));
    $cantItems = count($logicaItems);
    $notas = $divisionAsignatura->getNotas();
    $this->renderPartial("_notasParaImpresion1", array(
     "notas" => $notas,
     "logicaItems" => $logicaItems,
     "fechaPeriodo" => date("2012-01-02"),
     "divisionAsignatura" => $divisionAsignatura,
     "asignatura" => $asignatura,
     "asignaturaTipo" => $asignaturaTipo,
     "logicaPeriodo" => $logicaPeriodo,
     "cantItems" => $cantItems,));
  }

}
