
<?php

	class NovedadController extends Controller {

		//public $layout = "column1";
		public function filters() {
			return array(
//            'accessControl', // perform access control for CRUD operations
					'rights',
			);
		}

		public function actionIndex() {
			$cnf = LiquidConf::model()->findAll("fecha_confirmado is null and id <> 0");
			if (count($cnf) !== 1) {
				Helpers::error("Error en la lilquidación", "Falta dar de alta la liquidación de este mes, avisar a Claudia...");
				die;
			}
			$liquid_conf_id = $cnf[0]->id;
			$niveles = Nivel::model()->findAll();
			$articulo_tipo_novedad = Opcion::getOpcionText("articulo_tipo_novedad", Null, "liquidacion");
			$articulos = Articulo::model()->with("articuloTipo")->findAll("articulo_tipo_id=$articulo_tipo_novedad and articuloTipo.novedad_tipo = 1");
			$this->render("index", array("liquid_conf_id" => $liquid_conf_id, "niveles" => $niveles, "articulos" => $articulos));
		}

		public function actionCrudNovedades($novedad_cab_id = null, $fechaDesde = null, $fechaHasta = null, $articulo_id = null, $filtra_usuario  = 1) {
			//    $fecha = date("Y/m/d",  time());
			if ($fechaDesde and $fechaHasta) {
				$fechaAnd = " fecha >= \"" . date("Y/m/d", mystrtotime(Helpers::date("Y/m/d", $fechaDesde))) . "\"";
				$fechaAnd .= " and fecha <= \"" . date("Y/m/d", mystrtotime(Helpers::date("Y/m/d", $fechaHasta))) . "\"";
			} else {
				$fechaAnd = " true ";
			}
			$articulo_id_and = $articulo_id ? " n.articulo_id = $articulo_id " : " true ";
//    echo $fechaAnd;die;
			$novedad_cab_idAnd = $novedad_cab_id ? " novedad_cab_id = $novedad_cab_id " : " true ";
				$where_usuario = $filtra_usuario=='1' ? ' user_id = '.Yii::app()->user->id : 'true';
//			$user_id = Yii::app()->user->id;
			$novedades = Yii::app()->db->createCommand("
				select nc.*, n.*, n.id as novedad_id, concat(a.apellido,\", \",a.nombre) as alumno,
										ar.nombre as articulo_nombre
					from novedad n
								inner join novedad_cab nc on nc.id = n.novedad_cab_id
								inner join alumno a on n.alumno_id = a.id
					inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_admin = 1
								inner join alumno_division ad on ad.Alumno_id = a.id and ad.activo = true
								inner join division d on d.id = ad.Division_id
								inner join anio on anio.id = d.Anio_id
								inner join nivel on nivel.id = anio.Nivel_id
								left join articulo ar on ar.id = n.articulo_id
					where $where_usuario and $fechaAnd and $novedad_cab_idAnd and $articulo_id_and
					 order by nivel.orden, anio.nombre,d.nombre, a.apellido, a.nombre
          limit 2000
		")->queryAll();
			$articulo_tipo_novedad = Opcion::getOpcionText("articulo_tipo_novedad", Null, "liquidacion");
			$articulos = Articulo::model()->with("articuloTipo")->findAll("articulo_tipo_id=$articulo_tipo_novedad and articuloTipo.novedad_tipo = 1");
			$this->render("crudNovedades", array(
					"novedad_cab_id" => $novedad_cab_id,
					"novedades" => $novedades,
					"fechaDesde" => $fechaDesde,
					"fechaHasta" => $fechaHasta,
					"articulos" => $articulos,
					"articulo_id" => $articulo_id,
					'filtra_usuario' => $filtra_usuario)
			);
		}

		public function actionTraeAnios($nivel_id) {
			$this->renderPartial("traeAnios", array("nivel_id" => $nivel_id));
		}

		public function actionTraeDivisiones($anio_id) {
			$this->renderPartial("traeDivisiones", array("anio_id" => $anio_id));
		}

		public function actionTraeAlumnos($division_id) {
			$this->renderPartial("traeAlumnos", array("division_id" => $division_id));
		}

		public function actionSube() {
			$ret = new stdClass();
			$tipo = $_POST["tipo"];
			$articulo_id = $_POST["articulo_id"];
			$detalle = $_POST["detalle"];
			$importe = $_POST["importe"];
			$liquid_conf_id = $_POST["liquid_conf_id"];
			$ciclo_id = Ciclo::getActivo()->id;
			$id = $_POST["id"];
			//$ret->errores = null;
			$ret->cant = 0;
			$nc = new NovedadCab();
			$nc->fecha_carga = new CDbExpression('NOW()');
			$nc->fecha = new CDbExpression('NOW()');
			$nc->user_id = Yii::app()->user->id;
			$nc->liquid_conf_id = $liquid_conf_id;
			if (!$nc->save()) {
				$ret->errores = $nc->errores;
				echo json_encode($ret);
				die;
			}
			$ret->novedad_cab_id = $nc->id;
			if ($tipo == "nivel") {
				$qry = "
                select a.* 
                from alumno a
 				  inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_admin = 1 and not ae.ingresante
                  inner join alumno_division ad on ad.alumno_id = a.id and ad.activo = 1 and ad.ciclo_id = $ciclo_id
                  inner join division d on d.id = ad.division_id
                  inner join anio an on an.id = d.anio_id
                where an.nivel_id = $id and a.activo = 1";
			} elseif ($tipo == "anio") {
				$qry = "
                select a.* 
                from alumno a
                  inner join alumno_division ad on ad.alumno_id = a.id and ad.activo = 1 and ad.ciclo_id = $ciclo_id
            	  inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_admin = 1 and not ae.ingresante
                  inner join division d on d.id = ad.division_id and ad.activo = 1
                where d.anio_id = $id and a.activo = 1";
			} elseif ($tipo == "division") {
				$qry = "
                select a.* 
                from alumno a
            	  inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_admin = 1 and not ae.ingresante
                  inner join alumno_division ad on ad.alumno_id = a.id and ad.activo = 1 and ad.ciclo_id = $ciclo_id
                where ad.division_id = $id  and a.activo = 1";
			} elseif ($tipo == "alumno") {
				if (!isset($_POST["alumnos"])) {
					$alumnos = array($id);
				} else {
					$alumnos = $_POST["alumnos"];
				}
				$in = "(";
				foreach ($alumnos as $alumno) {
					$in .= $alumno . ",";
				}
				$in = substr($in, 0, -1);
				$in .= ")";
				$qry = "
                select a.* 
                    from alumno a
                        inner join alumno_division ad on ad.alumno_id = a.id and ad.activo = 1 and ad.ciclo_id = $ciclo_id
                    where a.activo = 1 and ad.alumno_id in $in
        ";
			}

			foreach (Alumno::model()->findAllBySql($qry) as $alumno) {
				$n = new Novedad();
				$n->novedad_cab_id = $nc->id;
				$n->alumno_id = $alumno->id;
				$n->articulo_id = $articulo_id;
				$n->detalle = $detalle;
				$n->importe = $importe;
//      echo $n->articulo_id;die;
				if (!$n->save()) {
					$ret->errores = $n->errors;
					echo json_encode($ret);
					die;
				} else {
					$ret->cant++;
				}
			}
			echo json_encode($ret);
		}

		public function actionBorraNovedad() {
			if (isset($_POST["selec"])) {
				foreach ($_POST["selec"] as $selec => $val) {
					borraNovedad($selec);
				}
			} else {
				borraNovedad($_POST["novedad_id"]);
			}
		}

		public function actionUpdNovedad($novedad_id, $importe) {
			$n = Novedad::model()->findByPk($novedad_id);
			$n->importe = $importe;
			$n->save();
		}

	}

	function borraNovedad($novedad_Id) {
		$n = Novedad::model()->findByPk($novedad_Id);
		$novedad_cab_id = $n->novedad_cab_id;
		$cant = Helpers::qryScalar("select count(*) from novedad where novedad_cab_id = $novedad_cab_id and id <> $novedad_Id");
		$cab = NovedadCab::model()->findByPk($novedad_cab_id);
		$user_id = $cab->user_id;
		$usuario = Yii::app()->db->createCommand("
       select nombre from user where id = $user_id ")->queryScalar(array());

		if (($cab->user_id == Yii::app()->user->id) or Yii::app()->user->checkAccess("Admin")) {
			$n->delete();
			if ($cant == 0) {
				$cab = NovedadCab::model()->findByPk($novedad_cab_id);
				$cab->delete();
			}
			$status = "ok";
		} else {
			$status = "La novedad fué cargada por el usuario $usuario <br/>y solo puede ser borrada por el!";
		}
		echo $status;
	}

?>
