<?php

class OrientacionController extends Controller {

    public function actionIndex() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        //$this->render('index');
        var_dump(Yii::app()->user);
    }

    public function actionAdmin() {
        $this->pageTitle = "Administración de orientación";
        $this->render("orientacion");
    }


    public function actionOptions($nivel_id) {
        $x = array('prompt' => '');
        echo CHtml::listOptions(null, CHtml::listData(Anio::model()->findAll("nivel_id=$nivel_id"), 'id', 'nombre'), $x);
    }

}