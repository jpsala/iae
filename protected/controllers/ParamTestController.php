<?php

class ParamTestController extends Controller {

  public $layout = "column1";

  public function actionIndex() {
    $pn = new ParamNivel(array(
        "controller"=>$this,
        "onchange"=>"changeNivel",
    ));
    $pa = new ParamAnio(array(
        "controller"=>$this,
        "onchange"=>"changeAnio",
    ));
    $pd = new ParamDivision(array(
        "controller"=>$this,
        "onchange"=>"changeDivision",
    ));
    $pal = new ParamAlumno(array(
        "controller"=>$this,
        "onchange"=>"changeAlumno",
    ));
    $pal->todos = false;
    $this->render("index",array(
        "paramNivel"=>$pn, 
        "paramAnio"=>$pa, 
        "paramDivision"=>$pd,
        "paramAlumno"=>$pal));
    
  }
  
}

?>
