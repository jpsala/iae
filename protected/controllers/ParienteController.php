<?php

class ParienteController extends Controller {

  public $layout = "column1";

  public function actionParientesConDNI() {
    $s = "
      select p.apellido, p.nombre, p.numero_documento as documento
        from pariente p
          inner join alumno a on a.Familia_id = p.Familia_id
          inner join alumno_estado ae on ae.id = a.estado_id
      where a.activo and ae.activo_admin";
    $q = Helpers::qryAll($s);
    $this->render("parientesConDNI", array("data"=>$q));
  }
}

?>
