<?php

class PerfilController extends Controller {

  public function actionIndex() {
    // renders the view file 'protected/views/site/index.php'
    // using the default layout 'protected/views/layouts/main.php'
    //$this->render('index');
    var_dump(Yii::app()->user);
  }

  public function actionAdmin() {
    $this->pageTitle = "Administración de perfiles";
    $this->render("admin");
  }

  public function actionAdminPerfiles($perfil_id) {
    $perfiles = PuestoTrabajoPerfil::model()->findAll("id = $perfil_id");
    $this->renderPartial("_adminPerfil", array("id " => $perfil_id, "perfiles" => $perfiles));
  }

  public function actionAdminGrabaPuesto() {
    $pt = PuestoTrabajo::model()->findByPk($_POST["puesto_id"]);
    if (!$pt) {
      $$pt = new PuestoTrabajo();
    }
    $pt->nombre = $_POST["nombre"];

    if (!$pt->save()) {
      echo json_encode($pt->errors);
    }
  }

}

