<?php

    class PeterController extends Controller
    {
        public $layout = "ko";
        public function actionCierre(){
            $this->render('cierre');
        }
        public function actionCierrePDF($desde, $hasta, $empresa){
            $this->renderPartial('cierrePDF', array('desde'=>$desde,'hasta'=>$hasta, 'empresa'=>$empresa));
        }
    }