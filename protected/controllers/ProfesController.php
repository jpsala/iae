<?php

    class ProfesController extends Controller
    {

        public $layout = "ko";

        public function actionAdmin($isAdmin = false, $user_id = -1) {
            if(!$isAdmin) {
                $user_id  = Yii::app()->user->id;
                $isAdmin  = $user_id ? false : true;
                $usuarios = array();
            } else {
                $usuarios = Helpers::qryAll("
                  SELECT u.*, (SELECT COUNT(*) FROM authassignment a WHERE a.userid = u.id AND a.itemname = 'Profesor') AS profe
                    FROM user u
                  ORDER BY u.apellido, u.nombre
                ");
            }
            $datos   = Helpers::qryOrEmpty("select u.*,(SELECT COUNT(*) FROM authassignment a WHERE a.userid = u.id AND a.itemname = 'Profesor') AS profe from user u where id = $user_id");
            $niveles = Helpers::qryAll("select * from nivel where activo");
            $this->render("admin", array(
                "datos"   => $datos, "niveles" => $niveles,
                "isAdmin" => $isAdmin, "usuarios" => $usuarios
            ));
        }

        public function actionAdminGetDatosUsuario($user_id) {
            $datos = Helpers::qryOrEmpty("select u.*,(SELECT COUNT(*) FROM authassignment a WHERE a.userid = u.id AND a.itemname = 'Profesor') AS profe from user u where id = $user_id");
            echo json_encode($datos);
        }

        public function actionAdminGraba() {
            if(!$_POST['id']) {
                throw new Exception('Falta el ID del usuario');
            }
            $user = User::model()->findByPk($_POST['id']);
            $user->setAttributes(Helpers::clearAttributes($_POST));
            $user->save();
            echo json_encode($user->errors);
        }

    }

?>
