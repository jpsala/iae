<?php

class PromocionController extends Controller {

    public function actionPromocionPreview() {
      /*
      * si se pasó el año se usa la siguiente variable
      */
      $estamosEnElSiguienteAnio = true;
      $cicloActivo = $estamosEnElSiguienteAnio ? Ciclo::getActivo()->id : Ciclo::getActivo()->id - 1;
      $diasParaAtras = (3600*24*4);
      $time = $estamosEnElSiguienteAnio ? (time() - $diasParaAtras) : time();
      $nombreCicloNuevo = date("Y", $time);
      $existe = Helpers::qryScalar("
        select count(*) from ciclo where nombre = \"$nombreCicloNuevo\"
      ") > 0;
      $promocionados = false;
        $sDiv = "select d.id as division_id, dmat.id as division_siguiente_id, 
                            d.nombre as division, a.nombre as anio,
                            n.nombre as nivel,nmat.nombre as nivel_sig, amat.nombre as anio_sig,
                            dmat.nombre as division_sig,
                            (select count(*) 
                                from alumno_division ad 
                                  inner join alumno a on ad.alumno_id = a.id and a.activo = 1
																	inner join alumno_estado ae on ae.id = a.estado_id 
																		and ae.activo_admin = 1 and ae.activo_edu = 1
                                where ad.division_id = d.id and ad.activo = 1 
                                      and ad.Ciclo_id = $cicloActivo
                            ) as count
                        from division d
                            inner join anio a on a.id = d.Anio_id
                            inner join nivel n on n.id = a.Nivel_id
                            inner join division dmat on dmat.id = d.division_id_siguiente
                            inner join anio amat on amat.id = dmat.Anio_id
                            inner join nivel nmat on nmat.id = amat.Nivel_id
                        where d.activo = 1
													group by d.id, a.id, n.id
													order by n.orden, a.orden, d.orden";
//        vd2($sDiv);
        $divs = Helpers::qryAll($sDiv);
        foreach ($divs as $key => $div) {
            $division_id = $div["division_id"];
            $sAlumnos = "
                select al.matricula, concat(al.nombre, ', ', al.apellido) as alumno_nombre
                      from alumno_division ad 
                          inner join division d on d.id = ad.Division_id and d.activo = 1 and d.id = $division_id
                          inner join division dmat on dmat.id = d.division_id_siguiente
                          inner join anio a on a.id = d.Anio_id
                          inner join anio amat on amat.id = dmat.Anio_id
                          inner join nivel n on n.id = a.Nivel_id
                          inner join nivel nmat on nmat.id = amat.Nivel_id
                          inner join alumno al on al.id = ad.Alumno_id
													inner join alumno_estado ae on ae.id = al.estado_id and ae.activo_admin = 1 and ae.activo_edu = 1
                          inner join socio s on s.Alumno_id = al.id
                      where ad.Ciclo_id = $cicloActivo and ad.activo = 1 and al.activo = 1 and ad.siguiente_division_id is null
                      order by al.apellido, al.nombre
            ";
//						vd($sAlumnos);
            $rows = Helpers::qryAll($sAlumnos);
            $divs[$key]["rows"] = $rows;
            if(count($rows) > 0){
              $promocionados = true;
            }
        }
        $this->render("promocionPreview", array("divs" => $divs, "existe" => $existe, "promocionados"=>$promocionados));
    }

    public function actionPromocion() {
        $this->render("promocion");
    }
    
    public function actionPromocionAlumnos() {
        $this->render("promocionAlumnos");
    }
    
    public function actionFix(){
        $this->render("fix");
    }
	
    public function actionUndo(){
        $this->render("undo");
    }

    public function actionVer(){
        $this->render("ver");
    }
}