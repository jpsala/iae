<?php

class proveedorController extends Controller {

    public $layout = "column1";

    public function actionproveedor($id = null) {
        $model = Proveedor::model()->findByPk($id);
        if (!$model) {
            $model = new Proveedor();
        }
        $this->render('proveedorIndex', array('model' => $model));
    }

    public function actionproveedorGraba() {
        $tran = Yii::app()->db->beginTransaction();

        $model = Proveedor::model()->findByPk($_POST['id']);

        if (!$model) {
            $model = new Proveedor();
            $socio = new Socio();
        }

        $model->attributes = $_POST;
        $model->razon_social = ucwords(strtolower($_POST['razon_social']));
        if (!$model->save()) {
            echo json_encode($model->getErrors());
            $tran->rollback();
            Yii::app()->end();
        } else {
            if (isset($socio)) {
                $socio->Proveedor_id = $model->id;
                $socio->tipo = "P";
                if (!$socio->save()) {
                    echo json_encode($socio->getErrors());
                    $tran->rollback();
                    Yii::app()->end();
                }
            }
        }
        echo json_encode("");
        $tran->commit();
    }

    public function actionproveedorForm($id) {
        $model = Proveedor::model()->findByPk($id);

        if (!$model) {
            //var_dump('not model');
            $model = new Proveedor();
            $model->codigo = Helpers::qryScalar("select max(codigo)+1 from proveedor");
        }

        $this->renderPartial('_proveedorForm', array('model' => $model));
    }

    public function actionproveedorBorra($id) {
			return;
        $socio_id = Helpers::qryScalar("select id from socio where proveedor_id = $id");
        $tr = Yii::app()->db->beginTransaction();
        if (Socio::model()->deleteAll("id=$socio_id") and
                Proveedor::model()->deleteAll("id=$id")) {
            $tr->commit();
        }
    }

}

?>
