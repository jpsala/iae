<?php

class PuestoTrabajoController extends Controller {

	public function actionAdmin() {
		$this->pageTitle = "Administración de perfiles";
		$this->render("puestos");
	}

	public function actionOptions() {
		echo "<option value=\"\"></option>";
		foreach (PuestoTrabajo::model()->findAll() as $d) {
			echo "<option value=\"$d->id\">$d->nombre</option>";
		}
	}

	public function actionAdminItems($puesto_trabajo_id) {
		$items = PuestoTrabajoPerfil::model()->findAll("puesto_trabajo_id = $puesto_trabajo_id");
		$this->renderPartial("_adminPuestos", array("puesto_trabajo_id" => $puesto_trabajo_id, "items" => $items));
	}

	public function actionAdminItemPerfilBorra($id) {
		$itemperfil = PuestoTrabajoPerfil::model()->findByPk($id);
		if (!$itemperfil->delete()) {
			echo json_encode($itemperfil->errors);
		}
	}

	public function actionAdminPuestoTrabajo($puesto_id) {
		$items = PuestoTrabajoPerfil::model()->findAll("id = $puesto_id");
		$this->renderPartial("_adminPerfil", array("puesto_id" => $puesto_id, "puestos" => $items));
	}

	public function actionAdminItemPerfilGraba() {
		$r = null;
		$criteria = new CDbCriteria();
		$criteria->addCondition("talonario_id = $_POST[talonario_id] ");
		//$criteria->addCondition("destino_id = $_POST[destino_id] ");
		$criteria->addCondition("puesto_trabajo_id = $_POST[puesto_trabajo_id] ");
		$existe = PuestoTrabajoPerfil::model()->findAll($criteria);
		if (!$existe) {
			$r->nuevo = true;
			$tran = Yii::app()->db->beginTransaction();
			$item = new PuestoTrabajoPerfil();
			$item->talonario_id = $_POST['talonario_id'];
			$item->destino_id = $_POST['destino_id'];
			$item->puesto_trabajo_id = $_POST['puesto_trabajo_id'];
			if (!$item->save()) {
				$tran->rollback();
				echo json_encode($item->getErrors());
				Yii::app()->end();
			}
			$tran->commit();
			$r->id = $item->id;
			echo json_encode($r);
		} else {
			$r->error = 'Error, talonario ya vinculado';
			echo json_encode($r);
		}
	}

	public function actionIndex() {
		$this->render("index");
	}

	public function actionGetTalonariosAjax($puesto_trabajo_id) {
		if ($puesto_trabajo_id !== "") {
			$this->renderPartial("getTalonarioAjax", array("puesto_trabajo_id" => $puesto_trabajo_id));
		}
	}

	public function actionAgregaComprobante($comprobante_id, $puesto_trabajo_id) {
		$pvn = Helpers::qryScalar("select punto_venta_numero from puesto_trabajo where id = $puesto_trabajo_id");
		$t = new Talonario();
		$t->comprob_id = $comprobante_id;
		$t->punto_venta_numero = $pvn;
		$t->letra = "X";
		$t->numero = 1;
		$t->activo = 0;
		$t->numeracion_manual = 1;
		$t->imprimeAuto = 0;
		if (!$t->save()) {
			echo json_encode($t->errors);
		} else {
			$select = "
				select t.id, c.nombre, t.letra, t.numero, t.activo, t.impresora, 
						 t.imprimeAuto, t.numeracion_manual, t.comprob_id
					from puesto_trabajo p
						inner join talonario t on t.punto_venta_numero = p.punto_venta_numero
						inner join comprob c on c.id = t.comprob_id
				where t.id = $t->id";
			$row = Helpers::qryObj($select);
			$this->getTalnoarioTr($row);
		}
	}

	public function actionModifica($talonario_id, $campo, $val) {
		$t = Talonario::model()->findByPk($talonario_id);
		$t->$campo = $val;
		if (!$t->save()) {
			echo json_encode($t->errors);
		} else {
			echo "ok";
		}
	}

	public function getTalnoarioTr($row) {
		?>
		<tr id="<?php echo $row->id; ?>">
			<?php $checkedActivo = $row->activo ? "checked" : ""; ?>
			<td>
				<input type="checkbox" class="td-corto" name="activo" <?php echo $checkedActivo; ?>/>
			</td>
			<td><?php echo $row->nombre; ?></td>
			<td class="hidden"><input name="comprob_id"  value="<?php echo $row->comprob_id; ?>"/></td>
			<td><input class="td-corto" name="letra" value="<?php echo $row->letra; ?>"/></td>
			<td><input class="td-medio" name="numero" value="<?php echo $row->numero; ?>"/></td>
			<?php $checkednumeracion_manual = $row->numeracion_manual ? "checked" : ""; ?>
			<td>
				<input type="checkbox" class="td-corto" name="numeracion_manual" <?php echo $checkednumeracion_manual; ?>/>
			</td>
			<td><input class="td-largo" name="impresora" value="<?php echo $row->impresora; ?>"/></td>
			<?php $checkedManual = $row->imprimeAuto ? "checked" : ""; ?>
			<td>
				<input class="td-corto" type="checkbox" name="imprimeAuto" <?php echo $checkedManual; ?>/>
			</td>
			<td></td>
			<td><input type="button" onclick="borraTalonario(<?php echo $row->id; ?>)" value="Borra"/></td>
		</tr>

		<?php
	}

	public function actionGraba() {
		$ret = new stdClass;
		$puesto_trabajo_id = $_POST["puesto_trabajo_id"];
		$punto_venta_numero = $_POST["punto_venta_numero"];
		$t = PuestoTrabajo::model()->findByPk($_POST["puesto_trabajo_id"]);
		if (!$t or $puesto_trabajo_id == "") {
			$t = new PuestoTrabajo();
			$existePV = Helpers::qryScalar("
			select count(*) from puesto_trabajo 
				where punto_venta_numero = $punto_venta_numero
			  ");
		} else {
			$existePV = Helpers::qryScalar("
			select count(*) from puesto_trabajo 
				where punto_venta_numero = $punto_venta_numero and
					   id <> $puesto_trabajo_id
			  ");
		}
		if ($existePV > 0) {
			$ret->state = "error";
			$ret->error = array("Ya existe el punto de venta $punto_venta_numero");
			echo json_encode($ret);
			die;
		}
		$t->setAttributes($_POST);
		if (!$t->save()) {
			$ret->state = "error";
			$ret->error = $t->errors;
		} else {
			$ret->state = "ok";
			$ret->id = $t->id;
		}
		echo json_encode($ret);
	}

	public function actionBorraTalonario($talonario_id) {
		$pt = Talonario::model()->findByPk($talonario_id);
		$ret = new stdClass();
		if (Doc::model()->find("talonario_id = $talonario_id")) {
			$ret->state = "error";
			$ret->error = array("Borre primero los documentos asociados");
		} elseif (!$pt->delete()) {
			$ret->state = "error";
			$ret->error = $pt->errors;
		} else {
			$ret->state = "ok";
			//$pv->save();
		}
		echo json_encode($ret);
	}

	public function actionBorra($puesto_trabajo_id) {
		$pt = PuestoTrabajo::model()->findByPk($puesto_trabajo_id);
		$ret = new stdClass();
		if (Talonario::model()->find("punto_venta_numero = $pt->punto_venta_numero")) {
			$ret->state = "error";
			$ret->error = array("Borre primero los comprobantes asociados");
		} elseif (!$pt->delete()) {
			$ret->state = "error";
			$ret->error = $pt->errors;
		} else {
			$ret->state = "ok";
			//$pv->save();
		}
		echo json_encode($ret);
	}
	
}
