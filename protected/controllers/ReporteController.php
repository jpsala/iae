<?php

class ReporteController extends Controller {

  public function actionIndex() {
    $rg = new ReporteGen($this, isset($_REQUEST["output"]) ? $_REQUEST["output"] : null);
    $rg->render();
  }

  public function actionAdmin() {
    $reportes = Reporte::model()->findAll();
    $this->render("admin", array("reportes" => $reportes));
  }

  public function action_adminForm($reporte_id) {
    $reporte = Reporte::model()->findByPk($reporte_id);
    $this->renderPartial("_adminForm", array("reporte" => $reporte));
  }

  public function actionTest() {
    $pdf = new Pdf();
    $html = $this->renderPartial("test", array(), true);
    $pdf->render($html);
}

}
