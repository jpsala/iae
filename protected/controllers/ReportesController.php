<?php

require_once('tcpdf/config/lang/spa.php');
require_once('tcpdf/tcpdf.php');

class MyPDF extends TCPDF {

  public $fsize;
  public $orientacion;

  public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $header = array(), $titulo = "", $fsize = null, $subTitulo = null) {
    parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
    $this->fsize = $fsize;
    $this->orientacion = $orientation;
    $this->SetFontSize($fsize);
    $this->titulo = $titulo;
    $this->header = $header;
    $this->SetCreator(PDF_CREATOR);
    $this->SetAuthor('');
    $this->SetTitle($titulo);
    $this->SetSubject('IAE');
    $this->SetKeywords('TCPDF, PDF, xxx');
    $this->SetPrintHeader(true);
    $this->SetPrintFooter(false);
    $this->subTitulo = $subTitulo;
    $this->SetAutoPageBreak(true);
  }

  public function Header() {
    $this->SetY(4);
    $this->SetX(15);
    if ($this->titulo) {
      $this->setJPEGQuality(90);
      //$this->Image('images/logo.jpg');
      $this->Image('images/logo1BN.png');

      if ($this->orientacion == 'P') {
        $this->SetFontSize(11);
        $this->Cell(180, 24, $this->titulo, 1, 1, "C", false, "", "", "", "");
      } else {
        $this->SetFontSize(8);
        $this->Cell(268, 24, $this->titulo, 1, 1, "C", false, "", "", "", "");
      }
    }
    if ($this->subTitulo) {
      $this->SetY(18);
      $this->SetX(15);
      if ($this->orientacion == 'P') {
        $this->SetFontSize(11);
        $this->Cell(180, 8, $this->subTitulo, 0, 1, "C", false, "", "", "", "");
      } else {
        $this->SetFontSize(8);
        $this->Cell(270, 8, $this->subTitulo, 0, 1, "C", false, "", "", "", "");
      }
    }
    $this->SetY(31);
    $this->SetX(15);
    ////$this->SetFontSize(11);

    foreach ($this->header as $col) {
      $this->cell($col[0], 0, $col[1], 0, "", "L", false, "", 0);
    }
  }

  public function Footer() {
    $this->SetY(0);
  }

  public function CreateTextBox($textval, $x = 0, $y, $width = 0, $height = 16, $fontsize = 10, $fontstyle = '', $align = 'L') {


    $this->SetXY($x, $y); // 20 = margin left
    $this->SetFont(PDF_FONT_NAME_MAIN, $fontstyle, $fontsize);
    $this->Cell($width, $height, $textval, 0, false, $align);
  }

  public function renderRow($row) {
    $this->SetX(15);
    $nrow = 0;

    foreach ($this->header as $headerRow) {
      //MultiCell(55, 5, $txt, 0, '', 0, 1, '', '', true);
      $this->cell($headerRow[0], 7, $row[$nrow], 1, $nrow + 1 == count($this->header) ? 2 : 0);
      $nrow++;
    }
  }

}

class ReportesController extends Controller {

  public $layout = "column1";

  function actionNivelAnioDivision() {
    echo 1;
    die;
    $this->render("nivelAnioDivision");
  }

  function actionAlumnosResumido1_no($division_id = null, $sexo = null) {
    $sexoAnd = $sexo ? " and a.sexo = \"$sexo\" " : "";
    if ($division_id) {
      $row = Yii::app()->db->createCommand("
					select n.nombre as nivel, a.nombre as anio, d.nombre as division
						from division d
							inner join anio a on a.id = d.Anio_id
							inner join nivel n on n.id = a.Nivel_id
						where d.id = $division_id
				")->query()->read();
      $titulo = $row["nivel"] . " " . $row["anio"] . " " . $row["division"];
      $rows = Yii::app()->db->createCommand("
					select concat(a.apellido, \", \", a.nombre) as nombre_alumno, a.numero_documento as dni_alumno, pa.nombre as nacionalidad_alumno, 
											a.fecha_nacimiento as fecha_nacimiento_alumno
						from alumno a
								inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_edu = 1
										inner join alumno_division ad on ad.Alumno_id = a.id
										inner join division d on d.id = ad.Division_id and ad.activo = 1
										left join localidad l on l.id = a.localidad_id
										left join pais pa on pa.id = l.Pais_id
						where a.activo = 1 and d.id = $division_id $sexoAnd 
						order by a.apellido, a.nombre
				")->queryAll();
      $header = array(array(70, "Alumno"), array(25, "DNI"), array(40, "Nacionalidad"), array(15, "Edad"), array(30, "Fech.Nac."),);

      $pdf = new MyPDF("P", 'mm', 'A4', true, 'UTF-8', false, $header, $titulo);

      $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
      $pdf->SetFont('times', '', 14);

      $pdf->SetMargins(40, 20, 0, true);
      $pdf->AddPage();
      foreach ($rows as $row) {
        $pdf->renderRow(array(substr($row["nombre_alumno"], 0, 31), $row["dni_alumno"], $row["nacionalidad_alumno"], edad(date("Y-m-d", mystrtotime($row["fecha_nacimiento_alumno"]))), date("d/m/Y", mystrtotime($row["fecha_nacimiento_alumno"])),));
      }
      $pdf->Output();
    } else {
      $this->render("/reportes/alumnosResumido1");
    }
  }

  function actionAlumnosResumido1($division_id = null, $sexo = null) {
    $sexoAnd = $sexo ? " and a.sexo = \"$sexo\" " : "";
    if ($division_id) {
      $nivel_id = Helpers::qryScalar("SELECT n.id FROM nivel n
				INNER JOIN anio a ON n.id = a.Nivel_id
				INNER JOIN division d ON a.id = d.Anio_id AND d.id = $division_id");
      $order = $nivel_id == 4 ? "a.apellido, a.nombre" : "a.sexo desc, a.apellido, a.nombre";
      $ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
      if ($_GET["output"] == "pdf") {
        $row = Yii::app()->db->createCommand("
						select n.nombre as nivel, a.nombre as anio, d.nombre as division
							from division d
								inner join anio a on a.id = d.Anio_id
								inner join nivel n on n.id = a.Nivel_id
							where d.id = $division_id
					")->query()->read();
        $titulo = $row["nivel"] . " " . $row["anio"] . " " . $row["division"];
        $rows = Yii::app()->db->createCommand("
						select concat(a.apellido, \", \", a.nombre) as nombre_alumno, a.numero_documento as dni_alumno, pa.nombre as nacionalidad_alumno, 
									 a.fecha_nacimiento as fecha_nacimiento_alumno
							from alumno a
								inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_edu = 1
								inner join alumno_division ad on ad.Alumno_id = a.id and ad.activo
								inner join division d on d.id = ad.Division_id /*((ad.activo or ad.promocionado or ad.egresado)*/
								left join localidad l on l.id = a.localidad_id
								left join pais pa on pa.id = l.Pais_id
							where a.activo = 1 and d.id = $division_id $sexoAnd
							order by $order
					")->queryAll();
        $header = array(array(70, "Alumno"), array(25, "DNI"), array(40, "Nacionalidad"), array(15, "Edad"), array(30, "Fecha Nac."),);

        $pdf = new MyPDF("P", 'mm', 'A4', true, 'UTF-8', false, $header, $titulo, 11);
        $pdf->SetMargins(0, 40, 0, true);
        $pdf->AddPage();
        foreach ($rows as $row) {
          $pdf->renderRow(array(substr($row["nombre_alumno"], 0, 31), $row["dni_alumno"], $row["nacionalidad_alumno"], edad(date("Y-m-d", mystrtotime($row["fecha_nacimiento_alumno"]))), date("d/m/Y", mystrtotime($row["fecha_nacimiento_alumno"])),));
        }
        $pdf->Output();
      } elseif ($_GET["output"] == "excel") {
        $this->renderPartial("/reportes/alumnosResumido1Excel", array("division_id" => $division_id, "sexoAnd" => $sexo));
      }
    } else {
      $this->render("/reportes/genNiAnDi", array("fechas" => false, "reporteNombre" => "alumnosResumido1", "ajax" => false, "excel" => true, "pdf" => true,));

      //				$this->render("/reportes/alumnosResumido1");
    }
  }

  function actionAlumnosResumido2($division_id = null, $sexo = null) {
    $ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
    $sexoAnd = $sexo ? " and a.sexo = \"$sexo\" " : "";
    $direccion = "concat (COALESCE(p.calle, ''), ' ', COALESCE(p.numero, ''), ' ', COALESCE(p.piso, ''), ' ', COALESCE(p.departamento, '')) ";
    if ($division_id) {
      $nivel_id = Helpers::qryScalar("SELECT n.id FROM nivel n
				  INNER JOIN anio a ON n.id = a.Nivel_id
				  INNER JOIN division d ON a.id = d.Anio_id AND d.id = $division_id");
      $order = $nivel_id == 4 ? "a.apellido, a.nombre" : "a.sexo desc, a.apellido, a.nombre";
      $row = Yii::app()->db->createCommand("
                    select n.nombre as nivel, a.nombre as anio, d.nombre as division
                      from division d
                        inner join anio a on a.id = d.Anio_id
                        inner join nivel n on n.id = a.Nivel_id
                      where d.id = $division_id
              ")->query()->read();
      $titulo = $row["nivel"] . " " . $row["anio"] . " " . $row["division"];
      $select = "
				select concat(a.apellido, \", \", a.nombre) as nombre_alumno, 
					a.numero_documento as dni_alumno, pa.nombre as nacionalidad_alumno, 
					a.fecha_nacimiento as fecha_nacimiento_alumno, p.telefono_trabajo,
					concat(p.apellido, \", \", p.nombre) as nombre_tutor, pa2.nombre as nacionalidad_tutor,
					td.nombre_corto as tipo_doc_tutor, p.numero_documento as doc_tutor, 
					p.profesion, $direccion as domicilio_tutor, p.telefono_casa, p.telefono_celular
				from alumno a
					inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_edu = 1
					inner join alumno_division ad on ad.Alumno_id = a.id and not ad.borrado
					inner join division d on d.id = ad.Division_id /*((ad.activo or ad.promocionado or ad.egresado)*/
					left join pariente p on a.Familia_id = p.Familia_id and a.vive_con_id = p.id
					left join localidad l on l.id = a.localidad_id
					left join pais pa on pa.id = l.Pais_id
					left join localidad l2 on l2.id = p.nacionalidad
					left join pais pa2 on pa2.id = l2.Pais_id
					left join tipo_documento td on td.id = p.tipo_documento_id      
				where a.activo = 1 and d.id = $division_id $sexoAnd and ad.Ciclo_id = $ciclo_id
				order by $order
			";
      //vd($select);
      $rows = Yii::app()->db->createCommand($select)->queryAll();
      $header = array(array(63, "Alumno"), array(63, "Domicilio"), array(58, "Teléfonos"));

      $pdf = new MyPDF("P", 'mm', 'A4', true, 'UTF-8', false, $header, $titulo, 11);
      $pdf->SetMargins(0, 40, 0, true);
      $pdf->AddPage();
      foreach ($rows as $row) {
        $pdf->renderRow(array(substr($row["nombre_alumno"], 0, 31), ucwords(strtolower($row["domicilio_tutor"])), substr($row["telefono_casa"], 0, 10) . ($row["telefono_trabajo"] ? "/" . substr($row["telefono_trabajo"], 0, 10) : "") . ($row["telefono_celular"] ? "/" . substr($row["telefono_celular"], 0, 10) : "")));
      }
      $pdf->Output();
    } else {
      $this->render("/reportes/alumnosResumido2");
    }
  }

  function actionAlumnosDniFNac($division_id = null, $sexo = null, $tipo = 1, $output = 'pdf') {
    $ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
    $nivel_id = Helpers::qryScalar("SELECT n.id FROM nivel n
			  INNER JOIN anio a ON n.id = a.Nivel_id
			  INNER JOIN division d ON a.id = d.Anio_id AND d.id = $division_id");
    $order = $nivel_id == 4 ? "a.apellido, a.nombre" : "a.sexo desc, a.apellido, a.nombre";
    $sexoAnd = $sexo ? " and a.sexo = \"$sexo\" " : "";
    if ($division_id) {
      $row = Yii::app()->db->createCommand("
				select n.nombre as nivel, a.nombre as anio, d.nombre as division
					from division d
						inner join anio a on a.id = d.Anio_id
						inner join nivel n on n.id = a.Nivel_id
					where d.id = $division_id
				")->query()->read();
      $titulo = $row["nivel"] . "-" . $row["anio"] . " " . $row["division"];
      $rows = Yii::app()->db->createCommand("
				select concat(a.apellido, \", \", a.nombre) as nombre_alumno, a.numero_documento as dni_alumno,
                            pa.nombre as nacionalidad_alumno, a.fecha_nacimiento as fecha_nacimiento_alumno,
							 concat(p.apellido, \", \", p.nombre) as nombre_tutor, pa2.nombre as nacionalidad_tutor,
							 td.nombre_corto as tipo_doc_tutor, p.numero_documento as doc_tutor, p.profesion,
							 concat(p.calle, \" \", p.numero, \" \", p.piso, \" \", p.departamento) as domicilio_tutor,
							 p.telefono_casa, p.telefono_celular, l.nombre as localidad_alumno
					from alumno a
						inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_edu = 1
						inner join alumno_division ad on ad.Alumno_id = a.id
						inner join division d on d.id = ad.Division_id and ad.activo /*((ad.activo or ad.promocionado or ad.egresado)*/
						left join pariente p on a.Familia_id = p.Familia_id and a.vive_con_id = p.id
						left join localidad l on l.id = a.localidad_id
						left join pais pa on pa.id = l.Pais_id
						left join localidad l2 on l2.id = p.nacionalidad
						left join pais pa2 on pa2.id = l2.Pais_id
						left join tipo_documento td on td.id = p.tipo_documento_id      
					where a.activo = 1 and d.id = $division_id $sexoAnd and ad.Ciclo_id = $ciclo_id
					order by $order
			")->queryAll();
      //$titulo = ($tipo == 1 ? "Nacionalidad" : "Lugar Nacimiento");
      if ($output == 'pdf') {
        $header = array(array(55, "Alumno"), array(62, "DNI"), array(30, "Fecha Nac."), array(30, "Lugar Nac."),);

        $pdf = new MyPDF("P", 'mm', 'A4', true, 'UTF-8', false, $header, $titulo, 11);
        $pdf->SetMargins(0, 40, 0, true);
        $pdf->AddPage();
        foreach ($rows as $row) {
          $pdf->renderRow(array(substr($row["nombre_alumno"], 0, 31), ucwords(strtolower($row["dni_alumno"])), date("d/m/Y", mystrtotime($row["fecha_nacimiento_alumno"])), $nacimiento = $tipo == 1 ? $row["nacionalidad_alumno"] : $row["localidad_alumno"],));
        }
        $pdf->Output();
      } else {
        $cols = array("nombre_alumno" => array("title" => "Alumno"), "dni_alumno" => array("title" => "DNI"), "fecha_nacimiento_alumno" => array("title" => "Fec.nac.", "format" => "date"), "localidad_alumno" => array("title" => "Localidad"));

        Helpers::excel($rows, $cols, 'alumnosDniFecNac', true);
        die;
      }
    } else {
      $this->render("/reportes/alumnosDniFnac");
    }
  }

  function actionAlumnosSolo($division_id = null, $sexo = null, $output = null) {
//			$ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();

    if ($division_id) {
      $ciclo_id = Ciclo::getActivoId();
      $nivel_id = Helpers::qryScalar("SELECT n.id FROM nivel n
       INNER JOIN anio a ON n.id = a.Nivel_id
       INNER JOIN division d ON a.id = d.Anio_id AND d.id = $division_id");
      $order = $nivel_id == 4 ? "a.apellido, a.nombre" : "a.sexo desc, a.apellido, a.nombre";
      $sexoAnd = $sexo ? " and a.sexo = \"$sexo\" " : "";
      $row = Yii::app()->db->createCommand("
        select n.nombre as nivel, a.nombre as anio, d.nombre as division
          from division d
          inner join anio a on a.id = d.Anio_id
          inner join nivel n on n.id = a.Nivel_id
          where d.id = $division_id
        ")->query()->read();
      $titulo = $row["nivel"] . " " . $row["anio"] . " " . $row["division"];
      $subTitulo = " Al " . date("d/m/Y", time());
      $rows = Yii::app()->db->createCommand("
        select concat(a.apellido, \", \", a.nombre) as nombre_alumno, a.numero_documento as dni_alumno, pa.nombre as nacionalidad_alumno, 
           DATE_FORMAT(a.fecha_nacimiento, '%d/%m/%Y') as fecha_nacimiento_alumno, concat(p.apellido, \", \", p.nombre) as nombre_tutor, pa2.nombre as nacionalidad_tutor,
           td.nombre_corto as tipo_doc_tutor, p.numero_documento as doc_tutor, p.profesion, concat(p.calle, \" \", p.numero, \" \", p.piso, \" \", p.departamento) as domicilio_tutor,
           p.telefono_casa, p.telefono_celular
          from alumno a
          inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_edu = 1
          inner join alumno_division ad on ad.Alumno_id = a.id
           inner join division d on d.id = ad.Division_id and ad.activo /*((ad.activo or ad.promocionado or ad.egresado)*/
          left join pariente p on a.Familia_id = p.Familia_id and a.vive_con_id = p.id
          left join localidad l on l.id = a.localidad_id
          left join pais pa on pa.id = l.Pais_id
          left join localidad l2 on l2.id = p.nacionalidad
          left join pais pa2 on pa2.id = l2.Pais_id
          left join tipo_documento td on td.id = p.tipo_documento_id      
          where a.activo = 1 and d.id = $division_id $sexoAnd
          order by $order
					 ")->queryAll();
      $header = array(array(180, "Alumno"),);
      if ($output == "PDF") {
        $pdf = new MyPDF("P", 'mm', 'A4', true, 'UTF-8', false, $header, $titulo, 11, $subTitulo);
        $pdf->SetMargins(0, 40, 0, true);
        $pdf->AddPage();
        foreach ($rows as $row) {
          $pdf->renderRow(array(substr($row["nombre_alumno"], 0, 31),));
        }
        $pdf->Output();
      } else {
        $cols = array(
         "nombre_alumno" => array("title" => "Alumno"),
         "dni_alumno" => array("title" => "DNI"),
         "dni_alumno" => array("title" => "Nacionalidad"),
         "fecha_nacimiento_alumno" => array("title" => "Nacimiento"),
         "nombre_tutor" => array("title" => "Tutor"),
         "nacionalidad_tutor" => array("title" => "Nacionalidad"),
         "tipo_doc_tutor" => array("title" => "Tipo Doc"),
         "doc_tutor" => array("title" => "Número"),
         "profesion" => array("title" => "Profesión"),
         "domicilio_tutor" => array("title" => "Domicilio"),
         "telefono_casa" => array("title" => "Tel.Casa"),
         "telefono_celular" => array("title" => "Tel.Celular"),
        );
        Helpers::excel($rows, $cols, 'soloAlumnos', true);
      }

    } else {
      $this->render("/reportes/alumnosSolo");
    }
  }
  function actionAlumnosNombreApellido($division_id = null, $sexo = null, $output = null) {
//			$ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();

    if ($division_id) {
      $ciclo_id = Ciclo::getActivoId();
      $nivel_id = Helpers::qryScalar("SELECT n.id FROM nivel n
       INNER JOIN anio a ON n.id = a.Nivel_id
       INNER JOIN division d ON a.id = d.Anio_id AND d.id = $division_id");
      $order = $nivel_id == 4 ? "a.apellido, a.nombre" : "a.sexo desc, a.apellido, a.nombre";
      $sexoAnd = $sexo ? " and a.sexo = \"$sexo\" " : "";
      $row = Yii::app()->db->createCommand("
        select n.nombre as nivel, a.nombre as anio, d.nombre as division
          from division d
          inner join anio a on a.id = d.Anio_id
          inner join nivel n on n.id = a.Nivel_id
          where d.id = $division_id
        ")->query()->read();
      $titulo = $row["nivel"] . " " . $row["anio"] . " " . $row["division"];
      $subTitulo = " Al " . date("d/m/Y", time());
      $rows = Yii::app()->db->createCommand("
        select concat(a.apellido, \", \", a.nombre) as nombre_alumno, a.numero_documento as dni_alumno, pa.nombre as nacionalidad_alumno, 
           DATE_FORMAT(a.fecha_nacimiento, '%d/%m/%Y') as fecha_nacimiento_alumno, concat(p.apellido, \", \", p.nombre) as nombre_tutor, pa2.nombre as nacionalidad_tutor,
           td.nombre_corto as tipo_doc_tutor, p.numero_documento as doc_tutor, p.profesion, concat(p.calle, \" \", p.numero, \" \", p.piso, \" \", p.departamento) as domicilio_tutor,
           p.telefono_casa, p.telefono_celular
          from alumno a
          inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_edu = 1
          inner join alumno_division ad on ad.Alumno_id = a.id
           inner join division d on d.id = ad.Division_id and ad.activo /*((ad.activo or ad.promocionado or ad.egresado)*/
          left join pariente p on a.Familia_id = p.Familia_id and a.vive_con_id = p.id
          left join localidad l on l.id = a.localidad_id
          left join pais pa on pa.id = l.Pais_id
          left join localidad l2 on l2.id = p.nacionalidad
          left join pais pa2 on pa2.id = l2.Pais_id
          left join tipo_documento td on td.id = p.tipo_documento_id      
          where a.activo = 1 and d.id = $division_id $sexoAnd
          order by $order
					 ")->queryAll();
      $header = array(array(180, "Alumno"),);
      if ($output == "PDF") {
        $pdf = new MyPDF("P", 'mm', 'A4', true, 'UTF-8', false, $header, $titulo, 11, $subTitulo);
        $pdf->SetMargins(0, 40, 0, true);
        $pdf->AddPage();
        foreach ($rows as $row) {
          $pdf->renderRow(array(substr($row["nombre_alumno"], 0, 31),));
        }
        $pdf->Output();
      } else {
        $cols = array(
         "nombre_alumno" => array("title" => "Alumno"),
        );
        Helpers::excel($rows, $cols, 'soloAlumnos', true);
      }

    } else {
      $this->render("/reportes/alumnosNombreApellido");
    }
  }


}

function edad($fecha_nacimiento) {
  list($anio, $mes, $dia) = explode("-", $fecha_nacimiento);
  $mes_actual = date("m");
  $dia_actual = date("d");
  //Toma la edad al 30/06
  if (($mes_actual > 6)) {
    $mes_actual = 6;
    $dia_actual = 30;
  }
  $anio_dif = date("Y") - $anio;
  $mes_dif = $mes_actual - $mes;
  $dia_dif = $dia_actual - $dia;
  if ($mes_dif < 0) {
    $anio_dif--;
  }

  return $anio_dif;
}

?>
