<?php

	class ReportesNuevoController extends Controller {

	public $layout = "bootstrap";

		public function actionRep1() {
			$this->pageTitle = $title = "Informe de prueba 1";
			Yii::app()->libs->add("knockout");
			/*
			 * render del informe
			 */
			$this->render("repGenView", array(
					"pdf" => true,
					"html" => true,
					"excel" => true,
					"reporte" => $this->createUrl("reportesNuevo/rep1Informe"),
					"title" => $title,
					"params" => array(
							"nivel_id" => array("required"=>true, "value" => 1),
							"anio_id" => array("required"=>true),
							"division_id" => array("required"=>true),
					)
			));
		}

		public function actionRep1Informe() {
			$ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
			$output = filter_input(INPUT_GET, "output");
			$division_id = filter_input(INPUT_GET, "division_id");
			$dataSelect = "
				SELECT /*n.nombre as nivel, a1.nombre as anio, d.nombre as division,*/ a.matricula, 
							concat(a.apellido, ', ', a.nombre) as nombre, a.email
					from alumno a
						inner join alumno_division ad on ad.alumno_id = a.id and ad.activo /*((ad.activo or ad.promocionado or ad.egresado)*/
						inner join division d on d.id = ad.division_id and d.id = $division_id
						inner join anio a1 on a1.id = d.anio_id
						inner join nivel n on n.id = a1.nivel_id
					where ad.Ciclo_id = $ciclo_id
			";
			$data = Helpers::qryAll($dataSelect);
			$colModel = $this->createColModel($data);
			$colModel["nombre"]["css"] = "";
			$colModel["matricula"]["css"] = "width:20mm;text-align:left";
//			$colModel["menu_id"]["visible"] = false;
//			$colModel["texto"]["css"] = "width:50mm;color:red";
//			$colModel["children"]["css"] = "width:19mm;color:blue;font-size:13px;text-align:right";
			$this->renderPartial("repGen", array("data" => $data, "output" => $output, "colModel"=>$colModel));
		}
		
		public function pdf($colModel, $html, $header) {
			$this->renderPartial("repGenPDF",array("colModel"=>$colModel, "html"=>$html, "header"=>$header));
		}

		public function createColModel($data) {
			$colModel = array();
			foreach (array_keys($data[0]) as $fld) {
				$colModel[$fld] = array("title" => ucwords($fld));
			}
			return $colModel;
		}

		public function excel($data, $colModel, $filename) {
			$this->renderPartial("repGenEXCEL",array("data"=>$data,"colModel"=>$colModel, "filename"=>$filename));
		}

		public function actionGetNiveles(){
			$niveles = Helpers::qryAll("select * from nivel n where n.activo = 1 and n.egresado = 0 order by orden");
			echo json_encode($niveles);
		}
		
		public function actionGetAnios($parent_id){
			$anios = Helpers::qryAll("select * from anio a where a.nivel_id = $parent_id and a.activo = 1 and a.egresados = 0 order by orden");
			echo json_encode($anios);
		}
		
		public function actionGetDivisiones($parent_id){
			$divisiones = Helpers::qryAll("select * from division d where d.anio_id = $parent_id and d.activo = 1 and d.egresados = 0 order by orden");
			echo json_encode($divisiones);
		}
		
	}
	