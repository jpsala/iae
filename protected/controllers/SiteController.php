<?php

class SiteController extends Controller {

  /**
   * Declares class-based actions.
   */
  public function actions() {
    return array(
        // captcha action renders the CAPTCHA image displayed on the contact page
        'captcha' => array(
            'class' => 'CCaptchaAction',
            'backColor' => 0xFFFFFF,
        ),
        // page action renders "static" pages stored under 'protected/views/site/pages'
        // They can be accessed via: index.php?r=site/page&view=FileName
        'page' => array(
            'class' => 'CViewAction',
        ),
    );
  }

  /**
   * This is the default 'index' action that is invoked
   * when an action is not explicitly requested by users.
   */
  public function actionIndex() {
    $this->pageTitle = "Sistema de Gestión Escolar";
    // renders the view file 'protected/views/site/index.php'
    // using the default layout 'protected/views/layouts/main.php'
    $this->render('index');
  }

  /**
   * This is the action to handle external exceptions.
   */
  public function actionError() {
    $this->layout = "nuevo";
    if ($error = Yii::app()->errorHandler) {
      if (Yii::app()->request->isAjaxRequest)
        echo $error['message'];
      else
        $this->render('error', array("error"=>$error));
    }
  }

  /**
   * Displays the contact page
   */
  public function actionContact() {
    $model = new ContactForm;
    if (isset($_POST['ContactForm'])) {
      $model->attributes = $_POST['ContactForm'];
      if ($model->validate()) {
        $headers = "From: {$model->email}\r\nReply-To: {$model->email}";
        mail(Yii::app()->params['adminEmail'], $model->subject, $model->body, $headers);
        Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
        $this->refresh();
      }
    }
    $this->render('contact', array('model' => $model));
  }

  /**
   * Displays the login page
   */
  public function actionLogin() {
    $model = new LoginForm;

    // if it is ajax validation request
    if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }

    // collect user input data
    if (isset($_POST['LoginForm'])) {
      $model->attributes = $_POST['LoginForm'];
      // validate user input and redirect to the previous page if valid
      if ($model->validate() && $model->login())
        if (Yii::app()->user->model->cambiar_password) {
          $this->actionCambioPassword();
          die;
        }
      $this->redirect(Yii::app()->user->returnUrl);
    }
    // display the login form
    $this->render('login', array('model' => $model));
  }

  public function actionCambioPassword() {
    $error = "";
    if (isset($_POST["submit"])) {
      $pa = isset($_POST["password-actual"]) ? $_POST["password-actual"] : "";
      $pn = isset($_POST["password"]) ? $_POST["password"] : "";
      $pv = isset($_POST["password-verificacion"]) ? $_POST["password-verificacion"] : "";
      $pass = Yii::app()->user->model->password;
      if(!$pa or !$pn or !$pv){
        $error.= "Debe ingresar todos los datos<br/>";
      }else if ($pass !== $pa and md5($pa) !== $pass) {
        $error.= "La contraseña es incorrecta<br/>";
      }else if ($pa == $pn) {
        $error.="La nueva contraseña debe ser distinta de la actual<br/>";
      } else if ($pn == $pv and $pn) {
        $user = User::model()->findByPk(Yii::app()->user->model->id);
        $user->password = md5($pn);
        $user->password_repeat = md5($pv);
        $user->cambiar_password = 0;
        if (!$user->save()) {
          ve($user->errors);
        } else {
          $this->redirect($this->createUrl("/site"));
        }
      } else {
        $error.="El nuevo Password y la verificación deben ser iguales<br/>";
      }
    }
    $this->render("cambioPassword", array("error" => $error));
  }

  /**
   * Logs out the current user and redirect to homepage.
   */
  public function actionLogout() {
    Yii::app()->user->logout();
    $this->redirect(Yii::app()->homeUrl);
  }

}