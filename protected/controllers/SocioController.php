<?php

class SocioController extends Controller {

  public $layout = "column1";

  public function actionGeneraSocioIds(){

    $tr = Yii::app()->db->beginTransaction();
    $users = Helpers::qryAll("
      select u.id from user u
        left join socio s on s.empleado_id = u.id
      where s.id is null
    ");
    foreach($users as $u){
      $user_id = $u['id'];
      ve(Helpers::qryExec("
        insert into socio(empleado_id) values($user_id)
      "));
    }
    $parientes = Helpers::qryAll("
      select u.id from pariente u
        left join socio s on s.pariente_id = u.id
      where s.id is null
    ");
    foreach($parientes as $u){
      $pariente_id = $u['id'];
      ve(Helpers::qryExec("
        insert into socio(pariente_id) values($pariente_id)
      "));
    }
    $tr->commit();
  }

  public function actionSociosAc() {
    $term = $_GET['term'];
    $comprob_id = isset($_GET['comprob_id']) ? $_GET['comprob_id'] : null;
    if (substr($term, 0, 1) == "#") {
      $dni = substr($term, 1);
      // $socio = Helpers::qryScalar('select')
    }
    if ($comprob_id and substr($term, 0, 1) == ".") {
      $numero = substr($term, 1);
      list($doc_id, $comprob, $socio, $retencion) = Helpers::qryDataRow("
                select d.id, c.nombre as comprob, case when s.alumno_id then concat(a.apellido, \", \", a.nombre) else p.razon_social end as socio, p.retencion
                    from doc d
                        left join socio s on s.id = d.socio_id
                        left join alumno a on a.id = s.alumno_id
                        left join proveedor p on a.id = s.proveedor_id
                        inner join talonario t on t.id = d.talonario_id
                        inner join comprob c on c.id = t.comprob_id
                where d.numero = $numero and t.comprob_id = $comprob_id and d.anulado = 0
            ");
      if ($comprob) {
        $ret[] = array(
          "id" => $doc_id,
          'label' => "$comprob $numero, $socio",
          "comprob_id" => $comprob_id,
          "doc_id" => $doc_id
        );
        echo json_encode($ret);
      }
    } else {
      $socioTipo = $_GET["socioTipo"] == "cliente" ? "alumno" : $_GET["socioTipo"];
      $criteria = new CDbCriteria();
      $criteria->limit = 20;
      $esAlumno = in_array($socioTipo, array("alumno", "cliente"));
      $campo = $esAlumno ? "matricula" : "codigo";
      $campoApellido = $esAlumno ? "apellido" : "razon_social";
      $campoNombre = $esAlumno ? "Nombre" : "razon_social";
      if (isset($dni)) {
        $criteria->addCondition("numero_documento like  \"%$dni%\"");
      } elseif (is_numeric($term)) {
        $criteria->addCondition("$socioTipo.$campo = \"$term\"");
      } else {
        $nombres = explode(",", $term);
        $apellido = trim($nombres[0]);
        $criteria->addCondition("$socioTipo.$campoApellido like \"$apellido%\"");
        if (count($nombres) > 1) {
          $nombre = trim($nombres[1]);
          $criteria->addCondition("$socioTipo.$campoNombre like \"$nombre%\"");
        }
        $criteria->order = "$campoApellido asc, $campoNombre asc";
      }
      $criteria->with = array("alumno", "proveedor");
      if ($esAlumno) {
        // $criteria->addCondition("alumno.activo = 1");
      }
      $socios = Socio::model()->with($socioTipo)->findAll($criteria);
      $ret = array();
      foreach ($socios as $socio) {
        if ($socioTipo == "alumno") {
          $qry = "
                    select concat(n.nombre,  '  ',  an.nombre, '  ', d.nombre) as curso, a.numero_documento,
                        a.porc_beca as beca, a.activo, saldo(s.id,null) as saldo,
                        a.obs_beca, a.*, f.obs as obs_familia,
                        concat(p.apellido, ', ', p.nombre) as encargado_pago,
                        td.nombre as encargado_tipo_doc, p.numero_documento as encargado_numero_doc
                    from alumno a
                            inner join socio s on s.alumno_id = a.id
			              				left join familia f on f.id = a.familia_id
                            left join alumno_division ad on ad.alumno_id = a.id and ad.activo = 1
                            left join division d on d.id = ad.division_id
                            left join anio an on an.id = d.anio_id
                            left join nivel n on n.id = an.nivel_id
                            left join pariente p on p.id = a.encargado_pago_id
                            left join tipo_documento td on td.id = p.tipo_documento_id
                        where a.id = $socio->Alumno_id
                    ";
          $data = Helpers::qry($qry
          );
          $curso = $data["curso"];
          $beca = $data["beca"];
          $obs_cobranza = $data["obs_familia"];
          $obs_beca = $data["obs_beca"];
          $activo = $data["activo"];
          $saldo = $data["saldo"];
          $responsable_pago = $data["encargado_pago"];
          $responsable_pago_tipo_doc = $data["encargado_tipo_doc"];
          $responsable_pago_numero_doc = $data["encargado_numero_doc"];
          $numero_documento = $data["numero_documento"];
        } else {
          $curso = "";
          $beca = "";
          $obs_cobranza = "";
          $obs_beca = "";
          $activo = 1;
          $saldo = "";
          $responsable_pago = "";
          $responsable_pago_tipo_doc = "";
          $responsable_pago_numero_doc = "";
          $numero_documento = "";
        }
        $ret[$socio->id] = array(
          "id" => $socio->id,
          'label' => (($socioTipo == "alumno") ? $socio->alumno->activo . "-" : "") . "(" . $socio->$socioTipo->$campo . ") " . $socio->nombre . (isset($dni) ? ' / dni: '.$numero_documento:''),
          'value' => $socio->nombre,
          "codigo" => $socio->$socioTipo->$campo,
          "curso" => $curso,
          "beca" => $beca,
          "obs_cobranza" => $obs_cobranza,
          "obs_beca" => $obs_beca,
          "activo" => $activo ? "" : "Inactivo",
          "saldo" => $saldo,
          "responsablePago" => $responsable_pago,
          "responsablePagoTipoDoc" => $responsable_pago_tipo_doc,
          "responsablePagoNumeroDoc" => $responsable_pago_numero_doc,
        );
        if(isset($dni)) {
          $ret[$socio->id]["numero_documento"] = $numero_documento;
        }
        if ($socioTipo === 'proveedor') {
          $ret[$socio->id]['retencion'] = $socio->proveedor->retencion;
        }
      }
      echo json_encode($ret);
    }
  }

  public function actionRecalculaSaldos() {
    Socio::recalculaSaldos();
  }

  public function actionChequeaSaldos() {
    $ret = array();
    $socios = Helpers::qryAllObj("
            SELECT p.id, p.codigo,
            CASE WHEN s.Proveedor_id THEN p.razon_social ELSE a.matricula END AS razon_social,
            s.*, saldo(s.id,NULL) AS saldo_bien
                FROM socio s
                    LEFT JOIN proveedor p ON p.id = s.proveedor_id
                    LEFT JOIN alumno a ON a.id = s.alumno_id
               /* where a.matricula = 2885*/ where s.tipo in ('A')
         ");
    foreach ($socios as $s) {
      $saldo_socio = 0;
      $suma_saldos = 0;
      $docs = Helpers::qryAllObj("
                select d.*, c.nombre
                    from doc d
                        inner join talonario t on t.id = d.talonario_id
                        inner join comprob c on c.id = t.comprob_id
                    where d.socio_id = $s->id and c.signo_cc = 1 and d.anulado = 0 and d.activo = 1
                     order by d.fecha_valor
            ");
      if ($docs) {
        $ret[$s->razon_social] = new stdClass();
        $ret[$s->razon_social]->ok = true;
        $ret[$s->razon_social]->codigo = $s->codigo;
        $ret[$s->razon_social]->id = $s->id;
      }
      foreach ($docs as $doc) {
        $suma_saldos += $doc->saldo;
        $saldo_socio += $doc->total * 1;
        $d = new stdClass();
        $d->id = $doc->id;
        $d->total = $doc->total * 1;
        $d->comprob = $doc->nombre;
        $d->numero = $doc->numero;
        $d->fecha = $doc->fecha_valor;
        $d->saldo = $doc->saldo * 1;
        $appls = Helpers::qryAllObj("
                        select d.*, c.nombre, a.importe as importe_aplicacion
                            from doc_apl a
                                inner join doc d on d.id = a.doc_id_origen and a.doc_id_destino = $d->id
                                inner join talonario t on t.id = d.talonario_id
                                inner join comprob c on c.id = t.comprob_id
                            where d.anulado = 0 and d.activo = 1
                            order by d.fecha_valor
                    ");
        $d->appls = array();
        $total_app = 0;
        foreach ($appls as $appl) {
          $a = new stdClass();
          $total_app += $appl->importe_aplicacion;
          $saldo_socio -= $appl->importe_aplicacion;
          $a->sucursal = $appl->sucursal;
          $a->comprob = $appl->nombre;
          $a->numero = $appl->numero;
          $a->fecha = $appl->fecha_valor;
          $a->total = $appl->total;
          $a->saldo = $appl->saldo;
          $a->importe_aplicacion = $appl->importe_aplicacion;
          $d->appls[] = $a;
        }
        if (count($appls) > 1) {
          //vd($d);
        }
        $total_app = $total_app;
        $d->saldo_calc = ($d->total - $d->saldo) - $total_app;
        $d->ok = (abs($d->saldo_calc) < .0001);
        if (!$d->ok) {
          //vd($d);
        }
        $ret[$s->razon_social]->docs[$doc->id] = $d;
        //                $ret[$s->razon_social]->ok = !$d->ok ? false : $ret[$s->razon_social]->ok;
      }
      if (isset($ret[$s->razon_social]->docs)) {
        $docs_pend = Helpers::qryAllObj("
                select d.*, c.nombre
                    from doc d
                        inner join talonario t on t.id = d.talonario_id
                        inner join comprob c on c.id = t.comprob_id
                    where d.socio_id = $s->id and c.signo_cc = -1 and d.anulado = 0 and d.activo = 1 and d.saldo <> 0
                     order by d.fecha_valor
            ");
        //vd($docs_pend);
        $ret[$s->razon_social]->docsPend = array();
        //vd($ret[$s->razon_social]);
        foreach ($docs_pend as $doc) {
          $suma_saldos -= $doc->saldo;
          $saldo_socio -= $doc->saldo;
          $d = new stdClass();
          $d->id = $doc->id;
          $d->total = $doc->total * 1;
          $d->comprob = $doc->nombre;
          $d->numero = $doc->numero;
          $d->fecha = $doc->fecha_valor;
          $d->saldo = $doc->saldo * 1;
          $appls = Helpers::qryAllObj("
                        select d.*, c.nombre, a.importe as importe_aplicacion
                            from doc_apl a
                                inner join doc d on d.id = a.doc_id_destino and a.doc_id_origen = $d->id
                                inner join talonario t on t.id = d.talonario_id
                                inner join comprob c on c.id = t.comprob_id
                            where d.anulado = 0 and d.activo = 1
                            order by d.fecha_valor
                    ");
          $d->appls = array();
          $a = new stdClass();
          $total_app = 0;
          foreach ($appls as $appl) {
            $total_app += $appl->importe_aplicacion;
          }
          $d->ok = !(($d->total + $d->saldo + $total_app) == 0);
          if (!$d->ok) {
            //vd($d->total,$d->saldo,$total_app,$d);
          }
          $ret[$s->razon_social]->docsPend[] = $d;
        }
        $ret[$s->razon_social]->saldo_calc = $saldo_socio;
        $ret[$s->razon_social]->saldo_bien = $s->saldo_bien * 1;
        $ret[$s->razon_social]->suma_saldos = $suma_saldos;
        if (($s->saldo_bien * 1) !== $saldo_socio or $saldo_socio !== $suma_saldos) {

        }
        //$ret[$s->razon_social]->ok = !$ret[$s->razon_social]->ok ? false : ($s->saldo*1) == $saldo_socio and $saldo_socio == $suma_saldos and ($s->saldo*1) == $suma_saldos;
        if (abs($s->saldo_bien - $suma_saldos) > 1) {
          //ve($s->razon_social,$s->saldo_bien*1,$suma_saldos, $s->saldo_bien*1-$suma_saldos);
          $ret[$s->razon_social]->ok = false;
        }
      }
    }
    //die;
    // vd2($ret);
    $this->render("chequeaSaldos", array("rows" => $ret));
  }

  public function actionCambiaRetencion($id, $retencion) {
    $ret = new stdClass();
    $ahora = date('Y-m-d');
    Helpers::qryExec("
                        update proveedor set retencion = $retencion,
                                             retencion_error = 'Actualización Manual',
                                             retencion_ultima_actualizacion = '$ahora'
                          where id = $id
                    ");
    $ret->error = 'Actualización Manual';
    $ret->ahora = date('d/m/Y');
    echo json_encode($ret);
  }

  public function actionCambiaCuit($id, $cuit) {
    $ret = new stdClass();
    $ahora = date('Y-m-d');
    Helpers::qryExec("
                        update proveedor set cuit = '$cuit',
                                             retencion_ultima_actualizacion = '$ahora'
                          where id = $id
                    ");
    $ret->ahora = date('d/m/Y');
    echo json_encode($ret);
  }

  public function actionActualizaRetenciones() {
    $data = array();
    $proveedores = Helpers::qryAll('
              SELECT id, cuit, razon_social FROM proveedor p
            ', array(), Yii::app()->db);
    $tr = Yii::app()->db->beginTransaction();
    foreach ($proveedores as $p) {
      $dataProveedor = new stdClass();
      $dataProveedor->id = $p['id'];
      //$p['cuit'] .  $p['razon_social'] . ':' . .'<br/>';
      $retencionData = $this->getRetencion(str_replace('-', '', $p['cuit']));
      //if(!$p['cuit']) {
      //}
      $ahora = date('Y-m-d');
      if ($retencionData->error) {
        $dataProveedor->error = $retencionData->error;
        $dataProveedor->retencion = null;
        $id = $p['id'];
        Helpers::qryExec("
                        update proveedor
                          set retencion_error = '$retencionData->error',
                              retencion = '0',
                              retencion_ultima_actualizacion = '$ahora'
                          where id = $id
                    ", array(), Yii::app()->db);
      } else {
        $dataProveedor->error = null;
        $retencion = str_replace(',', '.', $retencionData->retencion);
        $id = $p['id'];
        $dataProveedor->retencion = $retencion;
        Helpers::qryExec("
                        update proveedor set retencion = $retencion,
                                             retencion_error = '',
                                             retencion_ultima_actualizacion = '$ahora'
                          where id = $id
                    ", array(), Yii::app()->db);
      }
      $data[] = $dataProveedor;
//				vd($dataProveedor);
    }
    $q = Helpers::qryAll("SELECT * FROM proveedor", array(), Yii::app()->db);
//			vd($q);
    $tr->commit();
    echo json_encode($data);
  }

  public function actionProveedores() {
    $proveedores = Helpers::qryAll('
              SELECT id, cuit, razon_social, retencion, retencion_error,
                     DATE_FORMAT(retencion_ultima_actualizacion,"%e/%m/%Y") AS retencion_ultima_actualizacion
                FROM proveedor p
                ORDER BY p.razon_social
            ', array(), Yii::app()->db);
    echo json_encode($proveedores);
  }

  public function actionRetenciones() {
    // vd2(curl_version());die;
    $this->layout = 'nuevo';
    $this->render('retenciones');
  }

  public function actionGetRetencion($cuit){
    $coeficiente = $this->getRetencion($cuit);
    echo json_encode($coeficiente);
  }

  private function getRetencion($cuit) {

    $url = "https://dfe.arba.gov.ar/DomicilioElectronico/SeguridadCliente/dfeServicioConsulta.do";
    $retencionData = new stdClass();
    $retencionData->error = null;
    try{
      $r = curl_init($url);
    } catch(Exception $e) {
      ve2('Error en getRetencion, curl_init');
      vd2($e);
    }
    curl_setopt($r, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($r, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($r, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($r, CURLOPT_POST, 1);
    $a_date = date('Y-m-d');
    $desde = date("Ym", strtotime($a_date)) . '01';
    $hasta = date("Ymt", strtotime($a_date));
    $fileContent = "<?xml version = \"1.0\" encoding = \"ISO-8859-1\"?>
                <CONSULTA-ALICUOTA>
                <fechaDesde>$desde</fechaDesde>
                <fechaHasta>$hasta</fechaHasta>
                <cantidadContribuyentes>1</cantidadContribuyentes>
                <contribuyentes class=\"list\">
                    <contribuyente>
                    <cuitContribuyente>$cuit</cuitContribuyente>
                    </contribuyente>
                </contribuyentes>
                </CONSULTA-ALICUOTA>";
                /*TODO: Quitar */
    if (!file_put_contents('send.xml', $fileContent, 0)) {
      vd('Error creando el archivo send.xml');
    }
    $md5 = md5_file(realpath('send.xml'));
    $fileName = 'DFEServicioConsulta_' . $md5 . '.xml';
    //$f     = curl_file_create(realpath('xml'), 'application/octet-stream', $fileName);
    $f = new CurlFile(realpath('send.xml'), 'application/octet-stream', $fileName);
    $rf = fopen("/tmp/resultFile.xml", "w");
    // $rf = fopen("c:\\resultFile.xml", "w");
    curl_setopt($r, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($r, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($r, CURLOPT_IGNORE_CONTENT_LENGTH, 0);
    curl_setopt($r, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($r, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($r, CURLOPT_VERBOSE, false);
    curl_setopt($r, CURLOPT_HEADER, false);
    curl_setopt($r, CURLOPT_FILE, $rf);
    //curl_setopt($r, CURLOPT_HTTPHEADER, array('Content-Type:multipart/form-data'));
    curl_setopt(
        $r, CURLOPT_POSTFIELDS, array(
      'user' => '30639390469',
      'password' => '297900',
      'file' => $f
    ));
    curl_exec($r);
    if (curl_error($r)) {
      $retencionData->error = curl_error($r);
    } else {
      fclose($rf);
      $xml = simplexml_load_file('/tmp/resultFile.xml');
      $error = $xml->codigoError;
      if ($error) {
        //echo 'error '.$cuit . 'error: '.$error. '<br/>';
        $retencionData->error = (string) $xml->mensajeError;
        $retencionData->error = str_replace('<![CDATA[', '', $retencionData->error);
        $retencionData->error = str_replace(']]/>', '', $retencionData->error);
//                    if((string) $xml->codigoError == '2'){
//                    }
      } else {
        $retencionData->retencion = str_replace(',', '.', $xml->contribuyentes[0]->contribuyente->alicuotaRetencion);
        if (!$retencionData->retencion or $retencionData->retencion * 1 == 0) {
          $retencionData->error = 'retencion:' . $retencionData->retencion;
        }
      }
    }
    curl_close($r);

    return $retencionData;
  }

  public function actionReAplica() {
    $this->layout = 'nuevo';
    $this->render('reAplica');
  }

  public function actionDesAplicaAjax($matriculasStr) {
    $ret = new stdClass();
    $ret->status = 'ok';
    $matriculas = explode(',', $matriculasStr);
    $ret->ret = array();
    foreach ($matriculas as $m) {
      $tr = Yii::app()->db->beginTransaction();

      $socio_id = Helpers::qryScalar("
                select s.id
                    from socio s
                    inner join alumno a on a.id = s.Alumno_id
                where a.matricula = $m");
      //vd($socio_id);
      $ret->res[$m] = Helpers::qryExec("call borra_ap($socio_id)");
      $tr->commit();
    }
  }

  public function actionReAplicaAjax($matriculasStr) {
    $ret = new stdClass();
    $ret->status = 'ok';
    $matriculas = explode(',', $matriculasStr);
    $ret->ret = array();
    foreach ($matriculas as $m) {
      $tr = Yii::app()->db->beginTransaction();

      $socio_id = Helpers::qryScalar("
                select s.id
                    from socio s
                    inner join alumno a on a.id = s.Alumno_id
                where a.matricula = $m");
      //vd($socio_id);
      $ret->res[$m] = Helpers::qryExec("call borra_ap($socio_id)");
//      $tr->commit();
//      die;
      $docsCredito = Helpers::qryAll("
        select d.id, d.saldo
        from socio s
          inner join doc d on d.Socio_id = s.id
          inner join talonario t on t.id = d.talonario_id
          inner join comprob c on c.id = t.comprob_id
        where s.id = $socio_id and c.signo_cc = -1 and
          d.anulado <> 1 and d.activo = 1 and
          d.saldo > 0
        order by d.fecha_valor
        ");
      $docsDebito = Helpers::qryAll("
        select d.id, d.saldo
        from socio s
          inner join doc d on d.Socio_id = s.id
          inner join talonario t on t.id = d.talonario_id
          inner join comprob c on c.id = t.comprob_id
        where s.id = $socio_id and c.signo_cc = 1 and
          d.anulado <> 1 and d.activo = 1 and
          d.saldo > 0
        order by d.fecha_valor
        ");
      $ret->docsCredito = $docsCredito;
      $ret->docsDebito = $docsDebito;
      foreach ($docsCredito as $kc => $dc) {
//        while ($dc["saldo"] > 0) {
        foreach ($docsDebito as $kd => $dd) {
          if ($dc["saldo"] === 0 or $dd["saldo"] === 0) {
            continue;
          }
          if ($dc["saldo"] >= $dd["saldo"]) {
            $dc["saldo"] -= $dd["saldo"];
            $docsCredito[$kc]["saldo"] -= $dd["saldo"];
            $docsDebito[$kd]["saldo"] = 0;
            $dd["saldo"] = 0;
            $importe = $dd["saldo"];
          } else {
            $dd["saldo"] -= $dc["saldo"];
            $docsDebito[$kd]["saldo"] -= $dc["saldo"];
            $docsCredito[$kc]["saldo"] = 0;
            $dc["saldo"] = 0;
            $importe = $dc["saldo"];
          }
          Helpers::qryExec("
            INSERT INTO
            doc_apl(Doc_id_origen, doc_id_destino, importe)
            VALUES (:origen, :destino, :importe)
            ",array(
              'origen'=>$dc["id"],
              'destino'=>$dd["id"],
              'importe'=>$importe,

            ));
          Helpers::qryExec("
            update doc set saldo = " . $dc["saldo"] . "
              where id = " . $dc["id"]
          );
          Helpers::qryExec("
            update doc set saldo = " . $dd["saldo"] . "
              where id = " . $dd["id"]
          );
        }
        //      }
      }
      $tr->commit();
    }
    /*
      delete a.* from doc_apl a
      inner join doc d on d.id = a.Doc_id_origen
      inner join socio s on s.id = d.Socio_id and s.id = socio_id;
      update doc d
      inner join socio s on s.id =d.Socio_id and s.id = socio_id
      inner join talonario t on t.id = d.talonario_id
      inner join comprob c on c.id = t.comprob_id and c.signo_cc = 1

      set d.saldo = d.total;
      update doc d
      inner join socio s on s.id =d.Socio_id  and s.id = socio_id
      inner join talonario t on t.id = d.talonario_id
      inner join comprob c on c.id = t.comprob_id and c.signo_cc = -1
      set d.saldo = d.total;
     */
    //Helpers::qryExec($qry);
    echo json_encode($ret);
  }

  public function actionChequeaCta(){
    $this->render('chequeaCta');
  }


}
