<?php

class TalonarioController extends Controller
{

  public $layout = "nuevo";

  public function actionIndex()
  {
    $talonarios = Helpers::qryAll("
            SELECT t.id AS talonario_id, t.letra, t.numero, t.punto_venta_numero,
                   p.nombre AS puesto_trabajo_nombre
                FROM talonario t
                    INNER JOIN comprob c ON c.id = t.comprob_id
                    INNER JOIN puesto_trabajo p ON p.punto_venta_numero = t.punto_venta_numero
                ORDER BY p.nombre, c.compra_venta, c.nombre");
    $comprobantes = Helpers::qryAll("
            SELECT *
                FROM comprob c");
    $this->render("index", array("talonarios" => $talonarios, "comprobantes"=>$comprobantes));
  }

  public function actionNumeracion()
  {
    $data = Helpers::qryAll("
            SELECT t.id AS talonario_id, t.letra, t.numero, t.punto_venta_numero, p.nombre AS puesto,
                        CASE
                            WHEN c.compra_venta = 'V' THEN concat(c.nombre, ' ', 'venta')
                            ELSE concat(c.nombre, ' ', 'compra')
                        END AS comprobante
                FROM talonario t
                    INNER JOIN comprob c ON c.id = t.comprob_id
                    INNER JOIN puesto_trabajo p ON p.punto_venta_numero = t.punto_venta_numero
                 WHERE t.numeracion_manual = 0
                ORDER BY p.nombre, c.compra_venta, c.nombre");
    $this->render("numeracion", array("data" => $data));
  }

  public function actionIndexGraba($talonario_id)
  {
//echo $_GET["talonario_id"];return;
    $t         = Talonario::model()->find("id=$talonario_id");
    $t->numero = $_GET["numero"];
    if (!$t->save()) {
      vd($t->errors);
    }
  }

  public function actionAlta()
  {
    $this->render("alta");
  }


}

?>
