<?php

class TestController extends Controller
{

    public function filters()
    {
        return array(
            //            'accessControl', // perform access control for CRUD operations
            //            'rights', // perform access control for CRUD operations
        );
    }

    public function actionAsr()
    {
    }

    public function actionTest()
    {
      $this->render('test');
    }

    public function actionSsh()
    {
        $connection = ssh2_connect('iaeg.dyndns.org', 22, array('hostkey'=>'ssh-rsa'));
        if (ssh2_auth_pubkey_file(
            $connection,
            'jpsala',
            '/prg/iae/id_rsa.pub',
            '/prg/iae/id_rsa',
            ''
        )) {
            echo "Public Key Authentication Successful\n";
        } else {
            die('Public Key Authentication Failed');
        }
        die;
    }
    public function actionPhpInfo()
    {
        phpinfo();
    }
    public function actionIftop()
    {
        $iface = gethostbyaddr("127.0.0.1") === 'localhost.jp' ? 'wifi2': 'em2';
        ;
        $salida = shell_exec("sudo iftop -i $iface -o source -b -B -s 1 -t -f \"not host 192.168.2.254\"");
        $linesRaw = preg_split("[\r|\n]", trim($salida));
        vd2($linesRaw);
    }

    public function actionSdoVto()
    {
        $this->render("sdoVto");
    }

    public function actionSelect()
    {
        $this->render('select', array(
            'ejemplo' => 'ejemplo1', 'users' => User::model()->findAll()
        ));
    }

    public function actionUno()
    {
        $user_id = Yii::app()->user->id;
        /* @var $au CDbAuthManager */
        $au = Yii::app()->getAuthManager();

        if (!$au->getAuthItem("Admin")) {
            $au->createRole("Admin");
        }
        if (!$au->getAuthItem("Authenticated")) {
            $au->createRole("Authenticated");
        }
        if (!$au->checkAccess("Admin", $user_id)) {
            $au->assign("Admin", Yii::app()->user->id);
        }
        if (!$au->checkAccess("Authenticated", $user_id)) {
            $au->assign("Authenticated", Yii::app()->user->id);
        }

        $roles = Rights::getAssignedRoles(Yii::app()->user->Id);
        foreach ($roles as $role) {
            echo $role->name . "<br />";
        }

        echo Yii::app()->user->isSuperuser;
        echo "ok";
    }

    public function actionTest3()
    {
        $tr          = Yii::app()->db->beginTransaction();
        $d           = new Doc("insert", Comprob::ND_VENTAS);
        $d->total    = 100;
        $d->Socio_id = Socio::model()->find()->id;
        ve($d->socio->saldo);
        $d->save();
        ve($d->socio->saldo);
        $d           = Doc::model()->findByPk($d->id);
        $d->total    = 110;
        $d->Socio_id = Socio::model()->find()->id;
        ve($d->socio->saldo);
        $d->save();
        ve($d->socio->saldo);

        ve("anulaciones, saldo ant socio:" . $d->socio->saldo);
        ve("anulo");
        $d           = Doc::model()->findByPk($d->id);
        $d->anulado  = 1;
        $d->Socio_id = Socio::model()->find()->id;
        ve("antes:" . $d->socio->saldo);
        $d->save();
        ve("despues:" . $d->socio->saldo);
        ve("desanulo");
        $d = Doc::model()->findByPk($d->id);
        if (!$d) {
            ve("error" . $d->id);
        }
        $d->anulado  = 0;
        $d->Socio_id = Socio::model()->find()->id;
        ve("antes:" . $d->socio->saldo);
        $d->save();
        ve("despues:" . $d->socio->saldo);

        $tr->rollback();
    }

    public function actionCajaAbiertaP()
    {
        var_dump(Yii::app()->user->model->caja->abiertaP);
    }

    public function actionInstanciaAbierta()
    {
        var_dump(Yii::app()->user->model->caja->instanciaAbierta->attributes);
    }

    public function actionCajaAbre()
    {
        $caja = Yii::app()->user->model->caja;
        /* @var $a CInlineAction */
        if ($caja->abiertaP) {
            Helpers::error("La caja está abierta");
        } else {
            var_dump(Yii::app()->user->model->caja->attributes);
        }
    }

    public function actionAlumno($nombre = null, $apellido = null, $matricula = null)
    {
        $andMat = $matricula ? " and al.matricula = $matricula" : "";
        $qry    = Yii::app()->db->createCommand("
select *,s.id as socio_id,
	(select sum(doc.saldo * c.signo)
                    from doc doc
                       inner join talonario t on t.id = doc.talonario_id
                       inner join comprob c on c.id = t.comprob_id
                    where (doc.liquid_conf_id <> (select lc.id from liquid_conf lc where lc.fecha_confirmado is null)
														or doc.liquid_conf_id is null) and doc.socio_id = s.id) as saldo
	from alumno al
		inner join socio s on s.Alumno_id = al.id
		inner join alumno_division ad on ad.Alumno_id = al.id
		inner join division di on di.id = ad.Division_id
		inner join anio an on an.id = di.Anio_id
		inner join nivel ni on ni.id = an.Nivel_id
	where al.apellido like \"%$apellido%\" and al.nombre like \"%$nombre%\"
      " . $andMat)->queryAll();
        foreach ($qry as $data) {
            var_dump($data);
            $socio_id = $data["socio_id"];
            $docs     = Yii::app()->db->createCommand("
      select *
          from doc d
              inner join talonario t on t.id = d.talonario_id
              inner join comprob c on c.id = t.comprob_id
          where d.Socio_id = $socio_id
  ")->queryAll();
            var_dump($docs);
        }
    }

    public function actionTree()
    {
        $conceptosTreeData = $this->getConceptosData();
        $this->render("tree", array("conceptosTreeData" => $conceptosTreeData));
    }

    function getul($data, $st = "")
    {
        $br = "";
        //$br="\r";
        $retval = "<ul" . $st . ">$br";
        foreach ($data as $dato) {
            $retval .= '<li rel=\'' . $dato['id'] . '\'>' . $br . $dato['text'] . $br;
            if ($dato['children']) {
                $retval .= $this->getul($dato['children'], "");
            }
            $retval .= "</li>$br";
        }
        $retval .= "</ul>$br";

        return $retval;
    }

    function actionTest2()
    {
        $s = "
            SELECT       concat(
                'CREATE TABLE ',tables.table_name,'(',char(10)
                ,   '    history_auto_increment',char(10)
                ,   '        int unsigned',char(10)
                ,   '        not null',char(10)
                ,   '        auto_increment',char(10)
                ,   '        primary key',char(10)
                ,   ',   history_timestamp',char(10)
                ,   '        timestamp',char(10)
                ,   '        not null',char(10)
                ,   ',   history_user',char(10)
                ,   '        varchar(16)',char(10)
                ,   '        not null',char(10)
                ,   ',   history_operation',char(10)
                ,   '        enum(''DELETE'',''INSERT'',''UPDATE'')',char(10)
                ,   '        not null',char(10)
                ,   ','
                ,   group_concat(
                concat(
                '   '
                ,   columns.column_name,' '
                ,   columns.column_type,' '
                ,   case
                when columns.character_set_name is null then ''
                else concat(
                ' CHARACTER SET ',columns.character_set_name
                ,   ' COLLATE ',columns.collation_name
                )
                end
                ,   case columns.is_nullable
                when 'NO' then ' NOT NULL'
                else ''
                end
                ,   char(10)
                )
                order by columns.ordinal_position
                separator ','
                )
                ,   ')',char(10)
                ,   'ENGINE='
                ,   case tables.engine
                when 'InnoDB' then 'InnoDB'
                else 'MyISAM'
                end,char(10)
                ,   'DEFAULT CHARACTER SET ',collations.character_set_name,char(10)
                ,   'COLLATE ',tables.table_collation,char(10)
                ,   ';',char(10)
                )
                FROM         information_schema.tables
                INNER JOIN   information_schema.columns
                ON           tables.table_schema    = columns.table_schema
                AND          tables.table_name      = columns.table_name
                INNER JOIN   information_schema.collations
                ON           tables.table_collation = collations.collation_name
                WHERE        tables.table_schema    = SCHEMA()
                AND          tables.table_type      = 'BASE TABLE'
                GROUP BY     tables.table_name
                ,            tables.engine
                ,            tables.table_collation
                ,            collations.character_set_name
        ;";
        //$s="select * from articulo";
        $q = Yii::app()->db->createCommand($s)->queryAll();
        foreach ($q as $row) {
            ve($row);
            foreach ($row as $s) {
                vd($s);
                ve(Yii::app()->dbh->createCommand($s)->execute());
            }
            //echo array_values($s)."<br/>";
        };
    }

    function actionArreglaLiquidConf()
    {
        $cf_id = Comprob::FACTURA_VENTAS;
        echo "borrados : " . Yii::app()->db->createCommand("
            delete d.*
                from doc d
                    inner join talonario t on t.id = d.talonario_id
                where liquid_conf_id is not null and t.comprob_id = $cf_id ")->execute(array()) . "<br/>";
        $rows = Yii::app()->db->createCommand("
            select d.*
                from doc d
                    inner join talonario t on t.id = d.talonario_id
            where liquid_conf_id is not null and t.comprob_id <> $cf_id ")->queryAll();
        foreach ($rows as $r) {
            $doc_id = $r["id"];
            $lc_id  = $r["liquid_conf_id"];
            Yii::app()->db->createCommand("insert into doc_liquid (liquid_conf_id) values ($lc_id)")->execute();
            //            $s = "select LAST_INSERT_ID() as dl_id;";
            //            $dl_id =  Yii::app()->db->createCommand($s)->queryScalar();
            //            vd($dl_id);
            echo Yii::app()->db->createCommand("
                update doc set doc_liquid_id = LAST_INSERT_ID(), liquid_conf_id = null where id = $doc_id")->execute(array());
            Yii::app()->db->createCommand("
                select id from doc  where id = $doc_id")->queryScalar();
            $s = "select LAST_INSERT_ID() as dl_id;";
            echo Yii::app()->db->createCommand($s)->queryScalar() . "_";
        }
    }

    function actionIndex($action = "", $exec = 0)
    {
        $render = ($exec == 0) ? "render" : "renderPartial";
        $this->$render("exec", array("action" => $action, "exec" => $exec));
    }

    function actionCritickerGetScore()
    {
        echo $_GET['callback'] . '(' . json_encode(array("score" => Helpers::qryScalar("select valor from criticker where opcion = \"last_score\""))) . ")";
    }

    function actionCritickerSetScore()
    {
        $ret       = new stdClass();
        $ret->data = "1";
        echo $_GET['callback'] . '(' . json_encode($ret) . ")";
        die;
        echo json_encode($_GET);
        die;
        echo json_encode(Yii::app()->db->createCommand("update ")->execute(array("opcion" => $score)));
    }

    function actionCritickerTest()
    {
    }

    function actionDir()
    {
        $d = dir("g:\\videos");
        while ($path = $d->read()) {
            if (!is_dir($path)) {
                $info = pathinfo($path);
                $name = $info['filename'];
                if (!is_dir("g:\\videos\\" . $name) and !is_file("g:\\videos\\" . $name)) {
                    //ve($info);
                    //ve("g:\\videos\\" . $name);
                    mkdir("g:\\videos\\" . $name);
                }
            }
        }
    }


    function actionNuevo()
    {
        $this->layout = "nuevo";
        $this->render("nuevo");
    }


    public function actionTestKo1()
    {
        $this->layout = "nuevo";
        $this->render("testK1");
    }

    public function actionCurl()
    {
        //            $this->renderPartial('curl');
        //            die;
        //$url = "https://dfe.arba.gov.ar/DomicilioElectronico/SeguridadCliente/dfeServicioConsulta.do";
        $url = "https://dfe.arba.gov.ar/DomicilioElectronico/SeguridadCliente/dfeServicioConsulta.do";

        $curl = curl_init($url);
        $file = '<?xml version = "1.0" encoding = "ISO-8859-1"?>
                <CONSULTA-ALICUOTA>
                <fechaDesde>20141001</fechaDesde>
                <fechaHasta>20141031</fechaHasta>
                <cantidadContribuyentes>1</cantidadContribuyentes>
                <contribuyentes class="list">
                    <contribuyente>
                    <cuitContribuyente>20175036564</cuitContribuyente>
                    </contribuyente>
                </contribuyentes>
                </CONSULTA-ALICUOTA>';
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        //$post = urlencode('user').'='.urlencode('30639390469').'&'.urlencode('password').'='.urlencode('297900');
        $post = array(
            'user' => '30639390469', 'password' => '297900', 'file' => $file
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json'
            ));
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
        $result = curl_exec($curl);
        echo $result;

        return;


        $header = "POST HTTP/1.0 \r\n";
        $header .= "Content-type: text/xml \r\n";
        $header .= "Content-length: " . strlen($post_string) . " \r\n";
        $header .= "Content-transfer-encoding: text \r\n";
        $header .= "Connection: close \r\n\r\n";
        $header .= $post_string;
        /*
         * $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $output = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
         */
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_IGNORE_CONTENT_LENGTH, 0);
        //curl_setopt($ch, CURLOPT_USERPWD, "30689950767:MARC64");
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 14);
        //urlencode($name).'='.urlencode($value).'&'
        $post = urlencode('user') . '=' . urlencode('30689950767') . '&' . urlencode('password') . '=' . urlencode('MARC64');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $header);
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array('user:30689950767', 'password:MARC64'));


        $data = curl_exec($ch);
        //            var_export($ch);
        if (curl_errno($ch)) {
            //              var_export(curl_error($ch));
        } else {
            var_export($data);
            curl_close($ch);
        }
    }

    public function actionArba()
    {

        $url = "https://dfe.arba.gov.ar/DomicilioElectronico/SeguridadCliente/dfeServicioConsulta.do";

        $r = curl_init($url);
        curl_setopt($r, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($r, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($r, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($r, CURLOPT_POST, 1);
        $fileContent = '<?xml version = "1.0" encoding = "ISO-8859-1"?>
                <CONSULTA-ALICUOTA>
                <fechaDesde>20141101</fechaDesde>
                <fechaHasta>20141130</fechaHasta>
                <cantidadContribuyentes>1</cantidadContribuyentes>
                <contribuyentes class="list">
                    <contribuyente>
                    <cuitContribuyente>30689950767</cuitContribuyente>
                    </contribuyente>
                </contribuyentes>
                </CONSULTA-ALICUOTA>';
        if (!file_put_contents('send.xml', $fileContent, 0)) {
            vd('Error creando el archivo send.xml');
        }
        $md5      = md5_file(realpath('send.xml'));
        $fileName = 'DFEServicioConsulta_' . $md5 . '.xml';
        //$f     = curl_file_create(realpath('xml'), 'application/octet-stream', $fileName);
        $f  = new CurlFile(realpath('send.xml'), 'application/octet-stream', $fileName);
        $rf = fopen("c:\\resultFile.xml", "w");
        curl_setopt($r, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($r, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($r, CURLOPT_IGNORE_CONTENT_LENGTH, 0);
        curl_setopt($r, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($r, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($r, CURLOPT_VERBOSE, false);
        curl_setopt($r, CURLOPT_HEADER, false);
        curl_setopt($r, CURLOPT_FILE, $rf);
        //curl_setopt($r, CURLOPT_HTTPHEADER, array('Content-Type:multipart/form-data'));
        curl_setopt(
            $r,
            CURLOPT_POSTFIELDS,
            array(
                'user'     => '30639390469',
                'password' => '297900',
                'file'     => $f
            )
        );
        curl_exec($r);
        if (curl_error($r)) {
            echo 'Error...';
            var_dump(curl_error($r));
        } else {
            fclose($rf);
            $xml = simplexml_load_file('c:\\resultFile.xml');
            var_dump($xml->contribuyentes[0]->contribuyente->alicuotaRetencion);
        }
        curl_close($r);
    }

    public function actionArba2()
    {

        $url = "https://dfe.arba.gov.ar/DomicilioElectronico/SeguridadCliente/dfeServicioConsulta.do";

        $r = curl_init($url);
        curl_setopt($r, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($r, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($r, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($r, CURLOPT_POST, 1);
        $fileContent = '<?xml version = "1.0" encoding = "ISO-8859-1"?>
                <CONSULTA-ALICUOTA>
                <fechaDesde>20141001</fechaDesde>
                <fechaHasta>20141031</fechaHasta>
                <cantidadContribuyentes>1</cantidadContribuyentes>
                <contribuyentes class="list">
                    <contribuyente>
                    <cuitContribuyente>20175036564</cuitContribuyente>
                    </contribuyente>
                </contribuyentes>
                </CONSULTA-ALICUOTA>';
        if (!file_put_contents('xml', $fileContent, 0)) {
            vd('Error creando el archivo xml');
        }
        $md5      = md5_file(realpath('xml'));
        $fileName = 'DFEServicioConsulta_' . $md5 . '.xml';
        //            file_put_contents('c:\\'.$fileName, $fileContent, 0);
        //$f     = curl_file_create(realpath('xml'), 'application/octet-stream', $fileName);
        $f = new CurlFile(realpath('xml'), 'text/xml', $fileName);
        curl_setopt($r, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($r, CURLOPT_VERBOSE, false);
        curl_setopt($r, CURLOPT_HEADER, true);
        curl_setopt($r, CURLOPT_HTTPHEADER, array('application/octet-stream'));
        curl_setopt(
            $r,
            CURLOPT_POSTFIELDS,
            array(
                'user'     => '30639390469',
                'password' => '297900',
                'file'     => $f
            )
        );
        var_dump(curl_exec($r));
        if (curl_error($r)) {
            var_dump(curl_error($r));
        }
        curl_close($r);
    }

    public function actionNovedadesViejas()
    {
        $this->layout = 'nuevo';
        $a            = Helpers::qry("
                select concat(apellido, ', ', nombre) as nombres
                    from alumno where id = 42443
            ");
        $s            = "select nc.fecha_carga, n.detalle, n.importe
                from novedad as n
                    inner join novedad_cab as nc on n.novedad_cab_id = nc.id
                    inner join articulo as a on n.articulo_id = a.id
                where n.alumno_id = 42443 and nc.fecha_carga < '2013/12/31'";
        $q            = Helpers::qryAll($s);
        $this->render('novedadesViejas', array("data" => $q, 'alumno' => $a));
    }

    public function actionNotas()
    {
        $this->layout = "ko";
        $notas = Notas::getNotas(['division_id'=>371]);
        $this->render('notas', ['notas' => $notas->qry]);
    }

    public function actionAfip1()
    {
        $this->layout = 'nuevo';
        $this->render("afip1");
        vd(getcwd());
    }
}
