<?php

class UserController extends Controller {

  public function actionIndex() {
    $users = Helpers::qryAllObj("
		  select id, nombre, login from user
		  ");
    $this->render('index', array("users" => $users));
  }

  public function actionGetUserForm($user_id) {
    $user = Helpers::qryObj("select * from user where id = $user_id", null, true);
    $this->renderPartial("userForm", array("user" => $user));
  }

  public function actionGetUserAsignaciones($user_id) {
    $userRoles = Helpers::qryAllObj("
			select i.name, i.description, i.type,
				(select count(*) from authitemchild c where c.parent = i.name) as childs
					from authassignment a
						inner join authitem i on i.name = a.itemname
					where a.userid = $user_id
			");
    $freeRoles = Helpers::qryAllObj("
				select i.* , 
					(select count(*) from authitemchild c1 where c1.parent = i.name) as childs,
					(select count(*) from authitemchild c2 where c2.child = i.name) as parents
				from authitem i
					where i.name not in (select a.itemname from authassignment a where a.userid = $user_id and a.itemname = i.name)
				order by i.type
			");
    $this->renderPartial("userAsignaciones", array(
      "user_id" => $user_id,
      "userRoles" => $userRoles,
      "freeRoles" => $freeRoles,
    ));
  }

  public function actionGetUserChildRoles($user_id, $item) {
    $userRoles = Helpers::qryAllObj("
			select c.child as name
				from authassignment a
					inner join authitem i on i.name = a.itemname
					inner join authitemchild c on c.parent = i.name
				where a.itemname = \"$item\" and a.userid = $user_id
			");
    $this->renderPartial("userChildRoles", array("userRoles" => $userRoles));
  }

  public function actionGrabaUserForm() {
    $user = User::model()->findByPk($_POST["user_id"]);
    //$passwordAnt = $_POST["password"];
    $user->setAttributes($_POST);
    $ret = new stdClass;
    $ret->status = "ok";
    if ($user->password !== $_POST["password"]) {
      if ($_POST["password"] !== $_POST["password_repeat"]) {
        $ret->status = "error";
        $ret->errores = array("Debe repetir correctamente el Password");
      } elseif (strlen($_POST["password"]) < 8) {
        $ret->status = "error";
        $ret->errores = array("El password de muy corto");
      } else {
        $user->password = md5($_POST["password"]);
      }
    }
    if ($ret->status == "ok") {
      if (!$user->save()) {
        $ret->status = "error";
        $ret->errores = $user->errors;
      } else {
        $ret->status = "ok";
      }
    }
    echo json_encode($ret);
  }

  public function actionAsignaItem($user_id, $item) {
    $item = trim($item);
    /* @var $auth CDbAuthManager */
    $auth = Yii::app()->AuthManager;
    if ($auth->isAssigned($item, $user_id)) {
      $auth->revoke($item, $user_id);
    } else {
      $auth->assign($item, $user_id);
    }
  }

  public function actionAlta() {
    $model = new User('register');
    $this->performAjaxValidation($model);
    if (isset($_POST['User'])) {
      $model->setAttributes($_POST['User'], 'register');
      $model->password_repeat = $_POST['User']['password_repeat'];
      if ($model->save()) {
        $model->login();
        $this->redirect($this->createUrl("/"));
      }
    }
    $this->render('alta', array('model' => $model));
    echo "Por implementar... vuelva a la pagina anterior...";
  }

  public function actionAsignaciones() {
    $am = Yii::app()->getAuthManager();
    $this->render('asignaciones', array('authItems' => $am->getAuthItems()));
  }

  public function actionRoleUsuarios($role) {
    $usuarios = array();
    $i = 0;
    $au = Yii::app()->getAuthManager();
    foreach (User::model()->findAll() as $usuario) {
      $usuarios[$i]["id"] = $usuario->id;
      $usuarios[$i]["nombre"] = $usuario->nombre;
      $usuarios[$i]["asignado"] = $au->isAssigned($role, $usuario->id);
      $i++;
    }
    $this->renderPartial('_rol_usuarios', array('usuarios' => $usuarios));
  }

  public function actionAdmin() {
    $this->render("admin");
  }

  public function actionResetPassword($user_id) {
    $r = new stdClass();
    $u = User::model()->findByPk($user_id);
    $u->password = md5($u->login);
    $u->password_repeat = md5($u->login);
    $u->cambiar_password = 1;
    if (!$u->save()) {
      $r->errores = $u->errors;
    }
    echo json_encode($r);
  }

  public function actionBorra($user_id) {
    $r = new stdClass();
    $r->error = '';
    $tr = Yii::app()->db->beginTransaction();
    $r->status = Helpers::qryExec("
      delete from socio where Empleado_id = $user_id;
    ");
    if ($r->status === 1) {
      $r->status = Helpers::qryExec("
      delete from user where id = $user_id;
      ");
      if ($r->status === 1) {
        $tr->commit();
      } else {
        $r->error = 'error borrando de USER';
      }
    } else {
      $r->error = 'error borrando de SOCIO';
    }
    echo json_encode($r);
  }

  public function actionUsuarioNuevo($nombre) {
    $r = new stdClass();
    $u = User::model()->find("nombre = \"$nombre\"");
    if ($u) {
      $r->errores["Nombre"] = "Ya existe un usuario con este nombre";
    } else {
      $partes = explode(" ", $nombre);
      $login = strtolower($partes[0] . ((count($partes) > 1) ? $partes[1][0] : ""));
      $u = User::model()->find("login = \"$login\"");
      if ($u) {
        $r->errores["login"] = "Ya existe un usuario con este login";
      } else {
        $u = new User();
        $r->login = $login;
        $u->nombre = $nombre;
        $u->login = $login;
        $u->password = md5($u->login);
        $u->password_repeat = md5($u->login);
        $u->cambiar_password = 1;
        if (!$u->save()) {
          $r->errores = $u->errors;
        } else {
          $r->id = $u->id;
        }
      }
    }
    echo json_encode($r);
  }

  public function actionToggleUserRole($user, $role, $todos = false) {
    $au = Yii::app()->getAuthManager();
    /* @var $au CDbAuthManager */
    if ($user != "*") {
      $this->toggleRole($role, $user, $au);
    } else {
      foreach (User::model()->findAll() as $usuario) {
        if ($todos == "true") {
          if (!$au->isAssigned($role, $usuario->id))
            $au->assign($role, $usuario->id);
        } else {
          if ($au->isAssigned($role, $usuario->id))
            $au->revoke($role, $usuario->id);
        }
      }
    }
  }

  private function toggleRole($role, $user, $au) {
    if ($au->isAssigned($role, $user)) {
      $au->revoke($role, $user);
    } else {
      $au->assign($role, $user);
    }
  }

  protected function performAjaxValidation($model) {
    if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }
  }

  public function actionTestLogin() {
    $user = User::model()->findByPk(4);
    $user->login();
    $this->redirect($this->createUrl("/"));
  }

//@TODO: Falta programar el cambio de password
  public function actionCambioDePassword() {
    $this->porHacer();
  }

  public function actionGrabaUserDocumento(){
      $id = $_POST['id'];
      $doc = $_POST['documento'];
      Helpers::qryExec("update user set documento = '$doc' where id = $id");
  }

}
