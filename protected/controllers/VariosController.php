<?php

class VariosController extends Controller {

   public $actions;

   public function beforeAction($action) {
      $this->actions["RecBancoRepetido1"] = array(
          "title" => "Recibos banco repetidos #1927 y #1928",
          "obs" => ""
          . "Para Silvia: Las transferencias que tenían los números 1927 y 1928 están anuladas,<br/>"
          . "supongo que esas son las que no aparecen en la conciliacion<br/>",
      );
      $this->actions["Test"] = array(
          "title" => "Title Test",
          "obs" => "Obs. Test",
      );
      return parent::beforeAction($action);
   }

   public function actionIndex() {
      $this->render("index");
   }

   public function actionUsuarios() {
    $this->layout = "bootstrap";
    $this->render("usuarios");
   }

   public function actionRetenciones() {
    $this->layout = "bootstrap";
    $this->render("retenciones");
   }

   public function actionObsLog() {
      $rows = Helpers::qryAll('
              SELECT l.fecha, u.nombre, l.anterior, l.nuevo FROM log l
                INNER JOIN user u ON l.user_id = u.id
              WHERE clave = "familia-obs" and trim(l.anterior) <> trim(l.nuevo)');
      $div = Helpers::getTable("main", $rows);
      $this->renderPartial("/layouts/main", array("content" => $div));
   }

   public function actionRecBancoRepetido1() {
      $this->render("recBancoRepetido1");
   }

   public function actionArreglaAplicaciones() {
//        ini_set("memory_limit", "912M");
      /* @var $dbk CDbConnection */
      $dbk = Yii::app()->db;
//      $tr = Yii::app()->db->beginTransaction();
//        ve(Yii::app()->db->createCommand("
//            delete from doc_apl;
//            update doc d
//		inner join socio s on s.id =d.Socio_id
//		inner join talonario t on t.id = d.talonario_id
//		inner join comprob c on c.id = t.comprob_id and c.signo_cc = 1
//            set d.saldo = d.total;
//            update doc d
//		inner join socio s on s.id =d.Socio_id
//		inner join talonario t on t.id = d.talonario_id
//		inner join comprob c on c.id = t.comprob_id and c.signo_cc = -1
//            set d.saldo = d.total;
//
//            ")->execute());

      $sel = "
         select da.*
            from doc_apl da
               inner join doc do on do.id = da.Doc_id_origen
               inner join doc dd on dd.id = da.Doc_id_destino
            /*where do.socio_id = 36062 or dd.socio_id = 36062;*/
            /*where da.doc_apl_cab_id is not null*/
       ";
      $rows = $dbk->createCommand("$sel")->queryAll();
      $cant = 0;
      foreach ($rows as $row) {
         $ap = new DocApl();
         $id = $row["id"];
         $existe = Yii::app()->db->createCommand("
                select count(*) from doc_apl da where da.id = $id
            ")->queryScalar();
         if ($existe !== "0") {
            continue;
         }
         $ap->Doc_id_origen = $row["Doc_id_origen"];
         $ap->doc_id_destino = $row["doc_id_destino"];
         $ap->id = $row["id"];
         $ap->importe = $row["importe"];
         $ap->doc_apl_cab_id = $row["doc_apl_cab_id"];
         //vd($ap->attributes);
         if (!$ap->save()) {
            vd($ap->errors);
         }
         $cant++;
         //vd($ap->attributes);
      }
      ve("Cantidad de aplicaciones: $cant");
      //$tr->commit();
   }

   public function actionArreglaObsViejo() {
      $this->layout = "nuevo";
      $this->render("arreglaObs");
   }

   public function actionArreglaObs() {
      $this->layout = "nuevo";
      $this->render("arreglaObs");
   }

   public function actionKeepSession() {
      echo "ok";
   }

   public function actionSessionSecondsLeft() {
      echo Helpers::sessionSecondsLeft();
   }

   public function actionChequeaAplicaciones() {
      $this->layout = 'nuevo';
      $this->render('chequeaAplicaciones');
   }

   public function actionArreglaChequesNacion(){
      $qry = Helpers::qryAll("
            SELECT c.nombre as comprob, d.numero, dv.fecha, dv.importe, d1.nombre as destino FROM doc d
            INNER JOIN doc_valor dv ON d.id = dv.Doc_id
            INNER JOIN destino d1 ON dv.Destino_id = d1.id
            INNER JOIN talonario t ON d.talonario_id = t.id
            INNER JOIN comprob c ON t.comprob_id = c.id
          WHERE d.numero = 4986 and c.id = 10");
      $this->layout = "nuevo";
      $this->render("arreglaChequesNacion", array("qry"=>  json_encode($qry)));
   }

   public function actionArreglaChequesNacionAjax(){
      $ids = "(28461,38720,38721,38722,38723,38724,38725,38726,38727,38728,38729,38730,38744,38745,38746,38747,38748,38749,38750,38751,38752,38753,38754,38755,38756,38757,38758,38759,38760,38761,38762,38763,38764,38765,38766,38767,38768,38769,38770,38771,38772,38773,38774,38775,38776)";
      Helpers::qryExec("
         update doc_valor set destino_id=7 where id in $ids
         ");
   }

}
