<?php
$query = isset($this->body->query) ? $this->body->query : '';
//vd2($this->body);
$conPadres = isset($this->body->con_padres) && ($this->body->con_padres ? true: false);
if($query === "") {
  $socios = [];
} else {
  $apellido = $query;
  $nombre = '';
  $pos = $pos = strpos($query, ',');
  if($pos){
    $parts = explode(',', $query);
    $apellido = trim($parts[0]);
    $nombre = trim($parts[1]);
  }
  $select = "
    SELECT a.id, an.nombre as anio, d.nombre as division, n.nombre as nivel,
           a.nombre, a.apellido, 'a' as tipo, '' as relacion
    FROM alumno a
      INNER JOIN alumno_division ad ON a.id = ad.Alumno_id AND ad.activo
      inner join division d on d.id = ad.division_id
      inner join anio an on an.id = d.anio_id
      inner join nivel n on n.id = an.nivel_id
      INNER JOIN alumno_estado ae ON a.estado_id = ae.id AND ae.activo_edu
      INNER JOIN alumno_division_estado ade ON ad.alumno_division_estado_id = ade.id AND ade.muestra_edu
    where
      a.apellido like \"%$apellido%\" and a.nombre like \"%$nombre%\"  AND ade.muestra_edu AND ! ad.borrado
  ";
  // vd($select);3
  if($conPadres){
    $select .= "
    union
    SELECT p.id, '' as anio, '' as division, 'Pariente' as nivel,
    p.nombre, p.apellido, 'p' as tipo, pt.nombre as relacion
    FROM pariente p
      INNER JOIN pariente_tipo pt ON pt.id = p.Pariente_Tipo_id
    where
    p.apellido like \"%$apellido%\" and p.nombre like \"%$nombre%\"
    order by apellido, nombre limit 100";
    // vd($select);3
  } else {
    $select .= " order by apellido, nombre, nivel limit 100";
  }
  $socios = Helpers::qryAll($select);
  //echo($select);die;
  //for($a=1;$a<500;$a++){
  //	$socios = Helpers::qryAll($select);
  //}
  // $this->resp->status = 200;
  // $this->resp->access_token = session_id();
  }
$this->resp->data = $socios;
exit(json_encode($this->resp));
