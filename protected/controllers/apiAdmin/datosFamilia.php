<?php
$alumno_id = $this->body->alumno_id;
if($this->body->tipo === 'a'){
  $familia_id = Helpers::qryScalar("select familia_id from alumno where id = $alumno_id");
} else {
  $familia_id = Helpers::qryScalar("select familia_id from pariente where id = $alumno_id");
}
$select = "
  select a.id, a.apellido, a.nombre, 'alumno' as tipo, a.email, a.familia_id,
    concat( n.nombre, ' ', an.nombre , ' ', d.nombre ) as descripcion
  from alumno a
    INNER JOIN alumno_division ad ON a.id = ad.Alumno_id AND ad.activo
    inner join division d on d.id = ad.division_id
    inner join anio an on an.id = d.anio_id
    inner join nivel n on n.id = an.nivel_id
    INNER JOIN alumno_estado ae ON a.estado_id = ae.id AND ae.activo_edu
    INNER JOIN alumno_division_estado ade ON ad.alumno_division_estado_id = ade.id AND ade.muestra_edu
    where a.familia_id = $familia_id AND ade.muestra_edu AND ! ad.borrado /*and a.id != $alumno_id*/
  union
  select p.id, p.apellido, p.nombre, pt.nombre as tipo, p.email, p.familia_id,
         CONCAT(td.nombre, ' ', p.numero_documento) as descripcion
  from pariente p
    INNER JOIN pariente_tipo pt ON pt.id = p.Pariente_Tipo_id
    LEFT  JOIN tipo_documento td ON p.tipo_documento_id = td.id
    where p.familia_id = $familia_id
";
  // vd($select);3
  $alumnos = Helpers::qryAll($select);
  //echo($select);die;
  //for($a=1;$a<500;$a++){
  //	$socios = Helpers::qryAll($select);
  //}
  // $this->resp->status = 200;
  // $this->resp->access_token = session_id();
$this->resp->data = $alumnos;
exit(json_encode($this->resp));
