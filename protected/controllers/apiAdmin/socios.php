<?php
$query = ($this->body->query) ? $this->body->query : ' true ';
if($query === "") {
  $socios = [];
} else {
  //var_dump($this->body);die;
  //$query = $this->body->query;
  $soloLogin = (isset($this->body->todos) and $this->body->todos == 'true') ? ' true ':' s.login_buffet is not null ';
  $con_saldo = isset($this->body->con_saldo)?$this->body->con_saldo:false;
  $apellido = $query;
  $nombre = '';
  $pos = $pos = strpos($query, ',');
  if($pos){
    $parts = explode(',', $query);
    $apellido = trim($parts[0]);
    $nombre = trim($parts[1]);
  }
  $select = "
    select s.id, s.admin_buffet as admin,
      case
        when a.id then concat(coalesce(a.apellido,' '),', ', coalesce(a.nombre,' '))
        when e.id then concat(coalesce(e.apellido,' '), ', ', coalesce(e.nombre,' '))
        when p.id then concat(coalesce(p.apellido,' '), ', ', coalesce(p.nombre,' '))
      end as title,
      case
        when a.id then 'A'
        when e.id then 'E'
        when p.id then 'P'
      end as tipo,
      case
        when a.id then
        concat(
            an.nombre, ' ', d.nombre,
            ' DNI:', a.numero_documento
          )
        when e.id then 'Empleado'
        when p.id then t.nombre
      end as descripcion
    from socio s
      left join alumno a on a.id = s.alumno_id
      left join alumno_division ad on ad.alumno_id = a.id and ad.activo
      left JOIN alumno_division_estado ade ON ad.alumno_division_estado_id = ade.id
      left join division d on d.id = ad.division_id
      left join anio an on an.id = d.anio_id
      left join pariente p on p.id = s.pariente_id
      left join pariente_tipo t on p.pariente_tipo_id = t.id
      left join user e on e.id = s.empleado_id
    where
      CASE
        WHEN a.id THEN
          a.apellido like \"%$apellido%\" and a.nombre like \"%$nombre%\"  AND ade.muestra_edu AND ! ad.borrado
        WHEN p.id THEN
          p.apellido like \"%$apellido%\" and p.nombre like \"%$nombre%\" and p.apellido != '(falta apellido)' and p.nombre != '(falta nombre)'
        WHEN e.id THEN
          (e.apellido like \"%$apellido%\" and e.nombre like \"%$nombre%\")
          or e.nombre like \"%$apellido%\"
      END
      order by 2 limit 100";
  // vd($select);3
  $socios = Helpers::qryAll($select);
  //echo($select);die;
  //for($a=1;$a<500;$a++){
  //	$socios = Helpers::qryAll($select);
  //}
  // $this->resp->status = 200;
  // $this->resp->access_token = session_id();
  }
$this->resp->data = $socios;
exit(json_encode($this->resp));
