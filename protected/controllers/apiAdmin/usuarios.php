<?php
$descending = isset($this->body->descending) ? $this->body->descending : '';
$fetchCount = $this->body->fetchCount;
$filter = isset($this->body->filter) ? $this->body->filter : '';
$sortBy = isset($this->body->sortBy) ? $this->body->sortBy : '';
$startRow = $this->body->startRow;
$where = '';
if($filter) {
  $where = "where nombre like '%$filter%' or apellido like '%$filter%' or u.login like '%$filter%' or email like '%$filter%' or documento like '%$filter%'";
}
if($sortBy) {
  if($sortBy === 'apellido') {
    $sortBy = ' order by apellido, nombre ';
  } else {
    $sortBy = " order by u.$sortBy";
  }
  $sortBy .= ' ' . ($descending ? ' desc ' : ' asc ');
} else {
  $sortBy = " order by apellido, nombre ";
}
$this->resp->count = Helpers::qryScalar("
select count(*)
from user u
$where
");
$this->resp->roles = Helpers::qryAll('select * from role');
$this->resp->data = Helpers::qryAll("
  select u.id, u.nombre, u.apellido, u.email, u.login, u.password, u.documento,
  (select group_concat(role_id) from user_role where user_id = u.id) as roles
  from user u
   inner join socio s on s.empleado_id = u.id
  $where
  $sortBy
  limit $startRow, $fetchCount
");
exit(json_encode($this->resp));