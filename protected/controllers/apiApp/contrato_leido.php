<?php 
$contrato = $this->body;
// vd2($contrato);
// vd2([
//   "socio_id" => $contrato->socio_id,
//   "tipo" => $contrato->tipo,
//   "fecha" => date("Y/m/d")
// ]);
$existe = helpers::qryscalar(
  'select * from contrato_leido where socio_id = ' . $contrato->socio_id . ' and tipo = "' . $contrato->tipo .'"', [],
  Yii::app()->dbIAE
);
$this->resp->leido = $existe;
if(!$existe) {
  Helpers::qryExec(
    "insert into contrato_leido(socio_id,tipo,fecha) values(:socio_id, :tipo, :fecha)",
    [
      "socio_id" => $contrato->socio_id,
      "tipo" => $contrato->tipo,
      "fecha" => date("Y/m/d")
    ],
    Yii::app()->dbIAE
  );
};
// $this->resp->respuestacontrato = Helpers::qryExec($qry);
// $ret->credito = Helpers::qryScalar("select sum(d.saldo) from buffet_doc d where d.saldo < 0");
exit(json_encode($this->resp));