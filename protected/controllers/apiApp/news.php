<?php
$this->resp->success = true;
$host = gethostbyaddr("127.0.0.1");
/*TODO: Quitar */
if ($host === 'localhost') {
    $url = 'http://iae.dyndns.org/assets/img/';
} elseif ($host === 'localhost.jp') {
    $url = 'https://assets.iae.com.ar/img/';
} else {
    $url = 'https://assets.iae.com.ar/img/';
}
$select = "select n.id, n.titulo, n.texto, n.imagenes, DATE_FORMAT(n.fecha, \"%d/%m/%Y\") as fecha from noticia n where !n.borrado order by n.orden";
$this->resp->data = Helpers::qryAll($select);
// vd2($this->resp->data);
foreach ($this->resp->data as $key => $value) {
  // $files = array_slice(scandir('/prg/assets/img'), 2);
    $imagenes = [];
    $img = $value["imagenes"];
    $imgJson = $img ? json_decode($img) : [];
    foreach ($imgJson as $value) {
        $imagenes[] = $url . $value;
    }
    $this->resp->data[$key]["imagenes"] = $imagenes;
}
exit(json_encode($this->resp));
