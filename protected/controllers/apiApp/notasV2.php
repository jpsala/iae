<?php

$socio_id = $this->get['socio_id'];
$alumno_id = Helpers::qryScalar('select a.id from socio s inner join alumno a on a.id = s.alumno_id and s.id = ' . $socio_id);
// vd2($alumno_id);
$ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
list($alumno_division_id, $nivel_id) = getDatosVarios($alumno_id, $ciclo_id);
$periodos = getPeriodos($ciclo_id, $nivel_id);
$asignaturas = getAsignaturas($alumno_division_id);
$notas = [];
if($nivel_id === '1') {
  $notas = InicialModel::getEvaluaciones($alumno_id);
  $this->resp = $notas;
} else {
  $conceptuales = [];
  foreach ($asignaturas as $asignatura) {
        // vd2($asignaturas);
      // $notas[$asignatura['nombre']] = array();
      $notaPeriodos = [];
      foreach ($periodos as $periodo) {
        $nota = getNota($alumno_division_id, $asignatura['id'], $periodo['id']);
        if(trim($asignatura['nombre']) === 'Conceptual'){
          $conceptuales[] = ['periodo' => $periodo['id'], 'nota' => $nota];
        }else{
          $notaPeriodos[$periodo['id']] =$nota;
        }
      }
      if(trim($asignatura['nombre']) !== 'Conceptual'){
        $notas[] = [
          'asignatura' => trim($asignatura['nombre']),
          'nota' => $notaPeriodos
        ];
      }
    $this->resp = ['periodos' => $periodos, 'notas' => $notas, 'conceptuales' => $conceptuales];
  }
}
exit(json_encode($this->resp));
function getDatosVarios($alumno_id, $ciclo_id) {
    return Helpers::qryDataRow("
        SELECT ad.id AS alumno_division_id, a.Nivel_id AS nivel_id
            FROM alumno_division ad
                INNER JOIN alumno_division_estado ade ON ad.alumno_division_estado_id = ade.id AND ade.muestra_edu
                INNER JOIN division d ON ad.Division_id = d.id
                INNER JOIN anio a ON d.Anio_id = a.id
            WHERE ad.alumno_id = $alumno_id and not ad.borrado AND ad.Ciclo_id = $ciclo_id");
}

function getPeriodos($ciclo_id, $nivel_id) {
  $hoy = date("Y/m/d");
  $selectPeriodos = "
        SELECT lp.id, lp.nombre
        FROM logica_periodo lp
            INNER JOIN logica l ON lp.Logica_id = l.id
            INNER JOIN logica_ciclo lc ON l.id = lc.Logica_id AND lc.Ciclo_id = $ciclo_id
            INNER JOIN asignatura_tipo at ON lc.Asignatura_Tipo_id = at.id
                       AND at.tipo = 1 AND at.Nivel_id = $nivel_id
        where lp.fecha_inicio <= '$hoy'
        ORDER BY lp.orden";
        // vd2($selectPeriodos);
    $tmp = Helpers::qryAll($selectPeriodos);
    // vd2($tmp);
    return $tmp;
}
function getAsignaturas($alumno_division_id){
    $selectAsignaturas = "
        SELECT a.id, a.nombre
            FROM asignatura a
                INNER JOIN division_asignatura da ON a.id = da.Asignatura_id
                INNER JOIN division d ON da.Division_id = d.id
                INNER JOIN alumno_division ad ON d.id = ad.Division_id WHERE ad.id = $alumno_division_id";
    return  Helpers::qryAll($selectAsignaturas);
}

function getNota($alumno_division_id, $asignatura_id, $periodo_id){
    $selectNotas = "
        SELECT lp.id as periodo_id, a1.id as asignatura_id, n.nota, n.texto
        FROM nota n
            INNER JOIN asignatura a1 ON n.Asignatura_id = a1.id
            INNER JOIN logica_item li ON n.Logica_item_id = li.id AND li.nota_del_periodo
            INNER JOIN logica_periodo lp ON li.Logica_Periodo_id = lp.id
        WHERE n.Alumno_Division_id = $alumno_division_id /* AND n.nota  */
            and n.asignatura_id = $asignatura_id and lp.id = $periodo_id
    ";
    // vd2($selectNotas);
    $nota = Helpers::qryAll($selectNotas);
    if(!$nota){
        $nota = '';
    } elseif (count($nota) > 1) {
        ve2('Error, mas de una nota');
        vd2($nota);
    }else {
        $nota = $nota[0]['texto'] ? $nota[0]['texto'] : $nota[0]['nota'];
    }
    return $nota;
}