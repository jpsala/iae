<?php
  function getSocio($where, $conHijos = true){ 
    // $conHijos = $conHijos ? true: false;
    $select = "
      select * from socio_view s
      where $where";
    
    $socio = Helpers::qry($select);
    if ($socio and $conHijos) {
      $socio_id = $socio['socio_id'];
        // vd2($socio_id);
      $selectHijos = "
        SELECT s2.id, a.nombre, a.matricula, 1 as hijo, a1.Nivel_id, n.nombre as nivel_nombre
        FROM socio s 
          left JOIN pariente p ON s.Pariente_id = p.id
          left JOIN familia f ON p.Familia_id = f.id
          left JOIN alumno a ON f.id = a.Familia_id
          left JOIN alumno_division ad ON a.id = ad.Alumno_id AND ad.activo AND ! ad.borrado
          left JOIN division d ON ad.Division_id = d.id
          left JOIN anio a1 ON d.Anio_id = a1.id
          left JOIN socio s2 on s2.alumno_id = a.id
          left JOIN alumno_estado ae ON a.estado_id = ae.id AND ae.activo_edu
          left join nivel n on n.id = a1.nivel_id
        WHERE s.id = $socio_id and a.activo
        ORDER BY a.apellido, a.nombre
      ";
      // vd2($selectHijos);
      $hijos = Helpers::qryAll($selectHijos);
      $socio['socios'] = $hijos;
    }
    return $socio;
  }