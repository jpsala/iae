
<?php
$conn = in_array(gethostbyaddr("127.0.0.1"), array("localhost", "localhost.jp")) ? Yii::app()->db : Yii::app()->dbIAE;
$tr = $conn->beginTransaction();
$socio_id = $this->post['socioId'];
$subscription = $this->post['subscription'];  
// $existe = Helpers::qryScalar("select count(*) from subscription where socio_id = $socio_id", [], $conn);
$params = [
  'socio_id' => $socio_id,
  'subscription' => $subscription
];
// if(!$existe){
  $ret = Helpers::qryExec(
    "insert into subscription(socio_id, data) values(:socio_id, :subscription)",
    $params, $conn);
// } else {
  // $ret = Helpers::qryExec(
  //   "update subscription set data = :subscription where socio_id = :socio_id", 
  //   $params, $conn);
// }
$tr->commit();
$this->resp->data = $ret;
/* const subscription = req.body.subscription;
  const socioId = req.body.socioId;
  // eslint-disable-next-line no-use-before-define
  saveSubscriptionToDatabase(subscription, socioId)
    .then(() => {
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify({ data: { success: true } }));
    })
    .catch((error) => {
      res.status(500);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(error));
    });
  // res.status(201).json({});
*/
exit(json_encode($this->resp));