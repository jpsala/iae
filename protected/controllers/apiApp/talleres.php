<?php
// $division_id= $this->get['division_id'];
$socioId= $this->get['socio_id'];
$selectAlumnos = "
  SELECT ad.id AS alumno_division_id, an.nivel_id, a.id as alumno_id
  FROM alumno a
    inner join socio s on s.alumno_id = a.id
    INNER JOIN alumno_division ad ON a.id = ad.Alumno_id AND ad.activo
    inner join division d on d.id = ad.division_id
    inner join anio an on an.id = d.anio_id
    INNER JOIN alumno_estado ae ON a.estado_id = ae.id AND ae.activo_edu
    INNER JOIN alumno_division_estado ade ON ad.alumno_division_estado_id = ade.id AND ade.muestra_edu
  where s.id = $socioId";
$alumno = Helpers::qry($selectAlumnos);
$alumnoId = $alumno['alumno_id'];
$nivel_id = $alumno['nivel_id'];
$ret = new stdClass();
$selectCats = "
  SELECT * FROM taller_categoria order by orden
";
$categorias= Helpers::qryAll($selectCats);
$selectTalleres = "
  SELECT * FROM taller where LOCATE('$nivel_id', niveles) order by orden";
  // vd2($selectTalleres);
$talleres = Helpers::qryAll($selectTalleres);
$selectNotas = "
  SELECT ad.id AS alumno_division_id, tn.id AS nota_id, tn.nota, t.id AS taller_id, tn.taller_valoracion_id
  FROM alumno_division ad
    INNER JOIN alumno_division_estado ade ON ad.alumno_division_estado_id = ade.id AND ade.muestra_edu
    INNER JOIN taller_nota tn ON ad.id = tn.alumno_division_id
    INNER JOIN taller t ON tn.taller_id = t.id
  WHERE ad.alumno_id = $alumnoId AND ad.activo";
$notas = Helpers::qryAll($selectNotas);
$selectValoraciones= "
  SELECT * from taller_valoracion";
$valoraciones = Helpers::qryAll($selectValoraciones);

$this->resp->data = new stdClass();
$this->resp->data->alumno = $alumno;
$this->resp->data->valoraciones = $valoraciones;
$this->resp->data->categorias = $categorias;
$this->resp->data->talleres= $talleres;
$this->resp->data->notas = $notas;

exit(json_encode($this->resp));