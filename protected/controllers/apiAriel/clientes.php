<?php
$resp = new stdClass();
$resp->success = true;
$conn = Yii::app()->ariel->pdoInstance;
$select = "select * from cliente";
$qry = $conn->query($select);
$qry->setFetchMode(PDO::FETCH_ASSOC);
$resp->body = $this->body;
$resp->data = $qry->fetchAll();

exit(json_encode($resp));
