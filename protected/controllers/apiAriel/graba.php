<?php
$ret = new stdClass();
//		vd($_REQUEST);
// vd($this->body);
// $doc = $this->body->doc;
// $items = $this->body->items;
$conn = Yii::app()->ariel;
$tr = $conn->beginTransaction();
$cliente = $this->body;
$qry = "select id from cliente where id = $cliente->id";
$existe = $conn->createCommand($qry)->queryScalar();
if ($existe) {
	$select = "UPDATE cliente
        SET nombre = :nombre
					WHERE id = :id";
	$params = array(
        'nombre' => $cliente->nombre,
        'id' => $cliente->id
    );
} else {
	$select = "INSERT INTO cliente(
        nombre)
        VALUES(:nombre)";
	$params = array('nombre' => $cliente->nombre);
}
$conn->createCommand($select)->execute($params);
$cliente_id = Yii::app()->ariel->createCommand("select LAST_INSERT_ID();")->queryScalar();
$tr->commit();
$ret->status = 'ok';
$ret->id = $cliente_id;
exit(json_encode($ret));
