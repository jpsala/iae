<?php
$notas =  $this->body;
foreach ($notas as $nota) {
  $nota = (object) $nota;
  if(isset($nota->nueva) and ($nota->nueva == 1 or $nota->nueva == '1')) {
    try {
      $insertQuery = "
        insert into taller_nota(taller_id, alumno_division_id, taller_valoracion_id, periodo, nota)
          values(:taller_id, :alumno_division_id, :taller_valoracion_id, :periodo, :nota)
      ";
      $params = array(
        'nota' => $nota->nota,
        'taller_id' => $nota->taller_id,
        'alumno_division_id' => $nota->alumno_division_id,
        'taller_valoracion_id' => $nota->taller_valoracion_id,
        'periodo' => $nota->periodo
      );
    // vd2($updateQuery, $nota, $params);
      $resp = Helpers::qryExec($insertQuery, $params);
    } catch(Exception $e) {
      vd2('error', $e);
    }
  } else {
    try {
      // vd2($nota);
      $updateQuery = "
        update taller_nota set taller_id=:taller_id,  taller_valoracion_id=:taller_valoracion_id,
                alumno_division_id = :alumno_division_id, nota=:nota where id = :id";
      $params = array(
        'id' => $nota->id,
        'nota' => $nota->nota,
        'taller_id' => $nota->taller_id,
        'taller_valoracion_id' => $nota->taller_valoracion_id,
        'alumno_division_id' => $nota->alumno_division_id);
        // vd2($updateQuery, $nota, $params);
      $resp = Helpers::qryExec($updateQuery, $params);
    } catch(Exception $e) {
      vd2('error', $e);
    }
  }
}
// vd2($resp);
$this->resp->response = $resp;
exit(json_encode($this->resp));
