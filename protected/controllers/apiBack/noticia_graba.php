
<?php
$ret = new stdClass();
$tr = Yii::app()->db->beginTransaction();
$files = (isset($_FILES) && count($_FILES) > 0) ? $_FILES : [] ;
$news = json_decode($this->post['noticia']);
// vd2($files);
$news->fecha = date("Y/m/d", strtotime(str_replace("/", ".", $news->fecha)));
// vd2($news);
foreach ($news->imagenes as $key => $file) {
    $news->imagenes[$key] = basename($file);
}
foreach ($files as $file) {
    $news->imagenes[] = basename($file['name']);
}
$imagenes = isset($news->imagenes) ? json_encode($news->imagenes) : '';
// vd2($rolesArray);
try {
    if ($news->id === -1) {
        $existe = false;
    } else {
        $existe = Helpers::qryScalar("select u.id from noticia u where u.id = $news->id");
    }
    if ($existe) {
        $select = "UPDATE noticia
          SET titulo = :titulo, texto = :texto, fecha = :fecha, imagenes = '$imagenes' WHERE id = :id";
        $params = array(
          'titulo' => $news->titulo,
          'texto' => $news->texto,
          'fecha' => $news->fecha,
          'id' => $news->id,
        );
    } else {
        $select = "INSERT INTO noticia(
        titulo, texto, fecha, imagenes)
        VALUES(:titulo, :texto, :fecha, '$imagenes')";
        $params = array('titulo' => $news->titulo, 'texto' => $news->texto, 'fecha' => $news->fecha);
    }
    Helpers::qryExec($select, $params);
    /*
    $target_dir = "uploads/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    // Check if image file is a actual image or fake image
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($check !== false) {
            echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "File is not an image.";
            $uploadOk = 0;
        }
    }
    */
    $newsId = $existe ? $news->id : helpers::qryScalar('select LAST_INSERT_ID();');
    foreach ($files as $file) {
        saveImage($file);
    }
    $tr->commit();
    $ret->status = 200;
    $ret->id = $newsId;
} catch (Exception $e) {
    $ret->error = $e->getMessage();
    $ret->status = 'error';
    $ret->id = -1;
    $tr->rollBack();
}
exit(json_encode($ret));

function saveImage($file)
{
    $carpetaDestinoWeb = '/prg/assets/img/';
    $carpetaDestino = '/prg/imagenesParaAssets/';
    $archivoDestino = $carpetaDestino . basename($file['name']);
    $archivoDestinoWeb = $carpetaDestinoWeb . basename($file['name']);
    $tmp_name = $file['tmp_name'];
    if (is_uploaded_file($tmp_name)) {
        if (file_exists($archivoDestino)) {
            unlink($archivoDestino);
        }
        try {
            copy($tmp_name, $archivoDestinoWeb);
            if (!move_uploaded_file($tmp_name, $archivoDestino)) {
                die('Could not move file');
            }
        } catch (Exception $e) {
            die('File did not upload: ' . $e->getMessage());
        }
    }
    return $archivoDestino;
}
