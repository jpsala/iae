<?php
$division_id= $this->get['division_id'];
$periodo= $this->get['periodo'];
$nivel_id = Helpers::qryScalar("
  SELECT n.id
  FROM nivel n
    INNER JOIN anio a ON n.id = a.Nivel_id
    INNER JOIN division d ON a.id = d.Anio_id
  WHERE d.id = $division_id
");
$categorias= Helpers::qryAll("SELECT * FROM taller_categoria order by orden");
$talleres = Helpers::qryAll("SELECT * FROM taller where LOCATE('$nivel_id', niveles) order by orden");
$valoraciones = Helpers::qryAll("SELECT * from taller_valoracion");

$notas = [];
$selectAlumnos = "
  SELECT a.id, CONCAT(a.apellido, ', ', a.nombre) AS nombre, ad.id AS alumno_division_id
  FROM alumno a
    INNER JOIN alumno_division ad ON a.id = ad.Alumno_id AND ad.Division_id = $division_id AND ad.activo
    INNER JOIN alumno_estado ae ON a.estado_id = ae.id AND ae.activo_edu
    INNER JOIN alumno_division_estado ade ON ad.alumno_division_estado_id = ade.id AND ade.muestra_edu
  order by a.apellido, a.nombre /*limit 1*/";
$alumnos = Helpers::qryAll($selectAlumnos);
foreach ($alumnos as $alumno) {
  $notas[$alumno['id']] = [];
  foreach ($valoraciones as $valoracion) {
    $notasTalleres = [];
    $notasTalleres[0] = $valoracion['nombre'];
    foreach ($talleres as $taller) {
      $selectNota = "
        SELECT tn.*
          FROM taller_nota tn
          WHERE tn.alumno_division_id = :ad_id AND tn.taller_valoracion_id = :tv_id AND tn.taller_id = :t_id AND tn.periodo = :p_id
          order by tn.id asc
          LIMIT 1";
      $nota = Helpers::qry($selectNota, [
        "ad_id" => $alumno['alumno_division_id'],
        "tv_id" => $valoracion['id'],
        "t_id" => $taller['id'],
        "p_id" => $periodo,
      ]);
      if(!$nota){
        $nota = new stdClass();
        $nota->id = uniqid();
        $nota->nota = '';
        $nota->nueva = true;
        $nota->alumno_division_id = $alumno['alumno_division_id'];
        $nota->taller_valoracion_id =$valoracion['id'];
        $nota->taller_id = $taller['id'];
        $nota->periodo = $periodo;
      } else {
        $nota['nueva'] = false;
      }
      $notasTalleres[] = $nota;
    }
    $notas[$alumno['id']][]=$notasTalleres;
  }
}
$this->resp->data = new stdClass();
$this->resp->data->alumnos = $alumnos;
$this->resp->data->valoraciones = $valoraciones;
$this->resp->data->categorias = $categorias;
$this->resp->data->talleres= $talleres;
$this->resp->data->notas = $notas;

exit(json_encode($this->resp));