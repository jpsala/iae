<?php
$periodo = 0;
$categorias= Helpers::qryAll("SELECT * FROM taller_categoria order by orden");
$talleres = Helpers::qryAll("SELECT * FROM taller");
$valoraciones = Helpers::qryAll("SELECT * from taller_valoracion");
$selectAlumnos = "
  SELECT a.id, CONCAT(a.apellido, ', ', a.nombre) AS nombre, ad.id AS alumno_division_id
  FROM alumno a
    INNER JOIN alumno_division ad ON a.id = ad.Alumno_id AND ad.activo
    INNER JOIN alumno_estado ae ON a.estado_id = ae.id AND ae.activo_edu
    INNER JOIN alumno_division_estado ade ON ad.alumno_division_estado_id = ade.id AND ade.muestra_edu
    where (select count(*) from taller_nota n where n.alumno_division_id = ad.id) > 0
  order by a.apellido, a.nombre /*limit 1*/";
$alumnos = Helpers::qryAll($selectAlumnos);
$tr = Yii::app()->db->beginTransaction();
foreach ($alumnos as $alumno) {
  foreach ($valoraciones as $valoracion) {
    foreach ($talleres as $taller) {
      $selectNotas = "
        SELECT tn.*
          FROM taller_nota tn
          WHERE tn.alumno_division_id = :ad_id AND tn.taller_valoracion_id = :tv_id AND tn.taller_id = :t_id AND tn.periodo = :p_id
          order by tn.id asc";
      $notas = Helpers::qryAll($selectNotas, [
        "ad_id" => $alumno['alumno_division_id'],
        "tv_id" => $valoracion['id'],
        "t_id" => $taller['id'],
        "p_id" => $periodo,
      ]);
      if($notas and count($notas) > 1 ) {
        echo 'este es'.$notas[0]['id'].'-'.$notas[0]['nota'].' '.'borrar ';
        for ($i=1; $i < count($notas); $i++) {
          //  echo $notas[$i]['id'].'-'.$notas[$i]['nota'].', ';
          $borra = $notas[$i]['id'];
          Helpers::qryExec("
            delete from taller_nota where id = $borra
          ");
        }
        echo '<br>';
      }
    }
  }
}
$tr->commit();
