<?php
//hola
$socios = Helpers::qryAll("
  SELECT p.id, p.apellido, p.nombre, p.telefono_celular, telefono_ant, (
    SELECT GROUP_CONCAT(CONCAT( ' (', a.nombre, '  ', n.nombre,  ' ', a1.nombre,  '  ', d.nombre,  ' )' ) )
        from alumno a
            INNER JOIN alumno_division ad ON a.id = ad.Alumno_id AND ad.activo /*AND !ad.borrado*/
            INNER JOIN alumno_division_estado ade ON ad.alumno_division_estado_id = ade.id /*AND ade.muestra_admin*/
            INNER JOIN division d ON ad.Division_id = d.id
            INNER JOIN anio a1 ON d.Anio_id = a1.id
            INNER JOIN nivel n ON a1.Nivel_id = n.id
        WHERE a.Familia_id = p.Familia_id
    ) AS alumnos
    FROM pariente p
    WHERE  (
        SELECT COUNT(*) FROM pariente p1
          INNER JOIN familia f ON p1.Familia_id = f.id
          INNER JOIN alumno a ON f.id = a.Familia_id
          INNER JOIN alumno_division ad ON a.id = ad.Alumno_id AND ad.activo AND !ad.borrado
          INNER JOIN alumno_division_estado ade ON ad.alumno_division_estado_id = ade.id AND ade.muestra_admin
          WHERE a.Familia_id = p.Familia_id
      ) >= 0 and (p.telefono_celular like '%-%' or p.telefono_celular like '% %' or p.telefono_celular like '^0%' or p.telefono_celular like '^15%')
    ORDER BY p.apellido, p.nombre");

$this->resp->socios = $socios;

exit(json_encode($this->resp));
