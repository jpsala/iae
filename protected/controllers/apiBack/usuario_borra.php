<?php


$ret = new stdClass();
// vd($_REQUEST)
// vd($this->body);
// $doc = $this->body->doc
// $items = $this->body->items
$tr                    = Yii::app()->db->beginTransaction();
$usuario               = $this->get['id'];
$force                 = isset($this->get['force']) ? ($this->get['force'] === 'false' ? false : true) : false;
$deleteAuthAssignments = "delete from authassignment where userid = $usuario";
$deleteSocio           = "delete from socio where empleado_id = $usuario";
$deleteUsuario         = "delete from user where id = $usuario";
$cantDocsSelects       = 'SELECT count(id) FROM doc WHERE user_id = '.$usuario .' or user_id_empleado = '. $usuario;
$cantDocs               = Helpers::qryScalar($cantDocsSelects);
if (!$force and $cantDocs) {
    $ret->cantDocs    = $cantDocs;
    $ret->error = 'Error al eliminar usuarios, tiene documentos';
} else {
    if ($force and $cantDocs) {
        $existeUsuarioDeReemplazo = Helpers::qryScalar('select id from user where id = 1111111');
        if (!$existeUsuarioDeReemplazo) {
            Helpers::qryExec("INSERT INTO user(id,nombre,apellido,login, password) VALUE(1111111,'usuario borrado','usuario borrado', 'usuarioBorrado', id)");
            /*
            Error al eliminar usuario: CDbCommand falló al ejecutar la sentencia
            SQL: SQLSTATE[23000]: Integrity constraint violation: 1451 Cannot delete or update a parent
            row: a foreign key constraint fails
            (`iae-nuevo`.`log`, CONSTRAINT `FK_log_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)).
             The SQL statement executed was: delete from user where id = 96
            */
        }
        Helpers::qryExec("update socio set empleado_id = 1111111 where empleado_id = $usuario");
        Helpers::qryExec("update doc set user_id_empleado = 1111111 where user_id_empleado = $usuario");
        Helpers::qryExec("update doc set user_id = 1111111 where user_id = $usuario");
        Helpers::qryExec("update log set user_id = 1111111 where user_id = $usuario");
        Helpers::qryExec("update doc_apl_cab set user_id = 1111111 where user_id = $usuario");
        Helpers::qryExec("update comentario set user_id = 1111111 where user_id = $usuario");
        Helpers::qryExec("update conciliacion set user_id = 1111111 where user_id = $usuario");
    }

    try {
        Helpers::qryExec($deleteAuthAssignments);
        Helpers::qryExec($deleteSocio);
        Helpers::qryExec($deleteUsuario);
        $tr->commit();
    } catch (Exception $e) {
        $ret->error = 'Error al eliminar usuario: '.$e->getMessage();
        $tr->rollBack();
    }

    $ret->status = '200';
}//end if

exit(json_encode($ret));
