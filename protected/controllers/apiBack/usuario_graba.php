
<?php

$connection = ssh2_connect('iaeg.dyndns.org', 22, array('hostkey'=>'ssh-rsa'));
if (ssh2_auth_pubkey_file(
    $connection,
    'jpsala',
    '/prg/iae/id_rsa.pub',
    '/prg/iae/id_rsa'
)) {
    echo "Public Key Authentication Successful\n";
} else {
    die('Public Key Authentication Failed');
}
die;
$ret = new stdClass();
$file = (isset($_FILES) and count($_FILES) > 0) ? $_FILES['image'] : false;
$tr = Yii::app()->db->beginTransaction();
$usuario = json_decode($this->post["usuario"]);
$rolesArray = $usuario->roles;
if ($file) {
    $carpetaDestinoWeb = '/prg/assets/fotos/';
    $carpetaDestinoWebThumbnails = '/prg/assets/fotos/thumbnails/';
    $archivoDestinoWeb = $carpetaDestinoWeb . $usuario->documento . '.jpg';
    $archivoDestinoWebThumbnail = $carpetaDestinoWebThumbnails . $usuario->documento . '.jpg';
    
    $carpetaDestinoParaMoverAGoogle = '/prg/fotosParaAssets/';
    $archivoDestinoParaMoverAGoogle = $carpetaDestinoParaMoverAGoogle . $usuario->documento . '.jpg';
    $archivoDestinoThumbnailParaMoverAGoogle = $carpetaDestinoWebThumbnails . $usuario->documento . '.gif';
    $archivoDestinoThumbnailSmallParaMoverAGoogle = $carpetaDestinoWebThumbnails . $usuario->documento . '-small.gif';

    $tmp_name = $file['tmp_name'];
    if (is_uploaded_file($tmp_name)) {
        if (file_exists($archivoDestinoParaMoverAGoogle)) {
            unlink($archivoDestinoParaMoverAGoogle);
        }
        try {
            if (!move_uploaded_file($tmp_name, $archivoDestinoParaMoverAGoogle)) {
                die('Could not move file');
            }
            copy($archivoDestinoParaMoverAGoogle, $archivoDestinoWeb);
            resize($archivoDestinoParaMoverAGoogle);
            copy($carpetaDestinoWebThumbnails . $usuario->documento . '.gif', $archivoDestinoWebThumbnail);
            copy($carpetaDestinoWebThumbnails . $usuario->documento . '.gif', $archivoDestinoThumbnailParaMoverAGoogle);
            copy($carpetaDestinoWebThumbnails . $usuario->documento . '-small.gif', $archivoDestinoThumbnailSmallParaMoverAGoogle);
        } catch (Exception $e) {
            die('File did not upload: ' . $e->getMessage());
        }
    }
}
if ($usuario->id === -1) {
    $existe = false;
} else {
    $existe = Helpers::qryScalar("select u.id from user u where u.id = $usuario->id");
}
if ($existe) {
    $select = "UPDATE user
        SET nombre = :nombre, apellido = :apellido, login = :login WHERE id = :id";
    $params = array(
        'nombre' => $usuario->nombre,
                'apellido' => $usuario->apellido,
                'login' => $usuario->login,
        'id' => $usuario->id,
    );
} else {
    $select = "INSERT INTO user(
        nombre, apellido, login)
        VALUES(:nombre, :apellido, :login)";
    $params = array('nombre' => $usuario->nombre, 'apellido'=>$usuario->apellido, 'login'=>$usuario->login);
}
try {
    Helpers::qryExec($select, $params);
    $userId = $existe ? $usuario->id : helpers::qryScalar('select LAST_INSERT_ID();');
    $deleteAuth = "
      delete from authassignment where userid = $userId;
    ";
    Helpers::qryExec($deleteAuth);
    foreach ($rolesArray as $key => $role) {
        $insertAuth = "insert into authassignment(userid,itemname) values (:userId, :name)";
        Helpers::qryExec($insertAuth, ["userId" => $userId, "name" =>  $role]);
    }
    if (!$existe) {
        // vd2("select userid from authassignment where userid = $userId and itemname = 'Profesor'", "insert into authassignment(userid, itemname) value($userId, 'Profesor)");
        $existeAuthProfesor = Helpers::qryScalar("select userid from authassignment where userid = $userId and itemname = 'Profesor'");
        if (!$existeAuthProfesor) {
            Helpers::qryExec("insert into authassignment(userid, itemname) value(:userId, :itemname)", ["userId"=>$userId, "itemname"=>"Profesor"]);
        }
    }
    $tr->commit();
    $ret->status = 200;
    $ret->id = $userId;
} catch (Exception $e) {
    $ret->error = 'Error al crear usuario: '.$e->getMessage();
    $ret->status = 'error';
    $ret->id = -1;
    $tr->rollBack();
}
exit(json_encode($ret));

function resize($target_file)
{
    try {
        //throw exception if can't move the file
        $cmd = "/prg/iae.api/resize.sh " . $target_file;
        exec($cmd . ' 2>&1');
        // $cmd = "/prg/iae.api/cp_test.sh " . $target_file;
        // exec($cmd . ' 2>&1');
    } catch (Exception $e) {
        die('File did not upload: ' . $e->getMessage());
    }
}
