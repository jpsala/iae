<?php
// $division_id= $this->get['division_id'];
// $socioId= $this->get['socio_id'];
$selectUsuarios = "
  SELECT u.id, u.nombre, u.apellido, u.login, u.cambiar_password, u.documento,
    (SELECT GROUP_CONCAT(itemname) FROM authassignment a WHERE a.userid = u.id) AS rolesParaMostrar,
    (SELECT count(d.id) 
      FROM socio s 
        inner join doc d on d.socio_id = s.id
      WHERE user_id = u.id) AS Docs
  FROM user u where u.documento order by u.apellido, u.nombre";
  // vd2($selectUsuarios);
  $usuarios = Helpers::qryAll($selectUsuarios);
// foreach ($usuarios as $key => $user) {
//   ve2($user['Docs']);
// }
// vd2('fin');
$items = Helpers::qryAll("select name, description from authitem where type = 2");
foreach ($usuarios as $key => $usuario) {
    $itemsAssignedRaw = Helpers::qryAll("select itemname from authassignment where userid = " . $usuario["id"]);
    $itemsAssigned = [];
    foreach ($itemsAssignedRaw as $value) {
        $itemsAssigned[] = $value["itemname"];
    }
    $usuarios[$key]["roles"] = [];
    foreach ($items as $item) {
      // $role = new stdClass();
      // $role->name = $item["name"];
      // $role->value = in_array($item["name"], $itemsAssigned);
        if (in_array($item["name"], $itemsAssigned)) {
            $usuarios[$key]["roles"][] = $item["name"];
        }
    }
  // vd2($itemsAssigned, $items);
}
$this->resp->data = new stdClass();
$this->resp->data->usuarios = $usuarios;
$this->resp->data->roles = $items;

exit(json_encode($this->resp));
