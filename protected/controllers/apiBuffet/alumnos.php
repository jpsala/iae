<?php
$resp = new stdClass();
$q = $this->body->query;
$limit = $this->body->limit;
$con_saldo = isset($this->body->con_saldo)?$this->body->con_saldo:false;
$resp->success = true;
// $resp->results = Helpers::qryAll('select id as value, id, nombre as description, nombre as title, nombre as text, false as disabled from alumno limit 1, 10');
$resp->body = $this->body;
$pos = $pos = strpos($q, ',');
$apellido = $q;
$nombre = '';
if($pos){
  $parts = explode(',', $q);
  $apellido = $parts[0];
  $nombre = $parts[1];
}
$where = "a.apellido like '%$apellido%' and a.nombre like '%$nombre%'";
if($con_saldo){
  $where .= " and ( select sum(d.saldo) from buffet_doc d where d.saldo !=0 and d.socio_id = s.id and d.tipo = 't') > 0  ";
}
$selectAlumnos = "
  select s.id, concat(a.apellido, \", \", a.nombre) as title,
         concat(an.nombre, \" \", d.nombre, ' DNI:', a.numero_documento) as description
  from alumno a
    inner join alumno_division ad on ad.alumno_id = a.id and ad.activo
    inner join division d on d.id = ad.division_id
    inner join anio an on an.id = d.anio_id
    inner join socio s on s.alumno_id = a.id
  where $where limit $limit
";
$resp->select = $selectAlumnos;
$resp->data = Helpers::qryAll($selectAlumnos);
exit(json_encode($resp));
