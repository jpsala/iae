<?php
$ret = new stdClass();
$ret->status = 200;
//exit(json_encode($ret));
$socio_id = $this->body->socio_id;
Helpers::qryExec("
	update buffet_doc d set  d.saldo = d.total;
	update buffet_doc_det d set  d.saldo = d.importe;"
);
//die;
$selectNC =
	"select d.id, d.saldo
					from buffet_doc d
				where socio_id = $socio_id and d.saldo > 0 and tipo = 'c'";
$NCs = Helpers::qryAll($selectNC);
$selectDocs = "
        select d.id, d.saldo
					from buffet_doc d
				where socio_id = $socio_id and d.saldo > 0 and tipo = 't'";
$docs = Helpers::qryAll($selectDocs);
$selectDets = "
        SELECT d.id, d.saldo
					FROM buffet_doc_det d
				WHERE d.doc_id = :id and d.saldo > 0";
$tr = Yii::app()->db->beginTransaction();
foreach ($NCs as $nc) {
	$saldoNC = $nc['saldo'];
	foreach ($docs as $doc) {
		$saldoDoc = $doc['saldo'];
//		ve('saldoDocAntes',$saldoDoc);
		$dets = Helpers::qryAll($selectDets, array('id' => $doc['id']));
		foreach ($dets as $det) {
			if ($saldoNC > 0) {
				if ($saldoNC >= $det['saldo']) {
					$saldoDoc -= $det['saldo'];
					$saldoNC -= $det['saldo'];
					$det['saldo'] = 0;
//					var_dump('aplica nc '.$saldoNC .' sobre det '.$det[saldo]);
				} else {
					$det['saldo'] -= $saldoNC;
					$saldoDoc -= $saldoNC;
					$saldoNC = 0;
//					var_dump('aplica nc '.$saldoNc .' sobre det '.$det[saldo]);
				}
				Helpers::qryExec("
					UPDATE buffet_doc_det SET saldo = :saldo WHERE id = :id",
					array('saldo' => $det['saldo'], 'id' => $det['id'])
				);
			}
		}
//		ve('saldoDocDespues',$saldoDoc);
		Helpers::qryExec("
				UPDATE buffet_doc SET saldo = :saldo WHERE id = :id",
			array('saldo' => $saldoDoc, 'id' => $doc['id'])
		);
	}
	Helpers::qryExec("
				UPDATE buffet_doc SET saldo = :saldo WHERE id = :id",
		array('saldo' => $saldoNC, 'id' => $nc['id'])
	);
}
$tr->commit();
$NCs = Helpers::qryAll($selectNC);
$docs = Helpers::qryAll($selectDocs);
//ve($NCs,$docs);

exit(json_encode($ret));
