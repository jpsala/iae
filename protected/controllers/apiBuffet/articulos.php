<?php
$ret = new stdClass();
$paraBuffet = isset($this->body->para_buffet) ? $this->body->para_buffet : false;
$entityBody = json_decode(file_get_contents('php://input'));
$orden = $paraBuffet ? 'order by ba.categoria_id, ba.nombre' : 'order by ba.orden, ba.nombre';
$select = "
        SELECT
            ba.id, ba.nombre, ba.precio_venta, ba.imagen, ba.categoria_id,
            c.nombre as categoria_nombre, ba.comentarios, ba.borrado, ba.orden, ba.disponible
            FROM buffet_articulo ba
                inner join buffet_categoria c on c.id = ba.categoria_id
            where !ba.borrado and ! c.borrado
            $orden
        ";
$arts = Helpers::qryAll($select);
foreach ($arts as $key => $art) {
    $selectSub = "SELECT id, nombre, precio_venta
        FROM buffet_articulo_sub
        WHERE articulo_id = " . $art["id"] ." order by nombre
          ";
    $artsSub = Helpers::qryAll($selectSub);
    if (!$artsSub) {
        $artsSub = $art;
        $artsSub['nombre'] = '';
    }
    $arts[$key]['subs'] = $artsSub;
}
$ret->status = 200;
$ret->access_token = session_id();
$ret->data = $arts;
exit(json_encode($ret));
