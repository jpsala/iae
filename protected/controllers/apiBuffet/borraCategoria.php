<?php
$ret = new stdClass();
$tr = Yii::app()->db->beginTransaction();
$categoria = $this->body;
$select = "update buffet_categoria set borrado = 1 WHERE id = :id";
Helpers::qryExec($select, ['id'=>$categoria->id]);
$tr->commit();
$ret->status = 'ok';
exit(json_encode($ret));
