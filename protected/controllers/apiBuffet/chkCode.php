<?php
$code = $this->body;
$matricula = substr($code, 0 , 4);
$doc = substr($code, 4);
$documento_completo = str_replace(',', '', $code);
$documento_completo = str_replace('.', '', $documento_completo);
$documento_completo = str_replace(' ', '', $documento_completo);
$ret = new stdClass();
// Es alumno?
$s = "
    select s.id as socio_id, admin_buffet as is_admin,
        case when a.id
            then concat(a.apellido, ' ', a.nombre)
            when p.id then concat(p.apellido, ' ', p.nombre)
            else concat(e.apellido, ' ', e.nombre)
        end as nombre, saldo_buffet(s.id, null) as saldo, s.credito_buffet as credito, a.numero_documento
    from socio s
        left join alumno a on a.id = s.Alumno_id
        left join pariente p on p.id = s.Pariente_id
        left join empleado e on e.id = s.empleado_id
    where a.matricula = $matricula
";
$socio = Helpers::qry($s);
if(!$socio or substr($socio['numero_documento'], -4) !== $doc){
  $s = "
      select s.id as socio_id, admin_buffet as is_admin,
          case when a.id
              then concat(a.apellido, ' ', a.nombre)
              when p.id then concat(p.apellido, ' ', p.nombre)
              else concat(e.apellido, ' ', e.nombre)
          end as nombre, saldo_buffet(s.id, null) as saldo, s.credito_buffet as credito, p.numero_documento
      from socio s
          left join alumno a on a.id = s.Alumno_id
          left join pariente p on p.id = s.Pariente_id
          left join empleado e on e.id = s.empleado_id
          left join user u on u.id = s.empleado_id
      where p.numero_documento = $documento_completo or e.documento = $documento_completo or u.documento = $documento_completo
  ";
  // vd2($s);
  $socio = Helpers::qry($s);
}
if($socio){
  $ret->nombre = $socio['nombre'];
  $ret->id = $socio['socio_id'];
  $ret->status=200;
} else {
    $ret->status = 401;
}

exit(json_encode($ret));
