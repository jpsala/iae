<?php
$ret = new stdClass();
//vd($this->body);
$doc = $this->body->doc;
$items = $this->body->items;
$esBuffet = isset($this->body->es_buffet) ? $this->body->es_buffet : false;
// vd($esBuffet);
$tr = Yii::app()->db->beginTransaction();
//var_dump($items, $doc);die;
	$fecha = date('Y/m/d');
Helpers::qryExec(
	"INSERT INTO buffet_doc(fecha, socio_id, total, saldo, descuento) VALUES(:fecha, :socio_id, :total, :total, :descuento)
		", ['fecha' => $fecha, 'socio_id' => $doc->socio_id, 'total' => $doc->total, 'descuento' => $doc->descuento]);
$doc_id = helpers::qryScalar('select LAST_INSERT_ID();');
foreach ($items as $key => $item) {
	if (!isset($item->subId)) {
		$item->subId = Null;
		$item->subNombre = null;
	}
	Helpers::qryExec(
		"
				insert into buffet_doc_det(doc_id, articulo_id, detalle, importe, sub_articulo_id, saldo, cantidad)
							 values($doc_id, :articulo_id, :detalle, :importe, :subId, :importe, :cantidad)",
		array(
			'articulo_id' => $item->id,
			'detalle' => $item->nombre . ' ' . $item->subNombre,
			'importe' => $item->precio_venta * $item->cantidad,
			'cantidad' => $item->cantidad,
			'subId' => $item->subId));
}

$tr->commit();
$ret->status = 'ok';
$data = new stdClass();
$data->cliente = Helpers::qry("
	select concat(a.nombre, ', ', a.apellido) as nombre from socio s
		left join alumno a on s.alumno_id = a.id
	where s.id = $doc->socio_id
");
$doc->id = $doc_id;
$data->doc = $doc;
$data->items = $items;
//		$ret->data = $_REQUEST;
// exit(json_encode($ret));
//		exit(json($ret,



class MYPDF extends PDF {

	public $planilla;

	public function header() {

	}

	public function footer() {

	}

	public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false) {
		parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
	}

}

class Informe extends stdClass {
//
	public $pdf;
//
 	public function __construct($paperSize = "A6") {
		$this->paperSize = $paperSize;
		$this->pdf = new MYPDF("P", "mm", $paperSize);
		$this->prepara();
 	}
//
// 	public function imprime($imprimeAuto, $nombre_pc, $impresora) {
// 	}
//
// 	private function preparaPdf() {
// 	}
//

	public function prepara(){
		$this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", 18);
		$this->pdf->setCellPaddings(1, 2, 0, 0);
		$this->pdf->AddPage();
		$this->fontSize = 18;
	}

 	public function imprime($data) {
		$pdf = $this->pdf;
		$marginx = 10;
		$marginy = 0;
		$marginRight = 0;
		$fontSize = 18;
		$headerFontStyle = null;
		$fontStyle = null;
		$center = $pdf->getPageWidth() / 2;
		$x = $marginx;
		$y = $marginy;
		$lineHeight = 6;
		$pdf->Text( $x, $y  ,"#: " . $data->doc->id, null, null);
		$y += $lineHeight;
		$pdf->Text($x, $y  ,$data->cliente['nombre']);
		$y += $lineHeight;
		$pdf->Text($x, $y  ,"Fecha: " . date("d/m/Y", time()));
		$y += $lineHeight;
		$pdf->Text( $x, $y  ,"Hora: " . date("G:i", time()), null, null);
		$y += $lineHeight + 5;
		$colNombreX = 10;
		$colPrecioX = $pdf->getPageWidth();
		$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(128, 0, 0));
//		$pdf->Line($marginx, $y, $pdf->getPageWidth() - $marginRight, $y, 80, 30, $style);
		foreach($data->items as $item){
//			$pdf->Text($colNombreX, $y, $item->cantidad.' '.substr($item->nombre,0,18), null, null);
    $producto = $item->cantidad.' '.$item->nombre;
    $nombreWidth = $pdf->getPageWidth()- $marginRight - $colNombreX - 22;
//    $nombreWidth = 80;
//    $producto . $nombreWidth;
    $pdf->MultiCell(
          $nombreWidth,
          0, $producto, 0, '', false, 1, $colNombreX, $y);
      $ultimoY = $pdf->GetY();
			$pdf->Text($pdf->getPageWidth()- $marginRight + 5, $y+1, $item->precio_venta*$item->cantidad." $" , null, null, true,null,null,'R');
			$y = $ultimoY;
		}
  if($data->doc->descuento > 0){
			$pdf->Text($colNombreX, $y, 'Descuento', null, null);
			$pdf->Text($pdf->getPageWidth()- $marginRight, $y, '-'.$data->doc->descuento." $" , null, null, true,null,null,'R');
			$y += $lineHeight;
  }
//		$pdf->Line($marginx, $y, $pdf->getPageWidth() - $marginRight, $y, 80, 30, $style);
		$pdf->Text($colNombreX, $y, "Total", null, null);
		$pdf->Text($pdf->getPageWidth()- $marginRight, $y, $data->doc->total." $" , null, null, true,null,null,'R');

 	}

	public function guarda($esBuffet = false){
		$path = "./docsAutoPrint/buffet/";
		// $unique = uniqid();
		$unique = date('YmdHisu');
		$f = $path . ($esBuffet ? "buffet_$unique.pdf":"caja_$unique.pdf");
		$this->pdf->Output($f, 'F');
	}
//
}
    if($data->doc->total) {
        $i = new Informe();
        $i->imprime($data);

        $i->guarda($esBuffet);
    }

exit(json_encode($ret));
