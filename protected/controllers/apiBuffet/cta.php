<?php
$entityBody = json_decode(file_get_contents('php://input'));
$ret = new stdClass();
$socio_id = $this->body->socio_id;
$fecha = isset($this->body->fecha) ? $this->body->fecha : false;
//vd2($fecha);
$whereFecha = $fecha ? "d.fecha >= '$fecha'" : "true";
$limit = $fecha ? '' : 'limit 10';
// $all = isset($this->body->all) && $this->body->all ? 'true':'d.saldo != 0';
$selectCta = "
  (select d.id, DATE_FORMAT(d.fecha, '%Y/%m/%e') as fecha, d.saldo, d.total,
         case when d.tipo = 't' then 'Consumo' else 'Pago' end as detalle
  from buffet_doc d
  WHERE $whereFecha AND d.socio_id = $socio_id and d.borrado is null
  ORDER BY d.id DESC $limit) order by id asc
";
//var_dump($selectCtas);die;
$cta = Helpers::qryAll($selectCta);
$ret->fecha = $fecha;
if($limit !== '' and count($cta) > 0 and !$fecha){
  $ret->fecha = $cta[0]['fecha'];
}
$ret->limit = $limit;
$ret->saldo = Helpers::qryScalar("select saldo_buffet($socio_id, NULL)");
// $fechaParaSaldoAnt = date('Y/m/d',strtotime($fecha));
$ret->saldoAnt = $fecha ? Helpers::qryScalar("select saldo_ant_buffet($socio_id, '$fecha')") : 0;
$ret->limitf = "select saldo_ant_buffet($socio_id, '$fecha')";
$ret->data = $cta;
$ret->access_token = session_id();
$ret->status = 200;
// $ret->credito = Helpers::qryScalar("select sum(d.saldo) from buffet_doc d where d.saldo < 0");
exit(json_encode($ret));
