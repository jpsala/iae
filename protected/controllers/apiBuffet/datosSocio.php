<?php
$ret = new stdClass();
$socio_id = $this->body->socio_id;
$select = "
  select s.id, s.login_buffet, e.login, s.password_buffet, s.admin_buffet, saldo_buffet(s.id, null) as saldo,
     case
      when a.id then concat(a.apellido,', ', a.nombre)
      when e.id then e.nombre
      else concat(p.apellido, ', ', p.nombre)
     end as nombre,
     case
      when a.id then 'alumno'
      when e.id then 'empleado'
      when p.id then 'pariente'
     end as tipo,
     case
      when a.id then
       concat(
           an.nombre, ' ', d.nombre,
           ' DNI:', a.numero_documento
        )
      when e.id then 'Empleado'
      when p.id then t.nombre
     end as description
  from socio s
    left join alumno a on a.id = s.alumno_id
    left join alumno_division ad on ad.alumno_id = a.id and ad.activo
    left join division d on d.id = ad.division_id
    left join anio an on an.id = d.anio_id
    left join pariente p on p.id = s.pariente_id
    left join pariente_tipo t on p.pariente_tipo_id = t.id
    left join user e on e.id = s.empleado_id
  where s.id = $socio_id
  ";
$data = Helpers::qry($select);
$ret->status = 200;
$ret->data = $data;
exit(json_encode($ret));
