<?php
$ret = new stdClass();
$select = "
  SELECT u.nombre, u.apellido, u.login, u.documento, u.borrado, u.celular, u.legajo, 
         s.password_buffet, s.admin_buffet AS admin, s.credito_buffet, saldo_buffet(s.id, null) as saldo, 
         0 AS nuevo_pago
  FROM socio s
    INNER JOIN user u ON s.Empleado_id = u.id
  order by u.apellido, u.nombre
  ";
$data = Helpers::qryAll($select);
$ret->status = 200;
$ret->data = $data;
exit(json_encode($ret));
