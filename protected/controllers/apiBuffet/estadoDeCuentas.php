<?php
$entityBody = json_decode(file_get_contents('php://input'));
$ret = new stdClass();
// $fecha = isset($this->body->fecha) ? $this->body->fecha : false;
//vd2($fecha);
// $whereFecha = $fecha ? "d.fecha >= '$fecha'" : "true";
// $limit = $fecha ? '' : 'limit 10';
// $all = isset($this->body->all) && $this->body->all ? 'true':'d.saldo != 0';
$selectCta = "
    SELECT sv.id, sv.nombreCompleto as nombre, 
         saldo_buffet(sv.id, null) * -1 AS total,
         (select count(bd2.borrado) from buffet_doc bd2 where bd2.socio_id = sv.id and bd2.borrado and bd2.motivo_borrado != 'xxx') as con_borrados, 
         (SELECT DATE_FORMAT(MAX(bd.fecha), '%d/%m/%Y') FROM buffet_doc bd WHERE bd.socio_id = sv.id) AS ultima_fecha
    FROM buffet_doc d
      INNER JOIN socio_view sv ON sv.id = d.socio_id AND sv.id != 42593
    where (select count(*) from buffet_doc_det det where det.doc_id = d.id) > 0
    GROUP BY sv.id
    order by d.fecha desc

    ";
// var_dump($selectCta);die;
$cta = Helpers::qryAll($selectCta);
// $ret->fecha = $fecha;
// if($limit !== '' and count($cta) > 0 and !$fecha){
//   $ret->fecha = $cta[0]['fecha'];
// }
// $ret->limit = $limit;
// $ret->saldo = Helpers::qryScalar("select saldo_buffet($socio_id, NULL)");
// $fechaParaSaldoAnt = date('Y/m/d',strtotime($fecha));
// $ret->saldoAnt = $fecha ? Helpers::qryScalar("select saldo_ant_buffet($socio_id, '$fecha')") : 0;
// $ret->limitf = "select saldo_ant_buffet($socio_id, '$fecha')";
$ret->data = $cta;
// $ret->access_token = session_id();
$ret->status = 200;
// $ret->credito = Helpers::qryScalar("select sum(d.saldo) from buffet_doc d where d.saldo < 0");
exit(json_encode($ret));