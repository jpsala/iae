<?php
$ret = new stdClass();
//		vd($_REQUEST);
// vd($this->body);
// $doc = $this->body->doc;
// $items = $this->body->items;
$tr = Yii::app()->db->beginTransaction();
$articulo = $this->body;
$existe = Helpers::qryScalar("select count(*) from buffet_articulo a where a.id = $articulo->id");
if ($existe) {
	$select = "UPDATE buffet_articulo
        SET nombre = :nombre, precio_venta=:precio_venta,imagen=:imagen,
            categoria_id = :categoria_id, orden = :orden WHERE id = :id";
	$params = array(
        'nombre' => $articulo->nombre, 'precio_venta' => $articulo->precio_venta,
        'imagen'=>$articulo->imagen, 'categoria_id' => $articulo->categoria_id, 
        'orden'=>$articulo->orden, 'id' => $articulo->id
    );
} else {
	$select = "INSERT INTO buffet_articulo(
        nombre, imagen, categoria_id, precio_venta, orden)
        VALUES(:nombre, :imagen, :categoria_id, :precio_venta, :orden)";
	$params = array('nombre' => $articulo->nombre, 
        'precio_venta' => $articulo->precio_venta, 
        'categoria_id' => $articulo->categoria_id, 
        'imagen'=>$articulo->imagen, 'orden' => $articulo->orden);
}
Helpers::qryExec($select, $params);
$articulo_id = helpers::qryScalar('select LAST_INSERT_ID();');
$tr->commit();
$ret->status = 'ok';
$ret->id = $articulo_id;
exit(json_encode($ret));
