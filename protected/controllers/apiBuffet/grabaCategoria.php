
<?php
$ret = new stdClass();
//		vd($_REQUEST);
// vd($this->body);
// $doc = $this->body->doc;
// $items = $this->body->items;
$tr = Yii::app()->db->beginTransaction();
$categoria = $this->body;
$existe = Helpers::qryScalar("select count(*) from buffet_categoria c where c.id = $categoria->id");
if ($existe) {
	$select = "UPDATE buffet_categoria
        SET nombre = :nombre, imagen = :imagen WHERE id = :id";
	$params = array(
        'nombre' => $categoria->nombre,
				'imagen' => $categoria->imagen,
        'id' => $categoria->id
    );
} else {
	$select = "INSERT INTO buffet_categoria(
        nombre, imagen)
        VALUES(:nombre, :imagen)";
	$params = array('nombre' => $categoria->nombre, 'imagen'=>$categoria->imagen);
}
Helpers::qryExec($select, $params);
$categoria_id = helpers::qryScalar('select LAST_INSERT_ID();');
$tr->commit();
$ret->status = 'ok';
$ret->id = $categoria_id;
exit(json_encode($ret));
