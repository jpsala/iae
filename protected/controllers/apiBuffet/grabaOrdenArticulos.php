<?php
$ret = new stdClass();
//		vd($_REQUEST);
// vd($this->body);
// $doc = $this->body->doc;
// $items = $this->body->items;
$tr = Yii::app()->db->beginTransaction();
$articulos = $this->body;
$orden = 0;
foreach($articulos as $articulo_id){
	$select = "UPDATE buffet_articulo
        SET orden = :orden WHERE id = :id";
	$params = array(
        'orden'=>$orden++, 'id' => $articulo_id
    );
  Helpers::qryExec($select, $params);
}
$tr->commit();
$ret->status = 'ok';
exit(json_encode($ret));
