<?php
$ret = new stdClass();
$ret->status = 'ok';
$dir = file_exists('../buffet/images') ? '../buffet/images' : '../buffet.dev/images';
$f=array_diff(scandir($dir), array('..','.'));
$ret->data = [];
$id = 0;
foreach($f as $i){
	if(strpos(strtoupper($i), '.JPG') or strpos(strtoupper($i), '.PNG')){
		$ret->data[]=['id'=>$id++,'nombre'=> $i];
	}
}

exit(json_encode($ret));
