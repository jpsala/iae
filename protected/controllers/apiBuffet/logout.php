<?php
$this->resp->status = 200;
if(session_id()){
  session_destroy();
  $_SESSION = [];
}
exit(json_encode($this->resp));
