<?php
$doc_id = $_GET['doc_id'];
$fecha = date('Y/m/d');
$data = Helpers::qry("
  select d.*,
      case 
        when a.id then concat(a.apellido, ', ', a.nombre) 
        when e.id then concat(e.apellido, ', ', e.nombre)
        when p.id then concat(p.apellido, ', ', p.nombre)
      end as nombre
    from buffet_doc d
      left join socio s on s.id = d.socio_id
      left join alumno a on a.id = s.alumno_id
      left join user e on e.id = s.empleado_id
      left join pariente p on p.id = s.pariente_id
    where d.id = $doc_id
  ");
class MYPDF extends PDF {

	public $planilla;

	public function header() {

	}

	public function footer() {

	}

	public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false) {
		parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
	}

}

class Informe extends stdClass {
//
	public $pdf;
//
 	public function __construct($paperSize = "A6") {
		$this->paperSize = $paperSize;
		$this->pdf = new MYPDF("P", "mm", $paperSize);
		$this->prepara();
 	}
//
// 	public function imprime($imprimeAuto, $nombre_pc, $impresora) {
// 	}
//
// 	private function preparaPdf() {
// 	}
//

	public function prepara(){
		$this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", 11);
		$this->pdf->setCellPaddings(1, 2, 0, 0);
		$this->pdf->AddPage();
		$this->fontSize = 12;
	}

 	public function imprime($data) {
		$pdf = $this->pdf;
		$marginx = 10;
		$marginy = 0;
		$marginRight = 10;
		$fontSize = 10;
		$headerFontStyle = null;
		$fontStyle = null;
		$center = $pdf->getPageWidth() / 2;
		$x = $marginx;
		$y = $marginy;
		$lineHeight = 6;

		$pdf->Text($x, $y  ,"Fecha: " . date("d/m/Y", time()));
		$y += $lineHeight;
		$pdf->Text( $x, $y  ,"Hora: " . date("G:i", time()), null, null);
		$y += $lineHeight;
		$pdf->Text( $x, $y  ,"Cliente: " . $data['nombre'], null, null);
		$y += $lineHeight;
		$colNombreX = 10;
		$colPrecioX = $pdf->getPageWidth();
		$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(128, 0, 0));

		$pdf->Line($marginx, $y, $pdf->getPageWidth() - $marginRight, $y, 80, 30, $style);
		$pdf->Text($colNombreX, $y, "Pago", null, null);
		$pdf->Text($colNombreX + 40, $y, abs($data['total']), null, null);
		$y += $lineHeight;
		// $pdf->Text($colNombreX, $y, "Platos valor 0", null, null);
		// $pdf->Text($colNombreX + 40, $y, $data['descuento'], null, null);
    $pdf->Output('recibo.pdf', 'I');
 	}
}

$informe = new Informe();
$informe->imprime($data);