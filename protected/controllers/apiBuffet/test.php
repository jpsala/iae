<?php
$resp = new stdClass();
// $resp->query = $this->body->query;
$q = $_GET['q'];
$resp->success = true;
// $resp->results = Helpers::qryAll('select id as value, id, nombre as description, nombre as title, nombre as text, false as disabled from alumno limit 1, 10');
$resp->results = Helpers::qryAll("
  select s.id, concat(a.apellido, \", \", a.nombre) as title,
         concat(an.nombre, \" \", d.nombre, ' DNI:', a.numero_documento) as description
  from alumno a
    inner join alumno_division ad on ad.alumno_id = a.id and ad.activo
    inner join division d on d.id = ad.division_id
    inner join anio an on an.id = d.anio_id
    inner join socio s on s.alumno_id = a.id
  where apellido like \"%$q%\" limit 1, 100
");
exit(json_encode($resp));
