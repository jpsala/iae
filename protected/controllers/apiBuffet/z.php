<?php
// $fecha = '2017/07/07';
$fecha = isset($this->body->fecha) ? date('Y/m/d',strtotime($this->body->fecha)) : date('Y/m/d');
$resp = new stdClass();
$resp->success = true;
$resp->data = new stdClass();;
$resp->data->total_buffet = Helpers::qryScalar("
  select sum(d.total)
	from buffet_doc d
  where d.tipo = '' and d.socio_id = 42593
        and d.fecha = '$fecha'
");
$resp->data->total_pagos = Helpers::qryScalar("
  select sum(abs(d.total))
    from buffet_doc d
  where d.tipo = 'p' and d.fecha = '$fecha' and d.socio_id = 42593
");
$resp->data->platosCero = Helpers::qryScalar("
  select count(*)
  from buffet_doc_det d
    inner join buffet_doc bd on bd.id = d.doc_id
  where d.importe = 0 and bd.fecha = '$fecha'
  ");
exit(json_encode($resp));
