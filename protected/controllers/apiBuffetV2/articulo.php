<?php
$id = $this->body->id;
$selectCategorias = "select * from buffet_categoria";
$selectArticulo = "
        SELECT
            ba.id, ba.nombre, ba.precio_venta, ba.imagen, ba.categoria_id,
            c.nombre as categoria_nombre, ba.comentarios, ba.borrado, ba.orden, ba.disponible
            FROM buffet_articulo ba
                inner join buffet_categoria c on c.id = ba.categoria_id
            where ba.id = $id
        ";
// vd2($select);
$this->resp->data = new stdClass();
$this->resp->data->articulo = Helpers::qry($selectArticulo);
$this->resp->data->categorias = Helpers::qryAll($selectCategorias);
exit(json_encode($this->resp));