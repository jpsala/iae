<?php
$select = "
        SELECT
            ba.id, ba.nombre, ba.precio_venta, ba.imagen, ba.categoria_id,
            c.nombre as categoria_nombre, ba.comentarios, ba.borrado, ba.orden,
            ba.disponible, ba.tiene_descuento
            FROM buffet_articulo ba
                inner join buffet_categoria c on c.id = ba.categoria_id
            where !ba.borrado and ! c.borrado
            order by c.orden, c.nombre, ba.nombre
        ";
$arts = Helpers::qryAll($select);
$this->resp->status = 200;
$this->resp->data = new stdClass();
$this->resp->data->articulos = $arts;
$this->resp->data->categorias = Helpers::qryAll('select * from buffet_categoria');
exit(json_encode($this->resp));
