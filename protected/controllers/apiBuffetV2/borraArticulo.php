<?php
$ret = new stdClass();
$tr = Yii::app()->db->beginTransaction();
$id = $this->body;
$existe = Helpers::qryScalar("select count(*) from buffet_articulo a where a.id = $id and !a.borrado");
if ($existe) {
	$existenSubs = Helpers::qryScalar("select count(*) from buffet_articulo_sub a where a.articulo_id = $id");
	if ($existenSubs) {
		$select = "update buffet_articulo_sub set borrado = 1 WHERE articulo_id = :id";
		$params = array('id' => $id);
		Helpers::qryExec($select, $params);
	}
	$select = "update buffet_articulo set borrado = 1 WHERE id = :id";
	$params = array('id' => $id);
	Helpers::qryExec($select, $params);
}
$tr->commit();
$ret->status = 'ok';
exit(json_encode($ret));

