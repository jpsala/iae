<?php
$doc = $this->body->doc;
$items = $this->body->items;
$cliente = isset($this->body->doc->cliente) ? $this->body->doc->cliente : false;
$empleado_id = isset($this->body->socio_id) ? $this->body->socio_id : false;
$esBuffet = isset($this->body->es_buffet) ? $this->body->es_buffet : false;
$pago = isset($doc->pago) ? (float) $doc->pago : 0;
$total = (float) $doc->total;
$aCuenta = 0;
if($pago > $total) {
	$aCuenta = $pago - $total;
	$pago = $total;
}
$fecha = date('Y/m/d H:i:s');
$tr = Yii::app()->db->beginTransaction();

// grabo el pago

if($total > 0) {
	Helpers::qryExec(
		"INSERT INTO buffet_doc(fecha, socio_id, total, saldo, pago, descuento, socio_nombre, tipo)
						VALUES(:fecha, :socio_id, :total, :saldo, :pago, :descuento, :socio_nombre, 't')
			", ['fecha' => $fecha,
					'socio_id' => isset($cliente->id) ? $cliente->id : null,
					'socio_nombre' => isset($cliente->nombre_completo) ? $cliente->nombre_completo : '',
					'total' => $total, 'saldo' => $total - $pago, 'pago' => $pago,
					'descuento' => $doc->descuento]);
	$doc_id = helpers::qryScalar('select LAST_INSERT_ID();');
	foreach ($items as $key => $item) {
		Helpers::qryExec("
				insert into buffet_doc_det(doc_id, articulo_id, detalle, importe, saldo, cantidad)
							values($doc_id, :articulo_id, :detalle, :importe, :importe, :cantidad)",
			array(
				'articulo_id' => $item->id,
				'detalle' => $item->nombre,
				'importe' => $item->precio_venta * $item->cantidad,
				'cantidad' => $item->cantidad));
	}
}

// aplica pago a cuenta y aplica créditos

if(isset($cliente->id)){
	$socioId = $cliente->id;
	$socioNombre = $cliente->nombre_completo;
	$fecha = date('Y/m/d H:i:s');
	if($aCuenta > 0) {
		Helpers::qryExec("
		insert into buffet_doc(fecha,socio_id,total, saldo, pago, tipo, socio_nombre)
			values('$fecha',$socioId, $aCuenta*-1, $aCuenta*-1, 0, 'p', '$socioNombre')
		");
	}
	// aplica creditos
	$pagos = Helpers::qryAll("
		select * from buffet_doc d
			where d.socio_id = $socioId and tipo = 'p' and d.saldo < 0
			ORDER BY d.fecha asc
	");

	$deuda = Helpers::qryAll("
		select * from buffet_doc d
			where d.socio_id = $socioId and tipo = 't' and d.saldo > 0
			ORDER BY d.fecha asc
	");

	foreach ($pagos as $pagoRow) {
		$saldo = floatval($pagoRow['saldo']);

		$resto = aplicaPago($saldo, $deuda);
		if($resto === $saldo) {
			break;
		} else {
			actualiza($pagoRow['id'], $resto*-1);
		}
	}
}

$tr->commit();
$this->resp->status = 'ok';

function aplicaPago($pago, &$deuda) {
	$resto = abs(floatval($pago));
	foreach ($deuda as $key => $deudaRow) {
		$saldo = floatval($deudaRow['saldo']);
		if($resto > $saldo) {
			$resto -= $saldo;
			$saldo = 0;
		} else {
			$saldo -= $resto;
			$resto = 0;
		}
		$deuda[$key]['saldo'] = $saldo;
		actualiza($deudaRow['id'], $saldo);
	}
	return $resto;
}
function actualiza($id, $saldo) {
	Helpers::qryExec("
		update buffet_doc set saldo = $saldo where id = $id
	");
}

// Imresión del ticket

if($total <= 0) { // exit if $total <= 0, no imprime el ticket
	exit(json_encode($this->resp));
}

$data = new stdClass();
if(isset($doc->socio_id)) {
  $data->cliente = Helpers::qry("
    select concat(a.nombre, ', ', a.apellido) as nombre from socio s
      left join alumno a on s.alumno_id = a.id
    where s.id = $doc->socio_id
  ");
} else {
  $data->cliente = ['nombre' => ''];
}

$doc->id = $doc_id;
$data->doc = $doc;
$data->items = $items;
class MYPDF extends PDF {

	public $planilla;

	public function header() {

	}

	public function footer() {

	}

	public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false) {
		parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
	}

}

class Informe extends stdClass {
	public $pdf;
 	public function __construct($paperSize = "A6") {
		$this->paperSize = $paperSize;
		$this->pdf = new MYPDF("P", "mm", $paperSize);
		$this->prepara();
 	}

	public function prepara(){
		$this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", 18);
		$this->pdf->setCellPaddings(1, 2, 0, 0);
		$this->pdf->AddPage();
		$this->fontSize = 18;
	}

 	public function imprime($data) {
		$pdf = $this->pdf;
		$marginx = 10;
		$marginy = 0;
		$marginRight = 0;
		$fontSize = 18;
		$headerFontStyle = null;
		$fontStyle = null;
		$center = $pdf->getPageWidth() / 2;
		$x = $marginx;
		$y = $marginy;
		$lineHeight = 6;
		$pdf->Text( $x, $y  ,"#: " . $data->doc->id, null, null);
		$y += $lineHeight;
		$pdf->Text($x, $y  ,$data->cliente['nombre']);
		$y += $lineHeight;
		$pdf->Text($x, $y  ,"Fecha: " . date("d/m/Y", time()));
		$y += $lineHeight;
		$pdf->Text( $x, $y  ,"Hora: " . date("G:i", time()), null, null);
		$y += $lineHeight + 5;
		$colNombreX = 10;
		$colPrecioX = $pdf->getPageWidth();
		$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(128, 0, 0));
		foreach($data->items as $item){
			$producto = $item->cantidad.' '.$item->nombre;
			$nombreWidth = $pdf->getPageWidth()- $marginRight - $colNombreX - 22;
			$pdf->MultiCell(
          $nombreWidth,
          0, $producto, 0, '', false, 1, $colNombreX, $y);
      $ultimoY = $pdf->GetY();
			$pdf->Text($pdf->getPageWidth()- $marginRight + 5, $y+1, $item->precio_venta*$item->cantidad." $" , null, null, true,null,null,'R');
			$y = $ultimoY;
		}
		if($data->doc->descuento > 0){
				$pdf->Text($colNombreX, $y, 'Descuento', null, null);
				$pdf->Text($pdf->getPageWidth()- $marginRight, $y, '-'.$data->doc->descuento." $" , null, null, true,null,null,'R');
				$y += $lineHeight;
		}
		$pdf->Text($colNombreX, $y, "Total", null, null);
		$pdf->Text($pdf->getPageWidth()- $marginRight, $y, $data->doc->total." $" , null, null, true,null,null,'R');

 	}

	public function guarda($esBuffet = false){
		$path = "./docsAutoPrint/buffet/";
		$unique = date('YmdHisu');
		$f = $path . ($esBuffet ? "buffet_$unique.pdf":"caja_$unique.pdf");
		$this->pdf->Output($f, 'F');
	}
}
if($data->doc->total) {
		$i = new Informe();
		$i->imprime($data);

		$i->guarda($esBuffet);
}

exit(json_encode($this->resp));
