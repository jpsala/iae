<?php
$this->resp->data = Helpers::qryAll("
SELECT s.id,
    case WHEN a.id then CONCAT(a.apellido, ', ', a.nombre)
    ELSE CONCAT(u.apellido, ', ', u.nombre)
    END AS nombre,
    saldo_buffet(s.id, NULL) AS saldo,
    (select DATE_FORMAT(fecha, '%d/%m/%Y') from buffet_doc where socio_id = s.id and saldo > 0  order by id DESC LIMIT 1) as fecha_deuda
  FROM socio s
    left JOIN alumno a ON s.Alumno_id = a.id
    LEFT JOIN user u ON s.Empleado_id = u.id
  WHERE saldo_buffet(s.id, NULL) > 0 AND s.id != 42593
  ORDER BY 4 asc");
  exit(json_encode($this->resp));