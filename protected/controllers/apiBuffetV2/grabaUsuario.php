<?php
//		vd($_REQUEST);
// vd($this->body);
// $doc = $this->body->doc;
// $items = $this->body->items;
$tr = Yii::app()->db->beginTransaction();
$user = $this->body->user;
try {
  $existe = Helpers::qryScalar("select count(*) from user a where a.id = $user->id");
  if ($existe) {
    $select = "UPDATE user
      SET nombre = :nombre, apellido=:apellido, login=:login,
          email = :email, documento = :documento WHERE id = :id";
    $params = array(
      'id' => $user->id,
      'nombre' => $user->nombre, 'apellido' => $user->apellido,
      'login' => $user->login, 'email' => $user->email,
      'documento' => $user->documento
    );
    Helpers::qryExec($select, $params);
    // $user_id = $user->id;
  } else {
    $select = "INSERT INTO user(
      nombre, login, email, apellido, password, documento)
      VALUES(:nombre, :login, :email, :apellido, :password, :documento)";
    $params = array(
      'nombre' => $user->nombre,
      'apellido' => $user->apellido,
      'documento' => $user->documento,
      'email' => $user->email,
      'login' => $user->login, 'password' => $user->password
    );
    //code...
    Helpers::qryExec($select, $params);
    $user->id = helpers::qryScalar('select LAST_INSERT_ID();');
  }
  //exit(json_encode($user));
  // $existeSocio = Helpers::qryScalar("select count(*) from socio s where s.empleado_id = $user->id");
  $usuariosNoSocios = Helpers::qryAllObj("
    SELECT u.*
    FROM user u
      LEFT JOIN socio s ON u.id = s.Empleado_id
    WHERE s.Empleado_id IS null");
  foreach ($usuariosNoSocios as $u) {
    Helpers::qryExec("
      insert into socio (empleado_id, tipo, code) values ($u->id, 'e', 'ver')
    ");
  }
  // if (!$existeSocio) {
  //   Helpers::qryExec("
  //     insert into socio (empleado_id, tipo) values ($user->id, 'e')
  //   ");
  // };
  Helpers::qryExec("
                delete from user_role
                where user_id = $user->id");
  if (isset($user->roles)) {
    foreach ($user->roles as $role) {
      // vd2($role);
      // $roleId = $role['id'];
      Helpers::qryExec("
        insert into user_role(user_id,role_id)
        values($user->id, $role)");
    }
  }
  $tr->commit();
  $this->resp->status = 'ok';
} catch (CDbException $th) {
  // vd2(implode('-', $th->errorInfo));
  $this->resp->status = implode('-', $th->errorInfo);
  //throw $th;
}
$this->resp->id = $user->id;
exit(json_encode($this->resp));
