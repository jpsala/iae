<?php
$fechaDesde = isset($this->body->fecha) ? date('Y-m-d 00:00',strtotime($this->body->fecha)) : date('Y-m-d 00:00');
$fechaHasta = isset($this->body->fecha) ? date('Y-m-d 23:59:59',strtotime($this->body->fecha)) : date('Y-m-d 23:59:59');
$fecha = isset($this->body->fecha) ? date('d/m/Y',strtotime($this->body->fecha)) : date('d/m/Y');;
$data = new stdClass();
$data->total_buffet = Helpers::qryScalar("
select sum(d.total-d.saldo)
from buffet_doc d
where d.tipo in ('t','')
		and d.fecha between '$fechaDesde' and '$fechaHasta'
");
$data->total_pagos = Helpers::qryScalar("
  select sum(abs(d.total))
	  from buffet_doc d
  where d.tipo = 'p' and d.fecha between '$fechaDesde' and '$fechaHasta'
");
$data->platosCero = Helpers::qryScalar("
  select count(*)
  from buffet_doc_det d
    inner join buffet_doc bd on bd.id = d.doc_id
  where d.importe = 0 and bd.fecha between '$fechaDesde' and '$fechaHasta'
  ");
// vd2($data);
class MYPDF extends PDF {

	public $planilla;

	public function header() {

	}

	public function footer() {

	}

	public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false) {
		parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
	}

}

class Informe extends stdClass {
//
	public $pdf;
//
 	public function __construct($paperSize = "A6") {
		$this->paperSize = $paperSize;
		$this->pdf = new MYPDF("P", "mm", $paperSize);
		$this->prepara();
 	}
//
// 	public function imprime($imprimeAuto, $nombre_pc, $impresora) {
// 	}
//
// 	private function preparaPdf() {
// 	}
//

	public function prepara(){
		$this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", 11);
		$this->pdf->setCellPaddings(1, 2, 0, 0);
		$this->pdf->AddPage();
		$this->fontSize = 12;
	}

 	public function imprime($data) {
		$pdf = $this->pdf;
		$marginx = 10;
		$marginy = 0;
		$marginRight = 10;
		$fontSize = 10;
		$headerFontStyle = null;
		$fontStyle = null;
		$center = $pdf->getPageWidth() / 2;
		$x = $marginx;
		$y = $marginy;
		$lineHeight = 6;
		$pdf->Text($x, $y  ,"Fecha: " . date("d/m/Y", time()));
		$y += $lineHeight;
		$pdf->Text( $x, $y  ,"Hora: " . date("G:i", time()), null, null);
		$y += $lineHeight;
		$colNombreX = 10;
		$colPrecioX = $pdf->getPageWidth();
		$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(128, 0, 0));
		$pdf->Line($marginx, $y, $pdf->getPageWidth() - $marginRight, $y, 80, 30, $style);
		$pdf->Text($colNombreX, $y, "Total tickets", null, null);
		$pdf->Text($colNombreX + 40, $y, "$data->total_buffet", null, null);
		$y += $lineHeight;
		$pdf->Text($colNombreX, $y, "Total pagos", null, null);
		$pdf->Text($colNombreX + 40, $y, "$data->total_pagos", null, null);
		$y += $lineHeight;
		$pdf->Text($colNombreX, $y, "Platos valor 0", null, null);
		$pdf->Text($colNombreX + 40, $y, $data->platosCero, null, null);

 	}

	public function guarda(){
		$path = "./docsAutoPrint/buffet/";
		// $unique = uniqid();
		$unique = date('YmdHisu');
		$f = $path . "buffet_$unique.pdf";
		$this->pdf->Output($f, 'F');
	}
//
}
	$i = new Informe();
	$i->imprime($data);

	$i->guarda();
