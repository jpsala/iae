<?php
$query = isset($this->body->query) ? $this->body->query : '';
$conPadres = isset($this->body->con_padres) && ($this->body->con_padres ? true: false);
$conEmpleados = isset($this->body->con_empleados) && ($this->body->con_empleados ? true: false);
if ($query === "") {
    $socios = [];
} else {
    $apellido = $query;
    $nombre = '';
    $pos = strpos($query, ',');
    // vd2($pos);
    if ($pos !== false && $pos >= 0) {
        $parts = explode(',', $query);
        $apellido = trim($parts[0]);
        $nombre = trim($parts[1]);
    }
    $tipo = "'A',";
    $tipo .= $conPadres ? "'PA'," : '';
    $tipo .= $conEmpleados ? "'U' " : '';
    $tipo = substr($tipo, 0, strlen($tipo)-1);
    $select = "
    SELECT s.id, s.nombre, s.apellido, s.nombre_completo, s.tipo, s.descripcion
    FROM socio_view s
    WHERE s.activo and
      (
        (s.apellido like \"%$apellido%\" and s.nombre like \"%$nombre%\") or
        (s.nombre like \"%$apellido%\" and s.apellido like \"%$nombre%\")
      )
          and s.tipo in ($tipo)
    ";
    // vd2($select);
    $select = "
    select s.id, s.admin_buffet as admin,
      case
        when a.id then concat(coalesce(a.apellido,' '),', ', coalesce(a.nombre,' '))
        when e.id then concat(coalesce(e.apellido,' '), ', ', coalesce(e.nombre,' '))
        /* when p.id then concat(coalesce(p.apellido,' '), ', ', coalesce(p.nombre,' ')) */
      end as nombre_completo,
      case
        when a.id then 'A'
        when e.id then 'E'
        /* when p.id then 'P' */
      end as tipo,
      case
        when a.id then
        concat(
            an.nombre, ' ', d.nombre,
            ' DNI:', a.numero_documento
          )
        when e.id then concat('Empleado', ' - ', coalesce(e.documento, 'Sin documento'))
        /* when p.id then t.nombre */
      end as descripcion
    from socio s
      left join alumno a on a.id = s.alumno_id
      left join alumno_division ad on ad.alumno_id = a.id and ad.activo
      left JOIN alumno_division_estado ade ON ad.alumno_division_estado_id = ade.id
      left join division d on d.id = ad.division_id
      left join anio an on an.id = d.anio_id
      /* left join pariente p on p.id = s.pariente_id */
      /* left join pariente_tipo t on p.pariente_tipo_id = t.id */
      left join user e on e.id = s.empleado_id
    where
      CASE
        WHEN a.id THEN
          a.apellido like \"%$apellido%\" and a.nombre like \"%$nombre%\"  AND ade.muestra_edu AND ! ad.borrado
        /* WHEN p.id THEN */
        /*   p.apellido like \"%$apellido%\" and p.nombre like \"%$nombre%\" and p.apellido != '(falta apellido)' and p.nombre != '(falta nombre)' */
        WHEN e.id THEN
          ((e.apellido like \"%$apellido%\" and e.nombre like \"%$nombre%\")
          or e.nombre like \"%$apellido%\" ) and !e.borrado
      END
      order by 2";
      // vd2($select);
    $socios = Helpers::qryAll($select);
}
$this->resp->data = $socios;
exit(json_encode($this->resp));
