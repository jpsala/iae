<?php
$select = "
SELECT s.id, s.nombre, s.apellido, s.nombre_completo, s.tipo, s.descripcion
FROM socio_view s
WHERE s.activo and
  (
    (s.apellido like '%giorgis%' and s.nombre like '%%') or
    (s.nombre like '%giorgis%' and s.apellido like '%%')
  )
      and s.tipo in ('A','U')
";
$data = Helpers::qry($select);
$this->resp->data = $data;
exit(json_encode($this->resp));