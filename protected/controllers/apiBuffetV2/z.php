<?php
// $fecha = '2017/07/07';
$fechaDesde = isset($this->body->fecha) ? date('Y-m-d 00:00',strtotime($this->body->fecha)) : date('Y-m-d 00:00');
$fechaHasta = isset($this->body->fecha) ? date('Y-m-d 23:59:59',strtotime($this->body->fecha)) : date('Y-m-d 23:59:59');
$this->resp->data = new stdClass;
// $this->resp->data->fechas = [$fechaDesde, $fechaHasta, $this->body->fecha];
$this->resp->data->success = true;
$selectTotal = "
  select sum(d.total-d.saldo)
    from buffet_doc d
  where d.tipo in ('t','')
        and d.fecha between '$fechaDesde' and '$fechaHasta'
";
$this->resp->data->select = $selectTotal;
$this->resp->data->total_buffet = Helpers::qryScalar($selectTotal);
$this->resp->data->total_pagos = Helpers::qryScalar("
  select sum(abs(d.total))
	  from buffet_doc d
  where d.tipo = 'p' and d.fecha between '$fechaDesde' and '$fechaHasta'
");
$this->resp->data->platos_cero = Helpers::qryScalar("
  select count(*)
  from buffet_doc_det d
    inner join buffet_doc bd on bd.id = d.doc_id
  where d.importe = 0 and bd.fecha between '$fechaDesde' and '$fechaHasta'
  ");
exit(json_encode($this->resp));
