<?php
$socio_id = isset($_GET['socio_id']) ? $_GET['socio_id'] : null;
$nivel_id = isset($_GET['nivel_id']) ? $_GET['nivel_id'] : null;
$conLogo = isset($_GET['logo']) ? $_GET['logo'] === '1' : false;
$liquid_conf_id = isset($_GET['liquid_conf_id']) ? $_GET['liquid_conf_id'] : null;
// vd2($liquid_conf_id);
$comprobFactura = Comprob::FACTURA_VENTAS;
    $alumno_id = $socio_id ? Helpers::qryScalar("
      select s.alumno_id 
        from socio s where s.id = $socio_id") : null;
    $where = $nivel_id ? " where n.id = $nivel_id " : " ";
    $where = $where . ($alumno_id ? " and a.id = $alumno_id " : "");
    if ($liquid_conf_id) {
        $lc = LiquidConf::model()->findByPk($liquid_conf_id);
    } else {
        $lc = $liquid_conf_id = LiquidConf::getActive();
        $liquid_conf_id = $lc->id;
    }
    $alumnosSelect = "
      select n.nombre as nivel, anio.nombre as anio, d.nombre as division, lc.fecha_liquidacion,
        concat(a.apellido, ', ', a.nombre) as alumno, a.matricula,
        s.id as socio_id, p.calle, p.numero, p.piso, p.departamento,
        a.beca, a.visa_activo as visa,
        deb.activo as debito_activo, lc.descripcion
      from alumno_division ad
        inner join division d on d.id = ad.division_id and d.activo = 1
        inner join anio on anio.id = d.anio_id and anio.activo = 1
        inner join nivel n on n.id = anio.Nivel_id
        inner join alumno a on a.id = ad.Alumno_id  and a.activo = 1 /*and a.id = 43504*/
        inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_admin = 1
        left  join alumno_debito deb on deb.alumno_id = a.id
        inner join socio s on s.alumno_id = a.id
        inner join doc doc on doc.socio_id = s.id
        inner join doc_liquid dl on dl.id = doc.doc_liquid_id and dl.liquid_conf_id = $liquid_conf_id
        inner join liquid_conf lc on lc.id = dl.liquid_conf_id
        left join pariente p on p.id = a.vive_con_id
        inner join talonario t on t.id = doc.talonario_id and t.comprob_id = $comprobFactura
      $where  and ad.activo = 1 /*and a.matricula = 5125*/
      order by n.orden, anio.nombre, d.nombre, a.sexo, a.apellido, a.nombre";
    $alumnos = Helpers::qryAll($alumnosSelect);
    //vd($alumnos);
    foreach ($alumnos as $key => $a) {
        $alumnos[$key]["domicilio"] = $a["calle"] . " " . $a["numero"] . " " . $a["piso"] . " " . $a["departamento"];
        $socio_id = $a["socio_id"];
        $alumnos[$key]["doc"] = Helpers::qry("
        select d.id, d.fecha_creacion, d.total, d.detalle,
                    d.fecha_vto1, d.fecha_vto2, dl.saldo_anterior
        from doc d
            inner join talonario t on t.id = d.talonario_id and t.comprob_id = $comprobFactura
            inner join doc_liquid dl on dl.id = d.doc_liquid_id and dl.liquid_conf_id = $liquid_conf_id
        where d.socio_id = $socio_id");
        $doc = $alumnos[$key]["doc"];
        $doc_id = $doc["id"];
        if (!$doc_id) {
            vd("error!!!, no tiene ninguna factura!", $doc);
        }
        $alumnos[$key]["doc"]["det"] = Helpers::qryAll("
          select det.detalle as nombre, total, afectacion
          from doc_det det
              left join articulo a on a.id = det.articulo_id
          where det.Doc_id = $doc_id
          order by orden");
    }

    //vd($alumnos);
    $this->renderPartial("emisionComprobantes", array("alumnos" => $alumnos, "conLogo" => $conLogo));