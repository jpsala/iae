<?php 
$socio = $this->body;
$ret = new stdClass();
if(isset($socio->fecha_nacimiento)){
  $socio->fecha_nacimiento =  date_format(DateTime::createFromFormat('d/m/Y', $socio->fecha_nacimiento), 'Y/m/d');
}
if($socio->tipo === 'A'){
  $select = "
    update alumno a 
      set a.nombre = '$socio->nombre', a.apellido = '$socio->apellido'/*, calle = '$socio->domicilio_calle',
          a.numero = '$socio->domicilio_numero', a.piso = '$socio->domicilio_piso', a.depto = '$socio->domicilio_depto'*/,
          a.telefono_1 = '$socio->telefono', a.fecha_nacimiento = '$socio->fecha_nacimiento', a.email = '$socio->email' 
    where a.id = $socio->Alumno_id
  ";
}elseif($socio->tipo === 'PA'){
  $select = "
    update pariente a 
      set a.nombre = '$socio->nombre', a.apellido = '$socio->apellido', calle = '$socio->domicilio_calle',
          a.numero = '$socio->domicilio_numero', a.piso = '$socio->domicilio_piso', a.departamento = '$socio->domicilio_depto',
          a.telefono_celular = '$socio->telefono', a.fecha_nacimiento = '$socio->fecha_nacimiento'
    where a.id = $socio->Pariente_id
  ";
  
}
$updateSocio = "
    update socio s 
      set s.password = '$socio->password'
    where s.id = $socio->socio_id
  ";
$this->resp->respuestaSocio = Helpers::qryExec($updateSocio);
$this->resp->respuesta = Helpers::qryExec($select);
// $ret->credito = Helpers::qryScalar("select sum(d.saldo) from buffet_doc d where d.saldo < 0");
exit(json_encode($this->resp));


/*

    [id] => 35131
    [socio_id] => 35131
    [tipo] => A
    [login] => 
    [password] => 
    [roles] => user
    [Alumno_id] => 42350
    [Empleado_id] => 
    [Proveedor_id] => 
    [Cliente_id] => 
    [apellido] => Cunha Duarte 
    [nombre] =>  Sebastián
    [nombre_completo] => Cunha Duarte ,  Sebastián
    [numero_documento] => 44635256
    [domicilio_calle] => ingresar
    [domicilio_numero] => 0
    [domicilio_piso] => 0
    [domicilio_depto] => 
    [telefono] => ingresar
    [email] => ingresar@ingresar.com
    [description] => 3 A DNI:44635256
    [saldo] => 281.00
    [password2] => 

 id int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  nombre varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  apellido varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  Familia_id int(10) UNSIGNED NOT NULL,
  matricula varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  activo tinyint(1) DEFAULT 1,
  ingresante tinyint(1) NOT NULL DEFAULT 0,
  calle varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  numero smallint(6) NOT NULL DEFAULT - 1,
  piso char(4) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  departamento char(4) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  fecha_nacimiento datetime DEFAULT NULL,
  numero_documento varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  Tipo_Documento_id int(11) NOT NULL,
  localidad_id int(10) UNSIGNED NOT NULL,
  obra_social varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  numero_afiliado varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  grupo_sanguineo varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  telefono_1 varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  telefono_2 varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  email varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  sexo char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'M',
  porc_beca decimal(5, 2) DEFAULT NULL,
  titular_tel1 varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  titular_tel2 varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  vive_con_id int(10) UNSIGNED DEFAULT NULL,
  beca decimal(6, 2) NOT NULL DEFAULT 0.00,
  obs text CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  obs_cobranza varchar(255) DEFAULT NULL,
  obs_beca text CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  visa_numero varchar(19) NOT NULL DEFAULT '',
  visa_activo smallint(6) DEFAULT 0,
  visa_titular varchar(45) NOT NULL DEFAULT '',
  nacionalidad_opcion int(10) DEFAULT NULL,
  alumno_division_id_anterior_borrar int(10) UNSIGNED DEFAULT NULL,
  ex_ingresante smallint(5) UNSIGNED DEFAULT NULL,
  egresado tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  estado_id smallint(1) UNSIGNED NOT NULL DEFAULT 1,
  activo_ant smallint(1) DEFAULT NULL,
  fecha_ingreso date DEFAULT '0000-00-00',
  fecha_egreso date DEFAULT '0000-00-00',
  fecha_ingreso_estab date DEFAULT '0000-00-00',
  obs_cobranza_old text DEFAULT NULL,
  alumno_division_id_ant int(9) UNSIGNED DEFAULT NULL,
  estado_id_anterior tinyint(1) UNSIGNED DEFAULT NULL,
  encargado_pago_id int(10) UNSIGNED DEFAULT NULL,
  encargado_cambiado smallint(1) NOT NULL DEFAULT 0,
  encargado_visto smallint(1) UNSIGNED NOT NULL DEFAULT 0,
  email_nuevo varchar(100) DEFAULT '',
  libre smallint(1) UNSIGNED NOT NULL DEFAULT 0,
  location varchar(150) NOT NULL DEFAULT '',
*/
