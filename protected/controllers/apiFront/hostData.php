<?php
$ip = $this->body->ip;
$hostApiJson = json_decode(shell_exec("curl http://ip-api.com/json/$ip"));
$hostDataRaw = shell_exec("sudo whois $ip");
$hostDataLinesRaw = preg_split("[\r|\n]", trim($hostDataRaw));
$hostDataLines = [];
foreach($hostDataLinesRaw as $line) {
  if(substr($line, 0, 1) === '#' or substr($line, 0, 1) === '%') {
    continue;
  }
  $hostDataLines[] = $line;
}
$this->resp->data = $hostDataLines;
$this->resp->dataApi = $hostApiJson;
$this->resp->rawData = $hostDataRaw;
exit(json_encode($this->resp));