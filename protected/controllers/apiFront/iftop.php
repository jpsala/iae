<?php
$iface = gethostbyaddr("127.0.0.1") === 'localhost.jp' ? 'wifi2': 'em1';
$dropsRaw = shell_exec("sudo iptables -L -n | grep -i drop | awk '{ print $4 }' | grep -i 192 | grep -v \"192\.168\.2\.254\" | grep -v \"192\.168\.2\.255\" | grep -v \"192\.168\.2\.110\"");
$lineDrops = preg_split("[\r|\n]", trim($dropsRaw));
$drops = [];
$index = 0;
foreach($lineDrops as $key => $line) {
  $name = gethostbyaddr($line);
  $drops[$index]['ip'] = $line;
  $drops[$index]['name'] = $name;
  $index++;
}
$salida = shell_exec("sudo iftop -i $iface -o 40s -n -b -B -s 6 -t  "); //-f \"not dst net 192.168.2\"
$linesRaw = preg_split("[\r|\n]", trim($salida));
$basura = true;
$lines = array();
$linesCount = 0;
foreach($linesRaw as $line) {
  if($basura && substr($line,0,1) === '-'){
    $basura = false;
    continue;
  }
  if($basura){
    continue;
  }else{
    if(substr($line,0,1) === '-'){
      break;
    }
  }
  $lines[$linesCount] = trim($line);
  $linesCount++;
}

$colsOrigen = array();
$colsDestino = array();
$colsCountHalf = round(count($lines)/2);
$final = array();
for($i=0; $i < $colsCountHalf; $i++){
  $origen = $lines[$i*2];
  $rawColsOrigen = explode(' ', $origen);
  $destino = $lines[($i*2)+1];
  $rawColsDestino = explode(' ', $destino);
  $colsOrigen[$i] = [];
  $colsDestino[$i] = [];
  $index = 0;
  foreach($rawColsOrigen as $col){
    if(trim($col)){
      $colsOrigen[$i][$index++] = $col;
    }
  }
  $index = 0;
  foreach($rawColsDestino as $col){
    if(trim($col)){
      $colsDestino[$i][$index++] = $col;
    }
  }
}
// ve2($colsDestino);
for($i=0; $i < $colsCountHalf; $i++){
  // ve2($colsOrigen[$i]);
  $count = bytes($colsOrigen[$i][6]);
  $count = $count + bytes($colsDestino[$i][5]);
  if($count < 1024) continue;
  if(($colsOrigen[$i][1] ==='192.168.2.254') or ($colsDestino[$i][0] === '192.168.2.254')) continue;
  if((substr($colsOrigen[$i][1], 0, 9) != '192.168.2' ) and (substr($colsDestino[$i][0], 0, 9) != '192.168.2' )) continue;
  $final[$i]['origen']['ip'] = $colsOrigen[$i][1];
  $final[$i]['destino']['ip'] = $colsDestino[$i][0];
  try{
    $final[$i]['origen']['name'] = gethostbyaddr($colsOrigen[$i][1]);
    $final[$i]['destino']['name'] = gethostbyaddr($colsDestino[$i][0]);
  } catch(Exception $e) {
    $final[$i]['origen']['name'] = 'error: '.($colsOrigen[$i][1]);
    $final[$i]['destino']['name'] = 'error: '.($colsDestino[$i][0]);
  }
  $final[$i]['origenall'] = $colsOrigen;
  $final[$i]['destinoall'] = $colsDestino;
  // $final[$i]['bytes'] = $count < 1024 ? round($count,0) . 'B' : ($count <= (1024*1024) ? round($count/1024, 0).'KB' : round($count/1024/1024, 2).'MB');
  $final[$i]['bytes'] = $colsOrigen[$i][6].'/'.$colsDestino[$i][5];
  $final[$i]['count'] = $count;
  // $final[$i]['bytes'] = $count;
  // $final[$i]['origen']['ip'] = $colsOrigen[$i][1];
  // $final[$i]['destino']['ip'] = $colsDestino[$i][0];
  // $final[$i]['origen']['bytes'] = $colsDestino[$i][5];
  // $final[$i]['destino']['bytes'] = $colsOrigen[$i][6];
}

$this->resp->data = $final;
$this->resp->drops = $drops;
exit(json_encode($this->resp));

function bytes($num) {
  $tipo = substr($num, -2);
  $num = substr($num, 0, -2);
  if($tipo === 'KB'){
    $num = str_replace('.', '', substr($num, 0 , -2)) * 1024;
  }elseif($tipo === 'MB'){
    $num = str_replace('.', '', substr($num, 0 , -2)) * 1024 * 1024;
  } else {
    $num = str_replace('.', '', substr($num, 0 , -1));
  }
  return $num;
}