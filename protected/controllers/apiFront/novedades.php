<?php
$nivelesPedidos = isset($_GET["niveles"]) ? $_GET["niveles"] : "";
// vd2($nivelesPedidos);
$nivelesPedidos = $_GET["niveles"] === ""  ? "'ES', 'EP 1', 'EP 2', 'INICIAL'" : $_GET["niveles"];
$nivelesPedidosArray = split(',', $nivelesPedidos);
$select = "
  SELECT n.*, DATE_FORMAT(n.fecha, '%d/%m/%Y') as fecha 
  FROM noticia n
    left JOIN noticia_leida nl ON n.id = nl.noticia_id
  WHERE nl.id IS null
  order by n.id desc
";

$noticiasRow = (object) Helpers::qryAll($select);
$noticias = [];
foreach ($noticiasRow as $noticia) {
  $noticia = (object) $noticia;
  $noticia->niveles_pedidos = $nivelesPedidos;
  // ve2(strpos($noticia->niveles, 'EP'));
  if(trim($noticia->niveles) === '') {
    $noticia->para_niveles = 'para todos los niveles';
    $noticias[] = $noticia;
  } else {
    $ok = false;
    foreach ($nivelesPedidosArray as $nivelPedido) {
      $noticia->para_niveles = "Para $nivelPedido";
      if(strPos($noticia->niveles, $nivelPedido)>-1) {
        $ok = true;
      }
    }
    if($ok) {
      $noticias[] = $noticia;
    }
  } 
  // if (strpos($niveles, '') !== false) {
  //   echo 'true';
  // } 
}
$this->resp = $noticias;
exit(json_encode($this->resp));