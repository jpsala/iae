<?php
$socio_id = $this->get['socio_id'];
$alumno_id = Helpers::qryScalar('select a.id from socio s inner join alumno a on a.id = s.alumno_id and s.id = ' . $socio_id);
$ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
$selectAlumnoDivision = "
  select ad.id 
    from alumno_division ad 
  where ad.alumno_id = $alumno_id and ad.Ciclo_id = $ciclo_id AND !ad.borrado";
$alumno_division = Helpers::qryScalar($selectAlumnoDivision);
$selectNivel = "SELECT n.id
  FROM nivel n 
    INNER JOIN anio a ON n.id = a.Nivel_id
    INNER JOIN division d ON a.id = d.Anio_id
    INNER JOIN alumno_division ad ON d.id = ad.Division_id AND ad.id = $alumno_division";
$nivel_id = Helpers::qryScalar($selectNivel);
$this->resp->periodos = Helpers::qryAll("
  select lp.* from logica_periodo lp
    inner join logica l on l.id = lp.logica_id
    inner join logica_ciclo lc on lc.logica_id = l.id and lc.ciclo_id = $ciclo_id
    inner join asignatura_tipo at on at.id = lc.asignatura_tipo_id and at.tipo = 1  AND at.Nivel_id = $nivel_id
  order by lp.orden
");
exit(json_encode($this->resp));