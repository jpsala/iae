<?php
$socio_id = $_GET['socio_id'];
$alumno_id = Helpers::qryScalar("select alumno_id from socio where id = $socio_id");
$division_id = Helpers::qryScalar("SELECT ad.division_id FROM alumno_division ad WHERE ad.alumno_id = $alumno_id AND ad.activo = 1 AND ad.ciclo_id = (select max(id) FROM ciclo)");
$nivel_id = Helpers::qryScalar("SELECT a.nivel_id FROM division d inner join anio a on a.id = d.anio_id where d.id = $division_id");
$ciclo_id = Helpers::qryScalar("select max(id) from ciclo");
$alumno_division_id = Helpers::qryScalar("select id from alumno_division ad where ad.alumno_id = $alumno_id and ad.division_id = $division_id");
// vd2($division_id, '_', $alumno_id);
$periodos = Helpers::qryAll("
  SELECT lp.id, lp.nombre, 
          case WHEN DATE(now()) BETWEEN DATE(lp.fecha_inicio_ci) AND DATE(lp.fecha_fin_ci) then 1 ELSE 0 END AS actual
    FROM asignatura_tipo at 
      INNER JOIN logica_ciclo lc ON at.id = lc.Asignatura_Tipo_id
      INNER JOIN ciclo c ON lc.Ciclo_id = c.id
      INNER JOIN logica l ON lc.Logica_id = l.id
      INNER JOIN logica_periodo lp ON l.id = lp.Logica_id
      INNER JOIN logica_item li ON lp.id = li.Logica_Periodo_id
      INNER JOIN alumno_division ad ON ad.Alumno_id = 42476 AND ad.Division_id = 391
    WHERE at.Nivel_id = $nivel_id AND at.tipo = 1 
      AND lc.Ciclo_id = (SELECT MAX(id) FROM ciclo c) AND li.nota_del_periodo != 0 
      AND (!( DATE(lp.fecha_fin_ci) > DATE(NOW())) OR DATE(now()) BETWEEN DATE(lp.fecha_inicio_ci) AND DATE(lp.fecha_fin_ci))
    ORDER BY lp.orden");

    $this->resp->data = $periodos;
    // $ret->access_token = session_id();
    // $ret->credito = Helpers::qryScalar("select sum(d.saldo) from buffet_doc d where d.saldo < 0");
    exit(json_encode($this->resp));
?>
