<?php
$email = $this->body->email;
$host = gethostbyaddr("127.0.0.1") == 'localhost.jp' /*local*/ ? 'http://localhost:8080/activate' : 'http://app.iae.com.ar/activate';
$emailParaMandar = gethostbyaddr("127.0.0.1") == 'localhost.jp' /*local*/ ? 'jpsala@gmail.com' : $this->body->email;
// vd2($host, $email, $emailParaMandar);


$select = "
  select s.id as socio_id, upper(s.tipo) as tipo, s.login, s.password, s.roles,
    case
      when a.id then concat(a.apellido,', ', a.nombre)
      when e.id then e.nombre
      else concat(p.apellido, ', ', p.nombre)
    end as nombre,
    case
      when a.id then 'alumno'
      when e.id then 'empleado'
      when p.id then 'pariente'
    end as tipo,
    case
        when a.id then 'a'
        when p.id then 'p'
        when e.id then 'e'
      end as tipo_socio,
    case
      when a.id then
      concat(
          an.nombre, ' ', d.nombre,
          ' DNI:', a.numero_documento
        )
      when e.id then 'Usuario'
      when p.id then t.nombre
    end as description, saldo_buffet(s.id, null) as saldo
  from socio s
    left join alumno a on a.id = s.alumno_id
    left join alumno_division ad on ad.alumno_id = a.id and ad.activo
    left join division d on d.id = ad.division_id
    left join anio an on an.id = d.anio_id
    left join pariente p on p.id = s.pariente_id
    left join pariente_tipo t on p.pariente_tipo_id = t.id
    left join user e on e.id = s.empleado_id
  WHERE p.email = '$email' OR a.email = '$email' OR e.email = '$email'";

$socio = Helpers::qry($select);

if ($socio) {
  $socioId = $socio['socio_id'];
  $this->resp->status = 200;
  $mail = Yii::app()->mail->mailer;
  $this->layout = "//layouts/mail";
  $mail->Subject =  utf8_decode("Activación de su cuenta en el IAE");
  $body = "
    <h2><a href='$host/$socioId'>Activar su cuenta en el IAE</a></h2>
  ";
  $mail->MsgHTML($body);
  $mail->AddAddress($emailParaMandar, "IAE");
  if ($email !== "info@iae.esc.edu.ar") {
    //$mail->AddAddress("jpsala@gmail.com", "IAE");
  }
  $response = $mail->Send();
  if (!$response) {
  	$this->resp->error = $mail->ErrorInfo;
    $this->resp->status = 500;
    $this->resp->error_number = 1;
  }
	$this->resp->message = $response;
	$this->resp->user = $socio;
} else {
  $this->resp->error = 'Su dirección de correo no fué encontrada';
  $this->resp->error_number = 10;
  $this->resp->status = 501;
}

exit(json_encode($this->resp));
