<?php

class GenController extends CController {

    public function actionTest($matricula = 2964) {
        $aluDiv = $this->actionTestAjaxAlumno(2964, true);
        $this->render("test", array("aluDiv" => $aluDiv));
    }

    public function actionTestAjaxAlumno($matricula = null, $return = false) {
        //$matricula = -1;
        $s = "
            select a.id, a.matricula, \"\" as nombrefs, a.apellido, a.nombre, \"\" as asigfs ,/*nivel.id as nivel_id, */ nivel_id,
                        anio.nombre as anio_nombre, /*d.id as division_id,*/
                        d.nombre as division_nombre, a.obs,  \"\" as obsfs
                from alumno a
                        inner join alumno_division ad on ad.Alumno_id = a.id and ad.activo = 1
                        inner join division d on d.id = ad.Division_id 
                        inner join anio on anio.id = d.Anio_id
                        inner join nivel on nivel.id = anio.Nivel_id                
            where a.matricula = $matricula";
        $alumno = Helpers::qry($s);
        if (!$alumno) {
            $alumno = Helpers::qryFields($s);
        }
        
        $data["data"] = $alumno;
        $cols["id"]["tipo"] = "input";
        $cols["id"]["label"] = "";
        $cols["id"]["options"] = array("type"=>"hidden");
        $cols["matricula"]["label"] = "Mátrícula";
        $cols["matricula"]["div"] = "id-mat";
        $cols["nombre"]["placeholder"] = "Nombre";
        $cols["nombre"]["label"] = "";
        $cols["apellido"]["placeholder"] = "Apellido";
        $cols["apellido"]["label"] = "";
        $cols["anio_id"]["tipo"] = "hidden";
        $cols["division_id"]["tipo"] = "hidden";
        $cols["obs"]["label"] = "";
        $cols["obs"]["tipo"] = "textarea";
        $cols["nivel_id"]["tipo"] = "select";
        $cols["nivel_id"]["placeholder"] = "Seleccione Nivel";
        $cols["nivel_id"]["data"] = CHtml::listOptions($alumno["nivel_id"], CHtml::listData(Nivel::model()->findAll(), "id", "nombre"),$kk=array("prompt"=>"")) ;
        $cols["nivel_id"]["label"] = "";
        $cols["nivel_nombre"]["label"] = "";
        $cols["nivel_nombre"]["placeholder"] = "Nivel";
        $cols["anio_nombre"]["label"] = "";
        $cols["anio_nombre"]["placeholder"] = "Año";
        $cols["division_nombre"]["label"] = "";
        $cols["division_nombre"]["placeholder"] = "División";
        
        $fieldSets["asigfs"]["legend"] = "Asignación";
        $fieldSets["asigfs"]["campos"] = array("nivel_id", "anio_nombre", "division_nombre");
        $fieldSets["asigfs"]["nivel_nombre"]["place-holder"]="Nivel";
        $fieldSets["nombrefs"]["legend"] = "Nombres";
        $fieldSets["nombrefs"]["campos"] = array("nombre", "apellido");
        $fieldSets["obsfs"]["legend"]="Observaciones";
        $fieldSets["obsfs"]["campos"]=array("obs");
        
        $div = $this->getDiv($data, $cols, $fieldSets);
        if ($return) {
            return $div;
        } else {
            echo $div;
        }
    }

    public function actioinGetDiv($data, $cols = array(), $fieldSets = array()) {
        return $this->renderPartial("gen", array("nombre" => "alumno", "data" => $data, "cols" => $cols, "fieldSets" => $fieldSets), true);
    }

}

?>
