<?php
/**
 * CuitValidator class file.
 *
 * @author Juan Pablo Villaverde <jpablo@omnisciens.com>
 * @version 1.0
 * @link http://www.yiiframework.com/extension/
 * @license BSD
 */

/**
 * CuitValidator verifies if the attribute represents a valid CUIT as stablished by AFIP in Argentina.
 */
final class ECuitValidator extends CValidator
{
	/**
	 * @var boolean whether the attribute value can be null or empty. Defaults to true,
	 * meaning that if the attribute is empty, it is considered valid.
	 */
	public $allowEmpty=true;

	/**
	 * Validates the attribute of the object.
	 * If there is any error, the error message is added to the object.
	 * @param CModel the data object being validated
	 * @param string the name of the attribute to be validated.
	 */
	protected function validateAttribute($object,$attribute){		
		$value=$object->$attribute;
		if($this->allowEmpty && $this->isEmpty($value))
			return;
	        if(!$this->CPcuitValido($value)){
			$message=$this->message!==null?$this->message : Yii::t(__CLASS__,'La CUIT ingresada no es valida.');
			$this->addError($object,$attribute,$message);
		}
	}


	protected function CPcuitValido( $cuit ) {
	    $esCuit=false;
	    $cuit_rearmado="";
	    //separo cualquier caracter que no tenga que ver con numeros
	    for ($i=0; $i < strlen($cuit); $i++) {   
	        if ((Ord(substr($cuit, $i, 1)) >= 48) && (Ord(substr($cuit, $i, 1)) <= 57))     {
	            $cuit_rearmado = $cuit_rearmado . substr($cuit, $i, 1);
	        }
	    }
	    $cuit=$cuit_rearmado;
	    if ( strlen($cuit_rearmado) <> 11) {  // si to estan todos los digitos
	        $esCuit=false;
	    } else {
	        $x=$i=$dv=0;
	        // Multiplico los d�gitos.
	        $vec[0] = (substr($cuit, 0, 1)) * 5;
	        $vec[1] = (substr($cuit, 1, 1)) * 4;
	        $vec[2] = (substr($cuit, 2, 1)) * 3;
	        $vec[3] = (substr($cuit, 3, 1)) * 2;
	        $vec[4] = (substr($cuit, 4, 1)) * 7;
	        $vec[5] = (substr($cuit, 5, 1)) * 6;
	        $vec[6] = (substr($cuit, 6, 1)) * 5;
	        $vec[7] = (substr($cuit, 7, 1)) * 4;
	        $vec[8] = (substr($cuit, 8, 1)) * 3;
	        $vec[9] = (substr($cuit, 9, 1)) * 2;
	                    
	        // Suma cada uno de los resultado.
	        for( $i = 0;$i<=9; $i++) {
	            $x += $vec[$i];
	        }
	        $dv = (11 - ($x % 11)) % 11;
	        if ($dv == (substr($cuit, 10, 1)) ) {
	            $esCuit=true;
	        }
	    }
	    return( $esCuit );
	}
    
}