<?php

class Afip extends CComponent
{

  const MODO_PROD = 'prod';
  const MODO_DEV = 'dev';
  static private $dir;
  /*
   * 80 – CUIT

07 - CI Mendoza
86 – CUIL
08 - CI La Rioja
87 – CDI
09 - CI Salta
89 – LE
10 - CI San Juan
90 – LC
11 - CI San Luis
91 - CI extranjera
12 - CI Santa Fe
92 - en trámite
13 - CI Santiago del Estero
93 - Acta nacimiento
14 - CI Tucumán
95 - CI Bs. As. RNP
16 - CI Chaco
96 – DNI
17 - CI Chubut
94 – Pasaporte
18 - CI Formosa
00 - CI Policía Federal
19 - CI Misiones
01 - CI Buenos Aires
   */
  private $local = true;
  private $_doc;
  private $comprob;
  private $scriptRece1, $scriptBarras, $dirRece1;
//  private $dirRece1 = "e:\\tools\\PyAfipWs";
//   private $scriptRece1 = "rece1";

  public function __construct($mode = self::MODO_PROD, $comprob)
  {
    $this->comprob = $comprob;
    $this->local   = !(gethostbyaddr("127.0.0.1") == "localhost")
    && !(gethostbyaddr("127.0.0.1") == "localhost.jp");
    if ($this->local) {
//      $this->scriptRece1 = "python rece1.py ";
      $this->scriptRece1  = "rece1 ";
      $this->dirRece1     = self::getDir();
      $this->scriptBarras = "e:\\tools\\python\\python.exe barras.py ";
//      $this->dirRece1 = "e:\\tools\\afip-dev-ult";
      //$this->dirRece1 = "e:\\tools\\pyafipws-master";
    } else {
//      $this->dirRece1 = ($mode == self::MODO_PROD ? "/opt/afip_prod" : "/opt/afip_dev");
      $this->dirRece1     = ($mode == self::MODO_PROD ? "/opt/afip_prod/" : self::getDir());
      $this->scriptRece1  = "python rece1.py ";
      $this->scriptBarras = "/usr/bin/python /opt/afip_dev/barras.py ";
    }
    $this->initConfig();
  }

  static public function getDir()
  {
    if (gethostbyaddr("127.0.0.1") == "localhost") {
      return "/opt/afip_prod/";
    } else {
      return "e:\\tools\\afip-dev\\";
    }
  }

  private function initConfig()
  {
    $doc = json_encode(array(
      'id' => 0, // identificador Ãºnico (obligatorio WSFEX)
      'punto_vta' => 3,
      'tipo_cbte' => $this->comprob, // 1: FCA, 2: NDA, 3:NCA, 6: FCB, 11: FCC
      'tipo_doc' => 0, // 96: DNI, 80: CUIT, 99: Consumidor Final
      'nro_doc' => 0, // Nro. de CUIT o DNI
      'fecha_cbte' => date('Ymd'), // Formato AAAAMMDD
      'fecha_serv_desde' => date('Ymd'), // competar si concepto > 1
      'fecha_serv_hasta' => date('Ymd'), // competar si concepto > 1
      'fecha_venc_pago' => date('Ymd'), // competar si concepto > 1
      'concepto' => 2, // 1: Productos, 2: Servicios, 3/4: Ambos
//      'moneda_ctz' => 1, // 1 para pesos
      'moneda_id' => 'PES', // 'PES': pesos, 'DOL': dolares (solo exportacion)	  );
    ));
//	$doc = json_encode(array(
//		 'id' => 0, // identificador Ãºnico (obligatorio WSFEX)
//		 'punto_vta' => 3,
//		 'tipo_cbte' => 15, // 1: FCA, 2: NDA, 3:NCA, 6: FCB, 11: FCC
//		 'cbte_nro' => 0, // solicitar proximo con /ult
//		 'tipo_doc' => 9, // 96: DNI, 80: CUIT, 99: Consumidor Final
//		 'nro_doc' => '18627504', // Nro. de CUIT o DNI
//		 'fecha_cbte' => date('Ymd'), // Formato AAAAMMDD
//		 'fecha_serv_desde' => date('Ymd'), // competar si concepto > 1
//		 'fecha_serv_hasta' => date('Ymd'), // competar si concepto > 1
//		 'fecha_venc_pago' => date('Ymd'), // competar si concepto > 1
//		 'concepto' => 2, // 1: Productos, 2: Servicios, 3/4: Ambos
//		 'nombre_cliente' => 'Joao Da Silva',
//		 'domicilio_cliente' => 'Rua 76 km 34.5 Alagoas',
//		 'pais_dst_cmp' => 16, // solo exportacion
//		 'moneda_ctz' => 1, // 1 para pesos
//		 'moneda_id' => 'PES', // 'PES': pesos, 'DOL': dolares (solo exportacion)
//		 'obs_comerciales' => 'Observaciones Comerciales, texto libre',
//		 'obs_generales' => 'Observaciones Generales, texto libre',
//		 'forma_pago' => '30 dias',
//		 'incoterms' => 'FOB', // solo exportacion
//		 'id_impositivo' => 'PJ54482221-l', // solo exportacion
//		 // importes subtotales generales:
//		 'imp_neto' => '100.00', // neto gravado
//		 'imp_op_ex' => '2.00', // operacioens exentas
//		 'imp_tot_conc' => '3.00', // no gravado
//		 'imp_iva' => '21.00', // IVA liquidado
//		 'imp_trib' => '1.00', // otros tributos
//		 'imp_total' => '127.00', // total de la factura
//		 // Datos devueltos por AFIP (completados luego al llamar al webservice):
//		 'cae' => '', // ej. '61123022925855'
//		 'fecha_vto' => '', // ej. '20110320'
//		 'motivos_obs' => '', // ej. '11'
//		 'err_code' => '', // ej. 'OK'
//		 'descuento' => 0,
//		 'detalles' => array(
//			  array(
//				   'qty' => 1, // cantidad
//				   'umed' => 7, // unidad de medida
//				   'codigo' => 'P0001',
//				   'ds' => 'Descripcion del producto P0001',
//				   'precio' => 100,
//				   'importe' => 121,
//				   'imp_iva' => 21,
//				   'iva_id' => 5, // tasa de iva 5: 21%
//				   'u_mtx' => 123456, // unidad MTX (packaging)
//				   'cod_mtx' => 1234567890123, // cÃ³digo de barras para MTX
//				   'despacho' => 'NÂº 123456',
//				   'dato_a' => NULL, 'dato_b' => NULL, 'dato_c' => NULL,
//				   'dato_d' => NULL, 'dato_e' => NULL,
//				   'bonif' => 0,
//			  ),
//		 ),
//		 'ivas' => array(
//			  array(
//				   'base_imp' => 100,
//				   'importe' => 21,
//				   'iva_id' => 5,
//			  ),
//		 ),
//		 // Comprobantes asociados (solo notas de crÃ©dito y dÃ©bito):
//		 //'cbtes_asoc' => array (
//		 //   array('cbte_nro' => 1234, 'cbte_punto_vta' => 2, 'cbte_tipo' => 91, ),
//		 //   array('cbte_nro' => 1234, 'cbte_punto_vta' => 2, 'cbte_tipo' => 5, ),
//		 // ),
//		 'tributos' => array(
//			  array(
//				   'alic' => '1.00',
//				   'base_imp' => '100.00',
//				   'desc' => 'Impuesto Municipal Matanza',
//				   'importe' => '1.00',
//				   'tributo_id' => 99,
//			  ),
//		 ),
//		 'permisos' => array(),
//		 'datos' => array(),
//	));

    $this->_doc = json_decode($doc, true);
    $dirAnt     = getcwd();
    chdir($this->dirRece1);
    file_put_contents('entrada.json', json_encode(array($this->doc)));
    chdir($dirAnt);
  }

  public static function getTipoDoc($tipo)
  {
    return Helpers::qryScalar("select afip from tipo_documento where id = $tipo");
  }

  public function barras($codigo = null)
  {
    //vd($this->doc);
    if (!$codigo) {
      $texto  = $this->codigoDeBarras();
      $output = "";
      $ret    = "";
      $dirAnt = getcwd();

    } else {
      $texto = $codigo;
    }
    chdir($this->dirRece1);
    //$x = exec("e:/tools/python/python.exe --version", $output);
//    $x = exec("e:\\tools\\python\\python.exe --version 2> jj", $output);
    //$x = exec("dir", $output);
//    vd($x, $output);
    exec("del barras.png");
    $cmd = "$this->scriptBarras --barras $texto --archivo barras.png";
    $a   = exec($cmd, $output, $ret);
    if (!file_exists($this->dirRece1 . "barras.png")) {
      vd("no existe " . $this->dirRece1 . "barras.png", $cmd, $a, $output, $ret);
      throw new Exception("no existe " . $this->dirRece1 . "barras.png");
    }
    chdir($dirAnt);
    return $this->dirRece1;
  }

  public function codigoDeBarras()
  {
    $comprob_id = str_pad($this->doc['tipo_cbte'], 2, "0", STR_PAD_LEFT);
    $vto        = date("Ymd");
    return "20140679047" . $comprob_id . "0003" . $vto;
  }

  public function setDocProp($prop, $value)
  {
    if ($prop == 'importe') {
//	  $this->_doc['impTotal'] = $value;
//	  $this->_doc['impNeto'] = $value;
      $this->_doc['imp_total']    = $value;
      $this->_doc['imp_neto']     = $value;
      $this->_doc['imp_tot_conc'] = 0;
      $this->_doc['imp_iva']      = 0;
      $this->_doc['imp_trib']     = 0;
    } else {
      $this->_doc[$prop] = $value;
    }
  }

  public function getDoc()
  {
    return $this->_doc;
  }

  public function mandaDoc()
  {
    $ultimo_numero = $this->getUltimoNumero();
    if(!$ultimo_numero){
      return null;
    }
    $proximo_numero = $ultimo_numero + 1;
//    $this->_doc['cbtdesde']  = $proximo_numero;  // para WSFEv1
    $this->_doc['cbt_desde'] = $proximo_numero;  // para WSFEv1
    $this->_doc['cbt_hasta'] = $proximo_numero;  // para WSFEv1
//    $this->_doc['nro_doc']   = $proximo_numero;
//    ve($this->_doc);
    $output = "";
    $dirAnt = getcwd();
    chdir($this->dirRece1);
    file_put_contents('entrada.json', json_encode(array($this->_doc)));
    if (isset($_GET['debug'])) {
      vd("$this->scriptRece1 rece.ini /json");
    }
    exec("$this->scriptRece1 rece.ini /json");
    $json       = file_get_contents('salida.json', $output);
    $this->_doc = json_decode($json, True)[0];
    //vd($this->doc);
    chdir($dirAnt);
    return "ok";
//	$this->_doc = (object) $docs[0];
  }

  public function getUltimoNumero()
  {
    $output    = "";
    $tipo_cbte = $this->doc["tipo_cbte"];
    $punto_vta = $this->doc["punto_vta"];
    $dirAnt    = getcwd();
    chdir($this->dirRece1);
    if (isset($_GET['debug'])) {
      vd("$this->scriptRece1 rece.ini /json /ult $tipo_cbte $punto_vta");
    }
    //vd($this->dirRece1,"$this->scriptRece1 rece.ini /json /ult $tipo_cbte $punto_vta");
    $a = exec("$this->scriptRece1 rece.ini /json /ult $tipo_cbte $punto_vta", $output);
    if (!$a and !$output) {
      ve("error ejecutando " . "$this->scriptRece1 rece.ini /json /ult $tipo_cbte $punto_vta", $a, $output);
    }
    if (!file_exists('salida.json')) {
      vd("No existe salida.json", $a, $output);
    }
    $json     = file_get_contents('salida.json');
    $data     = json_decode($json, True)[0];
    if(isset($data["cbt_desde"])){
      $cbte_nro = intval($data["cbt_desde"]);
      //$this->_doc["cbte_nro"] = $cbte_nro;
      return $cbte_nro;
    }else{
      return "malll";
    }
    chdir($dirAnt);
  }

  public function getAfipDoc($tipoComprob, $puntoVenta, $nro)
  {
    $dirAnt = getcwd();
    chdir($this->dirRece1);
    exec("$this->scriptRece1 /debug /json /get $tipoComprob $puntoVenta $nro");
    $json = file_get_contents('salida.json');
    chdir($dirAnt);
    return $json;
  }

  public function grabaFactura($doc)
  {
    $da         = new DocAfip();
    $da->doc_id = $doc->id;
    $da->cae    = $this->_doc["cae"];
    $da->doc    = json_encode($this->_doc);
    if (!$da->save()) {
      vd($da->errors);
    };
  }

}
