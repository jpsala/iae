<?php

Yii::import('application.models._base.BaseAlumno');

/**
 * Representa un alumno
 * @todo getDivision(Ciclo $ciclo) Para traer el AlumnoDivision en la que estaba este alumno en ese ciclo, quiza podria reemplazar a getDivisionActiva si se le pasa $ciclo = null
 * @property AlumnoDivision $alumnoDivisionActiva  Devuelve el AlumnoDivision activo del alumno
 */
class Alumno extends BaseAlumno
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getResponsable()
    {
        $select = "select * from pariente p where p.id = $this->encargado_pago_id";
        return Helpers::qryObj($select);
    }

    public function setEstado($estado_id_nuevo, $save = false)
    {
        /* @var $nuevoEstado AlumnoEstado */
        $nuevoEstado = AlumnoEstado::model()->findByPk($estado_id_nuevo);
        //ve($nuevoEstado->attributes);
        $this->activo = $nuevoEstado->activo_admin;
        $this->egresado = $nuevoEstado->egresado;
        $this->ingresante = $nuevoEstado->ingresante;
        $this->estado_id = $nuevoEstado->id;
        if ($save) {
            if (!$this->save()) {
                throw new Exception(json_encode(array("Error cambiando el estado", $this->errors)));
            }
        }
    }

    public function rules()
    {
        return CMap::mergeArray(parent::rules(), array(
                array('matricula', 'unique'),
                //array("apellido, sexo, calle, numero, numero_documento, fecha_nacimiento, email, telefono_1", "required"),
//                    array("email", "email"),
            )
        );
    }

    public function scopes()
    {
        return array(
            'activos' => array(
                'condition' => 'activo=1',
                'order' => "sexo asc, apellido, nombre",
            ),
        );
    }

    public function beforeValidate()
    {
        if (parent::beforeSave()) {
            return true; //Helpers::validateDates($this);
        } else {
            return false;
        }
    }

    public function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord and $this->scenario !== "import" and $this->ingresante) {
                $nuevaMatricula = $this->matricula;
                while (true) {
                    $existe = Helpers::qryScalar(
                        "select count(*) from alumno where matricula = $nuevaMatricula"
                    );
                    if ($existe) {
                        $nuevaMatricula++;
                    } else {
                        break;
                    }
                };
                $this->matricula = $nuevaMatricula;
            } elseif ($this->isNewRecord and $this->scenario !== "import") {
                $this->matricula = $this->siguienteMatricula;
            }
            return true; //Helpers::validateDates($this);
        } else return false;
    }

    public function afterSave()
    {
        parent::afterSave();
    }

    public function getNivel_id()
    {
        $ad_id = $this->alumnoDivisionActivaId;
        $nivel_id = Yii::app()->db->qryScalar("
      select a.nivel_id
        from alumno_division ad
          inner join division d on d.id = ad.division_id
          inner join anio a on a.id = d.anio_id
        where ad.id = $ad_id
    ");
        return;
    }

    /**
     * @author JP
     *
     * Devuelve la division activa del alumno
     *
     * @return AlumnoDivision
     *
     */
    public function getAlumnoDivisionActiva($solo_id = false)
    {
        $ad = AlumnoDivision::model()->find(
            'alumno_id = :alumno_id and activo = true and !borrado', array('alumno_id' => $this->id)
        );
        return $solo_id ? $ad->id : $ad;
    }

    public function getAlumnoDivisionActivaId()
    {

        return $this->getAlumnoDivisionActiva(1);
    }

    static public function getSiguienteMatricula()
    {
        $qry = Yii::app()->db->createCommand("SELECT max(cast(matricula AS SIGNED)) AS max FROM alumno WHERE matricula")->queryRow();
        return $qry["max"] + 1;
    }

    static public function getSiguienteMatriculaIngresante()
    {
        $qry = Yii::app()->db->createCommand("SELECT max(cast(matricula AS SIGNED)) AS max FROM alumno WHERE matricula")->queryRow();
        return $qry["max"] + 1;
    }

    /**
     * Se le asigna una nueva division a este alumno
     * <code>
     * Se da de alta una instancia de AlumnoDivision y se le asignan
     * los id's del alumno y de la division etc
     * De tener este alumno una division activa se genera una excepción
     * ***********
     * Modificado Octavio
     * Debería recorrer todos los cursos del nivel y determinar cuales materias que cursó el alumno son
     * de una orientación determinada. Si el curso es diferente al actual poner la materia en "estado de
     * rendir equivalencia", si el curso es igual al actual y el ciclo tambien (evitando así repetidores) determinar
     * que notas tiene registradas para recuperar. Si no tiene ninguna asignaura por orientación se edita alumno_division
     * asignando la nueva división al alumno.
     *
     *
     * </code>
     *
     * @todo Terminar... (asignaDivision)
     *  ver bien que se va a hacer con las materias / notas parciales a rendir si hay cambio de orientación.
     * @author JP
     * @param Division $division
     */
    public function asignaDivision(Division $division)
    {

        $ada = $this->alumnoDivisionActiva;

        // recorro las cursadas anteriores
        $nivel_id = $ada->division->anio->Nivel_id;
        $ad_anteriores = AlumnoDivision::model()->findAllBySql("
            select ad.* from alumno_division ad
                    inner join division d on d.id = ad.division_id
                    inner join anio c on c.id = d.anio_id
            where alumno_id = $this->id and
                    ciclo_id <> $ada->Ciclo_id and
                    c.nivel_id = $nivel_id and
                    d.Orientacion_id is not null
         ");

//    $tr = Yii::app()->db->beginTransaction();
        $ada->activo = 0;
        $ada->borrado = 2; //cambio de division
        $ada->alumno_division_estado_id = 3; // cambio de división
        // $ada->fecha_baja = date("Y/m/d", time());
        if (!$ada->save()) {
            var_dump(json_encode($ada->errors));
            die;
        }
        $existe = Helpers::qryScalar("SELECT COUNT(*)
				FROM alumno_division ad
				WHERE ad.alumno_id = $ada->Alumno_id and ad.activo");
        if ($existe) {
            var_dump(json_encode(array("No puede tener mas de dos divisiones activas")));
            die;
        }
        $ad = new AlumnoDivision();
        $ad->Alumno_id = $this->id;
        $ad->Division_id = $division->id;
        $ad->activo = 1;
        $ad->fecha_alta = Helpers::date(null, time());
        $ad->fecha_alta = Helpers::date(null, time());
        $ad->Ciclo_id = Ciclo::getActivo()->id;
        if (!$ad->save()) {
            var_dump(json_encode($ad->errors));
            die;
        }
        /* @var $nota Nota */
        /*
         * Copio las notas de la asignación anterior a la nueva
         * @todo:Estará bien esto????
         */
        if ($ada->cantNotasCargadas) {
            $notas = Nota::model()->findAll("Alumno_Division_id = $ada->id");
            //var_dump($notas[0]->attributes);die;
            foreach ($notas as $nota) {
                $notaNueva = new Nota();
                $notaNueva->Asignatura_id = $nota->Asignatura_id;
                $notaNueva->Logica_item_id = $nota->Logica_item_id;
                $notaNueva->nota = $nota->nota;
                $notaNueva->Alumno_Division_id = $ad->id;
                $notaNueva->save();
            }
        }
//    $tr->rollback();
//    $tr->commit();
    }

    public function getSocio()
    {
        return Socio::model()->find("Alumno_id = $this->id");
    }

    public function getNombreCompleto()
    {
        return $this->apellido . ", " . $this->nombre;
    }

    public static function getLiquidacionDebitosVisaRows($soloPositivos = true, $banco = null)
    {
        $liquid_conf_id = LiquidConf::getActiveId();
        $exentos = self::getMatriculasNoLiquidar();
        if(!$banco) {
          //Galicia
          $andBanco = " AND SUBSTR(a.visa_numero, 1, 6) NOT IN (406997,433822,455198,455197,455196)";
        }else{
          //superville
          $andBanco = " AND SUBSTR(a.visa_numero, 1, 6) IN (406997,433822,455198,455197,455196)";
        }
        if ($exentos) {
            $whereExentos = " and not a.matricula in ($exentos)";
        } else {
            $whereExentos = "";
        }
        $comprobFactura = Comprob::FACTURA_VENTAS;
        $whereSigno = $soloPositivos ? 'dl.saldo_anterior + doc.total > 0' : ' true ';
        //flia/mat/alumno/Total/n°Visa/ID Facturación
        $docsSelect = "
            select a.familia_id, a.matricula, concat(a.apellido, ' ,', a.nombre) as alumno,
							doc.detalle,
							/*dl.saldo_anterior + doc.total total,*/
							saldo(s.id,null) as total,
							a.visa_numero, doc.id as doc_id, a.beca,
							/*dl.saldo_anterior as saldo_calculado,*/
							saldo(s.id, null) as saldo,
							dl.novedades as monto_novedades,
							saldo(s.id, null) as saldo_actual,
							saldo(s.id, null) - dl.saldo_anterior as movimientos_posteriores,
							lc.fecha_venc_1, lc.fecha_venc_2, nivel.nombre as nivel, anio.nombre as anio, divi.nombre as division
						from doc
							inner join doc_liquid dl on dl.id = doc.doc_liquid_id
							inner join socio s on s.id = doc.Socio_id
							inner join alumno a on a.id = s.Alumno_id and a.activo = 1
							inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_admin = 1
							inner join liquid_conf lc on lc.id = dl.liquid_conf_id and lc.id = $liquid_conf_id
							inner join talonario t on t.id = doc.talonario_id and t.comprob_id = $comprobFactura
							inner join alumno_division ad on ad.Alumno_id = a.id and ad.activo = 1
							inner join division divi on divi.id = ad.Division_id
							inner join anio on anio.id = divi.Anio_id
							inner join nivel on nivel.id = anio.Nivel_id
						where	$whereSigno AND a.visa_activo  $whereExentos/* and dl.saldo_anterior + doc.total - saldo(s.id,null) > 2*/ $andBanco
						order by /*nivel.orden, anio.nombre, divi.nombre, a.sexo desc,*/ a.apellido, a.nombre";
        //vd($docsSelect);
        $docs = Helpers::qryAll($docsSelect);
        //vd($docs);
        return $docs;;
    }

    public static function getLiquidacionDebitosVisaTotal($soloPositivos = true, $banco = null)
    {

        $liquid_conf_id = LiquidConf::getActiveId();
        $exentos = self::getMatriculasNoLiquidar();
        if(!$banco) {
          $andBanco = "AND SUBSTR(a.visa_numero, 1, 6) NOT IN (406997,433822,455198,455197,455196)";
        }else{ //superville
          $andBanco = "AND SUBSTR(a.visa_numero, 1, 6) IN (406997,433822,455198,455197,455196)";
        }
        if ($exentos) {
            $whereExentos = " and not a.matricula in ($exentos)";
        } else {
            $whereExentos = "";
        }
        $comprobFactura = Comprob::FACTURA_VENTAS;
        $whereSigno = $soloPositivos ? 'dl.saldo_anterior + doc.total > 0' : ' true ';
        $docsSelect = "
				select /*sum(dl.saldo_anterior + doc.total)*/ sum(saldo(s.id,null)) as total
					from doc
						inner join doc_liquid dl on dl.id = doc.doc_liquid_id
						inner join socio s on s.id = doc.Socio_id
						inner join alumno a on a.id = s.Alumno_id and a.activo = 1
						inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_admin = 1
						inner join liquid_conf lc on lc.id = dl.liquid_conf_id and lc.id = $liquid_conf_id
						inner join talonario t on t.id = doc.talonario_id and t.comprob_id = $comprobFactura
						inner join alumno_division ad on ad.Alumno_id = a.id and ad.activo = 1
						inner join division divi on divi.id = ad.Division_id
						inner join anio on anio.id = divi.Anio_id
						inner join nivel on nivel.id = anio.Nivel_id
					where	$whereSigno AND a.visa_activo  $whereExentos $andBanco
					order by nivel.orden, anio.nombre, divi.nombre, a.sexo desc, a.apellido, a.nombre";
        //vd($docsSelect);
        $total = Helpers::qryScalar($docsSelect);
        //vd($docs);
        return $total;;
    }

    public static function getLiquidacionData($banco = null)
    {
        $liquid_conf_id = LiquidConf::getActiveId();
        $exentos = self::getMatriculasNoLiquidar();
         if(!$banco) {
          $andBanco = "AND SUBSTR(a.visa_numero, 1, 6) NOT IN (406997,433822,455198,455197,455196)";
        }else{ //superville
          $andBanco = "AND SUBSTR(a.visa_numero, 1, 6) IN (406997,433822,455198,455197,455196)";
        }
        if ($exentos) {
            $whereExentos = " and not a.matricula in ($exentos)";
        } else {
            $whereExentos = "";
        }
        $comprobFactura = Comprob::FACTURA_VENTAS;
        $docsSelect = "
				select doc.id as doc_id, a.matricula, concat(a.apellido, ' ,', a.nombre) as alumno, a.beca,
					dl.cuota, dl.saldo_anterior as saldo, dl.novedades as monto_novedades,
					dl.saldo_anterior + doc.total as total, doc.detalle,
					saldo(s.id, null) as saldo_actual, dl.saldo_anterior as saldo_al_liquidar,
					lc.fecha_venc_1, lc.fecha_venc_2, nivel.nombre as nivel, anio.nombre as anio, divi.nombre as division
				from doc
					inner join doc_liquid dl on dl.id = doc.doc_liquid_id
					inner join socio s on s.id = doc.Socio_id
					inner join alumno a on a.id = s.Alumno_id and a.activo = 1
					inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_admin = 1
					inner join liquid_conf lc on lc.id = dl.liquid_conf_id and lc.id = $liquid_conf_id
					inner join talonario t on t.id = doc.talonario_id and t.comprob_id = $comprobFactura
					inner join alumno_division ad on ad.Alumno_id = a.id and ad.activo = 1
					inner join division divi on divi.id = ad.Division_id
					inner join anio on anio.id = divi.Anio_id
					inner join nivel on nivel.id = anio.Nivel_id
				where	dl.saldo_anterior + doc.total > 0 $whereExentos $andBanco
				order by nivel.orden, anio.nombre, divi.nombre, a.sexo desc, a.apellido, a.nombre";
        $docs = Helpers::qryAll($docsSelect);
        //vd($docsSelect);
        return $docs;
    }

    public static function getLiquidacionDataTotales()
    {
        $liquid_conf_id = LiquidConf::getActiveId();
        $exentos = self::getMatriculasNoLiquidar();
        if ($exentos) {
            $whereExentos = " and not a.matricula in ($exentos)";
        } else {
            $whereExentos = "";
        }
        $comprobFactura = Comprob::FACTURA_VENTAS;
        $select = "
			select sum(dl.saldo_anterior + doc.total) as total, count(*) as cant, max(doc.fecha_vto2) as fecha_vto2
			from doc
				inner join doc_liquid dl on dl.id = doc.doc_liquid_id
				inner join socio s on s.id = doc.Socio_id
				inner join alumno a on a.id = s.Alumno_id and a.activo = 1
				inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_admin = 1
				inner join liquid_conf lc on lc.id = dl.liquid_conf_id and lc.id = $liquid_conf_id
				inner join talonario t on t.id = doc.talonario_id and t.comprob_id = $comprobFactura
				left join alumno_division ad on ad.Alumno_id = a.id and ad.activo = 1
				left join division divi on divi.id = ad.Division_id
				left join anio on anio.id = divi.Anio_id
				left join nivel on nivel.id = anio.Nivel_id
			where dl.saldo_anterior + doc.total > 0 $whereExentos
			order by nivel.orden, anio.nombre, divi.nombre, a.apellido, a.nombre";
        //  vd($select);
        return Helpers::qry($select);
    }

    public static function getLiquidacionDataTotalConNegativos()
    {
        $liquid_conf_id = LiquidConf::getActiveId();
        $exentos = self::getMatriculasNoLiquidar();
        if ($exentos) {
            $whereExentos = " not a.matricula in ($exentos)";
        } else {
            $whereExentos = "true";
        }
        $comprobFactura = Comprob::FACTURA_VENTAS;
        $select = "
			select sum(dl.saldo_anterior + doc.total) as total
			from doc
				inner join doc_liquid dl on dl.id = doc.doc_liquid_id
				inner join socio s on s.id = doc.Socio_id
				inner join alumno a on a.id = s.Alumno_id and a.activo = 1
				inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_admin = 1
				inner join liquid_conf lc on lc.id = dl.liquid_conf_id and lc.id = $liquid_conf_id
				inner join talonario t on t.id = doc.talonario_id and t.comprob_id = $comprobFactura
				left join alumno_division ad on ad.Alumno_id = a.id and ad.activo = 1
				left join division divi on divi.id = ad.Division_id
				left join anio on anio.id = divi.Anio_id
				left join nivel on nivel.id = anio.Nivel_id
			where $whereExentos";
        //vd($select);
        return Helpers::qryScalar($select);
    }

    public static function getMatriculasNoLiquidar()
    {
        $liquid_conf_id = LiquidConf::getActiveId();
        $exentosData = Helpers::qryScalar("select no_matricular from liquid_conf where id = $liquid_conf_id");
        $exentos = $exentosData ? str_replace("\r\n", ",", $exentosData) : "";
        return $exentos;
    }

}
