<?php

Yii::import('application.models._base.BaseAlumnoDivision');

include_once("formulas/LogicaActiva.php");

/**
 * Representa la Division de un Alumno
 * 
 * Por ej. es devuelta por $alumno->divisionActiva
 * 
 * @todo revisar si es necesaria la propiedad "activo", parece que alcanza con "Ciclo_id", podria ser un getter?
 * @todo agregar un setter para "activo", no puede activar un AlumnoDivision si hay uno activo
 */
class AlumnoDivision extends BaseAlumnoDivision {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function scopes() {
    $alias = $this->tableAlias;
    return array(
        'activos' => array(
            "with"=>"alumno",
            'condition' => "alumno.activo=1 and $alias.activo = 1",
            "order" => "alumno.sexo desc, alumno.apellido asc, alumno.nombre asc",
        ),
    );
  }

  public function beforeValidate() {
    if ($this->isNewRecord) {
      $this->fecha_alta = new CDbExpression('NOW()');
    }
    return parent::beforeValidate();
  }

  /**
   * Devuelve las notas del alumno correspondientes a todas las asignaturas
   * 
   * Llama a @see{ getNotas(Asignatura) }
   * @author JP
   * @todo revisar este codigo, falta optimizar muuuucho
   * @return mixed[] array[][0]=asignatura y array[][1]=Nota[]
   */
  public function getNotas() {
    $notas = array();
    foreach ($this->division->divisionAsignaturas as $divisionAsignatura) {
      $asignatura = $divisionAsignatura->asignatura;
      $notas[] = $this->getNotasAsignatura($asignatura);
    }
    return $notas;
  }

  /**
   * Devuelve las notas del alumno correspondientes a $asignatura
   * @author JP
   * @param Asignatura $asignatura
   * @return Nota[]
   */
  public function getNotasAsignatura($asignatura, $logica_periodo_id = null) {
    $logica_id = Logica::getLogicaId($asignatura->Asignatura_Tipo_id, $this->ciclo->id);
    $notas = Formula::getNotasAsignatura($this->id, $asignatura, $logica_id, $logica_periodo_id);
    return $notas;
  }

  /**
   * Graba las notas de esta asignatura {@see AlumnoDivision->getNotasAsignatura(Asignatura $asignatura)}
   * @author JP
   * @param Asignatura $asignatura
   * @param Nota[] $notas
   * @return Nota[]
   */
  public function setNotasAsignatura($asignatura, $notas) {

    $logica = Logica::getLogica($asignatura->asignaturaTipo, $this->ciclo);
    $notas = $logica->setNotas($this, $asignatura, $notas);
    return $notas;
  }

  /**
   * Devuelve el total de inasistencias de este alumno en esta division
   * @todo Octavio:Ver si vale la pena meterlo en una logica porque despues habria que expandirlo para que devuelva las inasistencias por periodo, nose, hay que pensarlo bien
   * @todo que pasa si no tiene inasistencias?
   * , @return Integer El total de inasistencias de este alumno en esta division
   */
  public function getInasistencias() {
    $row = Yii::app()->db->createCommand("
                select sum(it.valor) as total from inasistencia_detalle idet 
                                left join inasistencia_tipo it
                                    on idet.inasistencia_tipo_id = it.id 
                            where idet.Alumno_Division_id = $this->id            
            ")->queryRow();
    return $row['total'];
  }

  public function getCantNotasCargadas() {
    $row = Yii::app()->db->createCommand("
            select count(*) as cant from nota n 
                where n.Alumno_Division_id = $this->id and
                      n.nota is not null
            ")->queryRow();
    return $row['cant'];
  }

}