<?php

Yii::import('application.models._base.BaseAnio');

class Anio extends BaseAnio {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeSave() {
        if(!$this->orden and $this->isNewRecord){
            $rec = Yii::app()->db->createCommand("select max(orden) as orden from anio where Nivel_id = $this->Nivel_id")->queryRow();
            $this->orden = $rec["orden"] + 10;
        }
        return parent::beforeSave();
    }
    
    public function relations() {
        $relations = parent::relations();
        return $relations;
    }

    /**
     * Devuelve las asignaturas activas de este curso
     * 
     * @return array Las asignaturas activas de este curso
     * @author JP
     */
    public function getAsignaturasActivas() {
        $asignaturasActivas = array();
        foreach ($this->asignaturas as $asignatura) {
            if ($asignatura->activa)
                $asignaturasActivas[] = $asignatura;
        }
        return $asignaturasActivas;
    }

    /**
     * Da de alta una division "$nombre" en este curso.
     * 
     * Detalle:
     * - 1: Alta de division
     * - 2: Se asigna curso a la division
     * - 3: Recorre las asignaturas de este curso
     *   - 3a: Crea {@link DivisionAsignatura}
     * 
     * @author JP
     * @param string $nombre El nombre del nuevo curso
     * @param int $activa
     * @return Division 
     */
    public function altaDivision($nombre, $activa = 1) {
        $d = new Division();
        $d->nombre = $nombre;
        $d->Anio_id = $this->id;
        $d->activo = $activa;
        $d->save();
        foreach ($this->asignaturasActivas as $a) {
            $ad = new DivisionAsignatura();
            $ad->attributes = array(
                'Division_id' => $d->id,
                'Asignatura_id' => $a->id,
            );
            $ad->save();
        }
        return $d;
    }

}