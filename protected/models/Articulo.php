<?php

Yii::import('application.models._base.BaseArticulo');

class Articulo extends BaseArticulo
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
  function numeroValidator() {
        if (!is_numeric($this->precio_neto)) {
            $this->addError("precio_neto", "El precio neto debe ser numérico");
        }
        return false;
    }
    
    public function beforeSave() {
    
        if ($this->isNewRecord)
            $this->fecha_creacion = new CDbExpression('NOW()');
        else
            $this->fecha_modificacion = new CDbExpression('NOW()');

        return parent::beforeSave();
    }
    
     public function rules() {
        return array_merge(parent::rules(), array(
                    array('precio_neto', 'numeroValidator'),
                    array('nombre','unique','message'=>'{attribute}:{value} ¡Ya Existe!'),

                ));
    }
}