<?php

Yii::import('application.models._base.BaseAsignatura');

class Asignatura extends BaseAsignatura {

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function getActiva($ciclo_id = null) {

        $ciclo_id = $ciclo_id ? $ciclo_id : Ciclo::getCicloParaCargaDeNotas()->id;

        $desde = $this->desdeCiclo->id;
        $hasta = $this->hastaCiclo ? $this->hastaCiclo->id : 99999999;

        return ($ciclo_id >= $desde and $ciclo_id <= $hasta) ? true : false;
    }
    
    public function getLogica($ciclo_id = null){
        $ciclo_id = $ciclo_id ? $ciclo_id : Ciclo::getCicloParaCargaDeNotas()->id;
        return LogicaCiclo::model()->find("Ciclo_id = $ciclo_id and Asignatura_Tipo_id = $this->Asignatura_Tipo_id")->logica;
    }

    public static function getAsignaturaTipoId($asignatura_id) {
         return Yii::app()->db->createCommand("
            select t.id from asignatura a 
                inner join asignatura_tipo t on a.Asignatura_Tipo_id = t.id
            where a.id = $asignatura_id
        ")->queryScalar();
    }
    
//    public function procesaNotas($notasAValidar){
//        return $notasValidadas = $this->logica->validaNotas($alumnoDivision, $asignatura, $notasAValidar);
//        //return $notasValidadas;
//    }
    
}