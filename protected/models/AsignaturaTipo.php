<?php

Yii::import('application.models._base.BaseAsignaturaTipo');

/**
 * Cada {@see Asignatura} tiene una AsignaturaTipo, que a su vez 
 * tiene una {@see LogicaCiclo} para cada {@see ciclo} en {@see LógicaCiclo} y esta última
 * tiene una {@see Logica} con sus {@see LogicaItems} que sirven para calcular la nota
 * por cada período de la {@see Asignatura}
 * 
 * @author JP
 */
class AsignaturaTipo extends BaseAsignaturaTipo {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Devuelve la lógica {@see Logica} asociada con este tipo de asignatura para el Ciclo $ciclo o el Ciclo activo si $ciclo es Null
     * @param Ciclo $ciclo si es null usa el ciclo activo
     * @return Logica 
     */
    public function getLogica($ciclo = null) {

        $ciclo = $ciclo ? $ciclo : Ciclo::getCicloParaCargaDeNotas()->id;
        return Logica::model()->findBySql("
                select l.* from logica l
                    inner join logica_ciclo lc 
                        on lc.logica_id = l.id and 
                           lc.asignatura_tipo_id=$this->id and 
                           lc.ciclo_id=$ciclo");
    }

    public static function AsignaturaTipoIdDesdeAsignaturaId($asignatura_id) {
        return Yii::app()->db->createCommand("
            select t.id from asignatura a 
                inner join asignatura_tipo t on a.Asignatura_Tipo_id = t.id
            where a.id = $asignatura_id
        ")->queryScalar();
    }

}