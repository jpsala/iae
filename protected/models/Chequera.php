<?php

Yii::import('application.models._base.BaseChequera');

class Chequera extends BaseChequera
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
  
  public function getSiguienteNumero(){
    return $this->siguiente_numero;
  }
  
  public function getNumeroChequera($destino_id){
    $sql = "select max(numero) from chequera where destino_id = $destino_id";
    $num = Helpers::qryScalar($sql);
    return $num + 1;
  }
  
  public static function listData($disponibles = null) {
        $where = $disponibles ? " where ultimo_numero > siguiente_numero" : "";
        $chequeras = Helpers::qryAll("select id, descripcion, numero from chequera $where order by descripcion");
        $ret = array();
        foreach ($chequeras as $chequera) {
           
            $ret[$chequera["id"]] = $chequera["descripcion"]  . " $chequera[numero]";
        }
        return $ret;
    }
}