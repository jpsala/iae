<?php

Yii::import('application.models._base.BaseCiclo');

// @TODO: No debería poder editar un ciclo que está bloqueado ????
class Ciclo extends BaseCiclo {

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public static function getActivoId() {
		return Helpers::qryScalar("select id from ciclo where activo = true");
	}

	public static function getActivoNombre() {
		return Helpers::qryScalar("select nombre from ciclo where activo = true");
	}

	public static function getActivo() {
		return Ciclo::model()->find('activo = true');
	}

  public static function esActual() {
    return self::getActivoId() === self::getCicloIdParaCargaDeNotas();
  }

	public static function getCicloIdParaCargaDeNotas() {
		$ciclo_id = Opcion::getOpcionText("ciclo_id", Null, "educativoNotas");
   if ($ciclo_id) {
     return Helpers::qryScalar("select id from ciclo where id = $ciclo_id");
   } else {
//     vd2(Helpers::qry("select * from ciclo where activo = 1"));
     return Helpers::qryScalar("select id from ciclo where activo = 1");
   }
	}

	public static function getCicloIdParaInformesEducativo($log = null) {
		$ciclo_id = Opcion::getOpcionText("ciclo_id", Null, "educativoInformes");
		if ($ciclo_id) {
			return Helpers::qryScalar("select id from ciclo where id = $ciclo_id");
		} else {
			return Helpers::qryScalar("select id from ciclo where activo = 1");
		}
	}

	public static function getCicloIdParaInformesAdmin($log = null) {
		$ciclo_id = Opcion::getOpcionText("ciclo_id", Null, "adminInformes");
		if ($ciclo_id) {
			return Helpers::qryScalar("select id from ciclo where id = $ciclo_id");
		} else {
			return Helpers::qryScalar("select id from ciclo where activo = 1");
		}
	}

	public static function getCicloParaCargaDeNotas() {
		$ciclo_id = self::getCicloIdParaCargaDeNotas();
		return Ciclo::model()->findByPk($ciclo_id);
	}

	public static function getCicloParaInformesEducativo() {
		$ciclo_id = self::getCicloIdParaInformesEducativo();
		return Ciclo::model()->findByPk($ciclo_id);
	}

	public static function getCicloNombreParaCargaDeNotas() {
		$ciclo = self::getCicloParaCargaDeNotas();
		return $ciclo->nombre;
	}

}
