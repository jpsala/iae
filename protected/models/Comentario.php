<?php

Yii::import('application.models._base.BaseComentario');

class Comentario extends BaseComentario {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeSave() {
        if ($this->isNewRecord) {
            $this->fecha_creacion = date("Y-m-d G:i");
            $this->fecha_modificacion = date("Y-m-d G:i");
        } else {
            $this->fecha_modificacion = date("Y-m-d G:i");
        }
        return parent::beforeSave();
    }
    
    public function getModificado(){
        return ($this->fecha_creacion != $this->fecha_modificacion);
    }

}