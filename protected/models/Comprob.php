<?php

Yii::import('application.models._base.BaseComprob');

class Comprob extends BaseComprob {

    const FACTURA_VENTAS = 4;
    const FACTURA_COMPRAS = 5;
    const NC_VENTAS = 3;
    const ND_VENTAS = 2;
    const NC_COMPRAS = 7;
    const ND_COMPRAS = 6;
    const RECIBO_VENTAS = 1;
    const AJUSTE_CAJA_ENTRADA = 9;
    const AJUSTE_CAJA_SALIDA = 10;
    const DEPOSITO_BANCARIO_SALIDA = 11;
    const DEPOSITO_BANCARIO_ENTRADA = 14;
    const TRANSFERENCIA_ENTRADA = 12;
    const TRANSFERENCIA_SALIDA = 13;
    const OP = 8;
    const MOVIMIENTO_BANCARIO_D = 15;
    const MOVIMIENTO_BANCARIO_H = 16;
    const RECIBO_BANCO = 17;
    const RETENCION = 18;
    const RECIBO_ELECTRONICO = 19;
    const NC_ELECTRONICO = 20;
    const ND_ELECTRONICO = 21;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function mueveCaja($comprob_id) {
        return in_array($comprob_id, $muevenCaja);
    }

    public static function getComprobante() {
        $select = "select c.*,
         concat(c.nombre ,\" \", IF( c.compra_venta = 'C', \"compra\", \"venta\")) as nombre_comprob
         from comprob c";
        $r = Helpers::qryAll($select);
        return $r;
    }

    public static function listData($tipo = null) {
        $where = $tipo ? " where compra_venta = $tipo" : "";
        $comprobs = Helpers::qryAll("select id, nombre, compra_venta from comprob $where order by compra_venta, nombre");
        $ret = array();
        foreach ($comprobs as $comprob) {
            if(($comprob["compra_venta"] == "V")){
                $tipo = "Venta";
            }elseif(($comprob["compra_venta"] == "C")){
                $tipo = "Compra";
            }else{
                $tipo = "Fondos";
            }
            $ret[$comprob["id"]] = $comprob["nombre"]  . " $tipo";
        }
        return $ret;
    }

  public static function tipoComprobElectronico($RECIBO_ELECTRONICO){
    return Helpers::qryScalar("
      SELECT c.afip_comprob
        FROM comprob c
          INNER JOIN talonario t ON c.id = t.comprob_id
        WHERE c.id = $RECIBO_ELECTRONICO");
  }

}

