<?php
Yii::import('application.models._base.BaseConcepto');

class Concepto extends BaseConcepto {
    //todo Ver conceptos de anulacion

    const ANUL_REC_VENTAS = 1797;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    static function getPathRecursive($concepto, &$paths) {
        $conceptos = Concepto::model()->findAll(array(
                "condition" => "concepto_id = $concepto->id",
                "order" => "id"
        ));
        /* @var $concepto Concepto */
        $paths[] = array("path" => $concepto->path, "id" => $concepto->id, "ingreso_egreso" => $concepto->ingreso_egreso);
        foreach ($conceptos as $concepto) {
            self::getPathRecursive($concepto, $paths);
        }
    }

    public function getPath() {
        $parent_id = $this->concepto_id;
        $conceptos = array($this->nombre);
        while ($parent_id) {
            $concepto = Yii::app()->db->createCommand("
        select concepto_id, nombre from concepto where id = $parent_id
      ")->queryRow();
            $parent_id = $concepto["concepto_id"];
            $conceptos[] = $concepto["nombre"];
        }
        return "/" . implode("/", array_reverse($conceptos));
    }
    static public function path($concepto_id) {
        $concepto = Helpers::qry("select id, nombre, concepto_id from concepto where id = $concepto_id");
        $parent_id = $concepto['concepto_id'];
        $conceptos = array($concepto['nombre']);
        while ($parent_id) {
            $concepto = Yii::app()->db->createCommand("
                select concepto_id, nombre from concepto where id = $parent_id
              ")->queryRow();
            $parent_id = $concepto["concepto_id"];
            $conceptos[] = $concepto["nombre"];
        }
        return "/" . implode("/", array_reverse($conceptos));
    }

    static public function getPathsForAutoComplete($filtro) {
        $paths = array();
        foreach (Concepto::model()->findAll(array(
                "condition" => "concepto_id is null",
                "order" => "id"
        )) as $concepto) {
            self::getPathRecursive($concepto, $paths);
        }
        $ret = array();
        foreach ($paths as $path) {
            if (!$filtro or strpos(strtoupper($path["path"]), strtoupper($filtro)) !== false) {
                $ret[] = array(
                        "id" => $path["id"],
                        'label' => $path["path"],
                        'value' => $path["path"],
                        'ingreso_egreso' => $path["ingreso_egreso"]);
            }
        };
        return json_encode($ret);
    }

    static public function getJsTree($id = null) {
        $id = $id ? $id : uniqid("treeview");
        $br = "";
        $criteria = array("condition" => "concepto_id is null", "order" => "nombre");

        echo "<ul id=\"$id\">$br";
        foreach (Concepto::model()->findAll($criteria) as $concepto) {
            $criteriaCant = array("condition" => "concepto_id = $concepto->id");
            $cant = Concepto::model()->count($criteriaCant);

            $class = $cant > 1 ? "rel=\"folder\">" : "rel=\"file\"";
            echo "<li id=\"$concepto->id\" cant=\"$cant\" $class>$br";
            echo "<a>$concepto->nombre</a>$br";
            if ($cant > 0) {
                self::getJsTreeRecursive($concepto);
            }
            echo "</li>$br";
        }
        echo "</ul>$br";
    }

    static function getJsTreeRecursive($concepto) {
        $br = "";
        $ul = "<ul>";
        $endul = "</ul>";
        $endli = "</li>";
        $conceptos = Concepto::model()->findAll(array(
                "condition" => "concepto_id = $concepto->id",
                "order" => "nombre"
        ));
        echo "<ul>$br";
        foreach ($conceptos as $concepto) {
            $cant = Concepto::model()->count(array(
                    "condition" => "concepto_id = $concepto->id"
            ));
            $class = $cant > 1 ? "rel=\"folder\">" : "rel=\"file\"";
            echo "<li id=\"$concepto->id\" cant=\"$cant\" $class>$br";
            echo "<a>$concepto->nombre</a>$br";
            if ($cant > 0) {
                echo "$ul$br";
                self::getJsTreeRecursive($concepto);
                echo "$endul$br";
            }
            echo "$endli$br";
        }
        echo "</ul>$br";
    }
    
    static public function treeAndInput($inputName = "doc[concepto_id]", $funcName = null, $value = "", $text = "") {
        //$imgPath = Yii::app()->getBaseUrl() . "/images";
        ?>
        <style  type="text/css">
            .jstree-apple a.jstree-search {
                color: black;
                font-weight: bold;
            }
        </style>

        <div id="treeConceptos-dlg" style="display:none">
            <input type="text" id="treeConceptosSearch" placeholder="Busqueda" onkeyup="treeConceptosSearch($(this));"/>
            <div id ="treeConceptos"><?php echo Concepto::getJsTree("treeConceptos"); ?></div>

        </div>

        <div id="concepto-div" class="row">
            <input id="concepto_ac" placeholder="Concepto"  value="<?php echo $text; ?>"/>
            <input id="concepto-button" tabindex="-1" type="button" value="..." onclick="conceptosTree();"/>
            <input name="<?php echo $inputName; ?>" type="hidden" id="concepto_id"  value="<?php echo $value; ?>"/>
        </div>

        <script type="text/javascript">
                var urlConceptoAC = "<?php echo Yii::app()->createUrl('/fondos/getConceptosPaths'); ?>";
                var url = "<?php echo Yii::app()->createUrl("/concepto"); ?>";
                $(function() {
                    $("#treeConceptosSearch").clearinput(
                            function() {
                                treeConceptosSearch();
                            }
                    );
                    conceptoAC();
                    $("#treeConceptos-dlg").dialog({
                        title: "Conceptos",
                        autoOpen: false,
                        position: [370, 20]
                    });
                    $("#treeConceptos").jstree({
                        opened: ["0"],
                        "plugins": ["search", "types", "themes", "html_data", "ui", "hotkeys", "contextmenu", "dnd", "crrm", "language"],
                        themes: {
                            theme: "apple",
                            dots: true,
                            icons: true
                        },
                        search: {
                            "case_insensitive": true,
                            "show_only_matches": true,
                        },
                        contextmenu: {items: customMenu},
                    }).bind("dblclick.jstree", function(event) {
                        var pos;
                        var node = $(event.target).closest("li");
                        var data = $(node[0]);
                        var dlg_id = "treeConceptos-dlg";
                        var concepto_id = data.attr("id");
                        if (pos = concepto_id.indexOf("_")) {
                            concepto_id = concepto_id.substr(pos + 1);
                        }
                        _concepto_id = concepto_id;
                        treeSelectConcepto(event, data, concepto_id, data.find('a').eq(0).text().trim(), data.find('a').length, dlg_id);
        <?php if ($funcName): ?>
            <?php echo $funcName . "(event, data,data.attr(\"id\"),data.find('a').eq(0).text().trim(),data.find('a').length,id);"; ?>
        <?php endif; ?>
                    }).delegate("a", "dblclick", function(event, data) {
                        event.preventDefault();
                    }).bind("remove.jstree", function(e, data) {
                        if (confirm('Seguro desea borrar este concepto?')) {

                            $.post(url + "/chgNode",
                                    {
                                        "operacion": "remove_node",
                                        "concepto_id": data.rslt.obj.attr("id").replace("node_", "")
                                    }, function(r) {
                                if (!r.errors) {
                                    $(data.rslt.obj).attr("id", "node_" + r.id);
                                }
                                else {
                                    $.jstree.rollback(data.rlbk);
                                    alert(r.errors);
                                }
                            }, "json"
                                    );
                        }
                    }).bind("open_node.jstree", function(event, data) {
                        if ((data.inst._get_parent(data.rslt.obj)).length) {
                            data.inst.open_node(data.inst._get_parent(data.rslt.obj), false, true);
                        }
                    }).bind("create.jstree", function(e, data) {
                        $.post(url + "/chgNode",
                                {
                                    "operacion": "create_node",
                                    "destino_id": data.rslt.parent.attr("id").replace("node_", ""),
                                    "orden": data.rslt.position,
                                    "nombre": data.rslt.name,
                                    "tipo": data.rslt.obj.attr("rel")
                                }, function(r) {
                            if (!r.errors) {
                                $(data.rslt.obj).attr("id", "node_" + r.id);
                            }
                            else {
                                $.jstree.rollback(data.rlbk);
                                alert($r.errors);
                            }
                        }, "json"
                                );
                    }).bind("rename.jstree", function(e, data) {
                        $.post(url + "/chgNode",
                                {
                                    "operacion": "rename_node",
                                    "origen_id": data.rslt.obj.attr("id").replace("node_", ""),
                                    "nombre": data.rslt.new_name
                                }, function(r) {
                            if (r != "ok") {
                                $.jstree.rollback(data.rlbk);
                            }
                        }
                        );
                    }).bind("move_node.jstree", function(e, data) {
                        data.rslt.o.each(function(i) {
                            $.ajax({
                                async: false,
                                type: 'POST',
                                url: url + "/chgNode",
                                data: {
                                    "operacion": "move_node",
                                    "origen_id": $(this).attr("id").replace("node_", ""),
                                    "destino_id": data.rslt.cr === -1 ? -1 : data.rslt.np.attr("id").replace("node_", ""),
                                    "orden": data.rslt.cp + i,
                                    "nombre": data.rslt.name,
                                    "copy": data.rslt.cy ? 1 : 0
                                },
                                success: function(r) {
                                    if (!r == "ok") {
                                        $.jstree.rollback(data.rlbk);
                                    }
                                    else {
                                        $(data.rslt.oc).attr("id", "node_" + r.id);
                                        if (data.rslt.cy && $(data.rslt.oc).children("UL").length) {
                                            data.inst.refresh(data.inst._get_parent(data.rslt.oc));
                                        }
                                    }
                                    $("#analyze").click();
                                }
                            });
                        });
                    }).bind("select_node.jstree", function(e, data) {
                        $("#treeConceptos").jstree("open_node", data.rslt.obj);
                    });
                    $("#treeConceptos").jstree("set_lang", "es");
                });

                function customMenu(node) {
                    // The default set of all items
                    var items = {renameItem: {
                            label: "Renombrar",
                            action: function(obj) {
                                this.rename(obj);
                            }
                        },
                        newItem: {
                            label: "Nuevo",
                            action: function(obj) {
                                this.create(obj);
                            }
                        }, deleteItem: {
                            label: "Borrar",
                            action: function(obj) {
                                this.remove(obj);
                            }
                        }
                    };
                    if ($(node).hasClass("folder")) {
                        // Delete the "delete" menu item
                        delete items.deleteItem;
                    }
                    return items;
                }

                function conceptosTree() {
                    var dlgId = "treeConceptos-dlg";
                    var treeId = "treeConceptos";
                    $("#" + dlgId).dialog("open");
                    if (typeof _concepto_id === "string") {
                        $("#" + treeId).jstree("select_node", $("#" + _concepto_id));
                    } else {
                        $("#" + treeId).jstree("open_node", $("#0"));
                    }
                }

                function treeSelectConcepto(event, data, id, text, cant, dialogId) {
                    $("#" + dialogId).dialog("close");
                    $("#concepto_id").val(id);
                    $("#concepto_ac").val(text);
                    _concepto_id = id;
                }

                function conceptoAC() {
                    $('#concepto_ac')
                            .autocomplete({
                        autoSelect: true,
                        autoFocus: true,
                        minLength: 0,
                        source: urlConceptoAC,
                        select: function(event, ui) {
                            concepto_id = ui.item.id;
                            $("#concepto-span").html(ui.item.label);
                            $("#concepto_id").val(concepto_id);
                            $("#letra").focus();
                        }
                    });
                }

                function treeConceptosSearch($obj) {
                    var sh = $obj ? $obj.val() : "";
                    $("#treeConceptos").jstree("search", sh);
                }
        </script>    

        <?php
    }

    static public function ac($concepto_id_id) {
        ?>


        <?php
    }

}