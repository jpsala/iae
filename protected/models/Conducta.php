<?php

Yii::import('application.models._base.BaseConducta');

class Conducta extends BaseConducta {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public static function getConductaParaCarga($division_id) {
        $ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();

    $alumno_divisiones = Helpers::qryAll("
      select ad.id, concat(alu.apellido,', ', alu.nombre) as alumno_nombre
        from alumno_division ad
          inner join alumno alu on alu.id = ad.alumno_id
          inner join alumno_estado ae on ae.id = alu.estado_id and ! ae.ingresante
        where ad.Division_id = $division_id 
			  and ad.Ciclo_id = $ciclo_id 
			  and (ad.activo or ad.promocionado or ad.egresado)
              and alu.activo = 1
        order by alu.sexo desc, alu.apellido, alu.nombre
    ");
    foreach ($alumno_divisiones as $ad) {
      $alumno_division_id = $ad['id'];
      $alumno_nombre = $ad["alumno_nombre"];
      $rows = Helpers::qryAll("
        select ad.alumno_id as alumno_id, ad.id, lp.nombre as periodo_nombre, li.abrev, li.nombre_unico, 
               c.id as conducta_id, c.conducta, lp.id as logica_periodo_id
          from alumno_division ad
            inner join division d on ad.division_id = d.id
            inner join anio a on a.id = d.anio_id
            inner join nivel n on n.id = a.nivel_id
            inner join asignatura_tipo at  on at.Nivel_id = n.id and at.tipo = 1
            inner join logica_ciclo lc on lc.Asignatura_Tipo_id = at.id and lc.Ciclo_id = $ciclo_id
            inner join logica_periodo lp on lp.logica_id = lc.Logica_id
            inner join logica_item li on li.logica_periodo_id = lp.id
              and li.tiene_conducta = 1
            left join conducta c on c.alumno_division_id = ad.id
              and c.logica_periodo_id = lp.id
        where ad.id = $alumno_division_id and ad.ciclo_id = $ciclo_id and (ad.activo or ad.promocionado or ad.egresado)
        order by lp.orden, li.orden");
      foreach ($rows as $row) {
        $ret[$alumno_nombre][$row["nombre_unico"]] = $row;
      }
    }
    return $ret;
  }

}