<?php

Yii::import('application.models._base.BaseDestino');

class Destino extends BaseDestino {

    const TIPO_CAJA = 1;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function rules() {
        return array(
                array('nombre,Destino_Tipo_id', 'required'),
                array('Destino_Tipo_id, Banco_id', 'length', 'max' => 10),
                array('nombre', 'length', 'max' => 45),
                array('nombre, Banco_id', 'default', 'setOnEmpty' => true, 'value' => null),
                array('id, Destino_Tipo_id, nombre, Banco_id', 'safe', 'on' => 'search'),
        );
    }

    public function __set($name, $value) {
        if (($name == 'Destino_Tipo_id') and !is_numeric($value)) {
            $obj = new DestinoTipo();
            $obj->nombre = $value;
            $obj->save();
            $value = $obj->id;
        }
        parent::__set($name, $value);
    }

    public function attributeLabels() {
        return array(
                'id' => Yii::t('app', 'ID'),
                'Destino_Tipo_id' => "Tipo de destino",
                'nombre' => Yii::t('app', 'Nombre'),
                'Banco_id' => null,
                'destinoTipo' => null,
                'banco' => null,
                'destinoInstancias' => null,
                'docValors' => null,
        );
    }

    static public function getCajaUsuarioId() {
        $user_id = Yii::app()->user->id;
        return Yii::app()->db->createCommand(
                        "SELECT pt.destino_id FROM user u
            inner join puesto_trabajo pt on pt.id = u.puesto_trabajo_id
            where u.id = $user_id
        ")->queryScalar();
    }

    static public function getCajaUsuario() {
        $user_id = Yii::app()->user->id;
        return Destino::model()->findBySql(
                        "SELECT pt.destino_id FROM user u
            inner join puesto_trabajo pt on pt.id = u.puesto_trabajo_id
            where u.id = $user_id
        ");
    }

    static public function getCajaInstanciaActiva() {
        $user_id = Yii::app()->user->id;
        return DestinoInstancia::model()->findBySql(
                        "SELECT di.* FROM user u
            inner join puesto_trabajo pt on pt.id = u.puesto_trabajo_id
            inner join destino_instancia di on di.Destino_id = pt.destino_id
            where u.id = $user_id and fecha_cierre is null
        ");
    }

    static public function getCajaInstanciaActivaId() {
        $user_id = Yii::app()->user->id;
        return Yii::app()->db->createCommand(
                        "SELECT di.id FROM user u
            inner join puesto_trabajo pt on pt.id = u.puesto_trabajo_id
            inner join destino_instancia di on di.Destino_id = pt.destino_id
            where u.id = $user_id and fecha_cierre is null
        ")->queryScalar();
    }

    static public function getCajas($soloAbiertas = true) {
        $and = $soloAbiertas ? " and di.fecha_cierre is null " : "";
        $user_id = Yii::app()->user->id;
        $tipoCaja = Destino::TIPO_CAJA;
        $puesto_trabajo_id = Helpers::qryScalar("
            select puesto_trabajo_id from user u where u.id = $user_id
        ");
        if ($puesto_trabajo_id == null) {
           Helpers::error("El usuario activo, $user_id, no tiene asignado un puesto de trabajo en \"user\"");
           die;
        }
        $select = "select distinct d.*
            from puesto_trabajo pt
                inner join user u on u.puesto_trabajo_id = pt.id
                inner join destino d on d.id = pt.destino_id
                inner join destino_instancia di on di.Destino_id = d.id
            where u.id = $user_id and d.tipo = $tipoCaja $and";
        $destinos = Destino::model()->findAllBySql($select);
        if (count($destinos) < 1) {
            Helpers::error("El usuario activo, $user_id, no tiene talonarios asociados en \"puesto_trabajo\" para comprob_id = $tipoCaja");
        }
        return $destinos;
    }

    static public function abre() {
        $cajaAbierta = Destino::getCajaInstanciaActivaId();
        if ($cajaAbierta) {
            throw new Exception("Ya hay una caja abierta");
        } else {
            $cajaAbierta = new DestinoInstancia();
            $cajaAbierta->Destino_id = Destino::getCajaUsuarioId();
            $cajaAbierta->fecha_apertura = new CDbExpression("now()");
            $cajaAbierta->save();
        }
    }

    static public function cierra() {
        $cajaAbierta = Destino::getCajaInstanciaActiva();
        if (!$cajaAbierta) {
            throw new Exception("La caja no está abierta");
        }
        $cajaAbierta->fecha_cierre = new CDbExpression("now()");
        $cajaAbierta->save();
    }

    public function getInstancias($soloAbierta = true) {
        $and = $soloAbierta ? " and fecha_cierre is null " : "";
        $select = "select *
      from destino_instancia di
      where di.Destino_id = $this->id $and";
        return DestinoInstancia::model()->findBySql($select);
    }

    public function getNombreMasCuenta() {
        return $this->nombre . " - " . $this->numero_cuenta;
    }

    static public function getActivo() {
        $user_id = Yii::app()->user->id;
        if (!$user_id) {
            return;
        }
        return Yii::app()->db->createCommand("
      select  i.id
        from user u 
      		inner join puesto_trabajo p on p.id = u.puesto_trabajo_id
      		inner join destino_instancia i on i.Destino_id = p.destino_id
        where u.id = $user_id and i.fecha_cierre is null
      ")->queryScalar();
    }

    public static function listData($tipo = null) {
        $where = $tipo ? " where tipo = $tipo" : "";
        $destinos = Helpers::qryAll("select id, nombre from destino $where order by id, nombre");
        $ret = array();
        foreach ($destinos as $destino) {
            $ret[$destino["id"]] = $destino["nombre"];
        }
        return $ret;
    }

}