<?php

Yii::import('application.models._base.BaseDivision');

class Division extends BaseDivision
{

  public static function model($className = __CLASS__)
  {
    return parent::model($className);
  }

  public function beforeSave()
  {
    if ($this->isNewRecord) {
      foreach ($this->anio->asignaturas as $asignatura) {
        $da = new DivisionAsignatura();
        $da->Division_id = $this->id;
        $da->Asignatura_id = $asignatura->id;
        $da->save();
      }
    }
    if (!$this->orden and $this->isNewRecord) {
      $rec = Yii::app()->db->createCommand("select max(orden) as orden from division where Anio_id = $this->Anio_id")->queryRow();
      $this->orden = $rec["orden"] + 10;
    }

    return parent::beforeSave();
  }

  /**
   * Devuelve las notas de toda la division
   * @example
   * <code>
   * getNotas()[0] trae las notas de todos los alumnos de la primer asignatura
   * getNotas()[0][0] trae las notas del primer alumno de la primer asignatura
   * getNotas()[0][0]['Final'] Trae la Nota[] final del primer alumno de la primer asignatura
   * </code>
   * @return type Array[] cada elemento contiene un array que contiene las notas de todos los alumnos de una asignatura
   *
   */
  public function getNotas()
  {
    /* @var $da DivisionAsignatura */
    $notas = array();
    foreach (DivisionAsignatura::model()
              ->with("division")
              ->findAll(array(
               "condition" => "division.anio_id = $this->Anio_id",
               "order" => "t.id"
                /*			   * @todo aca falta un orden supongo, igual que en asignatura  */
              )) as $da) {
      $notas["asignatura"] = $da->asignatura;
      $notas["notas"] = $da->getNotas();
    }

    return $notas;
  }

  public static function getNotasPorPeriodo($division_id, $logica_periodo_id = null, $esBoletin = false)
  {
    $whereBoletin = $esBoletin ? "imprime_en_boletin = 1" : "true";
    $whereLogicaPeriodo = $logica_periodo_id ? "  id = $logica_periodo_id " : " true ";
    $notas = array();
    $nivel_id = Helpers::qryScalar("
            select a.nivel_id
            from anio a
                inner join division d on a.id = d.`Anio_id` and d.id = $division_id
            limit 1
            ");

    $logica_id = Logica::getLogicaIdPorNivel($nivel_id);
    $ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
    $alumno_divisiones = Helpers::qryAll("
            select ad.id as alumno_division_id, a.matricula, ad.alumno_id, ciclo.nombre as ciclo,
                   concat(a.apellido,', ',a.nombre) as alumno_nombre, n.conducta_detallada,
                   n.inasistencia_detallada, an.nombre as anio, d.nombre as division, n.nombre as nivel
              from alumno_division ad
                inner join alumno a on a.id = ad.alumno_id
                inner join division d on d.id = ad.division_id
                inner join anio an on an.id = d.anio_id
                inner join nivel n on n.id = an.nivel_id
                inner join ciclo on ciclo.id = $ciclo_id
              where ad.Division_id = $division_id and
                    ad.Ciclo_id = $ciclo_id and a.matricula = 3930 and
                    a.activo = 1 /* and (a.apellido like \"%Slavin%\")*/
              order by a.sexo desc, a.apellido, a.nombre
              /*limit 20*/
          ");
    foreach ($alumno_divisiones as $ad) {
      $alumno_division_id = $ad["alumno_division_id"];
      $notas[$ad["alumno_nombre"]]["datos"] = Helpers::qry("
                select n.nombre as nivel, an.nombre as anio, d.nombre as division, a.matricula,
                            concat(asi.nombre, \" \", ns.nombre) as siguiente_division
                    from alumno_division ad
                        inner join alumno a on a.id = ad.alumno_id
                        inner join division d on d.id=ad.division_id
                        left join division ds on ds.id = d.division_id_siguiente
                        inner join anio an on an.id = d.anio_id
                        left join anio asi on asi.id = ds.Anio_id
                        inner join nivel n on n.id = an.nivel_id
                        left join nivel ns on ns.id = asi.Nivel_id
                    where ad.id = $alumno_division_id
            ");
    }
    $division_asignaturas = Helpers::qryAll("
            select da.id as division_asignatura_id, a.id as asignatura_id,
                   a.nombre as asignatura_nombre, da.boletin
              from division_asignatura da
                inner join asignatura a on a.id = da.asignatura_id
              where da.Division_id = $division_id /*and a.nombre = \"Literatura\"*/ and da.boletin = 1
              order by case when da.orden = 0 or da.orden is null then a.orden else da.orden end
          ");
    //        vd($division_asignaturas);
    $logicaPeriodos = Helpers::qryAll("
            select lp.id, lp.abrev, lp.imprime_en_boletin, lp.fecha_inicio,lp.fecha_inicio_ci, lp.fecha_fin, lp.fecha_fin_ci
              from logica_periodo lp
              where lp.`Logica_id` = $logica_id and $whereLogicaPeriodo and  $whereBoletin
              order by lp.orden
          ");
    foreach ($logicaPeriodos as $key => $lp) {
      $id = $lp["id"];
      $logica_items = Helpers::qryAll("
                select li.*
                  from logica_item li
                  where li.Logica_Periodo_id = $id and $whereBoletin
                  order by li.orden
              ");
      foreach ($alumno_divisiones as $ad) {
        $alumno_division_id = $ad["alumno_division_id"];
        $cant = 0;
        $ap = 0;
        $suma = 0;
        foreach ($division_asignaturas as $division_asignatura) {
          $cant++;
          $asignatura_id = $division_asignatura["asignatura_id"];
          $notasTran = LogicaActiva::getNotasAlumnoDivisionAsignatura($alumno_division_id, $asignatura_id);
          $ap += ($notasTran["CD"]["nota"]) ? 1 : 0;
          $suma += $notasTran["CD"]["nota"];
          //$notasParaFormula = array();
          $nota = "";
          foreach ($logica_items as $li) {
            $logica_item_id = $li["id"];
            if (isset($notasTran[$li["nombre_unico"]])) {
              $nota = $notasTran[$li["nombre_unico"]]["nota"];
            } else {
              $nota = "";
            }
            $notas[$ad["alumno_nombre"]]["notas"][$division_asignatura["asignatura_nombre"]][$li["nombre_unico"]] = $nota;
          }
        }
        if ($cant == $ap) {
          $notas[$ad["alumno_nombre"]]["desempenio"] = number_format(round($suma / $cant, 2), 2);
          if ($notas[$ad["alumno_nombre"]]["desempenio"] < 7) {
            $notas[$ad["alumno_nombre"]]["desempenio"] = 0;
          }
        } else {
          $notas[$ad["alumno_nombre"]]["desempenio"] = "";
        }
        //                $logica_items = Helpers::qryAll("
        //                        select li.*
        //                          from logica_item li
        //                          where li.Logica_Periodo_id = $id
        //                          order by li.orden
        //                      ");
        foreach ($logica_items as $li) {
          $logica_item_id = $li["id"];
          if ($li["tiene_conducta"]) {
            if ($ad["conducta_detallada"] == "0") {
              $amonestaciones = Helpers::qryScalar("
									select c.conducta as sanciones
									  from conducta c
									  where c.Alumno_Division_id = $alumno_division_id and
											c.logica_periodo_id = $id
								");
            } else {
              $amonestaciones = Helpers::qryScalar("
									select sum(d.cantidad) as sanciones
									  from conducta c
										inner join conducta_detalle d on d.conducta_id = c.id
									  where c.Alumno_Division_id = $alumno_division_id and
											c.logica_periodo_id = $id
								");
            }
            $notas[$ad["alumno_nombre"]]["amonestaciones"][$li["nombre_unico"]] = $amonestaciones ? $amonestaciones : 0;
            if (!isset($notas[$ad["alumno_nombre"]]["amonestaciones-total"])) {
              $notas[$ad["alumno_nombre"]]["amonestaciones-total"] = 0;
            }
            $notas[$ad["alumno_nombre"]]["amonestaciones-total"] += $amonestaciones;
          }
        }

        foreach ($logica_items as $li) {
          $logica_item_id = $li["id"];
          if ($li["tiene_conducta"]) {
            if ($ad["inasistencia_detallada"] == "0") {
              $inasistencias = Helpers::qryScalar("
                                select cantidad inasistencias
                                  from inasistencia i
                                  where i.Alumno_Division_id = $alumno_division_id and
                                        i.logica_periodo_id = $id
                            ");
            } else {
              $fecha_inicio = str_replace("00:00:00", "00:00:00", $lp["fecha_inicio_ci"]);
              $fecha_fin = $lp["fecha_fin_ci"];
              $inasistencias = Helpers::qryScalar("
				select sum(t.valor) inasistencias
				  from inasistencia i
					inner join inasistencia_detalle d on d.inasistencia_id = i.id
					inner join inasistencia_tipo t on t.id = d.inasistencia_tipo_id
				  where i.Alumno_Division_id = $alumno_division_id and
						d.fecha between \"$fecha_inicio\" and \"$fecha_fin\"
			  ");
            }
            $notas[$ad["alumno_nombre"]]["inasistencias"][$li["nombre_unico"]] = $inasistencias > 0 ? $inasistencias : 0;
            if (!isset($notas[$ad["alumno_nombre"]]["inasistencias-total"])) {
              $notas[$ad["alumno_nombre"]]["inasistencias-total"] = 0;
            }
            $notas[$ad["alumno_nombre"]]["inasistencias"][$li["nombre_unico"]] = $inasistencias ? $inasistencias : 0;
            $notas[$ad["alumno_nombre"]]["inasistencias-total"] += $inasistencias;
          }
        }
      }
    }

    //        vd($notas);
    return $notas;
  }

  /*
   *
   * Para planillaresumenfinal
   *
   */

  public static function getNotasPorPeriodo2($division_id, $logica_periodo_id = null, $asignatura_id = null, $conItems = false, $esPlanilla = false)
  {
    $asignatura_id = null;
    $notas = array();
    $ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
    $todasLasAsignaturas = $asignatura_id ? false : true;
    $todosLosPeriodos = $logica_periodo_id ? false : true;
    $wherePlanilla = $esPlanilla ? " and li.imprime_en_planilla = 1 " : "";
    $notas["datos"] = Helpers::qry("
					select ciclo.nombre as ciclo, an.nombre as anio, d.nombre as division, n.nombre as nivel
							from alumno_division ad
								inner join division d on d.id = ad.division_id
								inner join anio an on an.id = d.anio_id
								inner join nivel n on n.id = an.nivel_id
								inner join ciclo on ciclo.id = $ciclo_id
							where ad.Division_id = $division_id and
										ad.Ciclo_id = $ciclo_id and not ad.borrado
										/*and ad.activo*/
										/*((ad.activo or ad.promocionado or ad.egresado)*/
                                 and not ad.borrado = 1
						limit 1
			");
    $qry = "
				select ad.id as alumno_division_id, a.matricula, ad.alumno_id, ciclo.nombre as ciclo,
							 concat(a.apellido,', ',a.nombre) as alumno_nombre,  an.nombre as anio,
								d.nombre as division, n.nombre as nivel, n.conducta_detallada, n.inasistencia_detallada
					from alumno_division ad
						inner join alumno a on a.id = ad.alumno_id
						inner join alumno_estado ae on ae.id = a.estado_id and !ae.ingresante
						inner join division d on d.id = ad.division_id
						inner join anio an on an.id = d.anio_id
						inner join nivel n on n.id = an.nivel_id
						inner join ciclo on ciclo.id = $ciclo_id
                        inner join alumno_division_estado e ON e.id = ad.alumno_division_estado_id AND e.muestra_edu
					where ad.Division_id = $division_id and
								ad.Ciclo_id = $ciclo_id /*and ad.activo */
						 		and a.activo = 1 and not ad.borrado = 1 /*and ad.activo /*((ad.activo or ad.promocionado or ad.egresado)*/ /*and a.apellido = 'Giuria'*/
					order by case when n.id in (2,3) then a.sexo end desc, a.apellido, a.nombre
			";
    $alumno_divisiones = Helpers::qryAll($qry);
    //vd($qry);
//vd($alumno_divisiones);
    $whereAsignaturas = $asignatura_id ? " and da.asignatura_id = $asignatura_id " : "";
    $division_asignaturas_qry = "
							select da.id as division_asignatura_id, a.id as asignatura_id,
										 a.nombre as asignatura_nombre
								from division_asignatura da
									inner join asignatura a on a.id = da.asignatura_id
								where da.Division_id = $division_id $whereAsignaturas /*and da.id = 1012*/
								order by case when da.orden = 0 or da.orden is null then a.orden else da.orden end
						";
    $division_asignaturas = Helpers::qryAll($division_asignaturas_qry);

    //vd($division_asignaturas_qry);
    foreach ($division_asignaturas as $division_asignatura) {
      $notas["asignaturas"][] = $division_asignatura["asignatura_nombre"];
      $notas["asignaturas-desaprobadas"][$division_asignatura["asignatura_nombre"]] = 0;
    }


    $whereLogicaPeriodo = $logica_periodo_id ? " where lp.id = $logica_periodo_id " : "";
    $logica_periodos = Helpers::qryAll("
      select lp.id, lp.abrev, lp.nombre, lp.fecha_inicio_ci, lp.fecha_fin_ci, lp.es_final
       from logica_periodo lp
       $whereLogicaPeriodo
       order by lp.orden
     ");

    foreach ($alumno_divisiones as $ad) {
      $alumno_division_id = $ad["alumno_division_id"];
      $sumaAsignaturasDesaprobadas = 0;
      foreach ($division_asignaturas as $division_asignatura) {
        //$notasParaFormula = array();
        $asignatura_id = $division_asignatura["asignatura_id"];
        $notasTran = LogicaActiva::getNotasAlumnoDivisionAsignatura($alumno_division_id, $asignatura_id);
//        vd2($notasTran);
        foreach ($logica_periodos as $lp) {
          $logica_periodo_id = $lp["id"];
          $logica_items = Helpers::qryAll("
             select li.*
              from logica_item li
              where li.logica_periodo_id = $logica_periodo_id $wherePlanilla
              order by li.orden
							   ");
          $asignatura_id = $division_asignatura["asignatura_id"];
          foreach ($logica_items as $li) {
            $logica_item_id = $li["id"];
            //if ($li["manual"]) {
            $nota = Helpers::qryScalar("
              select n.nota
               from nota n
               where n.Alumno_Division_id = $alumno_division_id and
                  n.Asignatura_id = $asignatura_id and
                  n.Logica_item_id = $logica_item_id
              ");
            $nota = $nota ? $nota : 0;
            //} else {
            //                            $nota = self::formula($li["formula"], $notasParaFormula);
            //                     }
            //                  $notasParaFormula[$li["nombre_unico"]] = $nota;
            if ($conItems) {
              if ($todasLasAsignaturas) {
                $notas["alumnos"][$ad["alumno_nombre"]]["notas"][$li["nombre_unico"]][$asignatura_id] = $nota;
              } else {
                $notas["alumnos"][$ad["alumno_nombre"]]["notas"][$li["nombre_unico"]]["nota"] = $nota;
                $notas["alumnos"][$ad["alumno_nombre"]]["notas"][$li["nombre_unico"]]["nombre"] = $li["nombre"];
                $notas["alumnos"][$ad["alumno_nombre"]]["notas"][$li["nombre_unico"]]["abrev"] = $li["abrev"];
              }
            }
          }
          if (!isset($nota)) {
            //continue;
          }
          $nota = $notasTran[$li["nombre_unico"]]["nota"] ? $notasTran[$li["nombre_unico"]]["nota"] : 0;
          $nota = $nota === 0 ? '' : $nota;
          if (!$conItems) {
            if ($todosLosPeriodos) {
              if ($todasLasAsignaturas) {
                $notas["alumnos"][$ad["alumno_nombre"]]["notas"][$li["id"]][$asignatura_id] = $nota;
              } else {
                $notas["alumnos"][$ad["alumno_nombre"]]["notas"][$li["id"]] = $nota;
              }
            } else {
              if ($todasLasAsignaturas) {
                $notas["alumnos"][$ad["alumno_nombre"]]["notas"][$asignatura_id] = $nota;
              } else {
                $notas["alumnos"][$ad["alumno_nombre"]]["notas"] = $nota;
              }
            }
          }
          $nota = isset($nota) ? $nota : 0;
          if (is_numeric($nota)) {
            if (in_array($lp["nombre"], array("Diciembre", "Marzo"))) {
              $sumaAsignaturasDesaprobadas += ($nota < 4) ? 1 : 0;
            } else {
              $sumaAsignaturasDesaprobadas += ($nota < 7) ? 1 : 0;
            }
            if (in_array($lp["nombre"], array("Diciembre", "Marzo"))) {
              $notas["asignaturas-desaprobadas"][$division_asignatura["asignatura_nombre"]]
               += (($nota < 4) ? 1 : 0);
            } else {
              $notas["asignaturas-desaprobadas"][$division_asignatura["asignatura_nombre"]]
               += (($nota < 7) ? 1 : 0);
            }
          } else {
            $notas["asignaturas-desaprobadas"][$division_asignatura["asignatura_nombre"]]
             += ((strtoupper($nota) == "ANS") ? 1 : 0);
            $sumaAsignaturasDesaprobadas += ((strtoupper($nota) == "ANS") ? 1 : 0);
          }
        }
        $notas[$ad["alumno_nombre"]]["sumaAsignaturasDesaprobadas"] = $sumaAsignaturasDesaprobadas;

        if ($logica_periodo_id) {
          if ($ad["conducta_detallada"] == "0") {
            $amonestaciones = Helpers::qryScalar("
															select c.conducta as sanciones
																from conducta c
																where c.Alumno_Division_id = $alumno_division_id and
																			c.logica_periodo_id = $logica_periodo_id
													");
          } else {
            $amonestaciones = Helpers::qryScalar("
															select sum(d.cantidad) as sanciones
																from conducta c
																	inner join conducta_detalle d on d.conducta_id = c.id
																where c.Alumno_Division_id = $alumno_division_id /*and
																			c.logica_periodo_id = $logica_periodo_id            */
													");
          }
          if ($ad["inasistencia_detallada"] == "0") {
            //@DANGER
//            Cuidado!! lo cambié por el de abajo porque aparecían todas las inasistencias
//            $andPeriodo = $lp['es_final'] ? "" : "and i.logica_periodo_id = $logica_periodo_id";
            $andPeriodo = "and i.logica_periodo_id = $logica_periodo_id";
            $inasistencias = Helpers::qryScalar("
                        select sum(cantidad) inasistencias
                           from inasistencia i
                        where i.Alumno_Division_id = $alumno_division_id $andPeriodo
                              ");
          } else {
            $fecha_desde = $lp["fecha_inicio_ci"];
            $fecha_hasta = $lp["fecha_fin_ci"];
            $s = "
                              select sum(t.valor) inasistencias
                                 from inasistencia i
                                    inner join inasistencia_detalle d on d.inasistencia_id = i.id
                                    inner join inasistencia_tipo t on t.id = d.inasistencia_tipo_id
                                 where i.Alumno_Division_id = $alumno_division_id /*and
                                          d.fecha between \"$fecha_desde\" and \"$fecha_hasta\"*/
                                    ";
            $inasistencias = Helpers::qryScalar($s);
          }
          $notas[$ad["alumno_nombre"]]["amonestaciones"] = $amonestaciones;
          $notas[$ad["alumno_nombre"]]["inasistencias"] = $inasistencias;
        }
      }
    }

    // vd2($notas);
    return $notas;
  }

  public static function getNotasPlanillaNotas($division_id, $asignatura_id, $soloAprobados = false)
  {
    $esCicloActual = Ciclo::esActual();
    $andActivo = $esCicloActual ? " and ad.activo  " : " ";
    $nivel_id = Helpers::qryScalar("
				select a.nivel_id
				from anio a
						inner join division d on a.id = d.`Anio_id` and d.id = $division_id
				limit 1
				");
    $logica_id = Logica::getLogicaIdPorNivel($nivel_id);
    $ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
    $notas["datos"] = Helpers::qry("
				select ciclo.nombre as ciclo, an.nombre as anio, d.nombre as division, n.nombre as nivel
						from alumno_division ad
							inner join division d on d.id = ad.division_id
							inner join anio an on an.id = d.anio_id
							inner join nivel n on n.id = an.nivel_id
							inner join ciclo on ciclo.id = $ciclo_id
						where ad.Division_id = $division_id $andActivo /*((ad.activo or ad.promocionado or ad.egresado)*/
									and ad.Ciclo_id = $ciclo_id
					limit 1
		");
    $alumno_divisiones = Helpers::qryAll("
				select ad.id as alumno_division_id, a.matricula, ad.alumno_id, ciclo.nombre as ciclo, d.id as division_id,
							 concat(a.apellido,', ',a.nombre) as alumno_nombre,  an.nombre as anio,
								d.nombre as division, n.nombre as nivel, n.conducta_detallada, n.inasistencia_detallada
					from alumno_division ad
						inner join alumno a on a.id = ad.alumno_id
						inner join division d on d.id = ad.division_id
						inner join anio an on an.id = d.anio_id
						inner join nivel n on n.id = an.nivel_id
						inner join ciclo on ciclo.id = $ciclo_id
					where ad.Division_id = $division_id and
								ad.Ciclo_id = $ciclo_id and
								a.activo = 1 $andActivo /*((ad.activo or ad.promocionado or ad.egresado)*/
					order by /*a.sexo desc,*/ a.apellido, a.nombre
			");

    $logica_periodos = Helpers::qryAll("
				select lp.id, lp.abrev, lp.nombre
					from logica_periodo lp
					where logica_id = $logica_id
					order by lp.orden
			");

    $notas["statsLeyendas"][] = "A. Aprobados entre 10 y 7";
    $notas["statsLeyendas"][] = "B. Desaprobados entre 6 y 4";
    $notas["statsLeyendas"][] = "C. Aplazados entre 3 y 1";
    $notas["statsLeyendas"][] = "D. No calificados";
    foreach ($logica_periodos as $lp) {
      $logica_periodo_id = $lp["id"];
      $notas["stats"]["10y7"][$lp["abrev"]]["cant"] = 0;
      $notas["stats"]["6y4"][$lp["abrev"]]["cant"] = 0;
      $notas["stats"]["3y1"][$lp["abrev"]]["cant"] = 0;
      $notas["stats"]["noCalif"][$lp["abrev"]]["cant"] = 0;
      $notas["stats"]["10y7"][$lp["abrev"]]["porcentaje"] = 0;
      $notas["stats"]["6y4"][$lp["abrev"]]["porcentaje"] = 0;
      $notas["stats"]["3y1"][$lp["abrev"]]["porcentaje"] = 0;
      $notas["stats"]["noCalif"][$lp["abrev"]]["porcentaje"] = 0;
      $logica_items = Helpers::qryAll("
                        select li.*
                          from logica_item li
                          where li.logica_periodo_id = $logica_periodo_id and imprime_en_planilla = 1
                          order by li.orden
                        ");
      foreach ($logica_items as $li) {
        $notas["items"][$li["id"]] = $li["nombre"];
      }
      $lp["abrev"]++;
    }
    //vd($notas);
    $alumnosCant = 0;
    NotasAdmin::init($division_id);
    foreach ($alumno_divisiones as $ad) {
      $alumnosCant++;
      $alumno_division_id = $ad["alumno_division_id"];
      $notasTran = LogicaActiva::getNotasAlumnoDivisionAsignatura($alumno_division_id, $asignatura_id);
//	  vd2($notasTran['integradora']);
      //dd($notasTran);
      $n = NotasAdmin::getNotasAlumnoDivision($alumno_division_id);
      //            vd($n["final"]["aprobado"]);
      //$ap = Formulas::getNotaFinal($nivel_id, $alumno_division_id, $n["notasItems"]);
      //vd($ap);
      $lp["abrev"] = 0;
      foreach ($logica_periodos as $lp) {
        $logica_periodo_id = $lp["id"];
        $logica_items = Helpers::qryAll("
						select li.*
							from logica_item li
							where li.logica_periodo_id = $logica_periodo_id
							order by li.orden
						");
        $notaLp[$logica_periodo_id] = 0;
        foreach ($logica_items as $li) {
          if (isset($notasTran[$li["nombre_unico"]])) {
            $nota = $notasTran[$li["nombre_unico"]]; /* ["nota"]; */
            $notas["alumnos"][$ad["alumno_nombre"]]["notas"][$li["id"]] = $nota;
          }
        }
        $notaLp[$lp["abrev"]] = $nota["nota"];
        $lp["abrev"]++;
      }
      foreach ($logica_periodos as $lp) {
        $logica_periodo_id = $lp["id"];
        $nota = round($notaLp[$lp["abrev"]], 0);
        if (is_numeric($nota)) {
          if ($nota <= 10 and $nota >= 7) {
            $notas["stats"]["10y7"][$lp["abrev"]]["cant"]++;
          } elseif ($nota <= 6 and $nota >= 4) {
            $notas["stats"]["6y4"][$lp["abrev"]]["cant"]++;
          } elseif ($nota <= 3 and $nota >= 1) {
            $notas["stats"]["3y1"][$lp["abrev"]]["cant"]++;
            //							 $notas["stats"]["3y1"][$lp["abrev"]]["cant"] * 100 / $alumnosCant;
            //                        vd($notas["stats"]["3y1"][$lp["abrev"]], $nota,$lp["abrev"], $ad["alumno_nombre"]);
          } elseif ($nota) {
            $notas["stats"]["noCalif"][$lp["abrev"]]["cant"]++;
          }
        }
      }
    }
    foreach ($logica_periodos as $lp) {
      $logica_periodo_id = $lp["id"];
      $notas["stats"]["10y7"][$lp["abrev"]]["porcentaje"] = $notas["stats"]["10y7"][$lp["abrev"]]["cant"] * 100 / ($alumnosCant > 0 ? $alumnosCant : 1);
      $notas["stats"]["10y7"][$lp["abrev"]]["cant"] = $notas["stats"]["10y7"][$lp["abrev"]]["cant"] == 0 ? "" :
       number_format($notas["stats"]["10y7"][$lp["abrev"]]["cant"], 2);
      $notas["stats"]["10y7"][$lp["abrev"]]["porcentaje"] = $notas["stats"]["10y7"][$lp["abrev"]]["porcentaje"] == 0 ? "" :
       number_format($notas["stats"]["10y7"][$lp["abrev"]]["porcentaje"], 2);

      $notas["stats"]["6y4"][$lp["abrev"]]["porcentaje"] = $notas["stats"]["6y4"][$lp["abrev"]]["cant"] * 100 / ($alumnosCant > 0 ? $alumnosCant : 1);
      $notas["stats"]["6y4"][$lp["abrev"]]["cant"] = $notas["stats"]["6y4"][$lp["abrev"]]["cant"] == 0 ? "" :
       number_format($notas["stats"]["6y4"][$lp["abrev"]]["cant"], 2);
      $notas["stats"]["6y4"][$lp["abrev"]]["porcentaje"] = $notas["stats"]["6y4"][$lp["abrev"]]["porcentaje"] == 0 ? "" :
       number_format($notas["stats"]["6y4"][$lp["abrev"]]["porcentaje"], 2);

      $notas["stats"]["3y1"][$lp["abrev"]]["porcentaje"] = $notas["stats"]["3y1"][$lp["abrev"]]["cant"] * 100 / ($alumnosCant > 0 ? $alumnosCant : 1);
      $notas["stats"]["3y1"][$lp["abrev"]]["cant"] = $notas["stats"]["3y1"][$lp["abrev"]]["cant"] == 0 ? "" :
       number_format($notas["stats"]["3y1"][$lp["abrev"]]["cant"], 2);
      $notas["stats"]["3y1"][$lp["abrev"]]["porcentaje"] = $notas["stats"]["3y1"][$lp["abrev"]]["porcentaje"] == 0 ? "" :
       number_format($notas["stats"]["3y1"][$lp["abrev"]]["porcentaje"], 2);

      $notas["stats"]["noCalif"]["porcentaje"] = $notas["stats"]["noCalif"][$lp["abrev"]]["cant"] * 100 / ($alumnosCant > 0 ? $alumnosCant : 1);
      $notas["stats"]["noCalif"][$lp["abrev"]]["cant"] = $notas["stats"]["noCalif"][$lp["abrev"]]["cant"] == 0 ? "" :
       number_format($notas["stats"]["noCalif"][$lp["abrev"]]["cant"], 2);
      $notas["stats"]["noCalif"][$lp["abrev"]]["porcentaje"] = $notas["stats"]["noCalif"][$lp["abrev"]]["porcentaje"] == 0 ? "" :
       number_format($notas["stats"]["noCalif"][$lp["abrev"]]["porcentaje"], 2);
    }
    //vd($notas["stats"]);
    //vd($notas["stats"]);
    return $notas;
  }

  public static function getNotasPlanillaNotas2($division_id, $asignatura_id)
  {
    $nivel_id = Helpers::qryScalar("
            select a.nivel_id
            from anio a
                inner join division d on a.id = d.`Anio_id` and d.id = $division_id
            limit 1
            ");
    $logica_id = Logica::getLogicaIdPorNivel($nivel_id);
    $ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
    $notas["datos"] = Helpers::qry("
            select ciclo.nombre as ciclo, an.nombre as anio, d.nombre as division, n.nombre as nivel
                from alumno_division ad
                  inner join division d on d.id = ad.division_id
                  inner join anio an on an.id = d.anio_id
                  inner join nivel n on n.id = an.nivel_id
                  inner join ciclo on ciclo.id = $ciclo_id
                where ad.Division_id = $division_id and
                      ad.Ciclo_id = $ciclo_id
              limit 1
        ");
    $alumno_divisiones = Helpers::qryAll("
            select ad.id as alumno_division_id, a.matricula, ad.alumno_id, ciclo.nombre as ciclo,
                   concat(a.apellido,', ',a.nombre) as alumno_nombre,  an.nombre as anio,
                    d.nombre as division, n.nombre as nivel, n.conducta_detallada, n.inasistencia_detallada
              from alumno_division ad
                inner join alumno a on a.id = ad.alumno_id
                inner join division d on d.id = ad.division_id
                inner join anio an on an.id = d.anio_id
                inner join nivel n on n.id = an.nivel_id
                inner join ciclo on ciclo.id = $ciclo_id
              where ad.Division_id = $division_id and
                    ad.Ciclo_id = $ciclo_id and
                    a.activo = 1
              order by a.sexo desc, a.apellido, a.nombre
          ");

    $logica_periodos = Helpers::qryAll("
            select lp.id, lp.abrev, lp.nombre
              from logica_periodo lp
              where logica_id = $logica_id
              order by lp.orden
          ");

    $notas["statsLeyendas"][] = "A. Aprobados entre 10 y 7";
    $notas["statsLeyendas"][] = "B. Desaprobados entre 6 y 4";
    $notas["statsLeyendas"][] = "C. Aplazados entre 3 y 1";
    $notas["statsLeyendas"][] = "D. No calificados";
    foreach ($logica_periodos as $lp) {
      $logica_periodo_id = $lp["id"];
      $notas["stats"]["10y7"][$lp["abrev"]]["cant"] = 0;
      $notas["stats"]["6y4"][$lp["abrev"]]["cant"] = 0;
      $notas["stats"]["3y1"][$lp["abrev"]]["cant"] = 0;
      $notas["stats"]["noCalif"][$lp["abrev"]]["cant"] = 0;
      $notas["stats"]["10y7"][$lp["abrev"]]["porcentaje"] = 0;
      $notas["stats"]["6y4"][$lp["abrev"]]["porcentaje"] = 0;
      $notas["stats"]["3y1"][$lp["abrev"]]["porcentaje"] = 0;
      $notas["stats"]["noCalif"][$lp["abrev"]]["porcentaje"] = 0;
      $logica_items = Helpers::qryAll("
                        select li.*
                          from logica_item li
                          where li.logica_periodo_id = $logica_periodo_id and (imprime_en_planilla = 1)
                          order by li.orden
                        ");
      foreach ($logica_items as $li) {
        $notas["items"][$li["id"]] = $li["nombre"];
      }
      $lp["abrev"]++;
    }
    $alumnosCant = 0;
    foreach ($alumno_divisiones as $ad) {
      $alumnosCant++;
      $alumno_division_id = $ad["alumno_division_id"];
      //$notasParaFormula = array();
      $lp["abrev"] = 0;
      foreach ($logica_periodos as $lp) {
        $logica_periodo_id = $lp["id"];
        $logica_items = Helpers::qryAll("
                        select li.*
                          from logica_item li
                          where li.logica_periodo_id = $logica_periodo_id
                          order by li.orden
                        ");
        $notaLp[$logica_periodo_id] = 0;
        foreach ($logica_items as $li) {
          $logica_item_id = $li["id"];
          //                    if ($li["manual"]) {
          $nota = Helpers::qryScalar("
                                select n.nota
                                  from nota n
                                    inner join logica_item li on li.id = n.Logica_item_id
                                  where n.Alumno_Division_id = $alumno_division_id and
                                        n.Asignatura_id = $asignatura_id and
                                        n.Logica_item_id = $logica_item_id
                                ");
          //                    } else {
          //                        $nota = LogicaActiva::getNota($logica_item_id, $notasParaFormula);
          //                    }
          //                    $notasParaFormula[$li["nombre_unico"]] = $nota;
          $notas["alumnos"][$ad["alumno_nombre"]]["notas"][$li["id"]] = $nota;
        }
        $notaLp[$lp["abrev"]] = $nota;
        $lp["abrev"]++;
      }
      foreach ($logica_periodos as $lp) {
        $logica_periodo_id = $lp["id"];
        $nota = $notaLp[$lp["abrev"]];
        if (is_numeric($nota)) {
          if ($nota <= 10 and $nota >= 7) {
            $notas["stats"]["10y7"][$lp["abrev"]]["cant"]++;
            $notas["stats"]["10y7"][$lp["abrev"]]["porcentaje"] = $notas["stats"]["10y7"][$lp["abrev"]]["cant"] * 100 / $alumnosCant;
          } elseif ($nota <= 6 and $nota >= 4) {
            $notas["stats"]["6y4"][$lp["abrev"]]["cant"]++;
            $notas["stats"]["6y4"][$lp["abrev"]]["porcentaje"] = $notas["stats"]["6y4"][$lp["abrev"]]["cant"] * 100 / $alumnosCant;
          } elseif ($nota <= 3 and $nota >= 1) {
            //$notas["stats"]["3y1"][$lp["abrev"]]["cant"]++;
            //$notas["stats"]["3y1"][$lp["abrev"]]["porcentaje"] =
            // $notas["stats"]["3y1"][$lp["abrev"]]["cant"] * 100 / $alumnosCant;
            //                        vd($notas["stats"]["3y1"][$lp["abrev"]], $nota,$lp["abrev"], $ad["alumno_nombre"]);
          } else {
            $notas["stats"]["noCalif"][$lp["abrev"]]["cant"]++;
            $notas["stats"][3]["noCalif"]["porcentaje"] = $notas["stats"]["noCalif"][$lp["abrev"]]["cant"] * 100 / $alumnosCant;
          }
        }
      }
    }
    foreach ($logica_periodos as $lp) {
      $logica_periodo_id = $lp["id"];
      $notas["stats"]["10y7"][$lp["abrev"]]["cant"] = $notas["stats"]["10y7"][$lp["abrev"]]["cant"] == 0 ? "" :
       number_format($notas["stats"]["10y7"][$lp["abrev"]]["cant"], 2);
      $notas["stats"]["10y7"][$lp["abrev"]]["porcentaje"] = $notas["stats"]["10y7"][$lp["abrev"]]["porcentaje"] == 0 ? "" :
       number_format($notas["stats"]["10y7"][$lp["abrev"]]["porcentaje"], 2);
      $notas["stats"]["6y4"][$lp["abrev"]]["cant"] = $notas["stats"]["6y4"][$lp["abrev"]]["cant"] == 0 ? "" :
       number_format($notas["stats"]["6y4"][$lp["abrev"]]["cant"], 2);
      $notas["stats"]["6y4"][$lp["abrev"]]["porcentaje"] = $notas["stats"]["6y4"][$lp["abrev"]]["porcentaje"] == 0 ? "" :
       number_format($notas["stats"]["6y4"][$lp["abrev"]]["porcentaje"], 2);
      $notas["stats"]["3y1"][$lp["abrev"]]["cant"] = $notas["stats"]["3y1"][$lp["abrev"]]["cant"] == 0 ? "" :
       number_format($notas["stats"]["3y1"][$lp["abrev"]]["cant"], 2);
      $notas["stats"]["3y1"][$lp["abrev"]]["porcentaje"] = $notas["stats"]["3y1"][$lp["abrev"]]["porcentaje"] == 0 ? "" :
       number_format($notas["stats"]["3y1"][$lp["abrev"]]["porcentaje"], 2);
      $notas["stats"]["noCalif"][$lp["abrev"]]["cant"] = $notas["stats"]["noCalif"][$lp["abrev"]]["cant"] == 0 ? "" :
       number_format($notas["stats"]["noCalif"][$lp["abrev"]]["cant"], 2);
      $notas["stats"]["noCalif"][$lp["abrev"]]["porcentaje"] = $notas["stats"]["noCalif"][$lp["abrev"]]["porcentaje"] == 0 ? "" :
       number_format($notas["stats"]["noCalif"][$lp["abrev"]]["porcentaje"], 2);
    }

    //vd($notas["stats"]);
    return $notas;
  }

  public static function getNotasPlanillaNotasConIntegradora($division_id, $asignatura_id)
  {
    $nivel_id = Helpers::qryScalar("
            select a.nivel_id
            from anio a
                inner join division d on a.id = d.`Anio_id` and d.id = $division_id
            limit 1
            ");
    $logica_id = Logica::getLogicaIdPorNivel($nivel_id);
    $ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
    $notas["datos"] = Helpers::qry("
            select ciclo.nombre as ciclo, an.nombre as anio, d.nombre as division, n.nombre as nivel
                from alumno_division ad
                  inner join division d on d.id = ad.division_id
                  inner join anio an on an.id = d.anio_id
                  inner join nivel n on n.id = an.nivel_id
                  inner join ciclo on ciclo.id = $ciclo_id
                where ad.Division_id = $division_id and
                      ad.Ciclo_id = $ciclo_id
              limit 1
        ");
    $alumno_divisiones = Helpers::qryAll("
            select ad.id as alumno_division_id, a.matricula, ad.alumno_id, ciclo.nombre as ciclo,
                   concat(a.apellido,', ',a.nombre) as alumno_nombre,  an.nombre as anio,
                    d.nombre as division, n.nombre as nivel, n.conducta_detallada, n.inasistencia_detallada
              from alumno_division ad
                inner join alumno a on a.id = ad.alumno_id
                inner join division d on d.id = ad.division_id
                inner join anio an on an.id = d.anio_id
                inner join nivel n on n.id = an.nivel_id
                inner join ciclo on ciclo.id = $ciclo_id
                inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_edu
                inner join alumno_division_estado e ON e.id = ad.alumno_division_estado_id AND e.muestra_edu
      where ad.Division_id = $division_id and
                    ad.Ciclo_id = $ciclo_id and
                    a.activo = 1 and ad.ciclo_id = $ciclo_id and ad.activo = 1 and e.muestra_edu = 1
              order by a.apellido, a.nombre
          ");

    $logica_periodos = Helpers::qryAll("
            select lp.id, lp.abrev, lp.nombre
              from logica_periodo lp
              where logica_id = $logica_id
              order by lp.orden
          ");
    $notas["statsLeyendas"][] = "A. Aprobados entre 10 y 7";
    $notas["statsLeyendas"][] = "B. Desaprobados entre 6 y 4";
    $notas["statsLeyendas"][] = "C. Aplazados entre 3 y 1";
    $notas["statsLeyendas"][] = "D. No calificados";
    foreach ($logica_periodos as $key => $lp) {
      $logica_periodo_id = $lp["id"];
      $notas["stats"]["10y7"][$lp["abrev"]]["cant"] = 0;
      $notas["stats"]["6y4"][$lp["abrev"]]["cant"] = 0;
      $notas["stats"]["3y1"][$lp["abrev"]]["cant"] = 0;
      $notas["stats"]["noCalif"][$lp["abrev"]]["cant"] = 0;
      $notas["stats"]["10y7"][$lp["abrev"]]["porcentaje"] = 0;
      $notas["stats"]["6y4"][$lp["abrev"]]["porcentaje"] = 0;
      $notas["stats"]["3y1"][$lp["abrev"]]["porcentaje"] = 0;
      $notas["stats"]["noCalif"][$lp["abrev"]]["porcentaje"] = 0;
      $logica_items = Helpers::qryAll("
                        select li.*
                          from logica_item li
                          where li.logica_periodo_id = $logica_periodo_id and (imprime_en_planilla = 1  or tipo_nota=100)
                          order by li.orden
                        ");
      foreach ($logica_items as $li) {
        $notas["items"][$li["id"]] = $li["nombre"];
      }
      $lp["abrev"]++;
    }
    $alumnosCant = 0;
    LogicaActiva::$nivel_id = $nivel_id;
    foreach ($alumno_divisiones as $ad) {
      $alumnosCant++;
      $alumno_division_id = $ad["alumno_division_id"];
      //$notasParaFormula = array();
      $lp["abrev"] = 0;
      LogicaActiva::$notas = array();
      $notasTran = LogicaActiva::getNotasAlumnoDivisionAsignatura($alumno_division_id, $asignatura_id);
      foreach ($logica_periodos as $lp) {
        $logica_periodo_id = $lp["id"];
        $logica_items = Helpers::qryAll("
                        select li.*
                          from logica_item li
                          where li.logica_periodo_id = $logica_periodo_id
                          order by li.orden
                        ");
        $notaLp[$logica_periodo_id] = 0;
        foreach ($logica_items as $li) {
          $logica_item_id = $li["id"];
          if ($li["manual"]) {
            $nota = Helpers::qryScalar("
                                select n.nota
                                  from nota n
                                  where n.Alumno_Division_id = $alumno_division_id and
                                        n.Asignatura_id = $asignatura_id and
                                        n.Logica_item_id = $logica_item_id
                                ");
          }
          $notas["alumnos"][$ad["alumno_nombre"]]["notas"][$li["id"]] = isset($notasTran[$li["nombre_unico"]]["nota"])?$notasTran[$li["nombre_unico"]]["nota"]:'';
        }
        $notaLp[$lp["abrev"]] = $nota;
        $lp["abrev"]++;
      }
      foreach ($logica_periodos as $lp) {
        $logica_periodo_id = $lp["id"];
        $nota = round($notaLp[$lp["abrev"]], 0);
        if (is_numeric($nota)) {
          if ($nota <= 10 and $nota >= 7) {
            $notas["stats"]["10y7"][$lp["abrev"]]["cant"]++;
            $notas["stats"]["10y7"][$lp["abrev"]]["porcentaje"] = $notas["stats"]["10y7"][$lp["abrev"]]["cant"] * 100 / $alumnosCant;
          } elseif ($nota <= 6 and $nota >= 4) {
            $notas["stats"]["6y4"][$lp["abrev"]]["cant"]++;
            $notas["stats"]["6y4"][$lp["abrev"]]["porcentaje"] = $notas["stats"]["6y4"][$lp["abrev"]]["cant"] * 100 / $alumnosCant;
          } elseif ($nota <= 3 and $nota >= 1) {

          } else {
            $notas["stats"]["noCalif"][$lp["abrev"]]["cant"]++;
            $notas["stats"]["noCalif"]["porcentaje"] = $notas["stats"]["noCalif"][$lp["abrev"]]["cant"] * 100 / $alumnosCant;
          }
        }
      }
    }
    foreach ($logica_periodos as $lp) {
      $logica_periodo_id = $lp["id"];
      $notas["stats"]["10y7"][$lp["abrev"]]["cant"] = $notas["stats"]["10y7"][$lp["abrev"]]["cant"] == 0 ? "" :
       number_format($notas["stats"]["10y7"][$lp["abrev"]]["cant"], 2);
      $notas["stats"]["10y7"][$lp["abrev"]]["porcentaje"] = $notas["stats"]["10y7"][$lp["abrev"]]["porcentaje"] == 0 ? "" :
       number_format($notas["stats"]["10y7"][$lp["abrev"]]["porcentaje"], 2);
      $notas["stats"]["6y4"][$lp["abrev"]]["cant"] = $notas["stats"]["6y4"][$lp["abrev"]]["cant"] == 0 ? "" :
       number_format($notas["stats"]["6y4"][$lp["abrev"]]["cant"], 2);
      $notas["stats"]["6y4"][$lp["abrev"]]["porcentaje"] = $notas["stats"]["6y4"][$lp["abrev"]]["porcentaje"] == 0 ? "" :
       number_format($notas["stats"]["6y4"][$lp["abrev"]]["porcentaje"], 2);
      $notas["stats"]["3y1"][$lp["abrev"]]["cant"] = $notas["stats"]["3y1"][$lp["abrev"]]["cant"] == 0 ? "" :
       number_format($notas["stats"]["3y1"][$lp["abrev"]]["cant"], 2);
      $notas["stats"]["3y1"][$lp["abrev"]]["porcentaje"] = $notas["stats"]["3y1"][$lp["abrev"]]["porcentaje"] == 0 ? "" :
       number_format($notas["stats"]["3y1"][$lp["abrev"]]["porcentaje"], 2);
      $notas["stats"]["noCalif"][$lp["abrev"]]["cant"] = $notas["stats"]["noCalif"][$lp["abrev"]]["cant"] == 0 ? "" :
       number_format($notas["stats"]["noCalif"][$lp["abrev"]]["cant"], 2);
      $notas["stats"]["noCalif"][$lp["abrev"]]["porcentaje"] = $notas["stats"]["noCalif"][$lp["abrev"]]["porcentaje"] == 0 ? "" :
       number_format($notas["stats"]["noCalif"][$lp["abrev"]]["porcentaje"], 2);
    }

    //vd($notas["stats"]);
    return $notas;
  }

  public function __get($name)
  {
    switch ($name) {
      case "articulo_id":
        if ($this->attributes["articulo_id"] == 0) {
          return $this->anio->articulo_id;
        }
        break;

      case "Anio_id":


        break;

      default:
        break;
    }

    return parent::__get($name);
  }

  //    public static function getNotasPorPeriodo3($division_id, $logica_periodo_id, $asignatura_id) {
  //        $notas = array();
  //        $ciclo_id = Ciclo::getActivoId();
  //        $todasLasAsignaturas = $asignatura_id ? false : true;
  //        $todosLosPeriodos = $logica_periodo_id ? false : true;
  //        $notas["datos"] = Helpers::qry("
  //      select ciclo.nombre as ciclo, an.nombre as anio, d.nombre as division, n.nombre as nivel
  //        from alumno_division ad
  //          inner join division d on d.id = ad.division_id
  //          inner join anio an on an.id = d.anio_id
  //          inner join nivel n on n.id = an.nivel_id
  //          inner join ciclo on ciclo.id = $ciclo_id
  //        where ad.Division_id = $division_id and
  //              ad.activo = 1 and ad.Ciclo_id = $ciclo_id
  //        limit 1
  //    ");
  //        $alumno_divisiones = Helpers::qryAll("
  //      select ad.id as alumno_division_id, a.matricula, ad.alumno_id, ciclo.nombre as ciclo,
  //             concat(a.apellido,', ',a.nombre) as alumno_nombre,
  //             an.nombre as anio, d.nombre as division, n.nombre as nivel
  //        from alumno_division ad
  //          inner join alumno a on a.id = ad.alumno_id
  //          inner join division d on d.id = ad.division_id
  //          inner join anio an on an.id = d.anio_id
  //          inner join nivel n on n.id = an.nivel_id
  //          inner join ciclo on ciclo.id = $ciclo_id
  //        where ad.Division_id = $division_id and
  //              ad.activo = 1 and ad.Ciclo_id = $ciclo_id and
  //              a.activo = 1
  //        order by a.sexo desc, a.apellido, a.nombre
  //    ");
  //
  //        $whereAsignaturas = $asignatura_id ? " and da.asignatura_id = $asignatura_id " : "";
  //        $division_asignaturas = Helpers::qryAll("
  //      select da.id as division_asignatura_id, a.id as asignatura_id,
  //             a.nombre as asignatura_nombre
  //        from division_asignatura da
  //          inner join asignatura a on a.id = da.asignatura_id
  //        where da.Division_id = $division_id $whereAsignaturas
  //        order by case when da.orden = 0 or da.orden is null then a.orden else da.orden end
  //    ");
  //
  //
  //        foreach ($division_asignaturas as $division_asignatura) {
  //            $notas["asignaturas"][] = $division_asignatura["asignatura_nombre"];
  //            $notas["asignaturas-desaprobadas"][$division_asignatura["asignatura_nombre"]] = 0;
  //        }
  //
  //
  //        $whereLogicaPeriodo = $logica_periodo_id ? " where lp.id = $logica_periodo_id " : "";
  //        $logica_periodos = Helpers::qryAll("
  //      select lp.id, lp.abrev, lp.nombre
  //        from logica_periodo lp
  //        $whereLogicaPeriodo
  //    ");
  //
  //        foreach ($alumno_divisiones as $ad) {
  //            $alumno_division_id = $ad["alumno_division_id"];
  //            $sumaAsignaturasDesaprobadas = 0;
  //            foreach ($division_asignaturas as $division_asignatura) {
  //                $notasParaFormula = array();
  //                foreach ($logica_periodos as $lp) {
  //                    $logica_periodo_id = $lp["id"];
  //                    $logica_items = Helpers::qryAll("
  //          select li.*
  //            from logica_item li
  //            where li.logica_periodo_id = $logica_periodo_id
  //            order by li.orden
  //        ");
  //                    $asignatura_id = $division_asignatura["asignatura_id"];
  //                    foreach ($logica_items as $li) {
  //                        $logica_item_id = $li["id"];
  //                        if ($li["manual"]) {
  //                            $nota = Helpers::qryScalar("
  //                select n.nota
  //                  from nota n
  //                  where n.Alumno_Division_id = $alumno_division_id and
  //                        n.Asignatura_id = $asignatura_id and
  //                        n.Logica_item_id = $logica_item_id
  //            ");
  //                        } else {
  //                            $nota = self::formula($li["formula"], $notasParaFormula);
  //                        }
  //                        $notasParaFormula[$li["nombre_unico"]] = $nota;
  //                    }
  //                    $sumaAsignaturasDesaprobadas+=(($nota < 7) ? 1 : 0);
  //                    if ($todosLosPeriodos) {
  //                        if ($todasLasAsignaturas) {
  //                            $notas["alumnos"][$ad["alumno_nombre"]]["notas"][$li["id"]][$asignatura_id] = $nota;
  //                        } else {
  //                            $notas["alumnos"][$ad["alumno_nombre"]]["notas"][$li["id"]] = $nota;
  //                        }
  //                    } else {
  //                        if ($todasLasAsignaturas) {
  //                            $notas["alumnos"][$ad["alumno_nombre"]]["notas"][$asignatura_id] = $nota;
  //                        } else {
  //                            $notas["alumnos"][$ad["alumno_nombre"]]["notas"] = $nota;
  //                        }
  //                    }
  //                    $notas["asignaturas-desaprobadas"][$division_asignatura["asignatura_nombre"]]
  //                            += (($nota < 7) ? 1 : 0);
  //                }
  //                $notas[$ad["alumno_nombre"]]["sumaAsignaturasDesaprobadas"] = $sumaAsignaturasDesaprobadas;
  //                if ($logica_periodo_id) {
  //                    $amonestaciones = Helpers::qryScalar("
  //              select sum(d.cantidad) as sanciones
  //                from conducta c
  //                  inner join conducta_detalle d on d.conducta_id = c.id
  //                where c.Alumno_Division_id = $alumno_division_id and
  //                      c.logica_periodo_id = $logica_periodo_id
  //            ");
  //                    $inasistencias = Helpers::qryScalar("
  //            select sum(t.valor) inasistencias
  //              from inasistencia i
  //                inner join inasistencia_detalle d on d.inasistencia_id = i.id
  //                inner join inasistencia_tipo t on t.id = d.inasistencia_tipo_id
  //              where i.Alumno_Division_id = $alumno_division_id and
  //                    i.logica_periodo_id = $logica_periodo_id
  //          ");
  //                    $notas[$ad["alumno_nombre"]]["amonestaciones"] = $amonestaciones;
  //                    $notas[$ad["alumno_nombre"]]["inasistencias"] = $inasistencias;
  //                }
  //            }
  //        }
  //        return $notas;
  //    }
  //    public static function getNotasPorAsignatura($division_asignatura_id, $logica_periodo_id) {
  //        $notas = array();
  //        $ciclo_id = Ciclo::getActivoId();
  //        $division_id = Helpers::qryScalar("
  //      select da.id
  //        from division_asignatura da
  //        where da.Division_id = $division_asignatura_id
  //    ");
  //
  //        $notas["datos"] = Helpers::qry("
  //      select ciclo.nombre as ciclo, an.nombre as anio, d.nombre as division, n.nombre as nivel
  //        from alumno_division ad
  //          inner join division d on d.id = ad.division_id
  //          inner join anio an on an.id = d.anio_id
  //          inner join nivel n on n.id = an.nivel_id
  //          inner join ciclo on ciclo.id = $ciclo_id
  //        where ad.Division_id = $division_id and
  //              ad.activo = 1 and ad.Ciclo_id = $ciclo_id
  //        limit 1
  //    ");
  //        $alumno_divisiones = Helpers::qryAll("
  //      select ad.id as alumno_division_id, a.matricula, ad.alumno_id, ciclo.nombre as ciclo,
  //             concat(a.apellido,', ',a.nombre) as alumno_nombre,
  //             an.nombre as anio, d.nombre as division, n.nombre as nivel
  //        from alumno_division ad
  //          inner join alumno a on a.id = ad.alumno_id
  //          inner join division d on d.id = ad.division_id
  //          inner join anio an on an.id = d.anio_id
  //          inner join nivel n on n.id = an.nivel_id
  //          inner join ciclo on ciclo.id = $ciclo_id
  //        where ad.Division_id = $division_id and
  //              ad.activo = 1 and ad.Ciclo_id = $ciclo_id and
  //              a.activo = 1
  //        order by a.sexo desc, a.apellido, a.nombre
  //    ");
  //        $division_asignaturas = Helpers::qryAll("
  //      select da.id as division_asignatura_id, a.id as asignatura_id,
  //             a.nombre as asignatura_nombre
  //        from division_asignatura da
  //          inner join asignatura a on a.id = da.asignatura_id
  //        where da.Division_id = $division_id
  //        order by a.orden, a.nombre
  //    ");
  //
  //        foreach ($division_asignaturas as $division_asignatura) {
  //            $notas["asignaturas"][] = $division_asignatura["asignatura_nombre"];
  //            $notas["asignaturas-desaprobadas"][$division_asignatura["asignatura_nombre"]] = 0;
  //        }
  //
  //
  //        $logica_items = Helpers::qryAll("
  //      select li.*
  //        from logica_item li
  //        where li.Logica_Periodo_id = $logica_periodo_id
  //        order by li.orden
  //    ");
  //        foreach ($alumno_divisiones as $ad) {
  //            $alumno_division_id = $ad["alumno_division_id"];
  //            $sumaAsignaturasDesaprobadas = 0;
  //            foreach ($division_asignaturas as $division_asignatura) {
  //                $asignatura_id = $division_asignatura["asignatura_id"];
  //                $notasParaFormula = array();
  //                foreach ($logica_items as $li) {
  //                    $logica_item_id = $li["id"];
  //                    if ($li["manual"]) {
  //                        $nota = Helpers::qryScalar("
  //                select n.nota
  //                  from nota n
  //                  where n.Alumno_Division_id = $alumno_division_id and
  //                        n.Asignatura_id = $asignatura_id and
  //                        n.Logica_item_id = $logica_item_id
  //            ");
  //                    } else {
  //                        $nota = self::formula($li["formula"], $notasParaFormula);
  //                    }
  //                    $notasParaFormula[$li["nombre_unico"]] = $nota;
  //                }
  //                $sumaAsignaturasDesaprobadas+=(($nota < 7) ? 1 : 0);
  //                $notas["alumnos"][$ad["alumno_nombre"]]["notas"][$asignatura_id] = $nota;
  //                $notas["asignaturas-desaprobadas"][$division_asignatura["asignatura_nombre"]]
  //                        += (($nota < 7) ? 1 : 0);
  //            }
  //            $notas[$ad["alumno_nombre"]]["sumaAsignaturasDesaprobadas"] = $sumaAsignaturasDesaprobadas;
  //            foreach ($logica_items as $li) {
  //                $logica_item_id = $li["id"];
  //                if ($li["tiene_conducta"]) {
  //                    $nota = Helpers::qryScalar("
  //              select sum(d.cantidad) as sanciones
  //                from conducta c
  //                  inner join conducta_detalle d on d.conducta_id = c.id
  //                where c.Alumno_Division_id = $alumno_division_id and
  //                      c.logica_periodo_id = $logica_periodo_id
  //            ");
  //                }
  //            }
  //            $notas[$ad["alumno_nombre"]]["amonestaciones"] = $nota;
  //
  //            foreach ($logica_items as $li) {
  //                $logica_item_id = $li["id"];
  //                if ($li["tiene_conducta"]) {
  //                    $nota = Helpers::qryScalar("
  //            select sum(t.valor) inasistencias
  //              from inasistencia i
  //                inner join inasistencia_detalle d on d.inasistencia_id = i.id
  //                inner join inasistencia_tipo t on t.id = d.inasistencia_tipo_id
  //              where i.Alumno_Division_id = $alumno_division_id and
  //                    i.logica_periodo_id = $logica_periodo_id
  //          ");
  //                }
  //            }
  //            $notas[$ad["alumno_nombre"]]["inasistencias"] = $nota;
  //        }
  //        return $notas;
  //    }
  //    public static function formula($formula, $notas) {
  //        // hasta aca va en formula
  //        if (strlen(trim($formula)) > 1) {
  //            try {
  //                return eval($formula);
  //            } catch (Exception $e) {
  //                //$nota->error = "Error en la fórmula";
  //            }
  //        }
  //        // a partir de aca va en formula una vez ande bien
  //    }
}
