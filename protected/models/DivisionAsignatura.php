<?php

	Yii::import('application.models._base.BaseDivisionAsignatura');

	include_once("formulas/LogicaActiva.php");

	class DivisionAsignatura extends BaseDivisionAsignatura {

		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		public function getLogica($ciclo = null) {
			$ciclo = $ciclo ? $ciclo : Ciclo::getCicloParaCargaDeNotas();
			$at_id = DivisionAsignatura::model()->with(array("asignatura", "asignatura.asignaturaTipo"))->findByPk($this->id)->asignatura->asignaturaTipo->id;
			return LogicaCiclo::model()->with("logica")->find("Asignatura_Tipo_id = $at_id and Ciclo_id = $ciclo->id")->logica;
		}

		public function getNotas($logica_periodo_id = null, $cuali = false) {
			return LogicaActiva::getNotasDivisionAsignatura($this->Division_id, $this->Asignatura_id, $logica_periodo_id = null, $cuali);
		}

		public function grabaNotasSinValidar($notas, $cuali) {
      // vd2($cuali);
//			ve($this->attributes);
//			return;
			$ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
			$division_id = $this->Division_id;
			$asignatura_id = $this->Asignatura_id;
			$tr = Yii::app()->db->beginTransaction();
			foreach ($notas as $alumno_id => $notasAlumno) {
				foreach ($notasAlumno as $logica_item_id => $valor_nota) {
					$select = /** @lang text */
						"select id from alumno_division
                            where Alumno_id=$alumno_id /*and activo*/ and
                                         Division_id = $division_id and
                                         alumno_division.ciclo_id = $ciclo_id and
                                         alumno_division.borrado = 0";
//					vd($select);
					$alumno_division_id = AlumnoDivision::model()->findBySql($select)->id;
					if(!$alumno_division_id){
						throw new Exception("$select");
					};
//					vd($alumno_division_id);
					$nota = Nota::model()->findBySql("
						select n.* from nota n
							inner join alumno_division ad on ad.id = n.Alumno_Division_id
							where ad.id = $alumno_division_id AND
										n.Asignatura_id = $asignatura_id AND
										n.Logica_item_id = $logica_item_id AND
										ad.borrado = 0");
//					vd("
//						select * from nota n
//							inner join alumno_division ad on ad.id = n.Alumno_Division_id
//							where ad.id = $alumno_division_id AND
//										n.Asignatura_id = $asignatura_id AND
//										n.Logica_item_id = $logica_item_id AND
//										!ad.borrado");
//					$nota = Nota::model()->find("
//                    alumno_division_id=$alumno_division_id and
//                    asignatura_id = $asignatura_id and
//                    logica_item_id = $logica_item_id");
//					ve($nota->attributes);
					if ($nota) {
            if($cuali){
              $nota->texto = $valor_nota;
            } else {
              $nota->nota = $valor_nota;
            }
//						ve('xxx',$nota->attributes);
					} else {
						$nota = new Nota();
						$nota->Alumno_Division_id = $alumno_division_id;
						$nota->Asignatura_id = $asignatura_id;
						$nota->Logica_item_id = $logica_item_id;
            if($cuali){
              $nota->texto = $valor_nota;
            } else {
              $nota->nota = $valor_nota;
            }
					}
					if(!$cuali and !$nota->validate()){
						throw new Exception('no se pudo validar la nota');
					}
					$nota->save();
					$nota->save();
					$nota->save();
					$nota->save();
					$nota->save();
					if(!$nota->save()){
						throw new Exception('La nota no fué guardada');
					}
				}
			}
			$tr->commit();
		}

	}
