<?php

Yii::import('application.models._base.BaseDoc');

class Doc extends BaseDoc {

  //public $afip = null;
  public $oldRecord;
  private /* $_detalle, */
    $_total_anterior;
  private $cajaCerrada;
  private $esElectronica = false;

  public function __construct($scenario = 'insert', $comprob_id = null) {

    $this->oldRecord = $this;
    $this->esElectronica = isset($_GET["esElectronica"]);
    parent::__construct($scenario);

    if (!$comprob_id) {

      $caller = debug_backtrace(false);

      if (substr($caller[0]["file"], -17) == "CActiveRecord.php") {
        return;
      } else {
        throw new Exception("Hay que pasar el comprobante como parametro en el constructor, despues del escenario \nej.    \$model = new Doc(\"insert\",\"Recibo\");
");
      }
    }
    $comprob = Comprob::model()->findByPk($comprob_id);

    if (!$comprob) {
      Helpers::error("El comprobante con el id $comprob_id no esta dado de alta");
      die;
    }

    //    if (!$this->getTalonarioUsuario($comprob_id)) {
    //    }
    $this->talonario_id = $this->getTalonarioUsuario($comprob_id);

    if (!$this->talonario_id) {
      $user_id = Yii::app()->user->id;
      Helpers::error("El usuario $user_id no tiene asignado un talonario para el comprobante \"$comprob->nombre\"($comprob_id)");
      die;
    }
    //$mode = isset($_GET["mode"]) ? $_GET["mode"]:Afip::DEV;
    if ($this->talonario->comprob->afip_comprob) {
      $this->afip = true;
      $mode = isset($_GET["mode"]) ? $_GET["mode"] : Afip::MODO_PROD;
      $afip = new Afip($mode, $this->talonario->comprob->afip_comprob);
      if (!$afip->ultimoNumero) {
        throw new Exception("Problemas de conexión con AFIP");
      }
      $this->numero = str_pad($afip->ultimoNumero + 1, 8, "0", STR_PAD_LEFT);
      $this->sucursal = str_pad(3, 4, "0", STR_PAD_LEFT);
      $this->letra = 'X';
    } elseif (!$this->talonario->numeracion_manual) {
      $this->numero = str_pad($this->siguienteNumero, 8, "0", STR_PAD_LEFT);
      $this->sucursal = str_pad($this->talonario->punto_venta_numero, 4, "0", STR_PAD_LEFT);
      $this->letra = $this->talonario->letra;
    }
    //$this->user_id = Yii::app()->user->id;
    $this->anulado = 0;
    $this->fecha_creacion = $this->fecha_creacion ? $this->fecha_creacion : new CDbExpression('NOW()');
    $this->fecha_valor = $this->fecha_valor ? $this->fecha_valor : new CDbExpression('NOW()');

    //      $this->Comprob_Punto_Venta_id = $comprobPuntoVenta->id;
  }

  public function getTalonarioUsuario($comprob_id) {
    //$comprob_id = $comprob_id ? $comprob_id : $this->talonario->comprob->id;
    /* @var $user User */
    $user = Yii::app()->user->model;
    //vd($user->puestoTrabajo);
    if (!$user->puestoTrabajo) {
      Helpers::error("El usuario activo no tiene asociado un puesto de trabajo");
      die;
    }
    $user_id = $user->id;
//    vd("
//             select t.id
//          from user u
//              inner join puesto_trabajo p on p.id = u.puesto_trabajo_id
//              inner join talonario t on t.punto_venta_numero = p.punto_venta_numero and t.comprob_id = $comprob_id
//          where u.id =  $user_id");
    $select = "
             select t.id
          from user u
              inner join puesto_trabajo p on p.id = u.puesto_trabajo_id
              inner join talonario t on t.punto_venta_numero = p.punto_venta_numero and t.comprob_id = $comprob_id
          where u.id =  $user_id";

    //vd($select);
    return Yii::app()->db->createCommand($select)->queryScalar();
  }

  //  public function defaultScope() {
  //    $alias = $this->getTableAlias(false, false);
  //    return array(
  //        'condition' => "$alias.anulado <> 1",
  //    );
  //  }

  static public function altaDoc($post, &$errors = array(), $conTransaccion = true) {
    //Alta de documento.
    $comprob_id = $post['doc']['comprob_id'];
    $esElectronica = Helpers::qryScalar("select afip from comprob where id = $comprob_id");
//    $esElectronica = $esElectronica === "on" ? true:false;
    $graba = true;
    if ($conTransaccion) {
      $tr = Yii::app()->db->beginTransaction();
    }
    $doc_id = isset($_POST['doc']['id']) ? $_POST['doc']['id'] : null;
    if ($doc_id) {
      $doc = Doc::model()->findByPk($doc_id);
      $doc->anula();
    }
    $doc = new Doc("insert", $post['doc']['comprob_id']);
    $post['doc']['total'] = round($post['doc']['total'],2);
    $doc->setAttributes($post["doc"]);
    if (isset($post['doc']['fecha'])) {
      $doc->fecha_creacion = Helpers::fechaParaGrabar($post['doc']['fecha']);
    } else {
      $doc->fecha_creacion = $doc->fecha_creacion ? $doc->fecha_creacion : new CDbExpression('NOW()');
    }
    if ($esElectronica) {
      $doc->afip = 1;
    }

    if (isset($post['doc']['fecha_valor'])) {
      $doc->fecha_valor = Helpers::fechaParaGrabar($post['doc']['fecha_valor']);
    }

    if (isset($post['doc']['pago-a-cuenta-importe'])) {
      $doc->pago_a_cuenta_importe = $post['doc']['pago-a-cuenta-importe'];
    }

    if (isset($post['doc']['pago-a-cuenta-detalle'])) {
      $doc->pago_a_cuenta_detalle = $post['doc']['pago-a-cuenta-detalle'];
    }
    $errors["General"] = Doc::validaDoc($post);

    //grabo el CUIT
    if (isset($post['doc']['socio_nombre'])) {
      $doc->socio_nombre = $post['doc']['socio_nombre'];
    } elseif (isset($post['doc']['Socio_id'])) {
      $socio_id = $post['doc']['Socio_id'];
      $socio_nombre = Helpers::qryScalar("
        select case when p.id then p.nombre_fantasia when a.id then concat(a.apellido, ', ', a.nombre) end as nombre
          from socio s
           left join proveedor p on p.id = s.Proveedor_id
           left join alumno a on a.id = s.Alumno_id
          where s.id = $socio_id
        ");
      $post['doc']['socio_nombre'] = $socio_nombre;
      $doc->socio_nombre = $socio_nombre;
    }
    if (isset($post['doc']['cuit'])) {
      $post['doc']['cuit'] = format_cuit($post['doc']['cuit']);
      $doc->cuit = $post['doc']['cuit'];
    } elseif (isset($post['doc']['Socio_id'])) {
      $socio_id = $post['doc']['Socio_id'];
      $cuit = Helpers::qryScalar("
        select p.cuit from proveedor p
          inner join socio s on s.Proveedor_id = p.id and s.id = $socio_id
        ");
      $post['doc']['cuit'] = format_cuit($cuit);
      $doc->cuit = $cuit;
    }
    try {
      if (!$doc->save()) {
        $errors["General"] = array_merge($errors["General"], $doc->errors);

        return null;
      }
    } catch (Exception $exc) {
      $errors["Error de grabación, error ="] = $exc->getMessage();

      return $errors;
    }
//    vd2($doc->cuit);
    if (isset($post['doc-det'])) {
      $errors["Artículos"] = $doc->grabaDetalle($post["doc-det"]);
    }

    if ((isset($post['valores']))) {
      if (($post['doc']['comprob_id'] == Comprob::MOVIMIENTO_BANCARIO_D) || ($post['doc']['comprob_id'] == Comprob::MOVIMIENTO_BANCARIO_H)) {
        $errors["Valores"] = $doc->grabaValores($post["valores"], $post['doc']['destino_id'], Helpers::fechaParaGrabar($post['doc']['fecha']));
      } else {
        $errors["Valores"] = $doc->grabaValores($post["valores"]);
      }
    }
    if ((isset($post['intereses']))) {
      $errors["Intereses"] = $doc->grabaIntereses($post['intereses']);
    }

    if ((isset($post['aplicaciones']))) {
      $errors["Aplicaciones"] = $doc->grabaAplicaciones($post['aplicaciones']);
    }
    if ((isset($post['aplicacionesAuto']))) {
      $errors["Aplicaciones"] = $doc->grabaAplicaciones('auto');
      unset($errors["Aplicaciones"]);
    }
    if ($errors) {
      foreach ($errors as $key => $error) {
        if (count($error) > 0) {
          $graba = false;
        } else {
          if (is_array($error) and count($error) == 0) {
            unset($errors[$key]);
          }
        }
      }
    }
    if ($graba) {
      if (!isset($_POST["doc"]["id"]) or ! $_POST["doc"]["id"]) {
//					$retencion = Helpers::qry("
//										select v.importe, t.nombre, v.tipo, v.numero,  d.nombre as destino,
//												concat(p.nombre,'  ', ta.nombre ) as tarjeta,
//												b.nombre as banco, DATE_FORMAT(v.fecha, '%d/%m/%Y') as fecha,
//											from doc_valor v
//												inner join doc_valor_tipo t on t.id = v.tipo
//												left join destino d on d.id = v.Destino_id
//												left join tarjeta_plan p on p.id = v.tarjeta_plan_id
//												left join tarjeta ta on ta.id = p.tarjeta_id
//												left join banco b on b.id = v.banco_id
//											where v.doc_id = $doc_id and v.tipo = 7
//										");
        //		vd($doc["apl"]);
        $talonarioDataRetencion = Talonario::getTalonarioData(Comprob::RETENCION);
        $talonario_id = $talonarioDataRetencion["talonario_id"];
        /* @var $t Talonario */
        $t = Talonario::model()->findByPk($talonario_id);
        $t->numero++;
        $t->save();
      }
      if ($conTransaccion) {
        $afipOK = true;
        if ($esElectronica) {
          $datosCae = self::grabaDocAfip($doc);
          if ($datosCae["cae"]) {
            $doc->afip = 1;
            $doc->numero = $datosCae['cbt_desde'];
            $doc->sucursal = $datosCae['punto_vta'];
            if (!$doc->save()) {
              $doc->errors('doc-save', $doc->errors);
            }
            $doc->afip = $datosCae;
          } else {
            //$afipError->socio_id = ;
            $afipOK = false;
            $errors['afip'] = array();
            $errors['afip']['error'] = 'Error desde afip, no me devolvió un CAE';
            $errors['afip']['motivos_obs'] = "motivos_obs:" . $datosCae["motivos_obs"];
            $errors['afip']['err_msg'] = "err_msg:" . $datosCae["err_msg"];
            $errors['afip']['err_code'] = "err_code:" . $datosCae["err_code"];
          }
        }
        if (($esElectronica && $afipOK) || !$esElectronica) {
          $tr->commit();
        } else {
          $tr->rollback();
          $afipError = new AfipError();
          $afipError->socio_id = $doc->Socio_id;
          $afipError->doc = json_encode(array(
            "doc" => $doc->attributes,
            "docAfip" => $datosCae
          ));
          if (!$afipError->save()) {
            vd("error", $afipError->errors);
          }
        }
      }
      if (isset($afipOK) && $afipOK) {
        $errors = array();
      }
    } else {
      if ($conTransaccion) {
        $tr->rollback();
      }
      $doc = null;
    }
    return $doc;
  }

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  static public function validaDoc($post) {
    // Que cada comprobante tenga como maximo de pago su valor
    // pago a cuenta mas montos de pago por comprobante
    // la suma de los subimportes = total
    $comprob_id = $post['doc']['comprob_id'];

    $errores = array();
    //        if (isset($post["doc"]["Socio_id"])) {
    //            $partesNumero = explode("-", $post["doc"]["numero"]);
    //            $sucursal = $partesNumero[0] + 0;
    //            $numero = $partesNumero[1] + 0;
    //            $repetido = Helpers::qryScalar("
    //                select count(*) as existe
    //                    from doc d
    //                        where d.Socio_id = " . $post["doc"]["Socio_id"] . " and
    //                                                 d.sucursal = $sucursal  and
    //                                                 d.numero =  $numero and
    //                                                 d.anulado = 0");
    //            if ($repetido > 0) {
    //                $errores["numero"] = array('Ya hay un documento con este número para este proveedor');
    //                return $errores;
    //            }
    //        }

    $docTotal = $post['doc']['total'];
    if ($docTotal <= 0) {
      $errores["total"] = array('El total del documento debe ser mayor que 0');
    }

    $intereses = isset($post['intereses']) ? $post['intereses'] : array();
    $interesesTotal = 0;
    foreach ($intereses as $id => $data) {
      $interesesTotal += $data["valor"];
    }

    $aplicaciones = isset($post['aplicaciones']) ? $post['aplicaciones'] : array();
    $aplicacionesTotal = 0;
    foreach ($aplicaciones as $id => $valor) {
      $doc = Doc::model()->findByPk($id);
      if ($doc->total < $valor) {
        $errores["imputación"] = array("El pago imputado al comprobante($doc->total) no puede ser mayor al saldo del mismo($valor)");
      }
      if ($valor < 0) {
        $errores["imputación"] = array("La imputaciÃ³n debe ser positiva (ahora:$valor)");
      }
      $aplicacionesTotal += $valor;
    }

    $articulos = isset($post['articulos']) ? $post['articulos'] : array();
    $articulosTotal = 0;
    foreach ($articulos as $id => $valor) {
      $articulosTotal += abs($valor);
    }

    $pagoACuenta = 0;
    if (isset($post['doc']['pago-a-cuenta-importe'])) {
      $pagoACuenta += $post['doc']['pago-a-cuenta-importe'];
    }

    $valores = isset($post['valores']) ? $post["valores"] : array();
    $valoresTotal = 0;
    foreach ($valores as $valor) {
      $tipoDeValor = key($valor);

      if ($tipoDeValor == "undefined") {
        continue;
      }
      $valor = reset($valor);

      if ((is_numeric($valor["importe"])) && (!isset($valor["borrado"]))) {
        $valor["importe"] = round($valor["importe"], 2);
        $valoresTotal += $valor["importe"];
      }
      if ($valor["importe"] <= 0) {
        $numero = isset($valor["numero"]) ? $valor["numero"] : "--";
        $importe = $valor["importe"];
        $errores["valor"] = array("El valor($tipoDeValor:$numero) debe ser mayor que 0 (ahora:$importe) ");
      }
    }

    /*
     * $docTotal <> ($aplicacionesTotal + $interesesTotal + $pagoACuenta))
     */
    if (in_array($comprob_id, array(
        Comprob::RECIBO_VENTAS, Comprob::RECIBO_ELECTRONICO, Comprob::OP, Comprob::RECIBO_BANCO
      ))) {
      if (round($docTotal, 2) != round($aplicacionesTotal + $interesesTotal + $pagoACuenta, 2)) {
        $errores["total"] = array(
          "El total del documento($docTotal) debe ser igual a la suma de los pagos
              ($aplicacionesTotal) + los intereses($interesesTotal) + el pago a cuenta($pagoACuenta)"
        );
      }
    }
    $docTotal = round($docTotal, 2);
    $valoresTotal = round($valoresTotal, 2);
    /*
     * $docTotal <> ($valoresTotal)
     */
    if (in_array($comprob_id, array(
        Comprob::AJUSTE_CAJA_ENTRADA,
        Comprob::AJUSTE_CAJA_SALIDA,
        Comprob::RECIBO_VENTAS,
        Comprob::RECIBO_ELECTRONICO,
        Comprob::OP,
      ))) {
      if (($docTotal + 0) !== ($valoresTotal + 0)) {
        $dif = $docTotal - $valoresTotal;
        $errores["total de pagos"] = array("El total de pagos($valoresTotal) debe ser igual al total del documento($docTotal), diferencia = $dif");
      }
    }
    /*
     * Destino_id
     */
    if (in_array($comprob_id, array(
        Comprob::MOVIMIENTO_BANCARIO_D,
        Comprob::MOVIMIENTO_BANCARIO_H,
      ))) {
      if (!isset($post['doc']['destino_id']) or $post['doc']['destino_id'] == "") {
        $errores["destino_id"] = array("Debe ingresar una cuenta para este documento");
      }
    }
    /*
     * $articulosTotal < $docTotal
     */
    if (in_array($comprob_id, array(
        Comprob::MOVIMIENTO_BANCARIO_D,
        Comprob::MOVIMIENTO_BANCARIO_H,
      ))) {
      if ($articulosTotal < $docTotal) {
        $errores["suma de los importes de los artículos"] = array("Debe ser igual al total del documento");
      }
    }
    /*
     * Detalle
     */
    if (in_array($comprob_id, array(
        Comprob::MOVIMIENTO_BANCARIO_D,
        Comprob::MOVIMIENTO_BANCARIO_H,
        Comprob::AJUSTE_CAJA_ENTRADA,
        Comprob::AJUSTE_CAJA_SALIDA,
        //Comprob::ND_VENTAS,
        //Comprob::NC_VENTAS,
      ))) {
      if (!isset($post['doc']['detalle']) or trim($post['doc']['detalle']) == "") {
        $errores["detalle"] = array("Debe ingresar un detalle para este documento");
      }
    }

    return $errores;
  }

  public function grabaDetalle($detalles) {
    foreach ($detalles as $detalle) {
      if ($detalle) {
        $docDet = new DocDet();
        $docDet->Doc_id = $this->id;
        $docDet->setAttributes($detalle);
        if (!$docDet->save()) {
          return $docDet->errors;
        }
      }
    }

    return null;
  }

  public function grabaValores($valores, $destino_id = null, $fecha = null) {
    $fecha = $fecha ? $fecha : new CDbExpression('NOW()');
    $errores = array();
    foreach ($valores as $itempago) {
      foreach ($itempago as $valor) {
        if (isset($valor["cheques"])) {
          $cheques = explode(",", $valor["cheques"]);
          $errores["cheque-salen-de-cartera"] = array();
          foreach ($cheques as $chq) {
            if ($chq) {
              $dv = new DocValor();
              $errores["cheque-salen-de-cartera"][] = $dv->salidaCheque($chq, $this, $destino_id);
            }
          }
          if (count($errores["cheque-salen-de-cartera"])) {
            unset($errores["cheque-salen-de-cartera"]);
          }
        } elseif (isset($valor["cupones"])) {
          $cupones = explode(",", $valor["cupones"]);
          $json = json_encode($cupones);
          $phpStringArray = str_replace(array(
            "{", "}", ":"
            ), array(
            "array(", "}", "=>"
            ), $json);
          $phpStringArray = str_replace("[", "(", $phpStringArray);
          $phpStringArray = str_replace("]", ")", $phpStringArray);

          $errores["cupones-salen-de-caja"] = array();
          foreach ($cupones as $cupon) {
            if ($cupon) {
              $dv = new DocValor();
              $errores["cupones-salen-de-caja"][] = $dv->salidaCupon($cupon, $this, $destino_id);
            }
          }
          if ($cupones) {

            $this->altaCobrotarjetas($phpStringArray, $this->concepto_id);
          }
          if (count($errores["cupones-salen-de-caja"])) {
            unset($errores["cupones-salen-de-caja"]);
          }
        } elseif (isset($valor["retenciones"])) {
          $retenciones = explode(",", $valor["retenciones"]);
          $json = json_encode($retenciones);
          $phpStringArray = str_replace(array(
            "{", "}", ":"
            ), array(
            "array(", "}", "=>"
            ), $json);
          $phpStringArray = str_replace("[", "(", $phpStringArray);
          $phpStringArray = str_replace("]", ")", $phpStringArray);

          $errores["retenciones-salen-de-caja"] = array();
          foreach ($retenciones as $retencion) {
            if ($retencion) {
              $dv = new DocValor();
              $errores["retenciones-salen-de-caja"][] = $dv->salidaRetencion($retencion, $this, $destino_id);
            }
          }
          if ($retenciones) {

//                        $this->altaCobrotarjetas($phpStringArray, $this->concepto_id);
          }
          if (count($errores["retenciones-salen-de-caja"])) {
            unset($errores["retenciones-salen-de-caja"]);
          }
        } else {
          $dv = new DocValor();
          $dv->Doc_id = $this->id;
          $dv->setAttributes($valor);
          if (isset($valor["fecha"])) {
            $dv->fecha = Helpers::fechaParaGrabar($valor["fecha"]);
          } else {
            $dv->fecha = $fecha;
          }

          if ($destino_id) {
            $dv->Destino_id = $destino_id;
          }
        }
        if (isset($valor["chequera_id"]) and ( $valor["chequera_id"] !== null)) {
          $ch = Chequera::model()->findByPk($valor["chequera_id"]);
          $ch->siguiente_numero = $ch->siguiente_numero + 1;
          $ch->save();
        }

        if (!$dv->save()) {
          $errores[$dv->nombre] = $dv->errors;
        }
      }
    }

    return $errores;
  }

  static public function altaCobrotarjetas($cupones_a_depositar, $concepto_id) {
    $regs = Helpers::qryAll("select dv.lote, dv.tarjeta_plan_id, sum(dv.importe) as total, tp.dias_al_cobro, t.destino_id, t.nombre, tp.nombre as plan
                                        from doc_valor dv
                                        inner join tarjeta_plan tp on tp.id = dv.tarjeta_plan_id
                                        inner join tarjeta t on t.id = tp.tarjeta_id
                                        where dv.id in $cupones_a_depositar
                                        group by  dv.lote, dv.tarjeta_plan_id, tp.dias_al_cobro, t.destino_id, t.nombre, tp.nombre"
    );


    foreach ($regs as $reg) {
      $r = new Doc("insert", Comprob::DEPOSITO_BANCARIO_ENTRADA);
      $r->total = $reg['total'];
      $r->saldo = 0;
      $r->concepto_id = $concepto_id;
      $r->detalle = "Acred. Tarj. " . $reg['nombre'] . " " . $reg['plan'] . " Lote " . $reg['lote'];
      if (!$r->save()) {
        echo(json_encode($r->errors));
        die;
      }

      $dv = new DocValor();
      $dv->Doc_id = $r->id;
      $dv->fecha = Helpers::fechaParaGrabar(Helpers::getDiasHabiles(date("d/m/Y", time()), $reg['dias_al_cobro']));
      $dv->Destino_id = $reg['destino_id'];
      $dv->importe = $reg['total'];
      $dv->tipo = DocValor::TIPO_COBRO_TARJETAS;

      if (!$dv->save()) {
        echo(json_encode($r->errors));
        die;
      }
    }

    return $r;
  }

  public function grabaIntereses($intereses) {
    foreach ($intereses as $id => $data) {
      $nd = new Doc("insert", Comprob::ND_VENTAS);
      $nd->Socio_id = $this->Socio_id;
      $nd->total = $data["saldo"];
      $nd->saldo = $data["saldo"];
      $nd->detalle = $data["detalle"];
      $nd->concepto_id = 1919; //@todo: el concepto para nd por cheque tiene que ir en alguna configuraciÃ³n
      $nd->doc_id = $this->id;
      if (!$nd->save()) {
        return $nd->errors;
      }
      DocApl::Nueva(array(
        "origen" => $this->id,
        "destino" => $nd->id,
        "importe" => $data["valor"],
        "docAplCabId" => null,
      ));
    }
  }

  public function grabaAplicaciones($aplicaciones) {
    $errors = null;
    if ($aplicaciones and is_array($aplicaciones)) {
      $this->save();
      foreach ($aplicaciones as $id => $valor) {
        if ($valor) {
          if (!$id or ! $this->id) {
            $errors = array("Error en la aplicación origen:$this->id destino:$id valor:$valor");
          } else {
            $errors = DocApl::Nueva(array(
                "origen" => $this->id,
                "destino" => $id,
                "importe" => $valor,
                "docAplCabId" => null,
            ));
          }
          if ($errors) {
            return $errors;
          }
        }
      }
      $this->refresh();
    } elseif ($aplicaciones and $aplicaciones == 'auto') {
      // comienzo
      $ret = new stdClass();
      $socio_id = $this->Socio_id;
      // die;
      $docsCredito = Helpers::qryAll("
            select d.id, d.saldo, d.numero, c.nombre
            from socio s
              inner join alumno a on a.id = s.alumno_id
              inner join doc d on d.Socio_id = s.id
              inner join talonario t on t.id = d.talonario_id
              inner join comprob c on c.id = t.comprob_id
            where s.id = $socio_id and c.signo_cc = -1 and
              d.anulado <> 1 and d.activo = 1 and
              d.saldo > 0
            order by d.id asc
            ");
      $docsDebito = Helpers::qryAll("
            select d.id, d.saldo, d.numero, c.nombre
            from socio s
              inner join doc d on d.Socio_id = s.id
              inner join talonario t on t.id = d.talonario_id
              inner join comprob c on c.id = t.comprob_id
            where s.id = $socio_id and c.signo_cc = 1 and
              d.anulado <> 1 and d.activo = 1 and
              d.saldo > 0
            order by d.id asc
            ");
      $ret->docsCredito = $docsCredito;
      $ret->docsDebito = $docsDebito;
      $alu = Helpers::qry("select a.matricula, a.nombre, a.apellido from alumno a inner join socio s on s.alumno_id = a.id and s.id = $socio_id");
      // echo "<div><strong>".$alu['apellido']. ' ' . $alu['nombre']. "</strong></div>";
      foreach ($docsCredito as $kc => $dc) {
        //        while ($dc["saldo"] > 0) {
        foreach ($docsDebito as $kd => $dd) {
          if ($dc["saldo"] === 0 or $dd["saldo"] === 0) {
            continue;
          }
          $origen = $dc["nombre"].' #'. $dc['numero'] .' con saldo de  $'. $dc['saldo'];
          $destino= $dd["nombre"].' #'. $dd['numero'] .' con saldo de  $'. $dd['saldo'];
          if ($dc["saldo"] >= $dd["saldo"]) {
            $dc["saldo"] -= $dd["saldo"];
            $docsCredito[$kc]["saldo"] -= $dd["saldo"];
            $docsDebito[$kd]["saldo"] = 0;
            $importe = $dd["saldo"];
            $dd["saldo"] = 0;
          } else {
           $dd["saldo"] -= $dc["saldo"];
            $docsDebito[$kd]["saldo"] -= $dc["saldo"];
            $docsCredito[$kc]["saldo"] = 0;
            $importe = $dc["saldo"];
            $dc["saldo"] = 0;
          }
            // echo "
            //   <div style='margin-left:20px'>
            //         app:$origen sobre $destino 
            //   </div>
            // ";
          
          Helpers::qryExec("
                INSERT INTO
                doc_apl(Doc_id_origen, doc_id_destino, importe)
                VALUES (:origen, :destino, :importe)
                ", array(
            'origen' => $dc["id"],
            'destino' => $dd["id"],
            'importe' => $importe,
          ));
          Helpers::qryExec("
                update doc set saldo = " . $dc["saldo"] . "
                  where id = " . $dc["id"]
          );
          Helpers::qryExec("
                update doc set saldo = " . $dd["saldo"] . "
                  where id = " . $dd["id"]
          );
        }
        //      }
      }
      //fin
    }
  }

  private static function grabaDocAfip($doc) {
    $talonario_id = $doc->talonario_id;
    $comprob_afip_id = Helpers::qryScalar("select afip_comprob
      from comprob c
        inner join talonario t on t.comprob_id = c.id and t.id = $talonario_id");
    //$mode            = (isset($_GET["mode"]) and $_GET["mode"] === "prod") ? Afip::MODO_PROD : Afip::MODO_DEV;
    $mode = Afip::MODO_PROD;
    $afip = new Afip($mode, $comprob_afip_id);
    $resp = $doc->alumno->getResponsable();
    //vd($resp);
    $afip->setDocProp("tipo_doc", Afip::getTipoDoc($resp->tipo_documento_id));
    $afip->setDocProp("nro_doc", $resp->numero_documento);
    $afip->setDocProp("importe", $doc->total);

    if (!$afip->mandaDoc()) {
      echo "Error conectando (supongo) con el AFIP!!!! avisen men!";
      die;
    }
    $afip->grabaFactura($doc);
    //$afip->barras();
//    $datosCae               = new stdClass();
//    $datosCae->tipo         = $afip->doc["emision_tipo"];
//    $datosCae->cae          = $afip->doc["cae"];
//    $datosCae->err_code     = $afip->doc["err_code"];
//    $datosCae->err_msg      = $afip->doc["err_msg"];
//    $datosCae->fch_venc_cae = $afip->doc["fch_venc_cae"];
//    $datosCae->motivos_obs  = $afip->doc["motivos_obs"];
//    $datosCae->numero       = $afip->doc["cbt_desde"];
//    $datosCae->sucursal     = $afip->doc["punto_vta"];
    return $afip->doc;
  }

  /*
   *    Alta del documento completo
   */

  //@todo:que no permita grabar un documento con el mismo socio + mismo nÃºmero y misma sucursal

  static public function altaTransferenciaDeposito($doc) {
    //Alta de Transferencia.
    //verifica que este todo ok
    $tr = Yii::app()->db->beginTransaction();
    $retorno = new stdClass();
    $retorno->error = array();
    if (($doc['doc']['detalle'] == '') or ( $doc['doc']['importe'] == '')) {
      $retorno->error[] = 'Error, datos sin completar';
    }
    if (!isset($doc["doc"]["destino_id"])) {
      $retorno->error[] = 'Error, debe especificar el destino del depÃ³sito/tranferencia';
    }
    if (!isset($doc['valores'])) {
      $retorno->error[] = 'Error, no hay cargados valores';
    }
    //Verifica que el total de valores sea igual al importe cargado
    $total1 = 0;
    foreach ($doc['valores'] as $itempago) {
      foreach ($itempago as $id => $valor) {
        $total1 = $total1 + $valor['importe'];
      }
    }
    if ($total1 != $doc['doc']['importe']) {
      $retorno->error[] = 'Error, Los totales no coinciden';
    }

    // SalÃ­da de caja
    $r = new Doc("insert", $doc['comprob_tipo_id']);
    $r->total = $doc['doc']['importe'];
    $r->saldo = 0;
    $r->detalle = $doc['doc']['detalle'];

    if (!$r->save()) {
      echo(json_encode($r->errors));
      die;
    }

    $r->grabaValores($doc['valores']);

    if (!$r->save()) {
      echo(json_encode($r->errors));
      die;
    }

    //Entrada al Banco / Caja
    $r = new Doc("insert", $doc['comprob_tipo_id_entrada']);
    $r->total = $doc['doc']['importe'];
    $r->saldo = 0;
    $r->detalle = $doc['doc']['detalle'];

    if (!$r->save()) {
      echo(json_encode($r->errors));
      die;
    }
    $r->grabaValores($doc['valores'], $doc["doc"]["destino_id"]);

    if (!$r->save()) {
      echo(json_encode($r->errors));
      die;
    }

    if (count($retorno->error) > 0) {
      echo json_encode($retorno);
      die;
    }
    echo json_encode("");
    $tr->commit();

    return $r;
  }

  public function afterFind() {
    $this->oldRecord = clone $this;

    return parent::afterFind();
  }

  public function scopes() {
    $alias = $this->getTableAlias(false, false);

    return CMap::mergeArray(parent::scopes(), array(
        "todos" => array(
          "condition" => "$alias.anulado is null or anulado = 1",
        ),
        "activos" => array(
          "condition" => "$alias.anulado is null or anulado = 0",
        ),
    ));
  }

  public function rules() {
    return array_merge(parent::rules(), array(
      array('numero', 'numeroValidator'),
    ));
  }

  function numeroValidator() {
    if (!$this->talonario->numeracion_manual) {
      return true;
    }

    if (!is_numeric(substr($this->numero, -8)) or substr($this->numero, -8) <= 0) {
      //if (!is_numeric($this->numero) or $this->numero <= 0) {
      $this->addError("numero", $this->numero . " $this->numero Numero debe ser mayor que 0");
    }

    return false;
  }

  public function __get($name) {
    switch ($name) {
      case $name == "detalleAlt":
        if ($this->detalle) {
          return $this->detalle;
        } else {
          $ret = "";
          foreach (Yii::app()->db->createCommand("
                            select a.nombre
                              from doc_det dd
                                inner join articulo a on a.id = dd.articulo_id
                              where dd.Doc_id = $this->id
                            ") as $detalle) {
            $this->detalle .= trim($detalle, 0, 15) . "/";
          }
          $ret .= substr($this->detalle, -1);
          $ret .= substr($ret, 0, 25);
        }

        return $ret;
        break;
      case $name == "numeroCompleto":
        if ($this->numero) {
          //return str_pad($this->talonario->punto_venta_numero, 4, "0", STR_PAD_LEFT) . "-" . str_pad($this->numero, 8, "0", STR_PAD_LEFT);/
          return str_pad($this->sucursal, 4, "0", STR_PAD_LEFT) . "-" . str_pad($this->numero, 8, "0", STR_PAD_LEFT);
        }
        break;
      case $name == "siguienteNumero":
        if (!$this->talonario) {
          Helpers::error("Talonario no asignado");
        }
        return $this->talonario->numero + 1;
        break;
      //      case $name == "numero":
      //        return sprintf('%0' . (int) 8 . 's', parent::__get($name));
      //        break;
      //      case $name == "sucursal":
      //        return sprintf('%0' . (int) 4 . 's', parent::__get($name));
      //        break;
      case $name == "tieneCheques":
        return Yii::app()->db->createCommand("
                    select dv.id
                      from doc_valor dv
                      where dv.Doc_id = $this->id
                      limit 1
                  ")->queryScalar();
        break;
      case $name == "tieneCupones":
        return Yii::app()->db->createCommand("
                    select dv.id
                      from doc_valor dv
                      where dv.Doc_id = $this->id and dv.tipo = 2
                      limit 1
                  ")->queryScalar();
        break;
      case $name == "tieneAplicaciones":
        return Yii::app()->db->createCommand("
                    select a.id
                      from doc_apl a
                      where a.doc_id_origen = $this->id or a.doc_id_origen = $this->id
                      limit 1
                  ")->queryScalar();
        break;
      case $name == "cajaCerrada":
        return Yii::app()->db->createCommand("
                    select di.fecha_cierre from destino_instancia di where di.id =
                    (select v.Destino_Instancia_id from doc_valor v where v.doc_id = $this->id limit 1)
                 ")->queryScalar();
        break;
      case ($name == "numeroCompleto"):
        return sprintf('%0' . (int) 4 . 's', $this->sucursal) . "-" . sprintf('%0' . (int) 8 . 's', $this->numero);
        break;
      default:
        return parent::__get($name);
    }
  }

  // fin graba doc

  public function beforeValidate() {
    $comprob_id = $this->talonario->comprob_id;
    $arNumero = explode("-", $this->numero);
    if (count($arNumero) == 2) {
      $this->sucursal = $arNumero[0];
      $this->numero = $arNumero[1];
    } else {
      $this->sucursal = 1;
    }
    if ($this->isNewRecord and ! $this->afip) {
//      vd("SELECT count(*) FROM doc d
//                    WHERE d.anulado = 0 AND d.numero = $this->numero AND
//                          d.sucursal = $this->sucursal AND
//                          talonario_id = $this->talonario_id AND
//                          d.Socio_id = $this->Socio_id
//                ");
      $existe = Helpers::qryScalar("
                SELECT count(*) FROM doc d
                    WHERE d.anulado = 0 AND d.numero = :numero AND d.sucursal = :sucursal AND talonario_id = :talonario_id AND d.Socio_id = :socio_id
                ", array(
          "numero" => $this->numero, "sucursal" => $this->sucursal,
          "talonario_id" => $this->talonario_id,
          "socio_id" => $this->Socio_id
      ));
      if ($existe > 0) {
        $this->addError("Número", "El número de comprobante ya existe");
      }
    }
    if (!$this->talonario) {
      $this->addError("talonario_id", "Debe ingresar un talonario vÃ¡lido");
    }
    /*
     * Concepto_id
     */
    if (in_array($comprob_id, array(
        Comprob::MOVIMIENTO_BANCARIO_D,
        Comprob::AJUSTE_CAJA_ENTRADA,
        Comprob::AJUSTE_CAJA_SALIDA,
        Comprob::ND_VENTAS,
        Comprob::NC_VENTAS,
        Comprob::FACTURA_VENTAS,
        Comprob::FACTURA_COMPRAS,
      ))) {
      if (!$this->concepto_id) {
        $this->addError("concepto_id", "Debe ingresar un concepto para este documento");
      }
    }

    //    if (in_array($comprob_id, array(
    //                Comprob::MOVIMIENTO_BANCARIO
    //            ))) {
    //      if (!$this->destino_id) {
    //        $this->addError("Destino_id", "Debe ingresar una cuenta para este documento");
    //      }
    //    }

    /*
     * Socio
     */
    if (in_array($comprob_id, array(
        Comprob::FACTURA_COMPRAS,
        Comprob::FACTURA_VENTAS,
        Comprob::RECIBO_VENTAS,
        Comprob::RECIBO_ELECTRONICO,
        Comprob::RECIBO_BANCO,
        Comprob::OP,
        Comprob::NC_COMPRAS,
        Comprob::NC_VENTAS,
        Comprob::ND_VENTAS,
        Comprob::ND_COMPRAS,
      ))) {
      if (!$this->Socio_id or ! Socio::model()->exists("id = $this->Socio_id")) {
        $this->addError("Socio_id", "Debe especificar cliente/proveedor válido");
      }
    }

    return parent::beforeValidate();
  }

  public function beforeSave() {
    if ($this->isNewRecord) {
      $this->saldo = ($this->saldo == 0) ? $this->total : $this->saldo;
      if (!$this->talonario->numeracion_manual and ! $this->afip) {
        if ($this->talonario->numero <= $this->numero) {
          $this->talonario->numero = $this->numero;
          $this->talonario->save();
        }
      }
    } else {
      $this->fecha_modificacion = new CDbExpression('NOW()');
      $this->_total_anterior = $this->total;
    }
    $this->user_id = Yii::app()->user->id;
    if (parent::beforeSave()) {
      return Helpers::validateDates($this);
    } else {
      return false;
    }
  }

  public function afterSave() {
    if ($this->Socio_id) {
      if ($this->oldRecord->anulado and ! $this->anulado) { //desanula
        $this->socio->saldo += ($this->total * $this->talonario->comprob->signo_cc);
      } else {
        if (!$this->oldRecord->anulado and $this->anulado) { //anula
          $this->socio->saldo -= ($this->total * $this->talonario->comprob->signo_cc);
        } else {
          $this->socio->saldo -= ($this->oldRecord->total * $this->talonario->comprob->signo_cc);
          $this->socio->saldo += ($this->total * $this->talonario->comprob->signo_cc);
        }
      }
      $this->socio->saldo = round($this->socio->saldo, 2);
    }
    if ($this->socio and ! $this->socio->save()) {
      $socio_id = $this->socio->id;
      $nombre = Yii::app()->db->createCommand("
					select concat(a.apellido, ', ', a.nombre) as nombre
							from alumno a
									inner join socio s on s.Alumno_id = a.id
							where s.id = $socio_id
					")->queryScalar();
      Helpers::error("Error grabando el socio($nombre) en el aftersave de doc", array_merge($this->socio->attributes, $this->socio->errors), true);
      die;
    }

    return parent::afterSave();
  }

  public function anula($forceCajaCerrada = true) {
    if ($this->cajaCerrada) {
      if ($forceCajaCerrada) {
        //si es una caja cerrada y $force = true entonces:
      } else {
        throw new Exception("No se puede anular, el documento pertenece a una caja cerrada");
      }
    }
    if ($this->tieneAplicaciones) {
      $this->desAplica();
    }
    $this->liberaChequesYCupones();

    //			if ($this->tieneCupones) {
    //				$this->anulaCupones();
    //			}
    //			//Tiene Cheques me parece que no hace nada, trae doc_valor sea el valor que sea.
    //			if ($this->tieneCheques) {
    //				$this->liberaCheques();
    //			}
    $this->anulado = 1;
    $this->save();
  }

  public function desAplica() {
    $aplicaciones = Yii::app()->db->createCommand("
              select a.id from doc_apl a where a.doc_id_origen = $this->id
            ")->queryAll();
    foreach ($aplicaciones as $a) {
      $docApl = DocApl::model()->findByPk($a["id"]);
      $docApl->delete();
    }

    return true;
  }

  private function liberaChequesYCupones() {
    Yii::app()->db->createCommand("
				/* primero libero los cupones de este doc*/
				update doc_valor dv
					INNER JOIN
							(SELECT dv.doc_valor_id FROM doc d
								INNER JOIN doc_valor dv ON dv.Doc_id = d.id
							WHERE d.id = $this->id) AS dest ON dest.doc_valor_id = dv.id
					SET dv.cupon_estado = null, dv.doc_valor_id = null;
				/* luego los cheques */
				update doc_valor set chq_estado = null where doc_id = $this->id;
				/* luego desvinculo los valores que apuntaban acá, creo que es por los cheques */
				update doc_valor set doc_valor_id=null where doc_id = $this->id;
			")->execute();
  }

  public function getAlumno() {
    return Alumno::model()->findBySql("
      SELECT a.* FROM doc d
        INNER JOIN socio s ON d.Socio_id = s.id
        INNER JOIN alumno a ON s.Alumno_id = a.id
      where d.id = $this->id
  ");
  }

  //		private function liberaCheques() {
  //			Yii::app()->db->createCommand("
  //                update doc_valor set chq_estado = null, doc_valor_id=null where doc_id = $this->id;
  //            ")->execute();
  //		}
  //
  //		private function anulaCupones() {
  //
  //			Yii::app()->db->createCommand("
  //				update doc_valor dv
  //					INNER JOIN
  //							(SELECT dv.doc_valor_id FROM doc d
  //								INNER JOIN doc_valor dv ON dv.Doc_id = d.id
  //							WHERE d.id = $this->id) AS dest ON dest.doc_valor_id = dv.id
  //					SET dv.cupon_estado = null
  //			")->execute();
  //		}
}
