<?php

Yii::import('application.models._base.BaseDocApl');

class DocApl extends BaseDocApl {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeSave() {
        if ($this->importe <= 0) {
            $this->addError("importe", "El importe no puede ser 0 o menor");
          throw new Exception("docaplbefsaveEl importe no puede ser 0 o menor");
            return false;
        }
        $docOrigen = Doc::model()->findByPk($this->Doc_id_origen);
        $docDest = Doc::model()->findByPk($this->doc_id_destino);
        $docOrigen->saldo = round($docOrigen->saldo - $this->importe, 2);
        $docDest->saldo = round($docDest->saldo - $this->importe,2);
        if (!$docOrigen) {
            throw new Exception("no exixte doc origen ($this->Doc_id_origen)");
        }
        if (!$docDest) {
            throw new Exception("no exixte doc destino ($this->doc_id_destino)");
        }
        if (!$docOrigen->save()) {
            vd("Error grabando docOrigen en beforeSave de DocApl",$docOrigen->errors, $docOrigen->attributes);
        }
        if (!$docDest->save()) {
            vd("Error grabando docDest en beforeSave de DocApl",$docDest->errors, $docDest->attributes);
        }
//      vd($docOrigen->attributes, $docDest->attributes);
//      ve($this->attributes, $docOrigen->attributes, $docDest->attributes);
      $docOrigen = Doc::model()->findByPk($this->Doc_id_origen);
        return parent::beforeSave();
    }

    public function beforeDelete() {
        $docOrigen = Doc::model()->findByPk($this->Doc_id_origen);
        $docDest = Doc::model()->findByPk($this->doc_id_destino);
        $docOrigen->saldo += $this->importe;
        $docDest->saldo += $this->importe;
        $docOrigen->save();
        $docDest->save();
        return parent::beforeDelete();
    }

    public static function nueva(array $params) {
        if ($params["importe"] <= 0) {
            return null;
        }
        $apl = new DocApl();
        $apl->Doc_id_origen = $params["origen"];
        $apl->doc_id_destino = $params["destino"];
        $apl->importe = $params["importe"];
        if (isset($params["docAplCabId"])) {
            $apl->doc_apl_cab_id = $params["docAplCabId"];
        }
        if (!$apl->save()) {
            return($apl->errors);
        } else {
            return null;
        }
    }

    public static function borra($app_id) {
        $apl = DocApl::model()->findByPk($app_id);
        $apl->delete();
    }

}
