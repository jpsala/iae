<?php

    Yii::import('application.models._base.BaseDocValor');

    class DocValor extends BaseDocValor
    {

        const TIPO_EFECTIVO = 0;
        const TIPO_CHEQUE = 1;
        const TIPO_TARJETA = 2;
        const TIPO_CHEQUE_PROPIO = 3;
        const TIPO_RECAUDACION_BANCARIA = 4;
        const TIPO_TRANSFERENCIA = 5;
        const TIPO_COBRO_TARJETAS = 6;
        const TIPO_RETENCION = 7;

        public static function model($className = __CLASS__) {
            return parent::model($className);
        }

        public function rules() {
            $parentRules = parent::rules();

            //    $parentRules["Destino_Instancia_id"] = array('Destino_Instancia_id', 'required');
            return $parentRules;
        }

        public function attributeLabels() {
            $at                         = parent::attributeLabels();
            $at["Destino_Instancia_id"] = "Caja abierta";

            return $at;
        }

        public function beforeValidate() {
            if($this->doc->talonario_id) {
                if(!$this->Destino_Instancia_id) {
                    $this->Destino_Instancia_id = Destino::getActivo();
                    if(!$this->Destino_Instancia_id) {
                        $this->addError("Valores", "La caja está cerrada");
                    }
                }
            } else {
                $this->addError("Valores", "El documento no tiene asigando un comprobante");

                return $bd;
            }

            if(!$this->doc) {
                if(!$this->Destino_Instancia_id) {
                    $this->addError("Caja", "No puede estar cerrada");

                    return $bd;
                }
            }

            $bv = parent::beforeValidate();

            if(!$this->tipo or ($this->tipo == DocValor::TIPO_EFECTIVO)) {
                $this->tipo      = DocValor::TIPO_EFECTIVO;
                $this->moneda_id = Moneda::model()->find()->id;
            }
            if($this->tipo == DocValor::TIPO_CHEQUE) {
                $banco       = Banco::model()->findByPk($this->banco_id);
                $descripcion = $banco ? $banco->nombre : $this->numero;
                if(!$this->numero) {
                    $this->addError("numero", "Falta ingresar el número de cheque");
                }
                if(!$this->banco_id) {
                    $this->addError("banco_id", "Falta ingresar el banco del cheque $descripcion");
                }
                if(!$this->chq_cuit_endosante) {
                    //@todo: para despues es obligatorio
                    //$this->addError("chq_cuit_endosante", "Falta ingresar el cuit del endosante del cheque $descripcion");
                }
                if(!$this->chq_origen) {
                    $this->addError("chq_origen", "Falta ingresar el origen del cheque $descripcion");
                }
                if(!$this->fecha) {
                    //$this->addError("fecha", "Falta ingresar la fecha de cobro del cheque $descripcion");
                }
            elseif($this->tipo == DocValor::TIPO_RETENCION) {

						}
            } else {
                if($this->tipo == DocValor::TIPO_TARJETA) {
                    $tarjeta = isset($this->tarjeta_id) ? Tarjeta::model()->findByPk($this->tarjeta_id)->nombre : "";
                    if(!$this->tarjeta_plan_id) {
                        $this->addError("tarjeta_plan_id", "Falta ingresar el plan de la tarjeta");
                    }
                    if(!$this->numero) {
                        // Me pidió Lujan que este dato se opcional porque carga varios cupones en la misma salida
                        //@todo: agragar cuando lujan empiece a carga los cupones de a uno
                        //$this->addError("numero", "Falta ingresar el número de cupón de $tarjeta");
                    }
                    if(!$this->lote) {
                        $this->addError("lote", "Falta ingresar el lote de $tarjeta");
                    }
                } else {
                    if($this->tipo == DocValor::TIPO_CHEQUE_PROPIO) {
                        $chequera = Chequera::model()->findByPk($this->chequera_id);
                        if(!$this->chequera_id) {
                            $this->addError("", "Falta ingresar la chequera");
                        }
                        if(!$this->numero) {
                            $this->addError("numero", "Falta ingresar el numero de cheque de $chequera->descripcion");
                        }
                        if(!$this->fecha) {
                            $this->addError("fecha", "Falta ingresar la fecha de cobro de $chequera->descripcion");
                        }
                        if(!$this->chq_entregado_a) {
                            $this->addError("chq_entregado_a", "Falta ingresar el destino del cheque de $chequera->descripcion");
                        }
                        $this->Destino_id = $chequera ? $chequera->destino_id : null;
                    } else {
                        if(($this->tipo == DocValor::TIPO_RECAUDACION_BANCARIA) or ($this->tipo == DocValor::TIPO_TRANSFERENCIA)) {
                            $destino   = Destino::model()->findByPk($this->Destino_id);
                            $operacion = $this->tipo == DocValor::TIPO_RECAUDACION_BANCARIA ? "recaudación bancaria" : "transferencia";
                            if(!$this->Destino_id) {
                                $this->addError("Destino_id", "Falta ingresar la cuenta bancaria de la $operacion");
                            }
                            if(!$this->fecha) {
                                $this->addError("fecha", "Falta ingresar la fecha de la $operacion de $destino->nombre");
                            }
                            if($this->tipo == DocValor::TIPO_TRANSFERENCIA) {
                                if(!$this->numero) {
//                                    $this->addError("numero", "Falta ingresar el numero de la $operacion de $destino->nombre");
                                }
                            }
                        }
                    }
                }
            }
            if(!$this->importe or $this->importe < 0) {
                $this->addError($this->nombre, "El importe debe ser mayor a 0");
            }

            return $bv;
        }

        public function beforeSave() {
            //ve($this->attributes);
            if($this->importe <= 0) {
                $this->addError("importe", "El importe no puede ser 0 o menor");

                return false;
            }

            $this->importe = round($this->importe, 2);
            //@todo: Revisar las líneas de abajo, actualiza saldo en documentos a excepción de recibo y op
            if(!in_array($this->doc->talonario->comprob_id, array(
                Comprob::RECIBO_BANCO,
                Comprob::RECIBO_VENTAS,
                Comprob::RECIBO_ELECTRONICO,
                Comprob::OP,
            ))
            ) {
                $doc = $this->doc;
                $doc->saldo -= $this->importe;
                $doc->save();
            }
            /*
             * Saco la actualización del número del siguiente cheque en la chequera
             * y pruebo de llevarlo a otro lado
             */
            //        if ($this->chequera_id !== null) {
            //            $ch = Chequera::model()->findByPk($this->chequera_id);
            ////            $log = new Log();
            ////            $log->texto = var_export(
            ////                    array("Antes", "Doc", $doc->id,
            ////                    "Chequera", json_encode($ch->attributes)), true);
            ////            if (!$log->save()) {
            ////                vd($log->errors);
            ////            }
            //            $ch->siguiente_numero = $ch->siguiente_numero + 1;
            //            $ch->save();
            ////            $log = new Log();
            ////            $log->texto = var_export(
            ////                    array("Despues", "Doc", $doc->id,
            ////                    "Chequera", json_encode($ch->attributes)), true);
            ////            $log->save();
            ////            if (!$log->save()) {
            ////                vd($log->errors);
            ////            }
            //        }


            if(parent::beforeSave()) {
                return Helpers::validateDates($this);
            } else {
                return false;
            }
        }

        public function __get($name) {

            if($name == "destino") {

            } else {
                return parent::__get($name);
            }
        }

        public function getNombre() {
            switch ($this->tipo) {
                case DocValor::TIPO_EFECTIVO:
                    return "Efectivo";
                    break;
                case DocValor::TIPO_CHEQUE:
                    return "Cheque";
                    break;
                case DocValor::TIPO_TARJETA:
                    return "Tarjeta";
                    break;
                case DocValor::TIPO_TRANSFERENCIA:
                    return "Transferencia";
                    break;
                case DocValor::TIPO_RECAUDACION_BANCARIA:
                    return "Recaudación";
                    break;
                case DocValor::TIPO_CHEQUE_PROPIO:
                    return "Cheque Propio";
                    break;
                default:
                    break;
            }
        }

        public function chequeEntregadoA($doc_id) {
            $doc = Doc::model()->findByPk($doc_id);
            if($doc->Socio_id) {
                return socio::model()->findbyPk($doc->Socio_id)->GetNombre();
            } else {
                return $doc->detalle;
            }
        }

        public function salidaCheque($chq_id, $doc, $destino_id) {
            //chq_estado null = en cartera
            //chq_estado 1 = entregado
            //chq_estado 2 = entregado pero se le pone otro estado porque se reemplaza el moviemiento con otro chq
            $valor = DocValor::model()->findByPk($chq_id);
            $this->setAttributes($valor->attributes);
            $this->Destino_id           = $destino_id;
            $this->doc_valor_id         = $valor->id;
            $this->tipo                 = DocValor::TIPO_CHEQUE;
            $this->Doc_id               = $doc->id; //lo asigno así porque sino toma el de setAttributes
            $this->Destino_Instancia_id = Destino::getActivo();
            $this->obs                  = $doc->detalle;
            if(($doc->talonario->comprob_id == Comprob::DEPOSITO_BANCARIO_SALIDA) || ($doc->talonario->comprob_id == Comprob::TRANSFERENCIA_ENTRADA)) {
                $this->chq_estado = 2;
            } else {
                $this->chq_estado = 1;
            }
            $valor->chq_estado     = 2;
            $this->chq_entregado_a = $this->chequeEntregadoA($doc->id);
            if(!$this->save()) {
                $this->addError("docValor en salidaCheque", $this->errors);

                return $this->errors;
            }
            if(!$valor->save()) {
                $this->addError("docValor en salidaCheque", $this->errors);

                return $this->errors;
            }
            //    var_dump("valor",$valor->attributes);
            //    var_dump("this",$this->attributes);
        }

        public function salidaCupon($cupon, $doc, $destino_id) {
            //cupon_estado null = en caja
            //cupon_estado 1 = cerrado
            //cupon_estado 2 = cerrado pero se le pone otro estado porque se reemplaza el moviemiento con otro cupon
            $valor = DocValor::model()->findByPk($cupon);
            $this->setAttributes($valor->attributes);
            $this->Destino_id           = $destino_id;
            $this->doc_valor_id         = $valor->id;
            $this->tipo                 = DocValor::TIPO_TARJETA;
            $this->Doc_id               = $doc->id; //lo asigno así porque sino toma el de setAttributes
            $this->Destino_Instancia_id = Destino::getActivo();
            $this->obs                  = $doc->detalle;
            $this->cupon_estado         = 1;
            $valor->cupon_estado        = 2;
            if(!$this->save()) {
                $this->addError("docValor en salidaCupon", $this->errors);

                return $this->errors;
            }
            if(!$valor->save()) {
                $this->addError("docValor en salidaCupon", $this->errors);

                return $this->errors;
            }
            //    var_dump("valor",$valor->attributes);
            //    var_dump("this",$this->attributes);
        }

        public function salidaRetencion($retencion, $doc, $destino_id) {
            //retencion_estado null = en caja
            //retencion_estado 1 = cerrado
            //retencion_estado 2 = cerrado pero se le pone otro estado porque se reemplaza el moviemiento con otro retencion
            $valor = DocValor::model()->findByPk($retencion);
            $this->setAttributes($valor->attributes);
            $this->Destino_id           = $destino_id;
            $this->doc_valor_id         = $valor->id;
            $this->tipo                 = DocValor::TIPO_RETENCION;
            $this->Doc_id               = $doc->id; //lo asigno así porque sino toma el de setAttributes
            $this->Destino_Instancia_id = Destino::getActivo();
            $this->obs                  = $doc->detalle;
            $this->retencion_estado         = 1;
            $valor->retencion_estado        = 2;
            if(!$this->save()) {
                $this->addError("docValor en salidaRetencion", $this->errors);

                return $this->errors;
            }
            if(!$valor->save()) {
                $this->addError("docValor en salidaRetencion", $this->errors);

                return $this->errors;
            }
            //    var_dump("valor",$valor->attributes);
            //    var_dump("this",$this->attributes);
        }

        public function recuperaCupones() {
            $tipoTarjeta = DocValor::TIPO_TARJETA;
            $cupones = Yii::app()->db->createCommand("
                select d.fecha_creacion, dv.*,t.nombre, d.anulado
                from doc_valor dv
                inner join doc d on d.id = dv.doc_id
                inner join tarjeta_plan tp on tp.id = dv.tarjeta_plan_id
                inner join tarjeta t on t.id = tp.tarjeta_id
                where dv.tipo = $tipoTarjeta and dv.cupon_estado is null and d.anulado = 0 and d.activo = 1
          ")->queryAll();
            return $cupones;
        }

        public function recuperaRetenciones() {
            $tipoRetencion = DocValor::TIPO_RETENCION;
            $retenciones = Yii::app()->db->createCommand("
                select dv.id, d.numero, DATE_FORMAT(d.fecha_valor,'%d/%m/%Y') AS fecha, 
                       CASE WHEN p.id THEN p.razon_social ELSE d.detalle END AS origen,
                       case WHEN p.id THEN p.cuit ELSE dv.cuit END AS cuit,
                       dv.importe
                from doc_valor dv
                  inner join doc d on d.id = dv.doc_id
                  LEFT JOIN socio s ON d.Socio_id = s.id
                  LEFT JOIN proveedor p ON s.Proveedor_id = p.id
                where dv.tipo = $tipoRetencion and dv.retencion_estado is null and d.anulado = 0 and d.activo = 1
          ")->queryAll();
            return $retenciones;
        }

        function ReporteCajaResumido($instancia_id) {
            if(!$instancia_id) {
                return;
            }
            $tipoRetencion = DocValor::TIPO_RETENCION;
            $recibos = "(".Comprob::RECIBO_VENTAS.",".Comprob::RECIBO_ELECTRONICO.",".Comprob::ND_ELECTRONICO.",".Comprob::NC_ELECTRONICO.")";
            $select = " select dv.destino_id as dest1, di.destino_id as dest2, (dv.importe * c.signo_caja) as importe,
               d.fecha_creacion, dv.obs, d.numero,
               d.pago_a_cuenta_detalle, d.pago_a_cuenta_importe, d.saldo, d.total,
               c.id as comprob_id, c.nombre as comprob_nombre,
               c.signo_caja as comprob_signo, t.punto_venta_numero, d.detalle as doc_detalle,
               dv.numero as valor_numero, dv.tipo as valor_tipo,
               case
                when socio.alumno_id
                  then concat(a.apellido,\", \",a.nombre)
                else
                  p.razon_social
                end as socio_nombre
           from doc_valor dv
              inner join doc d on d.id = dv.Doc_id
              inner join talonario t on t.id = d.talonario_id
              inner join comprob c on c.id = t.comprob_id
              left join destino_instancia di on di.id = dv.Destino_Instancia_id
              left join socio on socio.id = d.socio_id
              left join alumno a on socio.alumno_id = a.id
              left join proveedor p on socio.proveedor_id = p.id
           where dv.Destino_Instancia_id = $instancia_id
                and ((di.destino_id = dv.destino_id) or dv.destino_id is null)
                /*and dv.tipo != $tipoRetencion*/
                and (d.anulado = 0 and d.activo = 1) and (comprob_id not in $recibos) and (comprob_id <> 11)";
            return $cajaresumida = Helpers::qryAll($select);
        }
        /*
         * Que es esto de abajo???
         */
//    $tipoTarjeta = DocValor::TIPO_TARJETA;
//    $cupones = Yii::app()->db->createCommand("
//                select d.fecha_creacion, dv.*,t.nombre, d.anulado
//                from doc_valor dv
//                inner join doc d on d.id = dv.doc_id
//                inner join tarjeta_plan tp on tp.id = dv.tarjeta_plan_id
//                inner join tarjeta t on t.id = tp.tarjeta_id
//                where dv.tipo = $tipoTarjeta and dv.cupon_estado is null and d.anulado = 0 and d.activo = 1
//          ")->queryAll();
//    return $cupones;

        public function SaldoAnterior($instancia_id) {
        $tipoRetencion = DocValor::TIPO_RETENCION;
        $selectSaldoAnt = "
        select sum(dv.importe * cs.signo_caja) as total
          from doc_valor dv
              inner join doc on doc.id = dv.Doc_id
              inner join talonario ts on ts.id = doc.talonario_id
              left join destino_instancia di on di.id = dv.Destino_Instancia_id
              inner join comprob cs on cs.id = ts.comprob_id
          where dv.Destino_Instancia_id < $instancia_id
            and ((di.destino_id = dv.destino_id) or dv.destino_id is null)
            and case when dv.tipo = 7 then dv.retencion_estado is null else true end            and doc.anulado = 0 and doc.activo = 1
            /*and dv.tipo != $tipoRetencion*/";
            //vd($selectSaldoAnt);
            // vd2($selectSaldoAnt);
            return $saldosAnt = Helpers::qryScalar($selectSaldoAnt);
        }

        public function TotalRecaudacion($instancia_id) {
          $tipoRetencion = DocValor::TIPO_RETENCION;
          $recibos = "(".Comprob::RECIBO_VENTAS.",".Comprob::RECIBO_ELECTRONICO.",".Comprob::ND_ELECTRONICO.",".Comprob::NC_ELECTRONICO.")";
          $strsql = " select Sum(dv.importe * c.signo_caja) as importe
          from doc_valor dv
              inner join doc d on d.id = dv.Doc_id
              inner join talonario t on t.id = d.talonario_id
              inner join comprob c on c.id = t.comprob_id
              left join destino_instancia di on di.id = dv.Destino_Instancia_id
          where dv.Destino_Instancia_id = $instancia_id

                and ((di.destino_id = dv.destino_id) or dv.destino_id is null)
                and (d.anulado = 0 and d.activo = 1) and (comprob_id in $recibos)
                /*and dv.tipo != $tipoRetencion*/";

            return $totalrecaudacion = Helpers::qryScalar($strsql);
        }

        public function TotalDepositos($instancia_id) {
            $comprob_deposito_id = Comprob::DEPOSITO_BANCARIO_SALIDA;
            $strsql              = "select sum(dv.importe * c.signo_caja) as importe
            from doc_valor dv
              inner join doc d on d.id = dv.Doc_id
              inner join talonario t on t.id = d.talonario_id
              inner join comprob c on c.id = t.comprob_id
              left join destino_instancia di on di.id = dv.Destino_Instancia_id
            where dv.Destino_Instancia_id = $instancia_id
                and ((di.destino_id = dv.destino_id) or dv.destino_id is null)
                and (d.anulado = 0 and d.activo = 1) and  (comprob_id = $comprob_deposito_id)";

            return $totaldepositos = Helpers::qryScalar($strsql);
        }

    }
