<?php

Yii::import('application.models._base.BaseDocValorCartera');

class DocValorCartera extends BaseDocValorCartera {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }
  
  public function getImporte() {
    $id = $this->id;
    return Yii::app()->db->createCommand("
            select dv.importe
              from doc_valor dv
              where dv.doc_valor_cartera_id = $id
          ")->queryScalar();
  }

}