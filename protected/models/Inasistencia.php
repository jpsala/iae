<?php

   Yii::import('application.models._base.BaseInasistencia');

   class Inasistencia extends BaseInasistencia
   {

      public static function model($className = __CLASS__) {
         return parent::model($className);
      }

      public static function getInasistenciasXAlumnoDivision($alumno_division_id, $logica_item_id = null) {
         $condLogicaItem = $logica_item_id ? " and logica_item_id = $logica_item_id " : "";
         $ciclo_id       = Ciclo::getActivoId();
         $ret            = array();
         $rows           = Yii::app()->db->createCommand("
        select li.nombre_unico, sum(it.valor) as total
          from inasistencia_detalle d
            inner join inasistencia i on i.id = d.inasistencia_id
            inner join alumno_division ad on ad.id = i.Alumno_Division_id and ad.id = $alumno_division_id
            inner join inasistencia_detalle id on id.inasistencia_id = i.id
            inner join inasistencia_tipo it on it.id = id.inasistencia_tipo_id
            inner join logica_periodo lp on lp.id = i.logica_periodo_id
            inner join logica_item li on li.Logica_Periodo_id = lp.id and li.tiene_conducta = 1 $condLogicaItem
            inner join logica_ciclo lc on lc.Logica_id = li.Logica_id and lc.Ciclo_id = $ciclo_id")->queryAll();
         foreach ($rows as $row) {
            if(count($row) > 0 and $row["nombre_unico"]) {
               $ret[$row["nombre_unico"]] = $row["total"];
            }
         }

         return $logica_item_id ? $ret["total"] : $ret;
      }

      public static function getInasistencias($division_id, $fecha = null, $ciclo_id = null) {
         $fecha    = $fecha ? Helpers::date("Y/m/d", $fecha) : date("Y/m/d", time());
         $ciclo_id = $ciclo_id ? $ciclo_id : Ciclo::getCicloParaCargaDeNotas()->id;
         //    $ads = AlumnoDivision::model()->with("alumno", "division")->findAll(array(
         //        "condition" => "Division_id = $division_id and t.activo = 1 and ciclo_id = $ciclo_id",
         //        "order" => "alumno.apellido,alumno.nombre")
         //    );
         ;
         //    $lp = LogicaPeriodo::model()
         //        ->findAll("\"$fecha\" >= fecha_inicio_ci and \"$fecha\" <= fecha_fin_ci");
         //    if (!$lp) {
         //      throw new Exception("No existe ninguna logicaPeriodo para el $fecha");
         //    } elseif (count($lp) > 1) {
         //      foreach ($lp as $row) {
         //        var_dump($row->attributes);
         //      }
         //      throw new Exception("Hay mas de una logicaPeriodo para el $fecha");
         //    }
         //    $logica_periodo = $lp[0]->id;
         //var_dump($lp[0]->attributes);die;
         //@TODO: tener en cuenta el ciclo y los alumnoDivision activos
         $select        =
            "select a.sexo, a.apellido, a.nombre,  concat(a.apellido,\", \",a.nombre) as nombre_completo, a.id as alumno_id,
                       null as inasistencia_id, ad.id as alumno_division_id, null as inasistencia_tipo_id,
                       null as inasistencia_tipo_nombre, null as fecha, null as detalle, null as inasistencia_detalle_id
               from alumno_division ad
                   inner join alumno a on a.id = ad.alumno_id
               where	ad.division_id = $division_id and a.activo = 1 
											and ad.activo /*((ad.activo or ad.promocionado or ad.egresado)*/
											 and ad.Ciclo_id = $ciclo_id
											and a.id not in
                   (select a.id
                       from alumno_division ad
                           left join inasistencia i on i.alumno_division_id = ad.id /*and i.logica_periodo_id = */
                           left join inasistencia_detalle id on i.id = id.inasistencia_id
                           left join inasistencia_tipo it on it.id = id.inasistencia_tipo_id
                           inner join alumno a on a.id = ad.alumno_id
                           inner join alumno_estado ae on ae.id = a.estado_id
                       where ad.division_id = $division_id and
                             a.activo = 1 and (id.fecha = '$fecha') and ad.activo = 1 and
                             !ae.ingresante
                   )
               UNION
               select a.sexo, a.apellido, a.nombre, concat(a.apellido,', ',a.nombre) as nombre_completo, a.id as alumno_id,
                           i.id as inasistencia_id, ad.id as alumno_division_id, it.id as inasistencia_tipo_id,
                           it.nombre as inasistencia_tipo_nombre, id.fecha, id.detalle as detalle, id.id as inasistencia_detalle_id
                   from alumno_division ad
                           left join inasistencia i on i.alumno_division_id = ad.id /*and i.logica_periodo_id = */
                           left join inasistencia_detalle id on i.id = id.inasistencia_id
                           left join inasistencia_tipo it on it.id = id.inasistencia_tipo_id
                           inner join alumno a on a.id = ad.alumno_id
                           inner join alumno_estado ae on ae.id = a.estado_id
                   where ad.division_id = $division_id and
                        a.activo = 1 and
                        !ae.ingresante and
                        (id.fecha = '$fecha') and (ad.activo or ad.promocionado or ad.egresado)
												and ad.Ciclo_id = $ciclo_id
               order by 2, 3
         ";
         $inasistencias = Yii::app()->db->createCommand($select)->query()->readAll();


         return $inasistencias;
      }

      public static function getInasistenciaParaCarga($division_id) {
         $ciclo_id          = Ciclo::getCicloIdParaCargaDeNotas();
         $alumno_divisiones = Helpers::qryAll("
      select ad.id, concat(alu.apellido,', ', alu.nombre) as alumno_nombre
        from alumno_division ad
          inner join alumno alu on alu.id = ad.alumno_id
          inner join alumno_estado ae on ae.id = alu.estado_id
        where ad.Division_id = $division_id and
              ad.Ciclo_id = $ciclo_id and alu.activo = 1 and !ae.ingresante
        order by alu.sexo desc, alu.apellido, alu.nombre
    ");
         foreach ($alumno_divisiones as $ad) {
            $alumno_division_id = $ad['id'];
            $alumno_nombre      = $ad["alumno_nombre"];
            $rows               = Helpers::qryAll("
        select ad.alumno_id as alumno_id, ad.id, lp.nombre as periodo_nombre, li.abrev, li.nombre_unico, 
               c.id as inasistencia_id, c.cantidad, lp.id as logica_periodo_id
          from alumno_division ad
            inner join division d on ad.division_id = d.id
            inner join anio a on a.id = d.anio_id
            inner join nivel n on n.id = a.nivel_id
            inner join asignatura_tipo at  on at.Nivel_id = n.id and at.tipo = 1
            inner join logica_ciclo lc on lc.Asignatura_Tipo_id = at.id and lc.Ciclo_id = $ciclo_id
            inner join logica_periodo lp on lp.logica_id = lc.Logica_id
            inner join logica_item li on li.logica_periodo_id = lp.id
              and li.tiene_conducta = 1
            left join inasistencia c on c.alumno_division_id = ad.id
              and c.logica_periodo_id = lp.id
        where ad.id = $alumno_division_id and ad.ciclo_id = $ciclo_id and
          (ad.activo or ad.promocionado or ad.egresado)
        order by lp.orden, li.orden");
            foreach ($rows as $row) {
               $ret[$alumno_nombre][$row["nombre_unico"]] = $row;
            }
         }

         return $ret;
      }

   }
	