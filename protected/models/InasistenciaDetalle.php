<?php

Yii::import('application.models._base.BaseInasistenciaDetalle');

class InasistenciaDetalle extends BaseInasistenciaDetalle {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * Dado un alumnodivision recupera sus inasistencias
   * @author Octavio
   * @param AlumnoDivision
   * @return $cantidad_faltas tipo InasistenciaDetalle
   * @todo Devolver solo la cantidad, hecho a manera de prueba. 
   */
  public static function obtieneCantidadInasistenciasPorAlumno(AlumnoDivision $alumnodivision) {
    $connection = Yii::app()->db;
    $sql = "select idet.* from inasistencia_detalle idet 
                left join inasistencia_tipo it
                    on idet.inasistencia_tipo_id = it.id 
            where idet.Alumno_Division_id = $alumnodivision->id";

    $rows = $connection->createCommand($sql)->queryAll();

    var_dump($rows);
    return;


    $inasistenciaDetalles = InasistenciaDetalle::model()->findAll(
    );
    $cantidad_faltas = InasistenciaDetalle::model()->findAllBySql("
            select idet.* from inasistencia_detalle idet 
                left join inasistencia_tipo it
                    on idet.inasistencia_tipo_id = it.id 
            where idet.Alumno_Division_id = $alumnodivision->id
                           
        ");

    return $cantidad_faltas;
  }

  public function beforeValidate() {
    if ($this->inasistencia_id) {
      $id = InasistenciaDetalle::model()->find("fecha = \"$this->fecha\" and inasistencia_id = $this->inasistencia_id");
      if ($id and $id->id <> $this->id) {
        $this->addError("fecha", "Solo una asistencia por fecha");
      }
    }
    return parent::beforeValidate();
  }

}