<?php

Yii::import('application.models._base.BaseInasistenciaTipo');

class InasistenciaTipo extends BaseInasistenciaTipo {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function getValorInasistencia($id) {
    $it = InasistenciaTipo::model()->find(
            'id = :id ', array('id' => $id)
    );
    return $it;
  }

  public static function getOptions($selected = null) {
    $x = array('prompt' => '');
    echo CHtml::listOptions(
            $selected, CHtml::listData(InasistenciaTipo::model()->findAll(), 'id', 'nombre'), $x
    );
  }
  public function rules() {
        return CMap::mergeArray(parent::rules(), array(
                    array('valor', 'numerical', 'integerOnly' => false),
                        )
        );
    }

}