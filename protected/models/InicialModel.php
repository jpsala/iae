<?php

class InicialModel extends CComponent {

	static public function getEvaluaciones($alumno_id) {
    $ciclo_id = Helpers::qryScalar("select max(id) from ciclo");
		$respuesta = new stdClass();
		$respuesta->estado = "ok";
		// $alumno = Helpers::qryObj("select * from alumno where id = $alumno_id");
    // return [];
		$alumno_division_id = Helpers::qryScalar("
				SELECT ad.id
					FROM alumno_division ad
						inner join alumno_division_estado ade on ade.id = ad.alumno_division_estado_id
				WHERE ad.Alumno_id = $alumno_id AND ad.activo = 1
						AND ade.muestra_edu AND ad.Ciclo_id = $ciclo_id
						AND !ad.borrado
				");
		if (!$alumno_division_id) {
			$respuesta->estado = "error";
			$respuesta->error = "no existe alumno_division para el alumno con id: $alumno_id";
			echo json_encode($respuesta);
			die;
    }
		$entro = false;
		$notas = array();
		$areasSelect = "
				SELECT ia.id, ia.nombre, ia.especial, ia.docente
					FROM inicial_area ia
						INNER JOIN anio d ON d.id = ia.seccion_id
						INNER JOIN division d1 ON d.id = d1.Anio_id
						inner JOIN alumno_division ad ON ad.Division_id = d1.id AND ad.id = $alumno_division_id
				";
		$areas = Helpers::qryAllObj($areasSelect);
		$periodos = Helpers::qryAllObj("select * from inicial_periodo order by orden");
		$subPeriodos = Helpers::qryAllObj("select * from inicial_sub_periodo");
		foreach ($areas as $key => $area) {
			$materias = Helpers::qryAllObj("
					SELECT * FROM inicial_materia im
						  WHERE im.area_id = $area->id
					");
			$areas[$key]->materias = array();
			foreach ($materias as $m) {
				$materia = new stdClass();
				$materia->nombre = $m->nombre;
				$materia->texto = $m->texto;
				$materia->periodos = array();
				foreach ($periodos as $p) {
				  $periodo = new stdClass();
					$periodo->tipo = $p->tipo;
					$periodo->subPeriodos = array();
					foreach ($subPeriodos as $s) {
						$subPeriodo = new stdClass();
						$subPeriodo->nombre = $s->nombre;
						$periodo->subPeriodos[] = $subPeriodo;
						$nota = Helpers::qryObj("
                              SELECT * FROM inicial_nota in1
                                WHERE
                                    in1.periodo_id = $p->id and
                                    in1.sub_periodo_id = $s->id and
                                    in1.materia_id = $m->id AND
                                    in1.alumno_division_id = $alumno_division_id
                            ");
						$subPeriodo->nota = $nota ? $nota->tildado : "";
                    $periodo->texto = $nota ? $nota->texto : '';
						$entro = true;
					}
					$materia->periodos[] = $periodo;
				}
				$areas[$key]->materias[] = $materia;
			}
			$areas[$key]->periodos = array();
			foreach ($periodos as $p) {
				$periodo = new stdClass();
				$periodo->nombre = $p->nombre;
				$periodo->id = $p->id;
        $periodo->tipo = $p->tipo;
        $periodo->ciclo = $p->ciclo;
        $periodo->mes = $p->mes;
				$periodo->firmas = array();
				$obs = Helpers::qryObj("
								SELECT * FROM inicial_observacion in1
									WHERE
										in1.periodo_id = $p->id and
										in1.alumno_division_id = $alumno_division_id and
										in1.area_id = $area->id
								");
				$periodo->obs = $obs ? $obs->texto : "";
				$periodo->docentes = array();
				foreach (array('lorena', 'paola') as $docente) {
					$firma = new stdClass();
					$firma->firmado = Helpers::qryObj("
								SELECT firmado FROM inicial_firma in1
									WHERE
										in1.periodo_id = $p->id and
										in1.alumno_division_id = $alumno_division_id and
										in1.area_id = $area->id and
										in1.docente = \"$docente\"
								");
					$firma->docente = $docente;
					$firma->image = "http://iae.dyndns.org/front/images/firma_$docente.png";
					$firma->firmado = ($firma->firmado && $firma->firmado->firmado && ($firma->firmado->firmado == '1')) ? 1 : 0;
					$periodo->firmas[] = $firma;
				}
				$areas[$key]->periodos[] = $periodo;
			}

		}
		$respuesta->areas = $areas;
		$respuesta->periodos = $periodos;
		$respuesta->subPeriodos = $subPeriodos;
		$respuesta->periodosCant = count($periodos);
		$respuesta->subPeriodosCant = count($subPeriodos);
		// $respuesta->alumno = $alumno;
		return $respuesta;
		// foreach ($areas as $area) {

		// 	foreach ($area->materias as $materia) {
		// 		foreach ($periodos as $periodo) {
		// 			foreach ($subPeriodos as $subPeriodo) {
		// 				$nota = Helpers::qryObj("
		// 						SELECT * FROM inicial_nota in1
		// 							WHERE
		// 								in1.periodo_id = $periodo->id and
		// 								in1.sub_periodo_id = $subPeriodo->id and
		// 								in1.materia_id = $materia->id AND
		// 								in1.alumno_division_id = $alumno_division_id
		// 						");
		// 				$notas[$area->id][$materia->id][$periodo->id][$subPeriodo->id] = $nota ? $nota->tildado : "";
		// 				$notas[$area->id][$materia->id]['texto'] = $nota['text'];
		// 				$entro = true;
		// 			}
		// 		}
		// 	}
		// }

		// return $respuesta;

		// cargo las observaciones por cada area/periodo
		foreach ($periodos as $periodo) {
			$obs = qryObj("
								SELECT * FROM inicial_observacion in1
									WHERE
										in1.periodo_id = $periodo->id and
										in1.alumno_division_id = $alumno_division_id and
										in1.area_id = $area->id
								");
			$observaciones[$area->id][$periodo->id]["texto"] = $obs ? $obs->texto : "";
			$observaciones[$area->id][$periodo->id]["obs_id"] = $obs ? $obs->id : -1;
		}
		// cargo las firmas
		$firmas = array();
		foreach ($areas as $area) {
			$tmpfirmas = qryAll("
                    SELECT f.id as firma_id, f.docente, f.firmado, f.periodo_id, f.area_id
                        FROM inicial_firma f
                        where f.area_id = $area->id and f.alumno_division_id = $alumno_division_id
                ");
			foreach ($tmpfirmas as $n) {
				$firmas[$n->periodo_id][$n->docente]["firmado"] = $n->firmado;
				$firmas[$n->periodo_id][$n->docente]["firma_id"] = $n->firma_id;
			}
			foreach ($periodos as $p) {
				foreach (array("patricia", "lorena") as $docente) {
					if (!isset($firmas[$p->id][$docente])) {
						$firmas[$p->id][$docente]["firmado"] = "0";
						$firmas[$p->id][$docente]["firma_id"] = "-1";
					}
				}
			}
			// fin de carga de firmas
		}
		if (!$entro) {
			$respuesta->estado = "error";
		} else {
			$respuesta->areas = $areas;
			$respuesta->periodos = $periodos;
			$respuesta->subPeriodos = $subPeriodos;
			$respuesta->notas = $notas;
			$respuesta->observaciones = $observaciones;
			$respuesta->firmas = $firmas;
			$respuesta->alumno = $alumno;
		}

		return $respuesta;
	}

	static public function getEvaluaciones2($alumno_id) {
		$respuesta = new stdClass();
		$respuesta->estado = "ok";
		$alumno = qryObj("select * from alumno where id = $alumno_id");
		$alumno_division_id = Helpers::qryScalar("
				SELECT ad.id
					FROM alumno_division ad
				WHERE ad.Alumno_id = $alumno_id AND ad.activo = 1
				");
		if (!$alumno_division_id) {
			$respuesta->estado = "error";
		}
		$entro = false;
		$areas = qryAllObj("
				SELECT ia.id, ia.nombre
					FROM inicial_area ia
						INNER JOIN anio d ON d.id = ia.seccion_id
						INNER JOIN division d1 ON d.id = d1.Anio_id
						inner JOIN alumno_division ad ON ad.Division_id = d1.id AND ad.id = $alumno_division_id
				");
		foreach ($areas as $area) {
			$materias = qryAllObj("
					SELECT * FROM inicial_materia im
						  WHERE im.area_id = $area->id
					");
			$area->materias = $materias;
		}
		$periodos = qryAllObj("select * from inicial_periodo");
		$subPeriodos = qryAllObj("select * from inicial_sub_periodo");
		$notas = array();
		foreach ($areas as $area) {
			// cargo las observaciones por cada area/periodo
			foreach ($periodos as $periodo) {
				$obs = qryObj("
								SELECT * FROM inicial_observacion in1
									WHERE
										in1.periodo_id = $periodo->id and
										in1.alumno_division_id = $alumno_division_id and
										in1.area_id = $area->id
								");
				$notas[$area->id][$periodo->id]["observaciones"] = $obs ? $obs->texto : "";
				$notas[$area->id][$periodo->id]["obs_id"] = $obs ? $obs->id : -1;
			}
			foreach ($area->materias as $materia) {
				foreach ($periodos as $periodo) {
					foreach ($subPeriodos as $subPeriodo) {
						$nota = qryObj("
								SELECT * FROM inicial_nota in1
									WHERE
										in1.periodo_id = $periodo->id and
										in1.sub_periodo_id = $subPeriodo->id and
										in1.materia_id = $materia->id AND
										in1.alumno_division_id = $alumno_division_id
								");
						$notas[$area->id][$materia->id][$periodo->id][$subPeriodo->id] = $nota ? $nota->tildado : "";
						$entro = true;
					}
				}
			}
		}
		if (!$entro) {
			$respuesta->estado = "error";
		} else {
			$respuesta->areas = $areas;
			$respuesta->periodos = $periodos;
			$respuesta->subPeriodos = $subPeriodos;
			$respuesta->notas = $notas;
			$respuesta->alumno = $alumno;
		}

		return $respuesta;
	}

}
