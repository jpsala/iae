<?php

Yii::import('application.models._base.BaseLiquidConf');

class LiquidConf extends BaseLiquidConf {

    public function afterSave() {
        if (parent::afterSave()) {
            return true; //Helpers::transformaFecha($this);
        } else {
            return false;
        }
    }

    public function beforeSave() {
        if (parent::beforeSave()) {
            return Helpers::validateDates($this);
        } else {
            return false;
        }
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function getActiveId() {
        $id = Opcion::getOpcionText("liquid_conf_id", Null, "liquidacion", false);
        if ($id) {
            $lc = LiquidConf::model()->findAll("id = $id");
        } else {
            $lc = LiquidConf::model()->findAll("id = (select max(id) from liquid_conf where fecha_confirmado is null and confirmada = 0)");
            if (!$lc) {
                $lc = LiquidConf::model()->findAll("id = (select max(id) from liquid_conf where fecha_confirmado is not null and confirmada = 1)");
            }
        }
        if (count($lc) !== 1) {
            throw new Exception("Error leyendo la configuración de la liquidación");
        } else {
            return $lc[0]->id;
        }
    }

    public static function getActive() {
        $id = Opcion::getOpcionText("liquid_conf_id", Null, "liquidacion", false);
        if ($id) {
            $lc = LiquidConf::model()->findAll("id = $id");
        } else {
            $lc = LiquidConf::model()->findAll("id = (select max(id) from liquid_conf where fecha_confirmado is null and confirmada = 0)");
            if (!$lc) {
                $lc = LiquidConf::model()->findAll("id = (select max(id) from liquid_conf where fecha_confirmado is not null and confirmada = 1)");
            }
        }
        if (count($lc) !== 1) {
            throw new Exception("Error leyendo la configuración de la liquidación");
        } else {
            return $lc[0];
        }
    }

}
