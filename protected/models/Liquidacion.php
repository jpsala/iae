<?php

Yii::import('application.models._base.BaseLiquidacion');

class Liquidacion extends BaseLiquidacion
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    
    public function getDescripcion(){
        $qry = "select max(numero_de_cuota) as max from liquidacion";
        $max = Helpers::qry($qry);
      if($this->isNewRecord){
        return "Cuota ".$max["max"];
      }else{
        return "Cuota ".$this->numero_de_cuota;
      }
    }
}