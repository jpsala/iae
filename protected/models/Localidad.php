<?php

Yii::import('application.models._base.BaseLocalidad');

class Localidad extends BaseLocalidad
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        public function __set($name, $value) {
        if (($name == 'Pais_id') and !is_numeric($value)) {
            $obj = new Pais();
            $obj->nombre = $value;
            $obj->save();
            $value = $obj->id;
        }
        parent::__set($name, $value);
    }

}