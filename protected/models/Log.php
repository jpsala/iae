<?php

	Yii::import('application.models._base.BaseLog');

	class Log extends BaseLog {

		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		public function beforeSave() {
			$this->user_id = Yii::app()->user->id;
			$this->fecha = new CDbExpression('NOW()');
			return parent::beforeSave();
		}

		public function beforeValidate() {
			return parent::beforeValidate();
		}

	}
	