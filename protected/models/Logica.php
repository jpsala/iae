<?php

Yii::import('application.models._base.BaseLogica');

include_once("formulas/LogicaActiva.php");

/**
 * Representa la logica de las notas de uno o mas AsignaturaTipos
 * 
 * Junto con LogicaCiclo y Asignatura se puede determinar en que ciclos estuvo activa
 */
class Logica extends BaseLogica {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function getLogica(AsignaturaTipo $asignaturaTipo, Ciclo $ciclo = null) {
        $ciclo = $ciclo ? $ciclo : Ciclo::getCicloParaCargaDeNotas();

        return Logica::model()->findBySql("
                select l.* from logica l
                    inner join logica_ciclo lc 
                        on lc.logica_id = l.id and 
                           lc.ciclo_id = $ciclo->id and
                           lc.asignatura_tipo_id = $asignaturaTipo->id
        ");
    }

    public static function getLogicaId($asignatura_tipo_id, $ciclo_id = null) {
        $ciclo_id = $ciclo_id ? $ciclo_id : Ciclo::getActivoId();

        return Yii::app()->db->createCommand("
                select l.id from logica l
                    inner join logica_ciclo lc 
                        on lc.logica_id = l.id and 
                           lc.ciclo_id = $ciclo_id and
                           lc.asignatura_tipo_id = $asignatura_tipo_id
        ")->queryScalar();
    }

    public static function getLogicaIdPorNivel($nivel_id) {
        $ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
        return Helpers::qryScalar("
            select l.logica_id
                from logica_ciclo l
                    inner join asignatura_tipo t on t.id = l.Asignatura_Tipo_id and t.tipo = 1 and t.Nivel_id = $nivel_id 
                        and l.Ciclo_id = $ciclo_id
        ");
    }

    public static function getLogicaIdPorAsignatura($asignatura_id, $ciclo_id = null) {
        $ciclo_id = $ciclo_id ? $ciclo_id : Ciclo::getActivoId();
        $asignatura_tipo_id = Helpers::qryScalar("
            select asignatura_tipo_id from asignatura where id = $asignatura_id
        ");
        return Yii::app()->db->createCommand("
                select l.id from logica l
                    inner join logica_ciclo lc on lc.logica_id = l.id and lc.ciclo_id = $ciclo_id and lc.asignatura_tipo_id = $asignatura_tipo_id
        ")->queryScalar();
    }

    public function procesaNotas($alumnoDivision, $asignatura, $notasAProcesar, $logica_periodo_id) {
        return LogicaActiva::procesaNotas($alumnoDivision, $asignatura, $notasAProcesar, $logica_periodo_id);
    }

    public static function getLogicaIdPorAlumnoDivison($alumno_division_id) {
        $nivel_id = Helpers::qryScalar("
            select an.nivel_id
                from alumno_division ad
                    inner join division d on d.id = ad.division_id
                    inner join anio an on an.id = d.Anio_id
           where ad.id = $alumno_division_id
        ");
        return self::getLogicaIdPorNivel($nivel_id);
    }

//    public function setNotasJS(AlumnoDivision $alumnoDivision, Asignatura $asignatura, $notas) {
//        
//    }
//    public function getNotas($alumno_division_id, Asignatura $asignatura, $logica_periodo_id = null) {
//        return Formula::getNotas($alumno_division_id, $asignatura, $this->id, $logica_periodo_id);
//    }
//
//    public function setNotas(AlumnoDivision $alumnoDivision, Asignatura $asignatura, $notas) {
//        
//    }
//    
//    public function procesaNotasNuevo($alumnoDivision, $asignatura, $notasAProcesar, $logica_periodo_id) {
//        return Formula::getNotas($alumnoDivision, $asignatura, $this->id, $logica_periodo_id, $notasAProcesar);
//    }
//    public function procesaNotasBackup($alumnoDivision, $asignatura, $notasAProcesar, $logica_periodo_id) {
//        $notas = array(); // es el array a devolver, tiene datos para ser usados en los views
//        $notasParaFormulas = array(); //para ser usado en las fórmulas, cada elemento tiene un objeto Nota, para hacerlo compatible con getnotas y setnotas
//        $orden = Yii::app()->db->createCommand("
//                      select orden from logica_periodo where id = $logica_periodo_id
//                      ")->queryScalar();
//
//        $rows = Yii::app()->db->createCommand("
//            select li.condicion, lp.orden as logica_periodo_orden, li.nombre_unico,
//                   li.orden as logica_item_orden, li.manual, li.id, li.formula, li.nombre
//              from logica_item li
//                inner join logica_periodo lp on lp.id = li.logica_periodo_id
//              where li.logica_id = $this->id
//              order by lp.orden, li.orden
//            ")->queryAll();
//        foreach ($rows as $li) {
//            $id = $li["id"];
//            $periodoEsActual = $li["logica_periodo_orden"] == $orden;
//            $periodoEsAnterior = $li["logica_periodo_orden"] < $orden;
//            $periodoEsPosterior = $li["logica_periodo_orden"] > $orden;
//            $nota = array(
//                    "nota" => "",
//                    "error" => "",
//                    "logica_periodo_id" => $logica_periodo_id,
//                    "manual" => $li["manual"],
//                    "logica_item_id" => $li["id"],
//                    "nombre" => $li["nombre"],
//                    "nombre_unico" => $li["nombre_unico"],
//            );
//            if ($li["manual"]) {
//                if (isset($notasAProcesar[$li["id"]])) { // esta nota viene cargada en $notasAProcesar
//                    $nota["nota"] = $notasAProcesar[$li["id"]];
//                } else { //si no viene cargada trato de tomarla de la base
//                    $nota["nota"] = Yii::app()->db->createCommand("
//                    select nota
//                      from nota
//                      where Alumno_Division_id = $alumnoDivision->id and 
//                        Asignatura_id = $asignatura->id and 
//                        Logica_Item_id = $id
//                  ")->queryScalar();
//                }
//            }
//            $notas[$li["nombre_unico"]] = $nota;
//            $notasParaFormulas[$li["nombre_unico"]] = $nota["nota"]; //cargo el array para ser procesado por las formulas
//
//            $nota["nota"] = LogicaItem::formula(
//                            $li["formula"], $nota, $notasParaFormulas, $nota["error"], $periodoEsActual, $periodoEsAnterior, $periodoEsPosterior);
//
//            $notasParaFormulas[$li["nombre_unico"]] = $nota["nota"]; //la cargo otra vez por si fué modificada en la formula
//
//            $notas[$li["nombre_unico"]] = $nota; //fundamental :-)
//        }
//        $this->formula($this->formula, $notas);
//        return $notas;
//    }
//    public function formula($formula, &$notas) {
//        // hasta aca va en formula
//        if (strlen(trim($formula)) > 1) {
//            try {
//                eval($formula);
//            } catch (Exception $e) {
////        $nota["error"] = "Error en la fórmula";
//            }
//        }
//        // a partir de aca va en formula una vez ande bien
//    }
}
