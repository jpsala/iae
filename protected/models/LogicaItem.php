<?php

Yii::import('application.models._base.BaseLogicaItem');

/**
 * Representa un periodo evaluatorio.
 * 
 * Un ejemplo de varios LineItems podrian ser PT, ST, TT y Final
 * 
 * Siendo los 3 primero editables y el 4to. una formula
 * 
 * @property string $nombre
 * Por ej. PT (Primter Trimestre)
 * @property boolean $requerida
 * @property text $formula<code>
 * Por ej. un LineItem llamado "Final" podria contener "$nota->nota = ($notasValidadas['PT']+$notasValidadas['ST']+$notasValidadas['TT'])/3);"
 * La formula puede contener varias lineas, cada una terminando en un ";" 
 * Dentro del contexto de ejecucion de la formula tenemos:
 * Nota[] $notas es un array con todos las notas evaluadas hasta ahora o con todas las notas en el caso de estar grabando
 * $nota (Nota) es la nota que se esta evaluando.
 * En formula

 * </code>
 * @todo deberia de haber alguna logica o campo/s para la validacion en javascript
 * @todo falta implementar los casos en que haya periodos que todavia no debieran calcularse, a pensar...
 * @see LineItem
 */
class LogicaItem extends BaseLogicaItem {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function relations() {
    $relations = parent::relations();
    return $relations;
  }

  public function setNota(AlumnoDivision $alumnoDivision, Asignatura $asignatura, &$notas) {

    if (isset($notas[$this->nombreInterno])) {
      $nota = $notas[$this->nombreInterno];
    } else {
      /* @var $notaBase Nota */
      $notaBase = Nota::model()->find("Alumno_Division_id = $alumnoDivision->id and Asignatura_id = $asignatura->id and Logica_Item_id = $this->id");
      $nota = array();
      if (!$notaBase) {
        $notas[$this->nombreInterno]->logica_item_id = $this->id;
        $notas[$this->nombreInterno]->error = null;
        $notas[$this->nombreInterno]->nombre = $this->nombre;
        $notas[$this->nombreInterno]->nombreInterno = $this->nombreInterno;
        $notas[$this->nombreInterno]->nota = null;
        $notaBase->Asignatura_id = $asignatura->id;
        $notaBase->Alumno_Division_id = $alumnoDivision->id;
        $notaBase->Logica_item_id = $this->id;
      } else {
        $notas[$this->nombreInterno]->logica_item_id = $this->id;
        $notas[$this->nombreInterno]->error = null;
        $notas[$this->nombreInterno]->nombre = $this->nombre;
        $notas[$this->nombreInterno]->nombreInterno = $this->nombreInterno;
        $notas[$this->nombreInterno]->nota = $notaBase->nota;
      }
    }

    if (strlen(trim($this->formula)) > 1) {
      try {
        eval($this->formula);
        $nota["nota"] = $notasValidadas[$this->nombreInterno]->nota;
      } catch (Exception $e) {
        echo $e->getTraceAsString();
      }
    }
    if (!$notaBase->save()) {
      //@TODO: Agregar el alumno, division, lógicaItem y asignatura a los datos del error
      throw new CHttpException(404, "Error grabando una nota");
    }

    return $nota;
  }

  public function getNota(AlumnoDivision $alumnoDivision, Asignatura $asignatura) {

    /* @var $notaBase Nota */
    $nota = null;
    $notaBase = Nota::model()->find("Alumno_Division_id = $alumnoDivision->id and Asignatura_id = $asignatura->id and Logica_Item_id = $this->id");
    $nota->id = $notaBase ? $notaBase->id : null;
    $nota->logica_item_id = $this->id;
    $nota->error = null;
    $nota->nombre = $this->nombre;
    $nota->nombreInterno = $this->nombreInterno;
    $nota->nota = $notaBase ? $notaBase->nota : null;

    return $nota;
  }

  public function validaNota($notasAValidar, &$notasValidadas) {
    
  }

  public function getActiva($fecha = null) {
    $fecha = $fecha ? $fecha : date("Y-m-d");
    if ($fecha >= $this->logicaPeriodo->fecha_inicio and $fecha <= $this->logicaPeriodo->fecha_fin) {
      return true;
    } else {
      return false;
    }
  }

  static function formula($formula, &$nota, $notas, $periodoEsActual, $periodoEsAnterior, $periodoEsPosterior) {
    //var_dump($formula,$notas);
    if (strlen(trim($formula)) > 1) {
      try {
        return eval($formula);
      } catch (Exception $e) {
        echo $e->getTraceAsString();
      }
    } else {
      return $nota["nota"];
    }
  }

}

