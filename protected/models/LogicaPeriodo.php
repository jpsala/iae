<?php

Yii::import('application.models._base.BaseLogicaPeriodo');

class LogicaPeriodo extends BaseLogicaPeriodo {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public static function getActiva($logica_id) {
    $hoy = date("Y/m/d", time());
    return LogicaPeriodo::model()->find("fecha_inicio_ci <= \"$hoy\" and fecha_fin_ci >= \"$hoy\" and logica_id = $logica_id");
  }

}