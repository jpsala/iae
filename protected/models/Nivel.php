<?php

Yii::import('application.models._base.BaseNivel');

class Nivel extends BaseNivel {
	const NIVEL_INICIAL = 1;
	const NIVEL_EP1 = 2;
	const NIVEL_EP2 = 3;
	const NIVEL_ES = 4;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeSave() {
        if (!$this->orden and $this->isNewRecord) {
            $rec = Yii::app()->db->createCommand("select max(orden) as orden from nivel")->queryRow();
            $this->orden = $rec["orden"] + 10;
        }
        return parent::beforeSave();
    }

    public function rules() {
        return CMap::mergeArray(parent::rules(), array(
                        array('orden', 'numerical', 'integerOnly' => true),
                        )
        );
    }

    public function defaultScope() {
        $alias = $this->getTableAlias(false, false);
        return array(
                'order' => "$alias.orden",
                'condition' => "$alias.activo = 1",
        );
    }

    public function scopes() {
        $alias = $this->getTableAlias(false, false);
        return CMap::mergeArray(parent::scopes(), array(
                        "todos" => array(
                        ),
                        "activos" => array(
                                "condition" => "$alias.activo = 1",
                        ),
                        "ordenados" => array(
                                "order" => "$alias.orden",
                        )
        ));
    }

    public static function listData($activo = null) {
        $where = $activo ? " where activo = $activo" : "";
        $niveles = Helpers::qryAll("select id, nombre from nivel $where order by id, nombre");
        $ret = array();
        $ret["-1"] = "Todos";
        foreach ($niveles as $nivel) {
            $ret[$nivel["id"]] = $nivel["nombre"];
        }
        return $ret;
    }

    public static function getNivelIdDesdeDivision($division_id) {
        return Yii::app()->db->createCommand("
            select a.nivel_id 
                from division d 
                      inner join anio a on a.id = d.anio_id
              where d.id = $division_id
        ")->queryScalar();
    }

    public static function getNivelIdDesdeAlumnoDivision($alumno_division_id) {
        return Yii::app()->db->createCommand("
            select a.nivel_id from alumno_division ad
                inner join division d on d.id = ad.Division_id
                  inner join anio a on a.id = d.anio_id
              where ad.id = $alumno_division_id
            ")->queryScalar();
    }

}