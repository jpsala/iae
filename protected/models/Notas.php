<?php
include_once("formulas/LogicaActiva.php");
//include_once ("formulas/" . Ciclo::getActivoNombre() . ".php");

class Notas extends stdClass {
    const TIPO_NORMAL = 0;
    const TIPO_FINAL_INTERMEDIO = 20;
    const TIPO_FINAL = 30;
    public static function getAsignaturas($division_id, $division_asignatura_id = null) {
        $andAsignatura = $division_asignatura_id ? " and da.id = $division_asignatura_id" : "";
        $selectAsignaturas = "
        select a.*
                from division_asignatura da
                    inner join asignatura a on da.`Asignatura_id` = a.id and a.activa = 1
        where da.Division_id = $division_id $andAsignatura
        order by a.orden";
        //vd(Helpers::qryAll($selectAsignaturas));
        return Helpers::qryAll($selectAsignaturas);
    }

    public static function getNotasAlumnoAsignaturaPeriodo($alumno_division_id, $asignatura_id, $logica_periodo_id
             , $nivel_id = null, $logica_id = null, $items = null, $tipo_nota = null) {
        return LogicaActiva::getNotasAlumnoAsignaturaPeriodo($alumno_division_id, $asignatura_id, $logica_periodo_id
                 , $nivel_id, $logica_id, $items, $tipo_nota);
    }

}

?>
