<?php

Yii::import('application.models._base.BaseNovedad');

class Novedad extends BaseNovedad
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    
    public function beforeSave() {
      if($this->importe == 0){
        $this->addError("importe", "No puede ser 0");
      }
      return parent::beforeSave();
    }
}