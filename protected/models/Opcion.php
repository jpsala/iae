<?php

	Yii::import('application.models._base.BaseOpcion');

	class Opcion extends BaseOpcion {

		private static $opciones;

		public static function model($className = __CLASS__) {
			return parent::model($className);
		}

		public static function getOpcionText($nombre, $default = null, $categoria = "", $error = true, $vd = false) {
			$key = $nombre . "/|/" . $categoria;
			$user_id = Yii::app()->user->id ? Yii::app()->user->id : -1;
			if (!isset(self::$opciones[$key])) {
				$select = "select data from opcion 
					where nombre = \"$nombre\" and categoria= \"$categoria\" and user_id= $user_id";
				$data = Helpers::qry($select);
				if (!$data or $data == false) {
					$select = "select data from opcion where nombre = \"$nombre\" and categoria= \"$categoria\"";
					$data = Helpers::qry($select);
				}
				if ($vd) {
					vd($select, $data);
				}
				if ($data == false) {
					if (!$default and $error) {
						throw new Exception("No existe la opción $nombre (categoría:$categoria) en la tabla de opciones");
					}
					$recs = Helpers::qryExec("insert into opcion (nombre,data,categoria) values (\"$nombre\",\"$default\",\"$categoria\")");
					if (!$recs) {
						throw new Exception("No se pudo ejecutar " . "insert into opcion (nombre,data,categoria) values (\"$nombre\",\"$default\",\"$categoria\")");
					}
					self::$opciones[$key] = $default;
				} else {
					self::$opciones[$key] = $data["data"];
				}
			}
			if ($vd) {
				vd(88);
			}
			return self::$opciones[$key];
		}

		public static function saveOpcion($nombre, $data, $categoria = "") {
			$key = $nombre . "/|/" . $categoria;
			$user_id = Yii::app()->user->id ? Yii::app()->user->id : -1;
			$select = "select data from opcion 
					where nombre = \"$nombre\" and categoria= \"$categoria\" and user_id= $user_id";
			$dataActual = Helpers::qryScalar($select);
			if (!$dataActual or $dataActual == false) {
				$recs = Helpers::qryExec("insert into opcion (nombre,data,categoria,user_id) values (\"$nombre\",\"$data\",\"$categoria\", $user_id)");
				self::$opciones[$key] = $dataActual;
			} else {
				$recs = Helpers::qryExec("update opcion "
								. "set nombre=\"$nombre\", data=\"$data\", categoria=\"$categoria\", user_id=$user_id "
								. "where nombre = \"$nombre\" and categoria = \"$categoria\" and user_id = \"$user_id\"");
			}
			return $recs;
		}

		public static function deleteOpcion($nombre, $categoria = "") {
			$user_id = Yii::app()->user->id ? Yii::app()->user->id : -1;
			$recs = Helpers::qryExec("delete from opcion where nombre = \"$nombre\" and categoria = \"$categoria\" and user_id = \"$user_id\"");
			return $recs;
		}

	}
	