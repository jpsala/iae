<?php

Yii::import('application.models._base.BasePariente');

class Pariente extends BasePariente
{
    public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function rules() {
        return CMap::mergeArray(parent::rules(), array(
                    //array("email", "email"),
                    //array("email1", "email"),
                        )
        );
    }

    public function afterSave(){
      parent::afterSave();
      if($this->isNewRecord){
        $socio = new Socio();
        $socio->Pariente_id = $this->id;
        $socio->tipo = 'R';
        $socio->save();
      }
    }

    public static function listData($familia_id) {
        $niveles = Helpers::qryAll("select p.id, concat(p.apellido,', ', p.nombre, ' (',pt.nombre, ')' ) as nombre from pariente  p
                                    left join pariente_tipo pt on pt.id = p.Pariente_Tipo_id
                                    where p.familia_id = $familia_id");
        $ret = array();
        foreach ($niveles as $nivel) {
            ve($nivel["id"]);
            $ret[$nivel["id"]] = $nivel["nombre"];
        }
        return $ret;
    }
}
