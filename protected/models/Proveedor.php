<?php

Yii::import('application.models._base.BaseProveedor');

class Proveedor extends BaseProveedor {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function rules() {
    $rules = parent::rules();
    return CMap::mergeArray($rules, array(
                array('correo_electronico1, correo_electronico2, correo_electronico3', 'email', 'checkMX' => true),
    ));
  }

  public function getNombreCompleto() {
    return $this->nombre_fantasia;
  }
  
  public function getNombre(){
    return $this->nombreCompleto;
  }

}