<?php

Yii::import('application.models._base.BasePuestoTrabajoTalonario');

class PuestoTrabajoTalonario extends BasePuestoTrabajoTalonario {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function getTalonarioData($comprob_id) {
        if (!$comprob_id) {
            throw new Exception("Debe especificar un tipo de comprobante");
        }
        $user_id = Yii::app()->user->id;
        if (!$user_id) {
            throw new Exception("No hay un usuario activo");
        }
        return Yii::app()->db->createCommand("
            select pt.*, p.nombre_pc
              from user u 
                    inner join puesto_trabajo p on p.id = u.puesto_trabajo_id
                    inner join puesto_trabajo_talonario pt on pt.puesto_trabajo_id = p.id
                    inner join talonario t on t.id = pt.talonario_id  and t.comprob_id = $comprob_id
              where u.id = $user_id
          ")->queryRow();
    }

}