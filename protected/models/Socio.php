<?php

Yii::import('application.models._base.BaseSocio');

class Socio extends BaseSocio {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    public function recupera_datos_socio_segun_tipo($value) {

        if (isset($value->Alumno_id)) {
            $value = Alumno::model()->findByPk($value->id);
        } else if (isset($value->Cliente_id)) {
            $value = Cliente::model()->findByPk($value->id);
        } else if (isset($value->Proveedor_id)) {
            $value = Proveedor::model()->findByPk($value->id);
        } else if (isset($value->Pariente_id)) {
            $value = Pariente::model()->findByPk($value->id);
        } else {
            //Socio debe ser Alumno,Cliente,Pariente o Proveedor
            throw new Exception("Tipo de socio no reconocido");
        }
        return $value;
    }

    public function getNombre() {
        if ($this->Alumno_id)
            return $this->alumno->apellido . ",  " . $this->alumno->nombre;
        elseif ($this->Cliente_id)
            return $this->cliente->nombre;
        elseif ($this->Proveedor_id)
            return $this->proveedor->razon_social;
        elseif ($this->Cliente_id)
            return $this->cliente->nombre;
        elseif ($this->Pariente_id)
            return $this->pariente->nombre;
    }

    public static function recalculaSaldos() {
        $tr = Yii::app()->db->beginTransaction();
        //Socio::model()->updateAll(array("saldo" => 0));
        foreach (Doc::model()->findAll("anulado = 0") as $doc) {
            if ($doc->Socio_id) {
                $doc->socio->saldo += ($doc->total * $doc->talonario->comprob->signo);
                if (!$doc->socio->save()) {
                    var_dump($doc->errors);
                    die;
                }
            }
        }
        $tr->commit();
    }

    public function getSaldoParaAplicar() {
        $qry = "
      select sum(d.saldo) as saldo
                from socio s 
                    inner join doc d on d.Socio_id = s.id
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
                where  c.signo_cc = -1 and d.socio_id = $this->id and d.saldo > 0
                            and d.anulado <> 1 and d.activo = 1";
        return Yii::app()->db->createCommand($qry)->queryScalar();
    }

}