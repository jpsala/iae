<?php

Yii::import('application.models._base.BaseTalonario');

class Talonario extends BaseTalonario {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public static function getTalonarioData($comprob_id) {
        if (!$comprob_id) {
            throw new Exception("Debe especificar un tipo de comprobante");
        }
        $user_id = Yii::app()->user->id;
        if (!$user_id) {
            throw new Exception("No hay un usuario activo");
        }
        return Yii::app()->db->createCommand("
            select p.id, p.nombre, p.punto_venta_numero, p.nombre_pc, t.imprimeAuto, t.impresora, t.numero, t.id as talonario_id
              from user u 
                    inner join puesto_trabajo p on p.id = u.puesto_trabajo_id
                    inner join talonario t on t.punto_venta_numero = p.punto_venta_numero and t.comprob_id = $comprob_id
				where u.id = $user_id
          ")->queryRow();
    }
}