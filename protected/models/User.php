<?php

Yii::import('application.models._base.BaseUser');

class User extends BaseUser {

  public $password_repeat;

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function rules() {
    return array(
        array('nombre, password, login', 'required'),
        array('cambiar_password, puesto_trabajo_id', 'numerical', 'integerOnly' => true),
        array('nombre, password, password_repeat', 'length', 'max' => 45),
        array('password', 'compare', 'compareAttribute' => 'password_repeat', "on"=>"insert"),
        array('login', 'filter', 'filter' => 'strtolower'),
        array('login', 'length', 'max' => 25),
        array('cambiar_password', 'default', 'setOnEmpty' => true, 'value' => null),
        array('password', 'unsafe', 'on' => 'update'),
        array('nombre, apellido, login, documento, fecha_nacimiento, lugar_nacimiento, cuit, estado_civil, telefono, celular, email, domicilio_calle, domicilio_numero, domicilio_piso, domicilio_depto, provincia, localidad, titulo, expedido_por, registro_numero, cargo, function, categoria, fecha_ingreso_iae, fecha_ingreso_docencia, categoria_general, obra_social, obra_social_numero_afiliado, afiliado_sindicato, afiliado_sindicato_numero, nivel_id, legajo', 'safe'),
        array('apellido, cambiar_password, login, puesto_trabajo_id, keep_session, documento, fecha_nacimiento, lugar_nacimiento, cuit, estado_civil, telefono, celular, email, domicilio_calle, domicilio_numero, domicilio_piso, domicilio_depto, provincia, localidad, titulo, expedido_por, registro_numero, cargo, function, categoria, fecha_ingreso_iae, fecha_ingreso_docencia, categoria_general, obra_social, obra_social_numero_afiliado, afiliado_sindicato, afiliado_sindicato_numero, nivel_id, legajo', 'default', 'setOnEmpty' => true, 'value' => null),
    );
  }

  public function beforeSave() {
    if (parent::beforeSave() && $this->isNewRecord) {
      // for example
      $this->password = md5($this->password);
    }
    return true;
  }

  public function afterSave(){
    parent::afterSave();
    if($this->isNewRecord){
      $socio = new Socio();
      $socio->Empleado_id = $this->id;
      $socio->tipo = 'E';
      if(!$socio->save()){
        vd($socio->errors);
      }
    }
  }

  public function login() {
    $identity = new UserIdentity($this->login, $this->password);
    $identity->authenticate();
    return Yii::app()->user->login($identity);
  }

  public function getName() {
    return $this->login;
  }

}
