<?php

/**
 * This is the model base class for the table "configuracion".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Configuracion".
 *
 * Columns in table "configuracion" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $id
 * @property string $nombre
 * @property string $codigo
 * @property string $valor
 *
 */
abstract class BaseConfiguracion extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'configuracion';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Configuracion|Configuracions', $n);
	}

	public static function representingColumn() {
		return 'nombre';
	}

	public function rules() {
		return array(
			array('nombre, codigo, valor', 'required'),
			array('nombre', 'length', 'max'=>45),
			array('codigo', 'length', 'max'=>10),
			array('valor', 'length', 'max'=>256),
			array('id, nombre, codigo, valor', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'nombre' => Yii::t('app', 'Nombre'),
			'codigo' => Yii::t('app', 'Codigo'),
			'valor' => Yii::t('app', 'Valor'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('nombre', $this->nombre, true);
		$criteria->compare('codigo', $this->codigo, true);
		$criteria->compare('valor', $this->valor, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}