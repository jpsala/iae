<?php

/**
 * This is the model base class for the table "criticker".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Criticker".
 *
 * Columns in table "criticker" available as properties of the model,
 * and there are no model relations.
 *
 * @property integer $id
 * @property string $opcion
 * @property string $valor
 *
 */
abstract class BaseCriticker extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'criticker';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Criticker|Critickers', $n);
	}

	public static function representingColumn() {
		return 'opcion';
	}

	public function rules() {
		return array(
			array('opcion', 'length', 'max'=>30),
			array('valor', 'length', 'max'=>255),
			array('opcion, valor', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, opcion, valor', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'opcion' => Yii::t('app', 'Opcion'),
			'valor' => Yii::t('app', 'Valor'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('opcion', $this->opcion, true);
		$criteria->compare('valor', $this->valor, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}