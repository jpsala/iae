<?php

/**
 * This is the model base class for the table "doc_afip".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "DocAfip".
 *
 * Columns in table "doc_afip" available as properties of the model,
 * followed by relations of table "doc_afip" available as properties of the model.
 *
 * @property string $id
 * @property string $doc_id
 * @property string $cae
 * @property string $obs
 * @property string $doc
 *
 * @property Doc $doc0
 */
abstract class BaseDocAfip extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'doc_afip';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'DocAfip|DocAfips', $n);
	}

	public static function representingColumn() {
		return 'cae';
	}

	public function rules() {
		return array(
			array('doc_id', 'required'),
			array('doc_id', 'length', 'max'=>10),
			array('cae', 'length', 'max'=>255),
			array('obs, doc', 'safe'),
			array('cae, obs, doc', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, doc_id, cae, obs, doc', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'doc0' => array(self::BELONGS_TO, 'Doc', 'doc_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'doc_id' => null,
			'cae' => Yii::t('app', 'Cae'),
			'obs' => Yii::t('app', 'Obs'),
			'doc' => Yii::t('app', 'Doc'),
			'doc0' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('doc_id', $this->doc_id);
		$criteria->compare('cae', $this->cae, true);
		$criteria->compare('obs', $this->obs, true);
		$criteria->compare('doc', $this->doc, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}