<?php

/**
 * This is the model base class for the table "inicial_firma".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "InicialFirma".
 *
 * Columns in table "inicial_firma" available as properties of the model,
 * followed by relations of table "inicial_firma" available as properties of the model.
 *
 * @property string $id
 * @property string $area_id
 * @property string $periodo_id
 * @property string $ciclo_id
 * @property string $alumno_division_id
 * @property string $docente
 * @property integer $firmado
 *
 * @property AlumnoDivision $alumnoDivision
 * @property Ciclo $ciclo
 * @property InicialArea $area
 * @property InicialPeriodo $periodo
 */
abstract class BaseInicialFirma extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'inicial_firma';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'InicialFirma|InicialFirmas', $n);
	}

	public static function representingColumn() {
		return 'docente';
	}

	public function rules() {
		return array(
			array('area_id, periodo_id, ciclo_id, alumno_division_id', 'required'),
			array('firmado', 'numerical', 'integerOnly'=>true),
			array('area_id, periodo_id, ciclo_id, alumno_division_id', 'length', 'max'=>10),
			array('docente', 'length', 'max'=>25),
			array('docente, firmado', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, area_id, periodo_id, ciclo_id, alumno_division_id, docente, firmado', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'alumnoDivision' => array(self::BELONGS_TO, 'AlumnoDivision', 'alumno_division_id'),
			'ciclo' => array(self::BELONGS_TO, 'Ciclo', 'ciclo_id'),
			'area' => array(self::BELONGS_TO, 'InicialArea', 'area_id'),
			'periodo' => array(self::BELONGS_TO, 'InicialPeriodo', 'periodo_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'area_id' => null,
			'periodo_id' => null,
			'ciclo_id' => null,
			'alumno_division_id' => null,
			'docente' => Yii::t('app', 'Docente'),
			'firmado' => Yii::t('app', 'Firmado'),
			'alumnoDivision' => null,
			'ciclo' => null,
			'area' => null,
			'periodo' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('area_id', $this->area_id);
		$criteria->compare('periodo_id', $this->periodo_id);
		$criteria->compare('ciclo_id', $this->ciclo_id);
		$criteria->compare('alumno_division_id', $this->alumno_division_id);
		$criteria->compare('docente', $this->docente, true);
		$criteria->compare('firmado', $this->firmado);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}