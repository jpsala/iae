<?php

/**
 * This is the model base class for the table "inicial_sub_periodo".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "InicialSubPeriodo".
 *
 * Columns in table "inicial_sub_periodo" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $id
 * @property string $nombre
 *
 */
abstract class BaseInicialSubPeriodo extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'inicial_sub_periodo';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'InicialSubPeriodo|InicialSubPeriodos', $n);
	}

	public static function representingColumn() {
		return 'nombre';
	}

	public function rules() {
		return array(
			array('nombre', 'required'),
			array('nombre', 'length', 'max'=>65),
			array('id, nombre', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'nombre' => Yii::t('app', 'Nombre'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('nombre', $this->nombre, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}