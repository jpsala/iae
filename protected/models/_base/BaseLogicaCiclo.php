<?php

/**
 * This is the model base class for the table "logica_ciclo".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "LogicaCiclo".
 *
 * Columns in table "logica_ciclo" available as properties of the model,
 * followed by relations of table "logica_ciclo" available as properties of the model.
 *
 * @property string $id
 * @property string $Ciclo_id
 * @property string $Logica_id
 * @property string $Asignatura_Tipo_id
 *
 * @property AsignaturaTipo $asignaturaTipo
 * @property Ciclo $ciclo
 * @property Logica $logica
 */
abstract class BaseLogicaCiclo extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'logica_ciclo';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'LogicaCiclo|LogicaCiclos', $n);
	}

	public static function representingColumn() {
		return 'id';
	}

	public function rules() {
		return array(
			array('Ciclo_id, Logica_id, Asignatura_Tipo_id', 'required'),
			array('Ciclo_id, Logica_id, Asignatura_Tipo_id', 'length', 'max'=>10),
			array('id, Ciclo_id, Logica_id, Asignatura_Tipo_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'asignaturaTipo' => array(self::BELONGS_TO, 'AsignaturaTipo', 'Asignatura_Tipo_id'),
			'ciclo' => array(self::BELONGS_TO, 'Ciclo', 'Ciclo_id'),
			'logica' => array(self::BELONGS_TO, 'Logica', 'Logica_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'Ciclo_id' => null,
			'Logica_id' => null,
			'Asignatura_Tipo_id' => null,
			'asignaturaTipo' => null,
			'ciclo' => null,
			'logica' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('Ciclo_id', $this->Ciclo_id);
		$criteria->compare('Logica_id', $this->Logica_id);
		$criteria->compare('Asignatura_Tipo_id', $this->Asignatura_Tipo_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}