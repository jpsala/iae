<?php

/**
 * This is the model base class for the table "logica_periodo".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "LogicaPeriodo".
 *
 * Columns in table "logica_periodo" available as properties of the model,
 * followed by relations of table "logica_periodo" available as properties of the model.
 *
 * @property string $id
 * @property string $nombre
 * @property string $abrev
 * @property string $fecha_inicio
 * @property string $fecha_fin
 * @property string $Logica_id
 * @property integer $orden
 * @property string $fecha_inicio_ci
 * @property string $fecha_fin_ci
 * @property integer $es_final
 * @property string $nombre_unico
 * @property integer $imprime_en_boletin
 *
 * @property Conducta[] $conductas
 * @property Inasistencia[] $inasistencias
 * @property LogicaItem[] $logicaItems
 * @property Logica $logica
 */
abstract class BaseLogicaPeriodo extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'logica_periodo';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'LogicaPeriodo|LogicaPeriodos', $n);
	}

	public static function representingColumn() {
		return 'nombre';
	}

	public function rules() {
		return array(
			array('nombre, abrev, Logica_id', 'required'),
			array('orden, es_final, imprime_en_boletin', 'numerical', 'integerOnly'=>true),
			array('nombre, abrev', 'length', 'max'=>45),
			array('Logica_id', 'length', 'max'=>10),
			array('nombre_unico', 'length', 'max'=>25),
			array('fecha_inicio, fecha_fin, fecha_inicio_ci, fecha_fin_ci', 'safe'),
			array('fecha_inicio, fecha_fin, orden, fecha_inicio_ci, fecha_fin_ci, es_final, nombre_unico, imprime_en_boletin', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, nombre, abrev, fecha_inicio, fecha_fin, Logica_id, orden, fecha_inicio_ci, fecha_fin_ci, es_final, nombre_unico, imprime_en_boletin', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'conductas' => array(self::HAS_MANY, 'Conducta', 'logica_periodo_id'),
			'inasistencias' => array(self::HAS_MANY, 'Inasistencia', 'logica_periodo_id'),
			'logicaItems' => array(self::HAS_MANY, 'LogicaItem', 'Logica_Periodo_id'),
			'logica' => array(self::BELONGS_TO, 'Logica', 'Logica_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'nombre' => Yii::t('app', 'Nombre'),
			'abrev' => Yii::t('app', 'Abrev'),
			'fecha_inicio' => Yii::t('app', 'Fecha Inicio'),
			'fecha_fin' => Yii::t('app', 'Fecha Fin'),
			'Logica_id' => null,
			'orden' => Yii::t('app', 'Orden'),
			'fecha_inicio_ci' => Yii::t('app', 'Fecha Inicio Ci'),
			'fecha_fin_ci' => Yii::t('app', 'Fecha Fin Ci'),
			'es_final' => Yii::t('app', 'Es Final'),
			'nombre_unico' => Yii::t('app', 'Nombre Unico'),
			'imprime_en_boletin' => Yii::t('app', 'Imprime En Boletin'),
			'conductas' => null,
			'inasistencias' => null,
			'logicaItems' => null,
			'logica' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('nombre', $this->nombre, true);
		$criteria->compare('abrev', $this->abrev, true);
		$criteria->compare('fecha_inicio', $this->fecha_inicio, true);
		$criteria->compare('fecha_fin', $this->fecha_fin, true);
		$criteria->compare('Logica_id', $this->Logica_id);
		$criteria->compare('orden', $this->orden);
		$criteria->compare('fecha_inicio_ci', $this->fecha_inicio_ci, true);
		$criteria->compare('fecha_fin_ci', $this->fecha_fin_ci, true);
		$criteria->compare('es_final', $this->es_final);
		$criteria->compare('nombre_unico', $this->nombre_unico, true);
		$criteria->compare('imprime_en_boletin', $this->imprime_en_boletin);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}