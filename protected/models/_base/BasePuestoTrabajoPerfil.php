<?php

/**
 * This is the model base class for the table "puesto_trabajo_perfil".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PuestoTrabajoPerfil".
 *
 * Columns in table "puesto_trabajo_perfil" available as properties of the model,
 * followed by relations of table "puesto_trabajo_perfil" available as properties of the model.
 *
 * @property string $id
 * @property string $talonario_id
 * @property string $destino_id
 * @property integer $puesto_trabajo_id
 *
 * @property Destino $destino
 * @property Talonario $talonario
 * @property PuestoTrabajo $puestoTrabajo
 */
abstract class BasePuestoTrabajoPerfil extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'puesto_trabajo_perfil';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'PuestoTrabajoPerfil|PuestoTrabajoPerfils', $n);
	}

	public static function representingColumn() {
		return 'id';
	}

	public function rules() {
		return array(
			array('talonario_id, puesto_trabajo_id', 'required'),
			array('puesto_trabajo_id', 'numerical', 'integerOnly'=>true),
			array('talonario_id, destino_id', 'length', 'max'=>10),
			array('destino_id', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, talonario_id, destino_id, puesto_trabajo_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'destino' => array(self::BELONGS_TO, 'Destino', 'destino_id'),
			'talonario' => array(self::BELONGS_TO, 'Talonario', 'talonario_id'),
			'puestoTrabajo' => array(self::BELONGS_TO, 'PuestoTrabajo', 'puesto_trabajo_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'talonario_id' => null,
			'destino_id' => null,
			'puesto_trabajo_id' => null,
			'destino' => null,
			'talonario' => null,
			'puestoTrabajo' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('talonario_id', $this->talonario_id);
		$criteria->compare('destino_id', $this->destino_id);
		$criteria->compare('puesto_trabajo_id', $this->puesto_trabajo_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}