<?php

/**
 * This is the model base class for the table "reporte_param".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ReporteParam".
 *
 * Columns in table "reporte_param" available as properties of the model,
 * followed by relations of table "reporte_param" available as properties of the model.
 *
 * @property string $id
 * @property string $nombre
 * @property string $tipo
 * @property string $label
 * @property string $default
 * @property string $reporte_id
 *
 * @property Reporte $reporte
 */
abstract class BaseReporteParam extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'reporte_param';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'ReporteParam|ReporteParams', $n);
	}

	public static function representingColumn() {
		return 'nombre';
	}

	public function rules() {
		return array(
			array('nombre, tipo, reporte_id', 'required'),
			array('nombre, tipo, label, default', 'length', 'max'=>45),
			array('reporte_id', 'length', 'max'=>10),
			array('label, default', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, nombre, tipo, label, default, reporte_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'reporte' => array(self::BELONGS_TO, 'Reporte', 'reporte_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'nombre' => Yii::t('app', 'Nombre'),
			'tipo' => Yii::t('app', 'Tipo'),
			'label' => Yii::t('app', 'Label'),
			'default' => Yii::t('app', 'Default'),
			'reporte_id' => null,
			'reporte' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('nombre', $this->nombre, true);
		$criteria->compare('tipo', $this->tipo, true);
		$criteria->compare('label', $this->label, true);
		$criteria->compare('default', $this->default, true);
		$criteria->compare('reporte_id', $this->reporte_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}