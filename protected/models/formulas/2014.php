<?php

	class Formulas extends stdClass {

		public static function getNota($nivel_id, $nota, $logicaItem, &$notas, $integradora) {
			switch ($nivel_id) {
				case 1:
					$nota = self::getNotaInicial($logicaItem, $nota, $notas, $integradora);
					break;
				case 2:
					$nota = self::getNotaEP1($logicaItem, $nota, $notas, $integradora);
					break;
				case 3:
					$nota = self::getNotaEP2($logicaItem, $nota, $notas, $integradora);
					break;
				case 4:
					//ve($notas);
					$nota = self::getNotaES($logicaItem, $nota, $notas, $integradora);
					break;
				default:
					throw new Exception("No hay una lógica para este nivel ($nivel_id)");
					break;
			}
			return $nota;
		}

		public static function getNotaFinal($nivel_id, $alumno_division_id, &$notas) {
			switch ($nivel_id) {
				case 1:
					break;
				case 2:
					$promedio = Helpers::qryScalar("
                    select desempenio 
                        from alumno_division ad
                            inner join desempenio d on d.alumno_division_id = ad.id
                    where ad.id = $alumno_division_id
                ");
					$aprobado = in_array($promedio, array("AS", "S", "MS", "B", "MB"));
					break;
				case 3 or 4: // ES
					$cant = 0;
					$suma = 0;
					$desaprobado = false;
					foreach ($notas as $notasMateria) {
						$suma += $notasMateria["CD"]["nota"];
						$cant++;
//                    if ($notasMateria["Final"]["nota"] < 7 and $notasMateria["Dic"]["nota"] < 4 and $notasMateria["fm"]["nota"] < 4) {
						if ($notasMateria["CD"]["estado"] == "Desaprobado") {
							$desaprobado = true;
						}
					}
					if (!$desaprobado) {
						$promedio = round($suma / $cant, 2);
						$aprobado = true;
					} else {
						$promedio = 0;
						$aprobado = false;
					}
					break;
				default:
					throw new Exception("No hay una lógica para este nivel ($nivel_id)");
					break;
			}
			return array("nota" => $promedio, "aprobado" => $aprobado);
		}

		public static function getNotaInicial($logicaItem, $nota, &$notas, $integradora) {
			$nota["nota"] = Trim(strtoupper($nota["nota"]));
			return $nota;
		}

		public static function getNotaEP1($logicaItem, $nota, &$notas, $integradora) {
			$nota["nota"] = Trim(strtoupper($nota["nota"]));
			if (($nota["manual"] == 1) and ( !in_array($nota["nota"], array(/*"MS", "S", "AS", "ANS", "EX", */"BUENA", "MUY BUENA", "REGULAR"))) and $nota["nota"]) {
				$nota["error"] = "La nota fué mal cargada, solo puede cargar MS, S, AS ANS y EX";
			}
			if (!$nota["error"]) {
				switch ($logicaItem["logica_item_nombre_unico"]) {
					case "T1":
						$nota["nota"] = trim($nota["nota"]);
						break;
					case "CD":
						$nota["nota"] = trim($notas["NF"]["nota"]);
//						$nota["nota"] = $nota["nota"] ? $nota["nota"] : "";
						break;
					default:
//						vd($logicaItem);
						$nota["nota"] = $nota["nota"] ? $nota["nota"] : "";
						break;
				}
			}
			return $nota;
		}

		public static function getNotaEP2($logicaItem, $nota, &$notas, $integradora) {
			$nota["nota"] = trim($nota["nota"]);
			if ($nota["nota"] and ! is_numeric($nota["nota"])) {
				$nota["error"] = "La nota debe ser numérica";
			} else if ($nota["nota"] and $nota["nota"] > 10) {
				$nota["error"] = "La nota no puede ser mayor a 10";
			}
			if (!$nota["error"]) {
				switch ($logicaItem["logica_item_nombre_unico"]) {
					case "NFF":
						if (!$notas["T33"]["nota"]) {
							$n = "";
						} else {
							$n = round(($notas["T11"]["nota"] + $notas["T22"]["nota"] + $notas["T33"]["nota"]) / 3, 0);
						}
						$nota["nota"] = $n >= 7 ? $n : "";
						break;
					case "CD":
						if ((!$notas["NF"]["nota"]) or ( $notas["NF"]["nota"] < 7)) {
							$dic = $notas["Dic"]["nota"];
							$fm = $notas["fm"]["nota"];
							if ($fm < 4 and $dic < 4) {
								$nota["estado"] = "Desaprobado";
								$nota["nota"] = "";
							} elseif ($fm >= 4) {
								$nota["nota"] = $fm;
							} elseif ($dic >= 4) {
								$nota["nota"] = $dic;
							}
//                        if ($nota["nota"] < 4) {
//                            if ($fm < 4) {
//                                $nota["nota"] = "";
//                                $nota["estado"] = "Desaprobado";
//                            } elseif ($fm >= 4) {
//                                $nota["nota"] = $fm;
//                            }
//                        }
							//                  $nota["nota"] = $n >= 7 ? $n : "";
						} else {
							$n = round($notas["NF"]["nota"], 2);
							$nota["nota"] = $n >= 7 ? $n : "";
						}
						break;
					default:
						$nota["nota"] = $nota["nota"] ? $nota["nota"] : "";
						break;
				}
			}
			return $nota;
		}

		public static function getNotaES($logicaItem, $nota, &$notas, $integradora) {
			$nota["nota"] = trim($nota["nota"]);
			$nota["estado"] = "Aprobado";
			if (strtoupper($nota["nota"]) == "AU") {
				$nota["nota"] = "AU";
			} elseif ($nota["nota"] and ! is_numeric($nota["nota"])) {
				$nota["error"] = "La nota debe ser numérica";
			} else if ($nota["nota"] and $nota["nota"] > 10) {
				$nota["error"] = "La nota no puede ser mayor a 10";
			}
//        if (!$nota["error"] /*or $nota["error"]*/) {
			switch ($logicaItem["logica_item_nombre_unico"]) {
				case "t1":
					$n = round(($notas["n1t1"]["nota"] + $notas["n2t1"]["nota"] + $notas["n3t1"]["nota"]) / 3, 0);
					$nota["nota"] = $n == 0 ? "" : $n;
					if ($n < 7) {
						$nota["estado"] = "Desaprobado";
					}
					break;

				case "t2":
					$n = round(($notas["n1t2"]["nota"] + $notas["n2t2"]["nota"] + $notas["n3t2"]["nota"]) / 3, 0);
					$nota["nota"] = $n == 0 ? "" : $n;
					if ($n < 7) {
						$nota["estado"] = "Desaprobado";
					}
					break;

				case "3T":
					$n = round(($notas["n1t3"]["nota"] + $notas["n2t3"]["nota"] + $notas["n3t3"]["nota"]) / 3, 0);
					$nota["nota"] = ($n == 0) ? "" : $n;
					if ($n < 7) {
						$nota["estado"] = "Desaprobado";
					}
					break;

				case "t3":
					if ($integradora) {
						$n = round(($notas["3T"]["nota"] + $notas["INT"]["nota"] ) / 2, 0);
					} else {
						$n = round(($notas["n1t3"]["nota"] + $notas["n2t3"]["nota"] + $notas["n3t3"]["nota"]) / 3, 0);
					}
					$nota["nota"] = $n == 0 ? "" : $n;
					if ($n < 7) {
						$nota["estado"] = "Desaprobado";
					}
					break;
				case "Final":
					//if ($integradora) {
//                    $n = round(($notas["t3"]["nota"] + $notas["INT"]["nota"] ) / 2, 0);
					//} else {
					//vd($notas);
					$n = round(($notas["t1"]["nota"] + $notas["t2"]["nota"] + $notas["t3"]["nota"] ) / 3, 2);
					$p = explode(".", $n);
					if (count($p) > 1 and $p[1] == "67") {
						$n = $p[0] . "." . "66";
					}
					$nota["nota"] = $n == 0 ? "" : $n;
					if ($n < 7) {
						$nota["estado"] = "Desaprobado";
					}
					//$nota["nota"]="x";
					break;
				case "Final":
					$dic = $notas["DIC"]["nota"];
					$n = round(($notas["t1"]["nota"] + $notas["t2"]["nota"] + $notas["t3"]["nota"] ) / 3, 2);
					$p = explode(".", $n);
					if (count($p) > 1 and $p[1] == "67") {
						$n = $p[0] . "." . "66";
					}
//                $nota["estado"] = ($nota["estado"] == ($n < 7)) ? "2" : "0";
//                if ($notas["t3"]["nota"] < 4) {
//                    $n = "";
//                }
					$nota["nota"] = $n;
					break;

				case "CD":
					$dic = $notas["Dic"]["nota"];
					$fm = $notas["fm"]["nota"];
					if (!$dic and ! $fm) {
						$nota['nota'] = $notas['Final']['nota'];
						$nota['estado'] = $notas['Final']['estado'];
						break;
					}
					if ($notas["t3"]["nota"] < 4 or $notas["Final"]["nota"] < 7) {
						if ($dic < 4 and $fm < 4) {
							$nota["estado"] = "Desaprobado";
							$nota["nota"] = "";
						} elseif ($fm >= 4) {
							$nota["nota"] = $fm;
						} elseif ($dic >= 4) {
							$nota["nota"] = $dic;
						}
						if ($nota["nota"] < 4) {
							if ($fm < 4) {
								$nota["estado"] = "Desaprobado";
								$nota["nota"] = "";
							} elseif ($fm >= 4) {
								$nota["nota"] = $fm;
							}
						}
						break;
					} else {
						$n = $notas["Final"]["nota"];
						$notas["Dic"]["nota"] = "";
					}
					$nota["nota"] = $n;
					break;
				default:
					$nota["nota"] = $nota["nota"] ? $nota["nota"] : "";
					break;
				//         }
			}
			if (in_array($logicaItem["logica_item_nombre_unico"], array(""))) {
				
			}
			return $nota;
		}

	}

?>