<?php

include_once("formulas/" . Ciclo::getCicloNombreParaCargaDeNotas() . ".php");

class LogicaActiva extends stdClass {

	public static $notas, $nivel_id, $cuali;
	private static $periodoEsAnterior, $periodoEsActual, $periodoEsPosterior, $logica_id, $alumno_division_id;

	public static function getNotasDivisionAsignatura($division_id, $asignatura_id, $logica_periodo_id = null, $cuali) {
    self::$cuali = $cuali==1;
		self::$nivel_id = Nivel::getNivelIdDesdeDivision($division_id);
		$nivel_id = Helpers::qryScalar("SELECT n.id FROM nivel n
			  INNER JOIN anio a ON n.id = a.Nivel_id
			  INNER JOIN division d ON a.id = d.Anio_id AND d.id = $division_id");
		$order = $nivel_id == 4 ?
			"a.apellido, a.nombre" :
			"a.apellido, a.nombre";
		$notas = array();
		$logica_id = null;
		//@TODO: and ae.activo_edu es una condición que cuando esté configurado un ciclo anterior va a hacer que no functione
		$asignatura_tipo_id = AsignaturaTipo::AsignaturaTipoIdDesdeAsignaturaId($asignatura_id);
    $ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
		$select = "
				select ad.id, ad.ciclo_id
						from alumno_division ad
								inner join alumno a on a.id = ad.`Alumno_id` and a.activo = 1
								inner join alumno_estado ae on ae.id = a.estado_id
								inner join alumno_division_estado ade on ade.id = ad.alumno_division_estado_id and ade.muestra_edu
						where ad.division_id = $division_id
                  and ad.Ciclo_id = $ciclo_id
									and not ad.borrado and ad.activo and ae.activo_edu
									/*and (ad.activo or ad.promocionado or ad.egresado)
									and (ad.activo or ad.promocionado or ad.egresado) AND !ae.ingresante*/
						      /*and (ae.activo_edu or ad.promocionado or ad.egresado)*/
						      /*and a.apellido like '%pais%'*/
						order by $order
				";
		$alumnoDivisiones = Yii::app()->db->createCommand($select)->queryAll();
//					vd($alumnoDivisiones);
		foreach ($alumnoDivisiones as $alumnoDivision) {
			$logica_id = $logica_id ? $logica_id : Logica::getLogicaId($asignatura_tipo_id, $alumnoDivision["ciclo_id"]);
			//vd($alumnoDivision["id"]);
			$notas[$alumnoDivision["id"]] = self::getNotasAlumnoAsignatura($alumnoDivision["id"], $asignatura_id, $logica_id, null, $logica_periodo_id);
		}
		return $notas;
	}

	static public function procesaNotas($alumnoDivision, $asignatura, $notasAProcesar, $logica_periodo_id) {
		self::$notas = array();
		/* @var $asignatura Asignatura */
		/* @var $alumnoDivision AlumnoDivision */
		$logica_id = Logica::getLogicaId($asignatura->Asignatura_Tipo_id, $alumnoDivision->Ciclo_id);
		self::$nivel_id = self::$nivel_id ? self::$nivel_id : Yii::app()->db->createCommand("
            select a.nivel_id from alumno_division ad
                inner join division d on d.id = ad.Division_id
                  inner join anio a on a.id = d.anio_id
              where ad.id = $alumnoDivision->id")->queryScalar();

		$items = Yii::app()->db->createCommand("
            select li.condicion, lp.orden as logica_periodo_orden, li.nombre_unico as logica_item_nombre_unico, li.logica_periodo_id,
                   li.orden as logica_item_orden, li.manual, li.id, li.formula, li.nombre
              from logica_item li
                inner join logica_periodo lp on lp.id = li.logica_periodo_id
              where li.logica_id = $logica_id
              order by lp.orden, li.orden
            ")->queryAll();

		foreach ($items as $item) {
			if (isset($notasAProcesar[$item["id"]])) {
				$nota = array(
					"nota" => $item["manual"] == 0 ? 0 : $notasAProcesar[$item["id"]],
					"error" => "",
					"logica_periodo_id" => $item["logica_periodo_id"],
					"manual" => $item["manual"],
					"logica_item_id" => $item["id"],
					"nombre" => $item["nombre"],
					"nombre_unico" => $item["logica_item_nombre_unico"],
					"estado" => null,
				);
				LogicaActiva::getValidaNota($alumnoDivision->id, $asignatura->id, $item, $nota, $logica_periodo_id);
			}
		}
		//vd(self::$notas);
		return self::$notas;
	}

	static public function getValidaNota($alumno_division_id, $asignatura_id, $logicaItem, $nota, $logicaActivaId) {
		$integradora = Helpers::qryScalar("select da.integradora
					from division_asignatura da
							inner join alumno_division ad on ad.Division_id = da.Division_id
					where da.Asignatura_id = $asignatura_id and ad.id = $alumno_division_id
			");
    $id = $logicaItem["id"];
		if (!$nota) {
			if ($logicaItem["manual"]) {
				$nota = Yii::app()->db->createCommand("
							select nota, texto, li.manual, logica_periodo_id, li.id as logica_item_id, \"\" as error,
													li.nombre, li.nombre_unico as logica_item_nombre_unico, tipo_nota, estado, li.tipo_nota,
													li.imprime_en_boletin, li.imprime_en_planilla, lp.abrev as logica_periodo_abrev, li.abrev as logica_item_abrev
								from nota n
									inner join logica_item li on li.id = n.logica_item_id
									inner join logica_periodo lp on lp.id = li.logica_periodo_id
								where Alumno_Division_id = $alumno_division_id and
									Asignatura_id = $asignatura_id and
									Logica_Item_id = $id
          ")->queryRow();
          // $nota["nota"] = self::$cuali ? $nota["text"] : $nota["nota"];
        }
        if (!isset($nota) or !$nota or !$logicaItem["manual"]) {
          $nota = array(
            "tipo_nota" => $logicaItem["tipo_nota"],
            "nota" => "",
            "texto" => "",
            "error" => "",
            "logica_periodo_id" => $logicaItem["logica_periodo_id"],
            "manual" => $logicaItem["manual"],
            "logica_item_id" => $logicaItem["id"],
            "nombre" => $logicaItem["nombre"],
            "logica_item_nombre_unico" => $logicaItem["logica_item_nombre_unico"],
            "estado" => isset($logicaItem["estado"]) ? $logicaItem["estado"] : "na",
            "imprime_en_boletin" => isset($logicaItem["imprime_en_boletin"]) ? $logicaItem["imprime_en_boletin"] : "na",
            "imprime_en_planilla" => isset($logicaItem["imprime_en_planilla"]) ? $logicaItem["imprime_en_planilla"] : "na",
            "logica_periodo_abreviacion" => isset($logicaItem["abreviacion"]) ? $logicaItem["abreviacion"] : "na",
            "logica_item_abreviacion" => isset($logicaItem["abreviacion"]) ? $logicaItem["abreviacion"] : "na",
          );
			}
		}
		// ve2($logicaItem["logica_item_nombre_unico"], $nota);
    self::$notas[$logicaItem["logica_item_nombre_unico"]] = $nota;
		$orden = $logicaActivaId ? Yii::app()->db->createCommand("
                      select orden from logica_periodo where id = $logicaActivaId
        ")->queryScalar() : null;
		if ($orden) {
			self::$periodoEsActual = $logicaItem["logica_periodo_orden"] == $orden;
			self::$periodoEsAnterior = $logicaItem["logica_periodo_orden"] < $orden;
			self::$periodoEsPosterior = $logicaItem["logica_periodo_orden"] > $orden;
		} else {
			self::$periodoEsActual = true;
			self::$periodoEsAnterior = false;
			self::$periodoEsPosterior = false;
    }
    if(!self::$cuali){
      self::$notas[$logicaItem["logica_item_nombre_unico"]] = Formulas::getNota(self::$nivel_id, $nota, $logicaItem, self::$notas, $integradora);
    } else {
      $nota['nota'] = $nota['texto'];
      self::$notas[$logicaItem["logica_item_nombre_unico"]] = $nota;
    }
		return self::$notas[$logicaItem["logica_item_nombre_unico"]];
	}

	static public function getNotasAlumnoAsignaturaPeriodos($alumno_division_id, $asignatura_id
		, $nivel_id = null, $logica_id = null, $items = null, $tipo_nota = null) {
		//vd($alumno_division_id, $asignatura_id, $logica_periodo_id, $nivel_id, $logica_id, $items);
		$nivel_id = $nivel_id ? $nivel_id : Helpers::qryScalar("
                select nivel.id
                    from nivel
                        inner join anio a on a.Nivel_id = nivel.id
                        inner join division d on d.Anio_id = a.id
                        inner join alumno_division ad on ad.division_id = d.id and ad.id = $alumno_division_id");
		self::$nivel_id = $nivel_id;

		$logica_id = $logica_id ? $logica_id : Logica::getLogicaIdPorAsignatura($asignatura_id, Ciclo::getActivo());

		$notas = array();

		$items = $items ? $items : Yii::app()->db->createCommand("
            select lp.id as logica_periodo_id, li.condicion, lp.orden as logica_periodo_orden, li.logica_periodo_id,
                    li.nombre_unico as logica_item_nombre_unico, lp.nombre_unico as logica_periodo_nombre_unico,
                    li.orden as logica_item_orden, li.manual, li.id, li.formula, li.nombre, li.orden as logica_item_orden,
                    lp.fecha_inicio_ci as logica_periodo_fecha_inicio, lp.fecha_fin_ci as logica_periodo_fecha_fin, tipo_nota, li.estado
              from logica_item li
                inner join logica_periodo lp on lp.id = li.logica_periodo_id
              where li.logica_id = $logica_id
              order by lp.orden, li.orden
        ")->queryAll();
		foreach ($items as $item) {
			$nota = LogicaActiva::getValidaNota($alumno_division_id, $asignatura_id, $item, null, $item["logica_periodo_id"]);
			$nota["logica_periodo_orden"] = $item["logica_periodo_orden"];
			$nota["logica_periodo_orden"] = $item["logica_periodo_orden"];
			$nota["logica_item_orden"] = $item["logica_item_orden"];
			$notas[$item["logica_periodo_nombre_unico"]][$item["logica_item_nombre_unico"]] = $nota;
		}
		return $notas;
	}

	static public function getNotasAlumnoAsignaturaPeriodo($alumno_division_id, $asignatura_id, $logica_periodo_id
		, $nivel_id = null, $logica_id = null, $items = null, $tipo_nota = null) {
		$periodos = self::getNotasAlumnoAsignaturaPeriodos($alumno_division_id, $asignatura_id
			, $nivel_id, $logica_id, $items, $tipo_nota);
		//vd($periodos);
		//return $periodos["$logica_periodo_id"] ;
		$notas = array();
		foreach ($periodos["$logica_periodo_id"] as $key => $nota) {
			if ((!$tipo_nota) or ($tipo_nota and $nota["tipo_nota"] == $tipo_nota)) {
				$notas[$key] = $nota;
			}
		}
		return $notas;
	}

	public static function itemVisible($item, $division_asignatura_id) {
		$integradora = Helpers::qryScalar("select coalesce(da.integradora,0)
	from division_asignatura da
	where da.id = $division_asignatura_id");
		return !($item["tipo_nota"] == 100 and !$integradora);
	}

	public static function getNotasAlumnoAsignatura($alumno_division_id, $asignatura_id, $logica_id, $logica_item_id = null, $logicaActivaId = null, $notasAProcesar = array()) {
		$whereLI = $logica_item_id ? " and li.id = $logica_item_id" : "";
		self::$notas = array();
		self::$nivel_id = self::$nivel_id ? self::$nivel_id : Nivel::getNivelIdDesdeAlumnoDivision($alumno_division_id);
		$integradora = Helpers::qryScalar("select coalesce(da.integradora,0)
				from division_asignatura da
					inner join alumno_division ad on ad.Division_id = da.Division_id
				where da.Asignatura_id = $asignatura_id and ad.id = $alumno_division_id");
		$division_asignatura_id = Helpers::qryScalar("
				select da.id
						from division_asignatura da
								inner join alumno_division ad on ad.Division_id = da.Division_id
                where asignatura_id = $asignatura_id and ad.id = $alumno_division_id");
		$items = Yii::app()->db->createCommand("
				select lp.id as logica_periodo_id, li.condicion, lp.orden as logica_periodo_orden, li.nombre_unico as logica_item_nombre_unico, li.logica_periodo_id,
							 li.orden as logica_item_orden, li.manual, li.id, li.formula, li.nombre, li.tipo_nota, li.estado
					from logica_item li
						inner join logica_periodo lp on lp.id = li.logica_periodo_id $whereLI
					where li.logica_id = $logica_id /*and not (li.tipo_nota=100 and $integradora <> 1)*/
					order by lp.orden, li.orden
			")->queryAll();
		foreach ($items as $item) {
			$visible = LogicaActiva::itemVisible($item, $division_asignatura_id);
			if ($visible) {
				if (isset($notasAProcesar[$item["id"]])) {
					$nota = array(
						"nota" => $notasAProcesar[$item["id"]],
						"error" => "",
						"logica_periodo_id" => $item["logica_periodo_id"],
						"manual" => $item["manual"],
						"logica_item_id" => $item["id"],
						"nombre" => $item["nombre"],
						"nombre_unico" => $item["logica_item_nombre_unico"],
					);
				} else {
					$nota = null;
				}
				LogicaActiva::getValidaNota($alumno_division_id, $asignatura_id, $item, $nota, $logicaActivaId);
			}
		}
		return self::$notas;
	}

	/*
	 *  Para Stickers
	 */

	public static function getNotasDivision($division_id, $logica_periodo_id = null, $alumno_id = null) {
    // ve2('$division_id', $division_id, '$logica_periodo_id',$logica_periodo_id, '$alumno_id', $alumno_id);
    $esCicloActual = Ciclo::esActual();
    $andActivo = $esCicloActual ? " and ad.activo  " : " ";
    $whereAlumno = $alumno_id ? " a.id = $alumno_id" : " true ";
		$ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
		$notas = array();
		$s = "
                select ad.id, concat(a.apellido,\", \", a.nombre) as alumno, a.id as alumno_id,
                							a.matricula, d.nombre as division, an.nombre as anio, n.nombre as nivel,
                							concat(asi.nombre, \" \", ns.nombre) as siguiente_division
                    from alumno_division ad
                        inner join alumno a on a.id = ad.alumno_id
                        inner join division d on d.id = ad.division_id
                        left join division ds on ds.id = d.division_id_siguiente
                        inner join anio an on an.id = d.anio_id
                        left join anio asi on asi.id = ds.Anio_id
                        inner join nivel n on n.id = an.nivel_id
                        left join nivel ns on ns.id = asi.Nivel_id
                        inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_edu
                        inner join alumno_division_estado e ON e.id = ad.alumno_division_estado_id AND e.muestra_edu
                    where ad.division_id=$division_id and $whereAlumno
                    	and ad.ciclo_id = $ciclo_id $andActivo and e.muestra_edu = 1
                    	and ad.borrado = 0 /*and a.matricula = 3358*/ /*((ad.activo or ad.promocionado or ad.egresado)*//* and apellido like \"%Ballina%\"*/
                order by a.sexo, a.apellido, a.nombre";
                // vd2($s);
		$alumnos_divisiones = Helpers::qryAll($s);
//        foreach ($alumnos_divisiones as $ad) {
//            $notas[$ad["alumno"]] = self::getNotasAlumnoDivision($ad["id"]);
//        }
//        vd($alumnos_divisiones);
//		foreach($alumnos_divisiones as $a){
//			ve($a['alumno']);
//		}
//		die;
		foreach ($alumnos_divisiones as $alu) {
      $notas[$alu["alumno"]]["notas"] = self::getNotasAlumnoDivision($alu["id"]);
			$notas[$alu["alumno"]]["inasistencias"] = self::getInasistenciasAlumnoDivision($alu["id"]);
			$notas[$alu["alumno"]]["conducta"] = self::getConductaAlumnoDivision($alu["id"]);
//	  vd($notas[$alu["alumno"]]["conducta"]);
			$notas[$alu["alumno"]]["datos"] = array(
				"alumno_id" => $alu["alumno_id"],
				"matricula" => $alu["matricula"],
				"nivel" => $alu["nivel"],
				"anio" => $alu["anio"],
				"division" => $alu["division"],
				"siguiente_division" => $alu["siguiente_division"],
			);
		}
		return $notas;
	}

	public static function getNotasAlumnoDivision($alumno_division_id) {
		$asignaturas = Helpers::qryAll("
            select da.asignatura_id, a.nombre
                from alumno_division ad
                    inner join division d on d.id = ad.Division_id
                    inner join division_asignatura da on da.division_id = d.id
                    inner join asignatura a on a.id = da.asignatura_id
                where ad.id = $alumno_division_id
                order by case when da.orden = 0 or da.orden is null then a.orden else da.orden end
        ");
		foreach ($asignaturas as $a) {
			$ret[$a["nombre"]] = self::getNotasAlumnoDivisionAsignatura($alumno_division_id, $a["asignatura_id"]);
		}
		return $ret;
	}

	public static function getNotasAlumnoDivisionAsignatura($alumno_division_id, $asignatura_id) {
		self::$notas = array();
		self::$alumno_division_id = $alumno_division_id;
		self::$nivel_id = self::$nivel_id ? self::$nivel_id : Nivel::getNivelIdDesdeAlumnoDivision($alumno_division_id);
    $ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
		$ad = Yii::app()->db->createCommand("
            select ad.id, ad.ciclo_id
                from alumno_division ad
                    inner join alumno a on a.id = ad.`Alumno_id` /*and a.activo*/
                where ad.id = $alumno_division_id and ad.ciclo_id = $ciclo_id
																						and !ad.borrado /*and ad.activo*/ /*((ad.activo or ad.promocionado or ad.egresado)*/
                order by a.sexo desc, a.apellido, a.nombre
            ")->queryRow();

		$asignatura_tipo_id = AsignaturaTipo::AsignaturaTipoIdDesdeAsignaturaId($asignatura_id);
		$logica_id = Logica::getLogicaId($asignatura_tipo_id, $ad["ciclo_id"]);

		$integradora = Helpers::qryScalar("select coalesce(da.integradora,0)
                from division_asignatura da
                    inner join alumno_division ad on ad.Division_id = da.Division_id
                where da.Asignatura_id = $asignatura_id and ad.id = $alumno_division_id");
        self::$notas['integradora'] = $integradora;
		$division_asignatura_id = Helpers::qryScalar("
                select da.id
                    from division_asignatura da
                        inner join alumno_division ad on ad.Division_id = da.Division_id
                        where asignatura_id = $asignatura_id and ad.id = $alumno_division_id");
    /*TODO: Quitar */
    if($logica_id){
      $items = Yii::app()->db->createCommand("
        select lp.id as logica_periodo_id, li.condicion, lp.orden as logica_periodo_orden,
          li.nombre_unico as logica_item_nombre_unico, li.logica_periodo_id,
          li.orden as logica_item_orden, li.manual, li.id, li.nombre, li.tipo_nota,
          li.estado, li.tiene_conducta, li.imprime_en_boletin, li.imprime_en_planilla
        from logica_item li
          inner join logica_periodo lp on lp.id = li.logica_periodo_id
        where li.logica_id = $logica_id /*and not (li.tipo_nota=100 and $integradora <> 1)*/
        order by lp.orden, li.orden
      ")->queryAll();
      foreach ($items as $item) {
        $visible = LogicaActiva::itemVisible($item, $division_asignatura_id);
        if ($visible) {
          $nota = null;
          LogicaActiva::getValidaNota($alumno_division_id, $asignatura_id, $item, $nota, null);
        }
      }
      //vd2(self::$notas);
    }
		return self::$notas;
	}

	public static function getInasistenciasAlumnoDivision($alumno_division_id) {
		$logica_id = Logica::getLogicaIdPorAlumnoDivison($alumno_division_id);
		$nivel_id = Nivel::getNivelIdDesdeAlumnoDivision($alumno_division_id);
		$lpSelect = "
				select lp.id as logica_periodo_id, lp.orden as logica_periodo_orden,
						lp.fecha_inicio_ci, lp.fecha_fin_ci, n.inasistencia_detallada
					from logica_periodo lp
		                    inner join nivel n on n.id = $nivel_id
					where lp.logica_id = $logica_id
					order by lp.orden
			";
		$lps = Yii::app()->db->createCommand($lpSelect)->queryAll();
		$totalInasistencias = 0;
		foreach ($lps as $lp) {
			$fecha_desde = $lp["fecha_inicio_ci"];
			$fecha_hasta = $lp["fecha_fin_ci"];
			$logica_periodo_id = $lp["logica_periodo_id"];
			if ($lp["inasistencia_detallada"] == 1) {
				$logica_periodo_id = $lp["logica_periodo_id"];
				$sInasistencias = "
							select sum(t.valor) as inasistencias
							from inasistencia i
											inner join inasistencia_detalle d on d.inasistencia_id = i.id
											inner join inasistencia_tipo t on t.id = d.inasistencia_tipo_id
							where i.Alumno_Division_id = $alumno_division_id and i.logica_periodo_id = $logica_periodo_id
							/*where i.Alumno_Division_id = $alumno_division_id and d.fecha between \"$fecha_desde\" and \"$fecha_hasta\"*/
						";
				$inasistencias = Helpers::qryScalar($sInasistencias);
				if ($alumno_division_id == 14168) {
					//		ve($sInasistencias, $inasistencias);
				}
				$inasistenciasRet[$lp["logica_periodo_id"]] = $inasistencias;
			} else {
				$inasistencias = Helpers::qryScalar("
                                select cantidad as inasistencias
                                  from inasistencia i
                                  where i.Alumno_Division_id = $alumno_division_id and
                                        i.logica_periodo_id = $logica_periodo_id
                            ");
				$inasistenciasRet[$lp["logica_periodo_id"]] = $inasistencias;
			}
			$totalInasistencias += $inasistencias;
		}

		return array("inasistencias" => $inasistenciasRet, "totalInasistencias" => $totalInasistencias);
	}

	/*
	 * Borrar la de abajo si anda la de arriba
	 */

	public static function getInasistenciasAlumnoDivisionAnt($alumno_division_id) {
		$logica_id = Logica::getLogicaIdPorAlumnoDivison($alumno_division_id);
		$nivel_id = Nivel::getNivelIdDesdeAlumnoDivision($alumno_division_id);
		$itemsSelect = "
                select lp.id as logica_periodo_id, li.condicion, lp.orden as logica_periodo_orden, li.nombre_unico as logica_item_nombre_unico, li.logica_periodo_id,
                       li.orden as logica_item_orden, li.manual, li.id, li.formula, li.nombre, li.tipo_nota, li.estado, li.tiene_conducta,
                       n.inasistencia_detallada, lp.fecha_inicio_ci, lp.fecha_fin_ci
                  from logica_item li
                    inner join logica_periodo lp on lp.id = li.logica_periodo_id
                    inner join nivel n on n.id = $nivel_id
                  where li.logica_id = $logica_id
                  order by lp.orden, li.orden
        ";
		$items = Yii::app()->db->createCommand($itemsSelect)->queryAll();
		$totalInasistencias = 0;
		foreach ($items as $item) {
			if ($item["tiene_conducta"] == 1) {
				$fecha_desde = $item["fecha_inicio_ci"];
				$fecha_hasta = $item["fecha_fin_ci"];
				$logica_periodo_id = $item["logica_periodo_id"];
				if ($item["inasistencia_detallada"] == 1) {
//                    $totalInasistencias = Helpers::qryScalar("
//                        select sum(t.valor) as inasistencias
//                        from inasistencia i
//                                inner join inasistencia_detalle d on d.inasistencia_id = i.id
//                                inner join inasistencia_tipo t on t.id = d.inasistencia_tipo_id
//                        where i.Alumno_Division_id = $alumno_division_id
//                ");
					$inasistencias = Helpers::qryScalar("
                        select sum(t.valor) as inasistencias
                        from inasistencia i
                                inner join inasistencia_detalle d on d.inasistencia_id = i.id
                                inner join inasistencia_tipo t on t.id = d.inasistencia_tipo_id
                        where i.Alumno_Division_id = $alumno_division_id and d.fecha between \"$fecha_desde\" and \"$fecha_hasta\"
                ");
					$inasistenciasRet[$item["logica_periodo_id"]] = $inasistencias;
				} else {
					$inasistencias = Helpers::qryScalar("
                                select cantidad as inasistencias
                                  from inasistencia i
                                  where i.Alumno_Division_id = $alumno_division_id and
                                        i.logica_periodo_id = $logica_periodo_id
                            ");
					$inasistenciasRet[$item["logica_periodo_id"]] = $inasistencias;
				}
				$totalInasistencias += $inasistencias;
			}
		}

		return array("inasistencias" => $inasistenciasRet, "totalInasistencias" => $totalInasistencias);
	}

	public static function getConductaAlumnoDivision($alumno_division_id) {
		$ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();

		$alumnoDivision = Yii::app()->db->createCommand("
				select ad.id, ad.ciclo_id, n.conducta_detallada, coalesce(de.desempenio,\"\") as desempenio, n.id as nivel_id
						from alumno_division ad
								inner join alumno a on a.id = ad.`Alumno_id` and a.activo
								inner join division d on d.id = ad.division_id
								inner join anio an on an.id = d.anio_id
								inner join nivel n on n.id = an.nivel_id
								left join desempenio de on de.alumno_division_id = ad.id
						where ad.id = $alumno_division_id and ad.ciclo_id = $ciclo_id and ad.activo /*((ad.activo or ad.promocionado or ad.egresado)*/
						order by a.sexo desc, a.apellido, a.nombre
			")->queryRow();
		$logica_id = Logica::getLogicaIdPorAlumnoDivison($alumno_division_id);

		$items = Yii::app()->db->createCommand("
				select lp.id as logica_periodo_id, li.condicion, lp.orden as logica_periodo_orden, li.nombre_unico as logica_item_nombre_unico, li.logica_periodo_id,
							li.orden as logica_item_orden, li.manual, li.id, li.formula, li.nombre, li.tipo_nota, li.estado, li.tiene_conducta
					from logica_item li
						inner join logica_periodo lp on lp.id = li.logica_periodo_id
					where li.logica_id = $logica_id
					order by lp.orden, li.orden
			")->queryAll();
		$AmonestacionesFinal = 0;
		foreach ($items as $item) {
			if ($item["tiene_conducta"] == 1) {
				$logica_periodo_id = $item["logica_periodo_id"];
				if ($alumnoDivision["conducta_detallada"] == "0") {
					$amonestaciones = Helpers::qryScalar("
								select c.conducta as sanciones
									from conducta c
									where c.Alumno_Division_id = $alumno_division_id and
												c.logica_periodo_id = $logica_periodo_id
						");
					$totalAmonestaciones[$item["logica_item_nombre_unico"]] = $amonestaciones;
					$AmonestacionesFinal = $amonestaciones;
				} else {
//						$amonestaciones = Helpers::qryScalar("
//								select sum(d.cantidad) as sanciones
//									from conducta c
//										inner join conducta_detalle d on d.conducta_id = c.id
//									where c.Alumno_Division_id = $alumno_division_id and
//												c.logica_periodo_id = $logica_periodo_id
//						");
					$amonestaciones = Helpers::qryScalar("
									select sum(d.cantidad) as sanciones
										from conducta c
											inner join conducta_detalle d on d.conducta_id = c.id
										where c.Alumno_Division_id = $alumno_division_id/* and
													c.logica_periodo_id = $logica_periodo_id*/
								");
					$totalAmonestaciones[$item["logica_item_nombre_unico"]] = $amonestaciones;
					//$AmonestacionesFinal += $amonestaciones; antes
					$AmonestacionesFinal = $amonestaciones;
				}
			}
		}
//	vd($totalAmonestaciones);
		return array("amonestaciones" => $totalAmonestaciones, "amonestacionesFinal" => $AmonestacionesFinal, "desempenio" => $alumnoDivision["desempenio"]);
	}

}

?>
