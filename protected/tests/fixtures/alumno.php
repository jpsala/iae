<?php
return array(
    'Nico'=>array(
        'nombre'=> 'Nicolas',
        'apellido' => 'Sala',
        'Familia_id' => 1,
        'Socio_id' => 2,
        'activo' => true,
    ),
    'Lau'=>array(
        'nombre'=> 'Laura',
        'apellido' => 'Sala',
        'Familia_id' => 1,
        'activo' => true,
    ),
    'Paula'=>array(
        'nombre'=> 'Paula',
        'apellido' => 'Massone',
        'Familia_id' => 2,
        'activo' => true,
    ),
)
?>
