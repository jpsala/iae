<?php

return array(
    'Nico1A' => array(
        'Alumno_id' => 1,
        'Division_id' => 1,
        'Ciclo_id' => 2,
        'activo' => true,
        'fecha_alta' => new CDbExpression('NOW()'),
        'fecha_baja' => Null,
    ),
    'Lau1A' => array(
        'Alumno_id' => 2,
        'Division_id' => 1,
        'Ciclo_id' => 2,
        'activo' => true,
        'fecha_alta' => new CDbExpression('NOW()'),
        'fecha_baja' => Null,
    ),
    'Paula2A' => array(
        'Alumno_id' => 3,
        'Division_id' => 3,
        'Ciclo_id' => 1,
        'activo' => false,
        'fecha_alta' => new CDbExpression('NOW()'),
        'fecha_baja' => new CDbExpression('NOW()'),
    ),
    'Paula3A' => array(
        'Alumno_id' => 3,
        'Division_id' => 4,
        'Ciclo_id' => 2,
        'activo' => true,
        'fecha_alta' => new CDbExpression('NOW()'),
        'fecha_baja' => Null,
    ),
        )
?>
