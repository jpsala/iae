<?php
return array(
    'art1'=>array(
        "nombre" => "Artículo 1",
        'precio_neto'=> 100.55,
        "iva"=>10.5
    ),
    'art2'=>array(
        "nombre" => "Artículo 2",
        'precio_neto'=> 200.55,
        "iva"=>10.5
    ),
    'art3'=>array(
        "nombre" => "Artículo 3",
        'precio_neto'=> 300.55,
        "iva"=>10.5
    ),
)
?>
