<?php
return array(
    'matematica'=>array(
        'nombre'=> 'Matemática',
        'Asignatura_Tipo_id' => 1,
        'Curso_id' => 1,
        'desde_ciclo' => 1
    ),
    'fisica'=>array(
        'nombre'=> 'Física',
        'Asignatura_Tipo_id' => 1,
        'Curso_id' => 1,
        'desde_ciclo' => 1,
        'hasta_ciclo' => 1,
    ),
    'ingles'=>array(
        'nombre'=> 'Ingles',
        'Asignatura_Tipo_id' => 1,
        'Curso_id' => 1,
        'desde_ciclo' => 1,
        'hasta_ciclo' => 2,
    ),
    'historia'=>array(
        'nombre'=> 'Historia',
        'Asignatura_Tipo_id' => 1,
        'Curso_id' => 1,
        'desde_ciclo' => 2,
    ),
    'geografia'=>array(
        'nombre'=> 'Geografía',
        'Asignatura_Tipo_id' => 1,
        'Curso_id' => 1,
        'desde_ciclo' => 3,
        'Asignatura_Orientacion_id' => 1,
    ),
);

?>
