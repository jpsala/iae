<?php
return array(
    '1A'=>array(
        'nombre'=> '1A',
        'Curso_id' => 1,
        'activo' => true,
        'Nivel_id' => 1,
    ),
    '1B'=>array(
        'nombre'=> '1B',
        'Curso_id' => 1,
        'activo' => true,
        'Nivel_id' => 1,
    ),
    '2A'=>array(
        'nombre'=> '2A',
        'Curso_id' => 2,
        'Nivel_id' => 1,
        'Orientacion_id'=>1,
        'activo' => true
    ),
    '2B'=>array(
        'nombre'=> '2B',
        'Curso_id' => 2,
        'Nivel_id' => 1,
        'Orientacion_id'=>2,
        'activo' => true
    ),
    '3A'=>array(
        'nombre'=> '3A',
        'Curso_id' => 3,
        'Nivel_id' => 1,
        'activo' => true,
        'Orientacion_id'=>1,
    ),
    '3B'=>array(
        'nombre'=> '3B',
        'Curso_id' => 3,
        'Nivel_id' => 1,
        'activo' => true,
        'Orientacion_id'=>2,
    ),
);

?>
