<?php

return array(
    't1' => array(
        'nombre' => 't1',
        'Logica_id' => 1,
        'editable' => true,
        'formula' => '' ,
    ),
    't2' => array(
        'nombre' => 't2',
        'Logica_id' => 1,
        'editable' => true,
        'formula' => '',
    ),
    't3' => array(
        'nombre' => 't3',
        'Logica_id' => 1,
        'editable' => true,
        'formula' => '',
    ),
    'final' => array(
        'nombre' => 'Final',
        'Logica_id' => 1,
        'editable' => false,
        'formula' => '$nota->nota = ($notas["t1"]->nota+$notas["t2"]->nota+$notas["t3"]->nota)/3;',
    ),
);
?>
