<?php

class AlumnoDivisionTest extends CDbTestCase {

    public $fixtures = array(
        'alumnos' => 'Alumno',
        'alumnoDivisions' => 'alumnoDivision',
        'asignaturas' => 'asignatura',
        'divisiones' => 'division',
        'notas' => 'nota',
    );

    // TODO: public function testAsignaDivision() {
//    public function testAsignaDivision() {
//
//        /* @var $a Alumno */
//        $a = $this->alumnos('Nico');
//        //$a = Alumno::model()->findByPk(1);
//        $this->assertEquals($a->divisionActiva->Division_id, 1);
//
//        $d = $this->divisiones('1B');
//        $a->asignaDivision($d);
//
//        $this->assertTrue($a->divisionActiva->Division_id == 2);
//    }

    public function testGetNotasAsignatura() {
        /* @var $alumnoDivision AlumnoDivision */

        $alumnoDivision = $this->alumnoDivisions('Nico1A');

        $notas = $alumnoDivision->getNotasAsignatura($this->asignaturas('matematica'));

        $this->assertEquals(5, $notas['t1']->nota);
        $this->assertEquals(8, $notas['t2']->nota);
        $this->assertEquals(8, $notas['t3']->nota);
        $this->assertEquals(7, $notas['Final']->nota);

        $notas = $alumnoDivision->getNotasAsignatura($this->asignaturas('fisica'));
        $this->assertEquals(null, $notas['t1']->nota);
        $this->assertEquals(null, $notas['t2']->nota);
        $this->assertEquals(null, $notas['t3']->nota);
        $this->assertEquals(0, $notas['Final']->nota);
    }

    public function testGetNotas() {
        /* @var $alumnoDivision AlumnoDivision */


        $alumnoDivision = $this->alumnoDivisions('Nico1A');

        $notas = $alumnoDivision->getNotas();
        
        $notasMate = $notas[0];
        
        $notaPt = $notasMate['t1'];
        
        $this->assertEquals('Matemática', $notaPt->asignatura->nombre);
        $this->assertEquals('Nicolas', $notaPt->alumnoDivision->alumno->nombre);
        $this->assertEquals(5, $notaPt->nota);

        $notasFisica = $notas[1];
        
        $notaFisicaPt = $notasFisica['t1'];

        $this->assertEquals('Física', $notaFisicaPt->asignatura->nombre);
        $this->assertEquals('Nicolas', $notaFisicaPt->alumnoDivision->alumno->nombre);
        $this->assertEquals(Null, $notaFisicaPt->nota);
        
        
    }

    public function testSetNotasAsignatura() {
        /* @var $alumnoDivision AlumnoDivision */

        $alumnoDivision = $this->alumnoDivisions('Nico1A');

        $notas = $alumnoDivision->getNotasAsignatura($this->asignaturas('matematica'));

        $this->assertEquals(7, $notas['Final']->nota);
        $this->assertEquals(4, count($notas));


        $notas['t1']->nota = 2;
        $alumnoDivision->setNotasAsignatura($this->asignaturas('matematica'), $notas);
        $this->assertEquals(6, $notas['Final']->nota);

        $notas = $alumnoDivision->getNotasAsignatura($this->asignaturas('matematica'));
        $this->assertEquals(6, $notas['Final']->nota);

        $notas = $alumnoDivision->getNotasAsignatura($this->asignaturas('fisica'));
        $notas['t1']->nota = 6;
        $notas['t2']->nota = 5;
        $notas['t3']->nota = 10;
        
        $alumnoDivision->setNotasAsignatura($this->asignaturas('fisica'), $notas);
        
        $this->assertEquals(7, $notas['Final']->nota);
        $this->assertEquals(4, count($notas));
    }

    public function testCantNotasCargadas(){
        $alumnoDivision = $this->alumnoDivisions('Nico1A');
        $this->assertTrue($alumnoDivision->cantNotasCargadas > 0);
    }
}

?>
