<?php

class AlumnoTest extends CDbTestCase {

    public $fixtures=array(
        'familias' => 'familia',
        'alumnos' => 'alumno',
        'alumnoDivisions' => 'alumnoDivision',
        'divisiones'=>'division',
        'orientaciones'=>'orientacion',
        'cursos'=>'curso',
        'niveles'=>'nivel',
        'users' => 'user',
    );
    
    public function testAlta() {
        
        Yii::app()->user->setId($this->users['JP']['id']);
         
        $a = new Alumno();
        $a->nombre = 'testNom';
        $a->apellido = 'testAp';
        $a->Familia_id = $this->familias['Sala']['id'];
        $this->assertTrue($a->save());
        
        $a = Alumno::model()->findByPk($a->id);
        $this->assertEquals($a->nombre, 'testNom');
        
        $this->assertTrue($a->delete());
    }
    
    public function testDivisionActiva(){
        /* @var $a Alumno */
        $a = $this->alumnos('Nico');
        $da = $a->alumnoDivisionActiva;
        $this->assertTrue($da instanceof AlumnoDivision);
    }
    
    public function testAsignaDivision(){
        
        $division = $this->divisiones('1B');
        
        $lau = $this->alumnos('Lau');
        
        $lau->asignaDivision($division);
        
        $this->assertEquals( $lau->alumnoDivisionActiva->division->id, $division->id);
        
        
        $nico = $this->alumnos('Nico');
        
        $nico->asignaDivision($division);

        $this->assertEquals( $nico->alumnoDivisionActiva->division->id, $division->id);
        
        $this->assertEquals(3, $nico->alumnoDivisionActiva->cantNotasCargadas);

       
        
        $paula = $this->alumnos('Paula');
        
        //$paula->asignaDivision($division);

        $this->assertEquals( $paula->alumnoDivisionActiva->division->id, 4);
        
        $this->assertEquals(3, $paula->alumnoDivisionActiva->cantNotasCargadas);
        
    }

}

?>
