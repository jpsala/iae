<?php

class AsignaturaTest extends CDbTestCase {

    public $fixtures = array(
        'ciclos' => 'ciclo',
        'asignaturas' => 'asignatura',
    );

    public function testActiva() {

        /* @var $a Asignatura */

        $ciclo[2010] = Ciclo::model()->find('nombre = "2010"')->id;
        $ciclo[2011] = Ciclo::model()->find('nombre = "2011"')->id;  // Activo
        $ciclo[2012] = Ciclo::model()->find('nombre = "2012"')->id;

        $a = $this->asignaturas('matematica'); // desde=1 - hasta=null
        $this->assertTrue($a->activa);
        $this->assertTrue($a->getActiva($ciclo[2010]));
        $this->assertTrue($a->getActiva($ciclo[2011]));
        $this->assertTrue($a->getActiva($ciclo[2012])); 

        $a = $this->asignaturas('fisica');  // desde=1 - hasta=1
        $this->assertFalse($a->activa);
        $this->assertTrue($a->getActiva($ciclo[2010]));
        $this->assertFalse($a->getActiva($ciclo[2011]));
        $this->assertFalse($a->getActiva($ciclo[2012]));

        $a = $this->asignaturas('ingles');  // desde=1 - hasta=2
        $this->assertTrue($a->activa);
        $this->assertTrue($a->getActiva($ciclo[2010]));
        $this->assertTrue($a->getActiva($ciclo[2011]));
        $this->assertFalse($a->getActiva($ciclo[2012]));

        $a = $this->asignaturas('historia'); // desde=2 - hasta=null
        $this->assertTrue($a->activa);
        $this->assertFalse($a->getActiva($ciclo[2010]));
        $this->assertTrue($a->getActiva($ciclo[2011]));
        $this->assertTrue($a->getActiva($ciclo[2012]));
        
        $a = $this->asignaturas('geografia'); // desde=3 - hasta=null
        $this->assertFalse($a->activa);
        $this->assertFalse($a->getActiva($ciclo[2010]));
        $this->assertFalse($a->getActiva($ciclo[2011]));
        $this->assertTrue($a->getActiva($ciclo[2012]));
        
    }

}

?>
