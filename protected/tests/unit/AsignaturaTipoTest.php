<?php

class AsignaturaTipoTest extends CDbTestCase {

    public $fixtures = array(
        'asignaturas' => 'asignatura',
        'asignaturaTipos' => 'asignaturaTipo',
        'logicas'=>'logica',
        'logicaCiclos'=>'logicaCiclo',
    );

    public function testGetLogica() {
        /* @var $matematica Asignatura */
        $matematica = $this->asignaturas('matematica');
        $logica = $matematica->asignaturaTipo->logica;
        $this->assertEquals('Materia', $logica->nombre);
        $this->assertEquals(4, count($logica->logicaItems));
        
    }

}
?>
