<?php

class CicloTest extends CDbTestCase {

    public $fixtures = array(
        'ciclos' => 'ciclo',
    );

    public function testAlta() {
        $c = new Ciclo();
        $c->nombre = '2013';
        $c->activo = false;
        $c->fecha_inicio = '01/03/2013';
        $c->fecha_fin = '12/31/2013';
        if(! $c->validate()) {
            var_dump($c->getErrors());
        }
        var_dump($c->errors);
        $this->assertTrue($c->save());

        $c = Ciclo::model()->findByPk($c->id);
        $this->assertTrue($c instanceof Ciclo);
    }

    public function testCicloActivo() {
        $this->assertEquals(Ciclo::getActivo()->nombre, '2011');
    }

    public function testBloqueo() {
        $c = Ciclo::model()->findByPk(1);
        $cb = Ciclo::bloqueo(false, $c->id);
        var_dump($cb);
    }

}

?>
