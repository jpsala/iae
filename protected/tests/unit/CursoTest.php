<?php

class CursoTest extends CDbTestCase {

    public $fixtures = array(
        'cursos' => 'curso',
    );

    public function testAsignaturasActivas() {
        /* @var $curso Curso */
        $curso = $this->cursos('1');
        $this->assertEquals(3,count($curso->asignaturasActivas));
    }

    public function testAltaDivision() {

        $nombreDivision = '1C';

        /* @var $curso Curso */
        $curso = $this->cursos('1');

        $this->assertEquals(5, count($curso->asignaturas));

        $nuevaDivision = $curso->altaDivision($nombreDivision);

        /* @var $d Division */
        $d = Division::model()->find('Curso_id=:Curso_id and nombre = :nombre', array('Curso_id' => $curso->id, 'nombre' => $nombreDivision));

        $this->assertEquals($d->attributes, $nuevaDivision->attributes);

        $this->assertTrue(in_array($d, $curso->divisions));
        
        $this->assertEquals(3,count($d->divisionAsignaturas));

        //$this->assertEquals
    }
}
?>
