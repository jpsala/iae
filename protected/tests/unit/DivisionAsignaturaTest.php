<?php

class DivisionAsignaturaTest extends CDbTestCase {

    public $fixtures = array(
        'alumnos' => 'Alumno',
        'alumnoDivisions' => 'alumnoDivision',
        'asignaturas' => 'asignatura',
        'divisiones' => 'division',
        'notas' => 'nota',
        'divisionAsignaturas'=>'divisionAsignatura',
    );

    public function testGetNotas() {
        /* @var $divisionAsignatura DivisionAsignatura */

        
        $divisionAsignatura = $this->divisionAsignaturas('1Amatematica');
        
        $notas = $divisionAsignatura->getNotas();
        $notasNico = $notas[0];
        $this->assertEquals('1A',$notasNico['t1']->alumnoDivision->division->nombre);
        $this->assertEquals('Matemática',$notasNico['t1']->asignatura->nombre);
        $this->assertEquals('Nicolas',$notasNico['t1']->alumnoDivision->alumno->nombre);
        $this->assertEquals(5,$notasNico['t1']->nota);
        $this->assertEquals(8,$notasNico['t2']->nota);
        $this->assertEquals(8,$notasNico['t3']->nota);
        $this->assertEquals(7,$notasNico['Final']->nota);

        $notasLau = $notas[1];
        $this->assertEquals('1A',$notasLau['t1']->alumnoDivision->division->nombre);
        $this->assertEquals('Matemática',$notasLau['t1']->asignatura->nombre);
        $this->assertEquals('Laura',$notasLau['t1']->alumnoDivision->alumno->nombre);
        $this->assertEquals(Null,$notasLau['t1']->nota);
        $this->assertEquals(Null,$notasLau['t2']->nota);
        $this->assertEquals(Null,$notasLau['t3']->nota);
        $this->assertEquals(0,$notasLau['Final']->nota);        
        
    }

}

?>
