<?php

class DivisionTest extends CDbTestCase {

    public $fixtures = array(
        'alumnos' => 'Alumno',
        'alumnoDivisions' => 'alumnoDivision',
        'asignaturas' => 'asignatura',
        'divisiones' => 'division',
        'notas' => 'nota',
    );

    public function testGetNotas() {
        /* @var $division Division */

        $division = $this->divisiones('1A');

        $notas = $division->getNotas();
        
        $notasMate = $notas[0];

        $notasNicoMate = $notasMate[0];

        $this->assertEquals('Nicolas', $notasNicoMate['t1']->alumnoDivision->alumno->nombre);
        $this->assertEquals('Matemática', $notasNicoMate['t1']->asignatura->nombre);
        $this->assertEquals('7', $notasNicoMate['Final']->nota);

        $notasLauMate = $notasMate[1];
        
        $this->assertEquals('Laura', $notasLauMate['t1']->alumnoDivision->alumno->nombre);
        $this->assertEquals('Matemática', $notasLauMate['t1']->asignatura->nombre);
        $this->assertEquals(0, $notasLauMate['Final']->nota);

        $notasFisica = $notas[1];

        $notasNicoFisica = $notasFisica[0];
        
        $this->assertEquals('Nicolas', $notasNicoFisica['t1']->alumnoDivision->alumno->nombre);
        $this->assertEquals('Física', $notasNicoFisica['t1']->asignatura->nombre);
        $this->assertEquals(0, $notasNicoFisica['Final']->nota);

        $notasLauFisica = $notasFisica[1];
        
        $this->assertEquals('Laura', $notasLauFisica['t1']->alumnoDivision->alumno->nombre);
        $this->assertEquals('Física', $notasLauFisica['t1']->asignatura->nombre);
        $this->assertEquals(0, $notasLauFisica['Final']->nota);



//        $this->assertEquals('Matemática', $notas[0][0]->nombre);
//        $this->assertEquals(7, $notas[0][1]['Final']->nota);
//
//        $this->assertEquals('Física', $notas[1][0]->nombre);
//        $this->assertEquals(0, $notas[1][1]['Final']->nota);
    }

//    public function testSetNotasAsignatura() {
//        /* @var $alumnoDivision AlumnoDivision */
//
//        $alumnoDivision = $this->alumnoDivisions('Nico1A');
//
//        $notas = $alumnoDivision->getNotasAsignatura($this->asignaturas('matematica'));
//
//        $this->assertEquals(7, $notas['Final']->nota);
//        $this->assertEquals(4, count($notas));
//
//
//        $notas['t1']->nota = 2;
//        $alumnoDivision->setNotasAsignatura($this->asignaturas('matematica'), $notas);
//        $this->assertEquals(6, $notas['Final']->nota);
//
//        $notas = $alumnoDivision->getNotasAsignatura($this->asignaturas('matematica'));
//        $this->assertEquals(6, $notas['Final']->nota);
//
//        $notas = $alumnoDivision->getNotasAsignatura($this->asignaturas('fisica'));
//        $notas['t1']->nota = 6;
//        $notas['t2']->nota = 5;
//        $notas['t3']->nota = 10;
//        
//        $alumnoDivision->setNotasAsignatura($this->asignaturas('fisica'), $notas);
//        
//        $this->assertEquals(7, $notas['Final']->nota);
//        $this->assertEquals(4, count($notas));
//    }
}

?>
