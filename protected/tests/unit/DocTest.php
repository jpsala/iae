<?php

class DocTest extends CDbTestCase {

    public $fixtures = array(
        'articulos' => 'articulo',
        'alumnos' => 'alumno',
        'socios' => 'socio',
        'docs' => 'doc',
        'docDets' => 'docDet',
        'proveedores' => 'proveedor',
        'clientes' => 'cliente',
        'comprobPuntoVentas' => 'comprobPuntoVenta',
    );
    
    public function testAltaDocFacturaCliente(){
        $cpv = $this->comprobPuntoVentas("jp");
        $doc = new Doc(null,$cpv->id);
        $this->assertEquals("Factura",$doc->comprobPuntoVenta->comprob->nombre);
        $this->assertEquals("Factura Cliente",$doc->comprobPuntoVenta->comprob->comprobTipo->nombre);
        $this->assertEquals("jp",$doc->comprobPuntoVenta->puntoVenta->terminal);
    }
    
//    public function AltaDocValor($id_doc){
//     $doc_valor = new DocValor();
//     $doc_valor->Doc_id = $id_doc;
//     $doc_valor->Moneda_id = 1;
//     $doc_valor->Destino_Instancia_id = 1;
//     $doc_valor->tipo = 'Efe'; 
//     
//     $doc_valor->save();
//    }
//    
//    public function AltaDetalleDoc($id_doc, $id_articulo){
//     $doc_det = new DocDet();
//     $doc_det->Doc_id = $id_doc;
//     $doc_det->Articulo_id = $id_articulo;
//     return $doc_det;
//    }
//    
//    public function testAltaFacturaProveedor(){
//        
//        
//        $doc = new Doc();
//
//
//        $this->assertFalse($doc->validate());
//        $this->assertTrue(is_array($doc->errors));
//        print_r($doc->errors);
//        $this->assertEquals(2,count($doc->errors));
//
//        $doc->Comprob_Punto_Venta_id = $this->comprobPuntoVentas("jp")->id;
//        
//        $proveedor = $this->proveedores("2");
//        $doc->socio = $proveedor;
//        $doc->numero = "12345";
//        $doc->agregaDetalle($this->AltaDetalleDoc($doc->id,1));
//        $doc->agregaDetalle($this->AltaDetalleDoc($doc->id,2));
//        $doc->agregaDetalle($this->AltaDetalleDoc($doc->id,3));
//        if (!$doc->save()) {
//            print_r($doc->errors);
//        }
//       // $this->AltaDocValor($doc->id);
//        
//        
//    }
//         

}

?>
