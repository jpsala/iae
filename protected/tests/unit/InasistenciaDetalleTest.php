<?php

class InasistenciaDetalleTest extends CDbTestCase {

    public $fixtures = array(
        //nombre del fixture => nombre de la clase 
        'inasistencia_detalles' => 'InasistenciaDetalle',
        'alumnoDivisions' => 'alumnoDivision',
    );

    public function testAltaInasisteanciaDetalle() {

        $id = new InasistenciaDetalle();
        $id->inasistencia_tipo_id = 1;
        $id->fecha = date("Y-m-d");
        $id->Alumno_Division_id = 1;
        $this->assertTrue($id->save());

        $tipo_inasistencia_detalle_recuperada = InasistenciaDetalle::model()->findByPk($id->id);
        $this->assertEquals($tipo_inasistencia_detalle_recuperada->id, 3);
    }

    /**
     * Recorre las inasistencias de un alumnodivision y las suma. Hecho para prueba, 
     * solo debería devolver la cantidad y no el objeto y recorrerlo 
     * @see  public static function obtieneCantidadInasistenciasPorAlumno(AlumnoDivision $alumnodivision)
     * @author Octavio
     * @TODO Revisar todo el modelo de inasistencias. 
     * @todo ahora no se si este test va acá o en alumnoDivision, ni idea
     */
    public function testGetCantidadInasistencias() {

        //@todo paraoctavio solo una pruebita, cambialo como quieras my friend
        //@todo parece que no anda ->inisistencias, solo con el getter, quizá tenga una relacion con ese nombre?
        $this->assertEquals(1, $this->alumnoDivisions('Nico1A')->getInasistencias());

//        $inasistencias = InasistenciaDetalle::obtieneCantidadInasistenciasPorAlumno($alumnoDivision);
//        var_dump($inasistenciaDetalles[0]->attributes);
//        foreach ($inasistenciaDetalles as $id) {
//            var_dump( $id->attributes);
//            $tipo_in = InasistenciaTipo::getValorInasistencia($id->inasistencia_tipo_id);
//            var_dump( $tipo_in->attributes);
//        }
        //throw new CException($total_faltas);
    }

}

?>
