<?php

class InasistenciaTipoTest extends CDbTestCase {

    public $fixtures=array(
        //nombre del fixture => nombre de la clase 
        'inasistencia_tipos' => 'InasistenciaTipo',
 
    );
    
    public function testAltaInasisteancia() {
        
        $it = new InasistenciaTipo();
        //Si le paso el valor del id 1 falla por la inicialización de los fixtures.
        //$it->id = 1;
        $it->nombre = 'Falta';
        $it->valor = 1;
        $this->assertTrue($it->save());
        
        $tipo_inasistencia_recuperada = InasistenciaTipo::model()->findByPk($it->id);
        $this->assertEquals($tipo_inasistencia_recuperada->nombre, 'Falta');
        
        $this->assertTrue($tipo_inasistencia_recuperada->delete());
    }
    
    public function testProbandoEstadoInicialFixture() {
        //Probando la inicialización del fixture (muy pavo).
        $tipo_inasistencia_recuperada = InasistenciaTipo::model()->findByPk(1);
        $inasistencia_tipo = $this->inasistencia_tipos('FC');
        
        if ($tipo_inasistencia_recuperada){
            $this->assertEquals($tipo_inasistencia_recuperada->nombre,$inasistencia_tipo["nombre"]);
        } else {
                        throw new CException('El tipo de inasistencia no existe');
        }
    }
}

?>
