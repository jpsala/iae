<?php

class LogicaItemTest extends CDbTestCase {

    public $fixtures = array(
        'divisiones' => 'division',
        'alumnos' => 'Alumno',
        'alumnoDivisions' => 'alumnoDivision',
        'asignaturas' => 'asignatura',
        'logicaitems' => 'logicaItem',
        'notas' => 'nota',
    );

    public function testProcesa() {
        /* @var $alumnoDivision AlumnoDivision */
        /* @var $logica Logica */
        $alumnoDivision = $this->alumnoDivisions('Nico1A');

        $asignatura = $this->asignaturas('matematica');

        $notas = array();

        $lis =  Logica::getLogica($asignatura->asignaturaTipo, $alumnoDivision->ciclo)->logicaItems;
        
        
        foreach ($lis as $li) {
            $notas[$li->nombre] = $li->getSetNotas($alumnoDivision, $asignatura, $notas);
        }

        $this->assertEquals(7, $notas['Final']->nota);
        $this->assertEquals(4, count($notas));

        $notas = $alumnoDivision->getNotasAsignatura($this->asignaturas('fisica'));
        $this->assertEquals(0, $notas['Final']->nota);
        $this->assertEquals(4, count($notas));
    }

}

?>
