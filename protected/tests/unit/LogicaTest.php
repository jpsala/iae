<?php

class LogicaTest extends CDbTestCase {

    public $fixtures = array(
        'alumnoDivisions' => 'alumnoDivision',
        'asignaturas' => 'asignatura',
    );

    public function testGetLogica() {

        $logica = Logica::getLogica(
                $this->asignaturas('matematica')->asignaturaTipo, 
                $this->alumnoDivisions('Nico1A')->ciclo
         );

        $this->assertEquals('Materia',$logica->nombre);
    }
    
    public function testGetNotas(){
        $alumnoDivision = $this->alumnoDivisions('Nico1A');
        $asignatura = $this->asignaturas('matematica');
        
        $logica = Logica::getLogica($asignatura->asignaturaTipo, $alumnoDivision->ciclo);
        $notas = $logica->getNotas($alumnoDivision, $asignatura);
        $this->assertEquals(5, $notas['t1']->nota);
        $this->assertEquals(8, $notas['t2']->nota);
        $this->assertEquals(8, $notas['t3']->nota);
        $this->assertEquals(7, $notas['Final']->nota);
    }
    
    public function testSetNotas(){
        $alumnoDivision = $this->alumnoDivisions('Nico1A');
        $asignatura = $this->asignaturas('matematica');
        
        $logica = Logica::getLogica($asignatura->asignaturaTipo, $alumnoDivision->ciclo);
        
        $notas = $logica->getNotas($alumnoDivision, $asignatura);
        
        $this->assertEquals(5, $notas['t1']->nota);
        $this->assertEquals(8, $notas['t2']->nota);
        $this->assertEquals(8, $notas['t3']->nota);
        $this->assertEquals(7, $notas['Final']->nota);
        
        $notas['t1']->nota = 3;
        $notas['t2']->nota = 2;
        $notas['t3']->nota = 4;
        $notas = $logica->setNotas($alumnoDivision, $asignatura, $notas);
        
        $this->assertEquals(3, $notas['t1']->nota);
        $this->assertEquals(2, $notas['t2']->nota);
        $this->assertEquals(4, $notas['t3']->nota);
        $this->assertEquals(3, $notas['Final']->nota);
        
        $notas = $logica->getNotas($alumnoDivision, $asignatura);
        
        $this->assertEquals(3, $notas['t1']->nota);
        $this->assertEquals(2, $notas['t2']->nota);
        $this->assertEquals(4, $notas['t3']->nota);
        $this->assertEquals(3, $notas['Final']->nota);        
    }

}

?>
