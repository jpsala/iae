<?php

class SocioTest extends CDbTestCase {

    public $fixtures=array(
        //nombre del fixture => nombre de la clase 
        'socio' => 'Socio',
 
    );
    
    public function testRecuperaDatosSocio() {
        $socio = Socio::model()->findByPk(1);
        $datos = Socio::model()->recupera_datos_socio_segun_tipo($socio);
        if ($datos instanceof Proveedor){
            $nombre_campo = 'razon_social';
            } 
            else{
            $nombre_campo = 'nombre';
            }
        $this->assertEquals($datos->$nombre_campo, "Proveedor 1");
        
    }
}

?>
