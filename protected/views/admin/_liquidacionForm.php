<?php
/* @var $this PostController */
/* @var $model Destino */
?>
<div class="form">
    <div class="row">
        <select id="Localidad" data-placeholder="Puede seleccionar una Localidad para modificar" class="chzn-select">
            <?php
            $x = array('prompt' => '');
            echo CHtml::listOptions($model->id, CHtml::listData(Localidad::model()->findAll(), 'id', 'nombre'), $x)
            ?>
        </select>
    </div>
    <form id="carga" method="post">
        <input id="id" type="hidden" name="id" value="<?php echo $model->id; ?>"/>

        <div class="row">
            <label for="descripcion">Descripción</label>
            <input class="clearinput" id="descripcion" type="text" name="descripcion"
                   value="<?php echo CHtml::encode($model->descripcion); ?>"/>
        </div>
        <?php
        /* @var $rdr CDbDataReader */; ?>
        <!--        <div class="row">
            <label for="pais_id">País</label>
            <select data-allows-new-values="true" id="pais_id" data-placeholder="Seleccione un país" class="chzn-select" name="Pais_id" value="<?php //echo $model->Pais_id; ?>">
                <?php
        //$x = array('prompt' => '');
        //echo CHtml::listOptions($model->Pais_id, CHtml::listData(Pais::model()->findAll(), 'id', 'nombre'), $x)
        ?>
            </select>
        </div>        -->
        <div class="botones">
            <input type="submit" name="submit" value="Grabar" id="submit"/>
            <?php if (!$model->getIsNewRecord()): ?>
                <input type="button" name="borrar" value="Borra" id="borra"/>
            <?php endif; ?>
            <input type="button" onclick="return refresh()" name="button" value="Nuevo" id="button"/>
        </div>
    </form>
    <div id="errores">
    </div>
</div>
