<input type="hidden" value="<?php echo $alumno->Familia_id; ?>" id="familia_id"/>

<div id="div-matricula" class="inline">
	<div class="inline">
		<label for="matricula">Matrícula</label>
		<input id="matricula" name="matricula" value="<?php echo $alumno->matricula; ?>" />
	</div>
	<div class="inline" id="div-estado">
		<?php $data = CHtml::listData(AlumnoEstado::model()->findAll(array("order" => "orden")), "id", "nombre"); ?>
		<?php echo CHtml::dropDownList("estado_id", $alumno->estado_id, $data); ?>
	</div>
	<div class="inline" id="div-activo" style="display:none">
		<label for="activo">Activo</label>
		<input type="checkbox" id="activo" name="activo" <?php echo $alumno->activo ? "CHECKED" : ""; ?> />
	</div>
	<div class="inline" id="div-ingresante"  style="display:none">
		<label for="activo">Ingresante</label>
		<input type="checkbox" id="ingresante" name="ingresante" <?php echo $alumno->ingresante ? "CHECKED" : ""; ?> />
	</div>
</div>

<div id="division-familia">
	<div id="asignacion-divison-foto">
		<div id="div-foto">
			<img id="foto" alt="" src="images/alumno-falta.jpg"/>
		</div>

		<div id="asignacion-division" class="row ui-widget-content ui-corner-all">
			<div class="ui-widget-header ui-corner-top">DivisiÃ³n</div>
			<?php /* @var $alumno Alumno */; ?>
			<?php $alumnoDivisionActiva = $alumno->AlumnoDivisionActiva; ?>
			<?php // if ($alumnoDivisionActiva): ?>
			<div class="datos" style="display:<?php echo $alumnoDivisionActiva ? "" : "none"; ?>">
				<span class="nivel"><?php echo $alumnoDivisionActiva ? $alumno->AlumnoDivisionActiva->division->anio->nivel->nombre : ""; ?></span>
				<span class="anio"><?php echo $alumnoDivisionActiva ? $alumno->AlumnoDivisionActiva->division->anio->nombre : ""; ?></span>
				<span class="division"><?php echo $alumnoDivisionActiva ? $alumno->AlumnoDivisionActiva->division->nombre : ""; ?></span>
			</div>
			<input type="button" value="re-asigna" id="re-asigna" onclick="return reasigna()"  style="display:<?php echo $alumnoDivisionActiva ? "" : "none"; ?>"/>
			<?php //endif; ?>
			<?php include(Yii::app()->basePath . "/views/common/_division.php"); ?>
		</div>
	</div>
	<div id="familia" class="row  ui-widget-content ui-corner-all">
		<div class="ui-widget-header ui-corner-top">Familia</div>
		<?php $styleElijeFamilia = ($alumno->Familia_id) ? "style='display:none'" : "" ?>
		<?php $obsCobranza = ($alumno->Familia_id and $alumno->familia->obs) ? $alumno->familia->obs : ""; ?>
		<input type="hidden" id="obs-cobranza-input" value="<?php echo $obsCobranza; ?>"/>
		<div id="div-elije-familia" <?php echo $styleElijeFamilia; ?>>
			<label for="familia-ac">Familia</label>
			<input name="familia-ac" id="familia-ac" value="" placeholder=""/>
		</div>
		<?php $styleReasignaFamilia = (!$alumno->id) ? "style='display:none'" : "" ?>
		<input <?php echo $styleReasignaFamilia; ?> type="button" value="Re-asigna a otra familia" id="reasigna-familia-button" onclick="return nuevaFamilia()"/>
		<input  type="button" value="Elimina&#13;&#10;miembro" onclick="return eliminaMiembro()" id="elimina-miembro-button"/>
		<input  type="button" value="Modifica&#13;&#10;miembro" onclick="return editaMiembro()" id="edita-miembro-button"/>
		<input  type="button" value="Agrega&#13;&#10;miembro" onclick="return nuevoMiembro()" id="nuevo-miembro-button"/>
		<!--<input  type="button" value="Obs&#13;&#10;Cobranza" onclick="return editaObsCobranza()" id="edita-obs-cobranza-button"/>-->
		<table  id="familia-grid"></table>
	</div>

</div>

<fieldset id="nombre-fieldset" class="ui-widget-content">
	<legend>Nombres</legend>
	<input id="apellido" name="apellido" value="<?php echo $alumno->apellido; ?>"placeholder="Apellido"/>
	<input id="nombre" name="nombre" value="<?php echo $alumno->nombre; ?>" placeholder="Nombre"/>
</fieldset>

<fieldset id="vivecon-fieldset" class="ui-widget-content">
	<legend>Vive Con</legend>
	<select id="vive_con_id" data-placeholder="Vive Con" class="chzn-select" name="vive_con_id" value="<?php echo $alumno->vive_con_id; ?>">
		<?php
			$x = array('prompt' => '');
			if (isset($alumno->Familia_id)) {
				echo CHtml::listOptions($alumno->vive_con_id, Pariente::listData(($alumno->Familia_id), 'id', 'nombre'), $x);
			} else {
				echo CHtml::listOptions($alumno->vive_con_id, Pariente::listData(("-1"), 'id', 'nombre'), $x);
			}
		?>
	</select>
</fieldset>

<!--<fieldset id="domicilio-fieldset" class="ui-widget-content">
	<legend>Domicilio</legend>
	<input maxlength="25" id="calle" name="calle" value="<?php //echo $alumno->calle;              ?>" placeholder="Calle"/>
	<input maxlength="7" id="numero" name="numero" value="<?php //echo $alumno->numero;              ?>" placeholder="NÃºm."/>
	<input maxlength="4" id="piso" name="piso" value="<?php //echo $alumno->piso;              ?>" placeholder="Piso"/>
	<input maxlength="4" id="departamento" name="departamento" value="<?php //echo $alumno->departamento;              ?>" placeholder="Depto."/>
</fieldset>-->

<fieldset id="ubicacion-fieldset" class="ui-widget-content">
	<legend>Lugar Nacimiento</legend>
	<select id="pais-select" name="pais-select" data-placeholder="Paí­s Nacimiento" class="chzn-select">
		<?php
			$x = array('prompt' => '');
			//@FIXME cambié un poco porque me daba error la lÃ­nea 68, agregué "and $alumno->localidad_id"
			if (isset($alumno->localidad_id) and $alumno->localidad_id) {
				$id_pais = Localidad::model()->findByPk($alumno->localidad_id)->Pais_id;
			} else {
				$id_pais = null;
			}
			echo CHtml::listOptions(Pais::model()->findByPk($id_pais), CHtml::listData(Pais::model()->findAll(), 'id', 'nombre'), $x)
		?>
	</select>

	<select id="provincia" name ="provincia" data-placeholder="Provincia Nacimiento" class="chzn-select">
		<?php
			$x = array('prompt' => '');
			if (isset($alumno->localidad_id)) {
				$id_provincia = Localidad::model()->findByPk($alumno->localidad_id)->provincia_id;
				echo CHtml::listOptions(Provincia::model()->findByPk($id_provincia), CHtml::listData(Provincia::model()->findAll("pais_id = $id_pais"), 'id', 'nombre'), $x);
			} else {
				$id_provincia = null;
				echo CHtml::listOptions(Provincia::model()->findByPk($id_provincia), CHtml::listData(Provincia::model()->findAll("id=-1"), 'id', 'nombre'), $x);
			}

			//echo CHtml::listOptions(Provincia::model()->findByPk($id_provincia), CHtml::listData(Provincia::model()->findAll(), 'id', 'nombre'), $x)
		?>
	</select>

	<select id="Localidad" name ="Localidad" data-placeholder="Localidad Nacimiento" class="chzn-select">
		<?php
			$x = array('prompt' => '');
			if (isset($alumno->localidad_id)) {
				echo CHtml::listOptions(
						$alumno->localidad_id, CHtml::listData(Localidad::model()->findAll("
							provincia_id = $id_provincia
						"), 'id', 'nombre'), $x);
			} else {
				echo CHtml::listOptions($alumno->localidad_id, CHtml::listData(Localidad::model()->findAll("id=-1"), 'id', 'nombre'), $x);
			}
		?>
	</select>
</fieldset>
<fieldset id="nacopcion-fieldset" class="ui-widget-content">
	<legend>Nacionalidad por Opcion?</legend>
	<select id="nacionalidad-select" name="nacionalidad-select" data-placeholder="Nacionalidad Opción" class="chzn-select">
		<?php
			$x = array('prompt' => '');
			if (isset($alumno->nacionalidad_opcion) and $alumno->nacionalidad_opcion) {
				$id_pais = $alumno->nacionalidad_opcion;
			} else {
				$id_pais = null;
			}
			echo CHtml::listOptions(Pais::model()->findByPk($alumno->nacionalidad_opcion), CHtml::listData(Pais::model()->findAll(), 'id', 'nombre'), $x)
		?>
	</select>
</fieldset>
<fieldset id="telefonos-fieldset" class="ui-widget-content">
	<legend>Teléfonos (1)</legend>
	<input maxlength="25" id="titular_tel1" name="titular_tel1" value="<?php echo $alumno->titular_tel1; ?>" placeholder="Titular teléfono"/>
	<input maxlength="25" id="telefono_1" name="telefono_1" value="<?php echo $alumno->telefono_1; ?>" placeholder="Teléfono 1"/>
</fieldset>
<fieldset id="telefonos-fieldset" class="ui-widget-content">
	<legend>Teléfonos (2)</legend>
	<input maxlength="25" id="titular_tel2" name="titular_tel2" value="<?php echo $alumno->titular_tel2; ?>" placeholder="Titular teléfono"/>
	<input maxlength="25" id="telefono_2" name="telefono_2" value="<?php echo $alumno->telefono_2; ?>" placeholder="Teléfono 2"/>
</fieldset>

<fieldset id="documento-fieldset" class="ui-widget-content">
	<legend>Documento</legend>
	<select id="documento_tipo_id" data-placeholder="Tipo de documento" class="chzn-select" name="Documento_Tipo_id" value="<?php echo $alumno->Tipo_Documento_id; ?>">
		<?php
			$x = array('prompt' => '');
			echo CHtml::listOptions($alumno->Tipo_Documento_id, CHtml::listData(TipoDocumento::model()->findAll(), 'id', 'nombre'), $x)
		?>
	</select>

	<input maxlength="20" id="numero_documento" name="numero_documento" value="<?php echo $alumno->numero_documento; ?>" placeholder="NÃºmero de Doc."/>
</fieldset>

<fieldset id="obra-social-fieldset" class="ui-widget-content">
	<legend>Obra Social</legend>
	<input maxlength="30" id="obra_social" name="obra_social" value="<?php echo $alumno->obra_social; ?>" placeholder="Obra Social"/>
	<input maxlength="30" id="numero_afiliado" name="numero_afiliado" value="<?php echo $alumno->numero_afiliado; ?>" placeholder="NÃºmero"/>
</fieldset>

<fieldset id="sexo-fieldset" class="ui-widget-content">
	<legend>Sexo</legend>
	<select  id="sexo" data-placeholder="Sexo... " class="chzn-select" name="sexo" value="<?php echo $alumno->sexo; ?>">
		<?php
			$x = array('prompt' => 'Sexo');
			echo CHtml::listOptions($alumno->sexo, array("M" => "Masculino", "F" => "Femenino"), $x)
		?>
	</select>
</fieldset>
<fieldset id="fnac-fieldset" class="ui-widget-content">
	<legend>Fecha Nacimiento</legend>
	<input id="fecha_nacimiento" name="fecha_nacimiento" value="<?php echo isset($alumno->fecha_nacimiento) ? date("d/m/Y", mystrtotime($alumno->fecha_nacimiento)) : ''; ?>" placeholder="Fecha Nac."/>
	<span>Fecha Ingreso</span>
	<?php //$fechaIng = date("d/m/Y", $alumno->fecha_ingreso != "0000-00-00" ? mystrtotime($alumno->fecha_ingreso) : time()); ?>
	<?php
		$fechaIng = ($alumno->fecha_ingreso != "0000-00-00" and $alumno->fecha_ingreso != "1969-12-31") ?
				date("d/m/Y", mystrtotime($alumno->fecha_ingreso)) : "";
	?>
	<?php
		$fechaEgr = ($alumno->fecha_egreso == "0000-00-00" or ! $alumno->fecha_egreso) ?
				"" :
				date("d/m/Y", mystrtotime($alumno->fecha_egreso));
	?>
	<input id="fecha_ingreso" name="fecha_ingreso" value="<?php echo $fechaIng; ?>" placeholder="Fecha Ingreso"/>
	<?php $fechaIngEstab = date("d/m/Y", $alumno->fecha_ingreso_estab != "0000-00-00" ? mystrtotime($alumno->fecha_ingreso_estab) : time()); ?>
	<?php $fechaIngEstab = ""; ?>
	<span>Fecha Ingreso al estab.</span>
	<input id="fecha_ingreso_estab" name="fecha_ingreso_estab" value="<?php echo $fechaIngEstab; ?>" placeholder="Fecha Ingreso al estab."/>
	<span>Fecha Egreso</span>
	<input id="fecha_egreso" name="fecha_egreso" value="<?php echo $fechaEgr; ?>" placeholder="Fecha Egreso"/>
</fieldset>
<fieldset id="gs-fieldset" class="ui-widget-content">
	<legend>Grupo Sanguineo</legend>
	<select  id="grupo_sanguineo" data-placeholder="Grupo Sanguineo... " class="chzn-select" name="grupo_sanguineo" value="<?php echo $alumno->grupo_sanguineo; ?>">
		<?php
			$x = array('prompt' => 'Grupo Sanguineo');
			echo CHtml::listOptions($alumno->grupo_sanguineo, array("O-" => "O-", "O+" => "O+", "A-" => "A-", "A+" => "A+", "B-" => "B-", "B+" => "B+", "AB-" => "AB-", "AB+" => "AB+"), $x)
		?>
	</select>
</fieldset>
<fieldset id="ce-fieldset" class="ui-widget-content">
	<legend>Correo Electrónico</legend>
	<input maxlength="45" id="email" name="email" value="<?php echo $alumno->email; ?>" placeholder="EMail"/>
</fieldset>

<fieldset id="porc-beca-fieldset" class="ui-widget-content">
	<legend>% Beca</legend>
	<input id="beca" name="beca" value="<?php echo $alumno->beca; ?>" placeholder="% Beca"/>
	<input id="obs_beca" name="obs_beca" value="<?php echo $alumno->obs_beca; ?>" placeholder="Observaciones Beca"/>
	<!--<input type="hidden" id="obs_cobranza" name="obs_cobranza" value="<?php //echo $obs_cobranza ? $alumno->obs_cobranza : "";        ?>" placeholder="Observaciones Cobranza"/>-->
</fieldset>

<fieldset id="alumno-obs">
	<legend>Observaciones de Cobranza</legend>
	<textarea name="obs_cobranza"><?php echo $obsCobranza; ?></textarea>
</fieldset>

<div class="buttons-div">
	<input type="button" value="Cancela" name="cancela" id="cancela" onclick="cancelaForm()"/>
	<input type="button" value="Graba" name="graba" id="graba" onclick="grabaForm()"/>
	<input type="button" value="Graba y Nuevo" name="grabaMasNuevo" id="graba-mas-nuevo" onclick="grabaFormMasNuevo()"/>
</div>
