<head>
	<?php include("admin.css.php"); ?>
	<?php include("adminpariente.js.php"); ?>    
</head>
<form name="pariente-form" id="pariente-form" action="#" method="post">
	<input type="hidden" name="Pariente_id" value="<?php echo $pariente->id; ?>" id="pariente_id"/>

	<fieldset id="apellido-fieldset" class="ui-widget-content">
		<legend>Apellido</legend>
		<input id="apellido" name="Apellido_pariente" value="<?php echo $pariente->apellido; ?>" />
	</fieldset>

	<fieldset id="nombres-fieldset" class="ui-widget-content">
		<legend>Nombres</legend>
		<input id="nombre" name="Nombre_pariente" value="<?php echo $pariente->nombre; ?>" />
	</fieldset>

	<fieldset id="profesion-fieldset" class="ui-widget-content">
		<legend>Profesión</legend>
		<input id="profesion" name="profesion" value="<?php echo $pariente->profesion; ?>" />
	</fieldset>

	<fieldset id="fnac1-fieldset" class="ui-widget-content">
		<legend>Fecha Nacimiento</legend>
		<input id="fecha_nacimiento_par" name="fecha_nacimiento_par" value="<?php echo isset($pariente->fecha_nacimiento) ? date("d/m/Y", mystrtotime($pariente->fecha_nacimiento)) : ''; ?>" placeholder="Fecha Nac."/>
	</fieldset>

	<fieldset id="tipo-fieldset" class="ui-widget-content">
		<legend>Tipo Pariente</legend>
		<select id="pariente-select" name="Pariente_Tipo_id">
			<?php $arr = array(); ?>
			<?php /* @var $pariente Pariente */ ?>
			<?php echo CHtml::listOptions($pariente->Pariente_Tipo_id, CHtml::listData(ParienteTipo::model()->findAll(), "id", "nombre"), $arr) ?>
		</select>
	</fieldset>

	<fieldset id="domicilio-fieldset" class="ui-widget-content">
		<legend>Domicilio</legend>
		<input maxlength="25" id="calle" name="calle_pariente" value="<?php echo $pariente->calle; ?>" placeholder="Calle"/>
		<input maxlength="7" id="numero" name="numero_pariente" value="<?php echo $pariente->numero; ?>" placeholder="Núm."/>
		<input maxlength="4" id="piso" name="piso_pariente" value="<?php echo $pariente->piso; ?>" placeholder="Piso"/>
		<input maxlength="4" id="departamento" name="departamento_pariente" value="<?php echo $pariente->departamento; ?>" placeholder="Depto."/>
	</fieldset>

	<fieldset id="telefonos1-fieldset" class="ui-widget-content">
		<legend>Teléfono Casa</legend>
		<input maxlength="25" id="telefono_casa" name="telefono_casa" value="<?php echo $pariente->telefono_casa; ?>" placeholder="Teléfono Fijo"/>
	</fieldset>

	<fieldset id="telefonos2-fieldset" class="ui-widget-content">
		<legend>Teléfono Trabajo</legend>    
		<input maxlength="25" id="telefono_trabajo" name="telefono_trabajo" value="<?php echo $pariente->telefono_trabajo; ?>" placeholder="Teléfono Trabajo"/>
	</fieldset>

	<fieldset id="telefonos3-fieldset" class="ui-widget-content">
		<legend>Teléfono Celular</legend>    
		<input maxlength="25" id="telefono_celular" name="telefono_celular" value="<?php echo $pariente->telefono_celular; ?>" placeholder="Teléfono Celular"/>
	</fieldset>

	<fieldset id="documento-fieldset" class="ui-widget-content">
		<legend>Documento</legend>
		<select id="documento_tipo_id" data-placeholder="Tipo de documento" class="chzn-select" name="Documento_Tipo_id_pariente" value="<?php echo $pariente->tipo_documento_id; ?>">
			<?php
				$x = array('prompt' => '');
				echo CHtml::listOptions($pariente->tipo_documento_id, CHtml::listData(TipoDocumento::model()->findAll(), 'id', 'nombre'), $x)
			?>
		</select>

		<input maxlength="20" id="numero_documento_par" name="numero_documento_pariente" value="<?php echo $pariente->numero_documento; ?>" placeholder="Número de Doc."/>
	</fieldset>
	<fieldset id="nacionalidad-fieldset" class="ui-widget-content">
		<legend>Nacionalidad</legend>
		<select id="nacionalidad" name="nacionalidad" data-placeholder="Nacionalidad" class="chzn-select">
			<?php
				$x = array('prompt' => '');
				if (isset($pariente->nacionalidad)) {
					$id_pais = $pariente->nacionalidad;
				} else {
					$id_pais = null;
				}
				echo CHtml::listOptions(Pais::model()->findByPk($id_pais), CHtml::listData(Pais::model()->findAll(), 'id', 'nombre'), $x)
			?>
		</select>
	</fieldset>

	<fieldset id="email1-fieldset" class="ui-widget-content">
		<legend>Correo Electrónico 1</legend>
		<input maxlength="45" id="email" name="email1" value="<?php echo $pariente->email; ?>" placeholder="Correo Electrónico"/>
	</fieldset>

	<fieldset id="email2-fieldset" class="ui-widget-content">
		<legend>Correo Electrónico 2</legend>
		<input maxlength="45" id="email2" name="email2" value="<?php echo $pariente->email1; ?>" placeholder="Correo Electrónico"/>
	</fieldset>

	<fieldset id="ubicacion-fieldset" class="ui-widget-content">
		<legend>Lugar Nacimiento</legend>
		<select id="pais-pariente-select" name="pais-pariente-select" data-placeholder="País Nacimiento" class="chzn-select">
			<?php
				$x = array('prompt' => '');
				if (isset($pariente->localidad_id1)) {
					$id_pais = Localidad::model()->findByPk($pariente->localidad_id1)->Pais_id;
				} else {
					$id_pais = null;
				}
				echo CHtml::listOptions(Pais::model()->findByPk($id_pais), CHtml::listData(Pais::model()->findAll(), 'id', 'nombre'), $x)
			?>
		</select>
		<select id="provincia-pariente" name ="provincia-pariente" data-placeholder="Provincia Nacimiento" class="chzn-select">
			<?php
				$x = array('prompt' => '');
				if (isset($pariente->localidad_id1)) {
					$id_provincia = Localidad::model()->findByPk($pariente->localidad_id1)->provincia_id;
					echo CHtml::listOptions(Provincia::model()->findByPk($id_provincia), CHtml::listData(Provincia::model()->findAll("pais_id = $id_pais"), 'id', 'nombre'), $x);
				} else {
					$id_provincia = null;
					echo CHtml::listOptions(Provincia::model()->findByPk($id_provincia), CHtml::listData(Provincia::model()->findAll("id=-1"), 'id', 'nombre'), $x);
				}
			?>
		</select> 
		<select id="localidad-pariente" name ="localidad-pariente" data-placeholder="Localidad Nacimiento" class="chzn-select">
			<?php
				$x = array('prompt' => '');
				if (isset($pariente->localidad_id1)) {
					echo CHtml::listOptions($pariente->localidad_id1, CHtml::listData(Localidad::model()->findAll("provincia_id = $id_provincia"), 'id', 'nombre'), $x);
				} else {
					echo CHtml::listOptions($pariente->localidad_id1, CHtml::listData(Localidad::model()->findAll("id=-1"), 'id', 'nombre'), $x);
				}
			?>
		</select>
	</fieldset>

	<fieldset id="vive-fieldset" class="ui-widget-content">
		<legend>¿ Vive ?</legend>
		<?php echo CHtml::checkBox("vive", $pariente->vive); ?>
	</fieldset>

</form>
