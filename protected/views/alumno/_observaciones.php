<?php foreach ($observaciones as $obs): ?>
  <?php creaObservacion($obs); ?>
<?php endforeach; ?>
<?php creaObservacion($obsNueva); ?>
<?php

function creaObservacion($obs) { ?>
  <?php /* @var $obs ObservacionesAlumno */; ?>
  <?php if (!$obs) return; ?>

  <?php $id = $obs->isNewRecord ? "nuevo" : $obs->id; ?>
  <?php $class = $obs->isNewRecord ? " nuevo " : ""; ?>
  <?php $fecha = !$obs->isNewRecord ? "Creado el " . date("d/m/Y \a \l\a\s G:i", mystrtotime($obs->fecha)) . " Horas" : " Observación NUEVA"; ?>
<div class="observacion ui-widget-content ui-corner-all <?php echo $class; ?>" 
       id="<?php echo $id; ?>" 
       alumno_id="<?php echo $obs->Alumno_id; ?>">
    <div class="observacion-titulo ui-widget-header ui-corner-top">
      <span class="observacion-user"><?php echo "Por: " . $obs->user->nombre; ?></span>
      <span class="observacion-fecha-creacion"><?php echo $fecha; ?> </span>
    </div>
    <div class="observacion-texto">

      <?php echo $obs->texto; ?>

    </div>
    <?php $data = CHtml::listData(ObservacionesTipo::model()->findAll(), "id", "nombre"); ?>
    <?php if ($obs->user->id == Yii::app()->user->id): ?>
      <div class="observacion-tipo">
        Tipo de observación
        <?php
        $opt = array();
        echo CHtml::dropDownList(
                "observaciones_tipo_id", $obs->observaciones_tipo_id, $data, $opt);
        ?>
      </div>
    <?php else: ?>
    <?php endif; ?>
    <div class="muestra-observacion-tipo  ui-widget-content ui-corner-all">
      <span class="negrita">
        Tipo de observación:
        <span class="observacion-tipo-texto black big">
          <?php echo $obs->observacionesTipo ? $obs->observacionesTipo->nombre : "Nueva"; ?>
        </span>
      </span>
    </div>

    <?php if ($obs->user->id == Yii::app()->user->id): ?>
    <?php else: ?>
    <?php endif; ?>

    <?php if ($obs->user->id == Yii::app()->user->id): ?>
      <div class="observacion-pie">
        <div class="div-buttons-actions">
          <a class=" a-icon" onclick="edita(this)"><span class="ui-icon ui-icon-pencil"></span>Edita</a>
          <a class=" a-icon" onclick="elimina(this)"><span class="ui-icon ui-icon-trash"></span>Elimina</a>
        </div>
        <div class="div-buttons-edit">
          <a class=" a-icon" onclick="graba(this)"><span class="ui-icon ui-icon-disk"></span>Graba</a>
          <a class=" a-icon" onclick="cancela(this)"><span class="ui-icon ui-icon-cancel"></span>Cancela</a>
          <?php $chequed = $obs->publica ? "CHEQUED" : ""; ?>
          <span class="publica">
            Pública<input type="checkbox" class="publica" name="publica" <?php echo $chequed; ?>/>
          </span>
        </div>
      </div>
    <?php endif; ?>
  </div>

<?php } ?>

<script type="text/javascript">
  var matricula = "<?php echo $matricula; ?>";
</script>


