<style type="text/css">
	#errores{position: fixed; top: 5px; right: 5px; display:none; }
	.span-19 {
		width: 954px;
	}
	.active-result.highlighted{border-radius: 4px}
	.ui-widget{font-size: 85%;}
	.ui-widget-header{ text-align: center}
	.chzn-container-single .chzn-single div b{background: url("images/chosen-sprite.png") no-repeat scroll 0 -2px transparent;}
	.chzn-container-single .chzn-single{height: 20px;line-height: 20px;}

	#main-form{ position: relative}

	#div-elije-alumno #alumno_ac{width:300px}


	#div-datos { float: left;  margin: 5px 0;  min-height: 341px;  padding: 5px;  position: relative;  width:100%  }

	#div-matricula{ width: 200px; }


	.row{margin: 4px 0px 5px 0px}
	.row label{    float: left;    padding-right: 7px;    padding-top: 4px;    text-align: right;    width: 72px;}

	fieldset{ padding: 5px; border-radius: 5px; margin: 2px 0 3px 0;}
	fieldset{background: none repeat scroll 0 0 transparent !important;}
	legend{float:left; padding-top:4px; margin-right:5px;width:85px; }

	#division-familia{float:right; width: 400px; clear: both}

	#familia{    min-height: 168px;  padding: 0; width: 400px; position: relative; float: right}
	#familia #div-elije-familia{ margin: 5px 0px 0px 5px }
	#familia #div-elije-familia label{width:120px; float: none}
	#familia #familia-ac{width: 300px}
	#familia #nuevo-miembro-button{ position: absolute; right: 4px; bottom: 6px; display: none;width: 58px;}
	#familia #edita-miembro-button{ position: absolute; right: 4px; bottom: 42px; display: none;width: 58px;}
	#familia #elimina-miembro-button{ position: absolute; right: 4px; bottom: 78px; display: none;width: 58px;}
	#familia #edita-obs-cobranza-button{ position: absolute; right: 4px; bottom: 114px; display: none;width: 58px;}
	#familia #reasigna-familia-button{  bottom: 6px;    position: absolute;    right: 106px; }

	#asignacion-divison-foto{float:right; width: 100%}
	#asignacion-division{    float: left; min-height: 181px;  padding: 0;  position: relative;  width: 200px;}
	#asignacion-division .datos {margin-top:3px;    max-width: 64%;    padding: 5px; float: left; font-weight: bold}
	#asignacion-division  input[type="button"]{bottom: 5px;
																						 position: absolute;
																						 right: 4px;}
	/*    #asignacion-division #graba-asignatura-nueva-button{font-size: 90%;    margin-top: -2px;    padding: 2px;}    */

	#div-foto{    border: 1px solid #C4C8CC;  border-radius: 9px 9px 9px 9px;  box-shadow: 5px 5px 7px #888888;  float: right;  height: 185px;  margin: 0 9px 5px 0;  position: relative;  text-align: center;}
	#div-foto img{   border-radius: 8px 8px 8px 8px;  height: 185px;  text-align: center;  vertical-align: middle;  }
	#div-foto .buttons{  text-align: center;  position: absolute; bottom: -40px;  width: 100%
	}

	#div-asignatura-edita input[type="button"]{ padding: 1px 0}
	.row .inline{display: inline-block}
	.row select {width: 180px !important}
	#div-test5 input{width: 150px !important}

	#domicilio-fieldset, #ubicacion-fieldset, #nombre-fieldset, #documento-fieldset, #obra-social-fieldset, #telefonos-fieldset{width: 484px}
	#nombre-fieldset input,#obra-social-fieldset input, #telefonos-fieldset input {width: 190px}

	.inline{display:inline}

	#matricula{width: 45px}
	/*    #div-activo{margin-left: 5px;ffloat: right;margin-right: 10px;margin-bottom: 5px}
			div-activo *{color:red;font-weight: bold;font-size: 105%}*/
	#activo{width: 8px}

	#documento_tipo_id_chzn{float: left}
	#div-test6 input{width: 127px !important}
	#numero_documento{float:left;margin-left:4px; margin-top: -1px; width: 160px}
	#calle{width: 140px !important}
	#fecha_nacimiento{}
	#numero{width: 45px !important}
	#pais-select{width: 180px}
	#localidad{width: 180px}
	#provincia{width: 180px}
	#numero_documento_par{width: 100px}
	#sexo, #grupo_sanguineo, #pariente-select{width: 180px}
	#vive_con_id {width: 280px}
	#nacionalidad{width: 200px}
	#email, #email2{width: 300px}
	#pais-pariente-select{width: 180px}
	#localidad-pariente{width: 180px}
	#provincia-pariente{width: 180px}

	#piso{width: 35px !important}
	#departamento{width: 44px !important}
	#div-beca input{width: 50px !important}
	#div-gs input{width: 70px !important}

	.buttons-div{     clear: both;    padding-top: 11px;    text-align: right;}
	.right-align{text-align: right}
	.titulo{padding: 3px; background-color: lightblue; border-radius: 5px 5px 0px 0px}

	#comentarios{ padding-top: 10px; clear: both}
	#alumno-obs textArea{height: 87px;width: 100%;}
	#obs-cobranza-dialog{display:none}
	#obs-cobranza-dialog textArea{height: 100%;width: 100%;}
	#pariente-form{overflow-x: hidden;overflow-y: hidden;}
	#obs_beca {width: 280px }
	#estado_id{width:150px}
	#estado_id_chzn {vertical-align: sub;}
</style>
