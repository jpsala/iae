<link rel="stylesheet" type="text/css" href="js/jqgrid/css/ui.jqgrid.css" />
<script src="js/jqgrid/js/i18n/grid.locale-es.js"></script>
<script src="js/jqgrid/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript">
	var urlAlumnoAC = "<?php echo $this->createUrl('alumno/autoComplete', array("activo" => "0")); ?>";
	var urlFamiliaAC = "<?php echo $this->createUrl('familia/autoComplete'); ?>";
	var urlFamiliaGrid = "<?php echo $this->createUrl("alumno/familiaGridAjax"); ?>";
	var alumno_id = null, familia_id = null;
	var $gridFamilia, obsFamilia = null;

	$(function() {
		init();
	});

	function init() {
		$("#sexo").chosen();
		initEstadoIdChosen();
		$("#grupo_sanguineo").chosen();
		$("#vive_con_id").chosen();
		$("#graba, #graba-mas-nuevo, #cancela, #cambia-foto-button, #nuevo-miembro-button, #edita-obs-cobranza-button, #edita-miembro-button, #elimina-miembro-button, #reasigna-familia-button").button();
		//$("#Localidad, #documento_tipo_id, #pais-select").chosen();

		$("#pais-select").chosen().change(function() {
			changePais($(this));
		});

		$("#provincia").chosen().change(function() {
			changeProvincia($(this));
		});

		$("#Localidad").chosen().change(function() {
		});
		$(" #documento_tipo_id").chosen();

		$("#fecha_nacimiento").datepicker();
		$("#nacionalidad-select").chosen();

		creaFamiliaAutoComplete();

		creaAlumnoAutoComplete();

		initDialogPariente();

		$("#obs-cobranza-dialog").dialog({
			title: "Observaciones de cobranza",
			autoOpen: false,
			width: 500,
			height: 350,
			position: [
				"top", 50
			],
			buttons: {
				Ok: function() {
					obsFamilia = $(this).find("textArea").val();
					$(this).dialog("close");
				}
			}
		});

		checks();


	}

	function initEstadoIdChosen(){
		$("#estado_id").chosen().change(estado_idChanged);
	};

	function estado_idChanged(event, object){
		var estado_id = event.target.value;
		var esIngresante = Number(estado_id) === 5;
		if(esIngresante && !alumno_id){
			traeMatriculaIngresante();
		}
	}

	function traeMatriculaIngresante(){
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo $this->createUrl("alumno/adminTraeMatriculaIngresante"); ?>",
			success: function(data) {
				//asignaNuevaMatriculaParaIngresante(data.matricula);
			}
		})
	}

	function asignaNuevaMatriculaParaIngresante(matricula){
		$('#matricula').val(matricula);
	}

	function checks() {
		$('#ingresante').click(function()
		{
			var thischeck = $(this);
			if (thischeck.is(':checked'))
			{
				if ($('#activo').attr('checked'))
				{
					alert('El alumno no puede estar activo y ser ingresante, se destilda Activo');
					$('#activo').attr('checked', false);
				}
			}
		});
		$('#activo').click(function()
		{
			var thischeck = $(this);
			if (thischeck.is(':checked'))
			{
				if ($('#ingresante').attr('checked'))
				{
					alert('El alumno no puede ser ingresante y estar activo, se destilda Ingresante');
					$('#ingresante').attr('checked', false);
				}
			}
		});
	}

	function changePais($this) {
		pais_id = $this.val();
		$.ajax({
			type: "GET",
			//dataType: "json",
			data: {pais_id: $this.val()},
			url: "<?php echo $this->createUrl("crud/TraeProvinciasPorPais"); ?>",
			success: function(data) {
				$("#provincia").html(data);
				$("#provincia").trigger("liszt:updated");
				$("#provincia_chzn").mousedown();

			},
			error: function(data, status) {
			}
		});
		return false;
	}

	function changeProvincia($this) {
		provincia_id = $this.val();
		$.ajax({
			type: "GET",
			//dataType: "json",
			data: {provincia_id: $this.val()},
			url: "<?php echo $this->createUrl("crud/TraeLocalidadesPorProvincia"); ?>",
			success: function(data) {
				$("#Localidad").html(data);
				$("#Localidad").trigger("liszt:updated");
				$("#Localidad_chzn").mousedown();

			},
			error: function(data, status) {
			}
		});
		return false;
	}

	function creaAlumnoAutoComplete() {
		$('#alumno_ac').focus().autocomplete({autoSelect: true, autoFocus: true, minLength: 2, source: urlAlumnoAC,
			select: function(event, ui) {
				alumno_id = ui.item.id;
				//                $("#alumno_ac").attr('disabled', "disabled");
				traeAlumno(ui.item.id);
				$("#nombre").focus();
			}
		});
	}

	function creaFamiliaAutoComplete() {
		$('#familia-ac').autocomplete({autoSelect: true, autoFocus: true, minLength: 2, source: urlFamiliaAC,
			select: function(event, ui) {
				$("#familia_id").val(ui.item.id);
				familia_id = ui.item.id;
				$("#div-elije-familia").hide();
				//                $("nuevo-miembro-button,").hide();
				initGridFamilia();
			}
		});
	}

	function traeAlumno(alumno_id) {
		alumno_id_global = alumno_id;
		var matricula, src;
			$.ajax({
			type: "GET",
			data: {"alumno_id": alumno_id},
			url: "<?php echo $this->createUrl("alumno/adminForm"); ?>",
			success: function(data) {
				$("#errores").hide();
				$("#div-datos").html(data);
				initGridFamilia();
				matricula = $("#matricula").val();
				if (alumno_id != -1) {
					src = "http://iae.dyndns.org/fotos/thumbnails/" + matricula + ".gif?v=1";
					$("#div-elije-alumno").hide();
				} else {
					creaFamiliaAutoComplete();
				}
				$("#foto").attr("src", src);
				$("#re-asigna, #graba,#graba-mas-nuevo, #cancela, #cambia-foto-button, #nuevo-miembro-button,#edita-obs-cobranza-button, #edita-miembro-button, #elimina-miembro-button, #reasigna-familia-button").button()
				$("#sexo").chosen();
				initEstadoIdChosen();
				$("#grupo_sanguineo").chosen();
				$("#vive_con_id").chosen();
				$("#pais-select").chosen().change(function() {
					changePais($(this));
				});
				$("#provincia").chosen().change(function() {
					changeProvincia($(this));
				});
				$("#Localidad").chosen().change(function() {
				});
				$(" #documento_tipo_id").chosen();
				$("#fecha_nacimiento").datepicker();
				$("#obs-cobranza-dialog textArea").val($("#obs_cobranza").val());
				$("#nacionalidad-select").chosen();
				checks();

			},
			error: function(data, status) {
			}
		});
	}


	function initGridFamilia() {
		familia_id = $("#familia_id").val();
		if (!familia_id)
			return;
		urlParams = '&q=2&familia_id=' + familia_id + '&alumno_id=' + alumno_id;
		$('#familia-grid').setGridParam({url: urlFamiliaGrid + urlParams});
		$gridFamilia = jQuery("#familia-grid").jqGrid({url: urlFamiliaGrid + urlParams,
			datatype: "json",
			colNames: ['Familia', 'Relación'],
			colModel: [
				{name: 'nombre', index: 'nombre', width: 210, editable: true, sortable: false},
				{name: 'relacion', index: 'relacion', width: 90, editable: true, sortable: false}
			],
			rowNum: 10,
			autowidth: false,
			rowList: [10, 20, 30],
			pager: '#pfamilia-grid',
			sortname: 'nombre',
			viewrecords: true,
			sortorder: "desc",
			editurl: "server.php"
		});

		$('.ui-jqgrid-hdiv').hide();
		$("#gbox_familia-grid").css("width", "292px");
		$(".ui-jqgrid-bdiv").css("height", "110px");
		$("#nuevo-miembro-button, #edita-obs-cobranza-button, #edita-miembro-button, #elimina-miembro-button, #reasigna-familia-button").button().show();
		$('#familia-grid').trigger('reloadGrid')
		$("#gbox_familia-grid").show();
		//$("#pfamilia-grid").hide();
		//jQuery("#familia-grid").jqGrid('navGrid',"#pfamilia-grid",{edit:false,add:false,del:false});
		//jQuery("#familia-grid").jqGrid('inlineNav',"#pfamilia-grid");
	}

	function reasigna() {
		var nivel, anio, division;
		//@FIXME no anda cuando el alumno es nuevo o no tenía asignada division
		// arreglado,probar bien
		if ($(".datos").is(":visible")) {
			$(".datos").hide();
			$("#pedido-de-datos").show();
			$("#div-select-division").show();
			$("#re-asigna").hide();
		} else {
			$(".datos").show();
			$("#pedido-de-datos").hide();
			$("#re-asigna").show();
			nivel = $("#nivel-select option:selected").text();
			anio = $("#anio-select option:selected").text();
			division = $("#division-select option:selected").text();
			$("#div-datos .nivel").html(nivel);
			$("#div-datos .anio").html(anio);
			$("#div-datos .division").html(division);

		}
	}

	function initDialogPariente() {
		$("#pariente-dialog").dialog({
			autoOpen: false,
			modal: true,
			title: "Pariente",
			width: 600,
			height: "auto",
			position: [
				"top", 10
			],
			autosize: false,
			buttons: {
				Cancel: function() {
					$(this).dialog("close");
				},
				Ok: function() {
					grabaFormMiembro($(this));
				}
			}
		})
	}

	function grabaFormMiembro($dlg) {
		var data = "alumno_id=" + alumno_id + "&" + $dlg.find("form").serialize();
		$.ajax({
			type: "POST",
			dataType: "json",
			data: data,
			url: "<?php echo $this->createUrl("alumno/adminParienteGraba"); ?>",
			success: function(data) {
				muestraErrores(data.errors);
				if (!data.errors) {
					$gridFamilia.trigger("reloadGrid");
					$dlg.dialog("close");

				}

			},
			error: function(data, status) {
			}
		});


		return false;
	}

	function  eliminaMiembro() {
		var row = $gridFamilia.jqGrid('getGridParam', 'selrow'), pariente_id;
		//par_1029
		if (!row || row.substr(0, 4) != "par") {
			alert("Debe seleccionar un pariente primero en la tabla de familiares");
			return;
		}

		if (!confirm("Esta seguro de querer borrar este miembro de la familia?")) {
			return;
		}

		pariente_id = row.substr(4, 5).trim();

		$.ajax({
			type: "GET",
			data: {pariente_id: pariente_id},
			url: "<?php echo $this->createUrl("alumno/adminParienteElimina"); ?>",
			success: function(data) {
				$gridFamilia.trigger("reloadGrid")
			},
			error: function(data, status) {
			}
		});

	}

	function  editaMiembro() {
		var row = $gridFamilia.jqGrid('getGridParam', 'selrow');
		//par_1029
		if (!row || row.substr(0, 3) != "par") {
			alert("Debe seleccionar un pariente primero en la tabla de familiares");
			return;
		}
		pariente_id = row.substr(4, 6).trim();
		traeFormPariente(pariente_id);
	}

	function nuevoMiembro() {
		if (!alumno_id || !familia_id) {
			alert("El alumno  debe ser dado de alta  y la familia asignada/creada antes de agregar un pariente...");
			return;
		}
		traeFormPariente(-1);
	}

	function traeFormPariente(pariente_id) {
		$.ajax({
			type: "GET",
			//dataType: "json",
			data: {pariente_id: pariente_id},
			url: "<?php echo $this->createUrl("alumno/adminTraePariente"); ?>",
			success: function(data) {
				$("#pariente-dialog").html(data);
				$("#pariente-dialog").dialog("open");
			},
			error: function(data, status) {
			}
		});

	}

	function nuevaFamilia() {
		if (!familia_id) {
			alert("El alumno  debe ser dado de alta antes de crear la familia...");
			return;
		} else {
			$("#div-elije-familia").show();
			$("#gbox_familia-grid, #elimina-miembro-button, #edita-obs-cobranza-button, #edita-miembro-button, #nuevo-miembro-button, #reasigna-familia-button").hide();
			creaFamiliaAutoComplete();
		}

	}

	function cancelaForm() {
		limpiaTodo();
		alumnoFocus();
		return false;
	}

	function grabaForm(nuevo) {
		$.ajax({
			type: "POST",
			dataType: "json",
			data: (familia_id ? "Familia_id=" + familia_id + "&" : "") +
					"alumno_id=" + alumno_id +
					"&" + $("form").serialize() +
					"&obs_familia=" + obsFamilia,
			url: "<?php echo $this->createUrl("alumno/adminGraba"); ?>",
			success: function(data) {
				if (muestraErroresDoc(data.errors))
					return;

				if (data.nuevo) {
					alumno_id = data.alumno_id;
					//TODO: aca voy
					//console.log(data,nuevo,$(ingresante).value());
					if (data.matricula !== matricula) {
						alert("El alumno se dió de alta con la matrícula Nro. " + data.matricula);
					}
					if (!nuevo) {
						$("#familia_id").val(data.familia_id);
						$("#div-elije-familia").hide();
						$("#reasigna-familia-button").show();
						jQuery("#familia-grid").GridUnload();
						initGridFamilia();
					}
				}
				$("#pais-select").chosen().change(function() {
					changePais($(this));
				});

				$("#provincia").chosen().change(function() {
					changeProvincia($(this));
				});

				$("#Localidad").chosen().change(function() {
				});
				$("#nacionalidad-select").chosen();
				if (nuevo && !data.errors) {
					limpiaTodo();
				}
			},
			error: function(data, status) {
			}
		});
		return false;
	}

	function muestraErrores(errores) {
		var $errores = $("#errores");
		if (!errores) {
			$errores.hide();
			return;
		}
		$errores.html("");
		for (i in errores) {
			$errores.append("<li class=\"error-item\">" + errores[i] + "</li");
		}
		console.log($errores);
		$errores.show();
	}

	function cambiaLaFoto() {
		$(".qq-upload-button input").click();
	}

	function grabaFormMasNuevo() {
		grabaForm(true);
		alumnoFocus();
		return false;
	}

	function limpiaTodo() {
		//$("#alumno_ac").removeAttr('disabled');
		traeAlumno(-1);
		$("#alumno_ac").val("").focus();
		alumno_id = null;
		familia_id = null;
		anio_id = null;
		division_id = null;
		nivel_id = null;
		$("#div-elije-alumno").show();
	}

	function editaObsCobranza() {
		$("#obs-cobranza-dialog").dialog("open");
	}

	function alumnoFocus() {
		setTimeout(function() {
			$("#alumno_ac").focus();
		}, 100);
	}
</script>
