<style type="text/css">
	#socio{width:250px;margin-left: 8px;}
	#alumno-cbu{width:200px}
	#obsfs{width: 98.6%}
	#debitofs{display: block;width: 50.5%}
	#visafs {display: inline;width: 50.46%; }
	#buttons-div{float: right; margin-top: 10px}
	#alumno-debito_cbu {width: 209px;}
	#obsfs div{width: 50%}
	.gen-fset-visafs-visa_activo{margin-left: 66px}
	/*gen-fset-visafs-visa_activo*/
</style>

<div id="div-socio">
	<input id="socio" placeholder="Alumno"/>
</div>

<form id="data-div"><?php echo $div; ?></form>

<div id="buttons-div">
	<button id="cancela" onclick="return cancelaAlumno();">Cancela</button>
	<button id="graba" onclick="return grabaAlumno();">Graba</button>
</div>

<script type="text/javascript">
	$("#graba, #cancela").button();
	$("#alumno-gen input:visible, #alumno-gen textarea").attr("disabled", "disabled");

	socioAC(function(item) {
		traeAlumno(item.id);
	}, "<?php echo $this->createUrl('socio/sociosAc'); ?>&socioTipo=alumno");

	function traeAlumno(id) {
		$.ajax({
			type: "GET",
			//dataType: "json",
			data: {socio_id: id},
			url: "<?php echo $this->createUrl("alumno/admin2Ajax"); ?>",
			success: function(data) {
				$("#data-div").html(data);
//                    bindKeys();
				if (id === -1) {
					$("#socio").removeAttr("disabled");
					$("#alumno-gen input:visible, #alumno-gen textarea").attr("disabled", "disabled");
					$("#socio").val("").focus();
				} else {
					$("#alumno-apellido").first().focus();
					$("#alumno-gen input:visible, #alumno-gen textarea").removeAttr("disabled");
					$("#socio").attr("disabled", "disabled");
				}
			},
			error: function(data, status) {
			}
		});
	}

	function cancelaAlumno() {
		traeAlumno(-1);
	}

	function grabaAlumno() {
		var data = $("#data-div").serialize();
		$.ajax({
			type: "GET",
			dataType: "json",
			data: data,
			url: "<?php echo $this->createUrl("alumno/admin2Graba"); ?>",
			success: function(data) {
				if (data) {
					muestraErroresDoc(data);
					return false;
				}
				traeAlumno(-1);
			},
			error: function(data, status) {
			}
		}
		);
	}

	function bindKeys() {
		var controles = "input:visible,body,textarea:visible,button:visible";
		$(controles).unbind("keydown.a");
		$(controles).bind("keydown.a", "ctrl+c , esc", function(e) {
			$("#cancela").click();
			return false;
		});
		$(controles).bind("keydown.a", "ctrl+s", function(e) {
			$("#graba").click();
			return false;
		});
	}

</script>