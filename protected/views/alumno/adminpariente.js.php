<link rel="stylesheet" type="text/css" href="js/jqgrid/css/ui.jqgrid.css" />
<script src="js/jqgrid/js/i18n/grid.locale-es.js"></script>
<script src="js/jqgrid/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript">
        $("#pais-pariente-select").chosen().change(function(){
            changePais($(this));
        }); 
        $("#provincia-pariente").chosen().change(function(){
             changeProvincia($(this));
        }); 
        $("#nacionalidad").chosen();
        
        $("#localidad-pariente").chosen().change(function(){
        }); 
        $("#documento_tipo_id").chosen(); 
        $("#pariente-select").chosen();
        $("#fecha_nacimiento_par").datepicker();

    

    function changePais($this){
        pais_id = $this.val();
        $.ajax({
            type: "GET",
            //dataType: "json",
            data: {pais_id:$this.val()},
            url: "<?php echo $this->createUrl("crud/TraeProvinciasPorPais"); ?>",
            success: function(data) {
                $("#provincia-pariente").html(data);
                $("#provincia-pariente").trigger("liszt:updated");
                $("#provincia-pariente_chzn").mousedown();

            },
            error: function(data,status){
            }
        });
        return false;
    }
    
     function changeProvincia($this){
        provincia_id = $this.val();
        $.ajax({
            type: "GET",
            //dataType: "json",
            data: {provincia_id:$this.val()},
            url: "<?php echo $this->createUrl("crud/TraeLocalidadesPorProvincia"); ?>",
            success: function(data) {
                $("#localidad-pariente").html(data);
                $("#localidad-pariente").trigger("liszt:updated");
                $("#localidad-pariente_chzn").mousedown();

            },
            error: function(data,status){
            }
        });
        return false;
    }


    
    function muestraErrores(errores){
        var $errores = $("#errores");
        if(!errores){
            $errores.hide();
            return;
        }
        $errores.html("");
        for(i in errores){
            $errores.append("<li class=\"error-item\">"+errores[i]+"</li");
        }
        $errores.show();
    }
    
</script>
