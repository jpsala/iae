<script type="text/javascript">
  var alumnoDialog = Object;
  alumnoDialog.buttonId = null;
  alumnoDialog.init = function(matricula,buttonId){
    src = "http://iae.dyndns.org/fotos/" + matricula + ".jpg";
    setTimeout(function(){$("#foto").attr("src","nada");},1);
    setTimeout(function(){
      $("#foto").attr("src",src);
    },1000);
    alumnoDialog.buttonId = buttonId;
    $("#"+alumnoDialog.buttonId).html('<button id="alumno-dialog-muestra" onclick="return alumnoDialog.showDialog();return false">Ver datos</button>');
    $("#alumno-dialog-muestra").button().show();
    $.ajax({
    type: "GET",
    //      dataType:"json",
    data: {matricula:matricula
    },
    url: "<?php echo $this->createUrl("alumno/datosAlumnoDialog"); ?>",
    success: function(data) {
      $("#alumno-dialog").html(data);
    },
    error: function(data,status){
    }
  });
    
  }
  
  alumnoDialog.showDialog = function(){
    $("#alumno-dialog").dialog("open");
    return false;
  }
  
  jQuery(document).ready(function(){
    $("#alumno-dialog").dialog({
      width:"auto",
      height:"auto",
      autoOpen:false,
      title:"Datos del alumno",
      buttons:{
        ok:function(){
          $(this).dialog("close");
        }
      }
    })  
  });

</script>
