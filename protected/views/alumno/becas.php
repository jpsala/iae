<style type="text/css">
    /*.container {width: 100%}*/
    #browse-table{}
    .filtro-td{width: 200px}
    /*.matricula-td{width: 70px}*/
    .beca-td input{width: 40px; text-align: right}
    .obs_beca-td {width:200px}
    .asignacion-td{width: 100px}
    .pariente-td{width: 200px}
    .domicilio-td{width: 200px}
    .saldo-td,.saldoFinal-td{text-align: right}
    #solo-con-beca-div{display: inline-block;vertical-align: middle;}
    #filtra-button{margin-left: 8px}
    #filtro-dif > input {margin-top: 0;vertical-align: text-top;}
    div.editThis {min-height: 19px;width: 250px; background-color: white !important}
    div.editThis:hover,
    div.editThis:focus {}
</style>
<div id="browse-div">
    <form>
        <div id="filtro-dif">
            <input id="filtro" placeholder="Filtro" value="<?php echo $filtro; ?>"/>
            <div id="solo-con-beca-div">
                <label id="solo-con-beca-label" for="solo-con-beca">Solo con beca:</label>
                <?php $checked = $solo_con_beca == "true" ? "CHECKED" : ""; ?>
                <input type="checkbox" id="solo-con-beca" <?php echo $checked; ?>/>
            </div>
            <button id="filtra-button" onclick="return filtra();" >Filtra</button>
        </div>
    </form>
    <table id="browse-table">
        <tr>
            <th class="filtro-td">Alumno</th>
            <th class="matricula-td">Matr.</th>
            <th class="beca-td">Beca</th>
            <th class="obs_beca-td">Obs.</th>
            <th class="asignacion-td">Asignacion</th>
            <th class="pariente-td">Vive con</th>
            <th class="saldo-td">Saldo</th>
            <th class="saldoFinal-td">ID de familia</th>
            <!--<th class="saldoFinal-td">Con Beca</th>-->
            <!--<th class="domicilio-td">Domicilio</th>-->
            <th></th>
        </tr>
        <?php foreach ($data as $a): ?>
            <tr alumno_id="<?php echo $a["id"]; ?>">
                <td class="filtro-td"><?php echo $a["alumno"]; ?></td>
                <td class="matricula-td"><?php echo $a["matricula"]; ?></td>
                <td class="beca-td">
                    <input type="text" value="<?php echo $a["beca"]; ?>" onchange="change($(this));"/>
                </td>
                <td class="obs_beca-td">
                        <div class="input editThis" contenteditable="true" onchange="change($(this));">
                            <?php echo $a["obs_beca"]; ?>
                        </div>
                </td>
                <td class="asignacion-td"><?php echo $a["asignacion"]; ?></td>
                <td class="pariente-td"><?php echo $a["pariente"]; ?></td>
                <td class="saldo-td"><?php echo number_format($a["saldo"], 2); ?></td>
                <!--<td class="saldoFinal-td"><?php // echo number_format($a["saldo"] - ($a["saldo"] * $a["beca"] / 100), 2); ?></td>-->
                <td class="familia_id-td"> <?php echo $a["familia_id"];?> </td>
                <!--<td class="domicilio-td"><?php //echo $a["pariente_domicilio"];         ?></td>-->
                <td></td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>

<script type="text/javascript">
                $("button").button();
                $("#filtro").focus().select();

                $('[contenteditable]').on('focus', function() {
                    var $this = $(this);
                    $this.data('before', $this.html());
                    return $this;
                }).on('blur', function() {
                    var $this = $(this);
                    if ($this.data('before') !== $this.html()) {
                        $this.data('before', $this.html());
                        $this.trigger('change');
                    }
                    return $this;
                });

                function filtra() {
                    $.ajax({
                        type: "GET",
                        //      dataType:"json",
                        data: {
                            filtro: $("#filtro").val(),
                            solo_con_beca: $("#solo-con-beca").is(":checked"),
                            ajax: 1
                        },
                        url: "<?php echo $this->createUrl("alumno/becas"); ?>",
                        success: function(data) {
                            $("#browse-div").html(data);
                        },
                        error: function(data, status) {
                        }
                    }
                    );
                    return false;
                }

                function change($obj) {
                    var $tr = $obj.closest("tr");
                    var beca = $tr.find(".beca-td input").val();
                    var saldo = $tr.find(".saldo-td").html().replace(",", "");
                    var saldoFinal = saldo - (saldo * beca / 100);
                    var alumno_id = $tr.attr("alumno_id");
                    $tr.find(".saldoFinal-td").html(addCommas(saldoFinal.toFixed(2)));
                    $tr.find(".beca-td input").val(Number(beca).toFixed(2));
                    $.ajax({
                        type: "POST",
                        data: {
                            alumno_id: alumno_id,
                            beca: $tr.find(".beca-td input").val(),
                            obs_beca: $tr.find(".editThis").html()
                        },
                        url: "<?php echo $this->createUrl("alumno/becasGraba"); ?>",
                        success: function(data) {
                        },
                        error: function(data, status) {
                        }
                    });
                }

                function addCommas(nStr) {
                    nStr += '';
                    x = nStr.split('.');
                    x1 = x[0];
                    x2 = x.length > 1 ? '.' + x[1] : '';
                    var rgx = /(\d+)(\d{3})/;
                    while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + ',' + '$2');
                    }
                    return x1 + x2;
                }
</script>