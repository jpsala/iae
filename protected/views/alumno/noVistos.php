<table class="table-bordered table-responsive table-condensed">
  <thead>
  <tr>
    <th>Alumno</th>
    <th>Encargado</th>
    <th>Documento</th>
    <th></th>
  </tr>
  </thead>
  <tbody data-bind="foreach:{data: data, as:'data'}">
  <tr>
    <td data-bind="text:alumno"></td>
    <td data-bind="text:encargado"></td>
    <td data-bind="text:nombre"></td>
    <td data-bind="text:numero_documento"></td>
<!--    <td data-bind="text:id"></td>-->
  </tr>
  </tbody>
</table>
<script>
  var vm = (function () {
    var pub = {};
    pub.data = <?php echo json_encode($noVistos);?>;
    return pub;
  })()
  ko.applyBindings(vm)
</script>