<style type="text/css">
  labelHighlight{color: #AAA; font-style: italic;}

  #content{padding:10px 5px 0px !important}

  #main-form {padding: 5px;margin: 5px 0px 5px; border: 1px #75C2F8 solid; border-radius: 8px ; position: relative;overflow: visible;min-height: 425px}

  #izq{ width: 67%;; float: left; border-radius: 5px; background-color: #f9fafc; border: #eff3fc solid 1px; padding: 5px;
        position: relative; margin: 7px 0}
  #der{   width: 30%;  float: right; margin-right: 13px}

  #izq-content{min-height: 330px}
  #izq-content th, #izq-content td, #izq-content caption {padding: 4px 3px 4px 5px !important}
  .titulo{ background-color: #C3D9FF;    border-bottom: 1px solid #CCCCCC;    border-bottom: 1px solid #EFF3FC;    border-radius: 5px 5px 5px 5px;  margin-bottom: 5px;    padding: 3px;    text-align: center;color: #555555;  font-weight: bold;}


  #total-div span.total, #total-pagos-div span.total{    position: absolute;    right: 4px; font-weight: bold}

  #botones{ clear: both;margin:11px 0px 2px 11px; position: relative; text-align: right; top:4;}
  tr:nth-child(even)    { background-color:#edf1fe}
  .esNuevo{color:red}
  .right-align {  text-align: right }
  .ui-widget { font-size: 12px; }  
  .bold{font-weight: bold}
  .total{background-image: url("images/pesos.gif");background-position: 0 50%;background-repeat: no-repeat;background-size: 10px auto;padding-left: 15px;}
  .tittle{padding: 5px}
  input::-webkit-input-placeholder, textarea::-webkit-input-placeholder {
    color: #636363;
  }
  input:-moz-placeholder, textarea:-moz-placeholder {
    color: #636363;
  }


  .indent{margin-left: 21px; max-width: 600px; border-left: #EEE dashed 1px}
  .observacion{margin: 7px 12px 7px 5px;min-height: 67px;width: 270px;float: left;}
  .observacion *{font-size:87%}
  .observacion-titulo{padding: 1px; text-align: left}
  .observacion-texto{padding: 6px;min-height: 21px;}
  .observacion-user{color:lightcoral}
  .observacion-pie{min-height: 22px;  border-top: lavender solid 1px; min-width: 100%;padding-top: 1px;}

  .observacion-fecha-creacion,.observacion-fecha-modificacion{float: right; margin:5px 5px 0 0;color:lightcoral}
  .observacion-fecha-modificacion{font-weight: bold; color:#D63301}
  .observacion-fecha-creacion{margin:0}

  #edit-template{display:none; width: 100%; max-height: 200px; }
  .editando{background-color: white}
  .a-icon{ text-decoration: none ; display: inline-block; line-height: 17px; margin: 0 2px; cursor: pointer; padding: 2px}
  .a-icon:hover{ text-decoration: underline;color: #0099FF}
  .a-icon span{float: left;margin-top:-1px}
  .div-buttons-edit,#div-observacion-template{display:none;width: 100%;}
  .div-buttons-actions,.div-buttons-edit{float:left}
  #observacion-nuevo{ margin: -10px 0 3px 10px; font-size: 70%}
  .observacion-estado{display: block;    margin: 1px 0 8px 5px;}
  .observacion-estado select{width: 130px; margin-top: 2px}
  .publica{float: right}
  #div-head{position: relative}
  .observacion-tipo{ margin: 10px 0 5px 0; display:none}
  .observacion-tipo select{height: 25px;margin: 0 0 0 10px;vertical-align: middle;width: 149px;}
  .muestra-observacion-tipo{margin: 0 8px 5px 5px;padding: 5px;}
  .negrita{font-weight: bold}
  .black{color:black}
  .big{font-size: 110%}
  .nuevo{background-color: lightgoldenrodyellow; border-color: red}
  #muestraDialogo{display: inline-block}
  #filtro-div{    display: inline-block;margin-bottom: 3px; padding-bottom: 2px; 
                  padding-left: 11px; padding-right: 0; padding-top: 2px; 
                  vertical-align: middle;}
  #filtro-button{margin-left: 18px;}
  #observaciones_tipo_filtro_chzn{float: left;margin-top: 1px;}
  #observaciones_tipo_filtro_label{float:left;line-height: 27px;margin-right: 6px;}
  #observaciones-solo-propias{margin-left: 10px;display: inline-block;}
  #solo_propias{vertical-align: bottom}

</style>