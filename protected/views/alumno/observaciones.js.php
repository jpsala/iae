<script type="text/javascript">
  var urlAC = "<?php echo $this->createUrl('socio/sociosAc'); ?>&socioTipo=<?php echo 'alumno'; ?>";
  var editando = false; textoBackup="";
  jQuery(document).ready(function(){
    onReady();
  });
    
  function onReady(){
    
    $("button").button(); 
    $("#filtro-div").hide();
    
    $('#socio').focus().autocomplete({
      autoSelect: true,autoFocus: true, 
      minLength: 2,source:urlAC,
      select: function( event, ui ) {
        socio_id = ui.item.id;
        traeObservaciones(ui.item.id);
      }
    });    
  }
  function filtra(){
    traeObservaciones(socio_id, $("#observaciones_tipo_filtro").val(), $("#solo_propias").attr("checked"));
  }
  
  function traeObservaciones(socio_id, filtro, soloPropias){
    var observaciones_tipo = "todas";
    if(filtro){
      observaciones_tipo = $("#observaciones_tipo_filtro").val();
    }
    if(!soloPropias){
      soloPropias=false;
    }
    $.ajax({
      type: "GET",
      //      dataType:"json",
      data: {
      },
      url: "<?php echo $this->createUrl("alumno/traeObservaciones"); ?>&socio_id="+socio_id+"&observaciones_tipo="+observaciones_tipo+"&soloPropias="+soloPropias,
      success: function(data) {
        $("#observaciones-items").html(data);
        $(".comentario-estado select").attr("disabled","disabled");
        $('#observaciones-items').masonry({
          itemSelector: '.observacion',
          columnWidth : 289
        });
        masonryReload();
        alumnoDialog.init(matricula,"muestraDialogo");
        $("#filtro-div").show();
      },
      error: function(data,status){
      }
    });
  }

  function masonryReload(){
    $('#observaciones-items').masonry('reload');
    setTimeout(function(){
      // we make masonry recalculate the element based on their current state.
      masonryReload();
    }, 250);
  }

  function nuevo(){
    var $nuevo = $("#div-observacion-template").clone();
    $nuevo.find(".div-buttons-edit").css("display","block").show();
    $nuevo.attr("id","nuevo").show();
    $(".indent:first").prepend($nuevo);
    $nuevo.after("<div class='indent'></div>");
    $nuevo.find(".observacion-texto").focus();
    $("#nuevo .observacion-estado select").removeAttr("disabled");    
  }
    
  function edita(obj){
    if(editando) return;
    $obj=$(obj);
    observacion_id = $obj.closest(".observacion").attr("id");
    $this = $("#"+observacion_id);
    $this.find(".observacion-tipo").show();
    $this.find(".muestra-observacion-tipo").hide();
    
    $observacionTexto = $this.find(".observacion-texto");
    textoBackup = $observacionTexto.html();
    $observacionTexto.attr("contenteditable","true");
    $observacionTexto.addClass("editando");
    $this.find(".observacion-texto").focus();
    $this.find(".div-buttons-actions").hide();
    $this.find(".div-buttons-edit").show();
    $(".observacion-estado select").removeAttr("disabled");
    
    editando = true;
  }
    
  function elimina(obj){
    if(editando) return;
    $obj=$(obj);
    observacion_id = $obj.closest(".observacion").attr("id");
    var $this = $("#"+observacion_id);
    $.ajax({
      type: "POST",
      data: {observacion_id:observacion_id},
      url: "<?php echo $this->createUrl("alumno/eliminaObservacion"); ?>",
      success: function(data) {
        $this.remove();
      },
      error: function(data,status){
      }
    });

  }
    
  function graba(obj){
    $obj=$(obj);
    observacion_id = $obj.closest(".observacion").attr("id");
    var $this = $("#"+observacion_id);
    var alumno_id = $this.attr("alumno_id");
    var publica = $this.find(".publica").is(":checked");
    var observaciones_tipo_id = $this.find("#observaciones_tipo_id").val();
    $observacionTexto = $this.find(".observacion-texto");
    texto = $observacionTexto.html();
    if(!texto){
      return;
    }
    $observacionTexto.removeClass("editando");
    $observacionTexto.removeAttr("contenteditable");
    $this.find(".observacion-tipo").hide();
    nuevoTipo = $this.find("select option:selected").text();
    $this.find(".observacion-tipo-texto").html(nuevoTipo);
    $this.find(".muestra-observacion-tipo").show();
    subeComentario(texto,observacion_id,alumno_id,observaciones_tipo_id,publica);
    $this.find(".div-buttons-actions").show();
    $this.find(".div-buttons-edit").hide();

  }
    
  function subeComentario(texto,observacion_id,alumno_id,observaciones_tipo_id,publica){
    var $this = $("#"+observacion_id);
    
    $.ajax({
      type: "POST",
      dataType:"json",
      data: {texto:texto, observacion_id:observacion_id,alumno_id:alumno_id,observaciones_tipo_id:observaciones_tipo_id,publica:publica},
      url: "<?php echo $this->createUrl("alumno/grabaObservacion"); ?>",
      success: function(data) {
        editando = false;
        if(data && observacion_id=="nuevo"){
          $("#observaciones-items").append(data.nuevo);
          $this.attr("id",data.id);
          $this.removeClass("nuevo");
          $("#observaciones-items").prepend($this);
          $this.find(".observacion-fecha-creacion").html(data.fecha);
        }else{
        }
      },
      error: function(data,status){
      }
    });

  }
    
  function cancela(obj){
    editando = false;
    $obj=$(obj);
    observacion_id = $obj.closest(".observacion").attr("id");
    var $this = $("#"+observacion_id);
    $this.find(".observacion-tipo").hide();
    $this.find(".muestra-observacion-tipo").show();
    $observacionTexto = $this.find(".observacion-texto");
    $observacionTexto.html(textoBackup);
    $observacionTexto.removeClass("editando");
    $observacionTexto.removeAttr("contenteditable");
    $this.find(".div-buttons-actions").show();
    $this.find(".div-buttons-edit").hide();
    $(".observacion-estado select").attr("disabled","disabled");

  }

</script>