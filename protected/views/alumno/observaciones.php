<?php
include("observaciones.css.php");
$baseUrl = Yii::app()->request->baseUrl;
$cs = Yii::app()->clientScript;
$cs->registerScriptFile($baseUrl . "/js/jquery.masonry.min.js", CClientScript::POS_END);
include("observaciones.js.php");
include("alumnoDialog.php");
?>
<form id="main-form" name="main" method="POST" action="#">
  <div id="div-head">
    <div id="div-socio">
      <input name="doc[socio]" id="socio" value="" placeholder="Alumno"/>
      <div id="muestraDialogo"></div>
      <div id="filtro-div" class="ui-widget-content ui-corner-all">
        <div id="observaciones_tipo_filtro_label">Tipo de observaciones:</div><?php
        $data = CHtml::listData(ObservacionesTipo::model()->findAll(), "id", "nombre");
        $opt = array("style" => "width:120px", "prompt" => "Todas");
        echo CHtml::dropDownList(
                "observaciones_tipo_filtro", "", $data, $opt);
        ?>
        <div id="observaciones-solo-propias">Solo propias: <input type="checkbox" id="solo_propias"/></div>
        <button id="filtro-button" onclick="filtra();
            return false">Filtra</button>
      </div>
    </div>
  </div>
  <div id="observaciones" class="ui-widget ui-widget-content ui-corner-all">
    <div class="tittle ui-widget ui-widget-content ui-corner-tl ui-corner-tr">Observaciones</div>
    <div id="observaciones-items">
    </div>
  </div>
</form>
<script type="text/javascript">
          $("#observaciones_tipo_filtro").chosen();
</script>



