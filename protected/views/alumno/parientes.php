<?php include("parientes.css.php"); ?>
<?php include("parientes_vm.js.php"); ?>
<div class="container">
  <!-- ko if:cargando() -->
  <div id="cargando">Cargando..</div>
  <!-- /ko -->
  <div class="alert alert-info">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Filtros</strong>

    <div class="alert-block">
      <div class="row">
        <div class="control-group col-xs-4">
          <select class="form-control"
                  data-bind="value: filtroNivel, options: niveles, optionsText:'nombre', optionsValue:'id', optionsCaption: 'Nivel (Opcional)...'">
          </select>
        </div>
        <div class="control-group col-xs-8">
          <input type="text" name="filtro" id="filtro"
                 placeholder="Filtro (* => todos)"
                 class="form-control" value=""
                 data-bind="textInput:filtro"
                 title="Filtro" required="required">
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <!-- ko if: alumnos().length -->
  <table class="table table-hover table-responsive table-stripped table-bordered margin_20">
    <thead>
	  <tr>
		<th>Alumno</th>
		<th>Encargado del pago</th>
		<th></th>
		<th></th>
		<th></th>
		<th>Visto</th>
		<th>Modif.</th>
	  </tr>
    </thead>
    <tbody data-bind="foreach:{data: alumnos, as:'a'}">
	  <tr data-bind="click:function(){$root.activar(a)}, template: editando() ? 'edit':'view'"></tr>
    </tbody>
  </table>
  <!-- /ko -->
</div>
<!--Template for View-->
<script type="text/html" id="view">
  <td>{{a.matricula}}</td>
  <td>{{a.nombre}}</td>
  <td><a href="#">{{a.encargado_pago().nombre}}</a></td>
  <td><a href="#">{{a.encargado_pago().tipo_documento_nombre}}</a></td>
  <td><a href="#">{{a.encargado_pago().numero_documento}}</a></td>
  <td><a href="#" data-bind="click: function(){toggleVisto(a, true)}"><i class='{{a.encargado_visto() ? "fa fa-check-square":"fa fa-square-o"}}'</a></td>
  <td><i class='{{a.encargado_cambiado() ? "fa fa-check-square":"fa fa-square-o"}}'</i></td>
</script>
<!--Template for edit-->
<script type="text/html" id="edit">
  <td data-bind="text:a.matricula"></td>
  <td data-bind="text:a.nombre"></td>
  <td>
    <select data-bind="value: $root.activo().encargado_pago_id,
                       valueUpdate: 'keyup',
                       options: $root.activo().parientes,
                       optionsText: 'nombre',
                       optionsValue: 'id',
                       optionsCaption: 'Encargado...'"></select>
  </td>
  <td>
    <select data-bind="value: a.encargado_pago().tipo_documento_id,
                       options: $root.tipoDocs,
                       optionsText: 'nombre',
                       optionsValue: 'id'"></select>
  </td>
  <td><input data-bind="textInput:a.encargado_pago().numero_documento"></td>
  <td><a href="#" data-bind="click: function(){toggleVisto(a, true)}"><i class='{{a.encargado_visto() ? "fa fa-check-square":"fa fa-square-o"}}'</a></td>
  <td><i class='{{a.encargado_cambiado() ? "fa fa-check-square":"fa fa-square-o"}}'</i></td>
<!--  <td><a data-bind="click: function(){toggleVisto(a)}, text: a.encargado_visto() ? 'Visto':'No visto'"></a></td>-->
</script>
<script>
  ko.applyBindings(vm);
</script>

