<script>
  /* global ko */
  /* global pub */

  var vm = (function () {
	var pub = {}, self = this;
	self.pub = pub;
	self.editando = ko.observable(false);
	pub.activo = ko.observable();
	self.getTipoDocs = function () {
	  return $.ajax({
		url: "<?php echo $this->createUrl('alumno/parientesGetTipoDocs'); ?>",
		success: function (data) {
		  ko.mapping.fromJSON(data, {
			create: function (o) {
			  return new TipoDoc(o.data);
			}
		  }, pub.tipoDocs);
		}
	  });
	};
	self.getNiveles = function () {
	  return $.ajax({
		url: "<?php echo $this->createUrl('alumno/parientesGetNiveles'); ?>",
		success: function (data) {
		  ko.mapping.fromJSON(data, {
			create: function (o) {
			  return new Nivel(o.data);
			}
		  }, pub.niveles);
		}
	  });
	};

	self.getAlumnosAjax = function () {
	  pub.cargando(true);
	  return $.ajax({
		data: {filtro: pub.filtro(), filtroNivel: pub.filtroNivel()},
		url: "<?php echo $this->createUrl('alumno/parientesGetAlumnos'); ?>",
		success: function (data) {
		  ko.mapping.fromJSON(data, {
			create: function (o) {
			  return new Alumno(o.data);
			}
		  }, pub.alumnos);
		}
	  });
	};

	self.carga = function () {
	  $.when(self.getAlumnosAjax()).done(init);
	};

	pub.cargando = ko.observable(false);
	pub.alumnos = ko.observableArray();
	pub.tipoDocs = ko.observableArray();
	pub.niveles = ko.observableArray();
	pub.templateName = ko.observable("view");
	pub.filtro = ko.observable("").extend({rateLimit: {timeout: 800, method: "notifyWhenChangesStop"}});
	pub.filtroNivel = ko.observable("");

	// Activa una persona nueva
	pub.activar = function (o) {
	  if (o !== pub.activo()) {
		if (pub.activo() && pub.activo().editando()) {
		  pub.activo().editando(false);
		}
		pub.activo(o);
		if (o.editando() === -1) {
		  o.editando(false);
		} else {
		  o.editando(true);
		}
		pub.templateName(self.editando() ? "edit" : "view");
	  }
	};


	pub.filtro.subscribe(function () {
	  if (pub.filtro().length > 2 || pub.filtro() === "*") {
		self.carga();
	  }
	});

	pub.filtroNivel.subscribe(function () {
	  if (pub.filtro().length > 2 || pub.filtro() === "*") {
		self.carga();
	  }
	});

	/*
	 Init
	 */
	function init() {
	  self.getTipoDocs();
	  self.getNiveles();
	  pub.cargando(false);
	}
	;

	/*
	 Run
	 */
	self.carga();
	return pub;

  })();


  function TipoDoc(o) {
	ko.mapping.fromJS(o, {}, this);
  }

  function Alumno(o) {
	var self = this;
	o.editando = false;
	o.encargado_pago = ko.observable();
	o.encargado_visto = Number(o.encargado_visto);
	o.encargado_cambiado = Number(o.encargado_cambiado);
	console.log(o.encargado_visto);
	ko.mapping.fromJS(o, {}, this);
	this.encargado_pago(this.parientes.find('id', this.encargado_pago_id()));
	if (!this.encargado_pago()) {
	  toastr.info("Falta el encargado de pago de " + o.nombre, null, {timeOut: "10000"});
	  this.encargado_pago = ko.observable({});
	  this.encargado_pago().nombre = ko.observable();
	  this.encargado_pago().tipo_documento_id = ko.observable();
	  this.encargado_pago().tipo_documento_nombre = ko.observable();
	}
	this.encargado_visto.subscribe(function (p) {
	  console.log(p, self);
	  grabaAlumnoParienteVisto(self, p);
	});
	this.toggleVisto = function (a, noActivar) {
	  //noActivar = noActivar
	  a.encargado_visto(!a.encargado_visto());
	  if (noActivar) {
		a.editando(-1);
	  }
	};
	this.visto = function (p) {
	  console.log(p);
	  return '<i class="fa fa-check-square"></i>';
	};

	this.encargado_pago_id.subscribe(function (p) {
	  self.encargado_pago(self.parientes.find('id', p));
	  grabaAlumno();
	});
	$.each(self.parientes(), function (i, o) {
	  $.each(o, function (i, fld) {
		fld.extend({rateLimit: {timeout: 1000, method: "notifyWhenChangesStop"}});
		fld.subscribe(function () {
		  grabaAlumno();
		});
	  });
	});
	this.encargado_pago().tipo_documento_id.extend({rateLimit: {timeout: 0, method: "notifyWhenChangesStop"}});
  }

  function Nivel(o) {
	var self = this;
	ko.mapping.fromJS(o, {}, this);
  }

  function grabaAlumno() {
	pub.activo().encargado_cambiado(true);
	pub.activo().encargado_visto(true);
	$.ajax({
	  url: "<?php echo $this->createUrl('alumno/parientesGrabaAlumno'); ?>",
	  data: {alumno_id: pub.activo().alumno_id(), pariente: ko.mapping.toJSON(pub.activo().encargado_pago)},
	  success: function (data) {
	  }
	});
  }
  function grabaAlumnoParienteVisto(a, visto) {
	$.ajax({
	  url: "<?php echo $this->createUrl('alumno/parientesGrabaEncargadoVisto'); ?>",
	  data: {alumno_id: a.alumno_id(), visto: visto},
	  success: function (data) {
	  }
	});
  }
</script>
