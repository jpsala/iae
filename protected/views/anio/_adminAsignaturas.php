<style>
	.asignatura_sub{float:right}
	.asignatura_sub:hover{font-weight: bold; color: red; font-style: oblique}
</style>
<form id="form-asignatura-nueva" method="POST" action="#">
	<div id="div-anio-edita">
		<label for="anio-nombre">Año</label>
		<input type="hidden" value="" id="anio-anio_id" name="anio_id"/>
		<input id="anio-nombre" type="text" name="nombre"/>
		<input id="button-graba-anio" type="button" value="Graba" onclick="return grabaAnio()"/>
		<input id="button-borra-anio" type="button" value="Borra" onclick="return borraAnio()"/>
	</div>  
	<div id="div-asignatura-edita">
		<label for="division-nombre">División</label>
		<input type="hidden" value="" id="division-division_id" name="division_id"/>
		<input id="division-nombre" type="text" name="nombre"/>
		<?php $orientaciones = Orientacion::model()->findAll("Anio_id=$anio_id"); ?>
		<?php if ($orientaciones): ?>
				<label id="label-for-orientacion" for="division-orientacion">Orientación</label>
				<?php echo CHtml::dropDownList("division-orientacion", null, CHtml::listData($orientaciones, "id", "nombre"), array("prompt" => "Tipo de orientación")); ?>
			<?php endif; ?>
		<input id="button-graba-division" type="button" value="Graba" onclick="return grabaDivision()"/>
<!--        <input id="button-nueva-division" type="button" value="Nueva" onclick="return nuevaDivision()"/>-->
	</div>
	<table class="ui-widget ui-widget-content" id="tabla-asignaturas">
		<tr class="ui-widget-header">
			<th>Nombre</th>
			<th>Abrev.</th>
			<th>Tipo</th>
			<th>Integradora</th>
			<th>Boletín</th>
			<th>&nbsp</th>
			<th>&nbsp</th>

		</tr>
		<?php $row = 0; /* @var $asignatura Asignatura */ ?>
		<?php $id = time(); ?>
		<?php foreach ($asignaturas as $asignatura): ?>
				<tr 
					class="tr-asignatura <?php echo ($row++ % 2) ? "tr-even" : ""; ?>" 
					asignatura_id="<?php echo $asignatura["id"]; ?>"
					>
					<td class="td-nombre">
						<?php echo $asignatura["nombre"]; ?>
						<?php if($asignatura["tipo"] == 3):?>
						<span class="asignatura_sub" asignatura_id="<?php echo $asignatura["id"];?>" onclick="return asignatura_sub_click(event)">+</span>
						<?php endif;?>
					</td>
					<td class="td-abrev"><?php echo $asignatura["abrev"]; ?></td>
					<td class="td-asignatura-tipo">
						<span><?php echo $asignatura["asignatura_tipo_nombre"]; ?></span>
						<input  type="hidden" value="<?php echo $asignatura["asignatura_tipo_id"]; ?>" 
										name="asignatura_tipo_id" class="asignatura_tipo_id"/>
					</td>
					<?php
					if ($asignatura["integradora"] == 1) {
						$integradora = "checked=checked";
					} else {
						$integradora = "";
					}
					?>
					<?php //$integradora = $asignatura["integradoras"] ? "checked=\"CHEQUED\"": ""; ?>
					<?php $id++; ?>
					<td class="td-division-integradora"><input id="<?php echo $id; ?>" type="checkbox" <?php echo $integradora; ?> onchange="return grabaIntegradora($(this))"/></td>
					<?php
					if ($asignatura["boletin"] == 1) {
						$boletin = "checked=checked";
					} else {
						$boletin = "";
					}
					?>
					<td class="td-division-boletin"><input class="<?php echo $id; ?>" type="checkbox" <?php echo $boletin; ?> onchange="return grabaBoletin($(this))"/></td>
					<?php // if ($asignatura["integradoras"] == -1): ?>
					<!--<script type="text/javascript">-->
					<!--$("#<?php // echo $id;  ?>").prop("indeterminate", true)-->
					<!--</script>-->
					<?php //endif; ?>
					<td class="td-division-asigna">&nbsp</td>
					<td class="td-borra" onclick="return borraAsignatura(<?php echo $asignatura["id"]; ?>)">Borra</td>
				</tr>
			<?php endforeach;
		?>
		<tr class ="tr-asignatura <?php echo ($row++ % 2) ? "tr-even" : ""; ?>" id="tr-asignatura-nueva">
			<td class="td-nombre"><input id="nombre-asignatura" name="nombre" onchange="return changeNomreAsignatura(this)"/></td>
			<td class="td-abrev"><input id="abrev-asignatura" name="abrev"/></td>
			<?php $listOptions = array(); ?>
			<td class="td-asignatura-tipo"><?php echo CHtml::dropDownList("Asignatura_Tipo_id", "", CHtml::listData(AsignaturaTipo::model()->findAll("Nivel_id=$nivel_id"), "id", "nombre"), array("prompt" => "Tipo de asignatura")); ?></td>
			<td>&nbsp</td>
			<td ><input type="button" id="graba-asignatura-nueva-button" value="Graba" onclick="return grabaAsignatura(this)"/></td>
		</tr>
	</table>
</form>

<script type="text/javascript">
	$("tr").draggable({
		appendTo: "body",
		helper: "clone"
	});
	var items = $("tr");
	items.droppable({
		activeClass: "ui-state-default",
		hoverClass: "ui-state-active",
		accept: ":not(.ui-sortable-helper)",
		drop: function(event, ui) {
			$('.placeholder').remove();
			row = ui.draggable;
			$(this).before(row);
			setTimeout(function() {
				grabaNuevoOrden();
			}, 400);
		}
	});
	function grabaNuevoOrden() {
		items = $("tr");
		var linkAsignaturas = [items.size()];
		var index = 0;
		items.each(
				function() {
					var asignatura_id = $(this).attr("asignatura_id");
					if (asignatura_id && Number(asignatura_id)) {
						linkAsignaturas[index++] = asignatura_id;
					}
				}
		);
		var asignaturas = linkAsignaturas.join(",");
		$.ajax({
			type: "POST",
			data: {asignaturas: asignaturas, division_id: division_id},
			url: "<?php echo $this->createUrl("anio/adminGrabaOrdenAsignaturas"); ?>",
			success: function(data) {
			},
			error: function(data, status) {
			}
		});
	}
</script>