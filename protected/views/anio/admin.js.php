
<script type="text/javascript">

	var nivel_id, anio_id, $tr_asignatura_nueva, division_id, orientacion_id;
	var $tr_asignatura = $('<tr asignatura_id="35" class="tr-asignatura ">\
            <td class="td-nombre">Matemática</td>                <td class="td-abrev">Matemática</td>\
            <td class="td-asignatura-tipo"><span>Normal</span><input type="hidden" class="asignatura_tipo_id"\
             name="asignatura_tipo_id" value="6"></td>                <td class="td-division-asigna">&nbsp;</td>\
            <td onclick="return borraAsignatura(35)" class="td-borra"></td>            </tr>');

	$(function() {
		init();
	});

	function init() {

		$("#limpia-orden-div").hide();

		$("button").button();

		$("#nivel-select").chosen().change(function() {
			changeNivel($(this));
		});

		$("#anio-select").chosen().change(function() {
			changeAnio($(this));
		});

		$("#division-select").chosen().change(function() {
			changeDivision($(this));
		})


		$(".clearinput").clearinput();

		$("#nivel-select_chzn").mousedown();

	}

	function changeNivel($this) {
		nivel_id = $this.val();
		anio_id = null;
		division_id = null;
		$("#limpia-orden-div").hide();
		$.ajax({
			type: "GET",
			//dataType: "json",
			data: {nivel_id: $this.val()},
			url: "<?php echo $this->createUrl("anio/options"); ?>",
			success: function(data) {
				$("#anio-select").html(data + "<option value=\"-1\">Nuevo Año</option>");
				$("#anio-select").trigger("liszt:updated");
				$("#nuevo-anio-button").show();
				$("#anio-select_chzn").mousedown();

			},
			error: function(data, status) {
			}
		});
		return false;
	}

	function changeAnio($this) {
		//        pedidoDeDatosVisible(false);
		division_id = null;
		anio_id = $this.val();
		$("#limpia-orden-div").hide();
		traeAsignaturas(function() {
			traeDivisiones();
		});
		return false;
	}

	function traeDivisiones() {
		$.ajax({
			type: "GET",
			//dataType: "json",
			data: {anio_id: anio_id},
			url: "<?php echo $this->createUrl("division/options"); ?>",
			success: function(data) {
				//orientacion_id = data.Orientacion.id;
				$("#division-select").html(data + "<option value=\"-1\">Nueva División</option><option value=\"-2\">Ninguna</option>");
				$("#div-asignatura-edita").hide();
				$("#div-anio-edita").show();
				$("#division-select").trigger("liszt:updated");
				$("#anio-nombre").val($("#anio-select option:selected").text());
				$("#anio-anio_id").val($("#anio-select").val());

				//                $("#division-select_chzn").mousedown();
			},
			error: function(data, status) {
			}
		});
		return false;
	}

	function changeDivision($this) {
		division_id = $this.val();
		$("#limpia-orden-div").hide();
		traeAsignaturas(function() {
			$("#div-anio-edita").hide();
			$("#div-asignatura-edita").show();
			if (division_id == -1) {
				nuevaDivision();
				$(" .td-division-asigna").html("");
			} else {
				//orientacion_id = $("#division-select option:selected").attr("orientacion_id");
				//console.log(orientacion_id);
				//$("#division-orientacion").val(orientacion_id);
				//$("#division-orientacion").trigger("liszt:updated");
				$("#division-nombre").val($("#division-select option:selected").text());
				$("#division-division_id").val($("#division-select").val());
				$("#division-orientacion").val($("#division-select option:selected").attr("orientacion_id"));
			}
			$("#division-nombre").focus();
			if (division_id == -1) {
				return;
			} else if (division_id == -2) {
				//$(".td-division-asigna").html("borra");
				return;
			}
			$.ajax({
				type: "GET",
				dataType: "json",
				data: {division_id: $this.val()},
				url: "<?php echo $this->createUrl("division/asignaturas"); ?>",
				success: function(data) {
					$("#limpia-orden-div").show();
					$(" .td-borra").html("");
					$(" .td-division-asigna").html("<a class=\"anchor-asigna\" onclick=asignaDesasignaAsignatura(event)>Asigna</a>");
					for (var key  in data) {
						$('tr[asignatura_id="' + data[key] + '"]').find(".anchor-asigna").html("Desasigna");
					}
				},
				error: function(data, status) {
				}
			});

		});
	}

	function limpiaOrden() {
		$.ajax({
			type: "GET",
			//      dataType:"json",
			data: {division_id: division_id},
			url: "<?php echo $this->createUrl("anio/limpiaOrdenDivision"); ?>",
			success: function(data) {
				traeAsignaturas();
			},
			error: function(data, status) {
			}
		}
		);
	}

	function nuevaDivision() {
		$("#division-nombre").val("");
		$("#division-division_id").val("");
	}

	function asignaDesasignaAsignatura(event) {
		var $rec = $(event.target).closest("tr");
		var asignatura_id = $rec.attr("asignatura_id");
		$(event.target).html($(event.target).html() == "Asigna" ? "Desasigna" : "Asigna");
		$.ajax({
			type: "GET",
			data: {asignatura_id: asignatura_id, division_id: division_id},
			url: "<?php echo $this->createUrl("anio/asignaDesasignaAsignatura"); ?>",
			success: function(data) {
			},
			error: function(data, status) {
			}
		});
		return false;
	}

	function grabaDivision() {

		data = "anio_id=" + anio_id + "&" + $("#div-asignatura-edita input ").serialize() + "&orientacion=" + $("#division-orientacion option:selected").val();
		$.ajax({
			type: "POST",
			//dataType: "json",
			data: data,
			url: "<?php echo $this->createUrl("anio/adminAsignaturasGrabaDivision"); ?>",
			success: function(data) {
				traeDivisiones();
			},
			error: function(data, status) {
			}
		});

	}

	function grabaAnio() {
		var data = "nivel_id=" + nivel_id + "&anio_id=" + anio_id + "&anio_nombre=" + $("#anio-nombre").val();

		$.ajax({
			type: "GET",
			//dataType: "json",
			data: data,
			url: "<?php echo $this->createUrl("anio/grabaAnio"); ?>",
			success: function(data) {

				$("#anio-select").val(data);
				$("#anio-select option:selected").text($("#anio-nombre").val());
				$("#anio-select option:selected").val(data);

				$("#anio-select").trigger("liszt:updated");
				//$("#anio-select_chzn span").html($("#anio-nombre").val());
				//$("#anio-select").val($("#anio-anio_id").val());
				anio_id = data;
			},
			error: function(data, status) {
			}
		});

	}

	function pedidoDeDatosVisible(visible) {
		//$("#pedido-de-datos").animate({"opacity": visible ? "show":"hide"},"slow");    
	}

	function traeAsignaturas($callBack) {
		$.ajax({
			type: "GET",
			//dataType: "json",
			data: {nivel_id: nivel_id, anio_id: anio_id, division_id: division_id},
			url: "<?php echo $this->createUrl("anio/adminAsignaturas"); ?>",
			success: function(data) {
				$("#asignaturas").html(data);
				$("#graba-asignatura-nueva-button").button();
				$(".tr-asignatura input:not(:hidden)").first().focus();
				$(".tr-asignatura:not('#tr-asignatura-nueva'):not('.asignatura_sub')").find("td:not('.asignatura_sub'):not('.td-borra'):not('.td-division-asigna')").click(function(event) {
					if (event.target.className !== "asignatura_sub") {
						clickAsignatura($(this).parent());
					}
				});
				if ($callBack) {
					$callBack();
				}
			},
			error: function(data, status) {
			}
		});
	}

	function clickAsignatura($this) {
		$rec = $("#tr-asignatura-nueva");
		$rec.find("#nombre-asignatura").val($this.find(".td-nombre").html());
		console.log($this.find(".td-nombre"));
		$rec.find("#abrev-asignatura").val($this.find(".td-abrev").html());
		$rec.attr("asignatura_id", $this.attr("asignatura_id"));
		$rec.find("#Asignatura_Tipo_id").val($this.find(".asignatura_tipo_id").val());
		$rec.find("input:not(:hidden)").first().focus();
	}

	function grabaAsignatura(input) {
		var $tr;
		$rec = $(input).closest("tr");
		asignatura_id = $rec.attr("asignatura_id");
		var data = "Anio_id=" + anio_id + "&" + "asignatura_id=" + asignatura_id + "&" + $rec.find("*").serialize();
		;
		$.ajax({
			type: "POST",
			dataType: "json",
			data: data,
			url: "<?php echo $this->createUrl("anio/adminAsignaturasGraba"); ?>",
			success: function(data) {
				if (data.error) {
					alert("Error en los datos, verifique")
				} else {
					if (!data.nuevo) {
						$tr = $('[asignatura_id="%id%"]'.replace("%id%", data.asignatura_id)).first();
						$rec.removeAttr("asignatura_id");
					} else {
						$tr = $tr_asignatura.clone();
						$tr.find(".td-borra").attr("onclick", "return borraAsignatura(" + data.asignatura_id + ")");
					}
					$tr.attr("asignatura_id", data.asignatura_id);
					$tr.find(".td-nombre").html($rec.find("#nombre-asignatura").val());
					$tr.find(".td-abrev").html($rec.find("#abrev-asignatura").val());
					$tr.find(".td-asignatura-tipo span").html($rec.find("#Asignatura_Tipo_id option:selected").text());
					$tr.find(".asignatura_tipo_id").val($rec.find("#Asignatura_Tipo_id").val());
					if (data.nuevo) {
						$rec.before($tr);
						$tr.find("td:not('.td-borra')").click(function() {
							clickAsignatura($(this).parent())
						});
					}
					$rec.find("#nombre-asignatura").val("");
					$rec.find("#abrev-asignatura").val("");
					$rec.find("#Asignatura_Tipo_id").val("");
					$rec.find("input:not(:hidden)").first().focus();
				}
			},
			error: function(data, status) {
			}
		});

	}

	function cancela() {
		pedidoDeDatosVisible(true);
		limpiaTodo();
		return false;
	}

	function changeNomreAsignatura($this) {
		if (!$("#abrev-asignatura").val()) {
			$("#abrev-asignatura").val($($this).val());
		}
	}

	function borraAsignatura(asignatura_id) {
		$.ajax({
			type: "GET",
			//dataType: "json",
			data: {asignatura_id: asignatura_id},
			url: "<?php echo $this->createUrl("anio/adminAsignaturasBorra"); ?>",
			success: function(data) {
				$('[asignatura_id="%id%"]'.replace("%id%", asignatura_id)).remove();
			},
			error: function(data, status) {
			}
		});

		return false;
	}

	function limpiaTodo() {

	}

	function nuevoAnio() {
		$.ajax({
			type: "POST",
//      dataType:"json",
			data: {
			},
			url: "<?php echo $this->createUrl(""); ?>",
			success: function(data) {
			},
			error: function(data, status) {
			}
		});
	}

	function grabaIntegradora($this) {
		$.ajax({
			type: "GET",
			//      dataType:"json",
			data: {anio_id: anio_id, division_id: division_id, asignatura_id: $this.closest("tr").attr("asignatura_id"), integradora: $this.prop("checked")},
			url: "<?php echo $this->createUrl("anio/grabaIntegradora"); ?>",
			success: function(data) {
				console.log(data);
			},
			error: function(data, status) {
			}
		}
		);
		;
	}

	function grabaBoletin($this) {
		$.ajax({
			type: "GET",
			//      dataType:"json",
			data: {anio_id: anio_id, division_id: division_id, asignatura_id: $this.closest("tr").attr("asignatura_id"), boletin: $this.prop("checked")},
			url: "<?php echo $this->createUrl("anio/grabaBoletin"); ?>",
			success: function(data) {
				console.log(data);
			},
			error: function(data, status) {
			}
		}
		);
		;
	}

	function asignatura_sub_click(event) {
		event.preventDefault();
		var url = "<?php echo $this->createUrl("/anio/asignaturaSubForm");?>";
		var asignatura_id = $(event.target).attr("asignatura_id");
		window.open(url + "&asignatura_sub_id=" + asignatura_id);

	}
</script>
