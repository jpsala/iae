<?php include("admin.css.php"); ?>
<?php include("admin.js.php"); ?>
<div id="pedido-de-datos">
    <div class="row">
        <select id="nivel-select" data-placeholder="Nivel" class="chzn-select">
            <?php
            $x = array('prompt' => '');
            echo CHtml::listOptions(null, CHtml::listData(Nivel::model()->findAll(), 'id', 'nombre'), $x)
            ?>
        </select>
    </div>
    <div class="row">
        <select id="anio-select" data-placeholder="Año" class="chzn-select">
            <?php
            $x = array('prompt' => '');
            echo CHtml::listOptions(null, CHtml::listData(Anio::model()->findAll("id=-1"), 'id', 'nombre'), $x)
            ?>
        </select>
    </div>
    <div class="row">
        <select id="division-select" data-placeholder="División" class="chzn-select">
            <?php $thmlOptions = array("id" => "division-select", "class" => "chzn-select", "prompt" => "División"); ?>
            <?php echo CHtml::listOptions(null, CHtml::listData(Division::model()->findAll("id=-1"), "id", "nombre"), $thmlOptions); ?>
        </select>
    </div>
</div>
<div id="limpia-orden-div">
    <span onclick="limpiaOrden(); return false;">Limpia el órden</span>
</div>
<form id="form-division" method="POST" action ="#" name="form-division">

</form>
<div id="asignaturas">
</div>

<!--    <div class="buttons-div">
        <button type="button" name="cancela" id="cancela" onclick="return cancela()">Cancela</button>
        <button type="button" name="ok" id="ok">Graba</button>
    </div>-->
<!--</div>-->

