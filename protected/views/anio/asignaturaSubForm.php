<style>
	#asignatura-select-div{line-height: 20px}
	#asignatura_id{width:180px}
	#label-asignatura{vertical-align: super;}
	#asignatura_id_chzn{vertical-align: text-bottom;}
</style>
<?php
	$asignatura = Asignatura::model()->findByPk($asignatura_sub_id);
	$asignaturas = Asignatura::model()->with("asignaturaTipo")->findAll("t.Anio_id = $asignatura->Anio_id and asignaturaTipo.tipo = 2");
	$opt = array("empty" => array(-1 => "Seleccione una materia"));
	$listData = CHtml::listData($asignaturas, "id", "nombre");
?>
<div id="asignatura-select-div">
	<span id="label-asignatura"><?php echo $asignatura->nombre; ?>&nbsp;Pertenece a&nbsp;</span>
	<?php echo CHtml::dropDownList("asignatura_id", $asignatura->asignatura_id, $listData, $opt); ?>
</div>

<script  type="text/javascript">
	$("#asignatura_id").chosen().change(function(a) {
		console.log($(a.target).val());
	});
</script>