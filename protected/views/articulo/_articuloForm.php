<?php
/* @var $this PostController */
/* @var $model Destino */
?>

<div class="form">
    <div class="row">
        <select id="Articulo" data-placeholder="Puede seleccionar un artículo para modificar" class="chzn-select">
            <?php
            $x = array('prompt' => '');
            echo CHtml::listOptions($model->id, CHtml::listData(Articulo::model()->findAll(), 'id', 'nombre'), $x)
            ?>
        </select>
    </div>
    <form id="carga" method="post">
        <input id="id" type="hidden" name="id" value="<?php echo $model->id; ?>"/>
        <div class="row">
        <label for="Articulotipo">Tipo de artículo</label>    
          <select id="Articulotipo" name="articulo_tipo_id" data-placeholder="Tipo de Artículo" class="chzn-select">
           <?php
            $y = array('prompt' => '');
             echo CHtml::listOptions($model->articulo_tipo_id, CHtml::listData(ArticuloTipo::model()->findAll(), 'id', 'nombre'), $y)
            ?>
          </select>
        </div>
        <div class="row">
            <label id ="lblnombre" for="nombre">Nombre</label>
            <input class="clearinput" id="nombre" type="text" name="nombre" value="<?php echo CHtml::encode($model->nombre); ?>"/>
        </div>
        <div class="row">
            <label for="precio_neto">Importe</label>
            <input class="clearinput" id="valor" type="text" name="precio_neto" value="<?php echo CHtml::encode($model->precio_neto); ?>"/>
        </div>
       
        <div id="divorden" class="row">
            <label id ="lblorden" for="orden">Orden</label>
            <input class="clearinput" id="orden" type="text" name="orden" value="<?php echo CHtml::encode($model->orden); ?>"/>
        </div>

        <div class="botones">
            <input type="submit" name="submit" value="Grabar" id="submit"/>
            <?php if (!$model->getIsNewRecord()): ?>
                <input type="button" name="borrar" value="Borra" id="borra"/>
            <?php endif; ?>
            <input type="button" onclick="return refresh();" name="nuevo" value="Nuevo" id="nuevo"/>
        </div>
    </form>
    <div id="errores">
    </div>
</div>
