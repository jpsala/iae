<?php
/* @var $this PostController */
/* @var $model Destino */
?>
<div class="form">
    <div class="row">
        <select id="Articulotipo" data-placeholder="Puede seleccionar un tipo de artículo para modificar" class="chzn-select">
            <?php
            $x = array('prompt' => '');
            echo CHtml::listOptions($model->id, CHtml::listData(ArticuloTipo::model()->findAll(), 'id', 'nombre'), $x)
            ?>
        </select>
    </div>
    <form id="carga" method="post">
        <input id="id" type="hidden" name="id" value="<?php echo $model->id; ?>"/>
        <div class="row">
            <label for="nombre">Nombre</label>
            <input class="clearinput" id="nombre" type="text" name="nombre" value="<?php echo CHtml::encode($model->nombre); ?>"/>
        </div>
        
        <div class="row"> <?php
           echo CHtml::radioButtonList('tipo',$model->novedad_tipo,array(null=>'Regular','1'=>'Novedad','2'=>'Novedad Interna'), array( 'separator' => " ",'labelOptions'=>array('style'=>'display:inline')))?>; 
        </div>


        <div class="row">
            <label for="maneja_orden">¿ Define Orden ?</label>
            <?php echo CHtml::checkBox("maneja_orden", $model->maneja_orden);?>
        </div>
        <div class="row">
            <label for="concepto">¿ Maneja Stock ?</label>
            <?php echo CHtml::checkBox("maneja_stock", $model->maneja_stock);?>
        </div>


           

        <div class="botones">
            <input type="submit" name="submit" value="Grabar" id="submit"/>
            <?php if (!$model->getIsNewRecord()): ?>
                <input type="button" name="borrar" value="Borra" id="borra"/>
            <?php endif; ?>
            <input type="button" onclick="return refresh()" name="button" value="Nuevo" id="button"/>
        </div>
    </form>
    <div id="errores">
    </div>
</div>
