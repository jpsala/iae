<style  type="text/css">
    label{float: left; width: 160px;line-height: 2em;    margin-right: 14px; text-align: right; padding-top: 4px;}
    #errores{border: 1px #09f solid; color:#09f; background-color: #f2dada; display: none; padding: 10px; margin: 0 auto; width: 500px; text-align: center}
    form{margin: 10px 0px 10px; border: 1px #75C2F8 solid; padding: 19px 20px 11px 27px;  border-radius: 8px; border-shadow: 5px}
    .botones{text-align: right} 
    #Articulotipo, #nombre, #Articulo {width: 350px}
    #orden{width: 50px}
    input[type=text] {
        color: #444;
        letter-spacing: 1px;
        word-spacing: 2px; 
        padding: 6px; 
    }   
    .ui-widget { font-size: 12px; }
</style>

<?php $this->breadcrumbs = array(Articulo::label(2),); ?>

<div id="_form">
    <?php include "_articuloForm.php"; ?>
</div>

<script type="text/javascript">

    init();


    function init() {

        $("button, #nuevo, #submit, #borra").button();
        $(".chzn-select").chosen();
        $(".clearinput").clearinput();
        $("#nombre").focus();

        $("#Articulotipo").chosen().change(function() {
            $id_art = $(this).find(":selected").attr("value");
            recupera_orden($id_art);
        });


        $("#Articulo").chosen().change(function() {
            ajax($(this).val());

        });

        $('#borra').click(function() {
            if (!confirm("¿Borra este Artículo?"))
                return;

            $.ajax({
                type: "GET",
                data: {'id': $('#id').val()},
                url: "<?php echo $this->createUrl('articulo/articuloBorra'); ?>",
                success: function(data) {
                    refresh();
                }
            });
        })

        $('form').submit(function() {
        console.log(0);
            $.ajax({
                type: "POST",
                data: $('form').serialize(),
                dataType: 'json',
                url: "<?php echo $this->createUrl('articulo/articuloGraba'); ?>",
                success: function(data) {
                        console.log(1);
                    if (data) {
                        console.log(2);
                        $('#errores').html("");
                        for (var i in data)
                        {
                            $('#errores').append(data[i][0] + "<br/>");
                        }
                        $('#errores').show();
                    } else {
                        console.log(3);
                        refresh();
                        //$("#nuevo").click();
                    }
                }
            });
            return false;
        });

    }

    function refresh() {
        console.log(1);
        window.location = window.location;
        return false;
    }
    function recupera_orden(id) {
        $.ajax({
            type: "GET",
            data: {"id": id},
            url: "<?php echo $this->createUrl("articulo/articuloManejaOrden"); ?>",
            success: function(data) {
                if (data) {
                    if (data == 0) {
                        $('#divorden').hide('slow');
                    } else
                    {
                        $('#divorden').show('slow');
                    }
                } else {
                    alert("vacio");
                }
                ;
            },
            error: function(data, status) {
            }

        });

    }
    function ajax(id) {
        $.ajax({
            type: "GET",
            data: {"id": id},
            url: "<?php echo $this->createUrl("articulo/articuloForm"); ?>",
            success: function(data) {
                $("#_form").html(data);
                $id_art = $("#Articulotipo").find(":selected").attr("value");
                recupera_orden($id_art);
            },
            error: function(data, status) {
            },
            complete: function() {
                init();
            }
        });
    }
</script>

