<style>
  table tr td:nth-child(1){width:80px}
  table tr td:nth-child(2){width:100px}

</style>
<div class="container top20">
  <!-- Display filename inside the button instead of its label -->


  <form action="<?php echo $this->createUrl("asr/subeArchivo"); ?>" method="post" enctype="multipart/form-data">
  <!--<input type="file" data-filename-placement="inside">-->
	<div style="position:relative;">
	  <a class='btn btn-primary' href='javascript:;'>
		Seleccionar archivo de sueldos...
		<input data-bind="value: archivo " name="archivo_sueldos" type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file_source" size="40"  onchange='$("#upload-file-info").html($(this).val());'>
	  </a>
	  &nbsp;
	  <span class='label label-info' id="upload-file-info"></span>
	</div>

	<input class="input input-sm" type="submit" value="Subir archivo" name="submit">
	<?php
	$dbh = new PDO('mysql:host=127.0.0.1;dbname=iae-nuevo', 'root', 'lani0363', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$h = $dbh->query("select * from sueldos_campos");
	$a = $h->fetchAll();
	echo "<table class='table table-striped table-condensed'>";
	foreach ($a as $value) {
	  $nombre_viejo = $value["nombre_viejo"];
	  $nombre_nuevo = $value["nombre_nuevo"];
	  $id = $value["id"];
	  echo "<tr>";
	  echo "<td>$nombre_viejo</td>";
	  echo "<td><input name=\"ids[$nombre_viejo]\" value=\"$nombre_nuevo\"/></td>";
	  echo "<td></td>";
	  echo "</tr>";
	}
	echo "</table>";
	?>
  </form>
</div>
<script>
  var vm = (function () {
	var vm = {};
	vm.archivo = ko.observable("");
	vm.muestraSubmit = ko.observable(false);
	return vm;
  })();
</script>
