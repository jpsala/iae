<style>
    .archivo-td, .archivo-th { width: 170px }
    .show * { display: none }
    #archivos_table td { cursor: pointer }
    #archivos_table tr td > table { margin: 10px }
    .importe { text-align: right }
    .center { text-align: center }
    .spinner-recibos{display: none}
</style>
<div class="container">
    <table id="archivos_table" class="table table-bordered table-responsive table-hover">
        <tr>
            <th class="archivo-th">Archivo</th>
            <th>Tipo</th>
            <th>Fecha</th>
            <th>Cantidad</th>
            <th>Total</th>
            <th>Banco</th>
            <th></th>
        </tr>
        <?php foreach ($files as $file): ?>
            <tr class="archivo" banco="<?php echo $file['summary']['banco'];?>" archivo="<?php echo $file['nombre'];?>" id="<?php echo $file['id']; ?>">
                <td class="archivo-td"><?php echo $file['nombre']; ?></td>
                <td><?php echo $file['tipo']; ?></td>
                <td><?php echo $file['summary']['fecha']; ?></td>
                <td><?php echo $file['summary']['cant']; ?></td>
                <td><?php echo $file['summary']['total']; ?></td>
                <td><?php echo $file['summary']['banco']; ?></td>
                <td class="center">
                    <button id="generaRecibos" class="generaRecibos btn btn-info">Generar Recibos</button>
                    <img class="spinner-recibos" src="images/spinner.gif"/>
                </td>
            </tr>
            <tr class="show" id="archivo_<?php echo $file['id']; ?>">
                <td colspan="6">
                    <table class="table table-bordered">
                        <tr>
                            <th>ID</th>
                            <th>Matrícula</th>
                            <th>Alumno</th>
                            <th>Importe</th>
                            <th>Motivo del rechazo</th>
                            <th>Detalle</th>
                            <th>Excluir</th>
                        </tr>
                        <?php foreach ($file['recs'] as $rec): ?>
                            <tr>
                                <td><?php echo $rec['id_archivo']; ?></td>
                                <td><?php echo $rec['matricula']; ?></td>
                                <td><?php echo $rec['nombre']; ?></td>
                                <td class="importe"><?php echo $rec['importe']; ?></td>
                                <td><?php echo $rec['motivo_rechazo']; ?></td>
                                <td><?php echo $rec['detalle']; ?></td>
                                <td><input type="checkbox" class="excluir" doc_id="<?php echo $rec['id_archivo'];?>"/></td>
                            </tr>
                        <?php endforeach; ?>
                        <tr>
                            <th>Total ok</th>
                            <th colspan="2"></th>
                            <th class="importe"><?php echo $file['totalRecsOk']; ?></th>
                        </tr>
                        <tr>
                            <th>Total rechazado</th>
                            <th colspan="2"></th>
                            <th class="importe"><?php echo $file['totalRecsRechazados']; ?></th>
                        </tr>
                        <tr>
                            <th>Total</th>
                            <th colspan="2"></th>
                            <th class="importe"><?php echo $file['totalRecs']; ?></th>
                        </tr>
                    </table>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>
<script>

    $('.generaRecibos').on('click', function (e) {
        var $tr = $(this).closest('tr');
        var archivo = $tr.attr('archivo');
        var banco = $tr.attr('banco');
        var $excluir = $('.excluir');
        var excluir = [];
        $excluir.each(function(i,o){
            if($(o).is(':checked')) {
                excluir.push({id: $(o).attr('doc_id')})
            }
        });
        $tr.find('.spinner-recibos').show();
        $(this).text('Generando...');
        $.ajax({
            url:"<?php echo $this->createUrl('/bancos/generarRecibos');?>",
            data:{archivo:archivo,excluir: JSON.stringify(excluir), banco: banco}
        }).then(function(data){
            $('.generaRecibos').text('Generar Recibos');
            $tr.find('.spinner-recibos').hide();
            if(data !== '[]'){
                alert('errores: ' + data);
            }else{
                alert('Los recibos fueron generados');
                window.location.reload();
            }
        });
    });
    $('.archivo').on('click', function (e) {
        var type= e.target.type;
        if(type !== 'submit') {
            var id = $(this).attr('id');
            $('#archivo_' + id).toggleClass('show');
        }
        return false;
    })
</script>
