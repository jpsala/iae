<script src="js/jquery.ajaxmock.min.js"></script>
<style>
	#ajaxlog{padding: 15px;border-radius: 5px}
</style>

<div class="container">
	<div class="row bs-example  col-xs-1">
		<p id="ajaxlog" class="bg-info">Antes</p>
	</div>	
</div>
<script  type="text/javascript">
	initAjaxMock();
	$.get("<?php echo $this->createUrl("/menu/index"); ?>")
		.done(function(a, b) {
			$("#ajaxlog").removeClass("bg-info").addClass("bg-success").html("Listo!!");
		});

	function initAjaxMock() {
		jQuery.ajaxMock.register("<?php echo $this->createUrl("/menu/index"); ?>", {
			responseText: 'responseFoo',
			statusCode: 200,
			status: 'OK',
			type: 'GET', // optional, takes a String as http request method default: 'GET'
			delay: 2000 // optional
		});
	}

</script>	