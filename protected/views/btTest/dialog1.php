<!-- Button to trigger modal --> 
<div class="container">
	<a href="#myModal" role="button" class="btn btn-primary pull-6" data-toggle="modal">Open Contact Form</a>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Modal title</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" name="commentform" method="post" action="send_form_email.php">
					<div class="form-group">
						<label class="control-label col-md-4" for="first_name">First Name</label>
						<div class="col-md-6">
							<input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-4" for="last_name">Last Name</label>
						<div class="col-md-6">
							<input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-4" for="email">Email Address</label>
						<div class="col-md-6 input-group">
							<span class="input-group-addon">@</span>
							<input type="email" class="form-control" id="email" name="email" placeholder="Email Address"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-4" for="comment">Question or Comment</label>
						<div class="col-md-6">
							<textarea rows="6" class="form-control" id="comments" name="comments" placeholder="Your question or comment here"></textarea>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" data-dismiss="modal" class="btn">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>
	</div>
</div><!-- End of Modal -->