<div class="container">
	<div class="row">
		<form class="form col-xs-4" data-bind="submit:submit">
			<div class="form group input-group" data-bind="css:{'has-error':nombreCompletoError}">
				<span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
				<input autofocus="true" required="true" 
							 data-bind="
								value:nombreCompleto,
								css:{'error':nombreCompletoError()}
							 " class="form-control" type="text" placeholder="Apellido, Nombre">
			</div>
			<span data-bind="css:{'text-danger':nombreCompletoError}, visible:nombreCompletoError()">
				Ingrese Apellido, Nombre
			</span>

			<div class="input-group col-xs-12 top5">
				<span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
				<input data-bind="value:nombre, valueUpdate:'afterkeydown'" class="form-control" type="text" placeholder="Nombre">
				<span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
				<input autofocus="" data-bind="value:apellido, valueUpdate:'afterkeydown'" class="form-control" type="text" placeholder="Apellido">
			</div>
			<input 
				type="submit" 
				class="btn btn-primary btn-default col-xs-4 span8 pull-right top17"
				data-bind="enable:! error()" />
		</form>
	</div>
</div>

<script type="text/javascript">
	DataViewModel = function() {
		self = this;
		self.apellido = ko.observable().extend({logChange: "first name"});
		self.nombre = ko.observable("");
		self.nombreCompletoError = ko.observable(false);
		self.nombreCompleto = ko.computed({
			read: function() {
				self.nombre(self.nombre().replace(",", "").trim());
				self.apellido(self.apellido().replace(",", "").trim());
				console.log(self.nombre(), self.apellido());
				var sep = (self.nombre() || self.apellido()) ? ", " : "";
				self.nombreCompletoError((self.nombre() || self.apellido()) && !(self.nombre() && self.apellido()) ? true : false);
				console.log("hola che".replace(/^[a-z]/, function(m){ return m.toUpperCase(); }));
				var nombreCompleto = ucWords(self.apellido() + sep + self.nombre());
				console.log(nombreCompleto);
				return nombreCompleto;
			},
			write:function(value) {
				var lastSpacePos = value.lastIndexOf(",");
				self.nombre(self.nombre().replace(",",""));
				self.apellido(self.apellido().replace(",",""));
				if (lastSpacePos > 0) { // Ignore values with no space character
					var ape=value.substring(0, lastSpacePos).trim();
					var nom=value.substring(lastSpacePos + 1).trim();
//					 nom = nom === "," ? "":nom;
					if(self.nombre()===nom && self.apellido()===ape){
						self.nombre("");
					}
					self.nombre(nom);
					self.apellido(ape);
				}else{
					self.apellido(value);
					self.nombre("");
				}
			}
		});
		self.error = ko.computed(function(){
			error = self.nombreCompletoError() || (!self.nombreCompleto());
			return error;
		}).extend({logChange:"Error:"});
		//self.nombre.extend({logChange:"Log:"});
		self.submit = function(a){
			if(self.error()){
//				console.log("error");
			}
//			console.log(a);
			return false;
		};
		
	};
	
	ko.extenders.logChange = function(target, option) {
		target.subscribe(function(newValue) {
			//console.log(option + ": " + newValue);
		});
		return target;
	};
	
	ko.applyBindings(new DataViewModel());
	
	function ucWords(str){
	return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}
	
</script>