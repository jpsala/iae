<link rel="stylesheet" type="text/css" href="js/tablesorter/themes/blue/style.css"/>
<script src="js/tablesorter/jquery.tablesorter.js"></script>
<script type="text/javascript">



    jQuery(document).ready(function() {
        onReady();
    });

    function onReady() {

     $(".fecha").datepicker();
     $(":button").button();

    }


    function submitMov() {
        $.ajax({
            type: "GET",
            dataType: "json",
            data: {fecha_desde:$("#fecha_desde").val(), fecha_hasta:$("#fecha_hasta").val(), en_cartera:$('#en-cartera:checked').val(),entregados:$('#entregados:checked').val(),agrupados:$('#agrupados:checked').val()},
            url: "<?php echo $this->createUrl("cartera/traeDatos"); ?>",
            success: function(data) {
                $("#movimientos-table").html(data.movimientos);
                $("#movimientos-table table").tablesorter({headers: {
                        // assign the secound column (we start counting zero) 
                        44: {
                            // disable it by setting the property sorter to false 
                            sorter: false
                        }
                    }});

//$("#fecha").val(data.fecha);
            },
            error: function(data, status) {
            }
        }
        );
    }
    


    
</script>
