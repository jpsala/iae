<?php $baseUrl = Yii::app()->request->baseUrl; ?>
<?php include("index.js.php"); ?>
<?php include("index.css.php"); ?>

<?php //$doc_id = 84963;     ?>
<?php $doc_id = null; ?>
<form id="main-form" name="main" method="POST" action="#">
    <div id="izq">
        <div class="titulo">
            <span>Cartera de Cheques de Terceros</span>
        </div>
        
            <div class="inline-row">
                <label for="fecha-desde">Desde Fecha : </label>
                <input class="fecha" type="text" id="fecha_desde" value="<?php echo date("d/m/Y", time() - (3600 * 24 * 30)); ?>"/>
            </div>
            
            <div class="inline-row">
                <label for="fecha-hasta">Hasta Fecha : </label>
                <input class="fecha" type="text" id="fecha_hasta" value="<?php echo date("d/m/Y", time()); ?>"/>
            </div>

            <div class="row">
                <label for="en-cartera">En cartera : </label>
                <input  type="checkbox" id="en-cartera" name= "en-cartera" value="checked" checked />
            </div>
            <div class="row">
                <label for="entregados">Entregados : </label>
                <input  type="checkbox" id="entregados" name= "entregados" value="checked" checked />
            </div>
            <div class="row">
                <label for="agrupados">Agrupados : </label>
                <input  type="checkbox" id="agrupados" name= "agrupados" value="checked" checked />
            </div>
    
                <div class="ui-widget-content ui-corner-all">
                    <div class="titulo">Cheques de Terceros</div>
                    <div id="movimientos-table">
                        <?php include("movimientos.php")?>
                    </div>
                </div>
            
        </div>
    </div>

    <div id="botones">
        <div id="botones">
            <input type="button" id="cancel" value="Cancela" onclick="return limpiaTodo();"/>
            <input type="button" id="ok" value="Aceptar" onclick="return submitMov();"/>
        </div>
    </div>
</form>
</div>