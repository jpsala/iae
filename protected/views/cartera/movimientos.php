<table class="tablesorter" id="movimientos-table" class="tabla">
    <thead>
        <tr>
            <th class="movimientos-fecha">Número</th>
            <th class="movimientos-fecha">Fecha</th>
             <th class="movimientos-detalle">banco</th>
            <th class="movimientos-debe">Importe</th>
            <th class="movimientos-obs">Entregado a </th>
            <th class="movimientos-obs">Origen</th>
        </tr>
    </thead>
    <tbody>
        <?php if (isset($movimientos)): ?>
            <?php foreach ($movimientos as $mov): ?>
                <tr>
                    <td><?php echo $mov["numero"]; ?></td>
                    <td><?php echo date("d/m/Y", mystrtotime($mov["fecha"])); ?></td>
                    <td><?php echo  $mov["nombre"]; ?></td>
                    <td class="debe"><?php echo $mov["importe"]; ?></td>
                    <td><?php echo  $mov["chq_entregado_a"]; ?></td>
                    <td><?php echo $mov["chq_origen"]; ?></td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
</table>
<?php if (isset($total_en_cartera)): ?>
<?php echo "Total de cheques en cartera $total_en_cartera"; ?>
<?php endif; ?>