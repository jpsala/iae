<?php
/* @var $this PostController */
/* @var $model Destino */
?>
<div class="form">
    <div class="row">
        <select id="Chequera" data-placeholder="Puede seleccionar una Chequera para modificar" class="chzn-select">
            <?php
            $x = array('prompt' => '');
            echo CHtml::listOptions($model->id, Chequera::listData(), $x)
            ?>
        </select>
    </div>
    <form id="carga" method="post">
        <input id="id" type="hidden" name="id" value="<?php echo $model->id; ?>"/>
        <div class="row">
            <label for="numero">Número:</label>
            <input  id="numero" type="label" name="numero" value="<?php echo CHtml::encode($model->numero); ?>" disabled/>
        </div>
        <div class="row">
        <label for="destino_id">Cuenta Destino</label>    
        <select id="destino_id" name="destino_id" data-placeholder="Seleccione una Cuenta Destino" class="chzn-select">
            <?php
            $x = array('prompt' => '');
            $tipo = '2';
            echo CHtml::listOptions($model->destino_id, CHtml::listData(Destino::model()->findAll("tipo = $tipo"), 'id', 'numero_cuenta'), $x)
            ?>
        </select>
        </div>
        <div class="row">
            <label for="p_numero">Primer Número</label>
            <input class="clearinput" id="p_numero" type="text" name="p_numero" value="<?php echo CHtml::encode($model->primer_numero); ?>"/>
        </div>
        <div class="row">
            <label for="u_numero">Ultimo Número</label>
            <input class="clearinput" id="u_numero" type="text" name="u_numero" value="<?php echo CHtml::encode($model->ultimo_numero); ?>"/>
        </div>
        <div class="row">
            <label for="s_numero">Siguiente Número</label>
            <input class="clearinput" id="s_numero" type="text" name="s_numero" value="<?php echo CHtml::encode($model->siguiente_numero); ?>"/>
        </div>
        <div class="row">
            <label for="descripcion">Descripción</label>
            <input class="clearinput" id="descripcion" type="text" name="descripcion" value="<?php echo CHtml::encode($model->descripcion); ?>"/>
        </div>
        <div class="row">
            <label for="diferido">¿ Diferido ?</label>
            <?php echo CHtml::checkBox("diferido", $model->diferido);?>
        </div>
        <div class="botones">
            <input type="submit" name="submit" value="Grabar" id="submit"/>
            <?php if (!$model->getIsNewRecord()): ?>
                <input type="button" name="borrar" value="Borra" id="borra"/>
            <?php endif; ?>
            <input type="button" onclick="return refresh()" name="button" value="Nuevo" id="button"/>
        </div>
    </form>
    <div id="errores">
    </div>
</div>
