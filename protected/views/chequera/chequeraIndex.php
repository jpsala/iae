<style type="text/css">
    label{float: left; width: 160px;line-height: 2em;    margin-right: 14px; text-align: right; padding-top: 4px;}
    #errores{border: 1px #09f solid; color:#09f; background-color: #f2dada; display: none; padding: 10px; margin: 0 auto; width: 500px; text-align: center}
    form{margin: 10px 0px 10px; border: 1px #75C2F8 solid; padding: 19px 20px 11px 27px;  border-radius: 8px; border-shadow: 5px}
    .botones{text-align: right} 
    #numero{width: 50px}
    #Chequera{width: 250px}
    #destino_id{width: 200px}
    #p_numero, #u_numero, #s_numero{width: 100px; text-align:right}
    #descripcion{width: 200px}
    input[type=text] {
        color: #444;
        letter-spacing: 1px;
        word-spacing: 2px; 
        padding: 6px; 
    }   
    .ui-widget { font-size: 12px; }
</style>

<?php $this->breadcrumbs = array(Chequera::label(2),); ?>

<div id="_form">
    <?php include "_chequeraForm.php"; ?>
</div>

<script type="text/javascript">
    
    init();
    

    function init(){
        $("button, #submit, #borra").button();
        $(".chzn-select").chosen();
        $(".clearinput").clearinput();
        $("#numero").focus();
        
        $("#Chequera").chosen().change(function(){
            ajax($(this).val());
        });
        
        $('#borra').click(function(){
            if(!confirm("Borra esta Chequera??"))
                return;
            
            $.ajax({
                type: "GET",
                data: {'id':$('#id').val()},
                url: "<?php echo $this->createUrl('Chequera/chequeraBorra'); ?>",
                success: function(data) {
                    refresh();
                }
            });  
        })
        
        $('form').submit(function(){
            $.ajax({
                type: "POST",
                data: $('form').serialize(),
                dataType:'json',
                url: "<?php echo $this->createUrl('chequera/chequeraGraba'); ?>",
                success: function(data) {
                    if(data){
                        if (data['obs'])
                        {
                         alert(data['obs']);
                        }
                        else
                            {
                        $('#errores').html("");
                        for(var i in data['err'])
                        {
                            $('#errores').append(data['err'][i][0]+"<br/>");
                        }
                        $('#errores').show();
                    }
                        
                    }else{
                        
                        refresh();
                    }
                    window.location = window.location;
                }
            });
            return false;
        });

    }
    function refresh(){
        ajax(null);
        return false;
    }
    function ajax(id){
        $.ajax({
            type: "GET",
            data: {"id":id},
            url: "<?php echo $this->createUrl("chequera/chequeraForm"); ?>",
            success: function(data) {
                $("#_form").html(data);
            },
            error: function(data,status){
            },
            complete: function(){
                init();
                
            }
        });
    }    
</script>

