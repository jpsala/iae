<?php 
	$ciclo_id_para_informes_global_educativo = Helpers::qryScalar(""
			. "select data "
			. "from opcion where nombre = 'ciclo_id' and categoria = 'educativoInformes' and user_id is null"
			);
	if(!$ciclo_id_para_informes_global_educativo){
		$ciclo_id_para_informes_global_educativo = Ciclo::getCicloIdParaInformesEducativo();
	}
	
	$ciclo_id_para_informes_global_admin = Helpers::qryScalar(""
			. "select data "
			. "from opcion where nombre = 'ciclo_id' and categoria = 'adminInformes' and user_id is null"
			);
	if(!$ciclo_id_para_informes_global_admin){
		$ciclo_id_para_informes_global_admin = Ciclo::getCicloIdParaInformesAdmin();
	}
	
	
	$ciclo_nombre_para_informes_global_educativo = Helpers::qryScalar(""
			. "select nombre from ciclo where id = $ciclo_id_para_informes_global_educativo");

	$ciclo_nombre_para_informes_global_admin = Helpers::qryScalar(""
			. "select nombre from ciclo where id = $ciclo_id_para_informes_global_admin");
	?>
	
<script>
	var vm = (function () {
			var vm = {};
			vm.data = {};
			vm.data.ciclo_id_para_informes_educativo = ko.observable(<?php echo Ciclo::getCicloIdParaInformesEducativo() ?>);
			vm.data.ciclo_id_para_informes_admin = ko.observable(<?php echo Ciclo::getCicloIdParaInformesAdmin() ?>);
			vm.ciclo_nombre_para_informes_global_educativo = ko.observable(<?php echo $ciclo_nombre_para_informes_global_educativo ?>);
			vm.ciclo_nombre_para_informes_global_admin = ko.observable(<?php echo $ciclo_nombre_para_informes_global_admin ?>);
			vm.ciclo_activo_nombre = ko.observable("<?php echo Ciclo::getActivoNombre() ?>");
			vm.init = function () {
					console.log('init');
			};
			return vm;
	})();
	vm.cancel = function(){
		window.location.reload();
	}
	vm.submit = function () {
		var data = ko.mapping.toJS(vm.data);
			$.ajax({
					type: "POST",
					dataType: "json",
					data: data,
					//data: $("form").serialize(),
					url: "<?php echo $this->createUrl("/ciclo/graba"); ?>",
					success: function (data) {
							toastr.info("Los datos fueron guardados",null,{timeOut:2500});
							console.log('ok');
					},
					error: function (data, status) {
						console.log('error', data, status);
					}
			}
			);
	};
	vm.init();

</script>