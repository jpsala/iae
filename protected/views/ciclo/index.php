<?php include("index.js.php"); ?>
<div class='container top20'>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4>Ciclos por defecto</h4>
			<!--<span data-bind="text:'El ciclo activo es ' + ciclo_nombre_para_informes_global_educativo()"></span>-->
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-12 top20">
					<form class="form" data-bind="submit: submit">
						<fieldset>
							<legend>Educativo</legend>
								<div class='form-group form-inline'>
									<?php $op = array("class" => "form-control"); ?>
									<?php $ld = CHtml::listData(Ciclo::model()->findAll(), 'id', "nombre"); ?>
									<?php $lo = CHtml::listOptions(3, $ld, $op); ?>
									<label for='ciclo'>Ciclo para Informes:</label>
									<select class="form-control" data-bind="value: data.ciclo_id_para_informes_educativo">
										<?php echo $lo; ?>
									</select>
									<code data-bind="text:'Global: ' + ciclo_nombre_para_informes_global_educativo()" class="help-block" style="width:150px;display:inline"></code>
								</div>
						</fieldset>
						<hl></hl>
						<fieldset>
							<legend>Administrativo</legend>
								<div class='form-group form-inline'>
									<?php $op = array("class" => "form-control"); ?>
									<?php $ld = CHtml::listData(Ciclo::model()->findAll(), 'id', "nombre"); ?>
									<?php $lo = CHtml::listOptions(3, $ld, $op); ?>
									<label for='ciclo'>Ciclo para Informes:</label>
									<select class="form-control" data-bind="value: data.ciclo_id_para_informes_admin">
										<?php echo $lo; ?>
									</select>
									<code data-bind="text:'Global: ' + ciclo_nombre_para_informes_global_admin()" class="help-block" style="width:150px;display:inline"></code>
								</div>
						</fieldset>
						<!--		<fieldset class="top20">
									<legend>Administrativo</legend>
									<div class="col-sm-4 control-group">
										<div class='form-group'>
											<label for='ciclo'>Ciclo para carga de notas:</label>
										</div>
									</div>
								</fieldset>-->
						<div class="btn-group pull-right">
							<button data-bind="click: cancel" class="btn btn-small btn-default">Cancela</button>
							<button type="submit" class="btn btn-small btn-primary">Graba</button>
						</div>
						<div class="row">
							<div class="col-xs-2">
								<input type="text" class="form-control" placeholder=".col-xs-2">
								<p class="help-block">First Name</p>
							</div>
							<div class="col-xs-3">
								<input type="text" class="form-control" placeholder=".col-xs-3">
								<p class="help-block">M.I</p>
							</div>
							<div class="col-xs-4">
								<input type="text" class="form-control" placeholder=".col-xs-4">
								<p class="help-block">Last</p>
							</div>
						</div>
					</form>
				</div>
			</div>
			<span data-bind="text: vm.ciclo_id"></span>
		</div>
	</div>
</div>
	<script>
		ko.applyBindings(vm);
	</script>