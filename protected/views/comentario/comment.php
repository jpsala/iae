<div class="comentario ui-widget-content ui-corner-all" id="<?php echo $comentario->id; ?>" parent_id="<?php echo $comentario->Comentario_id; ?>">
  <div class="comentario-titulo ui-widget-header ui-corner-top">
    <span class="comentario-user"><?php echo "Por: " . $comentario->user->nombre; ?></span>
    <span class="comentario-fecha-creacion"><?php echo "Creado el" . date("d/m/Y \a \l\a\s H:i", mystrtotime($comentario->fecha_creacion)); ?> Horas</span>
  </div>
  <div class="comentario-texto">

    <?php echo $comentario->comentario; ?>


  </div>
  <?php if (!$comentario->Comentario_id): ?>
    <?php if ($comentario->user->id == Yii::app()->user->id): ?>
      <span class="comentario-estado">
        Estado: 
        <?php
        $color = $comentario->estado ? $comentario->estado->color : "black";
        echo CHtml::dropDownList(
                "estado_id", $comentario->estado ? $comentario->estado : null, CHtml::listData(Estado::model()->findAll(), "id", "nombre"), array("style" => "color:$color")
        );
        ?>
      </span>
    <?php else: ?>
      <span class="comentario-estado" style="color:<?php echo $comentario->estado->color; ?>">estado: <?php echo $comentario->estado->nombre; ?></span>
    <?php endif; ?>
  <?php endif; ?>    

  <div class="comentario-pie">
    <?php if ($comentario->user->id == Yii::app()->user->id): ?>
      <div class="div-buttons-actions">
        <a class=" a-icon" onclick="edita(<?php echo $comentario->id; ?>)"><span class="ui-icon ui-icon-pencil"></span>Edita</a>
        <a class=" a-icon" onclick="elimina(<?php echo $comentario->id; ?>)"><span class="ui-icon ui-icon-trash"></span>Elimina</a>
        <a class=" a-icon" onclick="responde(<?php echo $comentario->id; ?>)"><span class="ui-icon ui-icon-arrowreturn-1-w"></span>Responde</a>
      </div>
      <div class="div-buttons-edit">
        <a class=" a-icon" onclick="graba(<?php echo $comentario->id; ?>)"><span class="ui-icon ui-icon-disk"></span>Graba</a>
        <a class=" a-icon" onclick="cancela(<?php echo $comentario->id; ?>)"><span class="ui-icon ui-icon-cancel"></span>Cancela</a>
      </div>
    <?php elseif (Yii::app()->user->id): ?>

      <div class="div-buttons-actions">
        <a class=" a-icon" onclick="responde(<?php echo $comentario->id; ?>)"><span class="ui-icon ui-icon-arrowreturn-1-w"></span>Responde</a>
      </div>
    <?php endif; ?>
    <?php if ($comentario->modificado): ?>
      <span class="comentario-fecha-modificacion"><?php echo "Modificado el " . date("d/m/Y \a \l\a\s H:i", mystrtotime($comentario->fecha_modificacion)); ?> Horas</span>
    <?php endif; ?>

  </div>
</div>


