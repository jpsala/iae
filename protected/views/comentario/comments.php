<style type="text/css">

  .indent{margin-left: 21px; max-width: 600px; border-left: #EEE dashed 1px}
  .comentario{width: 100%; min-height: 67px; position: relative; margin: 0px 0 5px 0}
  .comentario *{font-size:87%}
  .comentario-titulo{padding: 1px; text-align: left}
  .comentario-texto{padding: 6px;min-height: 21px;}
  .comentario-user{color:lightcoral}
  .comentario-pie{min-height: 22px;  border-top: lavender solid 1px; min-width: 100%;padding-top: 1px;}

  .comentario-fecha-creacion,.comentario-fecha-modificacion{float: right; margin:5px 5px 0 0;color:lightcoral}
  .comentario-fecha-modificacion{font-weight: bold; color:#D63301}
  .comentario-fecha-creacion{margin:0}

  #edit-template{display:none; width: 100%; max-height: 200px; }
  .editando{background-color: white}
  .a-icon{ text-decoration: none ; display: inline-block; line-height: 17px; margin: 0 2px; cursor: pointer; padding: 2px}
  .a-icon:hover{ text-decoration: underline;color: #0099FF}
  .a-icon span{float: left;margin-top:-1px}
  .div-buttons-edit,#div-comentario-template{display:none}
  .div-buttons-actions,.div-buttons-edit{float:left}
  #comentario-nuevo{ margin: -10px 0 3px 10px; font-size: 70%}
  .comentario-estado{display: block;    margin: 1px 0 8px 5px;}
  .comentario-estado select{width: 130px; margin-top: 2px}
</style>

<textarea id="edit-template">
</textarea>

<div parent_id="" id="div-comentario-template" class="comentario ui-widget-content ui-corner-all">
  <div class="comentario-titulo ui-widget-header ui-corner-top">
    <span class="comentario-user">Por: <?php echo Yii::app()->user->name; ?></span>
    <span class="comentario-fecha-creacion"></span>
  </div>
  <div class="comentario-texto">
    <div class="comentario-texto editando" contenteditable="true"></div>
  </div>
  <span class="comentario-estado">
    <?php
    echo CHtml::dropDownList(
            "estado_id", null, CHtml::listData(Estado::model()->findAll(), "id", "nombre")
    );
    ?>
  </span>
  <div class="comentario-pie">
    <div class="div-buttons-edit">
      <a onclick="graba('nuevo')" class=" a-icon"><span class="ui-icon ui-icon-disk"></span>Graba</a>
      <a onclick="cancela('nuevo')" class=" a-icon"><span class="ui-icon ui-icon-cancel"></span>Cancela</a>
    </div>
    <span class="comentario-fecha-modificacion"></span>

  </div>
</div>
<?php if (Yii::app()->user->id): ?>
  <a id ="comentario-nuevo" onclick="nuevo()" class=" a-icon"><span class="ui-icon ui-icon-document"></span>Nuevo comentario</a>
<?php endif; ?>

<?php
echo $this->comments;
?>

<script type="text/javascript">
  var editando = false; textoBackup="";
  var contexto = "<?php echo $contexto; ?>";
 
  $(function(){
    $(".comentario-estado select").attr("disabled","disabled");
  })
    
  function nuevo(){
    var $nuevo = $("#div-comentario-template").clone();
    $nuevo.find(".div-buttons-edit").css("display","block").show();
    $nuevo.attr("id","nuevo").show();
    $(".indent:first").prepend($nuevo);
    $nuevo.after("<div class='indent'></div>");
    $nuevo.find(".comentario-texto").focus();
    $("#nuevo .comentario-estado select").removeAttr("disabled");    
  }
    
  function responde(comentario_parent_id){
    if(editando) return;
        
    var $parent = $("#"+comentario_parent_id)
        
    var $nuevo = $("#div-comentario-template").clone().
      attr("parent_id",comentario_parent_id).
      find(".div-buttons-edit").show().end().
      attr("id","nuevo").
      show();
        
    $indent =  $parent.next(".indent").
      append($nuevo);
        
    $nuevo.
      after("<div class='indent'></div>").
      find(".comentario-texto").focus();
        
    editando = true;
  }
    
  function edita(comentario_id){
    if(editando) return;
    $this = $("#"+comentario_id);
    $comentarioTexto = $this.find(".comentario-texto");
    textoBackup = $comentarioTexto.html();
    $comentarioTexto.attr("contenteditable","true");
    $comentarioTexto.addClass("editando");
    $this.find(".comentario-texto").focus();
    $this.find(".div-buttons-actions").hide();
    $this.find(".div-buttons-edit").show();
    $(".comentario-estado select").removeAttr("disabled");

    editando = true;
  }
    
  function elimina(comentario_id){
    if(editando) return;
    var $this = $("#"+comentario_id);
    $.ajax({
      type: "POST",
      data: {comentario_id:comentario_id},
      url: "<?php echo $this->createUrl("comentario/elimina"); ?>",
      success: function(data) {
        $this.remove();
      },
      error: function(data,status){
      }
    });

  }
    
  function graba(comentario_id){
    var $this = $("#"+comentario_id);
    var parent_id = $this.attr("parent_id");
    $comentarioTexto = $this.find(".comentario-texto");
    texto = $comentarioTexto.html();
    if(!texto){
      return;
    }
    $comentarioTexto.removeClass("editando");
    $comentarioTexto.removeAttr("contenteditable");
    subeComentario(texto,comentario_id,parent_id);
    $this.find(".div-buttons-actions").show();
    $this.find(".div-buttons-edit").hide();

    editando = false;
  }
    
  function subeComentario(texto,comentario_id,comentario_parent_id){
    var $this = $("#"+comentario_id);
    comentario_parent_id = comentario_parent_id ? comentario_parent_id : "";
    estado_id = $("#"+comentario_id+" .comentario-estado select").val();
    $.ajax({
      type: "POST",
      data: {texto:texto, comentario_id:comentario_id,comentario_parent_id:comentario_parent_id, contexto:contexto, estado_id:estado_id},
      url: "<?php echo $this->createUrl("comentario/graba"); ?>",
      success: function(data) {
        if(data){
          $this.replaceWith(data);
          $(".comentario-estado select").attr("disabled","disabled");
        }
      },
      error: function(data,status){
      }
    });

  }
    
  function cancela(comentario_id){
    editando = false;
    if(comentario_id=="nuevo"){
      $("#"+comentario_id).remove();
      return;
    }
    var $this = $("#"+comentario_id);
    $comentarioTexto = $this.find(".comentario-texto");
    $comentarioTexto.html(textoBackup);
    $comentarioTexto.removeClass("editando");
    $comentarioTexto.removeAttr("contenteditable");
    $this.find(".div-buttons-actions").show();
    $this.find(".div-buttons-edit").hide();
    $(".comentario-estado select").attr("disabled","disabled");

  }
</script>
