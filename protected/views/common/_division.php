<style type="text/css">
    #pedido-de-datos{margin: 5px 0px 0px 6px}
    #pedido-de-datos .row{display:inline-block}
    #pedido-de-datos #nivel-select{width: 150px !important}
    #pedido-de-datos #anio-select{width: 100px !important}
    #pedido-de-datos #division-select{width: 100px !important}
</style>
<div id="pedido-de-datos">
    <?php if (!isset($alumnoDivisionActiva) or !$alumnoDivisionActiva or true): ?>
        <div class="row">
            <select id="nivel-select" data-placeholder="Nivel" class="chzn-select" name="nivel_id">
                <?php
                $x = array('prompt' => '');
                echo CHtml::listOptions(null, CHtml::listData(Nivel::model()->findAll(), 'id', 'nombre'), $x)
                ?>
            </select>
        </div>
        <div class="row">
            <select id="anio-select" data-placeholder="Año" class="chzn-select" name="anio_id">
                <?php
                $x = array('prompt' => '');
                echo CHtml::listOptions(null, CHtml::listData(Nivel::model()->findAll("id=-1"), 'id', 'nombre'), $x)
                ?>
            </select>
        </div>
        <div class="row" id="div-select-division">
            <select id="division-select" data-placeholder="Division" class="chzn-select" name="division_id">
                <?php
                $x = array('prompt' => '');
                echo CHtml::listOptions(null, CHtml::listData(Division::model()->findAll("id=-1"), 'id', 'nombre'), $x);
                ?>
            </select>
        </div>
    <?php elseif (isset($alumnoDivisionActiva) and $alumnoDivisionActiva): ?>
        <div class="row" id="div-select-division" style="display:none">
            <select id="division-select" data-placeholder="Division" class="chzn-select" name="division_id">
                <?php
                $x = array('prompt' => '');
                $anio_id = $alumnoDivisionActiva->division->Anio_id;
                echo CHtml::listOptions($alumnoDivisionActiva->Division_id, CHtml::listData(Division::model()->findAll("Anio_id=$anio_id"), 'id', 'nombre'), $x);
                ?>
            </select>
        </div>
    <?php endif; ?>
</div>

<script type="text/javascript">
    var logica_periodo_id, division = {};
    
    division.init = function(){
        
        $("button").button();
        
        $("#nivel-select").chosen().change(function(){
            division.changeNivel($(this));
        });

        $("#anio-select").chosen().change(function(){
            division.changeAnio($(this));
        });

        $("#division-select").chosen().change(function(){
            division.changeDivision($(this));
        });

        //        $(".clearinput").clearinput();
        
    }
    
    division.changeNivel = function ($this){
        $.ajax({
            type: "GET",
            //dataType: "json",
            data: {nivel_id:$this.val()},
            url: "<?php echo $this->createUrl("anio/options"); ?>",
            success: function(data) {
                $("#anio-select").html(data);
                $("#anio-select").trigger("liszt:updated");
                $("#anio-select_chzn").mousedown();

            },
            error: function(data,status){
            }
        });
        return false;
    }

    division.changeAnio = function($this){
        $.ajax({
            type: "GET",
            //dataType: "json",
            data: {anio_id:$this.val()},
            url: "<?php echo $this->createUrl("division/options"); ?>",
            success: function(data) {
                $("#division-select").html(data);
                $("#division-select").trigger("liszt:updated");
                $("#division-select_chzn").mousedown();

            },
            error: function(data,status){
            }
        });
        return false;

    }
    
    division.changeDivision = function ($this){
        if(reasigna){
            reasigna($this.val());
        }
        return false;
    }
    
    $(function(){
        division.init();
    });
    
</script>