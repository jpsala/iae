<script  type="text/javascript" src="js/views/concepto/mover.js"></script>
<style>
	.tree-col{overflow: auto; max-height: 500px}
	a{cursor: pointer}	
</style>
<?php

	function getJsTree($id = null, $concepto_id = null) {
		$id = $id ? $id : uniqid("treeview");
		$br = "";
		$criteria = array(
				"condition" => !$concepto_id ? "concepto_id is null" : "concepto_id = $concepto_id",
				"order" => "nombre"
		);

		echo "<ul  id=\"$id\">$br";
		foreach (Concepto::model()->findAll($criteria) as $concepto) {
			$cantMovs = Helpers::qryScalar("
				select count(*) from doc where concepto_id = $concepto->id
				");
			$criteriaCant = array("condition" => "concepto_id = $concepto->id");
			$cant = Concepto::model()->count($criteriaCant);

			$class = $cant > 1 ? "rel=\"folder\"" : "rel=\"file\"";
			echo "<li id=\"$concepto->id\" cant=\"$cant\" $class>$br";
			echo "<a class='list-group-item'>$concepto->nombre($cantMovs)</a>$br";
			if ($cant > 0) {
				getJsTreeRecursive($concepto);
			}
			echo "</li>$br";
		}
		echo "</ul>$br";
	}

	function getJsTreeRecursive($concepto) {
		$br = "";
		$ul = "<ul  class='list-group'>";
		$endul = "</ul>";
		$endli = "</li>";
		$conceptos = Concepto::model()->findAll(array(
				"condition" => "concepto_id = $concepto->id",
				"order" => "nombre"
		));
		echo "<ul>$br";
		foreach ($conceptos as $concepto) {
			$cantMovs = Helpers::qryScalar("
				select count(*) from doc where concepto_id = $concepto->id
				");
			$cant = Concepto::model()->count(array(
					"condition" => "concepto_id = $concepto->id"
			));
			$class = $cant > 1 ? "rel=\"folder\"" : "rel=\"file\"";
			echo "<li id=\"$concepto->id\" cant=\"$cant\" $class>$br";
			echo "<a class='list-group-item'> $concepto->nombre($cantMovs)</a>$br";

			if ($cant > 0) {
				echo "$ul$br";
				getJsTreeRecursive($concepto);
				echo "$endul$br";
			}
			echo "$endli$br";
		}
		echo "</ul>$br";
	}
?>

<div class="container">
	<div id="src" class="col-md-6 tree-col">
		<?php getJsTree("treeConceptos"); ?>
	</div>
	<div id="tgt" class="col-md-6 tree-col">
		<?php getJsTree("treeConceptos"); ?>
	</div>
</div>

<script  type="text/javascript">
	$(function() {
		var selectedClass = "active";
		$("#src").on("click", "a", function(e) {
			$("#src ." + selectedClass).removeClass(selectedClass);
			$(e.target).addClass(selectedClass);
		});
		$("#tgt").on("click", "a", function(e) {
			var $src = $("#src ." + selectedClass);
			var $tgt = $(e.target);
			var src_id = $src.closest("li").attr("id");
			var tgt_id = $tgt.closest("li").attr("id");
			if ($src.length > 0) {
				$.ajax({
					type: "GET",
					data: {src_id: src_id, tgt_id: tgt_id},
					url: "<?php echo $this->createUrl("concepto/moverAjax"); ?>",
					success: function(data) {
                        console.log(data);
                        var pos=$tgt.html().search('[(]');
                        var cont = $tgt.html();
                        $tgt.html(cont.substr(0,pos+1)+data+')');
						$src.closest("li").remove();
					},
					error: function(data, status) {
					}
				}
				);
			}
			;
		});
	});
</script>