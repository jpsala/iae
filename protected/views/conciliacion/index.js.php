<link rel="stylesheet" type="text/css" href="js/tablesorter/themes/blue/style.css"/>
<script src="js/tablesorter/jquery.tablesorter.js"></script>
<script type="text/javascript">
    var urlImpresion = "<?php echo $this->createUrl("/informe/ConciliacionInforme"); ?>";
    var doc = new Object(), ant = 0;
    var conciliacion_id = null;
//  var url = "<?php
//echo $this->createUrl('doc/movCaja',
//          array("comprob_id"=>$comprob_id));
?>";
    doc.pagoACuenta = 0, doc.totalPagos = 0, doc.total = 0;
    doc.saldo = 0, doc.disabled = false;

    doc.toggleEnabled = function(on) {
        doc.disabled = !on;
        if (on) {
            $(".td_edit_img *").removeAttr('disabled');
            $(".td_delete_img *").removeAttr('disabled');
            $("#pago").removeAttr('disabled');
            $("#izq *").removeAttr('disabled');
            $("#botones *").removeAttr('disabled');
            $("#pago-a-cuenta-div *").removeAttr('disabled');
        } else {
            $(".td_edit_img *").attr('disabled', "disabled");
            $(".td_delete_img *").attr('disabled', "disabled");
            $("#pago").attr('disabled', "disabled");
            $("#izq *").attr('disabled', "disabled");
            $("#botones *").attr('disabled', "disabled");
            $("#botones #CtaCte").removeAttr('disabled');
            $("#botones #cancel").removeAttr('disabled');
        }
        return false;
    }

    doc.muestraTotales = function() {
        doc.saldo = round(Number(doc.totalAPagar) - Number(doc.totalPagos));
        doc.total = round(Number(doc.totalPagos) + Number(doc.pagoACuenta));

        if (cajaCerrada) {
            doc.toggleEnabled(0);
        }

        $("form #total-pagos-div #total-a-pagar span.total").html(round(doc.totalAPagar));
        $("#total-pagos").html(round(doc.totalPagos));
        $("form #total-pagos-div #total-pagado span.total").html(round(doc.totalPagos));
        $("#total-pagos").html(doc.total);
        $("form #total-pagos-div #total-saldo span.total").html(round(doc.saldo));

        $("form #total-div span.total").html(round(doc.total));

        $("form #div-pago #pago").val(round(doc.total));

        //    $("#total-pagos-div div#total-a-pagar span.total").html(doc.total);

    };

    jQuery(document).ready(function() {
        onReady();
    });

    function onReady() {

        $("#agrega-articulo").button();
        $("#concilia").attr("disabled", true);

        $("#destino_id").chosen().change(function(a) {
            var banco = $(this).find("option:selected").attr("banco");
            $("#cuenta-banco-div span").html(banco);
        });

        $('#ingreso_egreso').attr({'disabled': 'disable'});

        $(":button").button();

        articuloChosen();

        $("#detalle").focus();

        $(".numeric").numeric();

        $("#numero").change(function() {
            //validaNumeroComprob($(this));
        });

    }

    function importeAjusteChange(e) {
        e.val(round(Number(e.val())));
        var pago = e.val();
        doc.totalPagos = round(doc.totalPagos - (ant - pago), 2);
        ant = pago;
        doc.muestraTotales();
        valores.refresh();
    }

    function igualaAlto() {
        var i = $("#izq").height(), d = $("#der").height();
        if (d > i) {
            $("#izq").height(d - 11);
        } else if (i > d) {
            $("#der").height(i);
        }
    }

    function submitMov(concilia) {
        var data = $("#main-form").serialize() + "&doc[conciliacion_id]=" + conciliacion_id + "&concilia="+concilia;
        $("#movimientos-table tr").each(function() {
            var mov_id = $(this).attr("mov_id");
            if (mov_id) {
                var checked = $(this).find("input").attr("checked");
                checked = checked ? checked : "";
                //console.log(mov_id);
                //console.log(checked);
                data += "&movimientos[" + mov_id + "]=" + checked;
            }
        });
        if(Number($("#total").val()) !== Number($("#mov-suma").val()) && concilia){
            alert("Hay una diferencia entre el total a conciliar y la suma de los movimientos marcados");
            return;
        }
        $.ajax({
            type: "POST",
            dataType: "json",
            data: data,
            url: "<?php echo $this->createUrl("conciliacion/graba"); ?>",
            success: function(data) {
//                log(data);
//                return;
                if (data.errores && muestraErroresDoc(data.errores)) {
                    return false;
                }
                alert("La conciliación fué grabada");
                //window.location = url;
                window.location = window.location;
            },
            error: function(data, status) {
                console.log(data,status);
                alerta(data.responseText,status);
            }
        });
        return false;
    }

    function agregaArticulo() {
        var vacio = false;
        $(".articulo-tr").each(function() {
            if ($(this).find(".articulo").val().length === 0 || $(this).find(".importe").val().length === 0) {
                vacio = true;
            }
            ;
        });
        if (vacio) {
            return;
        }
        $.ajax({
            type: "GET",
            //dataType: "json",
            data: {},
            url: "<?php echo $this->createUrl("doc/movBancoGetTr"); ?>",
            success: function(data) {
                $("#articulos-table").append(data);
                articuloChosen();
                $(".numeric").autoNumeric("init", {mDec: 2, aSep: ''});
                //articuloAC();
            },
            error: function(data, status) {
            }
        });

    }

    function articuloChosen() {
        $(".articulo").chosen().change(function() {
            setTimeout(function($this) {
                $this.closest("tr").find(".importe").focus();
            }, 100, $(this));
        });
    }

    function articuloAC() {
        $('.articulo')
                .autocomplete(
                {
                    autoSelect: true,
                    autoFocus: true,
                    minLength: 1,
                    source: urlArticuloAC,
                    select: function(event, ui) {
                    }
                }
        );
    }

    function borraArticulo($this) {
        $this.closest("tr").remove();
    }

    function destinoChange($this) {
        var destino_id = $this.val();
        $.ajax({
            type: "GET",
            dataType: "json",
            data: {destino_id: destino_id},
            url: "<?php echo $this->createUrl("conciliacion/traeDatos"); ?>",
            success: function(data) {
                $("#movimientos-table").html(data.movimientos);
                $("#numero").val(data.numero);
                $("#total").val(data.total);
                $("#saldo").val(data.saldo);
                conciliacion_id = data.conciliacion_id;
                $("#movimientos-table table").tablesorter({headers: {
                        // assign the secound column (we start counting zero)
                        44: {
                            // disable it by setting the property sorter to false
                            sorter: false
                        }
                    }});
                sumaMovimientos();

//$("#fecha").val(data.fecha);
            },
            error: function(data, status) {
            }
        }
        );
    }

    function sumaMovimientos() {
        var total = Number($("#saldo").val());
        $("#movimientos-table table tr").each(function() {
            var mov_id = $(this).attr("mov_id");
            var checked = $(this).find("input").attr("checked");
            if (mov_id && checked) {
                var debe = Number($(this).find(".debe").html());
                var haber = Number($(this).find(".haber").html());
                total += debe - haber;
            }
        });
        $("#mov-suma").val(total.toFixed(2));
        if ((total != 0) && (round(total) == round(Number($("#total").val())))) {
            $("#concilia").button({"disabled": false});
        } else {
            $("#concilia").button({"disabled": true});
        }
    }

    function checkChange() {
        sumaMovimientos();
    }

    function totalChange(){
        sumaMovimientos();
    }

    function imprime1() {
        window.open(urlImpresion+"&conciliacion=" + conciliacion_id + "&destino_id="+ $("#destino_id").val());
      }

</script>
