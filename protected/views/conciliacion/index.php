<?php $baseUrl = Yii::app()->request->baseUrl; ?>
<?php include("index.js.php"); ?>
<?php include("index.css.php"); ?>

<?php //$doc_id = 84963;     ?>
<?php $doc_id = null; ?>
<form id="main-form" name="main" method="POST" action="#">
    <input type="hidden" name="doc[comprob_id]" value=""/>
    <div id="izq">
        <div class="titulo">
            <span>Movimientos Bancarios</span>
        </div>
        <div id="izq-content">
            <div class="inline-row">
                <!--<label for="destino_id">Cuenta:</label>-->
                <span class="label">Cuenta:</span><select name="doc[destino_id]"
                        id="destino_id" 
                        onchange="destinoChange($(this));
                        return false"
                        data-placeholder="Seleccione una cuenta">
                    <option></option>
                    <?php echo $destinoListData; ?>
                </select>
            </div>
            <div id="div-numero">
                <span class="label">Número:</span><input tabindex="-1" name ="doc[numero]" id="numero" value="" ddisabled="DISABLED" placeholder="Número"/>
            </div>
            <div class="row">
                <span class="label">Saldo anterior:</span><input class="pago numeric" type="text" disabled="disabled" id="saldo" value=""/>
            </div>
            <div class="row">
                <!--<label for="total">Importe:</label>-->
                <span class="label">Total a conciliar:</span><input 
                    name="doc[total]" id="total" value="" 
                    onchange="totalChange();return false;"
                    placeholder="Importe" class="pago numeric"/>
            </div>
            <div class="row">
                <span class="label">Subtotal:</span><input class="pago numeric" type="text" disabled="disabled" id="mov-suma" value="0"/>
            </div>
            <div class="ui-widget-content ui-corner-all" id="articulos-div">
                <div class="ui-widget-content ui-corner-all">
                    <div class="titulo">Movimientos</div>
                    <div id="movimientos-table">
                        <?php include("movimientos.php")?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="botones">
        <div id="botones">
            
            <input type="button" id="ok" value="Graba" onclick="return submitMov(false);"/>
            <input type="button" id="concilia" value="concilia" onclick="return submitMov(true);"/>
            <input type="button" id="imprime" value="imprime" onclick="return imprime1();"/>
        </div>
    </div>
</form>
</div>