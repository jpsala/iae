<table class="tablesorter" id="movimientos-table" class="tabla">
    <thead>
        <tr>
            <th class="movimientos-fecha-acred">Fecha Acred.</th>
            <th class="movimientos-fecha">Fecha</th>
            <th class="movimientos-detalle">Detalle doc</th>
            <th class="movimientos-num">Número doc</th>
            <th class="movimientos-obs">Detalle valor</th>
            <th class="movimientos-numero">Nro.</th>
            <th class="movimientos-debe">Debe</th>
            <th class="movimientos-haber">Haber</th>
            <th class="conciliado"></th>
        </tr>
    </thead>
    <tbody>
        <?php if (isset($movimientos)): ?>
            <?php foreach ($movimientos as $mov): ?>
                <tr mov_id="<?php echo $mov["id"];?>">
                    <td><?php echo date("d/m/Y", mystrtotime($mov["fecha1"])); ?></td>
                    <td><?php echo date("d/m/Y", mystrtotime($mov["fecha_creacion"])); ?></td>
                    <td><?php echo $mov["detalle"]; ?></td>
                    <td><?php echo $mov["docnumero"].' '. $mov["abreviacion"]?></td>
                    <td><?php echo $mov["obs"] . " " . $mov["chq_entregado_a"]; ?></td>
                    <td><?php echo $mov["numero"]; ?></td>
                    <td class="debe"><?php echo $mov["signo_banco"] == "1" ? $mov["importe"] : ""; ?></td>
                    <td class="haber"><?php echo $mov["signo_banco"] == "1" ? "" : $mov["importe"]; ?></td>
                    <?php $checked = $mov["conciliacion_id"] ? "checked='checked'" : ""; ?>
                    <td><input onchange="checkChange($(this));return false;" type="checkbox" <?php echo $checked; ?> </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
</table>