<table class="ui-widget ui-widget-content" id="tabla-asignaturas">
  <tr class="ui-widget-header">
    <th>Fecha</th>
    <th>Cantidad</th>
    <th>detalle</th>
    <th>&nbsp</th>
    <th>&nbsp</th>
  </tr>
  <?php $row = 0; /* @var $asignatura Asignatura */ ?>
  <?php foreach ($items as $item): ?>
    <tr onclick="return seleccionaItem($(this));" class="tr-item <?php echo ($row++ % 2) ? "tr-even" : ""; ?>" id=" <?php echo $item->id; ?>">
      <td class="td-fecha"><?php echo date("d/m/Y",mystrtotime($item->fecha)); ?></td>
      <td class="td-cantidad"><?php echo $item->cantidad; ?></td>
      <td class="td-detalle"><?php echo $item->detalle; ?></td>
      <td></td>
      <td class="td-borra" onclick="return borraItem(<?php echo $item->id; ?>)"></td>
    </tr>
  <?php endforeach; ?>
  <tr class="<?php echo ($row++ % 2) ? "tr-even" : ""; ?>" id="tr-item-nueva">
    <td class="td-fecha"><input id="fecha-item" name="fecha" class="fecha"/></td>
    <td class="td-cantidad"><input id="cantidad-item" name="Cantidad"/></td>
    <td class="td-detalle"><input id="detalle-item" name="detalle"/></td>
    <td>&nbsp</td>
    <td ><input type="button" id="graba-item-nueva-button" value="Graba" onclick="return grabaItem()"/></td>
  </tr>
</table>
