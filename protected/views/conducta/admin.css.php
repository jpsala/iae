<style type="text/css">
    #errores{position: fixed; top: 5px; right: 5px; display:none; }
    .active-result.highlighted{border-radius: 4px}
    .ui-widget{font-size: 85%;}
    .ui-widget-header{ text-align: center}
    .chzn-container-single .chzn-single div b{background: url("images/chosen-sprite.png") no-repeat scroll 0 -2px transparent;}
    .chzn-container-single .chzn-single{height: 20px;line-height: 20px;}

    #main-form{ position: relative}

    #div-elije-alumno #alumno_ac{width:300px}


    #div-datos { float: left;  margin: 5px 0;  min-height: 341px;  padding: 5px;  position: relative;  width:100%  }

    #tabla-asignaturas{width: 500px}
    .tr-item{cursor:pointer}
    .tr-item:hover{background-color: lightyellow}
    .td-borra{background-image: url("images/delete.jpg");background-repeat: no-repeat; cursor: pointer; text-align: center;background-position: center center;}
    .td-fecha,.td-fecha input{width:90px}
    .td-cantidad,.td-cantidad input{width:70px}
    .td-detalle,.td-detalle input{width:500px}
    .row{margin: 4px 0px 5px 0px}
    .row label{    float: left;    padding-right: 7px;    padding-top: 4px;    text-align: right;    width: 72px;}
    .row .inline{display: inline-block}
    .row select {width: 180px !important}
    .buttons-div{     clear: both;    padding-top: 11px;    text-align: right;}
    .right-align{text-align: right}
    .titulo{padding: 3px; background-color: lightblue; border-radius: 5px 5px 0px 0px}
    #periodo-select{width: 220px}
    #periodo-div{display: inline;}
    #comentarios{ padding-top: 10px; clear: both}
</style>
