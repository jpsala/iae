<link rel="stylesheet" type="text/css" href="js/jqgrid/css/ui.jqgrid.css" />
<script src="js/jqgrid/js/i18n/grid.locale-es.js"></script>
<script src="js/jqgrid/js/jquery.jqGrid.min.js"></script>
<script src="js/common.js"></script>
<script type="text/javascript">
//  var urlAlumnoAC = "<?php echo $this->createUrl('alumno/autoComplete'); ?>";
//  var alumno_id = null;
  var logica_periodo_id = null;
  var $grid;
  var itemId;
  var fecha_hoy = "<?php echo $fecha_hoy; ?>";
  var $itemTemplate = $('<tr id="-1" class="tr-item "><td class="td-fecha"></td><td class="td-cantidad"></td><td class="td-detalle"></td><td></td><td onclick="return borraitem(-1)" class="td-borra"></td></tr>');
  $(function() {

    init();

  });

  function init() {

    updChosenPeriodos();

    $(".button").button();

  }

  function nivelChange($nivel) {
    nivel_id = $nivel.val();
    updChosenPeriodos(nivel_id);
  }

  function updChosenPeriodos(nivel_id) {
    $.ajax({
      type: "GET",
      url: "<?php echo $this->createUrl('logicaPeriodo/optionsConductaDesdeNivel'); ?>&nivel_id=" + nivel_id,
      success: function(data) {
        $("#periodo-select").html(data);
        $("#periodo-select").trigger("liszt:updated");
        $("#periodo-select").chosen().change(function() {
          periodoChange($(this));
        });
      },
      error: function(data, status) {
        division_asignatura_id = null;
      }
    });
  }

  function traeItems() {
    if (!logica_periodo_id || !alumno_id) {
      return;
    }
    $.ajax({
      type: "GET",
      data: {"alumno_id": alumno_id, "logica_periodo_id": logica_periodo_id},
      url: "<?php echo $this->createUrl("conducta/traeItems"); ?>",
      success: function(data) {
        $("#errores").hide();
        $("#div-datos").html(data);
        $(".tr-item").find("td").not(".td-borra").click(function() {
          seleccionaItem($(this));
        });
        $("#fecha-item").datepicker().val(fecha_hoy);
        $("#cantidad-item").val(1);
        $("#detalle-item").focus();
      },
      error: function(data, status) {
      }
    });
  }
  
  function seleccionaItem($this) {
    $("#fecha-item").val($this.find(".td-fecha").html());
    $("#cantidad-item").val($this.find(".td-cantidad").html());
    $("#detalle-item").val($this.find(".td-detalle").html());
    itemId = $this.attr("id");
  }

  function grabaItem() {
    var $this;
    if (itemId) {
      console.log(itemId);  
      $this = $("#" + itemId.trim());
      console.log($this);  
      
    } else {
      $this = $itemTemplate.clone();
    }
    $.ajax({
      type: "POST",
      dataType: "json",
      data: {
        alumno_id: alumno_id,
        logica_periodo_id: logica_periodo_id,
        item_id: itemId ? itemId : -1,
        fecha: $("#fecha-item").val(),
        cantidad: $("#cantidad-item").val(),
        detalle: $("#detalle-item").val()
      },
      url: "<?php echo $this->createUrl("conducta/grabaItem"); ?>",
      success: function(data) {
        if (data.errors) {
          muestraErrores(data.errors);
          return;
        }
        $("#tabla-asignaturas").find("tr").last().before($this);
        $this.find("td").not(".td-borra").click(function() {
          seleccionaItem($(this));
        });
        $this.find(".td-fecha").html($("#fecha-item").val());
        $this.find(".td-cantidad").html($("#cantidad-item").val());
        $this.find(".td-detalle").html($("#detalle-item").val());
        $this.find(".td-borra").attr("onclick", "return borraItem("+data.id+")");
        $this.attr("id", data.id);
        $("#fecha-item").val(fecha_hoy);
        $("#cantidad-item").val(1);
        $("#detalle-item").val("").focus();
        itemId = null;
      },
      error: function(data, status) {
      }
    });
    return false;
  }

  function borraItem(itemId) {
    if (!confirm("Está seguro de borrar este registro?")) {
      return;
    }
    $.ajax({
      type: "POST",
//      dataType:"json",
      data: {item_id: itemId},
      url: "<?php echo $this->createUrl("conducta/borraItem"); ?>",
      success: function(data) {
        $("#tabla-asignaturas").find("#" + itemId).remove();
      },
      error: function(data, status) {
      }
    });
    return false;
  }

  function divisionChange($division) {
    if (!$("periodo-select").val()) {
      $("#periodo_select_chzn").mousedown();
    }
  }
  
  function alumnoChange($alumno) {
    alumno_id = $alumno.val();
    traeItems();
  }

  function periodoChange($periodo) {
    logica_periodo_id = $periodo.val();
    $("#alumno_select_chzn").mousedown();
    alumno_id=$("#alumno-select").val();
    traeItems();
  }

</script>
