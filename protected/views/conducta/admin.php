<head>
  <?php include("admin.css.php"); ?>
  <?php include("admin.js.php"); ?>    
</head>
<?php
$paramNivel = new ParamNivel(array(
    "controller" => $this,
    "onchange" => "nivelChange",
        ));
$paramAnio = new ParamAnio(array(
    "controller" => $this,
        ));
$paramDivision = new ParamDivision(array(
    "controller" => $this,
    "onchange" => "divisionChange",
    "seleccionaAuto" => false,
        ));
$paramAlumno = new ParamAlumno(array(
    "controller" => $this,
    "onchange" => "alumnoChange"
        ));
?>
<form id="main-form" name="main" method="POST" action="#">
  <?php
  $paramNivel->render();
  $paramAnio->render();
  $paramDivision->render();
  ?>
  <div id="periodo-div">
    <select id="periodo-select" data-placeholder="Seleccione el período" class="chzn-select">
      <?php
      $x = array('prompt' => '');
      echo CHtml::listOptions(null, CHtml::listData(LogicaPeriodo::model()->findAll("id=-1"), 'id', 'nombre'), $x)
      ?>
    </select>
  </div>
  <?php
  $paramAlumno->render();
  ?>
  <div id="errores" class="validation"></div>
  <div id="div-datos" class="ui-widget-content ui-corner-all">
  </div>
</form>


