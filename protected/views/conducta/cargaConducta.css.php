<style type="text/css">

  /*  #pedido-de-datos .row{display:inline-block}
    #pedido-de-datos #nivel-select{width: 120px;}
    #pedido-de-datos #anio-select{width: 75px}
    #pedido-de-datos #division-select{width: 115px}
    #pedido-de-datos #asignatura-select{width: 220px}
    #pedido-de-datos #periodo-select{width: 180px}
  
    #div-para-notas{margin-top: 8px}
  
    input.nota{ width: 33px; text-align: right}
    textarea.nota{}
    .nota-no-editable{    border: 1px solid #CECECE;    border-radius: 2px 2px 2px 2px;    box-shadow: 0 11px 11px rgba(0, 0, 0, 0.1) inset, 0 1px 0 rgba(255, 255, 255, 0.2);    color: #999999;    float: right;    height: 15px;    outline: medium none;    padding: 3px;    width: 25px;    max-width: 25px}
    table#main-table{ background-color: #C3D9FF;    border: #0066A4 solid 1px; border-radius: 4px; padding: 2px}
    table#main-table tr{background-color: #fff}
    table#main-table .td-nota {width: 35px;}
    table#main-table td.td-alumno{width: 250px}
    table#main-table th{padding-left:3px}
        table#main-table th:last-child{min-width:2px}
    table#main-table *{font-size: 98%}
    table#main-table input{ padding-left: 2px; padding-right: 2px}
    table#main-table tr.error{display:none}
    table#main-table tr.error .td-error{padding: 0}
    table#main-table tr td input.error{box-shadow:0 1px 3px rgba(200, 20, 0, 0.8) inset, 0 1px 0 rgba(55, 55, 0, 0.1)}
    table#desc-table{background-color: #C3D9FF;    border: 1px solid #0066A4;    border-radius: 4px 4px 4px 4px;    margin: 10px 0 0;    padding: 4px;    width: 400px;}
    table#desc-table tr{background-color: #fff}
    table#desc-table td{ font-size: 90%; padding: 1px 0px 2px 6px}
    table#desc-table th{border-bottom: 1px solid #777777;  padding: 2px 0 2px 5px;}
  
    th, td, caption{padding:1px 0px 2px 0px}
    th{background-color: #C3D9FF; color: #555555}
    td.center,th.center{text-align: center}
    td.rec-img{    background-position: center center;    background-repeat: no-repeat;    height: 15px;    padding: 0;    width: 26px;}
    .error-span{background-color: LightYellow;    border: 2px solid #C3D9FF;    border-radius: 6px 6px 6px 6px;    color: SlateGrey;    display: inline-block;    margin: 2px 2px 6px 0;    padding: 4px 5px 4px 5px;}
    .error-span strong{color: Tomato;}
    .buttons-div{text-align: right}
    .tr-even { background-color:#edf1fe !important}
    .right-align{text-align: right}
    #top-botton-div{float: right}
    #imprime{float:right; margin-right: 5px}*/

  #top-botton-div {
    float: right;
  }

  #periodo-select{width:180px;}
  #periodo-select-div{display: inline}

  #conducta-table {width: 500px;margin-top: 26px;}

  .alumno-nombre-td{width:200px;padding-left: 5px}
  .conducta-th, .conducta-td{padding: 3px 0 0;
                             text-align: right;}
  .conducta-input,.conducta-span{float: right;
                                 text-align: center;
                                 width: 44px;}
  #conducta-table td {
    padding: 3px 0 0;
  }
</style>
