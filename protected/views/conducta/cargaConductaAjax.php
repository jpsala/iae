<?php
$this->renderPartial("/conducta/cargaConducta.css");
if (count($conductas) == 0) {
  die;
}
//var_dump($conductas);die;
?>
<form id="main-form" action="#">
  <table id="conducta-table">
    <?php $head = false; ?>
    <?php foreach ($conductas as $alumnoNombre => $data): ?>
      <?php if (!$head): ?>
        <tr>
          <th>Alumno</th>
          <?php foreach (array_keys($data) as $pn): ?>
            <th class="conducta-th">
              <span class="conducta-span"><?php echo $pn; ?></span>
            </th>
          <?php endforeach; ?>
          <?php $head = true; ?>
        </tr>
      <?php endif; ?>
      <tr>
        <td class="alumno-nombre-td"><?php echo $alumnoNombre; ?></td>
        <?php foreach ($data as $td): ?>
          <?php if ($td["logica_periodo_id"] == $logica_periodo_id): ?>
            <td class="conducta-td"><input 
              <?php //$cnd = $td["conducta_id"] ? "[".$td["conducta_id"]."]" : "[]"?>
                name="conducta[<?php echo $td["alumno_id"]; ?>][<?php echo $td["logica_periodo_id"]; ?>:<?php echo $td["conducta_id"]; ?>]"
                onchange="conductaChange($(this));
                    return false;"
                type="text" class="conducta-input" 
                value="<?php echo $td["conducta"]; ?>"/>
            </td>
          <?php else: ?>
            <td class="conducta-td"><input disabled="disabled" type="text" class="conducta-input" value="<?php echo $td["conducta"]; ?>"/></td>
            <?php endif; ?>
          <?php endforeach; ?>
        <?php endforeach; ?>
    <tr>
  </table>
</form>