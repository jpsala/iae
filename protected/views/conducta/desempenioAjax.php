<style>
    .desempenio-tablegen-Desempeño-td input{width:30px}
    #graba{margin:10px 5px 0 0;}
</style>
<?php
        $ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();

$select = "
        select d.id as desempenio_id, ad.id as alumno_division_id, concat(a.apellido, \", \", a.nombre) as Alumno, 
                    d.desempenio as Desempeño
            from alumno_division ad
                inner join alumno a on a.id = ad.Alumno_id
                left join desempenio d on d.alumno_division_id = ad.id
            where ad.division_id = $division_id and ad.ciclo_id = $ciclo_id and a.activo = 1
            order by  a.apellido, a.nombre
        ";
$ds = Helpers::qryAll($select);
//vd($ds);
foreach ($ds as $key => $d) {
    $desempenio_id = $d["desempenio_id"];
    $alumno_division_id = $d["alumno_division_id"];
    $val=$d["Desempeño"];
   // ve($val);
    $ds[$key]["Desempeño"] = "<input 
        name=\"desempenio[$alumno_division_id]\" 
            desempenio_id=\"$desempenio_id\" 
            alumno_division_id=\"$alumno_division_id\"
            value=\"$val\"/>";
}
$cols["master"] = array(
        "desempenio_id" => array("visible" => false),
        "alumno_division_id" => array("visible" => false),
        "Alumno" => array("label" => "Alumno", "width" => 250),
        "Desemeño" => array("label" => "Alumno", "width" => 50),
);
$tbl = Helpers::getTable("desempenio", $ds, $cols, array("title" => false, "filter" => false, "border" => true));
?>
<form onsubmit="return submitForm($(this))">
    <input type="submit" value="Graba" id="Graba"/>
<?php echo $tbl; ?>
</form>
<script>
    function submitForm() {
        var data = $("form").serialize();
        $.ajax({
            type: "POST",
            //      dataType:"json",
            data: data,
            url: "<?php echo $this->createUrl("conducta/desempenioGraba"); ?>",
            success: function(data) {
                console.log(data);
            },
            error: function(data, status) {
            }
        }
        );
        return false;
    }
    ;
</script>
