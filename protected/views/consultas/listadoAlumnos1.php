<?php

$ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();

$rows = Helpers::qryAll("select concat(a.apellido, ', ', a.nombre) as nombre_completo, n.nombre as nivel, 
			 an.nombre as anio, d.nombre as division, a.numero_documento as dni, 
			 a.telefono_1, a.telefono_2, p.calle, p.numero, p.piso, p.departamento, p.telefono_casa, p.telefono_celular, p.telefono_trabajo
	from alumno a
		inner join pariente p on p.id = a.vive_con_id
		inner join alumno_division ad on ad.Alumno_id = a.id and ad.ciclo_id = $ciclo_id
		inner join division d on d.id = ad.Division_id
		inner join anio an on an.id = d.Anio_id
		inner join nivel n on n.id = an.Nivel_id
        where a.activo = 1 and n.id = 4
        order by n.orden, an.nombre, d.nombre
    ");
echo '<head><meta content="text/html; charset=utf-8" http-equiv="Content-type"></head>';
foreach ($rows as $r) {
    foreach ($r as $f) {
        echo $f . ";";
    }
    echo "<br/>";
}

;
?>