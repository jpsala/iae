<?php
/* @var $this PostController */
/* @var $model Destino */
?>
<div class="form">
    <div class="row">
        <select id="Banco" data-placeholder="Puede seleccionar un Banco para modificar" class="chzn-select">
            <?php
            $x = array('prompt' => '');
            echo CHtml::listOptions($model->id, CHtml::listData(Banco::model()->findAll(), 'id', 'nombre'), $x)
            ?>
        </select>
    </div>
    <form id="carga" method="post">
        <input id="id" type="hidden" name="id" value="<?php echo $model->id; ?>"/>
        <div class="row">
            <label for="codigo">Codigo</label>
            <input class="clearinput" id="codigo" type="text" name="codigo" value="<?php echo CHtml::encode($model->codigo); ?>"/>
        </div>
        <div class="row">
            <label for="nombre">Nombre</label>
            <input class="clearinput" id="nombre" type="text" name="nombre" value="<?php echo CHtml::encode($model->nombre); ?>"/>
        </div>

        <div class="botones">
            <input type="submit" name="submit" value="Grabar" id="submit"/>
            <?php if (!$model->getIsNewRecord()): ?>
                <input type="button" name="borrar" value="Borra" id="borra"/>
            <?php endif; ?>
            <input type="button" onclick="return refresh()" name="button" value="Nuevo" id="button"/>
        </div>
    </form>
    <div id="errores">
    </div>
</div>
