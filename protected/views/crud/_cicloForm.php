<?php
/* @var $this PostController */
/* @var $model Destino */
?>
<div class="form">
    <div class="row">
        <select id="Ciclo" data-placeholder="Puede seleccionar un ciclo para modificar" class="chzn-select">
            <?php
            $x = array('prompt' => '');
            echo CHtml::listOptions($model->id, CHtml::listData(Ciclo::model()->findAll(), 'id', 'nombre'), $x)
            ?>
        </select>
    </div>
    <form id="carga" method="post">
        <input id="id" type="hidden" name="id" value="<?php echo $model->id; ?>"/>
        <div class="row">
            <label for="nombre">Nombre</label>
            <input class="clearinput" id="nombre" type="text" name="nombre" value="<?php echo CHtml::encode($model->nombre); ?>"/>
        </div>
        <div class="row">
            <label for="fecha_inicio">Fecha Inicio</label>
            <input id="fecha_inicio" name="fecha_inicio" value="<?php echo date("d/m/Y", mystrtotime($model->fecha_inicio)); ?>" placeholder="Fecha Inicio."/>
        </div>
        <div class="row">
            <label for="fecha_fin">Fecha Fín</label>
            <input id="fecha_fin" name="fecha_fin" value="<?php echo date("d/m/Y", mystrtotime($model->fecha_fin)); ?>" placeholder="Fecha fín."/>
        </div>

        <div class="row">
            <label for="activo">¿ Ciclo Activo ?</label>
            <?php echo CHtml::checkBox("activo", $model->activo);?>
        </div>
        <div class="row">
            <label for="bloqueado">¿ Ciclo Bloqueado ?</label>
            <?php echo CHtml::checkBox("bloqueado", $model->bloqueado);?>
        </div>        
        <div class="botones">
            <input type="submit" name="submit" value="Grabar" id="submit"/>
            <?php if (!$model->getIsNewRecord()): ?>
                <input type="button" name="borrar" value="Borra" id="borra"/>
            <?php endif; ?>
            <input type="button" onclick="return refresh()" name="button" value="Nuevo" id="button"/>
        </div>
    </form>
    <div id="errores">
    </div>
</div>
