<?php
/* @var $this PostController */
/* @var $model Destino */
?>
<div class="form">
    <div class="row">
        <select id="Comprob" data-placeholder="Puede seleccionar un comprobante para modificar" class="chzn-select">
            <?php
            $x = array('prompt' => '');
            echo CHtml::listOptions($model->id, CHtml::listData(Comprob::getComprobante(), 'id', 'nombre_comprob'), $x)
            ?>
        </select>
    </div>
    <form id="carga" method="post">
        <input id="id" type="hidden" name="id" value="<?php echo $model->id; ?>"/>
        <div class="row">
            <label for="nombre">Nombre</label>
            <input class="clearinput" id="nombre" type="text" name="nombre" value="<?php echo CHtml::encode($model->nombre); ?>"/>
        </div>
        <div class="row">
            <label for="abreviacion">Abreviación</label>
            <input class="clearinput" id="abreviacion" type="text" name="abreviacion" value="<?php echo CHtml::encode($model->abreviacion); ?>"/>
        </div>

        <div class="row">
            <label for="compra_venta">¿ Compra - Venta ?</label>
            <select  id="compra_venta" data-placeholder="Seleccione... " class="chzn-select" name="compra_venta" value="<?php echo $model->compra_venta; ?>">
                <?php
                $x = array('prompt' => ''); 
                echo CHtml::listOptions($model->compra_venta, array("C"=>"Compra","V"=>"Venta"), $x)
                ?>
            </select>
        </div>
        
        <div class="row">
            <label for="signo_caja">¿ Signo de Caja ?</label>
            <select  id="signo_caja" data-placeholder="Seleccione... " class="chzn-select" name="signo_caja" value="<?php echo $model->signo_caja; ?>">
                <?php
                $y = array('prompt' => ''); 
                echo CHtml::listOptions($model->signo_caja, array("1"=>"+","-1"=>"-"), $y)
                ?>
            </select>
        </div>
        
        <div class="row">
            <label for="afecta_caja">¿ Afecta caja ?</label>
            <select  id="afecta_caja" data-placeholder="Seleccione... " class="chzn-select" name="afecta_caja" value="<?php echo $model->afecta_caja; ?>">
                <?php
                $y = array('prompt' => ''); 
                echo CHtml::listOptions($model->afecta_caja, array("1"=>"SI","0"=>"NO"), $y)
                ?>
            </select>
        </div>
        
        <div class="row">
            <label for="signo">¿ Signo Caja?</label>
            <select  id="signo" data-placeholder="Seleccione... " class="chzn-select" name="signo" value="<?php echo $model->signo_cc; ?>">
                <?php
                $w = array('prompt' => ''); 
                echo CHtml::listOptions($model->signo_cc, array("1"=>"+","-1"=>"-"), $w)
                ?>
            </select>
        </div>
        
        <div class="botones">
            <input type="submit" name="submit" value="Grabar" id="submit"/>
            <?php if (!$model->getIsNewRecord()): ?>
                <input type="button" name="borrar" value="Borra" id="borra"/>
            <?php endif; ?>
            <input type="button" onclick="return refresh()" name="button" value="Nuevo" id="button"/>
        </div>
    </form>
    <div id="errores">
    </div>
</div>
