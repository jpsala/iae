<?php
/* @var $this PostController */
/* @var $model Destino */
?>
<div class="form">
    <div class="row">
        <select id="Destino" data-placeholder="Puede seleccionar una Destino para modificar" class="chzn-select">
            <?php
            $x = array('prompt' => '');
            echo CHtml::listOptions($model->id, CHtml::listData(Destino::model()->findAll(), 'id', 'nombre'), $x)
            ?>
        </select>
    </div>
    <form id="carga" method="post">
        <input id="id" type="hidden" name="id" value="<?php echo $model->id; ?>"/>
        <div class="row">
            <label for="nombre">Nombre</label>
            <input class="clearinput" id="nombre" type="text" name="nombre" value="<?php echo CHtml::encode($model->nombre); ?>"/>
        </div>
        <div class="row">
            <label for="destino_tipo_id">Tipo de destino</label>
            <select data-allows-new-values="true" id="destino_tipo_id" data-placeholder="Seleccione un tipo de destino" class="chzn-select" name="Destino_Tipo_id" value="<?php echo $model->Destino_Tipo_id; ?>">
                <?php
                $x = array('prompt' => '');
                echo CHtml::listOptions($model->Destino_Tipo_id, CHtml::listData(DestinoTipo::model()->findAll(), 'id', 'nombre'), $x)
                ?>
            </select>
        </div>        
        <div class="botones">
            <input type="submit" name="submit" value="Grabar" id="submit"/>
            <?php if (!$model->getIsNewRecord()): ?>
                <input type="button" name="borrar" value="Borra" id="borra"/>
            <?php endif; ?>
            <input type="button" onclick="return refresh()" name="button" value="Nuevo" id="button"/>
        </div>
    </form>
    <div id="errores">
    </div>
</div>
