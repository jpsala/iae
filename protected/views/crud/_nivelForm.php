<?php
/* @var $this PostController */
/* @var $model Destino */
?>
<div class="form">
    <div class="row">
        <select id="Nivel" data-placeholder="Puede seleccionar un Nivel para modificar" class="chzn-select">
            <?php
            $x = array('prompt' => '');
            echo CHtml::listOptions($model->id, CHtml::listData(Nivel::model()->findAll(), 'id', 'nombre'), $x)
            ?>
        </select>
    </div>
    <form id="carga" method="post">
        <input id="id" type="hidden" name="id" value="<?php echo $model->id; ?>"/>
        <div class="row">
            <label for="nombre">Nombre</label>
            <input class="clearinput" id="nombre" type="text" name="nombre" value="<?php echo CHtml::encode($model->nombre); ?>"/>
        </div>
        <div class="row">
            <label for="orden">Orden</label>
            <input class="clearinput" id="orden" type="text" name="orden" value="<?php echo CHtml::encode($model->orden); ?>"/>
        </div>
        <div class="row">
            <label for="Logica_id">Lógica del Nivel</label>
            <select id="logica_id" data-placeholder="Seleccione una Lógica" class="chzn-select" name="logica_id" value="<?php echo $model->Logica_id; ?>">
                <?php
                $x = array('prompt' => '');
                echo CHtml::listOptions($model->Logica_id, CHtml::listData(Logica::model()->findAll(), 'id', 'nombre'), $x)
                ?>
            </select>
        </div>        
        <div class="botones">
            <input type="submit" name="submit" value="Grabar" id="submit"/>
            <?php if (!$model->getIsNewRecord()): ?>
                <input type="button" name="borrar" value="Borra" id="borra"/>
            <?php endif; ?>
            <input type="button" onclick="return refresh()" name="button" value="Nuevo" id="button"/>
        </div>
    </form>
    <div id="errores">
    </div>
</div>
