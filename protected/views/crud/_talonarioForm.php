<?php
/* @var $this PostController */
/* @var $model Destino */
?>
<div class="form">
    <div class="row">
        <select id="Talonario" data-placeholder="Puede seleccionar un Talonario para modificar" class="chzn-select">
            <?php
            $x = array('prompt' => '');
            echo CHtml::listOptions($model->id, CHtml::listData(Talonario::getTalonario(), 'id', 'nombre_talonario'), $x)
            ?>
        </select>
    </div>
    <form id="carga" method="post">
        <input id="id" type="hidden" name="id" value="<?php echo $model->id; ?>"/>
        <div class="row">
            <label for="Comprob_id">Tipo de Comprobante</label>
            <select  id="Comprob_id" data-placeholder="Seleccione un tipo de comprobante " class="chzn-select" name="Comprob_id" value="<?php echo $model->comprob_id; ?>">
                <?php
                $x = array('prompt' => ''); 
                echo CHtml::listOptions($model->comprob_id, CHtml::listData(Comprob::getComprobante(), 'id', 'nombre_comprob'), $x)
                ?>
            </select>
        </div>
        <div class="row">
            <label for="numero">Número</label>
            <input class="clearinput" id="numero" type="text" name="numero" value="<?php echo CHtml::encode($model->numero); ?>"/>
        </div>
        <div class="row">
            <label for="punto_venta">Punto Venta</label>
            <input class="clearinput" id="punto_venta" type="text" name="punto_venta" value="<?php echo CHtml::encode($model->punto_venta_numero); ?>"/>
        </div>
        <div class="row">
            <label for="letra">Letra</label>
            <input class="clearinput" id="letra" type="text" name="letra" value="<?php echo CHtml::encode($model->letra); ?>"/>
        </div>
        <div class="row">
            <label for="manual">¿ Numeración manual ?</label>
            <?php echo CHtml::checkBox("manual", $model->numeracion_manual);?>
        </div>
        <div class="row">
            <label for="activo">¿ Talonario activo ?</label>
            <?php echo CHtml::checkBox("activo", $model->activo);?>
        </div>
        <div class="botones">
            <input type="submit" name="submit" value="Grabar" id="submit"/>
            <?php if (!$model->getIsNewRecord()): ?>
                <input type="button" name="borrar" value="Borra" id="borra"/>
            <?php endif; ?>
            <input type="button" onclick="return refresh()" name="button" value="Nuevo" id="button"/>
        </div>
    </form>
    <div id="errores">
    </div>
</div>
