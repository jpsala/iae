<?php
/* @var $this PostController */
/* @var $model Destino */
?>
<div class="form">
    <div class="row">
        <select id="TipoDoc" data-placeholder="Seleccione Tipo de Documento para modificar" class="chzn-select">
            <?php
            $x = array('prompt' => '');
            echo CHtml::listOptions($model->id, CHtml::listData(TipoDocumento::model()->findAll(), 'id', 'nombre'), $x)
            ?>
        </select>
    </div>
    <form id="carga" method="post">
        <input id="id" type="hidden" name="id" value="<?php echo $model->id; ?>"/>
        <div class="row">
            <label for="nombre_corto">Nombre Corto</label>
            <input class="clearinput" id="nombre_corto" type="text" name="nombre_corto" value="<?php echo CHtml::encode($model->nombre_corto); ?>"/>
        </div>
        <div class="row">
            <label for="nombre">Nombre</label>
            <input class="clearinput" id="nombre" type="text" name="nombre" value="<?php echo CHtml::encode($model->nombre); ?>"/>
        </div>

        <div class="botones">
            <input type="submit" name="submit" value="Grabar" id="submit"/>
            <?php if (!$model->getIsNewRecord()): ?>
                <input type="button" name="borrar" value="Borra" id="borra"/>
            <?php endif; ?>
            <input type="button" onclick="return refresh()" name="button" value="Nuevo" id="button"/>
        </div>
    </form>
    <div id="errores">
    </div>
</div>
