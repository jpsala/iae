<style  type="text/css">
    label{float: left; width: 160px;line-height: 2em;    margin-right: 14px; text-align: right; padding-top: 4px;}
    #errores{border: 1px #09f solid; color:#09f; background-color: #f2dada; display: none; padding: 10px; margin: 0 auto; width: 500px; text-align: center}
    form{margin: 10px 0px 10px; border: 1px #75C2F8 solid; padding: 19px 20px 11px 27px;  border-radius: 8px; border-shadow: 5px}
    .botones{text-align: right} 
    #Destino, #nombre, #Tarjeta {width: 350px}
    #editorial, #genero, #destino_tipo_id, #donante{width: 300px}
    input[type=text] {
        color: #444;
        letter-spacing: 1px;
        word-spacing: 2px; 
        padding: 6px; 
    }   
    .ui-widget { font-size: 12px; }
</style>

<?php $this->breadcrumbs = array(Articulo::label(2),); ?>

<div id="_form">
    <?php include "_tarjetaForm.php"; ?>
</div>

<script type="text/javascript">
    
    init();
    

    function init(){
        $("button, #submit, #borra").button();
        $(".chzn-select").chosen();
        $(".clearinput").clearinput();
        $("#nombre").focus();
        
        $("#Tarjeta").chosen().change(function(){
            ajax($(this).val());
        });
        
        $('#borra').click(function(){
            if(!confirm("¿Borra esta Tarjeta?"))
                return;
            
            $.ajax({
                type: "GET",
                data: {'id':$('#id').val()},
                url: "<?php echo $this->createUrl('Crud/tarjetaBorra'); ?>",
                success: function(data) {
                    refresh();
                }
            });  
        })
        
        $('form').submit(function(){
            $.ajax({
                type: "POST",
                data: $('form').serialize(),
                dataType:'json',
                url: "<?php echo $this->createUrl('crud/tarjetaGraba'); ?>",
                success: function(data) {
                    if(data){
                        $('#errores').html("");
                        for(var i in data)
                        {
                            $('#errores').append(data[i][0]+"<br/>");
                        }
                        $('#errores').show();
                    }else{
                        refresh();
                    }
                }
            });
            return false;
        });

    }
    function refresh(){
        ajax(null);
        return false;
    }
    function ajax(id){
        $.ajax({
            type: "GET",
            data: {"id":id},
            url: "<?php echo $this->createUrl("crud/tarjetaForm"); ?>",
            success: function(data) {
                $("#_form").html(data);
            },
            error: function(data,status){
            },
            complete: function(){
                init();
            }
        });
    }    
</script>

