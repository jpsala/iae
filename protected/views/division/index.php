<style type="text/css">
    table.divisiones td:nth-child(1){width:100px}
    table.divisiones td:nth-child(2){width:180px}
    table.divisiones td:nth-child(3){width:80px}
    table.divisiones td:nth-child(4){width:100px}
    table.divisiones td:nth-child(5){width:180px}
    table.divisiones td:nth-child(6){width:180px}
    table tr{cursor: pointer}
    table tr{cursor: pointer}
    table tr:hover{background-color: lightblue}
</style>
<?php
$ds = Helpers::qryAll("
    select d.id, n.nombre as nivel, a.nombre as anio, d.nombre, a.id as anio_id
        from division d
            inner join anio a on a.id = d.anio_id
            inner join nivel n on n.id = a.nivel_id
        where n.activo = 1 and a.activo = 1 and d.activo =1
        order by n.orden, a.orden, d.nombre");
?>
<div id="divisiones-dialog">
    <table class="divisiones">
        <thead>
            <tr>
                <th>Nivel</th>
                <th>Anio</th>
                <th>Division</th>
                <th></th>
            </tr>
        </thead>
        <?php foreach ($ds as $ds): ?>
            <tr 
                anio_id="<?php echo $ds["anio_id"]; ?>" 
                division_id="<?php echo $ds["id"]; ?>" 
                onclick="return seleccionaDivision($(this));">
                <td><?php echo $ds["nivel"]; ?></td>
                <td><?php echo $ds["anio"]; ?></td>
                <td><?php echo $ds["nombre"]; ?></td>
            </tr>
        <?php endforeach; ?>
        <tr 
            division_id="-1"
            onclick="return seleccionaDivision($(this));">
            <td colspan="3">Desasigna</td>
        </tr>
    </table>
</div>
<?php
$ds = Helpers::qryAll("
    select d.id, n.nombre as nivel, a.nombre as anio, d.nombre, 
                a.siguiente_anio_id, a2.nombre as nombre_siguiente_anio, 
                n2.id as nivel_id_siguiente, n2.nombre as nombre_siguiente_nivel, 
                d.division_id_siguiente, d2.nombre as nombre_siguiente_division, 
                a.siguiente_anio_id as siguiente_anio_id_1
        from division d
            inner join anio a on a.id = d.anio_id
            left join nivel n on n.id = a.Nivel_id
            left join division d2 on d2.id = d.division_id_siguiente
            left join anio a2 on a2.id = d2.anio_id
            left join nivel n2 on n2.id = a2.Nivel_id
        where n.activo = 1 and a.activo = 1 and d.activo =1
        order by n.orden, a.orden, d.nombre");
?>
<table class="divisiones">
    <thead>
        <tr>
            <th>Nivel</th>
            <th>Anio</th>
            <th>Division</th>
            <th>Siguiente Nivel</th>
            <th>Siguiente Anio</th>
            <th>Siguiente Division</th>
            <th></th>
        </tr>
    </thead>
    <?php foreach ($ds as $ds): ?>
        <tr
            id="<?php echo $ds["id"]; ?>" 
            siguiente_id="<?php echo $ds["division_id_siguiente"]; ?>"
            siguiente_anio_id_1="<?php echo $ds["siguiente_anio_id_1"]; ?>"
            onclick="return siguiente($(this));">
            <td><?php echo $ds["nivel"]; ?></td>
            <td><?php echo $ds["anio"]; ?></td>
            <td><?php echo $ds["nombre"]; ?></td>
            <td><?php echo $ds["nombre_siguiente_nivel"]; ?></td>
            <td><?php echo $ds["nombre_siguiente_anio"]; ?></td>
            <td><?php echo $ds["nombre_siguiente_division"]; ?></td>
        </tr>
    <?php endforeach; ?>
</table>

<script type="text/javascript">
                var division_id;
                $("#divisiones-dialog").dialog({
                    autoOpen: false,
                    height: "300",
                    width: "auto",
                    position: "right top+20",
                    title: "Haga click en una división para seleccionarla",
                    buttons: {
                        "Cancel": function() {
                            $(this).dialog("close");
                        }
                    }
                });
                function siguiente($this) {
                    var siguiente_anio_id = $this.attr("siguiente_anio_id_1");
                    division_id = $this.attr("id");
                    $("#divisiones-dialog table tr").hide();
                    $("#divisiones-dialog table tr[anio_id=" + siguiente_anio_id + "]").show();
                    $("#divisiones-dialog table tr[division_id=-1]").show();
                    $dlg = $("#divisiones-dialog");
                    $dlg.dialog("open");
                    return false;
                }
                function seleccionaDivision($this) {
                    $("#divisiones-dialog").dialog("close");
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        data: {division_id: division_id, siguiente_division_id: $this.attr("division_id")},
                        url: "<?php echo $this->createUrl("division/grabaSiguiente"); ?>",
                        success: function(data) {
                            $division = $("#" + division_id);
                            $division.find("td:nth-child(4)").html(data.nivel);
                            $division.find("td:nth-child(5)").html(data.anio);
                            $division.find("td:nth-child(6)").html(data.division);
                        },
                        error: function(data, status) {
                        }
                    }
                    );
                }
</script>