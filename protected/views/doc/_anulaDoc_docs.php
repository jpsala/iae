<div id="comprob_tipo" class="titulo">
  <?php echo $comprobTipoNombre; ?>
</div>
<table id="docs-table">
  <?php foreach ($rows as $row): ?>
    <tr doc_id="<?php echo $row["id"];?>">
      <td class=""><?php echo $row["matricula"]; ?></td>
      <td class=""><?php echo $row["nombre"]; ?></td>
      <td class=""><?php echo date("d/m/Y", mystrtotime($row["fecha_creacion"])  ); ?></td>
      <td class=""><?php echo $row["numero"]; ?></td>
      <td class=""><?php echo $row["detalle"]; ?></td>
      <td class="importe"><?php echo number_format($row["total"], 2); ?></td>
      <td class="importe"><?php echo number_format($row["saldo"], 2); ?></td>
      <td class="imprimir-a-td"><a class="imprimir-a" onclick="imprimeDoc($(this));
            return false;">Imprimir</a></td>
      <td class="anular-a-td"><a class="anular-a" onclick="anulaDoc($(this));
            return false;">Anular</a></td>
    </tr>
  <?php endforeach; ?>
</table>

<script type="text/javascript">
</script>
