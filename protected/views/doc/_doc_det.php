<style type="text/css">
  .doc-dets-detalle{min-width: 250px}
  #doc-dets-table .importe{text-align: right}
</style>

<table id="doc-dets-table">
  <tr>
    <th class="">Detalle</th>
<!--    <th class="importe">Cantidad</th>
    <th class="importe">Importe</th>-->
    <th class="importe">Total</th>
  </tr>
  <?php $total=0; ?>
  <?php foreach ($dets as $det): ?>
  <tr doc-det-id="<?php echo $det->id; ?>">
    <td class="doc-dets-detalle"><?php echo $det->articulo->nombre; ?></td>
<!--    <td class="importe doc-dets-cantidad"><?php //echo $det->cantidad; ?></td>
    <td class="importe doc-dets-importe"><?php //echo $det->importe; ?></td>-->
    <td class="importe doc-dets-total"><?php echo number_format($det->total,2); ?></td>
      <?php $total+=$det->total; ?>
  </tr>
  <?php endforeach; ?>
  <tr>
    <th colspan="4" class="importe"><?php echo number_format($total,2); ?></th>
  </tr>
</table>
