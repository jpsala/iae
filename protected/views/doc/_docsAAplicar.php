<table>
  <thead>
    <tr>
      <th>Tipo</th>
      <th>Número</th>
      <th class="detalle">Detalle</th>
      <th class="right-align">Fecha</th>
      <th class="right-align">Total</th>
      <th class="right-align">Saldo</th>
      <th class="right-align">Pago</th>
    </tr>
  </thead>
  <?php $totalPagos = 0; ?>
  <?php $totalAPagar = 0; ?>
  <?php foreach ($ctas as $doc): ?>
    <tr class="docs-a-pagar" id="<?php echo $doc["id"]; ?>" fecha-vto="<?php echo $doc['fecha_vto'];?>">
      <td><?php echo $doc["comprob"]; ?></td>
      <td><?php echo sprintf('%0' . (int) 4 . 's', $doc["sucursal"]) . "-" . sprintf('%0' . (int) 8 . 's', $doc["numero"]);; ?></td>
        <?php if($doc["cantDetalles"]==0){
          $detalle = $doc["detalle"];
          $tdClass="";
        }else{
          $tdClass=" tiene-cuotas ";
          $detalle = "<input class=\"pago\" type='button' value='".$doc["detalle"]."' tabindex=\"-1\"/>";
        } ?>
      <td class="<?php echo $tdClass;?>  detalle-td"><?php echo $detalle; ?></td>
      <td class="right-align"><?php echo date("d/m/Y", mystrtotime($doc["fecha_creacion"])); ?></td>
      <td class="right-align"><?php echo sprintf("%.2f", $doc["total"]); ?></td>
      <td class="right-align saldo"><?php echo sprintf("%.2f", $doc["saldo"]); ?></td>
      <td class="right-align pago">
        <input class="pago pagodoc" <?php echo ($doc_id == "null" or $doc_id == "") ? "" : "DdISABLED=\"DISABLED\""; ?> 
          saldo="<?php echo sprintf("%.2f", $doc["saldo"]); ?>"  
          name='aplicaciones[<?php echo $doc["id"]; ?>]' 
          value="<?php echo sprintf("%.2f", $doc["aplicacion"]); ?>"
          /></td>
    </tr>
    <?php $totalPagos += round($doc["aplicacion"], 2); ?>
    <?php //$totalAPagar += ($doc_id ? round($doc["saldo"],2) : round($doc["saldo"],2)); ?>
    <?php $totalAPagar += round($doc["saldo"], 2); ?>
  <?php endforeach; ?>
</table>

<input type="hidden" id="obs-cobranza-input" value="<?php echo isset($obsCobranza)?$obsCobranza:""; ?>"/>

<script type="text/javascript">
  doc.totalPagos = round(<?php echo $totalPagos; ?>,2);
  doc.totalAPagar = round(<?php echo $totalAPagar; ?>,2);
  matricula = "<?php echo isset($matricula)?$matricula:''; ?>";
  $(".detalle-td input").button();
  $(".pago input").autoNumeric("init",{mDec:2,aSep: ''}).clearinput();
</script>
