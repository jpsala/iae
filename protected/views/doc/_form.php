<style type="text/css">
    #doc-div{ border: #EFEFEF solid 1px; margin: 0 auto; min-height: 450px; border-radius: 8px; width: 85%; padding-left: 8px}
    #doc-div #top{overflow: auto}
    #doc-div #top #col1, #col2 {float: left;}
    #doc-div #pv{width: 35px}
    #doc-div #numero{width: 65px}
    #errores{border: 1px #09f solid; color:#09f; background-color: #f2dada; display: none; padding: 10px; margin: 0 auto; width: 500px; text-align: center}
    #doc-div form{margin: 5px 0px 5px; border: 1px #75C2F8 solid; padding: 5px 5px 5px 5px;  border-radius: 8px; border-shadow: 5px}
    #doc-div form label{float: left; width: 100px;line-height: 2em;    margin-right: 14px; text-align: right; padding-top: 4px;}
    .botones{text-align: right} 
    #Destino, #nombre{width: 350px}
    #editorial, #genero, #destino_tipo_id, #donante{width: 300px}
    input[type=text] {
        color: #444;
        letter-spacing: 1px;
        word-spacing: 2px; 
        padding: 6px; 
    }   
    .ui-widget { font-size: 12px; }    
</style>
<?php
/* @var $this PostController */
/* @var $model Doc */
?>
<div id="doc-div">
    <div class="form">
        <form method="POST" action="#">
            <input id="socio_id" type="hidden" value="<?php $model->Socio_id; ?>"/>
            <div id="top">
                <div id="col1">
                </div>
                <div id="col2">
                    <?php $numeroDisabled = $model->comprobPuntoVenta->comprob->numeracion_manual ? "" : 'disabled="disabled"'; ?>
                    <div class="row">
                        <label for="numero">Número </label>
                        <input id="pv" name="pv" value="<?php echo $model->comprobPuntoVenta->numero; ?>" <?php echo $numeroDisabled; ?>/>
                        <input id="numero" name="numero" value="<?php echo $model->numero; ?>" <?php echo $numeroDisabled; ?>/>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="botones clear" >
        <input type="submit" name="submit" value="Grabar" id="submit"/>
        <?php if (!$model->getIsNewRecord()): ?>
            <input type="button" name="borrar" value="Borra" id="borra"/>
        <?php endif; ?>
        <button onclick="return refresh()">Nuevo</button>
    </div>
    <div id="errores">
    </div>
</div>
</div>
