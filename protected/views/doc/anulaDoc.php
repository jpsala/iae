<style type="text/css">
    #comprob_tipo_id{width:250px}
    #comprob-tipo-id-div{display:inline-block}
    #filtro-fecha-div{ display: inline-block; vertical-align: top;margin-left: 15px}
    #trae-docs-button-div{ display: inline-block;vertical-align: top;margin-top: -2px;}
    #docs-table a{cursor:pointer; color: blue; text-decoration: none}
    .imprimir-a-td, .anular-a-td{width:38px}
    #filtro-fecha-div .label{vertical-align: middle}
</style>
<div id="filtro">
    <div id="comprob-tipo-id-div">
        <?php $arr = array("data-placeholder" => "Seleccione un tipo de documento...", "prompt" => ""); ?>
        <?php $data = Comprob::listData(); ?>
        <?php echo CHtml::dropDownList("comprob_tipo_id", Null, $data, $arr); ?>
    </div>
    <div id="filtro-fecha-div">
        <span class="label">Desde:</span><input type="text" id="filtro-fecha-desde" value="<?php echo date('d/m/Y', time() - 30 * 24 * 3600); ?>"/>
        <span class="label">Hasta:</span><input type="text" id="filtro-fecha-hasta" value="<?php echo date('d/m/Y', time()); ?>"/>
    </div>
    <div id="trae-docs-button-div"><button id="trae-docs-button" onclick="traeDocs();
            return false;">Trae Docs.</button></div>
    <div id="docs">
    </div>
</div>

<script type="text/javascript">

        $("#filtro-fecha-desde,#filtro-fecha-hasta").datepicker();

        $("#trae-docs-button").button();

        $("#comprob_tipo_id").chosen().change(function() {
        });

        function imprimeDoc($this) {
            $.ajax({
                type: "GET",
                //dataType: "json",
                data: {doc_id: $this.closest("tr").attr("doc_id")},
                url: "<?php echo $this->createUrl("doc/anulaDoc"); ?>",
                success: function(data) {
                    ;
                    //console.log(data);
                    //window.location = window.location;
                },
                error: function(data, status) {
                }
            });

        }

        function anulaDoc($this, force) {
            $.ajax({
                type: "GET",
                dataType: "json",
                data: {doc_id: $this.closest("tr").attr("doc_id"), force: force},
                url: "<?php echo $this->createUrl("doc/anulaDoc"); ?>",
                success: function(data) {
                    if (data.error) {
                        alert(data.error);
                        return;
                    }
                    if (force) {
                        alert("El documento fué anulado");
                        $this.closest("tr").remove();
                        return;
                    }
                    if (data.aplicaciones && !confirm("El documento tiene aplicaciones, confirma la anulación?")) {
                        return;
                    }
                    if (data.cajaCerrada && !confirm("El documento pertenece a una caja cerrada, confirma la anulación?")) {
                        return;
                    }
                    if (data.cajaCerrada) {
                        alert("Por ahora no se puede anular un documento con la caja cerrada, lo hacemos nosotros!");
                        return;
                    }
                    anulaDoc($this, true);
                },
                error: function(data, status) {
                }
            });
        }

        function traeDocs() {
            comprob_tipo_id = $("#comprob_tipo_id").val();
            if (!comprob_tipo_id) {
                alert("Especifique el tipo de documento a buscar");
                return;
            }
            $.ajax({
                type: "POST",
                //dataType: "json",
                data: {
                    comprob_tipo_id: comprob_tipo_id,
                    fecha_desde: $("#filtro-fecha-desde").val(),
                    fecha_hasta: $("#filtro-fecha-hasta").val()
                },
                url: "<?php echo $this->createUrl("doc/anulaDocGetDocs"); ?>",
                success: function(data) {
                    $("#docs").html(data);
                    //console.log(data);
                    //window.location = window.location;
                },
                error: function(data, status) {
                }
            });
        }

</script>