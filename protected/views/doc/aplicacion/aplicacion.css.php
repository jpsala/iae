<style type="text/css">
  labelHighlight{color: #AAA; font-style: italic;}
  #aplicacion-content{width: 100%}
  #izq,#der{height: 600px;overflow: auto}
  #izq{ width: 48.5%;; float: left; }
  #der{   width: 50.5%;  float: right}
  #botones{ clear: both;margin:11px 0px 2px 11px; position: relative; text-align: right; top: 0;}
  table *{font-size: 12px}
  tr:nth-child(even)    { background-color:#edf1fe}
  .comprob{}
  .ui-widget { font-size: 12px; }  
  .bold{font-weight: bold}
  .oculto{display: none}
  .total{background-image: url("images/pesos.gif");background-position: 0 50%;background-repeat: no-repeat;background-size: 10px auto;padding-left: 15px;}
  .collapse-expand-cell{width:10px;padding: 0px 3px;}
  .collapsed{
    background-image: url("images/collapsed.png");
    background-position: 0 50%;
    background-repeat: no-repeat;
    background-size: 20px 10;
    padding: 0 9px 0 5px;
    cursor:pointer;}
  .expanded{
    background-image: url("images/expanded.png");
    background-position: 0 50%;
    background-repeat: no-repeat;
    background-size: 20px 10;
    padding: 0 9px 0 5px;
    cursor:pointer}
  .importe,.saldo{text-align: right; width: 45px}
  .aplicaciones-div{margin: -8px; display:none;}
  table.aplicaciones{padding: 8px 8px 0px;margin-bottom: 8px !important; }
  table.aplicaciones th, table.aplicaciones td, table.aplicaciones caption {
    padding: 0px 0px 4px 0px !important;
  }
  .selected{background-color: lightskyblue !important}
  .doc-helper{
    padding:10px;
  }
  #datos-socio{padding:10px;overflow: auto;position: relative}
  .docs-tr{cursor: pointer}
  .desaplica-td img{ cursor: pointer;height: 12px;margin-left: 5px;}
  #buttons-div{    display: inline-block;
    float: right;
    height: 28px;
    margin: 0;
    position: absolute;
    right: 4px;
    text-align: right;
    top: 15px;}
  input::-webkit-input-placeholder, textarea::-webkit-input-placeholder {
    color: #636363;
  }
  input:-moz-placeholder, textarea:-moz-placeholder {
    color: #636363;
  }
  .edit{background:url("images/ok.gif")  no-repeat fixed white }
  #filtro-div{display:block;margin-top: 2px;}
  #filtro-div input{vertical-align: bottom;vertical-align: middle;}
  #busqueda{margin: 0 0 0 22px;}
  #busqueda-btn{height: 26px;width: 29px;}
  .encontrado{color:red}
  .fecha{text-align: right; width: 35px}
  #docs-table tr td,#creditos-table tr td{padding:5px 3px 5px 3px}
</style>