<script type="text/javascript">
  <?php /* @var $this Controller */; ?>
  <?php
  if ($socio) {
      $socioTipo = $socio->Alumno_id ? "alumno" : "proveedor";
  };
  ?>
  //http://localhost/iae/index.php?r=doc/aplicacion&socio_id=26069&comprobTipo=recibo
  var $source = null, $target, importeAnt;
  var socio_id = "<?php echo $socio ? $socio->id : ''; ?>";
  var socioTipo = "<?php echo $socioTipo; ?>";
  var urlAC = "<?php echo $this->createUrl('socio/sociosAc'); ?>&socioTipo=" + socioTipo;
  var vieneDeDoc = "<?php echo $vieneDeDoc; ?>";
  var url = "<?php echo $this->createAbsoluteUrl('doc/aplicacion'); ?>";
  var comprobTipo = "<?php echo $comprobTipo; ?>";

  function collapseExpand(obj) {
    var $obj = $(obj);
    var doc_id = $obj.closest("tr").attr("id");
    var $aplicacionesTable;
    var expanded = $obj.css("background-image").indexOf("expanded") !== -1;
    if ($obj.hasClass("all")) {
      if (!expanded) {
        $obj.removeClass("collapsed");
        $obj.addClass("expanded");
        $(".collapse-expand-cell span").removeClass("collapsed");
        $(".collapse-expand-cell span").addClass("expanded");
        $(".aplicaciones-div").show();
      } else {
        $obj.removeClass("expanded");
        $obj.addClass("collapsed");
        $(".collapse-expand-cell span").removeClass("expanded");
        $(".collapse-expand-cell span").addClass("collapsed");
        $(".aplicaciones-div").hide();
      }
    } else {
      $aplicacionesTable = $('.aplicaciones-div[parent_id="' + doc_id + '"]');
      if (expanded) {
        $obj.removeClass("expanded");
        $obj.addClass("collapsed");
        $aplicacionesTable.hide();
      } else {
        $obj.removeClass("collapsed");
        $obj.addClass("expanded");
        $aplicacionesTable.show();
      }
      if (expanded) {
        $(".all").removeClass("expanded");
        $(".all").addClass("collapsed");
      } else {
        $(".all").removeClass("collapsed");
        $(".all").addClass("expanded");
      }
    }

  }

  $(function () {

    $('#socio').focus().autocomplete({
      autoSelect: true, autoFocus: true,
      minLength: 2, source: urlAC,
      select: function (event, ui) {
        socio_id = ui.item.id;
        window.location = "<?php echo $this->createUrl("doc/aplicacion&socio_id="); ?>" + socio_id;
      }
    });

    $(".docs tr").hover(
      function () {
        $tr = $(this).closest("tr");
        doc_id = $tr.attr("id");
        $aplicaciones = $('.aplicaciones [doc_orig_id="' + doc_id + '"]');
        $aplicaciones.each(function () {
          $(this).addClass("selected");
          if (!$(this).is(":visible")) {
            var $obj = $(this).closest("div").closest("tr").prev("tr").children("td").first().find("span");
            var collapsed = $obj.css("background-image").indexOf("expanded") !== -1;
            if (!collapsed) {
              $obj.removeClass("collapsed");
              $obj.addClass("expanded");
            }

            $(this).closest("div").show();
          }
        });
      },
      function () {
        $tr = $(this).closest("tr");
        doc_id = $tr.attr("id");
        $('.aplicaciones [doc_orig_id="' + doc_id + '"]').removeClass("selected");
      });

    $(".aplicaciones tr").hover(
      function () {
        $tr = $(this).closest("tr");
        doc_origen = $tr.attr("doc_orig_id");
        $origen = $(".docs #" + doc_origen);
        if ($origen.length) {
          $origen.addClass("selected");
        }
      },
      function () {
        $tr = $(this).closest("tr");
        doc_origen = $tr.attr("doc_orig_id");
        $origen = $(".docs #" + doc_origen);
        if ($origen.length) {
          $origen.removeClass("selected");
        }
      });

    $('.draggable').draggable({
      helper: function (event) {
        $source = $(event.currentTarget);
        return $("<div class='doc-helper ui-widget-content ui-corner-all'>" +
          $source.find(".comprob").html() +
          "</div>").first();
      },
      drag: function (event, ui) {
        $source = $(event.target);
        saldo = Number($source.find(".saldo").html());
        if (saldo == 0) {
          return false
        } else {
          return true;
        }
      },
      start: function (event, ui) {
      }
    });

    $(".droppable").droppable({
      //    tolerance: 'touch',
      accept: ".draggable",
      //activeClass: "ui-state-hover",
      hoverClass: "ui-state-active",
      drop: function (event, ui) {
        $source = $(ui.draggable);
        $target = $(event.target);
        //$("#aplicacion-dialog").find("input").val($source.find(".saldo").html());
        //        $("#aplicacion-dialog").dialog("open");
        aplica($source, $target, $source.find(".saldo").html());
      }
    });

    $("button").button();

    $("#graba").button("disable");

    $("#busqueda-btn").button({icons: {primary: 'ui-icon-check'}});

  });

  function aplicaAuto() {
    $docs = $(".docs-tr");
    $docs.each(function () {
      $source = $(this);
      $docsDest = $(".droppable");
      $docsDest.each(function () {
        aplica($source, $(this), $source.find(".saldo").html());
      });
    });
  }

  function importeFocus(obj) {
    importeAnt = Number($(obj).val());
  }

  function importeChange(obj) {
    var $obj = $(obj);
    var doc_id = $obj.closest("tr").attr("doc_id");
    var $saldo = $("#" + doc_id).find(".saldo");
    var saldo = Number($saldo.html());
    $saldo.html(round(saldo + importeAnt), 2);
    var saldo = Number($saldo.html());
    var importe = Number($obj.val());
    var $aplicacionesDiv = $obj.closest(".aplicaciones-div");
    var parentId = $aplicacionesDiv.attr("parent_id");
    var $saldoACancelar = $("#" + parentId).find(".saldo");
    var saldoACancelar = Number($saldoACancelar.html());
    $saldoACancelar.html(saldoACancelar + importeAnt);
    var saldoACancelar = Number($saldoACancelar.html());
    if ((importe) > saldoACancelar) {
      importe = saldoACancelar;
      $obj.val(importe);
    }
    $saldoACancelar.html(round(saldoACancelar - importe, 2));
    if ((saldoACancelar + importeAnt - importe) < 0) {
      $obj.val(importeAnt);
      $obj.focus();
      alert("El importe es mayor al saldo del comprobante, verifique");
      return;
    }
    $saldo.html(saldo - importe);
    importeAnt = importe;
  }

  function cancelaAplicacion() {

  }

  function grabaAplicacion() {
    //@todo: subir los borrados también
    var data = $("form").serialize();
    $(".aplicaciones tr").each(function () {
      if ($(this).attr("borrado")) {
        data += "&borrar[]=" + $(this).attr("doc_apl_id");
      }
    });
    $.ajax({
      type: "POST",
      //      dataType:"json",
      data: data,
      url: "<?php echo $this->createUrl("doc/aplicacionGraba"); ?>",
      success: function (data) {
        if (data) {
          alerta(data);
          return;
        }
        if (comprobTipo) {
          vuelveAlDoc();
        } else {
          window.location = "<?php echo $this->createUrl("doc/aplicacion"); ?>" + "&socioTipo=" + "<?php echo $socioTipo; ?>";
        }
      },
      error: function (data, status) {
      }
    });
  }

  function desaplica(obj) {
    $("#graba").button("enable");
    $tr = $(obj).closest("tr");
    esAplicaciones = $tr.closest("table").hasClass("aplicaciones");
    if (esAplicaciones) {
      desaplicaUno($tr);
    } else {
      doc_id = $tr.attr("id");
      $aplicaciones = $('.aplicaciones [doc_orig_id="' + doc_id + '"]');
      $aplicaciones.each(function () {
        desaplicaUno($(this));
      })
    }
  }

  function desaplicaUno($tr) {
    if (!($tr).is(":visible")) {
      return;
    }
    doc_apl_id = $tr.attr("doc_apl_id");
    doc_origen = $tr.attr("doc_orig_id");
    $origen = $(".docs #" + doc_origen);
    if ($origen.length) {
      parent = $tr.closest("div").attr("parent_id");
      $saldo = $("#" + parent).find(".saldo");
      saldo = Number($saldo.html());
      $saldoOrigen = $origen.find(".saldo");
      saldoOrigen = Number($saldoOrigen.html());
      $importe = $tr.find(".importe");
      importe = Number($importe.html());
      $saldoOrigen.html(saldoOrigen + importe);
      $saldo.html(saldo + importe);
      $trTotal = $tr.closest("table").find("tr").last();
      $total = $trTotal.find(".importe");
      total = Number($total.html().replace(",", ""));
      $total.html(total - importe);
      total = Number($total.html())
      if (total == 0) {
        //                  $total.closest("tr").remove();
      }
      $tr.attr("borrado", true);
      $tr.hide();

    }
  }

  function filtro($obj) {
    var todo = "&socio_id=" + socio_id;
    if ($obj.is(':checked')) {
      todo += "&todo=1";
    }
    window.location = url + todo;
  }

  function vuelveAlDoc() {
    window.location = "<?php echo $this->createUrl("doc/$comprobTipo"); ?>" + "&socio_id=" + socio_id;
  }

  function cancelaAplicacion() {
    vuelveAlDoc();
    console.log("<?php echo $this->createUrl("doc/$comprobTipo"); ?>" + "&socio_id=" + socio_id);
    return false;
    ;
    window.location = "<?php echo $this->createUrl("doc/aplicacion"); ?>" + "&socioTipo=" + socioTipo;
  }

  function busqueda($obj) {
    var s = $('#busqueda').val().toUpperCase();
    $(".searchable").removeClass("encontrado");
    $(".searchable").filter(function () {
      txt = $(this).text().toUpperCase();
      if (txt.toUpperCase().indexOf(s) >= 0) {
        $(this).addClass("encontrado")
        $tr = $(this).closest("tr")
        if (!$tr.is(":visible")) {
          id = $tr.attr("doc_orig_id");
          $tr.closest(".aplicaciones-div").show();
        }
      }
    })
  }

  function aplica($source, $target, importe) {
    var $saldoACancelar = $("#" + $target.attr("id")).find(".saldo");
    var saldoACancelar = Number($saldoACancelar.html());
    if (saldoACancelar == 0) {
      return;
    }
    id = $target.attr("id");
    $appTable = $('.aplicaciones-div[parent_id="' + id + '"]').find("table");
    $lastTr = $appTable.find(".importe").last().parent();
    comprob = $source.find(".comprob").html();
    doc_id = $source.attr("id");
    fecha_creacion = $source.find(".fecha_creacion").html();
    saldo = $source.find(".saldo").html();
    //importeAnt = importe;
    importeAnt = 0;
    //    $source.find(".saldo").html(saldo - importe);
    $obj = $lastTr.before("\n\
      <tr doc_id='" + doc_id + "'>\n\
        <td>" + comprob + "</td>\n\
        <td>" + fecha_creacion + "</td>\n\
        <td colspan='2' class='importe'>\n\
          <input class='importe edit numeric' type='text' \n\
            name='importe[" + doc_id + "][" + id + "]' \n\
            value='" + importe + "'\n\
            onFocus='importeFocus(this) '\n\
            onchange='importeChange(this)'/>\n\
        </td>\
      </tr>\n\
    ");
    $appTable.parent().show();
    $lastTr.prev().find("input").autoNumeric("init", {mDec: 2, aSep: ''});
    importeChange($lastTr.prev().find("input"));
    $("#graba").button("enable");
  }

</script>