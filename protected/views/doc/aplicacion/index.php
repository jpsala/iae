<?php
include("aplicacion.css.php");
include("aplicacion.js.php");
?>
<div id="aplicacion-content" class="ui-widget">
    <div id="datos-socio" class="ui-widget-content ui-corner-all">
        <?php if ($socio): ?>
            <?php echo "Cliente: " . $socio->nombre; ?>
        <?php else: ?>
            <div id="div-socio">
                <!--            <label for="socio">Alumno</label>-->
                <input name="socio_id" id="socio" value="" placeholder="<?php echo $socioTipo; ?>"/>
            </div>
        <?php endif; ?>

        <div id="filtro-div">
            <?php $checked = $todo ? "checked" : ""; ?>
            <label id="filtro-label" for="filtro">Traer comprobantes sin saldo</label>
            <input name="filtro" id="filtro" type="checkbox" onchange="filtro($(this))"  <?php echo $checked; ?>/>
            <label id="busqueda-label" for="busqueda"></label>
            <input name="busqueda" id="busqueda" type="text" placeholder="Texto a buscar..."/>
            <button id="busqueda-btn" onclick="busqueda()"></button>
        </div>
        <div id="buttons-div">
            <button type="button" onclick="grabaAplicacion()" id="graba">
                Graba
            </button>
            <?php $function = $comprobTipo ? "vuelveAlDoc()" : "cancelaAplicacion()"; ?>
            <button type="button" onclick="<?php echo $function; ?>" id="cancelaVuelve">
                Cancela
            </button>
            <button type="button" onclick="aplicaAuto()"  id="Auto">
                Auto
            </button>
        </div>
    </div>
    <div id="izq" class="ui-widget-content ui-corner-all">
        <form>
            <input type="hidden" name="socio_id" value="<?php echo $socio ? $socio->id : ''; ?>"/>
            <table id="docs-table">
                <tr>
                    <td><span onclick="collapseExpand(this)" class="collapsed all"></span></td>
                    <th>Comprob.</th>
                    <th>Detalle</th>
                    <th class="fecha">Fecha</th>
                    <th class="importe">Total</th>
                    <th class="saldo">Saldo</th>
                    <td></td>
                </tr>
                <?php foreach ($docsPendientes as $doc): ?>
                    <tr id="<?php echo $doc["doc_id"]; ?>" class="droppable">
                        <td  class="collapse-expand-cell"><span  class="collapsed" onclick="collapseExpand(this)"></span></td>
                        <td class="searchable"><?php
                            echo $doc["comprob"] . " " .
                            sprintf('%0' . (int) 4 . 's', $doc["sucursal"]) . "-" . sprintf('%0' . (int) 8 . 's', $doc["numero"]);
                            ?></td>
                        <td class="searchable"><?php echo $doc["detalle"]; ?></td>
                        <td class="searchable fecha"><?php echo date("d/m/Y", mystrtotime($doc["fecha_creacion"])); ?></td>
                        <td class="searchable importe"><?php echo $doc["total"]; ?></td>
                        <td class="searchable saldo"><?php echo $doc["saldo"]; ?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="4">
                            <div parent_id="<?php echo $doc["doc_id"]; ?>" class="aplicaciones-div   ui-widget-content ui-corner-all">
                                <table class="aplicaciones" >
                                    <?php $total = 0; ?>
                                    <?php foreach ($doc["docsAplicados"] as $d): ?>
                                        <tr 
                                            doc_orig_id="<?php echo $d['doc_orig_id']; ?>"
                                            doc_apl_id="<?php echo $d['doc_apl_id']; ?>">
                                            <td class="searchable"><?php
                                                echo $d["comprob"] . " " .
                                                sprintf('%0' . (int) 4 . 's', $d["sucursal"]) . "-" . sprintf('%0' . (int) 8 . 's', $d["numero"]);
                                                ;
                                                ?></td>
                                            <td class="searchable fecha"><?php echo $d["fecha_creacion"]; ?></td>
                                            <td class="importe searchable"><?php echo $d["importe"]; ?></td>
                                            <td class="desaplica-td"><img title="desaplica" src="images/delete.jpg" onclick="desaplica(this)"/></td>
                                            <?php $total += $d["importe"]; ?>
                                        </tr>
                                    <?php endforeach; ?>
                                    <tr>
                                        <td class="bold">Total</td>
                                        <td colspan="2" class="importe bold"><?php echo number_format($total, 2, ".", ","); ?></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </form>
    </div>
    <div id="der" class="ui-widget-content ui-corner-all">
        <table id="creditos-table" class="docs">
            <tr>
                <th>Comprob.</th>
                <!--<th>Detalle</th>-->
                <th class="fecha">Fecha</th>
                <th class="importe">Total</th>
                <th class="saldo"> Saldo</th>
                <td></td>
            </tr>
            <?php foreach ($docs as $doc): ?>
                <tr  id="<?php echo $doc['doc_id']; ?>" class="docs-tr draggable ui-widget-content">
                    <td class="searchable comprob"><?php
                        echo $doc["comprob"] . " " .
                        sprintf('%0' . (int) 4 . 's', $doc["sucursal"]) . "-" . sprintf('%0' . (int) 8 . 's', $doc["numero"]);
                        ;
                        ?></td>
                    <!--<td class="searchable detalle"><?php //echo $doc["detalle"];  ?></td>-->
                    <td class="searchable fecha fecha_creacion"><?php echo date("d/m/Y", mystrtotime($doc["fecha_creacion"])); ?></td>
                    <td class="searchable importe"><?php echo $doc["total"]; ?></td>
                    <td class="searchable saldo"><?php echo $doc["saldo"]; ?></td>
                    <td class="desaplica-td"><img title="desaplica" src="images/delete.jpg" onclick="desaplica(this)"/></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>
<script type="text/javascript">
//    $(".draggable").draggable({ handle: "td" });
</script>