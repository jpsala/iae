<?php $id = $this->action->id; ?>
<style type="text/css">
    html{overflow: hidden;}
    .datos{ background-color: #F9FAFC;
            border: 0 solid #EFF3FC;
            border-radius: 5px 5px 5px 5px;
            float: left;
            margin: 1px 0 0 4px;
            padding: 5px;
            position: relative;
            width: 66.5%;
            margin: 0 0 5px 7px;}

    #treeConceptos{max-height: 480px !important;}

    #content{padding:10px 5px 0px !important}

    #main-form {padding: 5px;margin: 5px 0px 5px; border: 1px #75C2F8 solid; border-radius: 8px ; position: relative;overflow: visible;min-height: 451px}
    /*#izq{ width: 67%;; float: left; border-radius: 5px; background-color: #f9fafc; border: #eff3fc solid 1px; padding: 5px;position: relative; margin: 30px 0 0 0}*/
    #izq{
        background-color: #F9FAFC;
        border: 0 solid #EFF3FC;
        border-radius: 5px 5px 5px 5px;
        float: left;
        margin: 1px 0 0 4px;
        padding: 5px;
        position: relative;
        width: 66.5%;
    }  
    #der{   width: 30.8%;  float: right; margin-right: 0px; padding: 5px; overflow: visible}
    labelHighlight{color: #AAA; font-style: italic;}
    #div-head{padding: 5px}
    #div-head div{display:inline-block}
    #div-head label{ float: left;  width: 48px;}
    #div-head #socio{width: 300px; }
    #div-pago #pago{width: 60px; text-align: right;margin-left: -3px;}
    #doc-pendientes,#pago-a-cuenta-div, #total-pagos-div,#total-div,
    #forma-de-pago-div {border-radius: 5px; background-color: #f9fafc; width: 100%;  border: #eff3fc solid 1px; padding: 0px;position: relative}
    .row{margin: 6px 3px}
    .row{margin-bottom: 7px}
    #doc-pendientes{min-height: 245px;max-height: 245px; overflow: auto}
    #doc-pendientes th, #doc-pendientes td, #doc-pendientes caption {padding: 1px 3px 1px 5px !important}
    .titulo{ background-color: #C3D9FF;    border-bottom: 1px solid #CCCCCC;    border-bottom: 1px solid #EFF3FC;    border-radius: 5px 5px 5px 5px;  margin-bottom: 5px;    padding: 3px;    text-align: center;color: #555555;  font-weight: bold;}

    #doc-pendientes docs table thead tr th{text-align: right}
    #doc-pendientes docs table thead tr th:first-child{text-align: left}
    #doc-pendientes th.detalle{min-width: 110px; text-align: left}
    #doc-pendientes td.pago{padding-right: 0 !important;  text-align: right;}
    #doc-pendientes td.pago input{width: 83px; text-align: right}
    #pago-a-cuenta-div #pago_a_cuenta_detalle{width: 79%}
    #pago-a-cuenta-div #pago_a_cuenta_importe{    position: absolute;    right: 6px;    text-align: right;}

    #main-form #div-numero{background-image: url("images/invoice.jpg");background-position: 0 2px;background-repeat: no-repeat;background-size: 17px auto;padding-left: 19px;}
    #div-numero{width:100%/*position: absolute;    right: 9px; top: 9px*/}
    #div-numero input{ float: right; margin-right: 20px;   width: 110px; margin-top:5px; margin-bottom: 4px}

    #total-div span.total, #total-pagos-div span.total{    position: absolute;    right: 4px; font-weight: bold}

    #botones{ clear: both;margin:11px 0px 2px 11px; position: relative; text-align: right; top: 0;}
    .esNuevo{color:red}
    #cheque-form-template{display: none}
    #div-cheque-genera-interes{display: inline}
    #div-cheque-genera-interes label{display: inline; width:300px !important}
    .valor-tipo{width: 50px}
    .valor-importe{width: 80px; text-align: right}
    .right-align {  text-align: right }

    .bold{font-weight: bold}
    .oculto{display: none}
    .total{background-image: url("images/pesos.gif");background-position: 0 50%;background-repeat: no-repeat;background-size: 10px auto;padding-left: 15px;}


    input::-webkit-input-placeholder, textarea::-webkit-input-placeholder {
        color: #636363;
    }
    input:-moz-placeholder, textarea:-moz-placeholder {
        color: #636363;
    }

    #contextMenu, #nota-de-credito{display:none}
    #nota-de-credito{overflow: visible}
    #nota-de-credito form label{width: 200px}
    #nota-de-credito form select,#nota-de-credito form input{width: 300px;display: block;}
    .docs td{}
    #doc-dets-div{display:none}
    #obs-cobranza-dialog div{font-size: 12px !important;
                             height: 96%;
                             padding-bottom: 4px;
                             padding-left: 6px;
                             padding-right: 6px;
                             padding-top: 4px;
                             width: 97%;}
    .docs tr a{display: inline-block;
               text-decoration: none}
    .docs .ui-button{width: 220px; text-align: left}
    .tiene-cuotas *:hover{background-color: lavenderblush !important}
    /*  .divclearable > a {left: -1px;
                         position: absolute;
                         top: -3px;
      }*/
    #rep-ctacte{max-height: 500px!important; overflow: auto!important; wwidth:700px!important}
    #table-ctacte .ctacte-total th{border-top: black 1px solid}
    #table-ctacte .comprobante{text-align: right;width:100px}
    #table-ctacte .importe{text-align: right; width:60px}
    #rep-ctacte .fecha{width:60px}
    #rep-ctacte .tipo{width:40px}
    #rep-ctacte .nro_comprob{width:60px}
    #rep-ctacte .detalle{width:352px}
    #nota-de-credito label {
        float: left;
        line-height: 24px;
        width: 70px !important;
    }
    #nota-de-credito #concepto_chzn{display:block;margin: 10px 0;}
    #nota-de-credito #concepto-dlg{display:none}
    #nota-de-credito #concepto-a{cursor:pointer; text-decoration: none; color: #0099FF;}
    #nota-de-credito #concepto-a:hover{-moz-text-decoration-line:underline}
    #nota-de-credito #concepto-button{    display: inline;
                                          margin-bottom: 3px;
                                          margin-left: 4px;
                                          width: 20px;}
    #nota-de-credito #concepto-button.ui-button span.ui-button-text{padding: 0 7px 0 7px}
    #nota-de-credito #concepto-button-div{position:relative;display:inline}
    #nota-de-credito #concepto_ac{width:180px; display: inline;}
    #pago-a-cuenta-importe {
        text-align: right;
        width: 95px;
    }

    #pago-a-cuenta-detalle{width:84%}

    #detalle{width:250px}
    #concepto_ac{width:250px}



</style>