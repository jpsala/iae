<style type="text/css">
    #table-ctacte .tipo{ min-width:120px}
    #table-ctacte .importe{text-align: right}
    #table-ctacte .ctacte-total th{border-top: black 1px solid}
    #table-ctacte-div{max-height: 400px;overflow: auto}
    #table-ctacte-totales-div .importe{padding-right: 11px}
</style>
<div id="table-ctacte-div">
    <table id="table-ctacte">
        <tr>
            <th>Tipo</th>
            <th>Número</th>
            <th>Fecha</th>
            <th>Detalle</th>
            <th class="importe">Total</th>
            <th class="importe">Saldo</th>
        </tr>
        <?php /* @var $doc Doc */; ?>
        <?php $socio = Socio::model()->findByPk($socio_id); ?>
        <?php /* @var $doc Doc */; ?>
        <?php $total = 0; ?>
        <?php $saldo = 0; ?>
        <?php
        foreach (Doc::model()->with(array("talonario", "talonario.comprob"))
                ->findAll(array("condition" => "socio_id = $socio->id and anulado=0 and t.activo = 1", "order" => "t.fecha_valor asc")) as $doc):
            ?>
            <tr id="<?php echo $doc->id;?>">
                <?php $cantDetalles = Helpers::qryScalar("select count(*) from doc_det d where d.doc_id = $doc->id"); ?>
                <td  class="tipo"><?php echo $doc->talonario->comprob->nombre; ?></td>
                <td><?php echo $doc->numeroCompleto; ?></td>
                <td><?php echo $doc->fecha_creacion; ?></td>
                <?php
                if ($cantDetalles == 0) {
                    $detalle = $doc["detalle"];
                    $tdClass = "";
                } else {
					$tdClass = " tiene-cuotas ";
					//vd($doc->attributes);
                    $detalle = "<input class=\"pago\" type='button' value='" . $doc["detalle"] . "' tabindex=\"-1\"/>";
                }
                ?>
                <td class="detalle-td"><?php echo $detalle; ?></td>
                <?php $importe = $doc->talonario->comprob->signo_cc * $doc->total; ?>
                <?php $saldo+=$importe; ?>
                <td class="importe"><?php echo number_format($importe, 2); ?></td>
                <td class="importe"><?php echo number_format($saldo, 2); ?></td>
            </tr>
            <?php $total+=($doc->talonario->comprob->signo_cc * $doc->saldo); ?>
        <?php endforeach; ?>

    </table>
</div>
<div id="table-ctacte-totales-div">
    <table>
        <tr class="ctacte-total">
            <th class="">Total</th>
            <th class="importe"><?php echo sprintf("%.2f", $saldo); ?></th>
        </tr>
    </table>
</div>
<script type="text/javascript">
  //$(".detalle-td input").button();
  $(".detalle-td input").on("click",function(){
     muestraDocDetalle($(this)); 
  }).button();
</script>