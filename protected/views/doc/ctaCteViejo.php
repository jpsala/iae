<table id="table-ctacte">
  <tr>
    <th class="comprobante">Comprob</th>
    <th class="detalle">Detalle</th>
    <th class="fecha">Fecha</th>
    <th class="importe">Total</th>
    <th class="importe">Saldo</th>
  </tr>
  <?php /* @var $doc Doc */; ?>
  <?php /* @var $doc Doc */; ?>
  <?php
  $total = 0;
  $saldo = 0
  ?>
  <?php foreach ($rows as $row): ?>
    <tr>
      <td class="comprobante"><?php echo $row["COMPROBANTE"]; ?></td>
      <td class="detalle"><?php echo $row["DETALLE"]; ?></td>
      <td class="fecha"><?php echo date("d/m/Y", mystrtotime($row["FECHA"])); ?></td>
      <?php $total+=($row["DEBE"] - $row["HABER"]); ?>
      <td class="importe"><?php echo number_format($row["DEBE"] - $row["HABER"],2); ?></td>
      <td class="importe"><?php echo number_format( $total,2); ?></td>
    </tr>
    <?php $saldo+=($total); ?>
  <?php endforeach; ?>
  <tr class="ctacte-total">
    <th>Total</th>
    <th>&nbsp;</th>
    <th>&nbsp;</th>
    <th class="importe">&nbsp;</th>
    <th id="fin-tabla" class="importe"><?php echo number_format($total,2); ?></th>
  </tr>
</table>