<?php $this->breadcrumbs = array(Doc::label(2),); ?>

<div id="_form">
    <?php include "_form.php"; ?>
</div>

<script type="text/javascript">
    
    init();
    

    function init(){
        $("button, #submit, #borra").button();
        $(".chzn-select").chosen();
        $(".clearinput").clearinput();
        $("#nombre").focus();
        
        $("#Destino").chosen().change(function(){
            ajax($(this).val());
        });
        
        $('#borra').click(function(){
            if(!confirm("Borra esta publicación??"))
                return;
            
            $.ajax({
                type: "GET",
                data: {'id':$('#id').val()},
                url: "<?php echo $this->createUrl('Crud/destinoBorra'); ?>",
                success: function(data) {
                    refresh();
                }
            });  
        })
        
        $('form').submit(function(){
            $.ajax({
                type: "POST",
                data: $('form').serialize(),
                dataType:'json',
                url: "<?php echo $this->createUrl('crud/destinoGraba'); ?>",
                success: function(data) {
                    if(data){
                        $('#errores').html("");
                        for(var i in data)
                        {
                            $('#errores').append(data[i][0]+"<br/>");
                        }
                        $('#errores').show();
                    }else{
                        refresh();
                    }
                }
            });
            return false;
        });

    }
    function refresh(){
        ajax(null);
        return false;
    }
    function ajax(id){
        $.ajax({
            type: "GET",
            data: {"id":id},
            url: "<?php echo $this->createUrl("doc/_form"); ?>",
            success: function(data) {
                alert(id);
                $("#_form").html(data);
            },
            error: function(data,status){
            },
            complete: function(){
                init();
            }
        });
    }    
</script>

