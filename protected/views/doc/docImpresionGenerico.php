<?php

class MYPDF extends PDF {

  public $planilla;

  public function header() {
    
  }

  public function footer() {
    
  }

  public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false) {
    parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
  }

}

class Informe extends stdClass {

  public $pdf, $doc;

  public function __construct($doc, $paperSize = "A4") {
    $this->paperSize = $paperSize;
    $this->doc = $doc;
  }

  public function imprime($imprimeAuto, $nombre_pc, $impresora) {
    $this->pdf = new MYPDF("P", "mm", $this->paperSize);
    $this->pdf->marginY = 4;
    $this->pdf->marginX = 2;
    $this->pdf->planilla = $this;
    $this->pdf->SetCreator(PDF_CREATOR);
    $this->pdf->SetAuthor('JP');
    $this->pdf->SetTitle('Doc');
    $this->pdf->SetSubject('Doc');
    $this->pdf->SetKeywords('TCPDF, PDF, doc');
    $this->pdf->SetPrintHeader(true);
    $this->pdf->SetPrintFooter(true);
    $this->pdf->SetAutoPageBreak(false);
    $this->fontSize = 8;
    //$this->SetMargins(0, 0, -10, true);
    //$this->SetRightMargin(10);
    $this->preparaPdf();
    /*
     * Parameters:
       

      $left
      (float) Left margin.
       

      $top
      (float) Top margin.
       

      $right
      (float) Right margin. Default value is the left one.
       

      $keepmargins
      (boolean) if true overwrites the default page margins
     */
    $user_id = Yii::app()->user->id;
    if ($imprimeAuto == "S" or $imprimeAuto == "P") {
      $numero = str_pad($this->doc["sucursal"], 4, "0", STR_PAD_LEFT) . "-" . str_pad($this->doc["numero"], 8, "0", STR_PAD_LEFT);
      if (!$nombre_pc) {
        throw new exception("Debe especificar el nombre de la pc en \"puesto_trabajo\" (para el puesto de trabajo del usuario $user_id) para guardar el archivo en docsAutoPrint");
      }
      if (!$impresora or $impresora == "") {
        throw new exception("Debe especificar una impresora en \"talonario\" (para el puesto de trabajo del usuario $user_id) ");
      }
      if (gethostbyaddr("127.0.0.1") == "localhost") {
        $path = "./docsAutoPrint/$nombre_pc/";
      } elseif (gethostbyaddr("127.0.0.1") == "jp-PC") {
        $path = "docsAutoPrint\\$nombre_pc\\";
      }
      if (!file_exists($path)) {
        if (!mkdir($path, 0777, true)) {
          $currDir = getcwd();
          throw new Exception("No pudo crear la carpeta \"$path\" dentro de \"$currDir\" en el servidor</br>");
        }
      }
      $comprob_nombre = $this->doc["comprob_nombre"];
      $file = $path . $imprimeAuto . "@" . $impresora . "@" . $comprob_nombre . "-" . $numero . '.pdf';
      $this->pdf->Output($file, 'F');
    } else {
      $this->pdf->Output('borrar.pdf');
    }
  }

  private function preparaPdf() {

    /*
     * Cálculo del ancho de la columna alumnos
     */
    $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", 11);
    $this->pdf->setCellPaddings(1, 2, 0, 0);
    $this->pdf->AddPage();
    $this->fontSize = 10;
    $this->imprimeParte(1);
  }

  public function imprimeParte($parte) {
    $this->pdf->setLineStyle(array('width' => 15));
    $marginLeft = 3;
    $marginRight = 3;
    //$width = $this->pdf->getPageWidth() / 2;
    $y = 4;
    $this->pdf->textBox(array(
      "txt" => "Fecha: " . date("d/m/Y", mystrtotime($this->doc["fecha"])),
      "x" => 0, "y" => $y,
      "w" => $this->pdf->getPageWidth() - $marginRight - 10,
      "align" => "R"
    ));
    $y = 5;
    if ($this->doc["nombre_completo"]) {
      $this->pdf->textBox(array(
        "txt" => $this->doc["cliente_tipo"] . ":",
        "x" => $marginLeft, "y" => $y, "fontstyle" => ""
      ));
      $this->pdf->textBox(array(
        "txt" => $this->doc["nombre_completo"],
        "x" => $marginLeft + 30, "y" => $y,
        "fontstyle" => "b"
      ));
    }
    $y += 5;
    $numero = str_pad($this->doc["sucursal"], 4, "0", STR_PAD_LEFT) . "-" . str_pad($this->doc["numero"], 8, "0", STR_PAD_LEFT);
    $this->pdf->textBox(array(
      "txt" => "Comprobante :", "x" => $marginLeft,
      "y" => $y
    ));
    $this->pdf->textBox(array(
      "txt" => $this->doc["comprob_nombre"],
      "x" => $marginLeft + 30, "y" => $y
    ));
    $this->pdf->textBox(array(
      "txt" => "Número: " . $numero, "x" => 0, "y" => $y,
      "w" => $this->pdf->getPageWidth() - $marginRight,
      "align" => "R"
    ));

    $y += 7;
    if ($this->doc["detalle"]) {
      $this->pdf->line($marginLeft, $y, $this->pdf->getPageWidth() - $marginRight, $y, array('width' => 0.5));
      $y += 2;
      $this->pdf->textBox(array(
        "txt" => "Detalle:", "x" => $marginLeft, "y" => $y
      ));
      $y += 7;
      $this->pdf->textBox(array(
        "txt" => $this->doc["detalle"],
        "x" => $marginLeft + 5, "y" => $y
      ));
      $y += 7;
    }

    if ($this->doc["pago_a_cuenta_importe"] > 0) {
      $this->pdf->line($marginLeft, $y, $this->pdf->getPageWidth() - $marginRight, $y, array('width' => 0.5));
      $y += 1;
      $this->pdf->textBox(array(
        "txt" => "Pago a cuenta: ", "x" => $marginLeft,
        "y" => $y
      ));
      $y += 7;
      $this->pdf->textBox(array(
        "txt" => $this->doc["pago_a_cuenta_detalle"],
        "x" => $marginLeft + 5, "y" => $y
      ));
      $this->pdf->textBox(array(
        "txt" => $this->doc["pago_a_cuenta_importe"],
        "x" => 0, "y" => $y, "align" => "R",
        "w" => $this->pdf->getPageWidth() - $marginRight
      ));
      $y += 7;
    }

    if (count($this->doc["apl"]) > 0) {
      $this->pdf->line($marginLeft, $y, $this->pdf->getPageWidth() - $marginRight, $y, array('width' => 0.5));
      $y += 1;
      $this->pdf->textBox(array(
        "txt" => "Aplicaciones: ", "x" => $marginLeft,
        "y" => $y
      ));
      $y += 7;
    }
    //        $this->pdf->line($marginLeft, $y, $this->pdf->getPageWidth(),$y);
    //        $y += 1;

    foreach ($this->doc["apl"] as $apl) {
      $numero = str_pad($apl["sucursal"], 4, "0", STR_PAD_LEFT) . "-" . str_pad($apl["numero"], 8, "0", STR_PAD_LEFT);
      $this->pdf->textBox(array(
        "txt" => $apl["comprob"] . " $numero",
        "x" => $marginLeft + 5, "y" => $y
      ));
      $this->pdf->textBox(array(
        "txt" => $apl["importe"], "x" => 0, "y" => $y,
        "align" => "R",
        "w" => $this->pdf->getPageWidth() - $marginRight
      ));
      $y += 7;
    }

    $this->pdf->line($marginLeft, $y, $this->pdf->getPageWidth() - $marginRight, $y, array('width' => 0.5));
    $y += 1;
    $this->pdf->textBox(array(
      "txt" => "Forma de pago: ", "x" => $marginLeft,
      "y" => $y
    ));
    $y += 7;

    foreach ($this->doc["val"] as $val) {
      $tipo = $val["tipo"] + 0;
      $txt = $val["nombre"];
      //            $numero = $val["numero"];
      if ($tipo !== "1") {
        $txt .= " " . ($val["destino"]);
      }
      if ($tipo == "1") {
        $txt .= " " . ($val["banco"]);
      }

      if ($tipo == "2") {
        $txt .= " " . ($val["tarjeta"]);
      }

      if ($tipo == "1" or $tipo == "2" or $tipo == "3") {
        $txt .= "  #" . $val["numero"] . " " . $val["fecha"];
      }

      $this->pdf->textBox(array(
        "txt" => $txt, "x" => $marginLeft + 5, "y" => $y
      ));
      $this->pdf->textBox(array(
        "txt" => $val["importe"], "x" => 0, "y" => $y,
        "align" => "R",
        "w" => $this->pdf->getPageWidth() - $marginRight
      ));
      $y += 7;
    }


    $y += 1;
    $this->pdf->line($marginLeft, $y, $this->pdf->getPageWidth() - $marginRight, $y, array('width' => 0.5));
    $y += 4;
    $this->pdf->textBox(array(
      "txt" => "Total Comprobante", "x" => $marginLeft,
      "y" => $y, "", "align" => "L"
    ));
    $this->pdf->textBox(array(
      "txt" => $this->doc["total"], "x" => 0, "y" => $y,
      "align" => "R",
      "w" => $this->pdf->getPageWidth() - $marginRight
    ));
  }

}

$i = new Informe($doc);

$i->imprime($imprimeAuto, $nombre_pc, $impresora);
