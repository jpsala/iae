<?php

	class MYPDF extends PDF {

		public $planilla;

		public function header() {
			
		}

		public function footer() {
			
		}

		public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false) {
			parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
		}

	}

	class Informe extends stdClass {

		public $pdf, $doc;

		public function __construct($doc, $paperSize = "A4") {
			$this->paperSize = $paperSize;
			$this->doc = $doc;
		}

		public function imprime($imprimeAuto, $nombre_pc, $impresora) {
			$this->pdf = new MYPDF("L", "mm", $this->paperSize);
			$this->pdf->marginY = 4;
			$this->pdf->marginX = 2;
			$this->pdf->planilla = $this;
			$this->pdf->SetCreator(PDF_CREATOR);
			$this->pdf->SetAuthor('JP');
			$this->pdf->SetTitle('Doc');
			$this->pdf->SetSubject('Doc');
			$this->pdf->SetKeywords('TCPDF, PDF, doc');
			$this->pdf->SetPrintHeader(true);
			$this->pdf->SetPrintFooter(true);
			$this->pdf->SetAutoPageBreak(false);
			$this->fontSize = 8;
			$this->preparaPdf();
//        $this->pdf->Output("test.pdf");
			$user_id = Yii::app()->user->id;
			if ($imprimeAuto == "S" or $imprimeAuto == "P") {
				$numero = str_pad($this->doc["sucursal"], 4, "0", STR_PAD_LEFT) . "-" . str_pad($this->doc["numero"], 8, "0", STR_PAD_LEFT);
				if (!$nombre_pc) {
					throw new exception("Debe especificar el nombre de la pc en \"puesto_trabajo\" (para el puesto de trabajo del usuario $user_id) para guardar el archivo en docsAutoPrint");
				}
				if (!$impresora or $impresora == "") {
					throw new exception("Debe especificar una impresora en \"talonario\" (para el puesto de trabajo del usuario $user_id) ");
				}
				if (gethostbyaddr("127.0.0.1") == "localhost") {
					$path = "./docsAutoPrint/$nombre_pc/";
				} elseif (gethostbyaddr("127.0.0.1") == "jp-PC") {
					$path = "docsAutoPrint\\$nombre_pc\\";
				}
				if (!file_exists($path)) {
					if (!mkdir($path, 0777, true)) {
						$currDir = getcwd();
						throw new Exception("No pudo crear la carpeta \"$path\" dentro de \"$currDir\" en el servidor</br>");
					}
				}
				$comprob_nombre = $this->doc["comprob_nombre"];
				$file = $path . $imprimeAuto . "@" . $impresora . "@" . $comprob_nombre . "-" . $numero . '.pdf';
				$this->pdf->Output($file, 'F');
				$this->pdf->Output($file, 'F');
				//ve($path . $numero . '.pdf');
			} else {
				$this->pdf->Output('borrar.pdf');
			}
		}

		private function preparaPdf() {

			/*
			 * Cálculo del ancho de la columna alumnos
			 */
			$this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", 11);
			$this->pdf->setCellPaddings(1, 2, 0, 0);
			$this->pdf->AddPage();
			$this->fontSize = 9;
			$this->imprimeParte(1);
			$this->imprimeParte(2);
		}

		public function imprimeParte($parte) {
			$marginLeft = ($parte == 1) ? 0 : $this->pdf->getPageWidth() / 2;
			$width = $this->pdf->getPageWidth() / 2;
			$y = 30;
			$this->pdf->textBox(array("txt" => date("d/m/Y", mystrtotime($this->doc["fecha"])), "x" => $marginLeft + 100, "y" => $y));
			$y+=7;
			$numero = str_pad($this->doc["sucursal"], 4, "0", STR_PAD_LEFT) . "-" . str_pad($this->doc["numero"], 8, "0", STR_PAD_LEFT);
			$this->pdf->textBox(array("txt" => $numero, "x" => $marginLeft + 95, "y" => $y));
			$y+=25;
//			vd($this->doc);
			$this->pdf->textBox(array("txt" => $this->doc["nombre_completo"], "x" => $marginLeft + 5, "y" => $y));
			$this->pdf->textBox(array("txt" => $this->doc["division_nombre"], "x" => $marginLeft + 80, "y" => $y));
			$this->pdf->textBox(array("txt" => $this->doc["anio_nombre"], "x" => $marginLeft + 85, "y" => $y));
			$this->pdf->textBox(array("txt" => $this->doc["matricula"], "x" => $marginLeft + 108, "y" => $y));
			$this->pdf->textBox(array("txt" => $this->doc["nivel_nombre"], "x" => $marginLeft + 120, "y" => $y));
			$y = 110;
			foreach ($this->doc["apl"] as $apl) {
				$numero = str_pad($apl["sucursal"], 4, "0", STR_PAD_LEFT) + "-" + str_pad($apl["numero"], 8, "0", STR_PAD_LEFT);
				$detalle = $apl["detalle"] ? $apl["detalle"] : ($apl["comprob"] . " $numero");
				$this->pdf->textBox(array("txt" => $detalle, "x" => $marginLeft + 10, "y" => $y));
				$this->pdf->textBox(array("txt" => $apl["importe"], "x" => $marginLeft + 116, "y" => $y, "", "align" => "R", "w" => 19));
				$y +=7;
			}
			//$y += 5;
			if ($this->doc["pago_a_cuenta_importe"] > 0) {
				$this->pdf->textBox(array("txt" => $this->doc["pago_a_cuenta_detalle"], "x" => $marginLeft + 10, "y" => $y));
				$this->pdf->textBox(array("txt" => $this->doc["pago_a_cuenta_importe"], "x" => $marginLeft + 116, "y" => $y, "", "align" => "R", "w" => 19));
			}
			$y += 10;
			foreach ($this->doc["val"] as $val) {
				$tipo = $val["tipo"] + 0;
				$txt = $val["nombre"];
//            $numero = $val["numero"];
				if ($tipo !== "1") {
					$txt .= " " . ($val["destino"]);
				}
				if ($tipo == "1") {
					$txt .= " " . ($val["banco"] );
				}

				if ($tipo == "2") {
					$txt .= " " . ($val["tarjeta"] );
				}

				if ($tipo == "1" or $tipo == "2") {
					$txt .= "  #" . $val["numero"];
				}
				if ($tipo == "4") {
					$txt .= " " . ($val["fecha"]);
				}

				$this->pdf->textBox(array("txt" => $txt, "x" => $marginLeft + 10, "y" => $y));
				$this->pdf->textBox(array("txt" => $val["importe"], "x" => $marginLeft + 116, "y" => $y, "", "align" => "R", "w" => 19));
				$y += 7;
			}
			$y = 179;
			$this->pdf->textBox(array("txt" => $this->doc["total"], "x" => $marginLeft + 116, "y" => $y, "", "align" => "R", "w" => 19));
		}

	}

	$i = new Informe($doc);

	$i->imprime($imprimeAuto, $nombre_pc, $impresora);
	