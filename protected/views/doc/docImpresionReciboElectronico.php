<?php

class MYPDF extends PDF
{

  public $planilla;

  public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false)
  {
    parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
  }

  public function header()
  {

  }

  public function footer()
  {

  }

}

class Informe extends stdClass
{

  public $pdf, $doc;

  public function __construct($doc, $paperSize = "A4")
  {
    $this->paperSize = $paperSize;
    $this->doc       = $doc;
  }

  public function imprime($imprimeAuto, $nombre_pc, $impresora)
  {
    $this->pdf           = new MYPDF("P", "mm", $this->paperSize);
    $this->pdf->marginY  = 5;
    $this->pdf->marginX  = 0;
    $this->pdf->planilla = $this;
    $this->pdf->SetCreator(PDF_CREATOR);
    $this->pdf->SetAuthor('JP');
    $this->pdf->SetTitle('Doc');
    $this->pdf->SetSubject('Doc');
    $this->pdf->SetKeywords('TCPDF, PDF, doc');
    $this->pdf->SetPrintHeader(true);
    $this->pdf->SetPrintFooter(true);
    $this->pdf->SetAutoPageBreak(false);
    $this->fontSize = 8;
    $this->preparaPdf();
//        $this->pdf->Output("test.pdf");
    $user_id = Yii::app()->user->id;
    if ($imprimeAuto == "S" or $imprimeAuto == "P") {
      $numero = str_pad($this->doc["sucursal"], 4, "0", STR_PAD_LEFT) . "-" . str_pad($this->doc["numero"], 8, "0", STR_PAD_LEFT);
      if (!$nombre_pc) {
        throw new exception("Debe especificar el nombre de la pc en \"puesto_trabajo\" (para el puesto de trabajo del usuario $user_id) para guardar el archivo en docsAutoPrint");
      }
      if (!$impresora or $impresora == "") {
        throw new exception("Debe especificar una impresora en \"talonario\" (para el puesto de trabajo del usuario $user_id) ");
      }
      if (gethostbyaddr("127.0.0.1") == "localhost") {
        $path = "./docsAutoPrint/$nombre_pc/";
      } elseif (gethostbyaddr("127.0.0.1") == "jp-PC") {
        $path = "docsAutoPrint\\$nombre_pc\\";
      }
      if (!file_exists($path)) {
        if (!mkdir($path, 0777, true)) {
          $currDir = getcwd();
          throw new Exception("No pudo crear la carpeta \"$path\" dentro de \"$currDir\" en el servidor</br>");
        }
      }
      $comprob_nombre = $this->doc["comprob_nombre"];
      $file           = $path . $imprimeAuto . "@" . $impresora . "@" . $comprob_nombre . "-" . $numero . '.pdf';
      $this->pdf->Output($file, 'F');
      //ve($path . $numero . '.pdf');
    } else {
      $this->pdf->Output('borrar.pdf');
    }
  }

  private function preparaPdf()
  {
    $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", 12);
    $this->pdf->setCellPaddings(1, 1, 0, 0);
    $this->pdf->AddPage();
    $this->fontSize = 9;
    $this->imprimeParte(1);
  }

  public function imprimeParte($parte)
  {
    $cae = json_decode($this->doc["doc"]);

    $fecha_vto_cae = date("d/m/Y", strtotime($cae->fch_venc_cae));
    $cae           = $cae->cae;
    /*
         * @param $w (float) Width of cells. If 0, they extend up to the right margin of the page.
         * @param $h (float) Cell minimum height. The cell extends automatically if needed.
         * @param $txt (string) String to print
         * @param $border (mixed) Indicates if borders must be drawn around the cell. The value can be a number:<ul><li>0: no border (default)</li><li>1: frame</li></ul> or a string containing some or all of the following characters (in any order):<ul><li>L: left</li><li>T: top</li><li>R: right</li><li>B: bottom</li></ul> or an array of line styles for each border group - for example: array('LTRB' => array('width' => 2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)))
         * @param $align (string) Allows to center or align the text. Possible values are:<ul><li>L or empty string: left align</li><li>C: center</li><li>R: right align</li><li>J: justification (default value when $ishtml=false)</li></ul>
         * @param $fill (boolean) Indicates if the cell background must be painted (true) or transparent (false).
         * @param $ln (int) Indicates where the current position should go after the call. Possible values are:<ul><li>0: to the right</li><li>1: to the beginning of the next line [DEFAULT]</li><li>2: below</li></ul>
         * @param $x (float) x position in user units
         * @param $y (float) y position in user units
         * @param $reseth (boolean) if true reset the last cell height (default true).
         * @param $stretch (int) font stretch mode: <ul><li>0 = disabled</li><li>1 = horizontal scaling only if text is larger than cell width</li><li>2 = forced horizontal scaling to fit cell width</li><li>3 = character spacing only if text is larger than cell width</li><li>4 = forced character spacing to fit cell width</li></ul> General font stretching and scaling values will be preserved when possible.
         * @param $ishtml (boolean) INTERNAL USE ONLY -- set to true if $txt is HTML content (default = false). Never set this parameter to true, use instead writeHTMLCell() or writeHTML() methods.
         * @param $autopadding (boolean) if true, uses internal padding and automatically adjust it to account for line width.
         * @param $maxh (float) maximum height. It should be >= $h and less then remaining space to the bottom of the page, or 0 for disable this feature. This feature works only when $ishtml=false.
         * @param $valign (string) Vertical alignment of text (requires $maxh = $h > 0). Possible values are:<ul><li>T: TOP</li><li>M: middle</li><li>B: bottom</li></ul>. This feature works only when $ishtml=false and the cell must fit in a single page.
         * @param $fitcell (boolean) if true attempt to fit all the text within the cell by reducing the font size (do not work in HTML mode).
         * @return int Return the number of cells or 1 for html mode.

     */
    $this->pdf->SetMargins(0, 0, 1, true);
    $this->pdf->SetLeftMargin(0);
    $marginLeft = 0;
    $y          = 5;
    $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "B", 15);
    $this->pdf->textBox(array(
      "txt" => "ORIGINAL",
      "x" => $marginLeft, "y" => $y,
      "h" => 11, "maxh" => 11,
      "w" => $this->pdf->getPageWidth(),
      "border" => "LTRB",
      "align" => "C", "valign" => "M",
      "autopadding" => true,
    ));
    $y += 11;
    $this->pdf->textBox(array(
      "txt" => "",
      "x" => 0, "y" => $y,
      "h" => 69, "maxh" => 69,
      "w" => $this->pdf->getPageWidth(),
      "border" => "LRB",
      "align" => "C", "valign" => "T",
      "autopadding" => true,
    ));
    $wTipo = 16;
    $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "B", 20);
    $this->pdf->textBox(array(
      "txt" => "C",
      "x" => $marginLeft + ($this->pdf->getPageWidth() / 2) - ($wTipo / 2), "y" => $y,
      "h" => $wTipo, "maxh" => $wTipo,
      "w" => $wTipo,
      "border" => "LRB",
      "align" => "C", "valign" => "T",
      "autopadding" => true,
    ));
    $y += 4;
    $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "B", 8);
    $this->pdf->textBox(array(
      "txt" => "COD. 15",
      "x" => $marginLeft + ($this->pdf->getPageWidth() / 2) - ($wTipo / 2), "y" => $y,
      "h" => $wTipo, "maxh" => $wTipo,
      "w" => $wTipo,
      "align" => "C", "valign" => "M",
      "autopadding" => true,
    ));
    $y -= 1;
    $topHead = $y;
    $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "B", 18);
    $this->pdf->textBox(array("txt" => "Instituto Albert Einstein", "x" => $marginLeft + 10, "y" => $y));

    $y += 12;
    $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "B", 12);
    $w = $this->pdf->getStringWidth("Razón Social:");
    $this->pdf->textBox(array("txt" => "Razón Social:", "x" => $marginLeft + 10, "y" => $y));
    $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", 12);
    $this->pdf->textBox(array("txt" => "Albert Einstein S.A.", "x" => $marginLeft + 13 + $w, "y" => $y));
    $y += 8;
    $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "B", 12);
    $w = $this->pdf->getStringWidth("Domicilio Comercial:");
    $this->pdf->textBox(array("txt" => "Domicilio Comercial:", "x" => $marginLeft + 10, "y" => $y));
    $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", 12);
    $this->pdf->textBox(array("txt" => "Catamarca 3644", "x" => $marginLeft + 11 + $w, "y" => $y));
    $y += 6;
    $this->pdf->textBox(array("txt" => "Mar Del Plata - Buenos Aires", "x" => $marginLeft + 11 + $w, "y" => $y));
    $y += 7;
    $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "B", 12);
    $this->pdf->textBox(array("txt" => "Condición frente al IVA: IVA Sujeto Exento", "x" => $marginLeft + 10, "y" => $y));
    $y = $topHead;
    $x = ($this->pdf->getPageWidth() / 2) + 14;
    $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "B", 15);
    $this->pdf->textBox(array("txt" => "RECIBO C", "x" => $x, "y" => $y));
    $y += 9;
    $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "B", 12);
    $w = $this->pdf->getStringWidth("Punto de Venta: 0003");
    $this->pdf->textBox(array("txt" => "Punto de Venta: 0003", "x" => $x, "y" => $y));
    $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "B", 12);
    $this->pdf->textBox(array(
      "txt" => "Comp. Nro:" . str_pad($this->doc["numero"], 8, "0", STR_PAD_LEFT), "x" => $x + $w + 3, "y" => $y));
    $y += 6;
    $this->pdf->textBox(array(
      "txt" => "Fecha de Emisión: " . date("d/m/Y", mystrtotime($this->doc["fecha"])), "x" => $x, "y" => $y));
    $y += 6;
    $this->pdf->textBox(array(
      "txt" => "CUIT: 30-63939046-9", "x" => $x, "y" => $y));
    $y += 6;
    $this->pdf->textBox(array(
      "txt" => "Ingresos Brutos: 30-63939046-9", "x" => $x, "y" => $y));
    $y += 6;
    $this->pdf->textBox(array(
      "txt" => "Fecha de Inicio de Actividades: 01/03/1988", "x" => $x, "y" => $y));

    // Cliente
    $y += 9;
    $this->pdf->textBox(array(
      "txt" => "",
      "x" => 0, "y" => $y,
      "h" => 17, "maxh" => 17,
      "w" => $this->pdf->getPageWidth(),
      "border" => "TLR",
      "align" => "C", "valign" => "T",
      "autopadding" => true,
    ));
    $y += 2;
    $this->pdf->textBox(array("txt" => "Alumno", "x" => $marginLeft + 3, "y" => $y));
    $this->pdf->textBox(array("txt" => "Año", "x" => $marginLeft + 50, "y" => $y));
    $this->pdf->textBox(array("txt" => "Div", "x" => $marginLeft + 72, "y" => $y));
    $this->pdf->textBox(array("txt" => "Matrícula", "x" => $marginLeft + 82, "y" => $y));
    $this->pdf->textBox(array("txt" => "Sección", "x" => $marginLeft + 102, "y" => $y));
    $y += 7;
    $this->pdf->textBox(array("txt" => $this->doc["nombre_completo"], "x" => $marginLeft + 3, "y" => $y));
    $this->pdf->textBox(array("txt" => $this->doc["anio_nombre"], "x" => $marginLeft + 50, "y" => $y));
    $this->pdf->textBox(array("txt" => $this->doc["division_nombre"], "x" => $marginLeft + 72, "y" => $y));
    $this->pdf->textBox(array("txt" => $this->doc["matricula"], "x" => $marginLeft + 82, "y" => $y));
    $this->pdf->textBox(array("txt" => $this->doc["nivel_nombre"], "x" => $marginLeft + 102, "y" => $y));
    $y += 6;
    $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", 12);
    $this->pdf->textBox(array("txt" => "Responsable de pago informado:", "x" => $marginLeft + 3, "y" => $y));
    $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "B", 12);
    $w = $this->pdf->getStringWidth($this->doc["nombre_encargado"]) + 3;
    $this->pdf->textBox(array("txt" => $this->doc["nombre_encargado"], "x" => $marginLeft + 70, "y" => $y));
    $this->pdf->textBox(array("txt" => $this->doc["documento_tipo_encargado"], "x" => $marginLeft + $w + 73, "y" => $y));
    $this->pdf->textBox(array("txt" => $this->doc["documento_numero_encargado"], "x" => $marginLeft + $w + 83, "y" => $y));
//{"fch_venc_cae":"20150710",,"nro_doc":"23313542","tipo_doc":"96","tributos":[]}
    $y += 11;
    $w = $this->pdf->getStringWidth("Servicio Educativo") + 3;
    $this->pdf->textBox(array("txt" => "Servicio Educativo", "x" => $marginLeft + $marginLeft + 10, "y" => $y, 'border' => "B", "w" => $w));
    $y += 8;
    foreach ($this->doc["apl"] as $apl) {
      $numero  = str_pad($apl["sucursal"], 4, "0", STR_PAD_LEFT) + "-" + str_pad($apl["numero"], 8, "0", STR_PAD_LEFT);
      $detalle = $apl["detalle"] ? $apl["detalle"] : ($apl["comprob"] . " $numero");
      $this->pdf->textBox(array("txt" => $detalle, "x" => $marginLeft + 10, "y" => $y));
      $this->pdf->textBox(array("txt" => $apl["importe"], "x" => $marginLeft + 116, "y" => $y, "", "align" => "R", "w" => 19));
      $y += 7;
    }
    //$y += 5;
    if ($this->doc["pago_a_cuenta_importe"] > 0) {
      $this->pdf->textBox(array("txt" => $this->doc["pago_a_cuenta_detalle"], "x" => $marginLeft + 10, "y" => $y));
      $this->pdf->textBox(array("txt" => $this->doc["pago_a_cuenta_importe"], "x" => $marginLeft + 116, "y" => $y, "", "align" => "R", "w" => 19));
    }
    $y += 10;
    foreach ($this->doc["val"] as $val) {
      $tipo = $val["tipo"] + 0;
      $txt  = $val["nombre"];
//            $numero = $val["numero"];
      if ($tipo !== "1") {
        $txt .= " " . ($val["destino"]);
      }
      if ($tipo == "1") {
        $txt .= " " . ($val["banco"]);
      }

      if ($tipo == "2") {

        $txt .= " " . ($val["tarjeta"]);
      }

      if ($tipo == "1" or $tipo == "2") {
        $txt .= "  #" . $val["numero"];
      }
      if ($tipo == "4") {
        $txt .= " " . ($val["fecha"]);
      }

      $this->pdf->textBox(array("txt" => $txt, "x" => $marginLeft + 10, "y" => $y));
      $this->pdf->textBox(array("txt" => $val["importe"], "x" => $marginLeft + 116, "y" => $y, "", "align" => "R", "w" => 19));
      $y += 7;
    }

    /*
     * Abajo
     */
    $y = $this->pdf->getPageHeight() - 68;
    $this->pdf->textBox(array(
      "txt" => "",
      "x" => 0, "y" => $y,
      "h" => 22, "maxh" => 22,
      "w" => $this->pdf->getPageWidth(),
      "border" => "TLRB",
      "align" => "C", "valign" => "T",
      "autopadding" => true,
    ));
    $y += 2;
    $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "B", 12);
    $this->pdf->textBox(array("txt" => "Subtotal: $", "x" => $marginLeft + 120, "y" => $y, "", "align" => ""));
    $this->pdf->textBox(array("txt" => $this->doc["total"], "x" => $marginLeft + 190, "y" => $y, "", "align" => ""));
    $y += 7;
    $this->pdf->textBox(array("txt" => "Importe Otros Tributos: $", "x" => $marginLeft + 120, "y" => $y, "", "align" => ""));
    $this->pdf->textBox(array("txt" => 0, "x" => $marginLeft + 190, "y" => $y, "", "align" => ""));
    $y += 7;
    $this->pdf->textBox(array("txt" => "Importe Total: $", "x" => $marginLeft + 120, "y" => $y, "", "align" => ""));
    $this->pdf->textBox(array("txt" => $this->doc["total"], "x" => $marginLeft + 190, "y" => $y, "", "align" => ""));


    $y = $this->pdf->getPageHeight() - 25;
    $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", 12);
    $this->pdf->textBox(array("txt" => "CAE Nro.:", "x" => $marginLeft + 120, "y" => $y, "", "align" => ""));
    $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "B", 12);
    $this->pdf->textBox(array("txt" => $cae, "x" => $marginLeft + 173, "y" => $y, "", "align" => ""));
    $y += 7;
    $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", 12);
    $this->pdf->textBox(array("txt" => "Fecha de Vto. de CAE:", "x" => $marginLeft + 120, "y" => $y, "", "align" => ""));
    $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "B", 12);
    $this->pdf->textBox(array("txt" => $fecha_vto_cae, "x" => $marginLeft + 173, "y" => $y, "", "align" => ""));
    $afip = new Afip(Afip::MODO_PROD, Comprob::tipoComprobElectronico($this->doc["comprob_id"]));
    $y -= 24;
    $imgAfip = "http://" . $_SERVER["SERVER_NAME"] . '/' . Yii::app()->baseUrl . "/images/afip_logo2.png";
    $this->pdf->Image($imgAfip, 2, $y, 21, 9, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
//    $this->pdf->Image( $imgAfip, 2, $y, 28 , 9, 'JPEG', '', '', true, 300, '', false, false, 0, false, false, false);
    $y += 12;
    $afip->barras();//14
    $wbarras = ($this->pdf->getPageWidth() / 2) - 8;
    $this->pdf->Image(Afip::getDir() . "barras.png", 2, $y, $wbarras, 16, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
    $y += 17;
    $this->pdf->SetFont(PDF_FONT_MONOSPACED, '', 8, '', 'false');
    $this->pdf->textBox(array("stretch" => 2, "txt" => $afip->codigoDeBarras(), "x" => 2, "y" => $y, "w" => $wbarras, "align" => "C"));

//    $y+=4;
//    $this->pdf->textBox(array("txt" => "Einstein","x" => $marginLeft+13, "y" => $y));
//    $this->pdf->textBox(array("txt" => "ORIGINAL", "x" => $marginLeft + 5, "y" => $y, "align"=>"C"));

//    $width = ($this->pdf->getPageWidth());
//    $barWidth   = $width - 20;
//    $y          = 30;
//    $this->pdf->textBox(array("txt" => date("d/m/Y", mystrtotime($this->doc["fecha"])), "align" => "R", "x" => 0, "y" => $y));
//    $y += 7;
//    $numero = str_pad(3, 4, "0", STR_PAD_LEFT) . "-" . str_pad($this->doc["numero"], 8, "0", STR_PAD_LEFT);
//    $this->pdf->textBox(array("txt" => $numero, "x" => 0, "align" => "R", "y" => $y));
//    $y += 25;
////			vd($this->doc);
//    $this->pdf->textBox(array("txt" => $this->doc["nombre_completo"], "x" => $marginLeft + 5, "y" => $y));
//    $this->pdf->textBox(array("txt" => $this->doc["division_nombre"], "x" => $marginLeft + 80, "y" => $y));
//    $this->pdf->textBox(array("txt" => $this->doc["anio_nombre"], "x" => $marginLeft + 85, "y" => $y));
//    $this->pdf->textBox(array("txt" => $this->doc["matricula"], "x" => $marginLeft + 108, "y" => $y));
//    $this->pdf->textBox(array("txt" => $this->doc["nivel_nombre"], "x" => $marginLeft + 120, "y" => $y));
//    $y = 110;
//    foreach ($this->doc["apl"] as $apl) {
//      $numero  = str_pad($apl["sucursal"], 4, "0", STR_PAD_LEFT) + "-" + str_pad($apl["numero"], 8, "0", STR_PAD_LEFT);
//      $detalle = $apl["detalle"] ? $apl["detalle"] : ($apl["comprob"] . " $numero");
//      $this->pdf->textBox(array("txt" => $detalle, "x" => $marginLeft + 10, "y" => $y));
//      $this->pdf->textBox(array("txt" => $apl["importe"], "x" => $marginLeft + 116, "y" => $y, "", "align" => "R", "w" => 19));
//      $y += 7;
//    }
//    //$y += 5;
//    if ($this->doc["pago_a_cuenta_importe"] > 0) {
//      $this->pdf->textBox(array("txt" => $this->doc["pago_a_cuenta_detalle"], "x" => $marginLeft + 10, "y" => $y));
//      $this->pdf->textBox(array("txt" => $this->doc["pago_a_cuenta_importe"], "x" => $marginLeft + 116, "y" => $y, "", "align" => "R", "w" => 19));
//    }
//    $y += 10;
//    foreach ($this->doc["val"] as $val) {
//      $tipo = $val["tipo"] + 0;
//      $txt  = $val["nombre"];
////            $numero = $val["numero"];
//      if ($tipo !== "1") {
//        $txt .= " " . ($val["destino"]);
//      }
//      if ($tipo == "1") {
//        $txt .= " " . ($val["banco"]);
//      }
//
//      if ($tipo == "2") {
//        $txt .= " " . ($val["tarjeta"]);
//      }
//
//      if ($tipo == "1" or $tipo == "2") {
//        $txt .= "  #" . $val["numero"];
//      }
//      if ($tipo == "4") {
//        $txt .= " " . ($val["fecha"]);
//      }
//
//      $this->pdf->textBox(array("txt" => $txt, "x" => $marginLeft + 10, "y" => $y));
//      $this->pdf->textBox(array("txt" => $val["importe"], "x" => $marginLeft + 116, "y" => $y, "", "align" => "R", "w" => 19));
//      $y += 7;
//    }
//    $y = 179;
//
//    $this->pdf->textBox(array("txt" => $this->doc["total"], "x" => $marginLeft + 116, "y" => $y, "", "align" => "R", "w" => 19));
//    /*
//     * Código de barras
//     */
//    $style = array('text' => true, 'font' => 'helvetica', 'fontsize' => 9, 'align'=>'C');
//    $y      = $this->pdf->getPageHeight() - 28;
////    $cadena = calculaCadena($this["comprob_id"]);
//    $y    = $this->pdf->getPageHeight() - 30;
//    $this->pdf->textBox(array("txt" => $txt, "x" => $marginLeft + 10, "y" => $y));
//    $this->pdf->textBox(array("txt" => $val["importe"], "x" => $marginLeft + 116, "y" => $y, "", "align" => "R", "w" => 19));
//    $afip = new Afip(Afip::MODO_DEV, Comprob::tipoComprobElectronico($this->doc["comprob_id"]));
////    $doc  = $afip->getAfipDoc(Comprob::tipoComprobElectronico($this->doc["comprob_id"]), 3, $this->doc["numero"]);
//    $afip->barras();//14
//    $this->pdf->Image(Afip::getDir() . "barras.png", 2, $y, ($this->pdf->getPageWidth()/2)-8 , 16, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
//    //$this->pdf->write1DBarcode($cadena, 'I25', 0, $y, $barWidth, 16, 0.4, $style);
    //$pdf->textBox(array("txt" => $cadena, "x" => $margen, "y" => $y));

  }
}

function calculaCadena($doc)
{
  /*
   * El código de barras deberá contener los siguientes datos con su correspondiente orden:
  *Clave Unica de Identificación Tributaria (C.U.I.T.) del emisor de la factura (11 caracteres)
  *Código de tipo de comprobante (2 caracteres)
  *Punto de venta (4 caracteres)
  *Código de Autorización de Impresión (C.A.I.) (14 caracteres)
  *Fecha de vencimiento (8 caracteres)
  *Dígito verificador (1 carácter)
   */
  //vd($doc);
  //vd("20140679047".$comprob_id."0003".$vto);
  return "20140679047" . $comprob_id . "0003" . $vto;
}

$i = new Informe($doc);

$i->imprime($imprimeAuto, $nombre_pc, $impresora);
