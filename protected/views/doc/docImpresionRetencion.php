<?php

class MYPDF2 extends PDF {

  public $planilla;

  public function header() {

  }

  public function footer() {

  }

  public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false) {
    parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
  }

}

class Informe2 extends stdClass {

  public $pdf, $doc;

  public function __construct($doc, $paperSize = "A4") {
    $this->paperSize = $paperSize;
    $this->doc = $doc;
  }

  public function imprime($imprimeAuto, $nombre_pc, $impresora, $numero) {
    $this->pdf = new MYPDF2("P", "mm", $this->paperSize);
    $this->pdf->marginY = 4;
    $this->pdf->marginX = 2;
    $this->pdf->planilla = $this;
    $this->pdf->SetCreator(PDF_CREATOR);
    $this->pdf->SetAuthor('JP');
    $this->pdf->SetTitle('Doc');
    $this->pdf->SetSubject('Doc');
    $this->pdf->SetKeywords('TCPDF, PDF, doc');
    $this->pdf->SetPrintHeader(true);
    $this->pdf->SetPrintFooter(true);
    $this->pdf->SetAutoPageBreak(false);
    $this->fontSize = 8;
    $this->numero = $numero;
    $this->preparaPdf();
//        $this->pdf->Output("test.pdf");
      $user_id = Yii::app()->user->id;
      if ($imprimeAuto == "S" or $imprimeAuto == "P") {
        $numero = str_pad($this->doc["sucursal"], 4, "0", STR_PAD_LEFT) . "-" . str_pad($this->numero, 8, "0", STR_PAD_LEFT);
        if (!$nombre_pc) {
          throw new exception("Debe especificar el nombre de la pc en \"puesto_trabajo\" (para el puesto de trabajo del usuario $user_id) para guardar el archivo en docsAutoPrint");
        }
        if (!$impresora or $impresora == "") {
          throw new exception("Debe especificar una impresora en \"talonario\" (para el puesto de trabajo del usuario $user_id) ");
        }
//        if (gethostbyaddr("127.0.0.1") == "localhost") {
          $path = "./docsAutoPrint/$nombre_pc/";
//        } elseif (gethostbyaddr("127.0.0.1") == "jp-PC") {
//          $path = "docsAutoPrint\\$nombre_pc\\";
//        }else{
//          $path = "docsAutoPrint\\$nombre_pc\\";
//    }
//    vd2($path);
        if (!file_exists($path)) {
          if (!mkdir($path, 0777, true)) {
            $currDir = getcwd();
            throw new Exception("No pudo crear la carpeta \"$path\" dentro de \"$currDir\" en el servidor</br>");
          }
        }
        $comprob_nombre = $this->doc["comprob_nombre"];
        $file = $path . $imprimeAuto . "@" . $impresora . "@" . $comprob_nombre . "-" . $numero . '.pdf';
        $this->pdf->Output($file, 'F');
        //ve($path . $numero . '.pdf');
      } else {
        $this->pdf->Output('borrar2.pdf');
      }
    }

    private function preparaPdf() {

      /*
       * Cálculo del ancho de la columna alumnos
       */
      $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", 11);
      $this->pdf->setCellPaddings(1, 2, 0, 0);
      $this->pdf->AddPage();
      $this->fontSize = 9;
      $this->imprimeRetencion();
    }

    public function imprimeRetencion() {
      $marginLeft = 10;
      $width = $this->pdf->getPageWidth();
      $y = 11;
      $this->pdf->textBox(array("align"=>"C","txt" => "Sistema de Control de Retenciones de I.I.B.B.", "y"=>$y));
      $y = 22;
      $this->pdf->textBox(array("txt" => "Certificado Nro", "x" => $marginLeft + $width - 75, "y" => $y));
      $this->pdf->textBox(array("txt" => ": " . $this->numero, "x" => $marginLeft + $width - 45, "y" => $y));
      $y+=6;
      $this->pdf->textBox(array("txt" => "Fecha", "x" => $marginLeft + $width - 75, "y" => $y));
      $this->pdf->textBox(array("txt" => ": " . date("d/m/Y", mystrtotime($this->doc["fecha"])), "x" => $marginLeft + $width - 45, "y" => $y));
      $y+=9;
      $this->pdf->textBox(array("txt" => "A. - Datos del Agente de Retencion", "x" => $marginLeft, "y" => $y));
      $y+=9;
      $this->pdf->textBox(array("txt" => "Apellido y Nombre o Denominación: Albert Einstein SA", "x" => $marginLeft + 7, "y" => $y));
      $y+=9;
      $this->pdf->textBox(array("txt" => "C.U.I.T. Nro : 30-63939046-9", "x" => $marginLeft + 7, "y" => $y));
      $y+=9;
      $this->pdf->textBox(array("txt" => "Domicilio: Catamarca 3644 Localidad: MAR DEL PLATA C.P.:7600", "x" => $marginLeft + 7, "y" => $y));
      $y+=12;
      $this->pdf->textBox(array("txt" => "B. - Datos del Sujeto Retenido", "x" => $marginLeft, "y" => $y));
      $y+=9;
      $this->pdf->textBox(array("txt" => "Apellido y Nombre o Denominación: " . $this->doc["socio_nombre"], "x" => $marginLeft + 7, "y" => $y));
      $y+=9;
      $this->pdf->textBox(array("txt" => "C.U.I.T.Nro.:" . $this->doc['cuit'], "x" => $marginLeft + 7, "y" => $y));
//			$this->pdf->textBox(array("txt" => "C.U.I.T.Nro.:" . $this->doc['cuit'], "x" => $marginLeft + 7, "y" => $y));
    $this->pdf->textBox(array("txt" => "Ing. Brutos Nro.: " . $this->doc['nro_ingresos_brutos'], "x" => $marginLeft + 75, "y" => $y));
    $y += 9;
    $this->pdf->textBox(array("txt" => "Domicilio:" . $this->doc['domicilio'] . " Localidad: " . $this->doc['localidad'] . " C.P.:" . $this->doc['codigo_postal'], "x" => $marginLeft + 7, "y" => $y));

    $y += 10;
    $this->pdf->textBox(array("txt" => "C.- Datos de la Retención Practicada", "x" => $marginLeft, "y" => $y));
    $y += 9;
    $this->pdf->textBox(array("txt" => "Impuesto", "x" => $marginLeft + 7, "y" => $y));
    $this->pdf->textBox(array("txt" => ": " . "Impuesto a los Ingresos Brutos", "x" => $marginLeft + 110, "y" => $y));
    $y += 9;
    $this->pdf->textBox(array("txt" => "Régimen", "x" => $marginLeft + 7, "y" => $y));
    $this->pdf->textBox(array("txt" => ": " . "Retención IIBB", "x" => $marginLeft + 110, "y" => $y));
    $y += 9;
    $this->pdf->textBox(array("txt" => "Comprobante que Origina la Retención", "x" => $marginLeft + 7, "y" => $y));
    $this->pdf->textBox(array("txt" => ": Recibo Nro. 0001-" . str_pad($this->doc["numero"], 8, "0", STR_PAD_LEFT), "x" => $marginLeft + 110, "y" => $y));
    $y += 9;
    $this->pdf->textBox(array("txt" => "Monto del Comprobante que Origina la Retención", "x" => $marginLeft + 7, "y" => $y));
    $this->pdf->textBox(array("txt" => ": $" . $this->doc["total"], "x" => $marginLeft + 110, "y" => $y));
    $y += 9;
    $this->pdf->textBox(array("txt" => "Monto de la Retención", "x" => $marginLeft + 7, "y" => $y));
    $this->pdf->textBox(array("txt" => ": $" . $this->doc["retencion"]["importe"], "x" => $marginLeft + 110, "y" => $y));

    $y = $this->pdf->getPageHeight() - 47;
    ;
    $this->pdf->textBox(array("txt" => "Firma del Agente de Retención", "x" => $marginLeft + 7, "y" => $y));
    $y += 12;
    $this->pdf->textBox(array("txt" => "Aclaración:", "x" => $marginLeft + 7, "y" => $y));
    $y += 7;
    $this->pdf->textBox(array("txt" => "Cargo:", "x" => $marginLeft + 7, "y" => $y));

    $y += 10;
    $this->pdf->textBox(array("align" => "C", "border" => 1, "txt" => "Declaro que los datos consignados en este Formulario son correctos y completos sin omitir ni falsear dato alguno que deba contener, siendo fiel expresión de la verdad", "x" => $marginLeft + 7, "y" => $y));
  }

}

$i = new Informe2($doc);

$i->imprime($imprimeAuto, $nombre_pc, $impresora, $numero);

