<?php
if (isset($docDet)) {
  agregaDocDet($docDet);
  return;
}
?>
<table class="factura-detalle-table">
  <tr>
    <th>Artículo</th>
    <th>Cantidad</th>
    <th>Importe</th>
    <th>Total</th>
  </tr>
  <?php agregaDocDet(new DocDet()); ?>
  <?php

  function agregaDocDet($dd) { ?>
  <?php $docDetID = uniqid(); ?>
  <?php /* @var $dd DocDet */; ?>
    <tr class="doc-det-tr">
      <td class="doc-det-id"><input 
          class="doc-det-id"
          type="hidden" name='doc-det[<?php echo $docDetID; ?>][doc_det_id]'
          value="<?php echo $dd->id; ?>"/></td>
      <td class="doc-det-articulo-id-td"><input 
          class="doc-det-articulo-id"
          type="hidden" name='doc-det[<?php echo $docDetID; ?>][articulo_id]'
          value="<?php echo $dd->articulo_id; ?>"/></td>
      <td><input type="text" class="doc-det-nombre" name='doc-det[<?php echo $docDetID; ?>][detalle]'
                 value="<?php echo $dd->articulo ? $dd->articulo->nombre : ""; ?>"/></td>
      <td><input class="doc-det-cantidad" type='text' name="doc-det[<?php echo $docDetID; ?>][cantidad]"
                 value="<?php echo $dd->cantidad; ?>"/></td>
      <td><input class="doc-det-importe" type='text' name='doc-det[<?php echo $docDetID; ?>][importe]'
                 value="<?php echo $dd->importe; ?>"/></td>
      <td><input class="doc-det-total" type='text' name='doc-det[<?php echo $docDetID; ?>][total]'
                 value="<?php echo $dd->total; ?>" disabled="disabled"/></td>
    </tr>
    <?php
  }

  ;
  ?>
</table>