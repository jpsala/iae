<style type="text/css">
    .datos{ background-color: #F9FAFC;
            border: 0 solid #EFF3FC;
            border-radius: 5px 5px 5px 5px;
            padding: 5px;
            position: relative;}

    labelHighlight{color: #AAA; font-style: italic;}
    #content{padding:10px 5px 0px !important}

    #main-form {padding: 5px;margin: 5px 0px 5px; border: 1px #75C2F8 solid; border-radius: 8px ; position: relative;overflow: visible;min-height: 425px}

    #izq{ width: 67%;; float: left; }
    #der{   width: 30%;  float: right; margin-right: 13px}
    #div-head{padding: 5px}
    #div-head div{display:inline-block}
    #div-head label{ float: left;  width: 48px;}
    #div-head #socio{width: 300px; }
    #div-pago #pago{width: 60px; text-align: right;margin-left: -3px;}
    #factura-detalle-div,#pago-a-cuenta-div, #total-pagos-div,#total-div,
    #forma-de-pago-div {border-radius: 5px; background-color: #f9fafc; width: 100%;  border: #eff3fc solid 1px; padding: 5px;position: relative}

    #factura-detalle-div{min-height: 345px}
    #factura-detalle-div th, #factura-detalle-div td, #factura-detalle-div caption {padding: 4px 3px 4px 5px !important}
    .titulo{ background-color: #C3D9FF;    border-bottom: 1px solid #CCCCCC;    border-bottom: 1px solid #EFF3FC;    border-radius: 5px 5px 5px 5px;  margin-bottom: 5px;    padding: 3px;    text-align: center;color: #555555;  font-weight: bold;}

    #factura-detalle-div docs table thead tr th{text-align: right}
    #factura-detalle-div docs table thead tr th:first-child{text-align: left}
    #factura-detalle-div th.detalle{min-width: 110px; text-align: left}
    #factura-detalle-div td.pago{padding-right: 0 !important;  text-align: right;}
    #factura-detalle-div td.pago input{width: 65px; text-align: right}

    #pago-a-cuenta-div #pago_a_cuenta_detalle{width: 79%}
    #pago-a-cuenta-div #pago_a_cuenta_importe{    position: absolute;    right: 6px;    text-align: right;    width: 95px;}

    #total-div span.total, #total-pagos-div span.total{    position: absolute;    right: 4px; font-weight: bold}

    #botones{ clear: both;margin:11px 0px 2px 11px; position: relative; text-align: right; top: 0;}
    .esNuevo{color:red}
    #cheque-form-template{display: none}
    #div-cheque-genera-interes{display: inline}
    #div-cheque-genera-interes label{display: inline; width:300px !important}
    .valor-tipo{width: 50px}
    .valor-importe{width: 80px; text-align: right}
    .right-align {  text-align: right }

    .ui-widget { font-size: 12px; }  
    .bold{font-weight: bold}
    .oculto{display: none}
    .total{background-image: url("images/pesos.gif");background-position: 0 50%;background-repeat: no-repeat;background-size: 10px auto;padding-left: 15px;}


    input::-webkit-input-placeholder, textarea::-webkit-input-placeholder {
        color: #636363;
    }
    input:-moz-placeholder, textarea:-moz-placeholder {
        color: #636363;
    }

    #contextMenu, #nota-de-credito{display:none}
    #nota-de-credito{overflow: visible}
    #nota-de-credito form label{width: 200px}
    #nota-de-credito form select,#nota-de-credito form input{width: 300px;display: block;}
    #concepto_chzn{display:block;margin: 10px 0;}

    .doc-det-cantidad{width:50px;text-align: right}
    .doc-det-total, .doc-det-importe{width:70px;text-align: right}
    .doc-det-nombre{width:380px}
    .doc-det-articulo-id-td{display:none}
    .doc-det-id{display:none}
    .fecha{width:80px}
    #concepto_chzn{display:block;margin: 10px 0;}
    #tree{
        padding-bottom: 10px;
        padding-left: 17px;
        padding-right: 0;
        padding-top: 10px;
    }
    #concepto-dlg{display:none}
    a.jqtree_common{cursor:pointer;text-decoration: none}
    span.jqtree_common{cursor:pointer;text-decoration: none}
    #concepto-button {height:24px;width:24px;vertical-align: top}
    #numero{width:110px}
    #concepto-button .ui-button-text-only,#concepto-button .ui-button-text {
        padding-left: 5px;
        padding-bottom: 3px;
        padding-top: 1px;
    }
    #concepto-select{width:200px}
    #concepto_select_chzn{vertical-align: sub}
    .docs table tr td{padding:0!important}
    #letra{width:40px}
</style>
