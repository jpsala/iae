<link rel="stylesheet" type="text/css" href="js/jqTree/jqtree.css" media="" />
<script src="js/jqTree/tree.jquery.js"></script>
<?php
$baseUrl = Yii::app()->request->baseUrl;
$cs = Yii::app()->clientScript;
$cs->registerScriptFile($baseUrl . "/js/jquery.contextmenu.r2.packed.js", CClientScript::POS_END);
$cs->registerScriptFile($baseUrl . "/js/common.js", CClientScript::POS_BEGIN);
?>
<script type="text/javascript">
<?php $socioTipo = ($tipo == Comprob::FACTURA_VENTAS ? "alumno" : "proveedor"); ?>
    var urlAC = "<?php echo $this->createUrl('socio/sociosAc'); ?>&socioTipo=<?php echo $socioTipo; ?>";
    var urlACArt = "<?php echo $this->createUrl('doc/articuloAc'); ?>&artTipo=<?php echo $tipo; ?>";
    var host = "<?php echo gethostbyaddr('127.0.0.1'); ?>";
    var tipo = "<?php echo $tipo; ?>";
    var doc = new Object();
    var socio_id = null;
    var afectacion = "<?php echo $doc->talonario->comprob->signo_caja; ?>";
//  var urlDoc = "<?php echo ($tipo == Comprob::FACTURA_VENTAS ? $this->createUrl('doc/facturaVenta') : $this->createUrl('doc/facturaCompra')); ?>";
    doc.totalDetalle = 0, doc.total = 0;
    doc.saldo = 0, doc.disabled = false;
    doc.toggleEnabled = function(on) {
        doc.disabled = !on;
        if (on) {
            $(".td_edit_img *").removeAttr('disabled');
            $(".td_delete_img *").removeAttr('disabled');
            $("#pago").removeAttr('disabled');
            $("#izq *").removeAttr('disabled');
            $("#botones *").removeAttr('disabled');
            $("#pago-a-cuenta-div *").removeAttr('disabled');
            $(".doc-det-total").attr("disabled", "disabled");
        } else {
            $(".td_edit_img *").attr('disabled', "disabled");
            $(".td_delete_img *").attr('disabled', "disabled");
            $("#pago").attr('disabled', "disabled");
            $("#izq *").attr('disabled', "disabled");
            $("#botones *").attr('disabled', "disabled");
            $("#botones #CtaCte").removeAttr('disabled');
            $("#botones #cancel").removeAttr('disabled');
        }
        return false;
    }
    doc.muestraTotales = function() {
//    doc.saldo = round(Number(doc.totalAPagar) - Number(doc.totalDetalle));
        doc.total = round(Number(doc.totalDetalle));


        $("form #total-pagos-div #total-a-pagar span.total").html(round(doc.totalDetalle));
        $("form #total-div span.total").html(round(doc.total));
        $("#total-pagos").html(doc.total);

//    $("form #div-pago #pago").val(round(doc.total));
        //    $("#total-pagos-div div#total-a-pagar span.total").html(doc.total);

    }

    jQuery(document).ready(function() {
        onReady();
    });

    function onReady() {

        igualaAlto();


        valores.init();


        $(".fecha").datepicker({onSelect: function() {
                $(".fecha").datepicker("hide");
                setFocusAndSelect($("#concepto_ac"));
            }});

        $(":button").button();

        $('#socio').focus().autocomplete({
            autoSelect: true, autoFocus: true,
            minLength: 2, source: urlAC,
            select: function(event, ui) {
                socioACSelect(event, ui);
            }
        });

        $("body").on("change", "input", function() {
            $("#socio").attr('disabled', "disabled");
        });

        $("#doc-pendientes").on("focus", ".pago input", function() {
            ant = $(this).val()
        })

        $("#doc-pendientes").on("change", ".pago input", function() {
            pagoPendienteChange($(this));
        });

        $("#nota-de-credito select").chosen();

        acNombreArt();

        $(".factura-detalle-table tbody").on("change", "tr .doc-det-cantidad", function() {
            docDetCantidadChange($(this));
        })

        $(".factura-detalle-table tbody").on("change", "tr .doc-det-importe", function() {
            docDetImporteChange($(this));
        })

        $(".factura-detalle-table tbody").on("keyup", ".doc-det-nombre,.doc-det-importe,.doc-det-cantidad", function(event) {
            docDetAbajo(event.keyCode, $(this));
        });

        $(".factura-detalle-table tbody").on("focus", "tr .doc-det-total", function() {
            docDetAbajo(40, $(this));
        });

        $("#numero").change(function() {
            validaNumeroComprob($(this));
        });

    }

    function socioACSelect(event, ui) {
        socio_id = ui.item.id;
        setTimeout(function() {
            $("#doc_fecha").focus();
        }, 200);
    }

    function docDetAbajo(keyCode, $this) {
        var acActivo = ($(".ui-autocomplete .ui-menu").length > 0);
        if (!acActivo) {
            if (keyCode === 40) {
                setFocus($this.closest("tr").next("tr").find(".doc-det-nombre"));
            } else if (keyCode === 38) {
                setFocus($this.closest("tr").prev("tr").find(".doc-det-nombre"), 20);
            }
        }
        return false;
    }

    function chkDocDet() {
        var hayNuevo = false, $articulo_id, articulo_id;
        $(".doc-det-tr").each(function() {
            $id = $tr.find(".doc-det-id");
            $articulo_id = $(this).find(".doc-det-articulo-id");
            articulo_id = $articulo_id.val();
            if (!articulo_id) {
                hayNuevo = true;
            }
        });
        if (!hayNuevo) {
            traeNuevoDocDet();
        }
    }

    function traeNuevoDocDet() {
        $.ajax({
            type: "GET",
            data: {id: GUID()},
            url: "<?php echo $this->createUrl("doc/getDocDetVacia"); ?>",
            success: function(data) {
                $(".factura-detalle-table tbody").append(data);
                ;
                acNombreArt();
            },
            error: function(data, status) {
            }
        });
    }

    function acNombreArtSelect(event, ui, $this) {
        $tr = $this.closest("tr");
        $this.val(ui.item.value);
        $id = $tr.find(".doc-det-id");
        $articulo_id = $tr.find(".doc-det-articulo-id");
        $cant = $tr.find(".doc-det-cantidad");
        $importe = $tr.find(".doc-det-importe");
        if (!$cant.val() || $cant.val() === 0) {
            $cant.val(1);
        }
        $importe.val(ui.item.precio);
        $articulo_id.val(ui.item.id);
        total = ui.item.precio * $cant.val();
        $tr.find(".doc-det-total").val(round(total, 2));
        chkDocDet();
        calcTotal();
        setFocusAndSelect($tr.find(".doc-det-cantidad"));
    }

    function docDetImporteChange($this) {
        var $tr, total;
        $tr = $this.closest("tr");
        $trNext = $tr.next("tr");
        total = $tr.find(".doc-det-cantidad").val() * $this.val();
        $this.val(round($this.val(), 2));
        $tr.find(".doc-det-total").val(round(total, 2));
        calcTotal();
        setFocus($trNext.find(".doc-det-nombre", 5));
    }

    function docDetCantidadChange($this) {
        var $tr, total;
        $tr = $this.closest("tr");
        $importe = $tr.find(".doc-det-importe");
        total = $importe.val() * $this.val();
        $tr.find(".doc-det-total").val(round(total, 2));
        calcTotal();
        setFocus($tr.find(".doc-det-importe", 5));
    }

    function calcTotal() {
        doc.totalDetalle = 0;
        $(".factura-detalle-table tr").each(function() {
            importe = $(this).find(".doc-det-total").val()
            if (importe) {
                doc.totalDetalle += Number(importe);
            }
        });
        doc.muestraTotales();
        valores.refresh();
    }

    function acNombreArt() {
        var $id, $cant, total;
        $(".doc-det-nombre").each(function() {
            $(this).autocomplete({
                autoSelect: true, autoFocus: true,
                minLength: 0, source: urlACArt,
                select: function(event, ui) {
                    acNombreArtSelect(event, ui, $(this));
                }
            });
        })
    }

    function limpiaTodo() {
        valores.toggleEnabled(1);
        doc.toggleEnabled(1);
        $("#socio").removeAttr('disabled');
        $("#doc-pendientes div.docs table tbody").remove();
        $("#izq .total").html("0");
        $("#pago").val("");
        $("#socio").val("").focus();
        $("#pago-a-cuenta-div input").val("");
        $("#div-head").show();
        doc.totalDetalle = 0;
        doc.totalAPagar = 0;
        doc.disabled = false;
        doc_id = null;
        valores.limpia();
    }

    function reporteCtaCte() {
        var urlImpresionCtaCte = "<?php echo $this->createUrl("/doc/reciboCtaCte"); ?>";
        $.ajax({
            type: "GET",
            data: {socio_id: socio_id},
            url: urlImpresionCtaCte,
            success: function(data) {
                $("#rep-ctacte").html(data);
                $("#rep-ctacte").dialog({visible: true, title: "Reporte de Cta.Cte.", width: "auto"});
            },
            error: function(data, status) {
                alert("Error/es:\n" + data)
            }
        });
    }

    function submit() {
//    var docDet = $("")
        $(".doc-det-total").removeAttr("disabled");
        $(".factura-detalle-table tr").each(function() {
            if ($(this).find(".doc-det-nombre").val() === "")
                $(this).remove();
        });
        var data = $("#main-form").find("input").serialize();
        $(".doc-det-total").attr("disabled", "disabled");
        //var urlReciboImpresion = "<?php //echo $this->createUrl("/doc/reciboImpresion&doc_id=%doc_id%");                                          ?>";
        data += "&doc[tipo]=" + tipo;
        data += "&doc[total]=" + doc.totalDetalle;
        data += "&doc[Socio_id]=" + socio_id;
        $.ajax({
            type: "POST",
            data: data + valores.getData(),
            dataType: "json",
            url: "<?php echo $this->createUrl("doc/facturaGraba"); ?>",
            success: function(data) {
                if (data.errores && muestraErroresDoc(data.errores)) {
                    return false;
                }
                $("#div-head *").removeAttr('disabled');
                //window.location=urlReciboImpresion.replace("%doc_id%",data.id);
                //window.location = urlDoc;
                window.location = window.location;

            },
            error: function(data, status) {
            }
        });

        return false;
    }


    function igualaAlto() {
        i = $("#izq").height(), d = $("#der").height();
        if (d < i) {
            $("#der").height(i);
        } else if (d > i) {
            $("#izq").height(d);
        }
    }

</script>

