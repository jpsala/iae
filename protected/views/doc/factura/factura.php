<?php $this->renderPartial("/doc/common.js"); ?>
<?php //$this->renderPartial("/doc/common.css");  ?>
<?php include("factura.css.php"); ?>
<?php include("factura.js.php"); ?>
<?php $this->renderPartial("/doc/valores/valores.css"); ?>
<?php $this->renderPartial("/doc/valores/valores.js"); ?>
<?php /* @var $doc Doc */; ?>
<form id="main-form">
    <div class="datos">
        <div class="titulo">
            <span><?php echo $this->pageTitle; ?></span>
        </div>
        <input name="doc[comprob_id]" type="hidden" id="comprob_id" value="<?php echo $tipo; ?>" placeholder=""/>
        <div id="div-head">
            <div id="div-socio">
                <!--            <label for="socio"></label>-->
                <input name="doc[socio]" id="socio" value="" placeholder="<?php echo $tipo == Comprob::FACTURA_VENTAS ? "Cliente" : "Proveedor"; ?>"/>
            </div>
            <div id="div-pago">
                <input type="texto" class="fecha" name="doc[fecha]" id="doc_fecha" value="<?php echo date("d/m/Y", time()); ?>" placeholder="Fecha"/>
                <?php Concepto::treeAndInput(); ?>
                <div id="div-numero">
                    <input type="text" id="letra" value="<?php echo $doc->letra; ?>" name="doc[letra]" placeholder="Letra"/>
                    <input name ="doc[numero]" id="numero" value="<?php echo $doc->numeroCompleto; ?>" placeholder="Número"/>
                </div>
            </div>
        </div>
    </div>
    <div id="izq">
        <div id="factura-detalle-div">
            <div class="titulo">
                <span>Detalle</span>
            </div>
            <div class="docs">
                <?php $this->renderPartial("/doc/factura/_factura_detalle"); ?>
                <?php //include("_factura_detalle.php"); ?>
            </div>
        </div>
        <div id="total-pagos-div">
            <div id="total-a-pagar">
                <span class="label">Total</span>
                <span class="total">0</span>
            </div>
        </div>
    </div>


    <div id="der">
        <?php //include("/../valores/_valores.php"); ?>
        <?php $this->renderPartial("/doc/valores/_valores"); ?>
    </div>
</form>

<div id="botones">
    <input type="button" id="CtaCte" value="CtaCte" onclick="return reporteCtaCte();"/>
    <input type="button" id="cancel" value="Cancela" onclick="return limpiaTodo();"/>
    <input type="button" id="ok" value="Graba Factura" onclick="return submit();"/>
</div>

<div id="contextMenu">

    <ul>

        <li id="open">Nota de crédito</li>

    </ul>

</div>

<div id="rep-ctacte">
</div>

<div id="nota-de-credito">
    <form method="POST" action="#">
        <input type="text" name="importe" placeholder="Importe"/>
        <select id="concepto" data-placeholder="Concepto" name="concepto" class="chzn-select">
            <?php
            $x = array('prompt' => '');
            echo CHtml::listOptions(null, CHtml::listData(Concepto::model()->findAll(), 'id', 'nombre'), $x)
            ?>
        </select>
        <input type="text" name="detalle" placeholder="Observaciones"/>
    </form>
</div>

<div id='concepto-dlg'>
    <div id="tree" class='ui-widget ui-widget-content ui-corner-all'></div>
</div>
<script type="text/javascript">
      igualaPos();
</script>