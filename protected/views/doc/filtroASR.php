<style>
    .filtro-td { width: 10px }
    .filtro-td input { width: 10px }
</style>
<link rel="stylesheet" href="js/bootstrap-datepicker.css"/>
<script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
<!--<script  type="text/javascript" src="js/bootstrap-datepicker/bootstrap-datepicker.js"></script>-->
<div class="container top15">
    <div class="panel panel-default">
        <div class="panel-heading">
            <span class="panel-title">Filtro para ASR!!!</span>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-3 col-md-2">
                    <input autofocus type="text" class="form-control input input-sm"
                           placeholder="Filtro"
                           data-bind="value: filtro, valueUpdate:'input'"/>
                </div>
                <div class="col-xs-3 col-md-2">
                    <input class="datepicker" id="desde" type="text"
                           class="form-control input input-sm"
                           placeholder="Desde Fecha"
                           data-bind="value: filtroDesde, valueUpdate:'change'"/>
                </div>
                <div class="col-xs-3 col-md-2">
                    <input class="datepicker" id="hasta" type="text"
                           class="form-control input input-sm"
                           placeholder="Hasta Fecha"
                           data-bind="value: filtroHasta, valueUpdate:'change'"/>
                </div>
                <div class="col-xs-4 col-md-5">
                    <div class="from-group form-inline">
                        <label for="talonarios">Comprobantes</label>
                        <select class="form-control" id="talonarios"
                                data-bind="options: talonarios,optionsText:'nombre',
                                           optionsCaption: 'Todos', value:talonariosVal"></select>
                    </div>
                </div>
            </div>
            <div class="row top15">
                <div class="col-xs-12 col-md-12">
                    <table
                        class="table table-condensed table-hover table-striped table-responsive table-bordered">
                        <thead>
                        <tr>
                            <th>Comprobante</th>
                            <th>Fecha valor</th>
                            <th>Alumno</th>
                            <th>Destino</th>
                            <th>Banco</th>
                            <th>Total</th>
                            <th>Filtrado</th>
                        </tr>
                        </thead>
                        <tbody>
                        <!-- ko foreach: recibos -->
                        <tr>
                            <td data-bind="text:comprob"></td>
                            <td data-bind="text:fecha_valor"></td>
                            <td data-bind="text:alumno"></td>
                            <td data-bind="text:destino"></td>
                            <td data-bind="text:banco"></td>
                            <td class="right-align" data-bind="text:total"></td>
                            <td class="filtro-td">
                                <input type="checkbox" data-bind="checked:filtrado"/>
                            </td>
                        </tr>
                        <!-- /ko  -->
                        <tr data-bind="visible: filtrados().length > 0 ">
                            <td colspan="5">Total filtrados</td>
                            <td class="right-align"
                                data-bind="text:Math.round(totalFiltrados() * 100) / 100"></td>
                            <td></td>
                        </tr>
                        <tr data-bind="visible: noFiltrados().length > 0 ">
                            <td colspan="5">Total no filtrados</td>
                            <td class="right-align"
                                data-bind="text:Math.round(totalNoFiltrados() * 100) / 100"></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    vm = (function () {
        self = this;
        pub = {};
        pub.recibos = ko.observableArray([]);
        pub.filtro = ko.observable();
        pub.filtroDesde = ko.observable();
        pub.filtroHasta = ko.observable();
        pub.filtrados = pub.recibos.filter(function (e) {
            return e.filtrado();
        });
        pub.noFiltrados = pub.recibos.filter(function (e) {
            return !e.filtrado();
        });
        pub.reload = ko.computed(function () {
            var data = {};
            data.filtro = ko.utils.unwrapObservable(pub.filtro) || "";
            data.desde = ko.utils.unwrapObservable(pub.filtroDesde);
            data.hasta = ko.utils.unwrapObservable(pub.filtroHasta);
            data.talonarios = pub.talonariosVal && pub.talonariosVal() && pub.talonariosVal().id;
            if (data.filtro.length > 1 || (data.desde && data.hasta)) {
                var cargando = toastr.info("Bajando registros...", "", {timeOut: 5000});
                $.getJSON(self.params.urlRecibos, data, function (data) {
                    cargando.remove();
                    if (!data.recibos) {
                        toastr.error("Demasiados registros(" + data.cant + ")...", "", {timeout: 800});
                        return;
                    }
                    var t = toastr.info("cargando " + data.cant + " registros...", "", {timeOut: 3000});
                    ko.mapping.fromJS(data.recibos, {
                        create: function (val) {
                            val.data = ko.mapping.fromJS(val.data, {});
                            val.data.filtrado(val.data.filtrado() === 'true' ? true : false);
                            val.data.filtrado.subscribe(function () {
                                self.togleFiltrado(val.data);
                            });
                            return val.data;
                        }
                    }, pub.recibos);
                    setTimeout(function () {
                        t.remove();
                    }, 1500);
                });
            } else {
                pub.recibos.removeAll();
            }
        }).extend({rateLimit: {timeout: 800, method: "notifyWhenChangesStop"}});
        pub.talonarios = ko.observableArray();
        pub.talonariosVal = ko.observable(-1);
        self.togleFiltrado = function (o) {
            $.post(params.urlGrabaFiltro, {doc_id: o.doc_id, filtrado: o.filtrado});
        };
        self.getTalonarios = function () {
            $.ajax({
                url: self.params.urlTalonarios,
                dataType: 'json',
                success: function (data) {
                    ko.wrap.updateFromJS(pub.talonarios, data);
                }
            })
        };
        pub.totalFiltrados = ko.computed(function () {
            var total = 0;
            $.each(pub.filtrados(), function () {
                console.log(this.total());
                total += Number(this.total())
            });
            return total;
        });
        pub.totalNoFiltrados = ko.computed(function () {
            var total = 0;
            $.each(pub.noFiltrados(), function () {
                total += Number(this.total())
            });
            return total;
        });
        pub.init = function (params) {
            self.params = params;
            self.getTalonarios();
        };
        return pub;
    })();

    vm.init({
        urlRecibos: "<?php echo $this->createAbsoluteUrl("/doc/filtroRecibos"); ?>",
        urlGrabaFiltro: "<?php echo $this->createAbsoluteUrl("/doc/filtroGraba"); ?>",
        urlTalonarios: "<?php echo $this->createAbsoluteUrl("/doc/filtroRecibosTiposComprob"); ?>"
    });
    ko.applyBindings(vm);
    $("input:first").focus();

    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var desde = $('#desde').datepicker().on('changeDate', function (ev) {
        var newDate = new Date(ev.date);
        if (ev.date.valueOf() > hasta.date.valueOf()) {
            newDate.setDate(newDate.getDate() + 1);
            hasta.setValue(newDate);
        } else {
            hasta.setValue(hasta.date);
        }
        pub.filtroDesde(ev.date.toLocaleDateString());
        desde.hide();
        hasta.show();
    }).on("focus", function (ev) {
        hasta.hide();
    }).data('datepicker');

    var hasta = $('#hasta').datepicker({
        onRender: function (date) {
            return date.valueOf() < desde.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        pub.filtroHasta(ev.date.toLocaleDateString());
        hasta.hide();
    }).on("focus", function (ev) {
        desde.hide();
    }).data('datepicker');

</script>