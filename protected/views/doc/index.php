<link rel="stylesheet" type="text/css" href="js/jqgrid/css/ui.jqgrid.css" />
<script src="js/jqgrid/js/i18n/grid.locale-es.js"></script>
<script src="js/jqgrid/js/jquery.jqGrid.min.js"></script>

<style type="text/css">
    #busqueda{padding: 5px;margin: 5px 0px 5px; border: 1px #75C2F8 solid; padding: 5px 5px 5px 5px;  border-radius: 8px; border-shadow: 5px}
    #busqueda label{float: left; width: 100px;line-height: 2em;    margin-right: 14px; text-align: right; padding-top: 4px;}
    #busqueda #socio{width: 300px}
    #jqgrid{width: 100%}
    .ui-widget { font-size: 12px; }    
</style>    

<div class="form">
    <form method="POST" action="#">
        <div class="row" id="busqueda">   
            <label for="socio"><?php echo $socioTipo; ?></label>
            <input id="socio" value=""/>
        </div>
    </form>
</div>
<div id="jqgrid">
    <table id="grid"></table>
    <div id="gridpager"></div>
</div>


<script type="text/javascript">
    var urlAC = "<?php echo $this->createUrl('socio/sociosAc'); ?>&socioTipo=<?php echo $socioTipo; ?>";
    var urlGrid = '<?php echo $this->createUrl("doc/docsParaJqGrid"); ?>&model=Doc';
    
    jQuery(document).ready(function(){ 
        $('#socio').focus().autocomplete({
            autoSelect: true,
            autoFocus: true,
            minLength: 2,
            source:urlAC,
            select: function( event, ui ) {
                //$('#nombreProveedor,').html(ui.item.label);
                urlGrid = urlGrid+'&socio_id='+ui.item.id;
                $('#grid').setGridParam({url:urlGrid}); $('#grid').trigger('reloadGrid'); 
            }
        });        
        jQuery("#grid").jqGrid({
            url:urlGrid,
            datatype: "json",
            mtype: 'GET',
            colNames:['numero',"total"],
            colModel:[
                //                {name:'id',index:'id', width:55, sortable:false, editable:false, editoptions:{readonly:true,size:10}},
                {name:'numero',index:'numero', width:100,editable:true},
                {name:'total',index:'total', width:100,editable:true},
            ],
            rowNum:10,
            rowList:[10,20,30],
            pager: jQuery('#gridpager'),
            sortname: 'numero',
            viewrecords: true,
            sortorder: "asc",
            caption:"",
            width: 800,
            editurl:urlGrid
        }).navGrid('#gridpager');
    });
</script>


