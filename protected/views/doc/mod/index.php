<link  rel="stylesheet" href="js/select2/select2.css"/>
<script  type="text/javascript" src="js/knockout-3.1.0.js"></script>
<script  type="text/javascript" src="js/knockout-deferred-updates.min.js"></script>
<script  type="text/javascript" src="js/knockout-projections.min.js"></script>
<script  type="text/javascript" src="js/views/doc/mod.js"></script>
<script  type="text/javascript" src="js/select2/select2.min.js"></script>

<?php $optDestino = array("empty" => "Destino"); ?>
<?php $dataDestino = CHtml::listData(Destino::model()->findAll(/* "concepto_id = 1834" */), "id", "nombre"); ?>
<?php $optionsDestino = CHtml::listOptions("", $dataDestino, $optDestino); ?>
<?php $optConcepto = array("empty" => "Concepto"); ?>
<?php $dataConcepto = CHtml::listData(Concepto::model()->findAll(/* "concepto_id = 1834" */), "id", "nombre"); ?>
<?php $optionsConcepto = CHtml::listOptions("", $dataConcepto, $opt); ?>
<div class="container">
	<div>
		<h2>Modificación de documentos</h2>
	</div>
	<div class="row form-inline">
		<div class=" form-group top10">
			<label for="numero">Número</label>
			<input id="numero" class="form-control input-sm" data-bind="value:data.numero,disable:true "/>
		</div>
		<div class=" form-group top10">
			<label for="fecha">Fecha</label>
			<input id="fecha" class="form-control input-sm" data-bind="value:data.fecha_valor,disable:true "/>
		</div>
	</div>
	<div class="row top10">
		<div class=" form-group">
			<label for="concepto_id">Concepto</label>
			<select data-bind="value: data.concepto_id" name="concepto_id" id="concepto_id">
				<?php echo $optionsConcepto; ?>
			</select>
		</div>
	</div>
	<div class="row top20">
		<table class="table table-condensed table-hover table-striped">
			<thead>
				<tr>
					<th>Tipo</th>
					<th>Fecha</th>
					<th>Destino</th>
				</tr>
			</thead>
			<tbody data-bind="foreach: data.valores">
				<tr>
					<td data-bind="text: tipo"></td>
					<td data-bind="text: fecha"></td>
					<td>
						<select data-bind="value: destino_id" name="destino_id" class="destino">
							<?php echo $optionsDestino; ?>
						</select>
					</td>
				</tr>
			</tbody>
		</table>

	</div>
</div>

<script  type="text/javascript">
	var url = "<?php echo $this->createUrl("doc/modData"); ?>";
	docViewModel.init(url, "120542").done(function(){
		ko.applyBindings(docViewModel);
		$("#concepto_id").select2({width:200});
//		$("#concepto_id").select2("val", docViewModel.data.concepto_id());
		$(".destino").select2({width:200});
	});
</script>