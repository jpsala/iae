<style type="text/css">
    #modifica-valor-div{ overflow:visible !important; }
    .inline{display:inline}
    .tipo{width:3px}
    #modifica-dialog #mod-form{}
    #modifica-dialog .row{display:block;margin:3px}
    #modifica-dialog .inline{display:inline-block;margin:3px}
    #modifica-dialog label{float: left;line-height: 24px; margin-right: 5px;text-align: right;width: 74px;}
    #mod-fecha-valor-div label {text-align: right !important; width: 68px}
    #pago-a-cuenta-detalle input{width: 380px}
    #pago-a-cuenta-importe > label {width: 48px !important;}
    #concepto_ac{width:250px}
    #mod-detalle{width:380px}
    #mod-det{min-height: 250px;max-height: 350px;
             overflow: auto}
    #mod-det .fecha{width:120px}
    .mod-tipo-td{width:150px}
    .mod-detalle-td{width:80px}
    .mod-numero-td, .mod-numero-td input{ text-align: right;width: 45px;}
    .mod-banco-td, .mod-banco-td span{width:150px}
    .mod-importe-td{text-align: right;width: 85px}
    .mod-chq-origen{width:72px}
    .fecha{width:90px}
    #mod-table .modifica{width: 1px !important; cursor:pointer}
    #destino_id{width: 200px}
    /*.ui-dialog{max-height: 400px}*/
</style>
<form id="mod-form" name="mod-form">
    <input type="hidden" name="doc_id" value="<?php echo $doc["id"]; ?>"/>
    <div class="row">
        <label for="mod-numero">Numero:</label>
        <input id="numero"  name="cab[numero]" value="<?php echo $doc["numero"]; ?>"/>
    </div>
    <?php if ($doc["destino_id"]): ?>
        <div class="row">
            <label for="destino_id">Destino:</label>    
            <select id="destino_id" name="cab[destino_id]" data-placeholder="Seleccione una Cuenta Destino" class="chzn-select">
                <?php
                $x = array('prompt' => '');
                $t = '2';
                echo CHtml::listOptions($doc["destino_id"], CHtml::listData(Destino::model()->findAll("tipo = $t"), 'id', 'numero_cuenta'), $x)
                ?>
            </select>
        </div>
    <?php endif; ?>
    <?php
    $tipo = null;
    $val = null;
    ?>

    <div class="row">
        <label for="mod-total">Total:</label>
        <input id="mod-total"  name="cab[total]" value="<?php echo $doc["total"] ?>"/>
    </div>
    <div class="inline">
        <label for="mod-fecha-creacion">Fecha:</label>
        <input id="mod-fecha-creacion"  class="fecha" name="cab[fecha_creacion]" value="<?php echo date("d/m/Y", mystrtotime($doc["fecha_creacion"])); ?>"/>
    </div>
    <?php
    if (in_array($doc["comprob_id"], array(
                    Comprob::RECIBO_BANCO,
                    Comprob::RECIBO_VENTAS,
            ))):
        ?>
        <div class="inline" id="mod-fecha-valor-div">
            <label for="mod-fecha-valor">Fecha valor:</label>
            <input id="mod-fecha-valor"  class="fecha"  name="cab[fecha_valor]" value="<?php echo date("d/m/Y", mystrtotime($doc["fecha_valor"])); ?>"/>
        </div>
    <?php endif; ?>
    <div class="row">
        <label for="mod-hora">Hora:</label>
        <input id="mod-hora"  name="cab[hora]" value="<?php echo date("h:m:s", mystrtotime($doc["fecha_creacion"])); ?>"/>
    </div>
    <div class="row">
        <label for="mod-detalle">Detalle:</label>
        <input id="mod-detalle"  name="cab[detalle]" value="<?php echo $doc["detalle"] ?>"/>
    </div>
    <?php
    if (in_array($doc["comprob_id"], array(
                    Comprob::RECIBO_BANCO,
                    Comprob::RECIBO_VENTAS,
                    Comprob::OP,
            ))):
        ?>
        <div class="inline" id="pago-a-cuenta-detalle">
            <label for="mod-fecha-valor">A cuenta:</label>
            <input id="mod-pago-a-cuenta-detalle"   placeholder="Detalle" name="cab[pago_a_cuenta_detalle]" value="<?php echo $doc["pago_a_cuenta_detalle"]; ?>"/>
        </div>
        <div class="inline" id="pago-a-cuenta-importe">
            <label for="-mod-pago-a-cuenta-importe">Importe:</label>
            <input id="mod-pago-a-cuenta-importe" class="mod-importe"  name="cab[pago_a_cuenta_importe]" value="<?php echo $doc["pago_a_cuenta_importe"]; ?>"/>
        </div>
    <?php endif; ?>
    <?php if ($doc["concepto_id"]): ?>

        <div class="row">
            <label for="concepto-ac" id="mod-numero-label">Concepto:</label>
            <?php Concepto::treeAndInput("cab[concepto_id]", null, $doc["concepto_id"], $doc["concepto_nombre"]); ?>
        </div>
    <?php endif; ?>
    <?php if ($doc["apl"] and count($doc["apl"] > 0)): ?>
        <div id="mod-apl" class="ui-widget ui-widget-content ui-corner-all" >
            <table id="mod-table">
                <tr>
                    <th>Fecha</th>
                    <th>Fecha valor</th>
                    <th>Comprobante</th>
                    <th>Total</th>
                    <th>Aplicado</th>
                </tr>
                <?php foreach ($doc["apl"] as $a): ?>
                    <tr>
                        <td><?php echo date("d/m/Y", strtotime($a["fecha_creacion"])); ?></td>
                        <td><?php echo date("d/m/Y", strtotime($a["fecha_valor"])); ?></td>
                        <td><?php echo $a["comprob"]; ?></td>
                        <td><?php echo $a["total"]; ?></td>
                        <td><?php echo $a["importe"]; ?></td>
                    </tr>
                <?php endforeach; ?>    

            </table>
        </div>
    <?php endif; ?>
    <div id="mod-det" class="ui-widget ui-widget-content ui-corner-all" >
        <table id="mod-table">
            <tr>
                <th>Tipo</th>
                <th>Descripción</th>
                <th>Número</th>
                <th>Banco</th>
                <th>Cta. Destino</th>
                <th></th>
                <th>Importe</th>
                <th>Acred.</th>
            </tr>
            <?php foreach ($doc["val"] as $val) : ?>
                <?php $tipo = $val["tipo"] + 0; ?>
                <tr doc_val_id="<?php echo $val["id"]; ?>">
                    <?php
                    foreach (array("chequera_id", "tarjeta_plan_id", "banco_id", "importe", "numero", "tipo", "id",
                    "chq_cuit_endosante", "chq_entregado_a", "chq_origen", "destino_id") as $v) {
                        $name = $v;
                        $value = $val[$v];
                        $doc_val_id = $val["id"];
                        echo "<input type='hidden' class=\"$name\" value=\"$value\" name=\"doc-det[$doc_val_id][$name]\"/>";
                    };
                    ?>
                    <td class="tipo"><?php echo $tipo; ?></td>
                    <?php if (in_array($tipo, array(2))) : ?>
                        <td class="mod-detalle-td"><?php echo $val["tarjeta"] . " " . $val["plan"] . " lote:" . $val["lote"] ?></td>
                    <?php elseif (in_array($tipo, array(0))) : ?>
                        <td class="mod-detalle-td">Efectivo</td>
                    <?php elseif (in_array($tipo, array(1))) : ?>
                        <td class="mod-detalle-td">Cheque</td>
                    <?php elseif (in_array($tipo, array(3))) : ?>
                        <td class="mod-detalle-td">Cheque Propio</td>
                    <?php else: ?>
                        <td class=""></td>
                    <?php endif; ?>
                    <td class="mod-numero-td"><?php echo $val["numero"]; ?></td>
                    <?php if (in_array($tipo, array(1, 2, 3))) : ?>
                        <td class="mod-banco-td"><span class="mod-banco"><?php echo $val["banco"] ?></span></td>
                    <?php else: ?>
                        <td class=""></td>
                    <?php endif; ?>
                    <?php if (in_array($tipo, array(3))) : ?>
                        <td class="mod-destino-td"><span class="mod-destino"><?php echo $val["destino"] ?></span></td>
                    <?php else: ?>
                        <td class="mod-relleno"></td>
                    <?php endif; ?>
                    <td class="mod-relleno"></td>
                    <td class="mod-importe-td"><?php echo number_format($val["importe"], 2); ?></td>
                    <td class="mod-importe-td"><?php echo date("d/m/Y", mystrtotime($val["fecha"])); ?></td>
                    <td class="modifica" onclick="return modificaValor($(this));"></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</form>
<div id="modifica-valor-div" style="display:none">
</div>

<div style="display:none">
    <td class="tipo"><?php echo $tipo; ?></td>
    <td class="mod-tipo-td"><?php echo $val["nombre"]; ?></td>
    <?php if ($tipo !== 0): ?>
        <td class="mod-numero-td"><input class="mod-numero" name="doc-det[<?php echo $val["id"]; ?>][numero]" value="<?php echo $val["numero"] ?>"/></td>
    <?php else: ?>
        <td class="mod-numero-td"></td>
    <?php endif; ?>
    <?php if (in_array($tipo, array(1, 3))) : ?>
        <td class="mod-banco-td"><span class="mod-banco"><?php echo $val["banco"] ?></span></td>
        <td class="mod-chq-origen-td">
            <input class="mod-chq-origen" name="doc-det[<?php echo $val["id"]; ?>; ?>][chq_origen]" value="<?php echo $val["chq_origen"]; ?>"/>
        </td>
    <?php else: ?>
        <td class="mod-chq-origen-td"></td>
        <td class="mod-banco-td"><span class="mod-banco"></span></td>
        <?php endif; ?>

</div>
<script type="text/javascript">
    //$("#concepto_ac").val();
    $(".chzn-select").chosen();
    $(".fecha").datepicker();
    $("#modifica-valor-div").dialog({
        autoOpen: false,
        title: "ModificaciÃ³n de valor",
        width: "500px",
        height: "auto",
        modal: true,
        position: {
            my: "center, center+200",
            at: "top"
        },
        buttons: {
            Graba: function() {
                $(this).dialog("close");
                grabaDocValor();
            },
            Cancela: function() {
                $(this).dialog("close");
            }
        }});

    function modificaValor($obj) {
        $tr = $obj.closest("tr");
        var doc_val_id = $tr.attr("doc_val_id");
        data = "";
        $tr.find("input").each(function() {
            data += "&" + $(this).attr("name") + "=" + $(this).val();
        });
        $.ajax({
            type: "POST",
            //dataType:"json",
            data: data,
            url: "<?php echo $this->createUrl("doc/modificaValor"); ?>",
            success: function(data) {
                $("#modifica-valor-div").html(data).dialog("open");
            },
            error: function(data, status) {
            }
        }
        );
    }


</script>


