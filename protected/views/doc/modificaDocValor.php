<style type="text/css">
    #modifica-valor-div label {
        float: left;
        line-height: 23px;
        margin-right: 4px;
        text-align: right;
        width: 89px;
    }
    #modifica-valor-div .row{display: block !important; margin-bottom: 3px}
    #valor-banco_id{width:200px}
    #valor-destino_id{width:200px}
    #valor-importe-input{width:75px}
    #valor-numero-input{width:75px}
    #valor-chq_entregado_a-input{width:250px}
</style>
<?php $tipo = $data["tipo"] + 0; ?>
<input type="hidden" id="valor-id-input" value="<?php echo $data["id"]; ?>"
       <div class="row">
    <label for="valor-importe-input">Importe:</label><input id="valor-importe-input" value="<?php echo $data["importe"]; ?>"/>
</div>
<?php if ($tipo !== 0): ?>
    <div class="row">
        <label for="valor-numero-input">Número:</label><input id="valor-numero-input" value="<?php echo $data["numero"]; ?>"/>
    </div>
    <?php if ($data["banco_id"]): ?>
        <div class="row">
            <label for="valor-banco_id">Banco</label>    
            <?php $listData = CHtml::listData(Banco::model()->findAll(), 'id', 'nombre'); ?>
            <?php echo CHtml::dropDownList("valor-banco_id", $data["banco_id"], $listData); ?>
        </div>
        <div class="row">
            <label for="valor-chq_cuit_endosante-input">Cuit Endosante:</label><input id="valor-chq_cuit_endosante-input" value="<?php echo $data["chq_cuit_endosante"]; ?>"/>
        </div>
        <div class="row">
            <label for="valor-chq_entregado_a-input">Entregado a:</label><input id="valor-chq_entregado_a-input" value="<?php echo $data["chq_entregado_a"]; ?>"/>
        </div>
        <div class="row">
            <label for="valor-chq_origen-input">Origen:</label><input id="valor-chq_origen-input" value="<?php echo $data["chq_origen"]; ?>"/>
        </div>
    <?php endif; ?>
    <?php if ($data["destino_id"]): ?>
        <div class="row">
            <label for="valor-destino_id">Cuenta Destino:</label>    
            <?php $listData = CHtml::listData(Destino::model()->findAll("tipo = 2"), 'id', 'nombre'); ?>
            <?php echo CHtml::dropDownList("valor-destino_id", $data["destino_id"], $listData); ?>
        </div>
    <?php endif; ?>
<?php endif; ?>


<script type="text/javascript">
    $("#valor-banco_id").chosen().change(function() {
    });
    $("#valor-destino_id").chosen().change(function() {
    });

    function grabaDocValor() {
        var doc_det_id = $("#valor-id-input").val();
        var importe = $("#valor-importe-input").val();
        var numero = $("#valor-numero-input").val();
        var banco_id = $("#valor-banco_id").val();
        var banco_nombre = $("#valor_banco_id_chzn a span").html();
        var destino_nombre = $("#valor_destino_id_chzn a span").html();
        var chq_cuit_endosante = $("#valor-chq_cuit_endosante-input").val();
        var chq_entregado_a = $("#valor-chq_entregado_a-input").val();
        var chq_origen = $("#valor-chq_origen-input").val();
        var destino_id = $("#valor-destino_id").val();
        $tr = $("#mod-table tr[doc_val_id='" + doc_det_id + "']");
        $tr.find(".mod-numero-td").html(numero);
        $tr.find(".numero").val(numero);
        $tr.find(".mod-importe-td").html(importe);
        $tr.find(".importe").val(importe);
        $tr.find(".banco_id").val(banco_id);
        $tr.find(".chq_cuit_endosante").val(chq_cuit_endosante);
        $tr.find(".chq_entregado_a").val(chq_entregado_a);
        $tr.find(".chq_origen").val(chq_origen);
        $tr.find(".destino_id").val(destino_id);
        $tr.find(".mod-banco-td span").html(banco_nombre);
        $tr.find(".mod-destino-td span").html(destino_nombre);
        //$tr.find(".mod-banco-td span").html(destino_nombre);
    }
</script>