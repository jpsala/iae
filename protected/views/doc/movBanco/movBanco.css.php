<style type="text/css">
  labelHighlight{color: #AAA; font-style: italic;}

  #content{padding:10px 5px 0px !important}

  #main-form {padding: 5px;margin: 5px 0px 5px; border: 1px #75C2F8 solid; border-radius: 8px ; position: relative;overflow: visible;min-height: 425px}

  #izq{border-radius: 5px; background-color: #f9fafc; border: #eff3fc solid 1px; padding: 5px;position: relative; margin: 0 0 0 0}

  #izq-content{min-height: 330px}
  #izq-content th, #izq-content td, #izq-content caption {padding: 4px 3px 4px 5px !important}
  .titulo{ background-color: #C3D9FF;    border-bottom: 1px solid #CCCCCC;    border-bottom: 1px solid #EFF3FC;    border-radius: 5px 5px 5px 5px;  margin-bottom: 5px;    padding: 3px;    text-align: center;color: #555555;  font-weight: bold;}

  #div-numero{float:right}
  #div-numero{/*position: absolute;    right: 9px; top: 9px*/}
  #div-numero input{ float: right; margin-right: 5px;   width: 98px; margin-top:3px; margin-bottom: 4px}

  #total-div span.total, #total-pagos-div span.total{    position: absolute;    right: 4px; font-weight: bold}

  #botones{ clear: both;margin:11px 0px 2px 11px; position: relative; text-align: right; top:4px;}
  .esNuevo{color:red}
  .right-align {  text-align: right }
  .ui-widget { font-size: 12px; }
  .bold{font-weight: bold}
  .total{background-image: url("images/pesos.gif");background-position: 0 50%;background-repeat: no-repeat;background-size: 10px auto;padding-left: 15px;}


  input::-webkit-input-placeholder, textarea::-webkit-input-placeholder {
    color: #636363;
  }
  input:-moz-placeholder, textarea:-moz-placeholder {
    color: #636363;
  }

  #contextMenu, #nota-de-credito{display:none}
  #nota-de-credito{overflow: visible}
  #nota-de-credito form label{width: 200px}
  #nota-de-credito form select,#nota-de-credito form input{width: 300px;display: block;}
  #concepto_chzn{display:block;margin: 10px 0;}

  /*.row label{margin: 6px 3px;margin-bottom: 7px;max-width: 500px}*/
  .row{margin: 6px 3px 6px 0;margin-bottom: 7px;max-width: 500px}
  label{margin-left: 5px; width: 80px!important}
  .inline-row{display: inline-block}
  #detalle{width:350px}
  #importe{width:75px;text-align: right;}

  #tree{
    padding-bottom: 10px;
    padding-left: 17px;
    padding-right: 0;
    padding-top: 10px;
  }
  #concepto-dlg{display:none}
  a.jqtree_common{cursor:pointer;text-decoration: none}
  span.jqtree_common{cursor:pointer;text-decoration: none}
  #ingreso-egreso-label{vertical-align: super;}
  #concepto-a{cursor:pointer; text-decoration: none; color: #0099FF;}
  #concepto-a:hover{-moz-text-decoration-line:underline}
  #movimiento-caja-div{padding: 10px}
  #concepto-button{margin-bottom: 3px; margin-left: 3px}
  #concepto-button.ui-button span.ui-button-text{padding: 0 7px 0 7px}
  label{float: left; width: 70px; line-height: 24px;}
  #conceptosTree{width:200px;;
                 float: left;
                 left: 37px;
                 position: absolute;
                 top: 20px;
                 z-index: 1000;}
  #concepto-button-div{position:relative;display:inline}
  #articulos-div{    padding-bottom: 4px;
                     padding-left: 2px;
                     padding-right: 2px;
                     padding-top: 2px;
                     width: 370px;
                     overflow: visible;
  }
  #jqgrid{width:100%}
  #destino_id{width:200px}
  #articulos-table{width:359px}
  .articulos-table-nombre{width:300px}
  .articulos-table-nombre select{width:240px}
  .articulos-table-importe{max-width:90px; text-align: right}
  .articulos-table-importe input{width:80px; text-align: right}
  .borra-articulo-td{cursor:pointer}
  #cuenta-banco-div{
    vertical-align: super;
    margin-left: 5px;
  }
  #concepto-div {
    margin-top: 11px;
  }
  #concepto_ac {
    width: 300px;
  }
  #agrega-articulo {
    float: right;
    margin:10px 0 0 0;
    padding-bottom: 5px;
    padding-left: 5px;
    padding-right: 5px;
    padding-top: 5px;
  }
  #fecha-label{width: 39px !important}
</style>