<link rel="stylesheet" type="text/css" href="js/jqgrid/css/ui.jqgrid.css" />
<script src="js/jqgrid/js/i18n/grid.locale-es.js"></script>
<script src="js/jqgrid/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript">
  var urlArticuloAC = "<?php echo $this->createUrl('articulo/articulosAc'); ?>";
  var doc = new Object(), ant = 0;
  var afectacion = "<?php echo $entrada->talonario->comprob->signo_caja; ?>";
//  var url = "<?php //echo $this->createUrl('doc/movCaja',
//          array("comprob_id"=>$comprob_id)); ?>";
  doc.pagoACuenta = 0, doc.totalPagos = 0, doc.total = 0;
  doc.saldo = 0, doc.disabled = false;
  
  doc.toggleEnabled = function(on) {
    doc.disabled = !on;
    if (on) {
      $(".td_edit_img *").removeAttr('disabled');
      $(".td_delete_img *").removeAttr('disabled');
      $("#pago").removeAttr('disabled');
      $("#izq *").removeAttr('disabled');
      $("#botones *").removeAttr('disabled');
      $("#pago-a-cuenta-div *").removeAttr('disabled');
    } else {
      $(".td_edit_img *").attr('disabled', "disabled");
      $(".td_delete_img *").attr('disabled', "disabled");
      $("#pago").attr('disabled', "disabled");
      $("#izq *").attr('disabled', "disabled");
      $("#botones *").attr('disabled', "disabled");
      $("#botones #CtaCte").removeAttr('disabled');
      $("#botones #cancel").removeAttr('disabled');
    }
    return false;
  }
  
  doc.muestraTotales = function() {
    doc.saldo = round(Number(doc.totalAPagar) - Number(doc.totalPagos));
    doc.total = round(Number(doc.totalPagos) + Number(doc.pagoACuenta));

    if (cajaCerrada) {
      doc.toggleEnabled(0);
    }

    $("form #total-pagos-div #total-a-pagar span.total").html(round(doc.totalAPagar));
    $("#total-pagos").html(round(doc.totalPagos));
    $("form #total-pagos-div #total-pagado span.total").html(round(doc.totalPagos));
    $("#total-pagos").html(doc.total);
    $("form #total-pagos-div #total-saldo span.total").html(round(doc.saldo));

    $("form #total-div span.total").html(round(doc.total));

    $("form #div-pago #pago").val(round(doc.total));

    //    $("#total-pagos-div div#total-a-pagar span.total").html(doc.total);

  };

  jQuery(document).ready(function() {
    onReady();
  });

  function onReady() {

    $("#agrega-articulo").button();

    $("#destino_id").chosen().change(function(a) {
      var banco = $(this).find("option:selected").attr("banco");
      $("#cuenta-banco-div span").html(banco);
    });

    $('#ingreso_egreso').attr({'disabled': 'disable'});

    $(":button").button();

    articuloChosen();
    $("#fecha").datepicker();

    $("#fecha").focus();

    $(".numeric").numeric();
    
    

    $("#numero").change(function() {
      validaNumeroComprob($(this));
    });

  }

  function importeAjusteChange(e) {
    e.val(round(Number(e.val())));
    var pago = e.val();
    doc.totalPagos = round(doc.totalPagos - (ant - pago), 2);
    ant = pago;
    doc.muestraTotales();
    valores.refresh();
  }

  function igualaAlto() {
    var i = $("#izq").height(), d = $("#der").height();
    if (d > i) {
      $("#izq").height(d - 11);
    } else if (i > d) {
      $("#der").height(i);
    }
  }

  function submitMov() {
    var data = $("#main-form").serialize();
    $(".articulo-tr").each(function() {
      var $articulo = $(this).find(".articulo");
      var $importe = $(this).find(".importe");
      if (!(($articulo.val().length === 0 || $importe.val().length === 0))) {
        data += "&articulos[" + $articulo.val() + "]=" + $importe.val();
      }
    });

    $.ajax({
      type: "POST",
      dataType: "json",
      data: data,
      url: "<?php echo $this->createUrl("doc/movBancoGraba"); ?>",
      success: function(data) {
        if (data.errores && muestraErroresDoc(data.errores)) {
          return false;
        }
        //window.location = url;
        window.location = window.location;
      },
      error: function(data, status) {
      }
    });
    return false;
  }

  function agregaArticulo() {
    var vacio = false;
    $(".articulo-tr").each(function() {
      if ($(this).find(".articulo").val().length === 0 || $(this).find(".importe").val().length === 0) {
        vacio = true;
      }
      ;
    });
    if (vacio) {
      return;
    }
    $.ajax({
      type: "GET",
      //dataType: "json",
      data: {},
      url: "<?php echo $this->createUrl("doc/movBancoGetTr"); ?>",
      success: function(data) {
        $("#articulos-table").append(data);
        articuloChosen();
        //$(".numeric").autoNumeric("init", {mDec: 2, aSep: ''});
        //articuloAC();
      },
      error: function(data, status) {
      }
    });

  }

  function articuloChosen() {
    $(".articulo").chosen().change(function() {
      setTimeout(function($this) {
        $this.closest("tr").find(".importe").focus();
      }, 100, $(this));
    });
  }

  function articuloAC() {
    $('.articulo')
            .autocomplete(
            {
              autoSelect: true,
              autoFocus: true,
              minLength: 1,
              source: urlArticuloAC,
              select: function(event, ui) {
              }
            }
    );
  }

  function borraArticulo($this) {
    $this.closest("tr").remove();
  }
</script>
