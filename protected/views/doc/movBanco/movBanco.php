<?php $baseUrl = Yii::app()->request->baseUrl; ?>
<?php include("movBanco.js.php"); ?>
<?php include("movBanco.css.php"); ?>

<?php //$doc_id = 84963;     ?>
<?php $doc_id = null; ?>
<form id="main-form" name="main" method="POST" action="#">
    <?php /* @var $entrada Doc */; ?>
    <input type="hidden" name="doc[comprob_id]" value="<?php echo $entrada->talonario->comprob_id; ?>"/>
    <div id="izq">
        <div class="titulo">
            <span>Movimientos Bancarios</span>
        </div>
        <div id="izq-content">
            <div class="row">
                <label id="fecha-label" for="fecha">Fecha:</label>
                <input name="doc[fecha]" id="fecha" value="<?php echo date('d/m/Y', time()); ?>" placeholder="Importe" class="pago numeric"/>
             </div>
            <span class="row">
                <!--<label for="detalle">Detalle:</label>-->
                <input name="doc[detalle]" id="detalle" value="" placeholder="Detalle"/>
            </span>
            <div id="div-numero">
                <input tabindex="-1" name ="doc[numero]" id="numero" style="display:none" value="<?php echo $entrada->numeroCompleto; ?>" ddisabled="DISABLED" placeholder="Número"/>
            </div>
            <div class="row">
                <!--<label for="importe">Importe:</label>-->
                <input name="doc[total]" id="importe" value="" placeholder="Importe" class="pago numeric"/>
               
            </div>

            <div class="inline-row">
                <!--<label for="destino_id">Cuenta:</label>-->
                <select name="doc[destino_id]"id="destino_id" data-placeholder="Seleccione una cuenta">
                    <option></option>
                    <?php echo $destinoListData; ?>
                </select>
            </div>
            <div class="row inline-row" id="cuenta-banco-div">
                <!--<label for="">Banco:</label>-->
                <span></span>
            </div>
            <?php Concepto::treeAndInput(); ?>
            <div class="ui-widget-content ui-corner-all" id="articulos-div">
                <div class="ui-widget-content ui-corner-all">
                    <div class="titulo">Artículos</div>
                    <div id="jqgrid">
                        <table id="articulos-table" class="tabla">
                            <tr>
                                <th class="articulos-table-nombre">Nombre</th>
                                <th class="articulos-table-importe">Importe</th>
                                <td class="borra-articulo"></td>
                            </tr>
                            <?php include("movBanco_articulo_tr.php"); ?>
                        </table>
                    </div>
                    <input id="agrega-articulo" type="button" value="Agregar Artículo" onclick="agregaArticulo();
                  return false;"/>
                </div>
            </div>
        </div>
    </div>

    <div id="botones">
        <div id="botones">
            <input type="button" id="cancel" value="Cancela" onclick="return limpiaTodo();"/>
            <input type="button" id="ok" value="Graba" onclick="return submitMov();"/>
        </div>
    </div>
</form>
</div>