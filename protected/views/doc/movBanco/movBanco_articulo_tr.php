
<tr class="articulo-tr">
  <td class="articulos-table-nombre">
    <!--<input class="articulo" type="text" name="articulo-nombre[-1]"/>-->
    <select class="articulo" data-placeholder="Seleccione un articulo">
      <option value=""></option>
      <?php
      $listData =
              CHtml::listData(Articulo::model()->with("articuloTipo")->findAll("articuloTipo.novedad_tipo = 3"), "id", "nombre");
      ?>
      <?php $op = array(); ?>
      <?php echo CHtml::listOptions("", $listData, $op); ?>
    </select>
  </td>
  <td class="articulos-table-importe">
    <input class="importe numeric"  type="text"/>
  </td>
  <td class="borra-articulo-td"><a onclick="borraArticulo($(this));return false;"><img src="images/delete.jpg"/></a></td>
</tr>

