<script type="text/javascript">
  var doc = new Object(), ant = 0;
  var doc_id = "<?php echo $model->id; ?>";
  doc.pagoACuenta = 0, doc.totalPagos = 0, doc.total = 0;
  doc.saldo = 0, doc.disabled = false;
  doc.toggleEnabled = function (on) {
    doc.disabled = !on;
    if (on) {
      $(".td_edit_img *").removeAttr('disabled');
      $(".td_delete_img *").removeAttr('disabled');
      $("#pago").removeAttr('disabled');
      $("#izq *").removeAttr('disabled');
      $("#botones *").removeAttr('disabled');
      $("#pago-a-cuenta-div *").removeAttr('disabled');
    } else {
      $(".td_edit_img *").attr('disabled', "disabled");
      $(".td_delete_img *").attr('disabled', "disabled");
      $("#pago").attr('disabled', "disabled");
      $("#izq *").attr('disabled', "disabled");
      $("#botones *").attr('disabled', "disabled");
      $("#botones #CtaCte").removeAttr('disabled');
      $("#botones #cancel").removeAttr('disabled');
    }
    return false;
  };
  doc.muestraTotales = function () {
    doc.saldo = round(Number(doc.totalAPagar) - Number(doc.totalPagos));
    doc.total = round(Number(doc.totalPagos) + Number(doc.pagoACuenta));

    if (cajaCerrada) {
      doc.toggleEnabled(0);
      valores.toggleEnabled(0);
    }

    $("form #total-pagos-div #total-a-pagar span.total").html(round(doc.totalAPagar));
    $("#total-pagos").html(round(doc.totalPagos));
    $("form #total-pagos-div #total-pagado span.total").html(round(doc.totalPagos));
    $("#total-pagos").html(doc.total);
    $("form #total-pagos-div #total-saldo span.total").html(round(doc.saldo));

    $("form #total-div span.total").html(round(doc.total));

    $("form #div-pago #pago").val(round(doc.total));

    //    $("#total-pagos-div div#total-a-pagar span.total").html(doc.total);

  }
  doc.esSalida = <?php echo $model->talonario->comprob_id == Comprob::AJUSTE_CAJA_ENTRADA ? "false" : "true"; ?>;
  doc.comprob = <?php echo json_encode($model->talonario->comprob->attributes); ?>;
  var urlImpresion = "<?php echo $this->createUrl("doc/docImpresion"); ?>";
  var afectacion = "<?php echo $model->talonario->comprob->signo_caja; ?>";
  var url = "<?php echo $this->createUrl('doc/movCaja', array("comprob_id" => $comprob_id)); ?>";

  jQuery(document).ready(function () {
    onReady();
  });

  function onReady() {

    igualaPos();

    valores.init();

    if (doc_id != "") {
      traeValores(doc_id);
    }

    $("#user_id_empleado").attr("name", "doc[user_id_empleado]").chosen();

    $('#ingreso_egreso').attr({'disabled': 'disable'});

    $(":button").button();

    $("#izq-content").on("focus", ".pago", function () {
      ant = $(this).val()
    })

    $("#izq-content").on("change", ".pago", function () {
      importeAjusteChange($(this));
    })

    $(".numeric").autoNumeric("init", {mDec: 2, aSep: ''});

    $("#numero").change(function () {
      validaNumeroComprob($(this));
    });

  }

  function importeAjusteChange(e) {
    e.val(round(Number(e.val())));
    var pago = e.val();
    //doc.totalPagos = round(doc.totalPagos - (ant - pago), 2);
    doc.totalPagos = round(pago, 2);
    doc.total = round(pago, 2);
    ant = pago;
    doc.muestraTotales();
    console.log(111);
    $(".valor-id").first().focus();
    valores && valores.refresh();

  }

  function submitMov(imprime) {
    $("#ingreso_egreso").removeAttr("disabled");
    $.ajax({
      type: "POST",
      dataType: "json",
      data: $("#main-form").serialize() + valores.getData(),
      url: "<?php echo $this->createUrl("doc/movCajaGraba"); ?>",
      success: function (data) {
        if (data.errores && muestraErroresDoc(data.errores)) {
          return false;
        }
        if (imprime == '1') {
          window.open(urlImpresion + "&doc_id=" + data.doc_id, 'newwin', 'menubar=no,width=750,height=550,toolbar=no,scrollbars=yes,screenX=300');
        }
        ;
        alert('Movimiento Registrado');
        window.location = window.location;
      },
      error: function (data, status) {
      }
    });
    return false;
  }

  function limpiaTodo() {
    window.location = window.location;
  }

  function traeValores(doc_id) {
    $.ajax({
      type: "GET",
      data: {doc_id: doc_id},
      dataType: "json",
      url: "<?php echo $this->createUrl("doc/traeValores"); ?>",
      success: function (data) {
        $("#valores-table").html(data.cab);
        $("#valores-detalle-items").html(data.det);

        //$("#der").html(data);
      },
      error: function (data, status) {
      }
    });
  }
</script>
