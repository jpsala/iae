<?php $this->renderPartial("/doc/common.css"); ?>
<?php $this->renderPartial("/doc/common.js"); ?>
<?php include("movCaja.css.php"); ?>
<?php include("movCaja.js.php"); ?>
<?php $this->renderPartial("/doc/valores/valores.css"); ?>
<?php $this->renderPartial("/doc/valores/valores.js"); ?>
<?php
$baseUrl = Yii::app()->request->baseUrl;
$cs = Yii::app()->clientScript;
$cs->registerScriptFile($baseUrl . "/js/app/viewmodels/recibo_op.js", CClientScript::POS_BEGIN);
?>
<form id="main-form" name="main" method="POST" action="#">
  <?php /* @var $model Doc */; ?>
  <input type="hidden" name="doc[id]" value="<?php echo $model->id; ?>"/>
  <input type="hidden" name="doc[comprob_id]" value="<?php echo $model->talonario->comprob_id; ?>"/>
  <div id="div-numero">
    <input name ="doc[numero]" id="numero" value="<?php echo $model->numeroCompleto; ?>" ddisabled="DISABLED" placeholder="Número"/>
  </div>

  <div id="izq">
    <div id="mov-caja">
      <div class="titulo">
        <span>Movimientos de caja - <?php echo $model->talonario->comprob_id == Comprob::AJUSTE_CAJA_ENTRADA ? "Entrada" : "Salida"; ?></span>
      </div>
      <div id="izq-content">
        <?php Concepto::treeAndInput("doc[concepto_id]", null, $model->concepto_id, $model->concepto ? $model->concepto->nombre : ""); ?>
        <div class="row">
          <input name="doc[detalle]" id="detalle" value="<?php echo $model->detalle; ?>" placeholder="Detalle"/>
        </div>
        <?php if ($model->talonario->comprob_id == Comprob::AJUSTE_CAJA_SALIDA): ?>
          <div class="row">
            <?php $usuarios = User::model()->findAll(array("order" => "nombre")); ?>
            <?php
            echo CHtml::dropDownList("user_id_empleado", $model->user_id_empleado, CHtml::listData($usuarios, "id", "nombre"), array("prompt" => "", "data-placeholder" => "Seleccione un usuario", "name" => "doc[user_id_empleado]"));
            ?>
          <!--<input name="doc[empleado]" id="empleado" value="" placeholder="Empleado" class="pago numeric"/>-->
          </div>
        <?php endif; ?>


        <div class="row">
          <input name="doc[total]" id="importe" value="<?php echo $model["total"]; ?>" placeholder="Importe" class="pago numeric"/>
        </div>
        <?php if ($model->talonario->comprob_id == Comprob::AJUSTE_CAJA_SALIDA): ?>
          <div class="row">
            <!--<label for="detalle">Detalle:</label>-->
            <input name="doc[cuit]" id="cuit" data-bind="value: cuit" placeholder="CUIT"/>
            <!--ko if: obteniendoRetencion() -->
            <span>Obteniendo retención...</span>
            <!-- /ko -->
            <input type="text" data-bind="textInput: retencion" placeholder="% Retención">
            <!--ko if: !obteniendoRetencion() && retencion() -->
            <span>Retención: </span>
            <span data-bind="text:'%('+retencion()+')' "></span>
            <span data-bind="text:totalRetencion"></span>$
            <!-- /ko -->
          </div>
            <!--ko if: cuit -->
          <div class="row">
            <!--<label for="detalle">Detalle:</label>-->
            <input name="doc[socio_nombre]" id="socio_nombre" data-bind="value: socio_nombre" placeholder="Nombre"/>
          </div>
            <!-- /ko -->
        <?php endif; ?>
      </div>
    </div>

  </div>


  <div id="der">
    <?php $this->renderPartial("/doc/valores/_valores"); ?>
  </div>

  <div id="botones">
    <input type="button" id="cancel" value="Cancela" onclick="return limpiaTodo();"/>
    <input type="button" id="ok" value="Graba e Imprime" onclick="return submitMov('1');"/>
    <input type="button" id="ok1" value="Graba" onclick="return submitMov('0');"/>
  </div>
</form>
<script type="text/javascript">
  igualaPos();
  ko.applyBindings(vm);
  vm.urlRetencion = "<?php echo $this->createUrl("socio/getRetencion"); ?>"
  vm.tipo(13)
  console.log(vm);
</script>
