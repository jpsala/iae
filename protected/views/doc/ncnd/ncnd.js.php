<?php
$baseUrl = Yii::app()->request->baseUrl;
$cs      = Yii::app()->clientScript;
$cs->registerScriptFile($baseUrl . "/js/jquery.contextmenu.r2.packed.js", CClientScript::POS_END);
$cs->registerScriptFile($baseUrl . "/js/common.js", CClientScript::POS_BEGIN);
?>
<script type="text/javascript">
  var tipoSocio = "<?php echo $tipoSocio; ?>";
  var comprob_id = "<?php echo $comprob_id; ?>";
  var urlAC = "<?php echo $this->createUrl('socio/sociosAc'); ?>&socioTipo=<?php echo $tipoSocio; ?>";
  var doc = new Object(), ant = 0;
  var socio_id = "<?php echo $socio ? $socio->id : ''; ?>";
  var esNotaDeDebito = <?php echo ($comprob_id == Comprob::ND_VENTAS or $comprob_id == Comprob::ND_COMPRAS) ? "true" : "false" ?>;
  doc.pagoACuenta = 0, doc.totalPagos = 0, doc.total = 0
  doc.saldo = 0, doc.disabled = false;
  doc.toggleEnabled = function (on) {
    doc.disabled = !on;
    if (on) {
      $(".td_edit_img *").removeAttr('disabled');
      $(".td_delete_img *").removeAttr('disabled');
      $("#pago").removeAttr('disabled');
      $("#izq *").removeAttr('disabled');
      $("#botones *").removeAttr('disabled');
      $("#pago-a-cuenta-div *").removeAttr('disabled');
    } else {
      $(".td_edit_img *").attr('disabled', "disabled");
      $(".td_delete_img *").attr('disabled', "disabled");
      $("#pago").attr('disabled', "disabled");
      $("#izq *").attr('disabled', "disabled");
      $("#botones *").attr('disabled', "disabled");
      $("#botones #CtaCte").removeAttr('disabled');
      $("#botones #cancel").removeAttr('disabled');
    }
    return false;
  };
  doc.muestraTotales = function () {
    doc.saldo = round(Number(doc.totalAPagar) - Number(doc.totalPagos));
    doc.total = round(Number(doc.totalPagos) + Number(doc.pagoACuenta));

    if (cajaCerrada) {
      doc.toggleEnabled(0);
      valores.toggleEnabled(0);
    }

    $("form #total-pagos-div #total-a-pagar span.total").html(round(doc.totalAPagar));
    $("#total-pagos").html(round(doc.totalPagos));
    $("form #total-pagos-div #total-pagado span.total").html(round(doc.totalPagos));
    $("#total-pagos").html(doc.total);
    $("form #total-pagos-div #total-saldo span.total").html(round(doc.saldo));

    $("form #total-div span.total").html(round(doc.total));

    $("form #div-pago #pago").val(round(doc.total));

    //    $("#total-pagos-div div#total-a-pagar span.total").html(doc.total);

  }
  doc.esSalida = <?php echo $model->talonario->comprob_id == Comprob::AJUSTE_CAJA_ENTRADA ? "false" : "true"; ?>;

  jQuery(document).ready(function () {
    onReady();
  });

  function onReady() {

    igualaPos();

    valores.init();


    $("#fecha_valor").datepicker();

    $(":button").button();

    socioAutoComplete();

    if (socio_id != "") {
      console.log(socio_id);
      $("#socio").val("<?php echo $socio ? $socio->nombre : ''; ?>");
//            $("#div-head div#div-pago input#pago").focus();
//            $("#socio").attr('disabled', "disabled");
      traeCta(socio_id);
    }

    $("body").on("change", "input", function () {
      $("#socio").attr('disabled', "disabled");
    });

    $("form #div-pago").on("change", "#pago", function () {
      pagoTotalChange($(this));
    });

    $("#doc-pendientes").on("focus", ".pago input", function () {
      ant = $(this).val();
    });

    $("#doc-pendientes").on("change", ".pago input", function () {
      pagoPendienteChange($(this));
    });

    $(".docs").on("click", ".detalle-td input", function () {
      muestraDocDetalle($(this));
    });

    $("#numero").change(function () {
      validaNumeroComprob($(this));
    });

    $("#doc-dets-div").dialog({
      autoOpen: false,
      width: "auto",
      height: "auto",
      title: "Detalle",
      buttons: {
        ok: function () {
          $(this).dialog("close");
        }
      }
    });

    $("#obs-cobranza-dialog").dialog({
      autoOpen: false,
      width: 400,
      height: 200,
      title: "Observaciones de cobranza",
      position: [
        "top", 50
      ],
      buttons: {
        ok: function () {
          $(this).dialog("close");
        }
      }
    });

    $(".numeric").autoNumeric("init", {mDec: 2, aSep: ''});

    //setFocus($("#socio"),100);
    //$("#socio").focus();
  }

  function socioAutoComplete() {
    $('#socio').focus().autocomplete({
      autoSelect: true, autoFocus: true,
      minLength: 2, source: urlAC,
      select: function (event, ui) {
        socio_id = ui.item.id;
        chkSaldo(ui.item.id);
      }
    });
  }

  function pagoTotalChange(e) {
    $("#concepto_ac").focus().select();
    e.val(round(Number(e.val())));
    var pago = e.val();
    console.log(pago);
    //doc.totalPagos = round(doc.totalPagos - (ant - pago), 2);
    doc.totalPagos = round(pago, 2);
    doc.total = round(pago, 2);
    ant = pago;
    doc.muestraTotales();
    valores.refresh();
    $(".valor-id").first().focus();
  }

  function chkSaldo(socio_id) {
    $.ajax({
      type: "GET",
      data: {socio_id: socio_id},
      url: "<?php echo $this->createUrl("socioChkSaldo"); ?>",
      success: function (data) {
        if (data & Number(data) > 0) {
          window.location = "<?php echo $this->createUrl("doc/aplicacion&socio_id="); ?>" + socio_id + "&comprob_id=" + comprob_id;
        } else {
          traeCta(socio_id);
        }
      },
      error: function (data, status) {
      }
    });
  }

  function traeCta(socio_id, pago) {
    if (esNotaDeDebito) {
      return;
    }
    $.ajax({
      type: "GET",
      data: {"socio_id": socio_id, "pagoTotal": pago},
      url: "<?php echo $this->createUrl("doc/docsAAplicar"); ?>",
      success: function (data) {
        $("#doc-pendientes .docs").html(data);
        $(".pagodoc").val("0.00");
        doc.totalPagos = 0;
        doc.muestraTotales();
        igualaAlto();
        contextMenu();
        $("#pago").focus().select();
        var obsCobranza = $("#obs-cobranza-input").val();
        if (obsCobranza.length > 1) {
          $("#obs-cobranza-dialog").find("div").html(obsCobranza);
          $("#obs-cobranza-dialog").dialog("open");
        }
      },
      error: function (data, status) {
      }
    });
  }

  function pagoPendienteChange(e) {
    e.val(round(Number(e.val())));
    var pago = Number(e.val());
    var saldo = Number(e.attr("saldo"));
    if (Math.abs(pago) > Math.abs(saldo)) {
      e.val(ant);
      alert("Error: el pago es mayor al saldo");
      return false;
    }

    doc.totalPagos = round(doc.totalPagos - (ant - pago), 2);
    ant = pago;

  }

  function chkRecibo() {
    //    var data, index = 0, valor_id, input_nombre, valor_nombre;
    //    data = $("#main-form").find("input").serialize();
    //    data += "&doc[total]="+doc.total;
    //    $("#valores-table").find("tr").each(function(i,e){
    //      valor_nombre = $(e).find(".valor-nombre").html();
    //      data += "&valores["+index+"]["+valor_nombre+"][importe]=";
    //      data += $(e).find(".valor-importe").val();
    //      valor_id=$(e).find(".valor-id").attr("id");
    //      $("#form_"+valor_id).find("input").each(function(i,e){
    //        input_nombre = $(e).attr("name"));
    //        if(input_nombre){
    //          data += "&valores["+index+"]["+valor_nombre+"]["+input_nombre+"]=";
    //          data += $(e).val();
    //        }
    //      })
    //      index++;
    //    })
    //
    //
    //    //console.log(data["recibo numero"]);
    //
    //    return true;
  }

  function limpiaTodo() {
//    valores.toggleEnabled(1);
    doc.toggleEnabled(1);
    $("#socio").removeAttr('disabled');
    $("#doc-pendientes div.docs table tbody").remove();
    $("#izq .total").html("0");
    $("#pago").val("");
    $("#socio").val("").focus();
    $("#pago-a-cuenta-div input").val("");
    $("#div-head").show();
    doc.totalPagos = 0;
    doc.totalAPagar = 0;
    doc.disabled = false;
//    valores.limpia();
  }

  function reporteCtaCte() {
    var urlImpresionCtaCte = "<?php echo $this->createUrl("/doc/reciboCtaCte"); ?>";
    $.ajax({
      type: "GET",
      data: {socio_id: socio_id},
      url: urlImpresionCtaCte,
      success: function (data) {
        $("#rep-ctacte").html(data);
        $("#rep-ctacte").dialog({
          visible: true,
          title: "Reporte de Cta.Cte.",
          width: "auto",
          position: ["auto", 10]
        });
      },
      error: function (data, status) {
        alert("Error/es:\n" + data)
      }
    });
  }

  function reporteCtaCteViejo() {
    var urlImpresionCtaCte = "<?php echo $this->createUrl("/doc/reciboCtaCteViejo"); ?>";
    $.ajax({
      type: "GET",
      data: {socio_id: socio_id},
      url: urlImpresionCtaCte,
      success: function (data) {
        $("#rep-ctacte").html(data);
        $("#rep-ctacte").animate({scrollTop: 1000000}, 0);
        //$("table").stickyTableHeaders();
        $("#rep-ctacte").dialog({
          visible: true,
          title: "Reporte de Cta.Cte. del sistema viejo",
          width: "auto",
          position: ["auto", 0],
          open: function () {
            $("#rep-ctacte").animate({scrollTop: 1000000}, 0);
          },
          buttons: {
            ok: function () {
              $(this).dialog("close");
            }
          }
        });
      },
      error: function (data, status) {
        alert("Error/es:\n" + data)
      }
    });
  }

  function submit() {
    var data = $("#main-form").find("input").serialize();
    if (doc.totalPagos > Number($("#pago").val())) {
      alert("El total de imputaciones es mayor que el total del comprobante");
      return;
    }
    data += "&doc[Socio_id]=" + socio_id;
    if (valores.totalValores > 0) {
      data += valores.getData();
    }
    $.ajax({
      type: "POST",
      data: data,
      dataType: "json",
      url: "<?php echo $this->createUrl("doc/ncndGraba"); ?>",
      success: function (data) {
        if (data.errores && muestraErroresDoc(data.errores)) {
          return false;
        }
        $("#div-head *").removeAttr('disabled');
        window.location = window.location;
//        if (tipoSocio === "cliente") {
//          window.location = comprob_id == <?php echo Comprob::NC_VENTAS; ?> ? "<?php echo $this->createUrl('doc/ncVentas'); ?>" : "<?php echo $this->createUrl('doc/ndVentas'); ?>";
//        } else {
//          window.location = comprob_id == <?php echo Comprob::NC_COMPRAS; ?> ? "<?php echo $this->createUrl('doc/ncCompras'); ?>" : "<?php echo $this->createUrl('doc/ndCompras'); ?>";
//        }
        ;
      },
      error: function (data, status) {
      }
    });
    return false;
  }

  function igualaAlto() {
    var i = $("#izq").height(), d = $("#der").height();
    if (d !== i) {
      $("#der").height(i - 12);
    }
  }

  function contextMenu() {
    var id;
    $('.docs tr').contextMenu('contextMenu', {
      bindings: {
        'open': function (t) {
          id = $(t).attr("id");
          $("#nota-de-credito").dialog({
            width: "auto",
            buttons: {
              ok: function () {
                $dlg = $(this);
                $.ajax({
                  type: "POST",
                  dataType: "json",
                  data: $("#nota-de-credito form").serialize() + "&socio_id=" + socio_id + "&doc_id=" + id,
                  url: "<?php echo $this->createUrl("doc/reciboGrabaNotaDeCredito"); ?>",
                  success: function (data) {
                    if (data.error) {
                    } else {
                      $(t).find(".saldo").html(data.saldo);
                      $dlg.find("input").val("");
                      alert("La Nota de crédito fue grabada")
                    }
                  },
                  error: function (data, status) {
                  }
                });
                $(this).dialog("close");
              },
              cancelar: function () {
                $(this).dialog("close");
              }
            }
          });
          return true;
        }
      }
    });
  }

  function muestraDocDetalle($this) {
    $.ajax({
      type: "GET",
//      dataType:"json",
      data: {doc_id: $this.closest("tr").attr("id")},
      url: "<?php echo $this->createUrl("doc/getDetalle"); ?>",
      success: function (data) {
        $("#doc-dets-div").html(data);
        $("#doc-dets-div").dialog("open");
      },
      error: function (data, status) {
      }
    });
  }

</script>

