<?php $this->renderPartial("/doc/common.css"); ?>
<?php $this->renderPartial("/doc/common.js"); ?>
<?php include("ncnd.css.php"); ?>
<?php include("ncnd.js.php"); ?>
<?php $this->renderPartial("/doc/valores/valores.css"); ?>
<?php $this->renderPartial("/doc/valores/valores.js"); ?>
<?php /* @var $model Doc */; ?>
<form id="main-form" name="main" method="POST" action="#">

    <input name="doc[comprob_id]" type="hidden" value="<?php echo $comprob_id; ?>"/>
    <div id="div-numero">
        <input name ="doc[numero]" id="numero"
               value="<?php echo $model->numeroCompleto; ?>"
               placeholder="Número"/>
    </div>
    <div id="izq">
      <div class="datos">
        <div class="titulo">
          <span><?php echo $this->pageTitle; ?></span>
        </div>
        <div id="div-head" style="<?php echo $model->isNewRecord ? '' : 'display:none'; ?>">
          <div id="div-socio" class="row">
            <!--            <label for="socio"></label>-->
            <input name="doc[socio]" id="socio" value="" placeholder="<?php
            echo $tipoSocio == "cliente" ? "Alumno" : "Proveedor";
            ?>"/>
          </div>
          <div id="fecha-valor-div" class="row">
            <!--<span id="fecha-valor-input">Fecha valor</span>-->
            <input tabindex="-1" name="doc[fecha_valor]" id="fecha_valor" placeholder="" value="<?php echo date("d/m/Y", time()); ?>"/>
          </div>
          <div id="div-pago" class="row">
            <!--<label for="pago">Importe:</label>-->
            <input id="pago" class="numeric"  name="doc[total]" placeholder="Importe"/>
          </div>
          <?php Concepto::treeAndInput(); ?>
          <div id="ddiv-obs" class="row">
            <!--<label for="obs">Detalle:</label>-->
            <input id="detalle" name="doc[detalle]" placeholder="Detalle"/>
          </div>
        </div>
      </div>
        <div id="doc-pendientes"  style="display:<?php echo ($comprob_id == Comprob::ND_VENTAS or $comprob_id == Comprob::ND_ELECTRONICO or$comprob_id == Comprob::ND_COMPRAS) ? "none" : "" ?>">
            <div class="titulo">
                <span>Documentos a pagar/cancelar</span>
            </div>
            <div class="docs">
                <?php $this->renderPartial("/doc/_docsAAplicar", array("ctas" => array())); ?>
            </div>
        </div>
        <div id="total-pagos-div" style="display: none">
            <div id="total-a-pagar">
                <span class="label">Total</span>
                <span class="total">0</span>
            </div>
            <div id="total-pagado">
                <span class="label">Pago</span>
                <span class="total">0</span>
            </div>
            <div id="total-saldo">
                <span class="label">Saldo</span>
                <span class="total">0</span>
            </div>
        </div>
        <div id="total-div" style="display: none">
            <span class="label">Total del documento</span>
            <span class="total">0</span>
        </div>
      </div>


    <div id="der">
      <?php $this->renderPartial("/doc/valores/_valores"); ?>
    </div>
</form>

<div id="botones">
    <input type="button" id="CtaCte" value="CtaCte" onclick="return reporteCtaCte()"/>
    <input type="button" id="CtaCteViejo" value="CtaCte(sist.viejo)" onclick="return reporteCtaCteViejo()"/>
    <input type="button" id="cancel" value="Cancela" onclick="return limpiaTodo();"/>
    <input type="button" id="ok" value="Graba" onclick="return submit();"/>
</div>

<div id="contextMenu">

    <ul>

        <li id="open">Nota de crédito</li>

    </ul>

</div>

<div id="rep-ctacte">
</div>
<div id="doc-dets-div"></div>

<div id="nota-de-credito">
    <form method="POST" action="#">
        <input class="numeric" type="text" name="importe" placeholder="Importe"/>
        <select id="concepto" data-placeholder="Concepto" name="concepto_id" class="chzn-select">
            <?php
            $x = array('prompt' => '');
            echo CHtml::listOptions($model->id, CHtml::listData(Concepto::model()->findAll(), 'id', 'nombre'), $x)
            ?>
        </select>
        <input type="text" name="detalle" placeholder="Detalle"/>
    </form>
</div>

<div id="obs-cobranza-dialog">
    <div class="ui-widget ui-widget-content ui-corner-all"></div>
</div>