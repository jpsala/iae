<?php
//  include("vm.js.php");
$esElectronica = isset($_GET['esElectronica']);
$baseUrl = Yii::app()->request->baseUrl;
$cs = Yii::app()->clientScript;
$cs->registerScriptFile($baseUrl . "/js/jquery.contextmenu.r2.packed.js", CClientScript::POS_END);
$cs->registerScriptFile($baseUrl . "/js/common.js", CClientScript::POS_BEGIN);
if ($comprob_id == Comprob::OP) {
  $cs->registerScriptFile($baseUrl . "/js/app/viewmodels/recibo_op.js", CClientScript::POS_BEGIN);
}
?>
<script type="text/javascript">
  <?php $socioTipo = ($comprob_id == Comprob::OP ? "proveedor" : "alumno"); ?>
  var imprimeAuto = "<?php echo isset($imprimeAuto) ? $imprimeAuto : "N"; ?>";
  var comprob_id = "<?php echo $comprob_id; ?>";
  var urlAC = "<?php echo $this->createUrl('socio/sociosAc'); ?>&comprob_id=<?php echo $comprob_id; ?>&socioTipo=<?php echo $socioTipo; ?>";
  var cajaCerrada = "<?php echo $cajaCerrada; ?>";
  var socioTipo = "<?php echo $socioTipo; ?>";
  var socio_id = "<?php echo $socio ? $socio->id : ''; ?>";
  var doc = new Object();
  var urlImpresion = "<?php echo $this->createUrl("doc/docImpresion"); ?>";
  var urlDoc = "<?php echo $this->createUrl("/doc") . "/" . $action; ?>";
  var doc_id = "<?php echo $doc->id; ?>";
  var submitting = false;
  doc.pagoACuenta = 0, doc.totalPagos = 0, doc.total = 0;
  doc.saldo = 0, doc.disabled = false;
  doc.tipo = <?php echo $comprob_id; ?>;doc.toggleEnabled = function (on) {
  doc.disabled = !on;
  if (on) {
    $(".td_edit_img *").removeAttr('disabled');
    $(".td_delete_img *").removeAttr('disabled');
    $("#pago").removeAttr('disabled');
    $("#izq *").removeAttr('disabled');
    $("#botones *").removeAttr('disabled');
    $("#pago-a-cuenta-div *").removeAttr('disabled');
    $("#datos").hide();
  } else {
    $(".td_edit_img *").attr('disabled', "disabled");
    $(".td_delete_img *").attr('disabled', "disabled");
    $("#pago").attr('disabled', "disabled");
    $("#izq *").attr('disabled', "disabled");
    $("#botones *").attr('disabled', "disabled");
    $("#botones #CtaCte").removeAttr('disabled');
    $("#datos").show();
    $("#botones #cancel").removeAttr('disabled');
  }
  return false;
}
doc.muestraTotales = function () {
  doc.saldo = round(Number(doc.totalAPagar) - Number(doc.totalPagos));
  doc.total = round(Number(doc.totalPagos) + Number(doc.pagoACuenta));

  if (cajaCerrada) {
    doc.toggleEnabled(0);
    valores.toggleEnabled(0);
    alert('No es posible modificar el recibo ya que la caja en el que se realizó está cerrada');
  }

  $("form #total-pagos-div #total-a-pagar span.total").html(round(doc.totalAPagar));
  $("#total-pagos").html(round(doc.totalPagos));
  $("form #total-pagos-div #total-pagado span.total").html(round(doc.totalPagos));
  $("#total-pagos").html(doc.total);
  $("form #total-pagos-div #total-saldo span.total").html(round(doc.saldo));

  $("form #total-div span.total").html(round(doc.total));

  $("form #div-pago #pago").val(round(doc.total));

  //    $("#total-pagos-div div#total-a-pagar span.total").html(doc.total);

}
doc.esSalida = <?php echo $comprob_id == Comprob::OP ? "true" : "false"; ?>;
doc.matricula = <?php echo isset($socio->alumno) ? "\"" . $socio->alumno->matricula . "\"" : "false"; ?>;
jQuery(document).ready(function () {
  onReady();
});

function onReady() {

  bindHotkeys();

  $("#logo").dblclick(function () {
    window.location = "<?php echo $this->createUrl("doc/recibo&socio_id="); ?>" + socio_id;
  });

  igualaPos();

  $("#fecha_valor").datepicker();

  valores.init();


  $(":button").button();

  $('#socio').focus().autocomplete({
    autoSelect: true, autoFocus: true,
    minLength: 2, source: urlAC,
    select: function (event, ui) {
      //console.log(ui.item.comprob_id || "sin comprob_id");
      //return;
      if (ui.item.comprob_id) {
        if (ui.item.comprob_id == <?php echo Comprob::RECIBO_VENTAS; ?>) {
          window.location = "<?php echo $this->createUrl("/doc/recibo"); ?>&doc_id=" + ui.item.doc_id;
        } else if (ui.item.comprob_id == <?php echo Comprob::RECIBO_ELECTRONICO; ?>) {
          window.location = "<?php echo $this->createUrl("/doc/reciboElectronico"); ?>&doc_id=" + ui.item.doc_id;
        } else if (ui.item.comprob_id == <?php echo Comprob::RECIBO_BANCO; ?>) {
          window.location = "<?php echo $this->createUrl("/doc/reciboBanco"); ?>&doc_id=" + ui.item.doc_id;
        } else if (ui.item.comprob_id == <?php echo Comprob::OP; ?>) {
          window.location = "<?php echo $this->createUrl("/doc/op"); ?>&doc_id=" + ui.item.doc_id;
        }
        return;
      }
      socio_id = ui.item.id;
      socioChange(ui);
    }
  });

  if (socio_id != "") {
    $("#socio").val("<?php echo $socio ? $socio->nombre : ''; ?>");
    $("#div-head div#div-pago input#pago").focus();
    $("#socio").attr('disabled', "disabled");
    traeCta(socio_id, null, doc_id);
    traeValores(doc_id);
    if ((typeof (vm) !== "undefined") && (socioTipo === 'proveedor')) {
      vm.socioChange(<?php echo json_encode($socio && $socio->proveedor ? $socio->proveedor->attributes : null); ?>);
    }
  }

  $("body").on("change", "input", function () {
    $("#socio").attr('disabled', "disabled");
  });

  $("form #div-pago").on("change", "#pago", function () {
    pagoTotalChange($(this));
  });

  $("#doc-pendientes").on("focus", ".pago input", function () {
    ant = $(this).val();
  });

  $("#doc-pendientes").on("change", ".pago input", function () {
    pagoPendienteChange($(this));
  });

  $("#doc-pendientes").on("blur", ".pago input", function () {
    pagoPendienteBlur($(this));
  });

  $("#pago-a-cuenta-importe").change(function () {
    pagoACuentaChange($(this));
  });
  $("#pago-a-cuenta-importe").clearinput();
  $("#nota-de-credito select").chosen();

  $(document).keyup(function (e) {
    if (e.keyCode === 270) {
      var $e = $(e.target);
      if ($e.closest(".valor").length > 0) {
        confirmaValor(false);
      } else if (
       ($e.attr("class") in {"valor-id": 1, "pago": 2, "a-cuenta": 3, "body": 4})
       ||
       $e.context.localName === "body") {
        $('#cancel').click();
      }
    }
  });

  $(".docs").on("click", ".detalle-td input", function () {
    muestraDocDetalle($(this));
  });

  $("#doc-dets-div").dialog({
    autoOpen: false,
    width: "auto",
    height: "auto",
    title: "Detalle",
    buttons: {
      ok: function () {
        $(this).dialog("close");
      }
    }
  });

  $("#obs-dialog").dialog({
    autoOpen: false,
    width: 400,
    height: 500,
    title: "Observaciones",
    position: [
      "top", 50
    ],
    buttons: {
      ok: function () {
        $(this).dialog("close");
      }
    },
    close: function () {
      setFocusAndSelect($(".valor-id"), 200);
    }
  });

  $(".numeric").autoNumeric("init", {mDec: 2, aSep: ''});

  $("#numero").change(function () {
    validaNumeroComprob($(this));
  });
  $("#es_electronica").change(function () {
    console.log(1)
  });
  if (typeof (vm) !== "undefined") {
    vm.init();
  }

  if (typeof (vm) !== "undefined") {
    ko.applyBindings(vm);
    console.log(vm);
  }

}

function traeValores(doc_id) {
  $.ajax({
    type: "GET",
    data: {doc_id: doc_id},
    dataType: "json",
    url: "<?php echo $this->createUrl("doc/traeValores"); ?>",
    success: function (data) {
      $("#valores-table").html(data.cab);
      $("#valores-detalle-items").html(data.det);

      //$("#der").html(data);
    },
    error: function (data, status) {
    }
  });
}

function socioChange(ui) {
  chkSaldo(ui.item.id);
  if (ui.item.codigo) {
    $("#datos").html("Matrícula: " + ui.item.codigo + " / " + ui.item.curso + " / Beca:" + ui.item.beca + "%" + " / Resp.Pago:" + ui.item.responsablePago + " (" + ui.item.responsablePagoTipoDoc + "-" + ui.item.responsablePagoNumeroDoc + ' / dni:' + ui.item.numero_documento + ')').show();
    $("#saldo-cta").html(ui.item.saldo);
    doc.matricula = ui.item.codigo;
    if (typeof (vm) !== "undefined") {
      vm.socioChange(ui.item);
    }
    $("#inactivo").html(ui.item.activo);
    //$("#obs-div").html(ui.item.obs_cobranza);
    if (ui.item.obs_cobranza || ui.item.obs_beca) {
      $("#obs-dialog-cobranza").html(ui.item.obs_cobranza);
      $("#obs-dialog-beca").html(ui.item.obs_beca);
      $("#obs-dialog").dialog("open");
      setTimeout(function () {
        $("#obs-dialog").parent().find(".ui-button:nth-child(1)").focus();
      }, 300);
    }

  }
}

function pagoTotalChange($this) {
  traeCta(socio_id, $this.val());
  valores.refresh();
}

function pagoACuentaChange($this) {
  if ($("#pago-a-cuenta-detalle").val() == "") {
    $("#pago-a-cuenta-detalle").val("Pago a cuenta");
  }
  $this.val(round(Number($this.val())));
  doc.pagoACuenta = Number($this.val());
  doc.muestraTotales();
  valores.refresh();
  $(".valor-id").first().focus();


}

function chkSaldo(socio_id) {
  $.ajax({
    type: "GET",
    //      dataType:"json",
    data: {socio_id: socio_id},
    url: "<?php echo $this->createUrl("socioChkSaldo"); ?>",
    success: function (data) {
      if (Number(data) > 0) {
        window.location = "<?php echo $this->createUrl("doc/aplicacion&socio_id="); ?>" + socio_id + "&comprob_id=" + comprob_id;
      } else {
        traeCta(socio_id, null);
      }
    },
    error: function (data, status) {
    }
  });
}

function traeCta(socio_id, pago, doc_id) {
  $.ajax({
    type: "GET",
    data: {"socio_id": socio_id, "pagoTotal": pago, doc_id: doc_id},
    url: "<?php echo $this->createUrl("doc/docsAAplicar"); ?>",
    success: function (data) {
      $("#doc-pendientes .docs").html(data);
      doc.muestraTotales();
      igualaPos();
      valores.refresh();
      contextMenu();
      if ($(".valor-id").length > 0) {
        //$(".valor-id").focus();
        setFocusAndSelect($(".valor-id"), 100);
      } else {
        setFocusAndSelect($("#pago-a-cuenta-importe"), 100);
        //$("#pago-a-cuenta-importe").focus().select();
      }
      bindHotkeys();
      //var obsCobranza = $("#obs-cobranza-input").val();
      //if (obsCobranza.length > 1) {
//                    $("#obs-cobranza-dialog").find("div").html(obsCobranza);
//                    $("#obs-cobranza-dialog").dialog("open");
//                }
    },
    error: function (data, status) {
    }
  });
}

function pagoPendienteBlur($obj) {

}

function pagoPendienteChange(e) {
  e.val(round(Number(e.val())));
  var pago = e.val();
  var saldo = Number(e.attr("saldo"));
  if (Math.abs(pago) > Math.abs(saldo)) {
    e.val(ant);
    alert("Error: el pago es mayor al saldo");
    return false;
  }

  doc.totalPagos = round(doc.totalPagos - (ant - pago), 2);
  ant = pago;
  doc.muestraTotales();
  valores.refresh();
}

function chkRecibo() {
  //    var data, index = 0, valor_id, input_nombre, valor_nombre;
  //    data = $("#main-form").find("input").serialize();
  //    data += "&doc[total]="+doc.total;
  //    $("#valores-table").find("tr").each(function(i,e){
  //      valor_nombre = $(e).find(".valor-nombre").html();
  //      data += "&valores["+index+"]["+valor_nombre+"][importe]=";
  //      data += $(e).find(".valor-importe").val();
  //      valor_id=$(e).find(".valor-id").attr("id");
  //      $("#form_"+valor_id).find("input").each(function(i,e){
  //        input_nombre = $(e).attr("name"));
  //        if(input_nombre){
  //          data += "&valores["+index+"]["+valor_nombre+"]["+input_nombre+"]=";
  //          data += $(e).val();
  //        }
  //      })
  //      index++;
  //    })
  //
  //
  //    //console.log(data["recibo numero"]);
  //
  //    return true;
}

function limpiaTodo() {
  valores.toggleEnabled(1);
  doc.toggleEnabled(1);
  $("#socio").removeAttr('disabled');
  $("#doc-pendientes div.docs table tbody").remove();
  $("#izq .total").html("0");
  $("#pago").val("");
  $("#socio").val("").focus();
  $("#pago-a-cuenta-div input").val("");
  $("#inactivo").html("");
  $("#div-head").show();
  doc.totalPagos = 0;
  doc.totalAPagar = 0;
  doc.pagoACuenta = 0;
  doc.disabled = false;
  valores.limpia();
  setTimeout(function () {
    $("#socio").focus();
  }, 100);
}

function reporteCtaCte() {
  var urlImpresionCtaCte = "<?php echo $this->createUrl("/doc/reciboCtaCte"); ?>";
  $.ajax({
    type: "GET",
    data: {socio_id: socio_id},
    url: urlImpresionCtaCte,
    success: function (data) {
      $("#rep-ctacte").html(data);
      $("#rep-ctacte").dialog({
        visible: true,
        title: "Reporte de Cta.Cte.",
        width: "auto",
        position: ["auto", 10]
      });
      $("#table-ctacte-div").animate({scrollTop: 1000000}, 0);
    },
    error: function (data, status) {
      alert("Error/es:\n" + data)
    }
  });
}

function reporteCtaCteViejo() {
  var urlImpresionCtaCte = "<?php echo $this->createUrl("/doc/reciboCtaCteViejo"); ?>";
  $.ajax({
    type: "GET",
    data: {socio_id: socio_id},
    url: urlImpresionCtaCte,
    success: function (data) {
      $("#rep-ctacte").html(data);
      $("#rep-ctacte").animate({scrollTop: 1000000}, 0);
      //$("table").stickyTableHeaders();
      $("#rep-ctacte").dialog({
        visible: true,
        title: "Reporte de Cta.Cte. del sistema viejo",
        width: "auto",
        position: ["auto", 0],
        open: function () {
          $("#rep-ctacte").animate({scrollTop: 1000000}, 0);
        },
        buttons: {
          ok: function () {
            $(this).dialog("close");
          }
        }
      });
    },
    error: function (data, status) {
      alert("Error/es:\n" + data)
    }
  });
}

function submitReciboOp(imprime) {
//    return false;
  var data = $("#main-form").find("input").serialize();
  var esElectronica = "<?php echo $esElectronica; ?>";
  imprime = (!imprime) ? 0 : 1;
  data += "&doc[tipo]=" + doc.tipo;
  data += "&doc[total]=" + doc.total;
  data += "&doc[saldo]=" + doc.total;
  data += "&doc[Socio_id]=" + socio_id;
  data += "&doc[esElectronica]=" + esElectronica;
  if (submitting) {
    alert('Está grabando');
    return;
  }
  submitting = true;
  $.ajax({
    type: "POST",
    data: data + valores.getData(),
    dataType: "json",
    url: "<?php echo $this->createUrl("doc/docGraba"); ?>",
    complete: function (data) {
    submitting = false;
      //alerta(data.responseText);
      //return false;
    },
    success: function (data) {
//      console.log(data);
      if (data.errores && muestraErroresDoc(data.errores)) {
        return false;
      }
      $("#div-head *").removeAttr('disabled');
      var dataAnt = data;
      if (imprime === 1) {
        if (imprimeAuto == "S" || imprimeAuto == "P") { // el servidor lo graba y un script con el autohotkey lo imprime
          $.ajax({
             type: "GET",
             data: {doc_id: data.doc_id},
             url: urlImpresion,
             dataType: "json",
             success: function (data) {
               if (dataAnt.afip) {
                 alert("Proceso Afip:\nCAE obtenido:" + dataAnt.afip.cae + "\nErrores:" + dataAnt.afip.err_msg + "\nObservaciones:" + dataAnt.afip.motivos_obs)
               }
               window.location = urlDoc;
             },
             error: function (data, status) {
               if (data.responseText && data.responseText !== "") {
                 alerta(data.responseText, "No se pudo imprimir!", {
                   ok: function () {
                     $(this).dialog("close");
                     window.location = urlDoc;
                   }
                 });
               } else {
                 window.location = urlDoc;
               }
             }
           }
          );
          return false;
        } else {
          window.open(urlImpresion + "&doc_id=" + data.doc_id, 'newwwin', 'menubar=no,width=750,height=550,toolbar=no,scrollbars=yes,screenX=300');
        }
      }
//                return false;
      if (data.afip) {
        alert("Proceso Afip:\nCAE obtenido:" + data.afip.cae + "\nErrores:" + data.afip.err_msg + "\nObservaciones:" + data.afip.motivos_obs)
      }
      window.location = urlDoc;
    },
    error: function (data, status) {
      alert(data.responseText)
    }
  });

  return false;
}

function contextMenu() {
  var id;
  $('.docs tr').contextMenu('contextMenu', {
    bindings: {
      'open': function (t) {
        var doc_id = $(t).attr("id");
        $("#nota-de-credito").dialog({
          title: "Nota de crédito",
          width: "auto",
          buttons: {
            ok: function () {
              $.ajax({
                type: "POST",
                dataType: "json",
                data: $("#nota-de-credito form").serialize() + "&socio_id=" + socio_id + "&doc_id=" + doc_id,
                url: "<?php echo $this->createUrl("doc/reciboGrabaNotaDeCredito"); ?>",
                success: function (data) {
                  if (data.errores && muestraErroresDoc(data.errores)) {
                    return false;
                  }
                  var $pago = $(t).find(".pagodoc");
                  $pago.focus(); //para que entre antes y haga lo que tiene que hace el onfocus
                  var importeAnt = Number($(t).find(".saldo").html());
                  $(t).find(".saldo").html(data.saldo);
                  var diferencia = importeAnt - Number($(t).find(".saldo").html());
                  doc.totalAPagar -= Number(diferencia);
                  var pago = Number($pago.val());
                  $pago.val(pago - diferencia);
                  $pago.change();
                  $("#nota-de-credito")
                  $("#nota-de-credito").find("input").val("");
                  alert("La Nota de crédito fue grabada");
                },
                error: function (data, status) {
                }
              });
              $(this).dialog("close");
            },
            cancelar: function () {
              $(this).dialog("close");
            }
          }
        });
        return true;
      }
    }
  });
}

function muestraDocDetalle($this) {
  $.ajax({
    type: "GET",
//      dataType:"json",
    data: {doc_id: $this.closest("tr").attr("id")},
    url: "<?php echo $this->createUrl("doc/getDetalle"); ?>",
    success: function (data) {
      $("#doc-dets-div").html(data);
      $("#doc-dets-div").dialog("open");
    },
    error: function (data, status) {
    }
  });
}

function impresionCtaCte() {
  var fechaDesde = "<?php echo date("d/m/Y", time() - 30 * 24 * 60 * 60); ?>";
  var fechaHasta = "<?php echo date("d/m/Y", time()); ?>";
  var url = "<?php echo $this->createUrl("informe/alumnoCtaPDF"); ?>" + "&socio_id=" + socio_id + "&fecha_desde=" + fechaDesde + "&fecha_hasta=" + fechaHasta;
  window.open(url);
}

function bindHotkeys(e) {
  if (e) {
    $es = e;
  } else {
    $es = $("input:visible, body");

    $es.bind("keydown.esc.recibo", "esc", function (e) {
      $("#obs-dialog").dialog("close");
    });

    $("input:visible, body").unbind("keydown.a");
    $("input:visible, body").unbind("keydown.a");
  }
//        $es.bind("keydown.a", "ctrl+c", function(e) {
//            $("#cancel").click();
//            return false;
//        });

  $es.bind("keydown.a", "ctrl+s", function (e) {
    $("#ok-imprime").click();
    return false;
  });
  $es.bind("keydown.a", "ctrl+p", function (e) {
    if ($(this).hasClass("pagodoc")) {
      setTimeout(function () {
        $(".valor-id:visible").first().focus().select();
      });
      setTimeout(function () {
        $(".valor-id:visible").first().focus().select();
      });
    } else {
      $(".pago .pagodoc:visible").first().focus().select();
    }
    return false;
  });
}

/*
 Run
 */

</script>
