<?php include("recibo_op.css.php"); ?>
<?php $this->renderPartial("/doc/common.js"); ?>
<?php $this->renderPartial("/doc/common.css"); ?>
<?php include("recibo_op.js.php"); ?>
<?php $this->renderPartial("/doc/valores/valores.css"); ?>
<?php $this->renderPartial("/doc/valores/valores.js"); ?>
<?php /* @var $doc Doc */?>
<span data-bind="text: valores.koTotalValores"></span>
<form id="main-form" name="main" method="POST" action="#">
  <input type="hidden" name="doc[comprob_id]" value="<?php echo $comprob_id; ?>"/>
  <input type="hidden" name="doc[id]" value="<?php echo $doc->id; ?>"/>
  <div class="titulo">
    <span><?php echo $this->pageTitle; ?></span>
  </div>

  <div id="div-numero">
    <input name ="doc[numero]" id="numero" value="<?php echo $doc->numeroCompleto; ?>"
           placeholder="Número"/>
  </div>
  <div id="div-head" stylle="<?php echo $doc_id ? '' : 'display:none'; ?>">
    <div id="div-socio">
      <!--            <label for="socio"></label>-->
      <input name="doc[socio]" id="socio" value=""
             placeholder="<?php echo $comprob_id == Comprob::OP ? "Proveedor" : "Alumno"; ?>"
             />
      <span id="fecha-valor-input">Fecha valor</span>
      <input tabindex="-1" name="doc[fecha_valor]" id="fecha_valor" placeholder=""
             value="<?php echo date("d/m/Y", time()); ?>"/>
      <span id="datos" class="ui-widget ui-widget-content ui-corner-all ui-button">
				<?php
				if (isset($socio->alumno)) {
					echo "Matrícula: " . $data["matricula"] . " / Curso:" . $data["curso"] . " / Beca:" . $data["beca"]. " / "."Resp.Pago:".$data["encargado_pago"]." (".$data["encargado_tipo_doc"]."-".$data["encargado_numero_doc"].")";
				}
				?>
      </span>
    </div>
    <div id="div-pago" class="total">
      <!--            <label for="pago">Pago</label>-->
      <!--<input id="pago"  name="doc[pago]" placeholder="Pago"/>-->
    </div>
  </div>
  <div id="izq">
    <div id="doc-pendientes">
      <div class="titulo">
        <span id="inactivo"></span>
        <span>Documentos a pagar/cancelar</span>
      </div>
      <div class="docs">
      </div>
    </div>
    <div id="total-pagos-div">
      <div id="total-a-pagar">
        <span class="label">Total</span>
        <span class="total">0</span>
      </div>
      <div id="total-pagado">
        <span class="label">Pago</span>
        <span class="total">0</span>
      </div>
      <div id="total-saldo">
        <span class="label">Saldo</span>
        <span class="total">0</span>
      </div>
      <div id="total-saldo-cta">
				<?php
				$saldoCta = isset($data["saldo"]) ? $data["saldo"] : 0;
				?>
        <span class="label">Saldo Cta.Cte.</span>
        <span id="saldo-cta" class="total"><?php echo $saldoCta; ?></span>
      </div>
    </div>
    <div id="pago-a-cuenta-div">
      <div class="titulo">
        <span>Pago a cuenta</span>
      </div>
      <input class="" id="pago-a-cuenta-detalle" name="doc[pago-a-cuenta-detalle]"
             placeholder="Detalle" value="<?php echo $doc->pago_a_cuenta_detalle; ?>"/>
      <input class="a-cuenta numeric" id="pago-a-cuenta-importe"
             name="doc[pago-a-cuenta-importe]" placeholder="Importe"
             value="<?php echo $doc->pago_a_cuenta_importe; ?>"/>
    </div>
    <div id="total-div">
      <span class="label">Total del documento</span>
      <span class="total">0</span>
    </div>
  </div>


  <div id="der">
		<?php $this->renderPartial("/doc/valores/_valores"); ?>
  </div>
</form>

<div id="botones">
  <input type="button" id="ImpresionCtaCte" value="Impresión CtaCte" onclick="return impresionCtaCte();"/>
  <input type="button" id="CtaCte" value="CtaCte" onclick="return reporteCtaCte();"/>
  <input type="button" id="CtaCteViejo" value="CtaCte(sist.viejo)" onclick="return reporteCtaCteViejo()"/>
  <input type="button" id="cancel" value="Cancela" onclick="return limpiaTodo();"/>
  <input type="button" id="ok" value="Graba" onclick="return submitReciboOp();"/>
  <input type="button" id="ok-imprime" value="Graba e imprime" onclick="return submitReciboOp('imprime');"/>
</div>

<div id="contextMenu">

  <ul>

    <li id="open">Nota de crédito</li>

  </ul>

</div>

<div id="rep-ctacte">
</div>
<div id="doc-dets-div"></div>

<div id="nota-de-credito">
  <form method="POST" action="#">
    <input class="numeric" type="text" name="importe" placeholder="Importe"/>
		<?php Concepto::treeAndInput("concepto_id"); ?>
    <input type="text" name="obs" placeholder="Observaciones"/>
  </form>
</div>

<div id="obs-dialog">
  <div class="obs-label">Cobranza</div>
  <div id="obs-dialog-cobranza" class="ui-widget ui-widget-content ui-corner-all"></div>
  <div class="obs-label">Beca</div>
  <div id="obs-dialog-beca" class="ui-widget ui-widget-content ui-corner-all"></div>
</div>
<script type="text/javascript">
	igualaPos();
</script>