<script type="text/javascript">
    var doc = new Object(), ant = 0;
    doc.pagoACuenta = 0, doc.totalPagos = 0, doc.total = 0
    doc.saldo = 0, doc.disabled = false;
    doc.toggleEnabled = function(on) {
        doc.disabled = !on;
        if (on) {
            $(".td_edit_img *").removeAttr('disabled');
            $(".td_delete_img *").removeAttr('disabled');
            $("#pago").removeAttr('disabled');
            $("#izq *").removeAttr('disabled');
            $("#botones *").removeAttr('disabled');
            $("#pago-a-cuenta-div *").removeAttr('disabled');
        } else {
            $(".td_edit_img *").attr('disabled', "disabled");
            $(".td_delete_img *").attr('disabled', "disabled");
            $("#pago").attr('disabled', "disabled");
            $("#izq *").attr('disabled', "disabled");
            $("#botones *").attr('disabled', "disabled");
            $("#botones #CtaCte").removeAttr('disabled');
            $("#botones #cancel").removeAttr('disabled');
        }
        return false;
    }
    doc.muestraTotales = function() {
        doc.saldo = round(Number(doc.totalAPagar) - Number(doc.totalPagos));
        doc.total = round(Number(doc.totalPagos) + Number(doc.pagoACuenta));

        if (cajaCerrada) {
            doc.toggleEnabled(0);
            valores.toggleEnabled(0);
        }

        $("form #total-pagos-div #total-a-pagar span.total").html(round(doc.totalAPagar));
        $("#total-pagos").html(round(doc.totalPagos));
        $("form #total-pagos-div #total-pagado span.total").html(round(doc.totalPagos));
        $("#total-pagos").html(doc.total);
        $("form #total-pagos-div #total-saldo span.total").html(round(doc.saldo));

        $("form #total-div span.total").html(round(doc.total));

        $("form #div-pago #pago").val(round(doc.total));

        //    $("#total-pagos-div div#total-a-pagar span.total").html(doc.total);

    }
    doc.esSalida = true;
    var afectacion = "<?php echo $entrada->talonario->comprob->signo_caja; ?>";



    jQuery(document).ready(function() {
        onReady();
    });

    function onReady() {

//    igualaPos();

        $("#destino_id").chosen().change(function(a) {
        });

        $("#detalle").focus();


        valores.init();

        $(":button").button();

        $("#izq-content").on("focus", ".pago", function() {
            ant = $(this).val()
        });

        $("#izq-content").on("change", ".pago", function() {
            importeAjusteChange($(this));
        });

        $(".numeric").autoNumeric("init", {mDec: 2, aSep: ''});

        $("#numero").change(function() {
            validaNumeroComprob($(this));
        });

    }

    function importeAjusteChange(e) {
        e.val(round(Number(e.val())));
        var pago = e.val();
        doc.totalPagos = round(doc.totalPagos - (ant - pago), 2);
        ant = pago;
        doc.muestraTotales();
        valores.refresh();

    }

    function submitMov() {
        $.ajax({
            type: "POST",
            dataType: "json",
            data: $("#main-form").serialize() + valores.getData(),
            url: "<?php echo $this->createUrl("doc/transferenciaDepositoGraba"); ?>",
            success: function(data) {
                if (data) {
                    muestraErroresDoc(data);
                } else {
                    window.location = window.location;
                }
//                if (data && data.error) {
//                    var error = "";
//                    for (var i = 0; i < data.error.length; i++) {
//                        error += data.error[i] + "\n";
//                    }
//                    alert(error);
//                }
//                else {
//                    alert('Depósito Registrado');
//                    window.location = window.location;
//                }
            },
            error: function(data, status) {
            }
        });
        return false;
    }

    function limpiaTodo() {
        window.location = window.location;
        valores.toggleEnabled(1);
        doc.toggleEnabled(1);
        $("#izq .total").html("0");
        $("#pago").val("");
        $("#pago-a-cuenta-div input").val("");
        $("#div-head").show();
        doc.totalPagos = 0;
        doc.totalAPagar = 0;
        doc.pagoACuenta = 0;
        doc.disabled = false;
        valores.limpia();
    }

</script>