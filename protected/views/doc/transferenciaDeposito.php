<?php $this->renderPartial("/doc/common.js"); ?>
<?php $baseUrl = Yii::app()->request->baseUrl; ?>
<?php $this->renderPartial("/doc/common.css"); ?>
<?php include("transferenciaDeposito.js.php"); ?>
<?php include("transferenciaDeposito.css.php"); ?>
<?php include("valores/valores.css.php"); ?>
<?php include("valores/valores.js.php"); ?>
<?php //$doc_id = 84963;                 ?>
<?php $doc_id = null; ?>
<form id="main-form" name="main" method="POST" action="#">
  <?php /* @var $entrada Doc */; ?>
  <input type="hidden" name="comprob_tipo_id"
         value="<?php echo $entrada->talonario->comprob_id; ?>"/>
  <input type="hidden" name="comprob_tipo_id_entrada"
         value="<?php echo $salida->talonario->comprob_id; ?>"/> 
  <div id="div-numero">
    <input name ="doc[numero]"
           id="numero" value="<?php echo $entrada->numeroCompleto; ?>"
           ddisabled="DISABLED"
           placeholder="Número"/>
  </div>
  <input type="hidden" name="doc[tipoDestino]"
         value="<?php echo $tipoDestino; ?>"/>
  <div id="izq">
    <div class="titulo">
      <span><?php echo $tipoDestino === "T" ? "Transferencia" : "Depósito"; ?></span>
    </div>
    <div id="izq-content">

      <div class="row">
        <label for="detalle">Detalle:</label>
        <input name="doc[detalle]" id="detalle" value="" placeholder="Detalle"/>
      </div>
      <div class="row">
        <label for="importe">Importe:</label>
        <input name="doc[importe]" id="importe" value="" placeholder="Importe" class="pago numeric"/>
      </div>
      <div class="ui-widget-content ui-corner-all" id="cuentas-div">
        <div class="ui-widget-content ui-corner-all" id="cuenta-salida-div">
          <div class="titulo">Cuenta Origen</div>
          <label for="destino_id">Cuenta:</label>
          <div id="caja-salida-div"><?php echo $cajaActiva->nombre; ?></div>
        </div>
        <div class="ui-widget-content ui-corner-all" id="cuenta-entrada-div">
          <div class="titulo">Cuenta Destino</div>
          <div class="row">
            <label for="destino_id">Cuenta:</label>
            <select id="destino_id" name="doc[destino_id]" data-placeholder="Seleccione una cuenta">
              <option></option>
              <?php echo $destinoListData; ?>
            </select>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div id="der">
    <?php include("valores/_valores.php"); ?>
  </div>

  <div id="botones">
    <input type="button" id="cancel" value="Cancela" onclick="return limpiaTodo();"/>
    <input type="button" id="ok" value="Graba" onclick="return submitMov();"/>
  </div>
</form>
</div>
<script type="text/javascript">
      igualaPos();
</script>