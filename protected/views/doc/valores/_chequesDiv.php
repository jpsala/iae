<style type='text/css'>
    .cheque-nombre-td{width:300px}
    .cheque-numero-td{width:150px}
    .cheque-importe-td,.cheque-fecha-td,.cheque-numero-td{text-align: right}
    th span {
        /*padding-right: 13px;*/
    }
</style>
<div id="cheques-tercero">
    <table class="tablesorter"  cellspacing="1">
        <thead>
            <tr>
                <th><span>Banco</span></th>
                <th class='cheque-numero-td'><span>Número</span></th>
                <th class='cheque-fecha-td'><span>Acreditación</span></th>
                <th class='cheque-origen-td'><span>Origen</span></th>
                <th class='cheque-importe-td'><span>Importe</span></th>
                <th></th>
            </tr>
        </thead>
        <?php /* @var $chq DocValorCartera */; ?>
        <?php $total = 0; ?>
        <?php $tipoCheque = DocValor::TIPO_CHEQUE; ?>
        <tbody>
          <?php
            // nahila cheques anulados se tiene que ver para sacarlos de la cartera /* and !d.anulado */
            $docValores = DocValor::model()->findAllBySql("
              select dv.*
                from doc_valor dv
                  inner join doc d on d.id = dv.doc_id/* and !d.anulado */
              where dv.doc_valor_id is null and (dv.chq_estado is null and dv.tipo = $tipoCheque)
            ");
          ?>
            <?php foreach ($docValores as $chq): ?>
                <tr cheque_id="<?php echo $chq->id; ?>">
                    <td class='cheque-nombre-td'><?php echo $chq->banco ? $chq->banco->nombre : ""; ?></td>
                    <td class='cheque-numero-td'><?php echo $chq->numero; ?></td>
                    <td class='cheque-fecha-td'><?php echo date("d/m/Y", mystrtotime($chq->fecha)); ?></td>
                    <td class='cheque-origen-td'><?php echo $chq->chq_origen; ?></td>
                    <td class='cheque-importe-td'><?php echo number_format($chq->importe, 2, ".", ""); ?></td>
                    <td><input
                            class='cheque-check'
                            type="checkbox"
                            onclick='chequeTerceroClick($(this));'
                            name='cheque_tercero[<?php echo $chq->id; ?>]'/></td>
                </tr>
                <?php $total+=$chq->importe; ?>
            <?php endforeach; ?>
            <tr>
                <th>Total</th>
                <th class='cheque-numero-td'></th>
                <th class='cheque-fecha-td'></th>
                <th class='cheque-importe-td' id="cheques-tercero-dialog-total-td"><?php echo number_format($total, 2, ".", ""); ?></th>
                <th></th>
            </tr>
        </tbody>
    </table>
</div>
<script type="text/javascript">
                            $("#cheques-tercero table").tablesorter({headers: {
                                    // assign the secound column (we start counting zero)
                                    8: {
                                        // disable it by setting the property sorter to false
                                        sorter: false
                                    }
                                }});
</script>
