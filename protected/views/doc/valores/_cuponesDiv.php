<style type='text/css'>
    .tarjeta-nombre-td{width:300px}
    .cupon-importe-td,.cupon-fecha-td,.cupon-numero-td{text-align: right}
</style>
<div id="cupones">
    <table>
        <tr>
            <th>Tarjeta</th>
            <th class='cupon-numero-td'>Número</th>
            <th class='cupon-numero-td'>Lote</th>
            <th class='cupon-fecha-td'>Fecha</th>
            <th class='cupon-importe-td'>Importe</th>
            <th><input name="checktodos" type="checkbox" /></th>
            <th></th>
        </tr>
        <?php /* @var $chq DocValorCartera */; ?>
        <?php $total = 0; ?>
        <?php foreach (DocValor::model()->recuperaCupones() as $cupon): ?>
            <tr cupon_id="<?php echo $cupon['id']; ?>">
                <td class='tarjeta-nombre-td'><?php echo $cupon['nombre']; ?></td>
                <td class='cupon-numero-td'><?php echo $cupon['numero']; ?></td>
                <td class='cupon-numero-td'><?php echo $cupon['lote']; ?></td>
                <td class='cupon-fecha-td'><?php echo date("d/m/Y", mystrtotime($cupon['fecha_creacion'])); ?></td>
                <td class='cupon-importe-td'><?php echo number_format($cupon['importe'], 2); ?></td>
                <td><input 
                        class='cupon-check'
                        id='cupon-check'
                        type="checkbox" 
                        onclick='cuponClick($(this));'
                        name='cupon[<?php echo $cupon['id']; ?>]'/></td>
            </tr>
            <?php $total+=$cupon['importe']; ?>
        <?php endforeach; ?>
        <tr>
            <th>Total</th>
            <th class='cupon-numero-td'></th>
            <th class='cupon-numero-td'></th>
            <th class='cupon-fecha-td'></th>
            <th class='cupon-importe-td' id="cupones-dialog-total-td"><?php echo number_format($total, 2); ?></th>
            <th></th>
        </tr>
    </table>
</div>

<script type="text/javascript">
                $("input[name=checktodos]").change(function() {
                    $('input[type=checkbox]').each(function() {
                        if ($("input[name=checktodos]:checked").length === 1) {
                            this.checked = true;
                        } else {
                            this.checked = false;
                        }
                    });
                    cuponClick(this);
                });

</script>