<style type='text/css'>
    .tarjeta-nombre-td{width:300px}
    .retencion-importe-td,.retencion-fecha-td,.retencion-numero-td{text-align: right}
</style>
<div id="retenciones">
    <table>
        <tr>
            <th>Retención</th>
            <th class='retencion-numero-td'>Número</th>
            <th class='retencion-fecha-td'>Fecha</th>
            <th class='retencion-numero-td'>Origen</th>
            <th class='retencion-numero-td'>CUIT</th>
            <th class='retencion-importe-td'>Importe</th>
            <th><input name="checktodos" type="checkbox" /></th>
            <th></th>
        </tr>
        <?php /* @var $chq DocValorCartera */; ?>
        <?php $total = 0; ?>
        <?php foreach (DocValor::model()->recuperaRetenciones() as $retencion): ?>
            <tr retencion_id="<?php echo $retencion['id']; ?>">
                <td class='retencion-numero-td'><?php echo $retencion['numero']; ?></td>
                <td class='tarjeta-nombre-td'><?php echo $retencion['fecha']; ?></td>
                <td class='tarjeta-nombre-td'><?php echo $retencion['origen']; ?></td>
                <td class='tarjeta-nombre-td'><?php echo $retencion['cuit']; ?></td>
                <td class='retencion-importe-td'><?php echo number_format($retencion['importe'], 2); ?></td>
                <td><input 
                        class='retencion-check'
                        id='retencion-check'
                        type="checkbox"
                        onclick='retencionClick($(this));'
                        name='retencion[<?php echo $retencion['id']; ?>]'/></td>
            </tr>
            <?php $total+=$retencion['importe']; ?>
        <?php endforeach; ?>
        <tr>
            <th>Total</th>
            <th class='retencion-numero-td'></th>
            <th class='retencion-numero-td'></th>
            <th class='retencion-fecha-td'></th>
            <th class='retencion-importe-td' id="retenciones-dialog-total-td"><?php echo number_format(0, 2); ?></th>
            <th></th>
        </tr>
    </table>
</div>

<script type="text/javascript">
                $("input[name=checktodos]").change(function() {
                    $('input[type=checkbox]').each(function() {
                        if ($("input[name=checktodos]:checked").length === 1) {
                            this.checked = true;
                        } else {
                            this.checked = false;
                        }
                    });
                    retencionClick(this);
                });

</script>