<tbody>
    <?php $total = 0; ?>
    <?php $nombres = array(1 => "Efectivo", 2 => "Cheque", 3 => "Tarjeta", 4 => "Cheque-propio", 5 => "Recaudación", 6 => "Transferencia", 7 => "Retención"); ?>
    <?php foreach ($valores as $valor): ?>
        <?php $total += $valor["importe"]; ?>
        <?php $tipo = $valor["tipo"] + 1; ?>
        <tr id="rec_<?php echo $valor["id"] ?>">
            <td><input value="<?php echo $tipo; ?>" name="valor-id" id="<?php echo $valor["id"] ?>" class="valor-id"></td>
            <td class="valor-nombre-td"><span class="valor-nombre"><?php echo $nombres[$tipo]; ?></span></td>
            <td class="right-align valor-importe-td"><span class="divclearable"><span class="divclearable">
                        <input value="<?php echo $valor["importe"] ?>" class="valor-importe"/>
                        <a tabindex="-1" href="javascript:"></a></span><a tabindex="-1" href="javascript:"></a></span>
            </td>
            <td class="td_edit_img"><img src="images/edit.jpg"></td>
            <td class="td_delete_img"><img src="images/delete.jpg"></td>
        </tr>
    <?php endforeach; ?>

</tbody>
<script>
    //doc.total = <?php $total; ?>
    //valores.init();
    $("#valores-table").on("mouseover", "tr", function(e) {
        id = $(this).find(".valor-id").attr("id");
        if (id.length > 0) {
            muestraDetalle(e, id);
        }
    }).on("mouseout", "tr", function(e) {
        id = $(this).find(".valor-id").attr("id");
        if (id.length > 0) {
            muestraDetalle(e, id);
        }
    });
    //valores.totalValores = <?php echo $total; ?>;
    //doc.totalPagos = <?php echo $total; ?>;
    //valores.refresh();
</script>

<?php return; ?>    
