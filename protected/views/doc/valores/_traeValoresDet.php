<?php
    foreach ($valores as $valor) {
        switch ($valor["tipo"] + 0) {
            case 0:
                traeEfectivoDet($valor);
                break;
            case DocValor::TIPO_CHEQUE:
                traeChequeDet($valor);
                break;
            case 2:
                traeTarjetaDet($valor);
                break;
            case 3:
                traeChequePropioDet($valor);
                break;
            case 4 or 5:
                traeRecaudacion($valor);
                break;
            case 7:
                traeRetencion($valor);
                break;
            default:
                break;
        }
    }
?>
<?php

    function traeEfectivoDet($valor) {
        ?>
    <?php } ?>

<?php

    function traeRecaudacion($valor) {
        ?>
        <div id="form_<?php echo $valor["id"]; ?>" class="valor" style="display: none;">
            <!--    <input type="hidden" value="" name="rec-tipo-valor"/>-->
            <?php $tipo = $valor == "4" ? DocValor::TIPO_RECAUDACION_BANCARIA : DocValor::TIPO_TRANSFERENCIA; ?>
            <input type="hidden" name="tipo" value="<?php echo $tipo; ?>">

            <div class="row">
                <select name="Destino_id" data-placeholder="- Cta. Bancaria -"
                        class="Destino_id" style="width:150px">
                    <?php
                        $k = array();
                        echo CHtml::listOptions($valor["Destino_id"], CHtml::listData(
                                Destino::model()->with("banco")->findAll("tipo = 2"), "id", "nombreMasCuenta"
                            ), $k
                        );
                    ?>
                </select>

                <div>
                    <input placeholder=" - Fecha Acreditación - "
                           value="<?php echo $valor["fecha"]; ?>"
                           class="fecha_acreditacion" name="fecha" id="dp1394637331151"
                           size="10"><img class="ui-datepicker-trigger"
                                          src="images/calendar.gif" alt="..." title="...">
                    <input placeholder=" - Número - "
                           value="<?php echo $valor["numero"]; ?>" class="numero"
                           name="numero">
                    <input placeholder=" - Observaciones - "
                           value="<?php echo $valor["obs"]; ?>" class="obs" name="obs">
                </div>
            </div>
        </div>
        <script>
            $detalle = $("#form_<?php echo $valor["id"]; ?>");
            $detalle.find(".Destino_id").chosen().change(function () {
                setFocus($detalle.find(".fecha_acreditacion"), 200);
                //$("#lote").focus();
            });
        </script>
    <?php } ?>
<?php
    function traeRetencion($valor) {
        ?>
        <div id="form_<?php echo $valor["id"]; ?>" class="valor" style="display: none;">
            <!--    <input type="hidden" value="" name="rec-tipo-valor"/>-->
            <?php $tipo = DocValor::TIPO_RETENCION; ?>
            <input type="hidden" name="tipo" value="<?php echo $tipo; ?>">

            <div class="row">
                <div>
                    <!--				<input placeholder="Porcentaje" value="-->
                    <?php //echo $valor[""]; ?><!--" class="obs" name="obs">-->
                </div>
            </div>
        </div>
        <script>
            $detalle = $("#form_<?php echo $valor["id"]; ?>");
            $detalle.find(".Destino_id").chosen().change(function () {
                setFocus($detalle.find(".fecha_acreditacion"), 200);
                //$("#lote").focus();
            });
        </script>
    <?php } ?>

<?php
    function traeChequeDet($valor) {
        ?>
        <div id="form_<?php echo $valor["id"]; ?>" class="valor" style="display: none;">
            <input type="hidden" name="tipo" value="<?php echo DocValor::TIPO_CHEQUE; ?>">

            <div class="row">
                <div class="banco_mas_numero inner-row">
                    <select name="banco_id" id="" data-placeholder="- Banco -"
                            class="banco_id" style="width:150px">
                        <?php $k = array('prompt' => ''); ?>
                        <?php echo CHtml::listOptions($valor["banco_id"], CHtml::listData(Banco::model()->findAll(), "id", "nombreMasCodigo"), $k); ?>
                    </select>
                    <input placeholder=" - Número - "
                           value="<?php echo $valor["numero"] ?>" class="cheque-numero"
                           name="numero">
                </div>
                <div class="fecha_mas_origen inner-row">
                    <input placeholder=" - Fecha Acreditación - "
                           onchange="chequeFechaAcreditacionChange($(this));"
                           value="<?php echo $valor["fecha"]; ?>"
                           class="cheque-fecha-acreditacion" name="fecha"
                           id="dp1387032794193" size="10"
                        />
                    <input placeholder=" - Origen - "
                           value="<?php echo $valor["chq_origen"] ?>"
                           class="cheque-origen" name="chq_origen"/>
                </div>
                <div class="cuit_mas_interes inner-row">
                    <input placeholder=" - CUIT Endosante - "
                           value="<?php echo $valor["chq_cuit_endosante"] ?>"
                           class="cheque-cuit" name="chq_cuit_endosante">

                    <div id="div-cheque-genera-interes">
                        <label for="cheque-genera-interes">
                            Genera Interes
                            <input type="checkbox" placeholder="Genera Interes"
                                   onchange="chequeGeneraInteresChange($(this));" value=""
                                   class="cheque-genera-interes"
                                   name="cheque_genera_interes">
                        </label>
                    </div>
                </div>
                <script type="text/javascript">
                    $(".cheque-fecha-acreditacion").datepicker();
                    if (!doc.tipo) {
                        $("#div-cheque-genera-interes").hide();
                    }
                    $(".banco_id").chosen();
                    $detalle = $("#form_<?php echo $valor["id"]; ?>");
                </script>
            </div>
        </div>
    <?php
    }

    function traeTarjetaDet($valor) {
        ?>
        <div id="form_<?php echo $valor["id"]; ?>" class="valor" style="display: none;">
            <input type="hidden" name="tipo" value="2">

            <div class="row">
                <div class="inner-row">
                    <select style="width: 150px" name="tarjeta_id"
                            data-placeholder="- Tarjeta -" class="tarjeta_id">
                        <?php $k = array('prompt' => ''); ?>
                        <?php $tarjeta_plan_id = $valor["tarjeta_plan_id"]; ?>
                        <?php $tarjeta_id = Helpers::qryScalar("select tarjeta_id from tarjeta_plan where id = $tarjeta_plan_id"); ?>
                        <?php echo CHtml::listOptions($tarjeta_id, CHtml::listData(Tarjeta::model()->findAll(), "id", "nombre"), $k); ?>
                    </select>
                    <select style="width: 100px;" name="tarjeta_plan_id"
                            data-placeholder="- Plan Tarjeta -" class="plan_id">
                        <?php $p = array('prompt' => ''); ?>
                        <?php echo CHtml::listOptions($tarjeta_plan_id, CHtml::listData(TarjetaPlan::model()->findAll("Tarjeta_id=$tarjeta_id"), "id", "nombre"), $p); ?>
                    </select>
                </div>
                <div class="inner-row">
                    <!--            <input name="numero" class="tarjeta-numero" value="" placeholder=" - Número - "/>-->
                    <input placeholder=" - # Lote - "
                           value="<?php echo $valor["lote"]; ?>" id="lote" name="lote">
                    <input placeholder=" - # Cupón - "
                           value="<?php echo $valor["numero"]; ?>" id="cupon"
                           name="numero">
                </div>
            </div>
        </div>
        <script>
            $detalle = $("#form_<?php echo $valor["id"]; ?>");
            $detalle.find(".tarjeta_id").chosen().change(function () {
                changeTarjeta($("#form_<?php echo $valor["id"]; ?>"));
            });
            $detalle.find(".plan_id").chosen().change(function () {
                setFocus($("#lote"), 200);
                //$("#lote").focus();
            });
        </script>
    <?php
    }

    function traeChequePropioDet($valor) {
        ?>
        <div id="form_<?php echo $valor["id"]; ?>" class="valor tipo-chq-propio"
             style="display: none;">
            <input type="hidden" name="tipo"
                   value="<?php echo DocValor::TIPO_CHEQUE_PROPIO; ?>">

            <div class="row">
                <div class="inner-row">
                    <select style="width: 150px; " class="chequera_id"
                            data-placeholder="- Chequera -" name="chequera_id">
                        <?php $k = array(); ?>
                        <?php echo CHtml::listOptions($valor["chequera_id"], Chequera::listData(true), $k); ?>
                    </select>
                    <script type="text/javascript">
                        $("#form_<?php echo $valor["id"]; ?>").find(".chequera_id").chosen().change(function () {
                            changeChequera($("#form_<?php echo $valor["id"]; ?>"));
                        });
                    </script>
                    <input placeholder=" - Número - "
                           value="<?php echo $valor["numero"]; ?>" class="cheque-numero"
                           name="numero">
                </div>
                <div class="inner-row">
                    <input placeholder=" - Fecha de cobro - "
                           value="<?php echo $valor["fecha"]; ?>"
                           class="cheque-fecha-acreditacion hasDatepicker" name="fecha"
                           id="dp1387032794194" size="10"><img
                        class="ui-datepicker-trigger" src="images/calendar.gif" alt="..."
                        title="...">
                    <input type="hidden" placeholder=" - A la orden de - "
                           value="<?php echo $valor["chq_entregado_a"]; ?>"
                           class="cheque-a_la_orden_de" name="chq_entregado_a">
                </div>
                <div class="inner-row">
                    <input placeholder=" - Detalle - "
                           value="<?php echo $valor["obs"]; ?>" name="obs"
                           class="cheque-obs-div">

                    <div class="cheque-interes-div">
                        <label for="cheque-diferido">
                            Diferido
                            <input type="checkbox" placeholder=" - diferido - " value=""
                                   class="cheque-diferido" name="cheque-diferido">
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $detalle = $("#form_<?php echo $valor["id"]; ?>");
            $detalle.find(".cta_bcria_select").chosen().change(function () {
            });
            $detalle.find(".cheque-fecha, .rec-fecha-acreditacion, .tran-fecha-acreditacion, .cheque-fecha-acreditacion").datepicker();
        </script>
    <?php } ?>
