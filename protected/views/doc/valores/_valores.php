<?php
//probando svn
	$baseUrl = Yii::app()->request->baseUrl;
	$cs = Yii::app()->clientScript;
	$cs->registerScriptFile($baseUrl . "/js/common.js", CClientScript::POS_BEGIN);
?>
<div id="forma-de-pago-div">
	<div class="titulo">
		<span>Formas de pago</span>
	</div>
	<div id="valores">
    <div id="valores-table-div" >
			<table id="valores-retencion-table">
				<tr id="retencion-tr" data-bind="visible:((tipo()  === 8 || tipo() === 13) && muestraRetencion() && Number(totalRetencion() > 0))" style="display:none">
					<td class="valor-nombre-td"><span class="valor-nombre">Retención</span></td>
					<td class="right-align valor-retencion-td">
						<input value="7" aaadata-bind="textInput:retencion" class="valor-retencion">
					</td>
					<td class="right-align valor-importe-td">
						<input data-bind="textInput:totalRetencion" class="valor-importe">
					</td>
					<td class="td_edit_img"><img src="images/edit.jpg"></td>
					<td class="td_delete_img"><img data-bind="click:quitaRetencion" src="images/delete.jpg"></td>
				</tr>
			</table>
			<table id="valores-table">
			</table>
			<div class="total-table">
				<div id="div-total-valores">
					<span class="total-valores">Total valores</span>
					<span id="total-valores" class="total-td total bold right-align">0</span>
				</div>
				<div id="div-total-pagos">
					<span class="total-pagos">Total a pagar</span>
					<span id="total-pagos" class="total-td total bold right-align">0</span>
				</div>
				<div id="div-valores-saldo">
					<span class="">Diferencia</span>
					<span id="valores-saldo" class="total-td total bold right-align">0</span>
				</div>
			</div>
		</div>
	</div>
	<div id="valores-detalle">
		<div class="titulo">
			<span>Detalle de valor</span>
		</div>
		<div id="valores-detalle-content">
			<div id="valores-detalle-items">

			</div>
			<div id="botones-valores">
				<input type="button" id="valor-ok" onclick="return confirmaValor(true);
						return false;" value="Ok"/>
				<input type="button" id="valor-cancel" onclick="return confirmaValor(false);
						return false;" value="Cancel"/>
			</div>
		</div>
	</div>
</div>
<div id='cheques-tercero-dialog'>
	<div id="cheques-tercero-dialog-table-div">
	</div>
</div>

<div id='cupones-dialog'>
	<div id="cupones-dialog-table-div">
	</div>
</div>

<div id='retenciones-dialog'>
	<div id="retenciones-dialog-table-div">
	</div>
</div>
