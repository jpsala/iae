<div id="valores-table-div">
    <table id="valores-table">
        <tbody>
            <tr id="rec_1387033268884">
                <td><input value="" name="valor-id" id="1387033268884" class="valor-id"></td>
                <td class="valor-nombre-td"><span class="valor-nombre">Efectivo</span></td>
                <td class="right-align valor-importe-td"><span class="divclearable"><span class="divclearable"><input value="20" class="valor-importe"><a tabindex="-1" href="javascript:"></a></span><a tabindex="-1" href="javascript:"></a></span></td>
                <td class="td_edit_img"><img src="images/edit.jpg"></td>
                <td class="td_delete_img"><img src="images/delete.jpg"></td></tr>
            <tr id="rec_1387033275885">
                <td><input value="" name="valor-id" id="1387033275885" class="valor-id"></td>
                <td class="valor-nombre-td"><span class="valor-nombre">Cheque</span></td>
                <td class="right-align valor-importe-td"><span class="divclearable"><input value="15" class="valor-importe"><a tabindex="-1" href="javascript:"></a></span></td>
                <td class="td_edit_img"><img src="images/edit.jpg"></td>
                <td class="td_delete_img"><img src="images/delete.jpg"></td></tr>
            <tr id="rec_1387033283975">
                <td><input value="" name="valor-id" id="1387033283975" class="valor-id"></td>
                <td class="valor-nombre-td"><span class="valor-nombre">Tarjeta</span></td>
                <td class="right-align valor-importe-td"><span class="divclearable"><input value="10" class="valor-importe"><a tabindex="-1" href="javascript:"></a></span></td>
                <td class="td_edit_img"><img src="images/edit.jpg"></td>
                <td class="td_delete_img"><img src="images/delete.jpg"></td></tr>
            <tr id="rec_1387033290281">
                <td><input value="" name="valor-id" id="1387033290281" class="valor-id"></td>
                <td class="valor-nombre-td"><span class="valor-nombre">Cheque-propio</span></td>
                <td class="right-align valor-importe-td"><span class="divclearable"><input value="5" class="valor-importe"><a tabindex="-1" href="javascript:"></a></span></td>
                <td class="td_edit_img"><img src="images/edit.jpg"></td>
                <td class="td_delete_img"><img src="images/delete.jpg"></td></tr>
        </tbody></table>
    <div class="total-table">
        <div id="div-total-valores">
            <span class="total-valores">Total valores</span>
            <span class="total-td total bold right-align" id="total-valores">20</span>
        </div>
        <div id="div-total-pagos">
            <span class="total-pagos">Total a pagar</span>
            <span class="total-td total bold right-align" id="total-pagos">20</span>
        </div>
        <div id="div-valores-saldo">
            <span class="">Diferencia</span>
            <span class="total-td total bold right-align" id="valores-saldo">0</span>
        </div>
    </div>
</div>


<div id="valores-detalle">
    <div class="titulo">
        <span>Detalle de valor</span>
    </div>
    <div id="valores-detalle-content">
        <div id="valores-detalle-items">

            <div id="form_1387032810496" class="valor" style="display: none;">
                <input type="hidden" name="tipo" value="1">
                <div class="row">
                    <div class="banco_mas_numero inner-row">
                        <select style="width: 150px; display: none;" class="banco_id chzn-done" data-placeholder="- Banco -" id="sel4FY" name="banco_id">
                            <option value=""></option>
                            <option value="5">1 - Banco prueba</option>
                            <option value="6">007 - Galicia</option>
                            <option value="7">072 - Santader Rio</option>
                            <option value="8">027 - SUPERVILLE</option>
                            <option value="9">017 - Bbva Frances</option>
                            <option value="10">014 - Provincia De Buenos Aires</option>
                            <option value="11">265 - B.N.L.</option>
                            <option value="12">150 - Hsbc</option>
                            <option value="13">285 - Macro</option>
                            <option value="14">010 - LLOYDS BANK LTD</option>
                            <option value="15">259 - Itau</option>
                            <option value="16">016 - CITIBANK</option>
                            <option value="17">113 - EDIF.OLAVARRIA</option>
                            <option value="18">015 - Icbc</option>
                            <option value="19">191 - Credicoop Cooperativo Limitado</option>
                            <option value="20">011 - NACION ARGENTINA</option>
                            <option value="21">083 - PROV.DE CHUBUT</option>
                            <option value="22">029 - CIUDAD DE BS.AS.</option>
                            <option value="23">093 - BANCO DE LA PAMPA</option>
                            <option value="24">043 - SCOTIABANK QUILMES</option>
                            <option value="25">034 - MERCANTIL</option>
                            <option value="26">001 - DEUTSCHBANK</option>
                            <option value="27">005 - HOLANDES</option>
                            <option value="28">006 - SUDAMERIS</option>
                            <option value="29">008 - SHAW SA</option>
                            <option value="30">020 - PCIA DE CORDOBA</option>
                            <option value="31">039 - CAJA DE AHORRO</option>
                            <option value="32">255 - DEL SUQUIA</option>
                            <option value="33">097 - PCIA DE NEUQUEN</option>
                            <option value="34">178 - BANCO DE BALCARCE</option>
                            <option value="35">99 - INDUSTRIAL</option>
                            <option value="36">322 - INDUSTRIAL DE AZUL</option>
                            <option value="37">314 - BISEL</option>
                            <option value="38">229 - BUEN AYRE</option>
                            <option value="39">271 - VELOX</option>
                            <option value="40">281 - LINIERS</option>
                            <option value="41">297 - BANEX</option>
                            <option value="42">326 - MBK</option>
                            <option value="43">330 - SANTA FE</option>
                            <option value="44">255 - DEL SUQUIA</option>
                            <option value="45">914 - Banco Pcia BS AS (PAT B)</option>
                            <option value="46">060 - del tucuman</option>
                            <option value="47">059 - DE ENTRE RIOS</option>
                            <option value="48">285 - makro</option>
                            <option value="49">137 - banco empresario</option>
                            <option value="50">086 - BANCO DE SANTA CRUZ</option>
                            <option value="51">388 - Nuevo Banco Bisel</option>
                            <option value="52">991 - Credicoop Pat.</option>
                            <option value="53">309 - nuevo banco de la rioja</option>
                            <option value="54">299 - banco comafi</option>
                            <option value="55">045 - BANCO SAN JUAN</option>
                            <option value="56">317 - BANCO PATAGONIA</option>
                            <option value="57">268 - BANCO DE TIERRA DEL FUEGO</option>
                            <option value="58">387 - BANCO SUQUIA</option>
                            <option value="59">079 - REGIONAL DE CUYO</option>
                            <option value="60">386 - nuevo banco de entre rios</option>
                            <option value="61">324 - Banco Jujuy</option>
                            <option value="62">389 - BANCO COLUMBIA</option>
                            <option value="63">266 - BNP PARIBAS</option>
                            <option value="64">321 - BANCO SANTIAGO DEL ESTERO</option>
                            <option value="65">303 - Finansur</option>
                            <option value="66">065 - MUNICIPAL DE ROSARIO</option>
                            <option value="67">305 - BANCO JULIO</option>
                            <option value="68">254 - Banco Mariva</option>
                            <option value="69">094 - Banco de Corrientes</option>
                            <option value="70">311 - NUEVO BCO DEL CHACO</option>
                            <option value="71">044 - HIPOTECARIO</option>
                            <option value="72">277 - BANCO SAENZ</option>
                            <option value="11111">11111 - borrar</option>
                            <option value="11112">017 - </option>
                        </select><div id="sel4FY_chzn" class="chzn-container chzn-container-single" style="width: 150px;" title=""><a tabindex="-1" class="chzn-single" href="javascript:void(0)"><span>1 - Banco prueba</span><div><b></b></div></a><div style="left: -9000px; width: 148.2px; top: -1px;" class="chzn-drop"><div class="chzn-search"><input type="text" autocomplete="off" style="width: 132.4px;"></div><ul class="chzn-results"><li style="" class="active-result result-selected" id="sel4FY_chzn_o_1">1 - Banco prueba</li><li style="" class="active-result" id="sel4FY_chzn_o_2">007 - Galicia</li><li style="" class="active-result" id="sel4FY_chzn_o_3">072 - Santader Rio</li><li style="" class="active-result" id="sel4FY_chzn_o_4">027 - SUPERVILLE</li><li style="" class="active-result" id="sel4FY_chzn_o_5">017 - Bbva Frances</li><li style="" class="active-result" id="sel4FY_chzn_o_6">014 - Provincia De Buenos Aires</li><li style="" class="active-result" id="sel4FY_chzn_o_7">265 - B.N.L.</li><li style="" class="active-result" id="sel4FY_chzn_o_8">150 - Hsbc</li><li style="" class="active-result" id="sel4FY_chzn_o_9">285 - Macro</li><li style="" class="active-result" id="sel4FY_chzn_o_10">010 - LLOYDS BANK LTD</li><li style="" class="active-result" id="sel4FY_chzn_o_11">259 - Itau</li><li style="" class="active-result" id="sel4FY_chzn_o_12">016 - CITIBANK</li><li style="" class="active-result" id="sel4FY_chzn_o_13">113 - EDIF.OLAVARRIA</li><li style="" class="active-result" id="sel4FY_chzn_o_14">015 - Icbc</li><li style="" class="active-result" id="sel4FY_chzn_o_15">191 - Credicoop Cooperativo Limitado</li><li style="" class="active-result" id="sel4FY_chzn_o_16">011 - NACION ARGENTINA</li><li style="" class="active-result" id="sel4FY_chzn_o_17">083 - PROV.DE CHUBUT</li><li style="" class="active-result" id="sel4FY_chzn_o_18">029 - CIUDAD DE BS.AS.</li><li style="" class="active-result" id="sel4FY_chzn_o_19">093 - BANCO DE LA PAMPA</li><li style="" class="active-result" id="sel4FY_chzn_o_20">043 - SCOTIABANK QUILMES</li><li style="" class="active-result" id="sel4FY_chzn_o_21">034 - MERCANTIL</li><li style="" class="active-result" id="sel4FY_chzn_o_22">001 - DEUTSCHBANK</li><li style="" class="active-result" id="sel4FY_chzn_o_23">005 - HOLANDES</li><li style="" class="active-result" id="sel4FY_chzn_o_24">006 - SUDAMERIS</li><li style="" class="active-result" id="sel4FY_chzn_o_25">008 - SHAW SA</li><li style="" class="active-result" id="sel4FY_chzn_o_26">020 - PCIA DE CORDOBA</li><li style="" class="active-result" id="sel4FY_chzn_o_27">039 - CAJA DE AHORRO</li><li style="" class="active-result" id="sel4FY_chzn_o_28">255 - DEL SUQUIA</li><li style="" class="active-result" id="sel4FY_chzn_o_29">097 - PCIA DE NEUQUEN</li><li style="" class="active-result" id="sel4FY_chzn_o_30">178 - BANCO DE BALCARCE</li><li style="" class="active-result" id="sel4FY_chzn_o_31">99 - INDUSTRIAL</li><li style="" class="active-result" id="sel4FY_chzn_o_32">322 - INDUSTRIAL DE AZUL</li><li style="" class="active-result" id="sel4FY_chzn_o_33">314 - BISEL</li><li style="" class="active-result" id="sel4FY_chzn_o_34">229 - BUEN AYRE</li><li style="" class="active-result" id="sel4FY_chzn_o_35">271 - VELOX</li><li style="" class="active-result" id="sel4FY_chzn_o_36">281 - LINIERS</li><li style="" class="active-result" id="sel4FY_chzn_o_37">297 - BANEX</li><li style="" class="active-result" id="sel4FY_chzn_o_38">326 - MBK</li><li style="" class="active-result" id="sel4FY_chzn_o_39">330 - SANTA FE</li><li style="" class="active-result" id="sel4FY_chzn_o_40">255 - DEL SUQUIA</li><li style="" class="active-result" id="sel4FY_chzn_o_41">914 - Banco Pcia BS AS (PAT B)</li><li style="" class="active-result" id="sel4FY_chzn_o_42">060 - del tucuman</li><li style="" class="active-result" id="sel4FY_chzn_o_43">059 - DE ENTRE RIOS</li><li style="" class="active-result" id="sel4FY_chzn_o_44">285 - makro</li><li style="" class="active-result" id="sel4FY_chzn_o_45">137 - banco empresario</li><li style="" class="active-result" id="sel4FY_chzn_o_46">086 - BANCO DE SANTA CRUZ</li><li style="" class="active-result" id="sel4FY_chzn_o_47">388 - Nuevo Banco Bisel</li><li style="" class="active-result" id="sel4FY_chzn_o_48">991 - Credicoop Pat.</li><li style="" class="active-result" id="sel4FY_chzn_o_49">309 - nuevo banco de la rioja</li><li style="" class="active-result" id="sel4FY_chzn_o_50">299 - banco comafi</li><li style="" class="active-result" id="sel4FY_chzn_o_51">045 - BANCO SAN JUAN</li><li style="" class="active-result" id="sel4FY_chzn_o_52">317 - BANCO PATAGONIA</li><li style="" class="active-result" id="sel4FY_chzn_o_53">268 - BANCO DE TIERRA DEL FUEGO</li><li style="" class="active-result" id="sel4FY_chzn_o_54">387 - BANCO SUQUIA</li><li style="" class="active-result" id="sel4FY_chzn_o_55">079 - REGIONAL DE CUYO</li><li style="" class="active-result" id="sel4FY_chzn_o_56">386 - nuevo banco de entre rios</li><li style="" class="active-result" id="sel4FY_chzn_o_57">324 - Banco Jujuy</li><li style="" class="active-result" id="sel4FY_chzn_o_58">389 - BANCO COLUMBIA</li><li style="" class="active-result" id="sel4FY_chzn_o_59">266 - BNP PARIBAS</li><li style="" class="active-result" id="sel4FY_chzn_o_60">321 - BANCO SANTIAGO DEL ESTERO</li><li style="" class="active-result" id="sel4FY_chzn_o_61">303 - Finansur</li><li style="" class="active-result" id="sel4FY_chzn_o_62">065 - MUNICIPAL DE ROSARIO</li><li style="" class="active-result" id="sel4FY_chzn_o_63">305 - BANCO JULIO</li><li style="" class="active-result" id="sel4FY_chzn_o_64">254 - Banco Mariva</li><li style="" class="active-result" id="sel4FY_chzn_o_65">094 - Banco de Corrientes</li><li style="" class="active-result" id="sel4FY_chzn_o_66">311 - NUEVO BCO DEL CHACO</li><li style="" class="active-result" id="sel4FY_chzn_o_67">044 - HIPOTECARIO</li><li style="" class="active-result" id="sel4FY_chzn_o_68">277 - BANCO SAENZ</li><li style="" class="active-result" id="sel4FY_chzn_o_69">11111 - borrar</li><li style="" class="active-result" id="sel4FY_chzn_o_70">017 - </li></ul></div></div>
                        <input placeholder=" - Número - " value="" class="cheque-numero" name="numero">
                    </div>
                    <div class="fecha_mas_origen inner-row">
                        <input placeholder=" - Fecha Acreditación - " onchange="chequeFechaAcreditacionChange($(this));" value="14/12/2013" class="cheque-fecha-acreditacion hasDatepicker" name="fecha" id="dp1387032794193" size="10"><img class="ui-datepicker-trigger" src="images/calendar.gif" alt="..." title="...">
                        <input placeholder=" - Origen - " value="" class="cheque-origen" name="chq_origen">
                    </div>
                    <div class="cuit_mas_interes inner-row">
                        <input placeholder=" - CUIT Endosante - " value="" class="cheque-cuit" name="chq_cuit_endosante">
                        <div id="div-cheque-genera-interes">
                            <label for="cheque-genera-interes">
                                Genera Interes
                                <input type="checkbox" placeholder="Genera Interes" onchange="chequeGeneraInteresChange($(this));" value="" class="cheque-genera-interes" name="cheque_genera_interes">
                            </label>
                        </div>
                    </div>
                    <script type="text/javascript">
                            if (doc.matricula) {
                                $("#form_1387032810496").find(".cheque-origen").val(doc.matricula);
                            }
                            if (!doc.tipo) {
                                $("#div-cheque-genera-interes").hide();
                            }
                    </script>
                </div>
            </div>




            <div id="form_1387032863554" class="valor" style="display: none;">
                <input type="hidden" name="tipo" value="2">
                <div class="row">
                    <div class="inner-row">
                        <select style="width: 150px; display: none;" name="tarjeta_id" data-placeholder="- Tarjeta -" class="tarjeta_id chzn-done" id="selWWJ">
                            <option value=""></option>
                            <option value="3">Visa</option>
                            <option value="4">Amex</option>
                            <option value="5">Nativa</option>
                        </select><div id="selWWJ_chzn" class="chzn-container chzn-container-single" style="width: 150px;" title=""><a tabindex="-1" class="chzn-single" href="javascript:void(0)"><span>Visa</span><div><b></b></div></a><div style="left: -9000px; width: 148.2px; top: -1px;" class="chzn-drop"><div class="chzn-search"><input type="text" autocomplete="off" style="width: 132.4px;"></div><ul class="chzn-results"><li style="" class="active-result result-selected" id="selWWJ_chzn_o_1">Visa</li><li style="" class="active-result" id="selWWJ_chzn_o_2">Amex</li><li style="" class="active-result" id="selWWJ_chzn_o_3">Nativa</li></ul></div></div>
                        <select style="width: 100px; display: none;" name="tarjeta_plan_id" data-placeholder="- Plan Tarjeta -" class="plan_id chzn-done" id="sel3W8"><option value=""></option>
                            <option value="6">Crédito</option>
                            <option value="9">Débito</option>
                        </select><div id="sel3W8_chzn" class="chzn-container chzn-container-single" style="width: 100px;" title=""><a tabindex="-1" class="chzn-single" href="javascript:void(0)"><span>Crédito</span><div><b></b></div></a><div style="left: -9000px; width: 98.2px; top: -1px;" class="chzn-drop"><div class="chzn-search"><input type="text" autocomplete="off" style="width: 82.4px;"></div><ul class="chzn-results"><li style="" class="active-result result-selected" id="sel3W8_chzn_o_1">Crédito</li><li style="" class="active-result" id="sel3W8_chzn_o_2">Débito</li></ul></div></div>
                    </div>
                    <div class="inner-row">
                <!--            <input name="numero" class="tarjeta-numero" value="" placeholder=" - Número - "/>-->
                        <input placeholder=" - # Lote - " value="" id="lote" name="lote">
                        <input placeholder=" - # Cupón - " value="" id="cupon" name="numero">
                    </div>
                </div>
            </div>





            <div id="form_1387032880423" class="valor tipo-chq-propio" style="display: none;">
                <input type="hidden" name="tipo" value="3">
                <div class="row">
                    <div class="inner-row">
                        <select style="width: 150px; display: none;" class="chequera_id chzn-done" data-placeholder="- Chequera -" name="chequera_id" id="selIZF">
                            <option value=""></option>
                            <option value="4">Galicia 2</option>
                            <option value="7">Nacion 3</option>
                            <option value="8">Nacion 4</option>
                            <option value="9">Nacion 5</option>
                            <option value="1">Provincia 1</option>
                        </select><div id="selIZF_chzn" class="chzn-container chzn-container-single" style="width: 150px;" title=""><a tabindex="-1" class="chzn-single" href="javascript:void(0)"><span>Galicia 2</span><div><b></b></div></a><div style="left: -9000px; width: 148.2px; top: 22px;" class="chzn-drop"><div class="chzn-search"><input type="text" autocomplete="off" style="width: 132.4px;"></div><ul class="chzn-results"><li style="" class="active-result result-selected" id="selIZF_chzn_o_1">Galicia 2</li><li style="" class="active-result" id="selIZF_chzn_o_2">Nacion 3</li><li style="" class="active-result" id="selIZF_chzn_o_3">Nacion 4</li><li style="" class="active-result" id="selIZF_chzn_o_4">Nacion 5</li><li style="" class="active-result" id="selIZF_chzn_o_5">Provincia 1</li></ul></div></div>
                        <script type="text/javascript">
                            $("#form_1387032880423").find(".chequera_id").chosen().change(function() {
                                changeChequera($("#form_1387032880423"));
                            });
                            ;
                        </script>
                        <input placeholder=" - Número - " value="" class="cheque-numero" name="numero">
                    </div>
                    <div class="inner-row">
                        <input placeholder=" - Fecha de cobro - " value="14/12/2013" class="cheque-fecha-acreditacion hasDatepicker" name="fecha" id="dp1387032794194" size="10"><img class="ui-datepicker-trigger" src="images/calendar.gif" alt="..." title="...">
                        <input type="hidden" placeholder=" - A la orden de - " value="-" class="cheque-a_la_orden_de" name="chq_entregado_a">
                    </div>
                    <div class="inner-row">
                        <input placeholder=" - Detalle - " value="" name="obs" class="cheque-obs-div">
                        <div class="cheque-interes-div">
                            <label for="cheque-diferido">
                                Diferido
                                <input type="checkbox" placeholder=" - diferido - " value="" class="cheque-diferido" name="cheque-diferido">
                            </label>
                        </div>
                    </div>
                </div>
            </div>




            <div id="form_1387032890148" class="valor" style="display: none;">
            <!--    <input type="hidden" value="" name="rec-tipo-valor"/>-->
                <input type="hidden" name="tipo" value="4">
                <div class="row">
                    <select style="width: 150px; display: none;" class="select chzn-done" data-placeholder="- Cta. Bancaria -" name="Destino_id" id="selRJH">
                        <option value=""></option>
                        <option value="4">Banco Provincia - 6189-6983/6</option>
                        <option value="6">Galicia - 622/0 340/1</option>
                        <option value="7">Nacion - 35058320/00</option>
                    </select><div id="selRJH_chzn" class="chzn-container chzn-container-single" style="width: 150px;" title=""><a tabindex="-1" class="chzn-single chzn-default" href="javascript:void(0)"><span>- Cta. Bancaria -</span><div><b></b></div></a><div style="left: -9000px; width: 148.2px; top: 22px;" class="chzn-drop"><div class="chzn-search"><input type="text" autocomplete="off" style="width: 132.4px;"></div><ul class="chzn-results"><li style="" class="active-result" id="selRJH_chzn_o_1">Banco Provincia - 6189-6983/6</li><li style="" class="active-result" id="selRJH_chzn_o_2">Galicia - 622/0 340/1</li><li style="" class="active-result" id="selRJH_chzn_o_3">Nacion - 35058320/00</li></ul></div></div>
                    <div>
                        <input placeholder=" - Fecha Acreditación - " value="14/12/2013" class="fecha hasDatepicker" name="fecha" id="dp1387032794195" size="10"><img class="ui-datepicker-trigger" src="images/calendar.gif" alt="..." title="...">
                        <input placeholder=" - Número - " value="" class="numero" name="numero">
                        <input placeholder=" - Observaciones - " value="" class="obs" name="obs">
                    </div>
                </div>
            </div>
        </div>
        <div id="botones-valores" style="display: none;">
            <input type="button" value="Ok" onclick="return confirmaValor(true);
                                return false;" id="valor-ok" class="ui-button ui-widget ui-state-default ui-corner-all" role="button" aria-disabled="false">
            <input type="button" value="Cancel" onclick="return confirmaValor(false);
                                return false;" id="valor-cancel" class="ui-button ui-widget ui-state-default ui-corner-all" role="button" aria-disabled="false">
        </div>    
    </div>
</div>