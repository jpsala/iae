<style text="text/css">
    #forma-de-pago-div{position: relative; /*min-height: 100%*/}
    #forma-de-pago-div {border-radius: 5px; background-color: #f9fafc; width: 100%;  border: #eff3fc solid 1px;;position: relative;height: 380px}
    #forma-de-pago-div .nuevo-valor{text-align: left}
    #forma-de-pago-div .valor-id{ width: 20px}
    #forma-de-pago-div #valores{position: relative; height: 353px}
    #forma-de-pago-div #valores table{ width: 99%;margin: auto; border-radius: 5px; padding: 0px; }
    #forma-de-pago-div th, #forma-de-pago-div td, #forma-de-pago-div caption {  padding: 4px 3px 4px 3px; }
    #forma-de-pago-div #valores table tr{background: #EF3FC}
    #forma-de-pago-div #valores table tr .importe{width: 60px; text-align: right}
    #forma-de-pago-div #valores .total-table{bottom: 0;padding: 3px;position: absolute;width: 98%;}
    #forma-de-pago-div #valores .total-table .total{float:right}
    #forma-de-pago-div #valores .total-table .total-valores{clear: both}
    #forma-de-pago-div #valores .total-table .total-pagos{}
    #forma-de-pago-div #valores-detalle{ position: absolute; bottom: 0px; left:0; border-top: #EFF3FC solid 1px; min-height: 220px; width: 100%}
    #forma-de-pago-div #valores-detalle .valor{ padding: 0 1px 0px 1px; min-height: 100% }
    #forma-de-pago-div #valores-detalle .valor{ min-height: 100% }
    #forma-de-pago-div #valores-detalle .valor *{ font-size: 94%;}
    #forma-de-pago-div .inner-row{margin-bottom: 3px; line-height:8px}
    #forma-de-pago-div #valores-detalle .valor label{ width: 50px; float: left}
    #forma-de-pago-div #valores-detalle .valor .botones-valores{ padding: 2px ; position: absolute; right: 3px; bottom: 3px}
    #forma-de-pago-div .banco_mas_numero .banco_id{vertical-align: top;}
    #forma-de-pago-div .cheque-numero{vertical-align: top}
    #forma-de-pago-div .cheque-numero, #forma-de-pago-div .cuit_mas_interes, #forma-de-pago-div .fecha_mas_origen{margin-bottom: 3px}
    #forma-de-pago-div .td_delete_img,#forma-de-pago-div .td_edit_img{width: 10px; cursor: pointer}
    #forma-de-pago-div .td_delete_img img{background-color: transparent;}
    #forma-de-pago-div .td_edit_img img{background-color: transparent;}
    #forma-de-pago-div .banco_id{width: 100px}
    #forma-de-pago-div #valores-table-div{  max-height: 126px;  overflow: auto;}
    #forma-de-pago-div .valor {display:none}
    #cheque-form-template{display: none}
    #div-cheque-genera-interes{display: inline-block;
                               max-width: 53px;
                               vertical-align: top;}
    #div-cheque-genera-interes label{display: inline; width:300px !important}
    .valor-tipo{width: 50px}
    .valor-importe{width: 80px; text-align: right}

    .cheque-numero, .tarjeta-numero {
        display: inline-block;
        margin-bottom: 5px;
        margin-left: 4px;
        vertical-align: text-bottom;
        width: 100px;
    }
    .chzn-container-single .chzn-single {
        height: 21px;
        line-height: 21px;
    } 
    .chzn-container-single .chzn-single div b {
        background: url("images/chosen-sprite.png") no-repeat scroll 0 -2px transparent;
    }
    #botones-valores{display:none;bottom: 69px;position: absolute;right: 1px;text-align: right;}
    #botones-valores button .ui-button-text{padding: 0.1em 1em !important}
    .cheque-fecha-acreditacion {
        width: 125px;
    }
    .cheque-fecha {
        display: inline-block;
        width: 92px;
    }
    .cheque-origen{width: 140px;margin-left: 5px}
    .cheque-cuit {display: inline-block;width: 131px;}
    .entregado-a {display: inline-block;width: 187px;}
    .cheque-genera-interes {vertical-align: middle;}
    .cheque-interes-div{    display: inline-block;
                            margin-left: 4px;
                            vertical-align: sub;
                            width: 80px;}
    .cheque-detalle{display: inline-block}
    .cheque-interes-div label{width: 80px !important}
    .cheque-interes-div label input.cheque-diferido{vertical-align: middle !important}
    .tipo-chq-propio .cheque-detalle{width:278px !important}
    .rectran-numero{display:block}
    .rectran-fecha-acreditacion hasDatepicker{display:block}
    .rectran-obs{width:90%}
    #cheques-tercero{overflow: auto}
		.valor-retencion{width:40px}
</style>