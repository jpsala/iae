<link rel="stylesheet" type="text/css" href="js/tablesorter/themes/blue/style.css"/>
<script src="js/tablesorter/jquery.tablesorter.js"></script>
<script type="text/javascript">

  var nuevoValorTemplate = '<tr id="rec_%id%"><td><input class="valor-id" id="%id%" name="valor-id" value=""/></td><td class="valor-nombre-td"></td><td class="right-align valor-importe-td"></td><td class="td_edit_img"><img src="images/edit.jpg"/></td><td class="td_delete_img"><img src="images/delete.jpg"/></td></tr>';
  var vieneDeValor, agregandoValor;
  var cajaCerrada = false;
  var valorAnt;
  var valores = new Object();
  var interesChequeDiario = 0.001;
  var estaEnValores;

  valores.totalValores = 0;

  valores.refresh = function () {
    refresh(false);
  };

  valores.limpia = function () {
    $("#forma-de-pago-div #valores #valores-table tbody").html("");
    $("#valores #valores-table-div .total-table .total-td").html("0");
    valores.totalValores = 0;
  };

  valores.toggleEnabled = function (on) {
    if (on) {
      $("#valores *").removeAttr('disabled');
      $('.valor-importe[bloqueado="1"]').attr('disabled', "disabled");
      doc.disabled = false;
    } else {
      $("#valores *").attr('disabled', "disabled");
      doc.disabled = true;
    }
    return false;
  }

  valores.init = function () {

    $("#valores-table-div").find(".valor").each(function (e, o) {
      $(o).appendTo("#valores-detalle-items");
    });

    $(".valor-importe").autoNumeric("init", {mDec: 2, aSep: ''});

    $("#forma-de-pago-div").on("click", ".td_edit_img", function (e) {
      id = $(this).parent().find(".valor-id").attr("id");
      var tipo_valor = $(this).parent().find(".valor-nombre").html();
      if (id.length > 0) {
        if (tipo_valor === "Cheque" && doc.esSalida) {
          $("#cheques-tercero-dialog").dialog("open");
        } else
        if (tipo_valor === "Tarjeta" && doc.esSalida) {
          $("#cupones-dialog").dialog("open");
        } else
        {
          preparaEdicionValor("form_" + id, tipo_valor);
        }
      }
    });

    $("#valores-table").on("mouseover", "tr", function (e) {
      id = $(this).find(".valor-id").attr("id");
      if (id.length > 0) {
        muestraDetalle(e, id);
      }
    }).on("mouseout", "tr", function (e) {
      id = $(this).find(".valor-id").attr("id");
      if (id.length > 0) {
        muestraDetalle(e, id);
      }
    })

    $("#forma-de-pago-div").on("click", ".td_delete_img", function (e) {
      id = $(this).parent().find(".valor-id").attr("id");
      var valor_tipo = $(this).parent().find(".valor-nombre").html();
      borraDetalleValor(id, e, valor_tipo);
    });

    $("#valores-detalle").on("focus", "input", function () {
      doc.toggleEnabled(false);
      valores.toggleEnabled(false);
    });

    $("#valores-table").on("change", ".valor-id", function () {
      valorTipoChange($(this));
    });

    $("#valores-table").on("change", ".valor-importe", function () {
      valorImporteChange($(this));
    });

    $("#valores-table").on("keypress", ".valor-importe", function (e) {
      if (e.which == 13) {
        valorImportePressEnter($(this));
      }
    });

    $("#valores-table").on("keypress", ".valor-id", function (e) {
//            if (e.which === 13) {
//                e.which = null;
//                e.preventDefault();
//                valorTipoChange($(this));
//                return;
//            }
    });

    $(".botones-valores button").button();

    refresh(true);

    traeCheques();

    traeCupones();

    traeRetenciones();

  };

  valores.getData = function () {
    var data = "", index = 0;
    cheques = "";
    $(".cheque-check:checked").each(function () {
      $tr = $(this).closest("tr");
      cheques += $tr.attr("cheque_id") + ",";
    });
    cupones = "";
    $(".cupon-check:checked").each(function () {
      $tr = $(this).closest("tr");
      cupones += $tr.attr("cupon_id") + ",";
    });
    retenciones = "";
    $(".retencion-check:checked").each(function () {
      $tr = $(this).closest("tr");
      retenciones += $tr.attr("retencion_id") + ",";
    });

    $("#valores-table").find("tr").each(function (i, e) {
      visible = $(this).is(":visible");
      if (visible) {
        valor_nombre = $(e).find(".valor-nombre").html();
        if (valor_nombre == "Cheque" && cheques !== "") {
          data += "&valores[" + index + "][" + valor_nombre + "][cheques]=" + cheques;
        }
        if (valor_nombre == "Tarjeta" && cupones !== "") {
          data += "&valores[" + index + "][" + valor_nombre + "][cupones]=" + cupones;
        }
        if (valor_nombre == "Retención" && retenciones !== "") {
          data += "&valores[" + index + "][" + valor_nombre + "][retenciones]=" + retenciones;
        }
        data += "&valores[" + index + "][" + valor_nombre + "][importe]=";
        data += $(e).find(".valor-importe").val();
        if (!visible) {
          data += "&valores[" + index + "][" + valor_nombre + "][borrado]=1";
        }
        valor_id = $(e).find(".valor-id").attr("id");
        $("#form_" + valor_id + " *").each(function (i, e) { //todo lo que tiene el attr "nombre"
          input_nombre = $(e).attr("name");
          if (input_nombre) {
            data += "&valores[" + index + "][" + valor_nombre + "][" + input_nombre + "]=";
            data += $(e).val();
          }
        });
        data += $(e).val();
      }
      index++;
    });
    if (!doc.comprob || doc.comprob.nombre !== 'Entrada de caja') {
      if (typeof (vm) !== "undefined" && vm.totalRetencion() && Number(vm.totalRetencion())) {
        if (vm.totalRetencion() && Number(vm.totalRetencion()) && vm.muestraRetencion()) {
          data += "&valores[" + index + "][retencion][importe]=" + vm.totalRetencion();
          data += "&valores[" + index + "][retencion][tipo]=7";
          if (vm.cuit && vm.cuit()) {
            data += "&valores[" + index + "][retencion][cuit]=" + vm.cuit();
          }
          index++;
        }
      }
    }
    return data;
  };

  function refresh(estaEnValores) {
    actualizaTotalValores();
    $("#div-total-pagos #total-pagos").html(round(doc.total, 2));
    $("#valores-saldo").html(round(doc.total - valores.totalValores, 2));
    chkValores(estaEnValores);
  }

  function chkValores(estaEnValores) {
    var total = doc.pagoACuenta ? doc.pagoACuenta : 0;
    if (typeof (vm) !== "undefined") {
      total += vm.totalRetencion();
    }
    $("#forma-de-pago-div .valor-id:visible").each(function () {
      importe = Number($(this).parent().parent().find(".valor-importe").val());
      importe = (importe === 0) ? "" : importe;
      if (importe) {
        total += Number(importe);
      } else {
        id = $(this).attr("id");
        if (id.length) {
          borraDetalleValor(id);
        }
        $(this).parents("tr").remove();
      }
    });

    if ((doc.total + doc.pagoACuenta > total) && !agregandoValor) {
      agregaValor(estaEnValores);
    }
  }

  function actualizaTotalValores() {
    valores.totalValores = 0;
    $(".valor-importe:visible").each(function () {
      valores.totalValores += Number($(this).val());
    });
    valores.totalValores = Number(round(valores.totalValores, 2));
    $("#total-valores").html(round(valores.totalValores, 2));
    if (typeof (vm) !== "undefined") {
      console.log("CHG");
      vm.valoresTotalesChange();
    }
  }

  function calculaResto() {
    var resto = round(doc.total - valores.totalValores, 2);
    return resto;
  }

  function valorTipoChange(e) {
    var tipo = Number(e.val());
    var parent = e.parents("tr");
    var importe_td = parent.find(".valor-importe-td");
    var nombre_td = parent.find(".valor-nombre-td");
    var resto = calculaResto();
    var id = "form_" + e.attr("id");
    var $det = $("#" + id);
    if ($det.length > 0) {
      e.val(valorAnt);
      alert("No puede modificar la forma de pago, borre primero el registro anterior");
      return;
    }
    valorAnt = tipo;
    if (tipo === 1) {
      importe_td.html("<input class='valor-importe' value='%importe%'/>".replace("%importe%", resto));
      nombre_td.html("<span class='valor-nombre'>Efectivo</span>");
      $newInput = importe_td.find("input");
      setFocusAndSelect($newInput, 200);
      $newInput.clearinput();

      vieneDeValor = e;
    } else if (tipo === 2) {
      if (doc.esSalida) {
        var $existePagoConCheque = $("#valores-table").find(".valor-nombre-td:contains('Cheque')");
        if ($existePagoConCheque.length > 0) {
          e.val("");
        } else {
          importe_td.html("<input class='valor-importe' value='%importe%'/>".replace("%importe%", resto));
          vieneDeValor = e;
          nombre_td.html("<span class='valor-nombre'>Cheque</span>");
        }
        $("#cheques-tercero-dialog").dialog("open");
        return;
      } else {
        importe_td.html("<input class='valor-importe' value='%importe%'/>".replace("%importe%", resto));
        vieneDeValor = e;
        nombre_td.html("<span class='valor-nombre'>Cheque</span>");
        agregaDetalle(tipo, id);
      }
    } else if (tipo === 3) {

      if (doc.esSalida) {

        var $existePagoConTarjeta = $("#valores-table").find(".valor-nombre-td:contains('Tarjeta')");
        if ($existePagoConTarjeta.length > 0) {
          e.val("");
        } else {
          importe_td.html("<input class='valor-importe' value='%importe%'/>".replace("%importe%", resto));
          vieneDeValor = e;
          nombre_td.html("<span class='valor-nombre'>Tarjeta</span>");
        }
        $("#cupones-dialog").dialog("open");

        return;
      } else {
        importe_td.html("<input class='valor-importe' value='%importe%'/>".replace("%importe%", resto));
        nombre_td.html("<span class='valor-nombre'>Tarjeta</span>");
        vieneDeValor = e;
        agregaDetalle(tipo, id);
      }
    } else if (tipo === 4) {
      importe_td.html("<input class='valor-importe' value='%importe%'/>".replace("%importe%", resto));
      nombre_td.html("<span class='valor-nombre'>Cheque-propio</span>");
      vieneDeValor = e;
      agregaDetalle(tipo, id);
    } else if (tipo === 5 || tipo === 6) {
      importe_td.html("<input class='valor-importe' value='%importe%'/>".replace("%importe%", resto));
      var rectranNombre = (tipo === 5) ? "Rec. Bancaria" : "Transferencia";
      nombre_td.html("<span class='valor-nombre'>" + rectranNombre + "</span>");
      vieneDeValor = e;
      agregaDetalle(tipo, id);
    } else if (tipo === 7) {
      if (doc.comprob && doc.comprob.nombre === 'Entrada de caja') {
        var $existePagoConRetencion = $("#valores-table").find(".valor-nombre-td:contains('Retención')");
        if ($existePagoConRetencion.length > 0) {
          e.val("");
        } else {
          importe_td.html("<input class='valor-importe' value='%importe%'/>".replace("%importe%", resto));
          vieneDeValor = e;
          nombre_td.html("<span class='valor-nombre'>Retención</span>");
        }
        $("#retenciones-dialog").dialog("open");
        return;
      } else {
        importe_td.html("<input class='valor-importe' value='%importe%'/>".replace("%importe%", resto));
        nombre_td.html("<span class='valor-nombre'>Retención</span>");
        vieneDeValor = e;
        console.log('asdfasdf', tipo,id);
        agregaDetalle(tipo, id);
      }
    } else {
      console.log('xxasdfasdf', tipo,id);
      vieneDeValor = null;
      importe_td.html("");
      nombre_td.html("");
    }
    importe_td.find("input").change();
    importe_td.find("input").autoNumeric("init", {mDec: 2, aSep: ''});
    importe_td.find("input").clearinput();
    actualizaTotalValores();
    if (typeof bindHotkeys === 'function') {
      bindHotkeys();
    }

  }

  function agregaDetalle(tipo_valor, id) {
    $.ajax({
      type: "GET",
      data: {tipo_valor: tipo_valor, id: id},
      url: "<?php echo $this->createUrl("doc/valoresTraeValorForm"); ?>",
      success: function (data) {
        $detalle = $("#valores-detalle-items").append(data).find(".valor").last();
        initValores(tipo_valor, $detalle);
        preparaEdicionValor(id, tipo_valor, $detalle);
      },
      error: function (data, status) {
      }
    });
  }

  function initValores(tipo_valor, $detalle) {
    $("#botones-valores button").button();
    if (tipo_valor == 2) {
      $detalle.find(".banco_id").chosen().change(function () {
        setFocus($detalle.find(".cheque-numero"), 100);
      });
      $detalle.find(".cheque-fecha-acreditacion").datepicker({
      });
      setTimeout(function () {
        $detalle.find(".chzn-container").mousedown();
      }, 200, $detalle);
    } else if (tipo_valor == 3) {
      $detalle.find(".tarjeta_id").chosen().change(function () {
        changeTarjeta($detalle);
      });
      $detalle.find(".plan_id").chosen().change(function () {
        setFocus($("#lote"), 200);
        //$("#lote").focus();
      });
      setTimeout(function () {
        $detalle.find(".chzn-container").first().mousedown();
      }, 200, $detalle);
    } else if (tipo_valor == 4) {
      $detalle.find(".cta_bcria_select").chosen().change(function () {
      });
      setTimeout(function () {
        $detalle.parent().find(".chzn-container").mousedown();
      }, 300, $detalle);
      $detalle.find(".cheque-fecha, .rec-fecha-acreditacion, .tran-fecha-acreditacion, .cheque-fecha-acreditacion").datepicker();
    } else if ((tipo_valor === 5) || (tipo_valor === 6)) {
      $detalle.find(".fecha").datepicker({
        onSelect: function () {
          setFocus($detalle.find(".numero"), 100);
        }
      });
      $detalle.find(".select").chosen().change(function () {
        $detalle.find(".fecha").focus();
      });
      setTimeout(function () {
        $detalle.find(".chzn-container").mousedown();
      }, 200, $detalle);
    }
  }

  function valorImporteChange(e) {
    var $tr = e.closest("tr");
    var valor_tipo = $tr.find(".valor-nombre").html();
    var valor_id = $tr.find(".valor-id").attr("id");



    e.val(round(e.val()));

    if (valor_tipo === "Cheque") {
      console.log('function valorImporteChange interesCheque( form_', valor_id, e.val());

      interesCheque("form_" + valor_id, e.val());
    }

    actualizaTotalValores();
    console.log(doc.total, valores.totalValores);
    if (doc.total < valores.totalValores) {
      if ($("#pago-a-cuenta-detalle").length > 0) {
        if ($("#pago-a-cuenta-detalle").val() === "") {
          $("#pago-a-cuenta-detalle").val("Pago a cuenta");
        }
        alert("Total a pagar es mayor que el total de valores, se genera un pago a cuenta por la diferencia");
        doc.pagoACuenta = round(valores.totalValores - doc.totalPagos, 2);
        $("#pago-a-cuenta-importe").val(doc.pagoACuenta);
        $("#pago-a-cuenta-importe").focus().change();
      } else {
        dif = doc.total - valores.totalValores;
        alert("Total a pagar es mayor que el total de valores, se modifica el total del documento. Total:" + doc.totalPagos + " Valores:" + valores.totalValores + " Diferencia:" + dif);
        //@DANGER quité la línea de abajo para salida de caja, andará?
//        $("#importe").focus().val(valores.totalValores).change();
      }
      actualizaTotalValores();
      doc.muestraTotales();
      setFocus($(e));
    } else if (doc.total === valores.totalValores) {
      // console.log("doc.total === valores.totalValores");
      setFocus($("#ok"));
//    } else if (doc.total > valores.totalValores) {
    }

    doc.muestraTotales();

    refresh(true);

  }

  function valorImportePressEnter($this) {
    if (doc.total == valores.totalValores) {
      setFocus($("#ok"));
    }
  }

  function agregaValor(estaEnValores) {
    if (agregandoValor) {
      return;
    }
    var $newInput, tmp, $valoresTable, id = new Date().getTime();
    agregandoValor = true;
    refresh(true);
    if (doc.total > valores.totalValores) {
      tmp = nuevoValorTemplate;
      tmp = tmp.replace("%importe%", "0");
      tmp = tmp.replace(/%id%/g, id);
      $valoresTable = $("#valores #valores-table");
      $valoresTable.append(tmp);
      $newInput = $valoresTable.find("#" + id);
      if (typeof bindHotkeys === 'function') {
        bindHotkeys($newInput);
      }
      if (estaEnValores) {
        setFocus($newInput);
      }
    }
    agregandoValor = false;

    return $newInput;
  }

  function preparaEdicionValor(id, tipo_valor, $detalle) {
    if (tipo_valor == 2 && (doc.esSalida)) {
      return;
    }
    ;
    if (!doc.disabled) {
      $("#botones-valores").show();
      //actualizaTotalValores();
      $("#" + id).show();
      doc.toggleEnabled(false);
      valores.toggleEnabled(false);
    } else {
      e.preventDefault();
      $("#banco").focus();
      return false;
    }
  }

  function borraDetalleValor(id, e, valor_tipo) {
    if (valor_tipo && valor_tipo == "Cheque" && doc.esSalida) {
      $("#form_" + id).remove();
      $("#" + id).parent().parent().remove();
    } else if (!doc.disabled) {
      $("#form_" + id).hide();
      $("#" + id).parent().parent().hide().find(".valor-importe").val("").focus().change();
//      refresh(true);
    } else {
      if (e)
        e.preventDefault();
      $("#banco").focus();
      return false;
    }
    refresh(true);
  }

  function confirmaValor() {
    doc.toggleEnabled(true);
    valores.toggleEnabled(true);
    $(".valor").css("display", "none");
    $("#botones-valores").css("display", "none");
    if (vieneDeValor)
      vieneDeValor.parent().parent().find(".valor-importe").focus().select();
    return false;
  }

  function changeTarjeta($this) {
    var $tarjeta = $this.find(".tarjeta_id");
    var $plan = $this.find(".plan_id");
    $.ajax({
      type: "GET",
      data: {tarjeta_id: $tarjeta.val()},
      url: "<?php echo $this->createUrl("Doc/RecuperaPlanes"); ?>",
      success: function (data) {
        $plan.html(data);
        $plan.trigger("liszt:updated");
        $plan.parent().find(".chzn-container").mousedown();
      },
      error: function (data, status) {
      }
    });
    return false;
  }

  function changeChequera($this) {
    var $chequeraSelect = $this.find(".chequera_id");
    var chequera_id = $chequeraSelect.val();
    var $chequeFechaEmision = $this.find(".cheque-fecha");
    var $chequeraNumeroEdit = $this.find(".cheque-numero");
    $.ajax({
      type: "GET",
//      dataType:"json",
      data: {chequera_id: chequera_id},
      url: "<?php echo $this->createUrl("doc/getChequeraSiguienteNumero"); ?>",
      success: function (data) {
        $chequeraNumeroEdit.val(data);
        $chequeFechaEmision.focus();
      },
      error: function (data, status) {
      }
    });
  }

  function importeChange(obj) {
    $obj = $(obj);
    doc.muestraTotales();
    refresh(true);
  }

  function muestraDetalle(e) {
    if (doc.disabled) {
      return;
    }
    $chq = $("#form_" + id);
    if (e.type == "mouseout") {
      $chq.find(".botones-valores").show();
      $chq.hide();
    } else {
      $chq.find(".botones-valores").hide();
      $chq.show();
    }
  }

  function DateDiff(date1, date2) {
    var datediff = date1.getTime() - date2.getTime(); //store the getTime diff - or +
    return (datediff / (24 * 60 * 60 * 1000)); //Convert values to -/+ days and return value
  }

  function traeCheques() {
    $.ajax({
      type: "POST",
//      dataType:"json",
      data: {
      },
      url: "<?php echo $this->createUrl("doc/getChequesDiv"); ?>",
      success: function (data) {
        $("#cheques-tercero-dialog-table-div").html(data);
        $("#cheques-tercero-dialog").dialog({
          title: "Cartera de cheques",
          buttons: {
            Ok: function () {
              var $importe = $("#valores-table").find(".valor-nombre-td:contains('Cheque')").closest("tr").find(".valor-importe");
              var total = $("#cheques-tercero-dialog-total-td").html();
              $importe.val(total);
              valorImporteChange($importe);
              $("#cheques-tercero-dialog").dialog("close");
            }
          },
          autoOpen: false,
          position: [220, 40],
          width: 650,
          height: 500,
          modal: true,
          resizable: false
        });
      },
      error: function (data, status) {
      }
    });
  }

  function chequeTerceroClick($obj) {
    var total = 0, $tr;
    $(".cheque-check:checked").each(function () {
      $tr = $(this).closest("tr");
      total += Number($tr.find(".cheque-importe-td").html().replace(/,/g, ""));
    });
    $("#cheques-tercero-dialog-total-td").html(round(Number(total), 2));

  }

  function interesCheque(form_id, importe, vieneDeCambioFecha) {
    var $form = $("#" + form_id);
    vieneDeCambioFecha = vieneDeCambioFecha ? true : false;
    $interes_tr = $("#interes_" + $form.attr("id"));
    console.log('function interesCheque $interes_tr ant', $interes_tr);
    if ($interes_tr.length > 0) {
      var interesPagoAnt = Number($interes_tr.find(".interes input").val());
      var interesSaldoAnt = Number($interes_tr.find(".chq-saldo").val());
      doc.totalAPagar -= interesSaldoAnt;
      doc.totalPagos -= interesPagoAnt;
      // doc.saldo -=  round(interesPagoAnt, 2);
      $interes_tr.remove();
      doc.muestraTotales();
      console.log('function interesCheque $interes_tr.remove() importe interes anterior, doc', interesSaldoAnt, doc);
    }

    if ($form.find(".cheque-genera-interes").attr("checked") || vieneDeCambioFecha) {
      $form.find(".cheque-genera-interes").attr("checked", "chequed");
      var cheque_numero = $form.find(".cheque-numero").val();
      var currentTime = new Date();
      var fecha_acreditacion = $form.find(".cheque-fecha-acreditacion").val();
      fecha_acreditacion = String(fecha_acreditacion.substr(3, 2)) + '/' + String(fecha_acreditacion.substr(0, 2)) + '/' + String(fecha_acreditacion.substr(6, 4));
      var interes = 0;
      var banco_id = $form.find(".banco_id").val();
      if (!fecha_acreditacion || !cheque_numero || !banco_id) {
        if (!fecha_acreditacion) {
          colorize($(".cheque-fecha-acreditacion"));
        }
        if (!cheque_numero) {
          colorize($form.find(".cheque-numero"));
        }
        if (!banco_id) {
          colorize($form.find(".chzn-single"));
        }
        $form.find(".cheque-genera-interes").removeAttr("checked");
        return;
      }
      //@todo: el interes se calcula sumando el de cada cheque, de acuerdo a su fecha valor
      var fechaValor = $('#fecha_valor').val();
      if(!fechaValor) {
        fechaValor = currDate = currentTime.getMonth() + 1 + "/" + currentTime.getDate() + "/" + currentTime.getFullYear();

      }
      fechaValor = String(fechaValor.substr(3, 2)) + '/' + String(fechaValor.substr(0, 2)) + '/' + String(fechaValor.substr(6, 4));
      var dias = DateDiff(new Date(fecha_acreditacion), new Date(fechaValor)) - 15;
      console.log('function interesCheque, dias %O, acred. %O, doc %O', dias, fecha_acreditacion, fechaValor);
      if (dias > 0) {
        interes = round(dias * (importe * interesChequeDiario), 2);
        console.log('function interesCheque, interes %O, importe. %O, interesChequeDiario %O', interes, importe, interesChequeDiario);
      } else {
        $form.find(".cheque-genera-interes").removeAttr("checked");
        colorize($(".cheque-fecha-acreditacion"));
        return;
      }
      console.log('function interesCheque, interes:%O', interes);
      var id = GUID();
      var detalle = "Int. chq.#" + cheque_numero;
      var inputSaldo = "<input class=\"chq-saldo\" type='hidden' name=\"intereses[" + id + "][saldo]\" value=\"" + interes + "\" />";
      var inputInteres = "<input value=\"0.00\" name=\"intereses[" + id + "][valor]\" saldo=\"" + interes + "\"/>";
      $("#doc-pendientes .docs table")
              .append(
                      "<tr id='interes_" + form_id + "'>" +
                      "<td>" + detalle +
                      " <input name=\"intereses[" + id + "][detalle]\"" +
                      "        value=\"" + detalle + "\"" +
                      " type=\"hidden\"/>" +
                      "</td>" +
                      "<td class='importe pago' colspan='5'>" +
                      interes +
                      inputSaldo +
                      "</td>" +
                      "<td class='importe pago interes' colspan='5'>" +
                      inputInteres +
                      "</td>" +
                      "</tr>"
                      );
      doc.totalAPagar = round(Number(doc.totalAPagar) + interes, 2);
    }
    doc.muestraTotales();
  }

  function chequeGeneraInteresChange($this) {
    var $form = $this.closest(".valor");
    var valor_id = $form.attr("id").replace('form_', '');
    var importe = $("#" + valor_id).closest("tr").find(".valor-importe").val();
    console.log('function chequeGeneraInteresChange interesCheque( ', $form.attr("id"), importe);

    interesCheque($form.attr("id"), importe);
  }

  function chequeFechaAcreditacionChange($this) {
    var $form = $this.closest(".valor");
    var valor_id = $form.attr("id").replace('form_', '');
    var importe = $("#" + valor_id).closest("tr").find(".valor-importe").val();
    console.log('function chequeFechaAcreditacionChange', $form.attr("id"), importe, true);

    interesCheque($form.attr("id"), importe, true);
    setFocus($detalle.find(".cheque-origen"), 100);

  }


  function xagregaDetalleCheque(e, id) {
    var $valoresDetalle = $("#valores-detalle");
    e.parent().parent().find("img").show();
    $chq = $("#cheque-form-template").clone();
    $chq.attr("id", id).show("fast", function () {
      $valoresDetalle.children(".valor").css("display", "none");
      $valoresDetalle.append($chq);
      $chq.find(".cheque-fecha").datepicker();
      $chq.find(".banco_id").chosen({"width": 200}).change(function () {
        $(".cheque-numero").focus();
      });
      $chq.find(".chzn-container").mousedown();
    });
  }

  function xagregaDetalleTarjeta(e, id) {
    var $valoresDetalle = $("#valores-detalle");
    e.parent().parent().find("img").show();
    $tarjeta = $("#tarjeta-form-template").clone();
    $tarjeta.attr("id", id).show("fast", function () {
      $valoresDetalle.children(".valor").css("display", "none");
      $valoresDetalle.append($tarjeta);
      $tarjeta.find(".tarjeta_id").chosen({"width": 200}).change(function () {
        changeTarjeta($tarjeta.find(".tarjeta_id"))
      });
      $tarjeta.find(".plan_id").chosen().change(function () {
        $("#lote").focus();
      });
      ;
      $tarjeta.find("#tarjeta_id_chzn").mousedown();
    });
  }

  function traeCupones() {
    $.ajax({
      type: "POST",
//      dataType:"json",
      data: {
      },
      url: "<?php echo $this->createUrl("doc/getCuponesDiv"); ?>",
      success: function (data) {
        $("#cupones-dialog-table-div").html(data);
        $("#cupones-dialog").dialog({
          title: "Cupones por Cerrar",
          buttons: {
            Ok: function () {
              var $importe = $("#valores-table").find(".valor-nombre-td:contains('Tarjeta')").closest("tr").find(".valor-importe");
              var total = $("#cupones-dialog-total-td").html();
              $importe.val(total);
              valorImporteChange($importe);
              $("#cupones-dialog").dialog("close");
            }
          },
          autoOpen: false,
          position: [220, 40],
          width: 650,
          height: 500,
          modal: true,
          resizable: false
        });
      },
      error: function (data, status) {
      }
    });
  }

  function cuponClick($obj) {
    var total = 0, $tr;
    $(".cupon-check:checked").each(function () {
      $tr = $(this).closest("tr");
      total += Number($tr.find(".cupon-importe-td").html().replace(/,/g, ""));
    });
    $("#cupones-dialog-total-td").html(round(Number(total), 2));

  }

  function traeRetenciones() {
    $.ajax({
      type: "POST",
//      dataType:"json",
      data: {
      },
      url: "<?php echo $this->createUrl("doc/getRetencionesDiv"); ?>",
      success: function (data) {
        $("#retenciones-dialog-table-div").html(data);
        $("#retenciones-dialog").dialog({
          title: "Cupones por Cerrar",
          buttons: {
            Ok: function () {
              var $importe = $("#valores-table").find(".valor-nombre-td:contains('Retención')").closest("tr").find(".valor-importe");
              var total = $("#retenciones-dialog-total-td").html();
              $importe.val(total);
              valorImporteChange($importe);
              $("#retenciones-dialog").dialog("close");
            }
          },
          autoOpen: false,
          position: [220, 40],
          width: 650,
          height: 500,
          modal: true,
          resizable: false
        });
      },
      error: function (data, status) {
      }
    });
  }


  function retencionClick($obj) {
    var total = 0, $tr;
    $(".retencion-check:checked").each(function () {
      $tr = $(this).closest("tr");
      total += Number($tr.find(".retencion-importe-td").html().replace(/,/g, ""));
    });
    $("#retenciones-dialog-total-td").html(round(Number(total), 2));

  }
</script>
