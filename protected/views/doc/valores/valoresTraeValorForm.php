<?php if ($tipo_valor == "2"): ?>
    <div class="valor" id="<?php echo $id ?>">
        <input type="hidden" value="<?php echo DocValor::TIPO_CHEQUE; ?>" name="tipo"/>
        <div class="row">
            <div class="banco_mas_numero inner-row">
                <select name="banco_id" id="" data-placeholder="- Banco -" class="banco_id" style="width:150px">
                    <?php $k = array('prompt' => ''); ?>
                    <?php echo CHtml::listOptions(Null, CHtml::listData(Banco::model()->findAll(), "id", "nombreMasCodigo"), $k); ?>
                </select>
                <input name="numero" class="cheque-numero" value="" placeholder=" - Número - "/>
            </div>
            <div class="fecha_mas_origen inner-row">
                <input name="fecha" 
                       class="cheque-fecha-acreditacion" 
                       value="<?php echo date("d/m/Y", time()); ?>"
                       onchange="chequeFechaAcreditacionChange($(this));"
                       placeholder=" - Fecha Acreditación - "/>
                <input name="chq_origen" class="cheque-origen" 
                       value="" placeholder=" - Origen - "/>
            </div>
            <div class="cuit_mas_interes inner-row">
                <input name="chq_cuit_endosante" class="cheque-cuit" value="" placeholder=" - CUIT Endosante - "/>
                <div id="div-cheque-genera-interes">
                    <label for="cheque-genera-interes">
                        Genera Interes
                        <input name="cheque_genera_interes" 
                               class="cheque-genera-interes" 
                               value="" 
                               onchange="chequeGeneraInteresChange($(this));"
                               placeholder="Genera Interes" 
                               type="checkbox"/>
                    </label>
                </div>
            </div>
            <script type="text/javascript">
                           if (doc.matricula) {
                               $("#<?php echo $id ?>").find(".cheque-origen").val(doc.matricula);
                           }
                           if (!doc.tipo) {
                               $("#div-cheque-genera-interes").hide();
                           }
            </script>
        </div>
    </div>
<?php endif; ?>

<?php if ($tipo_valor == "3"): ?>
    <div class="valor" id="<?php echo $id ?>">
        <input type="hidden" value="<?php echo DocValor::TIPO_TARJETA; ?>" name="tipo"/>
        <div class="row">
            <div class="inner-row">
                <select class="tarjeta_id" data-placeholder="- Tarjeta -" name="tarjeta_id" class="tarjeta_id" style="width:150px">
                    <?php $k = array('prompt' => ''); ?>
                    <?php echo CHtml::listOptions(Null, CHtml::listData(Tarjeta::model()->findAll(), "id", "nombre"), $k); ?>
                </select>
                <select class="plan_id" data-placeholder="- Plan Tarjeta -" name="tarjeta_plan_id" class="plan_id" style="width:100px">
                    <?php $p = array('prompt' => ''); ?>
                    <?php echo CHtml::listOptions(Null, CHtml::listData(TarjetaPlan::model()->findAll("Tarjeta_id=-1"), "id", "nombre"), $p); ?>
                </select>
            </div>
            <div class="inner-row">
        <!--            <input name="numero" class="tarjeta-numero" value="" placeholder=" - Número - "/>-->
                <input name="lote" id="lote" value="" placeholder=" - # Lote - "/>
                <input name="numero" id="cupon" value="" placeholder=" - # Cupón - "/>
            </div>
        </div>
    </div>

<?php endif; ?>

<?php if ($tipo_valor == "4"): ?>
    <div class="valor tipo-chq-propio" id="<?php echo $id ?>">
        <input type="hidden" value="<?php echo DocValor::TIPO_CHEQUE_PROPIO; ?>" name="tipo"/>
        <div class="row">
            <div class="inner-row">
                <select name="chequera_id" data-placeholder="- Chequera -" class="chequera_id" style="width:150px">
                    <?php $k = array('prompt' => ''); ?>
                    <?php echo CHtml::listOptions(Null, Chequera::listData(true), $k); ?>
                </select>
                <script type="text/javascript">
                    $("#<?php echo $id; ?>").find(".chequera_id").chosen().change(function() {
                        changeChequera($("#<?php echo $id; ?>"));
                    });
                    ;
                </script>
                <input name="numero" class="cheque-numero" value="" placeholder=" - Número - "/>
            </div>
            <div class="inner-row">
                <input name="fecha"
                       class="cheque-fecha-acreditacion"
                       value="<?php echo date("d/m/Y", time()); ?>" 
                       placeholder=" - Fecha de cobro - "
                       />
                <input type="hidden" name="chq_entregado_a" class="cheque-a_la_orden_de"
                       value="-" placeholder=" - A la orden de - "/>
            </div>
            <div class="inner-row">
                <input  class="cheque-obs-div" name="obs" class="cheque_detalle"
                       value="" placeholder=" - Detalle - "/>
                <div class="cheque-interes-div">
                    <label for="cheque-diferido">
                        Diferido
                        <input name="cheque-diferido" class="cheque-diferido" value="" placeholder=" - diferido - " type="checkbox"/>
                    </label>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if (($tipo_valor == "5") or ($tipo_valor == "6")): ?>
    <div class="valor" id="<?php echo $id ?>">
    <!--    <input type="hidden" value="<?php //echo $tipo_valor;                    ?>" name="rec-tipo-valor"/>-->
        <input type="hidden" value="<?php
        echo $tipo_valor == "5" ? DocValor::TIPO_RECAUDACION_BANCARIA : DocValor::TIPO_TRANSFERENCIA;
        ?>" name="tipo"/>
        <div class="row">
            <select name="Destino_id" data-placeholder="- Cta. Bancaria -" class="select" style="width:150px">
                <?php
                $k = array('prompt' => '');
                echo CHtml::listOptions(Null, CHtml::listData(
                                Destino::model()->with("banco")->findAll("tipo = 2"), "id", "nombreMasCuenta"
                        ), $k
                );
                ?>
            </select>
            <div>
                <input name="fecha" class="fecha" value="<?php echo date("d/m/Y", time()); ?>" 
                       placeholder=" - Fecha Acreditación - "/>
                <input name="numero" class="numero" value="" placeholder=" - Número - "/>
                <input name="obs" class="obs" value="" placeholder=" - Observaciones - "/>
            </div>
        </div>
    </div>
<?php endif; ?>
