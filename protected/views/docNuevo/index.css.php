<style type="text/css">
	.navbar {margin-bottom: 2px}
	.importe{width: 40px; text-align: right}
	#apps-div, #valores-div{overflow: auto}
	#apps-div table, #valores-div table, #totales-div table {   margin-bottom: 10px;}
	#docs-div .total{font-weight: bold; padding-right: 9px; width:100px}
	.importe input{width:70px;font-size: 11.5px}
	#data-form{padding: 15px 0}
	#numero-input{width:112px}
	#fecha-input{width:84px}
	.bs:after {
		color: #BBBBBB;
		font-size: 12px;
		font-weight: 700;
		left: 15px;
		letter-spacing: 1px;
		position: absolute;
		text-transform: uppercase;
		top: 15px;
	}
	.bs {
		border-radius: 4px 4px 0 0;
		box-shadow: none;
		border-color: #E5E5E5 #EEEEEE #EEEEEE;
		border-style: solid;
		border-width: 1px 0;
		box-shadow: 0 3px 6px rgba(0, 0, 0, 0.05) inset;
		padding: 40px 20px 10px;
		//		position: relative;		
	}
	#apps-div:after{content:"Aplicaciones"}
	#valores-div:after{content:"Valores"}
	#totales-div:after{content:"Totales"}
	#data-div:after{content: attr(doc-tipo)}
	#totales-div .table > tbody > tr > td{border-top:none}
</style>
