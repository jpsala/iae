<script  type="text/javascript" src="js/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script  type="text/javascript" src="js/bootstrap-datepicker/locales/bootstrap-datepicker.es.js"></script>
<script  type="text/javascript" src="js/views/doc/docViewModel.js"></script>
<link rel="stylesheet" href="js/bootstrap-datepicker/datepicker3.css"/>
<?php include("index.css.php"); ?>
<style>td > * {
    //vertical-align : middle;
	}
	.input-sm{height:24px !important;}
	//td>span{line-height: 30px;display:inline-block}
</style>
<div id="docs-div" class="container">
	<span data-bind="text:cant"></span>
	<div id="data-div" class="col-xs-12 bs" data-bind="attr:{'doc-tipo':options.doc_tipo_str}">
		<form id="data-form" class="col-xs-12 form form-inline">
			<div class="form-group">
				<label for="numero-input">Número</label>
				<input id="numero-input" type='text'
							 data-bind="
										hasfocus:data().numeracion_manual,
										value:numeroCompleto(),
										enable:data().numeracion_manual
							 " class="form-control input-sm"/>
			</div>
			<div class="form-group">
				<label for="fecha-input">Fecha</label>
				<input id="fecha-input" type='text' 
							 data-bind="
										value:data().fecha_valor,
										hasfocus:!data().numeracion_manual"
							 class="form-control input input-sm"/>
				<i class="fa fa-calendar" style="cursor:pointer" onclick="$('#fecha-input').datepicker('show')"></i>
			</div>
		</form>
	</div>
	<div class="container row">
		<div id="apps-div" class="col-xs-8 bs">
			<table class="table table-condensed table-hover table-striped table-responsive">
				<thead>
					<tr>
						<td class="small">Comprob</td>
						<td class="small">Detalle</td>
						<td class="small">Fecha</td>
						<td class="small">Total</td>
						<td class="small">Saldo</td>
						<td class="small">Importe</td>
						<td></td>
					</tr>
				</thead>
				<tbody>
					<!-- ko foreach: apps -->
					<tr>
						<td><span class="small" data-bind="text:comprob"></span></span></td>
						<td><span class="small" data-bind="text:detalle"></span></td>
						<td><span class="small" data-bind="text:fecha"></span></td>
						<td><span class="small importe" data-bind="text:total"></span></td>
						<td><span class="small importe" data-bind="text:saldo"></span></td>
						<td><span class="small importe">
								<input class="form-control pull-right input-sm importe" type="text" 
											 data-bind="
													value:importe, valueUpdate:'input',
											 "/>
						</td>
					</tr>
					<!-- /ko -->
					<tr>
						<td class="small" >Total</td>
						<td class="small" ></td>
						<td class="small" ></td>
						<td class="small importe"></td>
						<td class="small importe"></td>
						<td class="small importe total" data-bind="text:fmt(appsTotal())"></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div id="valores-div" class="col-xs-4 bs">
			<table class="table table-condensed table-hover table-striped table-responsive">
				<thead>
					<tr>
						<td class="small">Tipo</td>
						<td class="small">Importe</td>
						<td></td>
					</tr>
				</thead>
				<tbody>
					<!-- ko foreach: valores -->
					<tr>
						<td><span class="small" data-bind="text:tipo_nombre"></span></td>
						<td>
							<span class="small importe">
								<input class="form-control pull-right input-sm importe" type="text" 
											 data-bind="
											value:importe, 
											valueUpdate:'input',
											numeroValido:null,
											event:{focus:selectedValor} 

											 "/>
							</span>
						</td>
					</tr>
					<!-- /ko -->
					<tr>
						<td class="small" >Total</td>
						<td class="small importe total" data-bind="
						text:fmt(valoresTotal()),
						style: {color: total()<0 ? 'red':''}						">
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="pull-right">
			<a href="#" class="btn btn-warning" data-bind="click:cancel">Cancela</a>
			<a href="#" class="btn btn-default btn-primary" data-bind="click:submit">Graba</a>
		</div>
	</div>
	<div  id="totales-div" class="bs col-xs-4 pull-right">
		<table id="totales-table" class="table table-condensed">
			<tr>
				<td>Applicaciones</td>
				<td class="importe total pull-right" data-bind="text:fmt(appsTotal())"></td>
			</tr>
			<tr>
				<td id="pago-a-cuenta-detalle">
					<input  type="text" data-bind="value:data().pago_a_cuenta_detalle"/>
				</td>
				<td class="importe total pull-right">
					<input class="text-right" type="text" data-bind="value:data().pago_a_cuenta_importe, valueUpdate:'input'"/>
				</td>
			</tr>
			<tr>
				<td>Valores</td>
				<td class="importe total pull-right" data-bind="
						text:'-'+fmt(valoresTotal())"
						></td>
			</tr>
			<tr>
				<td>General</td>
				<td class="importe total pull-right" data-bind="
						text:fmt(total()),
						style: {color: !totalCero() ? 'red':''}
						"></td>
			</tr>
		</table>
	</div>
</div>

<div id="footer" data-bind="visible:true">
	<div class="container">
		<pre data-bind="text: JSON.stringify(ko.toJS(debug), null, 2), visible:true"></pre>
	</div>
</div>

<script  type="text/javascript">
	var params = {
		doc_id:105445,socio_id:35265, doc_tipo_id:1, doc_tipo_str: "Recibo",
		submitUrl: "<?php echo $this->createUrl("submit"); ?>",
		valorVacioUrl:"<?php echo $this->createUrl("docNuevo/valorVacioAjax"); ?>",
		dataUrl:"<?php echo $this->createUrl("docNuevo/dataAjax"); ?>",
		valoresUrl:"<?php echo $this->createUrl("docNuevo/valoresAjax"); ?>",
		appsUrl:"<?php echo $this->createUrl("docNuevo/appsAjax"); ?>"				
	};
	
	docViewModel.init(params)
			.then(function(msg){
				if(msg==="ok"){
					docViewModelReady();
				}else{
					alert("error: "+ msg);
				}
	});
	
	function docViewModelReady(){
			ko.applyBindings();
			$("#fecha-input").datepicker({
				language: 'es',
				startView: 1,
				todayBtn: "linked",
				autoclose:true,
				daysOfWeekDisabled: "0",
				todayHighlight: true
			});
			docViewModel.debug.push(docViewModel.root);
	}
</script>
