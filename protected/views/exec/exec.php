<?php if ($action == ""): ?>
    <style type="text/css">
        #test-div a{cursor: pointer;margin-left: 10px}
        #test-div{padding: 5px}
        #ajax{margin-top: 15px; min-height: 100px; width: 100%;padding: 10px}
    </style>

<?php endif; ?>
<?php
if ($action == "") {
    echo '<div id="test-div" class="ui-widget ui-widget-content ui-corner-all">';
}

class test extends stdClass {

    public function __construct($action, $controller, $exec) {
        /* @var $controller CController */
        if ($action == "") {
            foreach (get_class_methods("test") as $m) {
                if ($m !== "__construct") {
                    $url = $controller->createUrl("exec/index", array("action" => $m, "exec" => 1));
                    echo "<button id=\"$m\" onclick=\"return irA('" . $url . "','" . $m . "');\">$m</button>";
                }
            }
        } else {
            if (!method_exists($this, $action)) {
                throw new Exception("No existe el metodo $action en el view test");
            } else {
                try {
                    $this->$action();
                } catch (Exception $exc) {
                    ve($exc->getTraceAsString());
                }
            }
        }
    }

    private function docLiquiqPerdidos() {
        $cf = Comprob::FACTURA_VENTAS;
        $rows = Yii::app()->db->createCommand("
                select doc.*, l.*
                    from doc 
                        inner join doc_liquid l on l.id = doc.doc_liquid_id
                        inner join talonario t on t.id = doc.talonario_id
                    where l.liquid_conf_id = (select max(id) from liquid_conf) and t.comprob_id = $cf
            ")->queryAll();
        vd($rows);
    }

    private function verUltimaLiquidacion() {
        $cf = Comprob::FACTURA_VENTAS;
        $rows = Yii::app()->db->createCommand("
                select concat(a.apellido, ', ', a.nombre) as alumno, doc.*, l.*
                    from doc 
                        inner join doc_liquid l on l.id = doc.doc_liquid_id
                        inner join socio s on s.id = doc.`Socio_id`
                        inner join alumno a on a.id = s.`Alumno_id`
                        inner join talonario t on t.id = doc.talonario_id
                    where l.liquid_conf_id = (select max(id) from liquid_conf) and t.comprob_id = $cf
            ")->queryAll();
        vd($rows);
    }

    private function becas() {
        $cf = Comprob::FACTURA_VENTAS;
        $cantRecargos = Yii::app()->db->createCommand("
                select count(*)
                    from doc 
                        inner join doc_liquid l on l.id = doc.doc_liquid_id
                        inner join talonario t on t.id = doc.talonario_id
                    where l.liquid_conf_id = (select max(id) from liquid_conf) and t.comprob_id = $cf and
                                l.beca > 0
            ")->queryScalar();
        echo "Cantidad de recargos : $cantRecargos<br/>";
    }

    private function statusUltimaLiquidacion() {
        $cf = Comprob::FACTURA_VENTAS;
        $cantRecargos = Yii::app()->db->createCommand("
                select count(*)
                    from doc 
                        inner join doc_liquid l on l.id = doc.doc_liquid_id
                        inner join talonario t on t.id = doc.talonario_id
                    where l.liquid_conf_id = (select max(id) from liquid_conf) and t.comprob_id <> $cf
            ")->queryScalar();
        echo "Cantidad de recargos : $cantRecargos<br/>";
        $cantLiquidados = Yii::app()->db->createCommand("
                select count(*)
                    from doc 
                        inner join doc_liquid l on l.id = doc.doc_liquid_id
                        inner join talonario t on t.id = doc.talonario_id
                    where l.liquid_conf_id = (select max(id) from liquid_conf) and t.comprob_id = $cf
            ")->queryScalar();
        echo "Cantidad de liquidados : $cantLiquidados<br/>";
        $cantDocLiquid = Yii::app()->db->createCommand("
                select count(*)
                    from doc_liquid l
                    where l.liquid_conf_id = (select max(id) from liquid_conf)
            ")->queryScalar();
        echo "Cantidad de docLiquid : $cantDocLiquid<br/>";
        $cantDocLiquidSueltos = Yii::app()->db->createCommand("
                select count(*) from doc_liquid l
                    where l.id not in
                        (select d.doc_liquid_id from doc d where d.doc_liquid_id = l.id)
            ")->queryScalar();
        echo "Cantidad de docLiquid Sueltos: $cantDocLiquidSueltos<br/>";
    }

    private function borraDocLiquidSuelgos() {
        $docLiquidSueltos = Yii::app()->db->createCommand("
                select * from doc_liquid l
                    where l.id not in
                        (select d.doc_liquid_id from doc d where d.doc_liquid_id = l.id)
            ")->queryAll();
        ve($docLiquidSueltos);
        return;
        $cantDocLiquidSueltos = Yii::app()->db->createCommand("
                delete l.* from doc_liquid l
                    where l.id not in
                        (select d.doc_liquid_id from doc d where d.doc_liquid_id = l.id)
            ")->execute();
        echo "Cantidad de docLiquid Sueltos Borrados: $cantDocLiquidSueltos<br/>";
    }

    private function ultimosDocConDocValor() {
        $s = "
            select  d.fecha_vto1, d.fecha_vto2, d.fecha_creacion, d.fecha_modificacion, d.fecha_valor, v.fecha from doc d
                left join doc_valor v on v.doc_id = d.id
                order by d.id desc
                limit 100 ";
        ve($docs = Helpers::qryAllOrEmpty($s));
    }

    private function ParaOctavio() {
        $s = "
            select d.id, c.nombre as comprob, d.numero, d.detalle,  d.fecha_creacion as \"Fec. Creación\", d.fecha_valor, d.total, d.saldo
                from doc d
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
              where d.socio_id = 36558
              order by d.id asc";
        $alumnos = Helpers::qryAll($s);
        foreach ($alumnos as $key => $row) {
            $sValores = "
                select t.nombre as tipo, v.numero, v.fecha, v.obs, v.importe
                    from doc_valor v
                        inner join doc_valor_tipo t on t.id = v.tipo
                    where v.doc_id=" . $row["id"];
            $alumnos[$key]["detailData"]["valores"] = Helpers::qryAll($sValores);
            $sApps = "
                select a.importe, d.detalle, concat(\"or\",\".\",d2.numero) as origen, concat(\"des\",\".\",d.numero) as destino, d.total,  d.saldo, d.fecha_creacion as \"Fec. Creación\"
                    from doc_apl_cab c
                        inner join doc_apl a on a.doc_apl_cab_id = c.id
                        left join doc d on d.id = a.doc_id_destino
                        left join doc d2 on d2.id = a.doc_id_origen
                    where a.doc_id_origen =" . $row["id"] . " or  a.doc_id_destino =" . $row["id"];
            $alumnos[$key]["detailData"]["apps"] = Helpers::qryAll($sApps);
        }
        $cols["alumno"] = array(
                "Fec. Creación" => array("date" => true, "format" => "d/m/Y"),
                "fecha_valor" => array("date" => true, "format" => "d/m/Y", "title" => "fec. valor"),
        );
        $cols["id"] = array(
                "hidden" => true,
        );
        $cols["valores"] = array(
                "fecha" => array("date" => true, "format" => "d/m/Y", "title" => "fecha"),
        );
        $cols["apps"] = array(
                "Fec. Creación" => array("date" => true, "format" => "d/m/Y"),
        );
        $div = Helpers::getTable("alumno", $alumnos, $cols, true, false);
        echo $div;
    }

    private function ultimosDocs() {
        $s = "
            select d.id, c.nombre as comprob, d.numero, d.fecha_creacion, d.fecha_valor, d.total, d.saldo
                from doc d
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
              where (select count(*) from doc_apl a where a.doc_id_origen = d.id) > 1";
        "
              order by d.id desc
              limit 100";
        $alumnos = Helpers::qryAllOrEmpty($s);
        foreach ($alumnos as $key => $row) {
            $sValores = "
                select *
                    from doc_valor v
                    where v.doc_id=" . $row["id"];
            $alumnos[$key]["detail"]["valores"] = Helpers::qryAllOrEmpty($sValores);
            $sApps = "
                select a.importe, d.detalle, d.total, d.saldo, d.fecha_creacion
                    from doc_apl_cab c
                        inner join doc_apl a on a.doc_apl_cab_id = c.id
                        inner join doc d on d.id = a.doc_id_destino
                    where a.doc_id_origen =" . $row["id"];
            $alumnos[$key]["detail"]["apps"] = Helpers::qryAllOrEmpty($sApps);
        }
        $cols["alumno"] = array(
                "fecha_creacion" => array("date" => "d/m/Y"),
        );
        $div = Helpers::getTable("alumno", $alumnos, $cols, true);
        echo $div;
    }

    private function ctaCteViejo() {
//            $matricula = Yii::app()->db->createCommand("
//            select a.matricula
//              from socio s
//                inner join alumno a on a.id = s.alumno_id
//              where s.id = $socio_id
//          ")->queryScalar();
        $matricula = 2690;
        $db = Yii::app()->fb->connection;
        $select = "
          select * from te_trae_cc($matricula,Null)";
        $sth = ibase_query($db, $select);
        $rows = array();
        while ($row = ibase_fetch_assoc($sth)) {
            $rows[] = $row;
        }
        vd($rows);
    }

    private function chkCtaAlumnos() {
        
    }

    private function saldos2doVtoParaBorrar() {
        $s = "select concat(a.apellido, \", \", a.nombre) as alumno, saldo(s.id,null) as saldo
                from doc d 
                    inner join socio s on s.id = d.socio_id
                    inner join alumno a on a.id = s.Alumno_id
                    inner join doc_liquid l on l.id = d.doc_liquid_id
                    inner join liquid_conf c on c.id = l.liquid_conf_id
                where d.total = 40 and d.detalle = \"Recargo 2do. Vto.\" and c.id = 5 and d.fecha_modificacion is null and d.anulado = 0 and d.saldo = d.total and saldo(d.socio_id,null) < 2400
                order by a.apellido, a.nombre
        ";
        $rows= Helpers::qryAll($s);
        $cols=array("saldos"=>array(
                ""
        ));
        $t = Helpers::getTable("saldos", $rows);
        $t .= "
            <script>
                $(function(){
                    alert($(\".tablegen-table-tr-search\").length + \" registros \");
                });
            </script>
        ";
        echo $t;
    }

    private function cantidadDeFamiliasActivas() {
        $select = "
            select count(*)  as cantidad from familia f
                where (select count(*) from alumno a where a.familia_id = f.id and a.activo=1) > 0
         ";
        $cantidad = Helpers::qryScalar($select);
        echo "Familias con al menos 1 alumno activo: " . $cantidad;
    }

}

new test($action, $this, $exec);

if ($action == "") {
    echo "</div>";
}
?>

<?php if ($action == ""): ?>
    <div id="ajax" class="ui-widget ui-widget-content ui-corner-all"></div>

    <script type="text/javascript">

        $("button").button();

        function irA(url, action) {
            $("#" + action).unbind("keydown.a");
            $("#" + action).bind("keydown.a", "alt+c", function() {
                $("#" + action).click();
                return false;
            });
            $.ajax({
                type: "GET",
                url: url,
                success: function(data) {
                    $("#ajax").html(data);
                },
                error: function(data, status) {
                }
            }
            );
            return false;
        }
    </script>
<?php endif; ?>