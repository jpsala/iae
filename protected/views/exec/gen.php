<?php $divGen = isset($data["options"]["divNombre"]) ? $data["options"]["divNombre"] : "$modelName-gen"; ?>
<style type="text/css">
<?php if ($borders): ?>
		#<?php echo $divGen; ?> {clear: both; padding: 7px; margin: 7px; overflow: visible}
<?php else: ?>
		#<?php echo $divGen; ?> {clear: both; overflow: visible}
<?php endif; ?>
	#<?php echo $divGen; ?> .gen-label-fset{float:left; width: 60px;  text-align: left }
	#<?php echo $divGen; ?> textarea{width: 99%}
	#<?php echo $divGen; ?> .label-float label{width:72px; float: left;  text-align: left; margin-right: 4px}
	#<?php echo $divGen; ?> label.gen-label{display: inline-block;line-height: 23px;margin-left: 6px}
	#<?php echo $divGen; ?> .gen-control{}
	/*#<?php //echo $divGen;        ?> .label-float{margin-bottom: 5px;}*/
	#<?php echo $divGen; ?> .gen-div-grp {}
	#<?php echo $divGen; ?> .gen-div-grp div{ display: inline-block}
	#<?php echo $divGen; ?> fieldset{padding: 5px}
	#<?php echo $divGen; ?> .gen-select{}
	#<?php echo $divGen; ?> .chzn-container{vertical-align: top; margin-right: 5px; margin-topp:  -3px; vertical-align: bottom;line-height: 23px;display:inline-block}
	#<?php echo $divGen; ?> .gen-vertical-bottom{vertical-align: baseline;}
	#<?php echo $divGen; ?> fieldset div{display: inline-block}
	#<?php echo $divGen; ?> .gen-date{width:100px}
	#<?php echo $divGen; ?> fieldset, .gen-div-solo, .gen-div-grp{margin-bottom:  10px}
	#<?php echo $divGen; ?> .gen-fset-control-div{  line-height: 20px;margin-bottom: 3px;vertical-align: sub; margin-top: 5px}
<?php foreach ($data as $masterFldName => $masterVal): ?>
	<?php if (isset($cols[$masterFldName]["style"])): ?>
		<?php $id = isset($cols[$masterFldName]["id"]) ? $cols[$masterFldName]["id"] : "$modelName-$masterFldName"; ?>
			#<?php echo $id; ?>{<?php echo $cols[$masterFldName]["style"]; ?>}
	<?php endif; ?>
							
<?php endforeach; ?>
</style>
<?php
$classGenDiv = $borders ? ("ui-widget ui-widget-content ui-corner-all  " . (isset($data["class"]) ? $data["class"] : "")) : "";
$divs = array();
foreach ($data as $masterFldName => $masterVal) {
	//ve($masterVal);
	$label = isset($cols[$masterFldName]["label"]) ? $cols[$masterFldName]["label"] : $masterFldName;
	$placeholder = isset($cols[$masterFldName]["placeholder"]) ? $cols[$masterFldName]["placeholder"] : "";
	$id = isset($cols[$masterFldName]["id"]) ? $cols[$masterFldName]["id"] : "$modelName-$masterFldName";
	$div = isset($cols[$masterFldName]["div"]) ? $cols[$masterFldName]["div"] : $masterFldName . "-div";
	$fieldset = isset($fieldSets[$masterFldName]) ? $fieldSets[$masterFldName] : null;
	$tipo = isset($cols[$masterFldName]["tipo"]) ? $cols[$masterFldName]["tipo"] : "input";
	$data = isset($cols[$masterFldName]["data"]) ? $cols[$masterFldName]["data"] : Null;
	$name = $modelName . "[" .
		(isset($cols[$masterFldName]["name"]) ? $cols[$masterFldName]["name"] : $masterFldName) .
		"]";
	$options = isset($cols[$masterFldName]["options"]) ? $cols[$masterFldName]["options"] : array();
	$dropDowns = "";
	$optionsStr = "";
	foreach ($options as $optNombre => $optVal) {
		$optionsStr .= $optNombre . "=\"$optVal\"";
	}
	$classLabelFset = inFieldSet($masterFldName, $fieldSets) ? " " : " ";
	$htmlLabel = ($label !== "" and $tipo !== "hidden") ? "<label class=\"$classLabelFset gen-label\" for=\"$id\">$label</label>" : "";

	if ($tipo == "dropDown") {
		$arr = array("class" => "gen-select");
		$htmlControl = CHtml::dropDownList($name, $masterVal, $data, $arr);
		$dropDowns .= "#" . $name . ",";
	} elseif ($tipo == "checkbox") {
		$checked = $masterVal ? " checked " : " ";
		$htmlControl = "<input  type=\"hidden\" name=\"$name\" value=\"0\" />" .
			"<input  type=\"checkbox\"  id=\"$id\" " .
			" placeholder=\"$placeholder\"  class=\"gen-control gen-vertical-bottom \" " .
			$optionsStr .
			" value=\"1\" " .
			" name=\"$name\"" .
			$checked . "/>";
	} elseif ($tipo == "hidden") {
		$htmlControl = "<input  type=\"hidden\" id=\"$id\" placeholder=\"$placeholder\" " .
			" name=\"$name\"  class=\"gen-control\" " .
			$optionsStr . " value=\"$masterVal\"  name=\"$name\" />";
	} elseif ($tipo == "textarea") {
		$htmlControl = "<textarea id=\"$id\" placeholder=\"$placeholder\" " .
			"class=\"gen-control \" " .
			$optionsStr . " name=\"$name\">$masterVal</textarea>";
	} else {
		$class = "gen-control";
		$isDate = isset($cols[$masterFldName]["date"]) ? $cols[$masterFldName]["date"] : "";
		if ($isDate) {
			$class .=" gen-date";
			$format = isset($cols[$masterFldName]["format"]) ? $cols[$masterFldName]["format"] : "d/m/Y";
			$masterVal = date($format, mystrtotime($masterVal));
		}
		$htmlControl = "<input  type=\"text\" id=\"$id\" placeholder=\"$placeholder\" " .
			"class=\"$class\" value=\"$masterVal\" " .
			$optionsStr . " value=\"$masterVal\"  name=\"$name\" />";
	}
	if ($fieldset) {
		$fieldSets[$masterFldName]["nombre"] = $masterFldName;
		$divs[$div][$masterFldName] = $fieldSets[$masterFldName];
	} else {
		$fset = inFieldSet($masterFldName, $fieldSets);
		$datos = array("label" => $htmlLabel, "tipo" => $tipo, "control" => $htmlControl, "nombre" => $masterFldName);
		if ($fset) {
			$fieldSets[$fset]["controles"][$masterFldName] = $datos;
		} else {
			$divs[$div][$masterFldName] = $datos;
		}
	}
}
echo "<div id=\"$divGen\" class=\"$classGenDiv\">";
foreach ($divs as $divNombre => $controles) {
	if (count($divs[$divNombre]) > 1) {
		$class = " label ";
		echo "<div id=\"$divNombre\" class=\"gen-div-grp\">";
	} else {
		$class = " label-float gen-div-solo ";
	}
	foreach ($controles as $control) {
		if (!isset($control["tipo"])) {
			$fieldSetNombre = $control["nombre"];
			echo "<fieldset id=\"$fieldSetNombre\" class=\" ui-widget ui-widget-content ui-corner-all\">";
			if (isset($fieldSets[$fieldSetNombre]["legend"])) {
				$legend = $fieldSets[$fieldSetNombre]["legend"];
				echo "<legend>$legend</legend>";
			}
			foreach ($fieldSets[$fieldSetNombre]["controles"] as $fsetControl) {
				if ($fsetControl["label"]) {
					$classLabelFloat = $fsetControl["tipo"] == "textarea" ? " " : " label-float ";
					$fsetControlNombre = $fsetControl["nombre"];
					echo "<div class=\"gen-fset-control-div $classLabelFloat gen-fset-$fieldSetNombre-$fsetControlNombre\">";
				}
				echo $fsetControl["label"] . $fsetControl["control"];
				if ($fsetControl["label"]) {
					echo "</div>";
				}
			}
			echo "</fieldset>";
		} else {
			$controlNombre = $control["nombre"];
			if ($control["tipo"] !== "hidden") {
				echo "<div id=\"alumno-$controlNombre-div\" class=\"$class gen-control\">";
			}
			echo $control["label"] . $control["control"];
			if ($control["tipo"] !== "hidden") {
				echo "</div>";
			}
		}
	}

	if ((count($divs[$divNombre])) > 1) {
		echo "</div>";
	}
}

echo "</div>";

function inFieldSet($masterFldName, $fieldSets) {
	foreach ($fieldSets as $nombre => $fset) {
		if (!isset($fset["campos"])) {
			return false;
		}
		if (in_array($masterFldName, array_values($fset["campos"]))) {
			return $nombre;
		}
	}
	return false;
}
?>
<script type="text/javascript">
	$("#<?php echo $divGen; ?> .gen-select").chosen();
	$(function() {
		$("#<?php echo $divGen; ?> .gen-date").datepicker();
		$("#mytest").datepicker();
	});

</script>