<?php
$cols = isset($colsGeneral[$modelName]) ? $colsGeneral[$modelName] : array();
$defaults = array(
        "borders" => true,
        "filter" => true,
        "title" => true,
);
$options = array_merge($defaults, $options);
?>

<?php $divGenTable = "$modelName-tablegen"; ?>
<style type="text/css">
<?php if ($options["borders"]): ?>
        #<?php echo $divGenTable; ?> {clear: both; padding: 7px; margin: 7px; overflow: visible}
<?php else: ?>
        #<?php echo $divGenTable; ?> {clear: both; overflow: visible}
<?php endif; ?>
    .tablegen-table-div {margin-left: 10px}
    .tablegen-table-title-div {padding:5px;background-color: #EEEEFF;background-image: none;color: #FF0000;
    }
    #tablegenquickfilter-div{padding:5px; margin-bottom: 5px}
    .tablegen-border{margin: 8px 0px 0px 0px ; padding: 6px 0 0 0; border-radius: 6px}
    #tablegenquickfilter-div > input {
        width: 70px;
    }
    #tablegenquickfilter-div label {
        width: 38px;
    }
    /*#tablegenquickfilter-div{margin-top: 4px}*/
</style>
<?php if ($options["filter"]): ?>
    <div id="tablegenquickfilter-div" class="ui-widget ui-widget-content ui-corner-all">
        <label for="tablegenquickfilter">Filtro:</label>
        <input id="tablegenquickfilter" type="text"/>
    </div>
<?php endif; ?>
<div class="tablegen-border ui-widget ui-widget-content">
    <?php
    renderTable($data, $modelName, $cols, false, $colsGeneral, $options);

    function renderTable($rows, $modelName, $cols, $detail = false, $colsGeneral = array(), $options) {
        ?>
        <?php $colsModel = $cols; ?>
        <?php $divGenTable = "$modelName-tablegen"; ?>
        <?php $class = $divGenTable . ($detail ? "-table-detail " : "-table "); ?>
        <?php $class .= ($detail ? "tablegen-table-detail" : "tablesorter tablegen-table"); ?>
        <?php $trSearchClass = ($detail ? "" : "tablegen-table-tr-search"); ?>
        <table class=" <?php echo $class; ?> .tablegen-table-div">
            <?php if ($options["title"]): ?>
                <div class="ui-widget ui-widget-content ui-corner-all <?php echo "$divGenTable-tablegen-title-div tablegen-table-title-div"; ?>">
                    <span><?php echo $modelName; ?></span>
                </div>
            <?php endif; ?>
            <?php
            if (isset($rows[0])) {
                echo "<tr>";
                foreach (array_keys($rows[0]) as $fldName) {
                    if ($fldName !== "detailData") {
                        $style = (isset($colsModel[$fldName]["style"])) ? $colsModel[$fldName]["style"] : " ";
                        $width = (isset($colsModel[$fldName]["width"])) ? ";width:" . $colsModel[$fldName]["width"] . ";" : " ";
                        $title = (isset($colsModel[$fldName]["title"])) ? $colsModel[$fldName]["title"] : $fldName;
                        $display = ((isset($colsModel[$fldName]["visible"])) and !$colsModel[$fldName]["visible"] ) ? ";display:none;" : " ";
                        echo "<th style=\"$style $display $width\">$title</th>";
                    }
                }
                echo "</tr>";
            }
            ?>
            <?php $id = 0; ?>
            <?php foreach ($rows as $row): ?>
                <tr id="<?php echo $divGenTable . "_" . $id; ?>" class="<?php echo $trSearchClass; ?>">
                    <?php $cantCols = 0; ?>
                    <?php foreach ($row as $fldNombre => $fld): ?>
                        <?php
                        if (isset($colsModel[$fldNombre]["total"])) {
                            $totales[$fldNombre] = isset($totales[$fldNombre]) ? $totales[$fldNombre] + $fld: 0;
                        }
                        if (isset($colsModel[$fldNombre]["currency"])) {
                            $fld = number_format($fld, 2);
                        } elseif (isset($colsModel[$fldNombre]["date"])) {
                            $format = isset($colsModel[$fldNombre]["format"]) ? $colsModel[$fldNombre]["format"] : "d/m/Y";
                            $fld = date($format, mystrtotime($fld));
                        }
                        $cantCols++;
                        ?>
                        <?php if (!is_array($fld)): ?>
                            <?php $display = ((isset($colsModel[$fldNombre]["visible"])) and !$colsModel[$fldNombre]["visible"]) ? " display:none " : " "; ?>
                            <?php $style = (isset($colsModel[$fldNombre]["style"])) ? $colsModel[$fldNombre]["style"] : " "; ?>
                            <td style="<?php echo "$style; $display;" ?>" class="<?php echo "$divGenTable-$fldNombre-td" ?>"><?php echo $fld ?></td>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </tr>
                <?php if (isset($row["detailData"])): ?>
                    <?php foreach ($row["detailData"] as $modelName => $detailData): ?>
                        <?php if (count($detailData) > 0): ?>
                            <?php $cols = isset($colsGeneral[$modelName]) ? $colsGeneral["$modelName"] : array(); ?>
                            <tr class="tablegen-table-detail-td <?php echo $divGenTable . "_" . $id; ?>">
                                <td colspan="<?php echo $cantCols - 1; ?>">
                                    <div class="ui-widget ui-widget-content ui-corner-all <?php echo $class . "-div"; ?>">
                                        <?php //vd($cols);;?>
                                        <?php renderTable($detailData, $modelName, $cols, true, null, $options); ?>
                                    </div>
                                </td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
                <?php $id++; ?>
            <?php endforeach; ?>

            <?php if (isset($rows[0])): ?>
                <tr>
                    <?php foreach (array_keys($rows[0]) as $fldName): ?>
                            <?php $display = ((isset($colsModel[$fldName]["visible"])) and !$colsModel[$fldName]["visible"]) ? " display:none " : " "; ?>
                            <?php $style = (isset($colsModel[$fldName]["style"])) ? $colsModel[$fldName]["style"] : " "; ?>
                        <?php if (isset($colsModel[$fldName]["total"])): ?>
                            <td style="<?php echo "$style; $display;" ?>"><?php echo number_format($totales[$fldName],2); ?></td>
                        <?php else: ?>
                            <td style="<?php echo "$style; $display;" ?>"></td>
                        <?php endif; ?>
                    <?php endforeach ?>
                </tr>
            <?php endif ?>
                            
        </table>
        <?php
    }
    ?>
</div>
<script type="text/javascript">
    var timeout = null;

    $("#<?php echo $divGenTable; ?> .tablegen-select").chosen();

    $(function() {
        $("#<?php echo $divGenTable; ?> .tablegen-date").datepicker();
<?php if ($options["filter"]): ?>
            initTableGenFilter();
            $("#tablegenquickfilter").focus();
<?php endif; ?>
    });

    function initTableGenFilter() {
        $("#tablegenquickfilter").val("");
        $(".indexColumn").remove();
        $(".tablesorter .tablegen-table-tr-search").each(function() {
            var t = $(this).text().toLowerCase();
            $("<td class='indexColumn'></td>")
                    .hide().text(t).appendTo(this);
        });
        $("#tablegenquickfilter").keyup(function() {
            if (timeout) {
                clearTimeout(timeout);
            }
            timeout = setTimeout(function(o) {
                filtra(o);
            }, 400, $(this));
        });

        function filtra(o) {
            var s = o.val().toLowerCase().split(" ");
            $(".tablesorter tr:hidden").show();
            $.each(s, function() {
                $tr = $(".tablesorter .tablegen-table-tr-search .indexColumn:not(:contains('"
                        + this + "'))").parent();
                console.log($tr);
                $tr.each(function() {
                    $("." + $(this).attr("id")).hide();
                });
                $tr.hide();
            });
        }

    }

</script>