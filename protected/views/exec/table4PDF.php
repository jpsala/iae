<?php $divGenTable = "$modelName-tablegen"; ?>
<style type="text/css">
    .tablegen-table-div {margin-left: 10px}
    .tablegen-table-title-div {padding:5px;background-color: #EEEEFF;background-image: none;color: #FF0000;
    }
    #tablegenquickfilter-div{padding:5px; margin-bottom: 5px}
</style>

<?php
renderTable($data, $modelName, $cols);

function renderTable($rows, $modelName, $cols, $detail = false) {
    ?>
    <?php $colsModel = isset($cols[$modelName]) ? $cols[$modelName] : array(); ?>
    <?php $divGenTable = "$modelName-tablegen"; ?>
    <?php $class = $divGenTable . ($detail ? "-table-detail " : "-table "); ?>
    <?php $class .= ($detail ? "tablegen-table-detail" : "tablesorter tablegen-table"); ?>
    <?php $trSearchClass = ($detail ? "" : "tablegen-table-tr-search"); ?>
    <table class=" <?php echo $class; ?> .tablegen-table-div">
        <div class="ui-widget ui-widget-content ui-corner-all <?php echo "$divGenTable-tablegen-title-div tablegen-table-title-div"; ?>">
            <span><?php echo $modelName; ?></span>
        </div>
        <?php
        if (isset($rows[0])) {
            echo "<tr>";
            foreach (array_keys($rows[0]) as $fldName) {
                if ($fldName == "detailData")
                    continue;
                if (isset($colsModel[$fldName]["hidden"]) and $colsModel[$fldName]["hidden"]) {
                    continue;
                }
                $title = (isset($colsModel[$fldName]["title"])) ? $colsModel[$fldName]["title"] : $fldName;
                $classth = "$divGenTable-$fldName-th";
                echo "<th class=\"$classth\">$title</th>";
            }
            echo "</tr>";
        }
        ?>
        <?php $id = 0; ?>
        <?php foreach ($rows as $row): ?>
            <tr id="<?php echo $divGenTable . "_" . $id; ?>" class="<?php echo $trSearchClass; ?>">
                <?php $cantCols = 0; ?>
                <?php foreach ($row as $fldNombre => $fld): ?>
                    <?php
                    if (isset($colsModel[$fldNombre]["hidden"]) and $colsModel[$fldNombre]["hidden"]) {
                        continue;
                    }
                    ?>
                    <?php
                    if (isset($colsModel[$fldNombre]["date"])) {
                        $format = isset($colsModel[$fldNombre]["format"]) ? $colsModel[$fldNombre]["format"] : "d/m/Y";
                        $fld = date($format, mystrtotime($fld));
                    }
                    $cantCols++;
                    ?>
                    <?php if (!is_array($fld)): ?>
                        <td class="<?php echo "$divGenTable-$fldNombre-td" ?>"><?php echo $fld ?></td>
                    <?php endif; ?>
            <?php endforeach; ?>
            </tr>
            <?php if (isset($row["detailData"])): ?>
                <?php foreach ($row["detailData"] as $modelName => $detailData): ?>
                <?php if (count($detailData) > 0): ?>
                        <tr class="tablegen-table-detail-td <?php echo $divGenTable . "_" . $id; ?>">
                            <td colspan="<?php echo $cantCols - 1; ?>">
                                <div class="ui-widget ui-widget-content ui-corner-all <?php echo $class . "-div"; ?>">
                                    <?php //vd($modelName);; ?>
                    <?php renderTable($detailData, $modelName, $cols, true); ?>
                                </div>
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
            <?php $id++; ?>
    <?php endforeach; ?>
    </table>
    <?php
}
?>
