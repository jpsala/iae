<style type="text/css">
  .importe{text-align:right;}
  .negrita{font-weight: bold}
</style>
<?php $totalRecaudado = 0; ?>
<?php
$tarjetaAnt = "";
$totalTajeta = 0;
//$totalSaldo = 0;
$totalCheque = 0;
$totalEfectivo = isset($saldos["0"]) ? $saldos["0"] : 0;
$totalSaldoGeneral = 0;
foreach ($qrys as $tipoValorNombre => $qry):
  ?>

  <table>
    <tr>
      <td>Tipo <?php echo $tipoValorNombre; ?></td>
    </tr>
    <tr><td>
        <table>
          <tr>
            <th>Comprob.</th>
            <th>Fecha</th>
            <th>Detalle</th>
            <th class="importe">Importe</th>
            <th class="importe">Saldo</th>
          </tr>
          <tr>
            <th>Saldo anterior</th>
            <th></th>
            <th></th>
            <th class="importe"></th>
            <th class="importe"><?php  echo  number_format($saldosAnt["$tipoValorNombre"], 2); ?></th>
            <?php $totalSaldo = 0; ?>
            <?php $totalSaldo = $saldosAnt["$tipoValorNombre"]; ?>
            <?php $totales[$tipoValorNombre] = 0; ?>
            <?php $totalRecaudado = 0; ?>
            <?php $totalDepSalida = 0; ?>
            <?php $totalDepEntrada = 0; ?>
          </tr>
          <?php foreach ($qry as $row): ?>
            
            <?php $tipo_valor_id = $row["valor_tipo"]; ?>
            <?php $totales[$tipoValorNombre] += $row["importe"]; ?>
            <?php if ($row["comprob_id"] == Comprob::RECIBO_VENTAS): ?>
              <?php $totalRecaudado += $row["importe"]; ?>
            <?php elseif ($row["comprob_id"] == Comprob::DEPOSITO_BANCARIO_SALIDA): ?>
              <?php $totalDepSalida += $row["importe"]; ?>
            <?php elseif ($row["comprob_id"] == Comprob::DEPOSITO_BANCARIO_ENTRADA): ?>
              <?php $totalDepEntrada += $row["importe"]; ?>
            <?php else: ?>
            
          
              <tr><?php $totalSaldo += $row["importe"]; ?></tr>  
              <tr>
                <?php
                if ($tipo_valor_id == 1) {
                  $totalCheque += $row["importe"];
                } else if ($tipo_valor_id == 0) {
                  $totalEfectivo += $row["importe"];
                }
                ?>
                <td>
                  <?php
                  echo $row["comprob_nombre"] . " " . $row["punto_venta_numero"] . "-"
                  . str_pad($row["numero"], 8, "0", STR_PAD_LEFT);
                  ?>
                </td>
                <td class="fecha">
                  <?php echo date("d/m/Y H:i", mystrtotime($row["fecha_creacion"])); ?>
                </td>
                <td>
                  <?php
                  echo $row["pago_a_cuenta_detalle"];
                  echo $row["obs"] ? $row["obs"] : ($row["doc_detalle"]?$row["doc_detalle"]:$row["socio_nombre"]);
//                  if ($tipo_valor_id == 1):
//                    echo "  Chq.#" . $row["valor_numero"];
//                  elseif ($tipo_valor_id == 2):
//                    echo " Cupón#" . $row["valor_numero"] . "";
//                  endif;
                  ?>
                </td>
                <td class="importe">
                  <?php
                   echo $row["comprob_signo"] == '-1' ? '(' . number_format($row["importe"], 2) . ')' : number_format($row["importe"], 2);
                   //$totalSaldo += $row["importe"];
                  ?>
                </td>
                <td class="importe"><?php echo number_format($totalSaldo, 2); ?></td>
              </tr>
            
            <?php endif; ?>
            <?php
            if ($row["comprob_id"] == Comprob::RECIBO_VENTAS) {
             // $totalRecaudado += $row["importe"];
            }
            ?>
          <?php endforeach; ?>
          <?php if ($totalRecaudado): ?>
            <tr>
              <td colspan="2"></td>
              <td colspan='1' class="negrita">Total Recaudación</td>
              <td class='importe'><?php echo number_format($totalRecaudado , 2, ".", ","); ?></td>
            </tr>
          <?php endif; ?>

          
          <?php if ($totalDepSalida): ?>
            <tr>
              <td colspan="2"></td>
              <td colspan='1' class="negrita">Total Depósitos bancarios salida</td>
              <td class='importe'><?php echo number_format($totalDepSalida, 2, ".", ","); ?></td>
            </tr>
          <?php endif; ?>
          <?php if ($totalDepEntrada): ?>
            <tr>
              <td colspan="2"></td>
              <td colspan='1' class="negrita">Total Depósitos bancarios entrada</td>
              <td class='importe'><?php echo number_format($totalDepEntrada, 2, ".", ","); ?></td>
            </tr>
          <?php endif; ?>
          <?php
          echo "<tr>
            <td colspan='3'>Total</td>
            <td class='importe negrita'>" . number_format($totales[$tipoValorNombre], 2) . "</td>
            <td class='importe negrita'>" . number_format($totalSaldo + $totalRecaudado + $totalDepSalida , 2) . "</td>
          </tr>";
          ?>
          <?php $totalSaldoGeneral += ($totalSaldo + $totalRecaudado + $totalDepSalida) ;?>  
          <?php $totalRecaudado = 0;  $totalSaldo=0; $totales[$tipoValorNombre] = 0;  ?>
        </table>
      </td></tr>
    <?php; ?>
  </table>
  <?php
endforeach;
?>

<table>
  <tr>
    <td>TOTAL GENERAL</td><td class='importe negrita'><?php echo number_format($totalSaldoGeneral, 2); ?></td>
  </tr>
</table>
