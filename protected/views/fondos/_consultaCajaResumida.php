<div id="caja-ant">
  <div id="caja-div-header" class="ui-widget ui-widget-content ui-corner-all">
    Saldo Apertura
  </div>
  <div id="caja-div" class="ui-widget ui-widget-content ui-corner-all">
    <table id="caja-resumen">
      <tr>
        <th>Tipo</th>
        <th class="importe-td">Importe</th>
      </tr>
      <?php foreach ($rowsSaldo as $row): ?>
        <tr>
          <td> <?php echo $row["nombre"];                  ?></td>
          <td class="importe-td"><?php echo number_format($row["importe"],2); ?></td>
        </tr>
      <?php endforeach; ?>
    </table>
  </div>
</div>

<div id="caja-actual">
  <div id="caja-div-header" class="ui-widget ui-widget-content ui-corner-all">
    Saldo Actual
  </div>
  <div id="caja-div" class="ui-widget ui-widget-content ui-corner-all">
    <table id="caja-resumen">
      <tr>
        <th>Tipo</th>
        <th class="importe-td">Importe</th>
      </tr>
      <?php foreach ($rowsSaldoActual as $row): ?>
        <tr>
          <td><?php echo $row["nombre"];     ?></td>
          <td class="importe-td"><?php echo number_format($row["importe"],2); ?></td>
        </tr>
      <?php endforeach; ?>
    </table>
  </div>
</div>