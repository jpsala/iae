<script type="text/javascript">
  var dates = <?php echo $fechas; ?>;
  var urlImpresion = "<?php echo $this->createUrl("/fondos/ResumenCaja"); ?>";
  function imprime() {
        window.open(urlImpresion+"&instancia_id=" + instancia_id);
        //alert('imprime');
      }
</script>

<select name="instancia-select" id="instancia-select">
  <?php foreach ($instancias as $i): ?>
    <option value="<?php echo $i["id"] ?>">
      <?php
      $total = (number_format($i["total"], 3));
      echo date("d/m/y h:i:s", mystrtotime($i["fecha_apertura"]))
      . " - "
      . ($i["fecha_cierre"] ? date("d/m/y h:i:s", mystrtotime($i["fecha_cierre"])) : " abierta ");
      ?>
    </option>
  <?php endforeach; ?>
</select>

<button class="imprime" onclick="imprime();return false;" id="imprime" name="imprime">Imprime</button>
<button class="" onclick="changeInstancia();return false;" id="desglosar" name="desglosar">Desglosar Recaudación</button>
