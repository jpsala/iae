<form method="POST">
    <?php if ($abierta): ?>
        Caja abierta
    <?php else: ?>
        <?php if ($confirmado): ?>
            La caja fué cerrada
        <?php else: ?>
            Está seguro de cerrar la caja? 
            <input type="submit" name="cierra-caja" value="SI !"/>
        <?php endif; ?>
    <?php endif; ?>
</form>
