<style type="text/css">
  #caja-select{width:150px}
  #caja-fecha{margin-top: 2px; vertical-align: top; width: 77px; margin-left: 15px;}
  #instancia-select{width:300px}
  #caja-resumen{width:100%;margin-bottom: 0.4em !important;}
  #caja-div{width:200px;overflow: auto;margin-bottom: 20px; margin-top: 10px}
  .importe-td{text-align: right}
  #caja-div-header{padding: 8px}
  #caja-ant,#caja,#caja-actual{float:left;margin-left: 10px}
  #caja-detallada-div{clear: both}

  #instancias-div{margin-top:10px}
  #instancia-div{float:right}
  #cajas-div{width:345px;float:left}

</style>
<div id="cajas-div">
  <select name="caja-select" id="caja-select">
    <?php foreach ($cajas as $caja): ?>
      <option value="<?php echo $caja->id ?>"><?php echo $caja->nombre; ?></option>
    <?php endforeach; ?>
  </select>
  <input type="text" name="caja-fecha" id="caja-fecha" value="<?php echo date("d/m/Y"); ?>"/>
  <div id="instancias-div">
  </div>

</div>
<div id="instancia-div">

</div>

<div id="caja-detallada-div" class="ui-widget ui-widget-content ui-corner-all">

</div>

<script type="text/javascript">
  $("#caja-select").chosen().change(function() {
    //changeCaja($(this));
  });
  $("#caja-fecha").datepicker({
    beforeShowDay: highlightDays
  }).change(function() {
    changeFecha();
  });

  changeFecha();

  //  changeInstancia()

  function highlightDays(date) {
    for (var i = 0; i < dates.length; i++) {
      if (dates[i] == $.datepicker.formatDate('yy-mm-dd', date)) {
        return [true, 'highlight'];
      }
    }
    return [true, ''];
  }

  function changeFecha() {
    $obj = $("#caja-fecha");
    destino_id = $("#caja-select").val();
    $.ajax({
      type: "GET",
      //      dataType:"json",
      data: {fecha: $obj.val(), destino_id: destino_id},
      url: "<?php echo $this->createUrl("fondos/traeCajaInstancias"); ?>",
      success: function(data) {
        $("#instancias-div").html(data);
        $("#instancia-select").chosen().change(function() {
          changeInstancia($(this));
        });
        changeInstancia($(this));
      },
      error: function(data, status) {
      }
    });

  }

  function changeInstancia() {
    var $obj = $("#instancia-select");
    instancia_id = $obj.val();
    $.ajax({
      type: "GET",
      //      dataType:"json",
      data: {instancia_id: instancia_id},
      url: "<?php echo $this->createUrl("fondos/traeCajaResumida"); ?>",
      success: function(data) {
        $("#instancia-div").html(data);
        traeCajaDetallada(instancia_id);
      },
      error: function(data, status) {
      }
    });
  }

  function traeCajaDetallada(instancia_id) {
     if ($("#desglosar").html() === 'Desglosar Recaudación'){
        desglosa = true;
        $("#desglosar").html("Agrupar Recaudación") ;
    } else{
            desglosa = false;
            $("#desglosar").html("Desglosar Recaudación") ;
    }
    $.ajax({
      type: "GET",
      //      dataType:"json",
      data: {instancia_id: instancia_id, desglosa: desglosa},
      url: "<?php echo $this->createUrl("fondos/cajaDetallada"); ?>",
      success: function(data) {
        $("#caja-detallada-div").html(data);
      },
      error: function(data, status) {
      }
    });
  }
</script>
