<style type='text/css'>
  #tree{
    padding-bottom: 10px;
    padding-left: 17px;
    padding-right: 0;
    padding-top: 10px;
  }
  #concepto-dlg{display:none}
  a.jqtree_common{cursor:pointer;text-decoration: none}
  span.jqtree_common{cursor:pointer;text-decoration: none}
  #ingreso-egreso-label{vertical-align: super;}
  #concepto-a{cursor:pointer; text-decoration: none;color: #0099FF;}
  #concepto-a:hover{-moz-text-decoration-line:underline}
  #movimiento-caja-div{padding: 10px}
  #concepto-button{margin-bottom: 3px; margin-left: 4px}
  #concepto-button.ui-button span.ui-button-text{padding: 0 7px 0 7px}
  .row{margin-bottom: 7px}
  label{float: left; width: 70px; line-height: 24px;}
</style>
<link rel="stylesheet" type="text/css" href="js/jqTree/jqtree.css" media="" />

<script src="js/jqTree/tree.jquery.js"></script>


<?php if (!$instancia): ?>
  La caja está cerrada...
  <?php return; ?>
<?php endif; ?>
<?php Concepto::tree("concepto_id","concepto_ac"); ?>
<div id='movimiento-caja-div' class="ui-widget ui-widget-content ui-corner-all">
  <div class="row">
    <!--    <label id="ingreso-egreso-label" for="ingreso-egreso">Tipo de movimiento</label>
        <select id="ingreso-egreso">
          <option value=""> -Seleccione el tipo de movimiento- </option>
          <option value="I">Ingreso</option>
          <option value="E">Egreso</option>
        </select>-->
    <?php //echo $tipo == "E" ? "Entrada" : "Salida"; ?>
  </div>
  <div id="concepto-div" class="row">
    <label for="concepto_ac">Concepto</label>
    <input name="concepto_ac" id="concepto_ac" value="" placeholder=""/>
    <button id="concepto-button" onclick="return conceptosTree();">...</button>
  </div>
  <div id="importe-div" class="row">
    <label id="importe-label" for="importe">Importe:</label>
    <input type="text" id="importe" value="<?php echo $movCaja->total; ?>"/>
  </div>
</div>


<script type='text/javascript'>
      var concepto_id;
      var data = <?php echo Concepto::getTree(); ?>;
      var urlConceptoAC = "<?php echo $this->createUrl('fondos/getConceptosPaths'); ?>";
      $("#concepto-button").button();
      $('#concepto_ac')
              .focus()
              .autocomplete(
              {
                autoSelect: true,
                autoFocus: true,
                minLength: 2,
                source: urlConceptoAC,
                select: function(event, ui) {
                  concepto_id = ui.item.id;
                  $("#concepto-span").html(ui.item.label);
                  $("#importe").focus().select();
                }
              }
      );
      $(function() {
        $("#concepto-dlg").dialog({
          title: "Conceptos",
          heigth: "auto",
          autoOpen: false,
          buttons: {
            cancel: function() {
              $("#concepto-dlg").dialog("close");
            },
            ok: function() {
              $("#concepto_ac").val(concepto_path);
              $("#importe").focus().select();
              $("#concepto-dlg").dialog("close");
            }
          }
        });

        $('#tree').tree({
          data: data
        });

        $('#tree').bind(
                'tree.select',
                function(event) {
                  // The clicked node is 'event.node'
                  var node = event.node;
                  concepto_id = node.id;
                  concepto_path = node.path;
                  ingreso_egreso = node.ingreso_egreso;
                }
        );
      });

      
</script>
