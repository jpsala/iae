<?php

class MYPDF extends PDF {

  public $planilla;

  public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false) {
    parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
  }

  public function Header() {
    parent::Header();
    $this->planilla->linea = 40;
    $this->SetFont(PDF_FONT_NAME_MAIN, "", $this->planilla->fontSizeDatos);
    $this->planilla->currY = $this->planilla->margenY;
    $this->planilla->col1 = $this->planilla->margenX + 3;
    $this->planilla->col2 = $this->planilla->col1 + 50;
    $this->planilla->col3 = $this->planilla->col2 + 20;
    $this->planilla->col4 = $this->planilla->col3 + 65;
    $this->planilla->col5 = $this->planilla->col4 + 25;
    $this->planilla->col6 = $this->planilla->col5 + 25;

    $this->CreateTextBox("Informe Resumido de Caja:", $this->planilla->col1, $this->planilla->currY, null, null, 15);
    $this->CreateTextBox("Comprobante", $this->planilla->col1, $this->planilla->currY + 7, null, null);
    $this->CreateTextBox("Fecha", $this->planilla->col2, $this->planilla->currY + 7, null, null);
    $this->CreateTextBox("Detalle", $this->planilla->col3 , $this->planilla->currY + 7, null, null);
    $this->CreateTextBox("Debe", $this->planilla->col4, $this->planilla->currY + 7, null, null);
    $this->CreateTextBox("Haber", $this->planilla->col5, $this->planilla->currY + 7, null, null);
    $this->CreateTextBox("Saldo", $this->planilla->col6, $this->planilla->currY + 7, null, null);
}

  public function Footer() {
    $this->SetY(0);
    $this->SetX(0);
  }

}

class Informe extends stdClass {

  public $pdf, $margenX, $margenY;
  public $fontSize;
  public $hoja;
  public $col1, $col2, $col3, $col4, $currY, $currX;
  public $headerHeight, $anchoColumnas;
  public $linea = 40;

  public function __construct($instancia, $hoja = "A4") {
    $this->hoja = $hoja;
    $this->movimientos = DocValor::model()->ReporteCajaResumido($instancia);
    $this->SaldoAnterior = DocValor::model()->SaldoAnterior($instancia);
    $this->TotalRecaudacion = DocValor::model()->TotalRecaudacion($instancia);
    $this->TotalDepositos = DocValor::model()->TotalDepositos($instancia);
  }

  public function imprime() {
    $this->pdf = new MYPDF("P", "mm", $this->hoja);
    $this->pdf->planilla = $this;
    $this->pdf->SetCreator(PDF_CREATOR);
    $this->pdf->SetAuthor('JP');
    $this->pdf->SetTitle('Planilla Resumen Caja');
    $this->pdf->SetSubject('Planilla Resumen Caja');
    $this->pdf->SetKeywords('TCPDF, PDF, Planilla');
    $this->pdf->SetPrintHeader(true);
    $this->pdf->SetPrintFooter(false);
    $this->pdf->SetAutoPageBreak(true);
    $this->margenX = 3;
    $this->margenY = 10;
    $this->fontSize = 7;

    $this->fontSizeDatos = $this->fontSize + 5;

    $this->preparaPdf2();
    $this->pdf->Output();
  }

  private function preparaPdf2(){
      $this->pdf->AddPage();

      $saldo = 0;
      $this->pdf->CreateTextBox(' ', 5, $this->linea - 5 , null, null);
      $this->pdf->CreateTextBox(' ', 25, $this->linea - 5 , null, null);
      $this->pdf->CreateTextBox('Saldo Anterior', 75, $this->linea - 5 , null, null, $this->fontSize);
      //$this->pdf->CreateTextBox($this->SaldoAnterior, 160, $linea - 15 , 20, null,null,null,'R');
      $saldo += $this->SaldoAnterior;
      $this->pdf->CreateTextBox(number_format($saldo,2), 185, $this->linea -5, 20, null,$this->fontSize,null,'R');



      foreach ($this->movimientos as $row) {
         $this->pdf->CreateTextBox($row["comprob_nombre"] . " " . $row["punto_venta_numero"] . "-"
                  . str_pad($row["numero"], 8, "0", STR_PAD_LEFT), 5, $this->linea, null, null,$this->fontSize);
         $this->pdf->CreateTextBox(date("d/m/Y", mystrtotime($row["fecha_creacion"])), 55, $this->linea, null, null,$this->fontSize);
         $this->pdf->CreateTextBox($row["obs"] ? $row["obs"] : ($row["doc_detalle"]?$row["doc_detalle"]:$row["socio_nombre"]), 75, $this->linea, null, null,$this->fontSize);
         $saldo +=  $row["importe"];
         if ($row["importe"] > 0)
            $this->pdf->CreateTextBox($row["importe"], 135, $this->linea, 20, null,$this->fontSize,null,'R');
         else $this->pdf->CreateTextBox($row["importe"], 160, $this->linea, 20, null,$this->fontSize,null,'R');

         $this->pdf->CreateTextBox(number_format($saldo,2), 185, $this->linea, 20, null,$this->fontSize,null,'R');
         $this->linea += 5;
      }
      $this->pdf->CreateTextBox(' ', 5, $this->linea , null, null);
      $this->pdf->CreateTextBox(' ', 25, $this->linea , null, null);
      $this->pdf->CreateTextBox('TOTAL RECAUDACION', 75, $this->linea, null, null,$this->fontSize);
       if ($this->TotalRecaudacion > 0)
            $this->pdf->CreateTextBox($this->TotalRecaudacion, 135, $this->linea, 20, null,$this->fontSize,null,'R');
         else $this->pdf->CreateTextBox($this->TotalRecaudacion, 160, $this->linea, 20, null,$this->fontSize,null,'R');
      $saldo += $this->TotalRecaudacion;
      $this->pdf->CreateTextBox(number_format($saldo,2), 185, $this->linea, 20, null,$this->fontSize,null,'R');

      $this->pdf->CreateTextBox(' ', 5, $this->linea + 5, null, null);
      $this->pdf->CreateTextBox(' ', 25, $this->linea + 5, null, null);
      $this->pdf->CreateTextBox('TOTAL DEPOSITOS', 75, $this->linea + 5, null, null,$this->fontSize);
      if ($this->TotalDepositos > 0)
         $this->pdf->CreateTextBox($this->TotalDepositos, 135, $this->linea + 5, 20, null,$this->fontSize,null,'R');
      else $this->pdf->CreateTextBox($this->TotalDepositos, 160, $this->linea + 5 , 20, null,$this->fontSize,null,'R');
     $saldo += $this->TotalDepositos;
      $this->pdf->CreateTextBox(number_format($saldo,2), 185, $this->linea + 5, 20, null,$this->fontSize,null,'R');
  }


}

$i = new Informe($instancia_id, "A4");
$i->imprime();
?>
