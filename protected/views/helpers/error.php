<style type="text/css">
    .helpers-error{color:red; margin: 5px;}
    .helpers-texto{ font-size: 14px; margin: 15px}
</style>
<div class="helpers-error ui-widget ui-widget-content ui-corner-all">
    <div class="titulo"><?php echo $_GET["mensaje"]; ?></div>
    <div class="helpers-texto">
        <?php if (is_array($data)): ?>
            <?php ve($data); ?>
        <?php else: ?>
            <?php echo $data; ?>
        <?php endif; ?>
    </div>
</div>