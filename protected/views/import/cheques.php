<form action="<?php echo $this->createUrl("import/cheques"); ?>" method="POST">
    <input type="submit" value="Ok"/>
    <table>
        <?php while ($row = ibase_fetch_object($rows)): ?>
            <tr id="<?php echo $row->ID_CHQ_TERCERO; ?>">
                <td><?php echo $row->NUMERO; ?></td>
                <td><?php echo $row->ID_BANCO; ?></td>
                <td><?php echo number_format($row->IMPORTE, 2); ?></td>
                <td><?php echo $row->FEC_ACRED; ?></td>
                <td><?php echo $row->FIRMANTE; ?></td>
                <td>
                    <input type="checkbox" name="sel[<?php echo $row->ID_CHQ_TERCERO; ?>]" hecked="CHECKED"/>
                </td>
            </tr>
        <?php endwhile; ?>
    </table>
</form>
