<style type="text/css">
    #log{border: #555555 solid 1px; border-radius: 5px; padding: 5px; margin-top: 10px; min-height: 20px}
    a{display: block}
</style>
<a class="importar" href="<?php echo $this->createUrl("import/alumnos"); ?>">Alumnos</a>
<a class="importar" href="<?php echo $this->createUrl("import/docs"); ?>">Docs</a>
<div id="log">

</div>
<script type="text/javascript">
    $(".importar").click(function(e){
        var id = $(this).html();
        var url = $(this).attr("href");
        $("#log").html("Importando "+id+"...");
        $.ajax({
            type: "POST",
            url: url,
            success: function(data) {
                $("#log").html(data);
            },
            error: function(data,status){
                $("#log").html(data);
            }
        });

        return false;
    })
</script>