<style type="text/css">
    .importe{text-align: right; width:150px}
</style>
<?php
echo "Diferencias en los saldos de clientes al 
            <input id=\"fecha\" value=\"$fechaParaSaldo\"/>
            <button onclick=\"return traeSaldos();\">Ok</button><br/><br/>";
echo "<table>";
echo "<tr>";
echo "<th >Alumno</th>";
echo "<th class=\"importe\">Saldo bien?</th>";
echo "<th class=\"importe\">Saldo mal?</th>";
echo "</tr>";
while ($row = ibase_fetch_object($sth)) {
    $dif = round($row->SALDO - $row->SALDO_VIEJO, 0);
    if (abs($dif) > 1) {
        echo "<tr>";
        echo "<td>";
        echo $row->MATRICULA . " -> " . $row->NOMBRE;
        echo "</td>";
        echo "<td class=\"importe\">";
        echo number_format($row->SALDO, 2);
        echo "</td>";
        echo "<td class=\"importe\">";
        echo number_format($row->SALDO_VIEJO, 2);
        echo "</td>";
        echo "</tr>";
    }
}
echo "</table>";
?>

<script type="text/javascript">
    function traeSaldos(){
        console.log($("#fecha").val());
        window.location = "<?php echo $this->createUrl("/import/saldosClientes2");?>&fechaParaSaldo="+$("#fecha").val();
    }
    </script>