<table class="" id="tabla-asignaturas">
  <tr class="ui-widget-header">
    <th>Alumno</th>
    <th>Inasistencia</th>
    <th>detalle</th>
    <th>&nbsp</th>
    <th>&nbsp</th>
  </tr>
  <?php $row = 0; /* @var $asignatura Asignatura */ ?>
  <?php foreach ($inasistencias as $i): ?>
    <tr class="tr-item <?php echo ($row++ % 2) ? "tr-even" : ""; ?>" inasistencia_id="<?php echo $i["inasistencia_id"]; ?>" inasistencia_detalle_id="<?php echo $i["inasistencia_detalle_id"]; ?>" alumno_id="<?php echo $i["alumno_id"]; ?>">
      <td class="td-alumno"><?php echo $i["nombre_completo"]; ?></td>
      <td class="td-inasistencia"><span class="inasistencia-tipo-nombre"><?php echo $i["inasistencia_tipo_nombre"]; ?></span><select class="inasistencia-tipo-id" data-placeholder="Tipo de inasistencia"><?php echo InasistenciaTipo::getOptions($i["inasistencia_tipo_id"]);?></select></td>
      <td class="td-detalle"><?php echo $i["detalle"] ?></td>
      <td></td>
      <td class="td-borra"  onclick="return borraInasistencia(<?php echo $i["inasistencia_detalle_id"]; ?>,this)"></td> 
    </tr>
  <?php endforeach; ?>
</table>
