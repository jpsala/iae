<link rel="stylesheet" type="text/css" href="js/jqgrid/css/ui.jqgrid.css" />
<script src="js/jqgrid/js/i18n/grid.locale-es.js"></script>
<script src="js/jqgrid/js/jquery.jqGrid.min.js"></script>
<script src="js/common.js"></script>
<script type="text/javascript">
	var urlImpresion = "<?php echo $this->createUrl("inasistencia/reporte"); ?>";
	var fecha = "<?php echo $fecha_hoy ?>";
	var division_id, editandoTipoInasistencia, editandoDetalle;
	var $inasistencia_tipo_nombre_ant, inasistencia_tipo_nombre_ant_texto;
	$(function() {
		init();
	});

	function init() {

		$("#nivel-select").chosen().change(function() {
			changeNivel($(this));
		});

		$("#anio-select").chosen().change(function() {
			changeAnio($(this));
		});

		$("#division-select").chosen().change(function() {
			changeDivision($(this));
		});

		$(".button, button").button();

		$("#nivel-select_chzn").mousedown();

		$("#fecha").datepicker({
			onSelect: function() {
				$("#ui-datepicker-div").hide();
				fecha = $(this).val();
				$("#div-datos").html("");
				traeItems();
			}
		});

	}

	function creaAlumnoAutoComplete() {
		$('#alumno_ac').focus().autocomplete({autoSelect: true, autoFocus: true, minLength: 2, source: urlAlumnoAC,
			select: function(event, ui) {
				alumno_id = ui.item.id;
				traeItems();
			}
		});
	}

	function creaChosenPeriodos() {
		$.ajax({
			type: "GET",
			url: "<?php echo $this->createUrl("logicaPeriodo/options"); ?>",
			success: function(data) {
				$("#periodo-select").html(data);
				logica_periodo_id = $("#periodo-select").val();
				$("#periodo-select").chosen().change(function() {
					logica_periodo_id = $(this).val();
					traeItems();
				});
			},
			error: function(data, status) {
				division_asignatura_id = null;
			}
		});
	}

	function changeNivel($this) {
		$("#div-datos").html("");
		$.ajax({
			type: "GET",
			//dataType: "json",
			data: {nivel_id: $this.val()},
			url: "<?php echo $this->createUrl("anio/options"); ?>",
			success: function(data) {
				$("#anio-select").html(data);
				$("#anio-select").trigger("liszt:updated");
				$("#anio-select_chzn").mousedown();

			},
			error: function(data, status) {
			}
		});
		return false;
	}

	function changeAnio($this) {
		$("#div-datos").html("");
		$.ajax({
			type: "GET",
			//dataType: "json",
			data: {anio_id: $this.val()},
			url: "<?php echo $this->createUrl("division/options"); ?>",
			success: function(data) {
				$("#division-select").html(data);
				$("#division-select").trigger("liszt:updated");
				$("#division-select_chzn").mousedown();

			},
			error: function(data, status) {
			}
		});
		return false;

	}

	function changeDivision($this) {
		//$("#periodo-select_chzn").mousedown();
		division_id = $this.val();
		//$("#fecha").datepicker("show");
		$("#div-datos").html("");
		traeItems();
		return false;

	}

	function borraItem(itemId) {
		if (!confirm("Está seguro de borrar este registro?")) {
			return;
		}
		$.ajax({
			type: "POST",
			//      dataType:"json",
			data: {item_id: itemId},
			url: "<?php echo $this->createUrl("conducta/borraItem"); ?>",
			success: function(data) {
				$("#tabla-asignaturas").find("#" + itemId).remove();
			},
			error: function(data, status) {
			}
		});
		return false;
	}

	function borraInasistencia(itemId, td) {
		if (!confirm("Está seguro de borrar este registro?")) {
			return;
		}
		var $tr = $(td).closest("tr");
		$.ajax({
			type: "POST",
//      dataType:"json",
			data: {item_id: itemId},
			url: "<?php echo $this->createUrl("inasistenciaXAlumno/borraItem"); ?>",
			success: function(data) {
				$(".tr-item").removeClass(".td-inasistencia");
				$tr.attr("inasistencia_detalle_id", "");
				$tr.attr("inasistencia_id", "");

				$tr.find(".inasistencia-tipo-nombre").html("");
				$tr.find(".td-detalle").html("");

				//$("#tabla-asignaturas").find(".td-inasistencia").hide()
			},
			error: function(data, status) {
			}
		});
		return false;
	}

	function traeItems() {
		if (!fecha || !division_id) {
			return;
		}
		$.ajax({
			type: "GET",
			//      dataType:"json",
			data: {
				fecha: fecha,
				division_id: division_id
			},
			url: "<?php echo $this->createUrl("inasistencia/traeItems"); ?>",
			success: function(data) {
				$("#div-datos").html(data);
				$(".tr-item").click(function() {
					$(".tr-item").removeClass("selected-tr");
					$(this).addClass("selected-tr")
				})
				$(".td-inasistencia").click(function() {
					editaInasistenciaTipo($(this));
				})
				$(".td-detalle").click(function() {
					editaDetalle($(this));
				})
				$("#impresion_div").show().find("button").button().click(function() {
					window.location = urlImpresion + "&division_id=" + division_id + "&fecha=" + fecha;
				});
			},
			error: function(data, status) {
			}
		});
	}

	function editaDetalle($this) {
		var $tr = $this.closest("tr"), alu = $tr.attr("alumno_id");
		var tipo = $tr.find(".inasistencia-tipo-nombre").html();
		if (editandoDetalle || !tipo) {
			return;
		}
		if (editandoTipoInasistencia) {
			$("#tabla-asignaturas").find(".chzn-container").hide()
			$(".inasistencia-tipo-nombre").css("display", "").show();
			editandoTipoInasistencia = false;
		}
		editandoDetalle = true;
		val = $this.html();
		$this.html("<input type='text' id='detalle_id'/>");
		$this.find("#detalle_id").val(val).focus().change(function() {
			cambiaDetalle($this, $(this).val());
		}).blur(function() {
			cambiaDetalle($this, $(this).val());
		});
	}

	function cambiaDetalle($td_detalle, val) {
		$td_detalle.html(val);
		grabaItem($td_detalle.parent());
		editandoDetalle = false;
	}

	function editaInasistenciaTipo($this) {
		var $tr = $this.closest("tr"), $chosen, nuevoTipoNombre;
		var alu = $tr.attr("alumno_id");
		var $inasistencia_tipo_nombre = $this.find(".inasistencia-tipo-nombre");
		if (editandoTipoInasistencia == alu || editandoDetalle) {
			return;
		}
		$(".inasistencia-tipo-nombre").css("display", "").show();
		editandoTipoInasistencia = alu;
		$("#tabla-asignaturas").find(".chzn-container").hide();
		$(".inasistencia-tipo-id").hide();
		$chosen = $this.parent().find(".chzn-container");
		$inasistencia_tipo_nombre.hide();
		if ($chosen.length) {
			$chosen.show();
		} else {
			$this.find(".inasistencia-tipo-id").show();
			$this.find(".inasistencia-tipo-id").chosen().change(function() {
				nuevoTipoNombre = $this.find(".inasistencia-tipo-id option:selected").text();
				$inasistencia_tipo_nombre.html(nuevoTipoNombre).show();
				grabaItem($tr);
				setTimeout(function() {
					editandoTipoInasistencia = null;
					$tr.find(".chzn-container").hide();
				}, 0)
			})
		}
		$inasistencia_tipo_nombre_ant = $inasistencia_tipo_nombre;
		inasistencia_tipo_nombre_ant_texto = $inasistencia_tipo_nombre.html();
	}

	function grabaItem($tr) {
		var inasistencia_detalle_id = $tr.attr("inasistencia_detalle_id");
		var detalle = $tr.find(".td-detalle").html();
		var tipo_id = $tr.find(".inasistencia-tipo-id").val();
		var division_id = $("#division-select").val();
		var alumno_id = $tr.attr("alumno_id"), param;
		$.ajax({
			type: "GET",
			dataType: "json",
			data: {
				inasistencia_detalle_id: inasistencia_detalle_id,
				tipo_id: tipo_id,
				detalle: detalle,
				alumno_id: alumno_id,
				division_id: division_id,
				fecha: $("#fecha").val()
			},
			url: "<?php echo $this->createUrl("inasistencia/graba"); ?>",
			success: function(data) {
				if (data.estado !== "ok") {
					alert(data.error);
					return;
				}
				$tr.attr("inasistencia_detalle_id", data.id);
				param = "return borraInasistencia(" + data.id + ",this)";
				$tr.find(".td-borra").attr("onclick", param);
			},
			error: function(data, status) {
			}
		});
	}

	function verInasistencias() {
		if (!division_id) {
			alert("Seleccione primero una división");
			return;
		}
		window.open(" <?php echo $this->createUrl("/inasistencia/verInasistencias"); ?>&division_id=" + division_id);
	}
</script>
