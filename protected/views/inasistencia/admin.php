<head>
    <?php include("admin.css.php"); ?>
    <?php include("admin.js.php"); ?>    
</head>
<form id="main-form" name="main" method="POST" action="#">
    <div id="pedido-de-datos">
        <div class="row">
            <select id="nivel-select" data-placeholder="Nivel" class="chzn-select">
                <?php
                $x = array('prompt' => '', "style" => "width:200");
                echo CHtml::listOptions(null, CHtml::listData(Nivel::model()->findAll(), 'id', 'nombre'), $x)
                ?>
            </select>
        </div>
        <div class="row">
            <select id="anio-select" data-placeholder="Año" class="chzn-select">
                <?php
                $x = array('prompt' => '');
                echo CHtml::listOptions(null, CHtml::listData(Nivel::model()->findAll("id=-1"), 'id', 'nombre'), $x)
                ?>
            </select>
        </div>
        <div class="row">
            <select id="division-select" data-placeholder="Division" class="chzn-select">
                <?php
                $x = array('prompt' => '');
                echo CHtml::listOptions(null, CHtml::listData(Division::model()->findAll("id=-1"), 'id', 'nombre'), $x)
                ?>
            </select>
        </div>
        <!--    <div class="row">
              <select id="periodo-select" data-placeholder="Período" class="chzn-select">
        <?php
//        $x = array('prompt' => '');
//        echo CHtml::listOptions(null, CHtml::listData(LogicaPeriodo::model()->findAll("id=-1"), 'id', 'nombre'), $x)
        ?>
              </select>
            </div>-->

        <div class="row fecha">
            <input type="text" name="fecha" id="fecha" value="<?php echo $fecha_hoy; ?>"/>
        </div>
        <button id="ver" onclick="verInasistencias(); return false">Ver todas</button>

    </div>

    <div id="errores" class="validation"></div>
    <div id="div-datos" class="ui-widget-content ui-corner-all">
    </div>
</form>
<div id="impresion_div">
    <button>Imprimir</button>
</div>


