<?php
$this->renderPartial("/inasistencia/cargaInasistencia.css");

$paramNivel = new ParamNivel(array(
    "controller" => $this,
    "onchange" => "nivelChange",
        ));
$paramAnio = new ParamAnio(array(
    "controller" => $this,
        ));
$paramDivision = new ParamDivision(array(
    "controller" => $this,
    "onchange" => "divisionChange",
        ));

$paramNivel->render();
$paramAnio->render();
$paramDivision->render();
?>

<div id="periodo-select-div">
  <select id="periodo-select" data-placeholder="Período" class="chzn-select">
    <?php
    $x = array('prompt' => '');
    echo CHtml::listOptions(null, CHtml::listData(LogicaPeriodo::model()->findAll("id=-1"), 'id', 'nombre'), $x)
    ?>
  </select>
</div>
<div id="top-botton-div">
  <button class="top-botton ok"
          onclick="return submitInasistencia();
              return false;" 
          type="button"
          name="ok" 
          id="ok">graba</button>
  <button class="top-botton" type="button" name="cancela" id="cancela" 
          onclick="cancelaInasistencia();
              return false;">Cancela</button>

  <button class="" onclick="imprime();
              return false;" id="imprime">Imprime</button>
</div>
<div id="div-para-inasistencias"></div>

<script type="text/javascript">
            var nivel_id = null

            $(function() {

              $(".top-botton").attr("DISABLED", "DISABLED");
              $("button").button();
//              $(".ok").click(function() {
//                $("form").submit();
//              });

              updChosenPeriodos();
              $("#periodo-select").chosen().change(function() {
                periodoChange($(this));
              });
              $("#imprime").button("disable");
            });
            function nivelChange($nivel) {
              nivel_id = $nivel.val();
              updChosenPeriodos();
            }

            function periodoChange($periodo) {
              if (!division_id) {
                alert("seleccione una division");
                return;
              }
              logica_periodo_id = $periodo.val();
              traeInasistencia(division_id);
            }

            function divisionChange() {
              updChosenPeriodos(true);
              //$("#periodo-select").trigger(change);
            }

            function updChosenPeriodos(openAfterUpdate) {
              $.ajax({
                type: "GET",
                url: "<?php echo $this->createUrl('logicaPeriodo/optionsInasistenciaDesdeNivel'); ?>&nivel_id=" + nivel_id,
                success: function(data) {
                  $("#periodo-select").html(data);
                  $("#periodo-select").trigger("liszt:updated");
                  if(openAfterUpdate){
                    $("#periodo_select_chzn").mousedown();                  }
                },
                error: function(data, status) {
                }
              });
            }

            function traeInasistencia() {
              $.ajax({
                type: "GET",
                data: {division_id: division_id, logica_periodo_id: logica_periodo_id},
                url: "<?php echo $this->createUrl("inasistencia/cargaInasistenciaAjax"); ?>",
                success: function(data) {
                  $("#div-para-inasistencias").html(data);
                  afterTraeInasistencia();
                },
                error: function(data, status) {
                }
              });
            }

            function afterTraeInasistencia() {
            console.log(1);
              $("#top-botton-div button").button("disable");
              $(".inasistencia-input:not(:disabled):not(:hidden)").first().focus();
            }

            function cancelaInasistencia() {
              traeInasistencia();
            }

            function toggle(onoff) {
              enabled = onoff == "on" ? true : false;
              if (!enabled) {
                $(".top-botton").button("enable");
              } else {
                $(".top-botton").button("disable");
              }
              $("#nivel-select").attr('disabled', !enabled).trigger("liszt:updated");
              $("#anio-select").attr('disabled', !enabled).trigger("liszt:updated");
              $("#division-select").attr('disabled', !enabled).trigger("liszt:updated");
              $("#asignatura-select").attr('disabled', !enabled).trigger("liszt:updated");
              $("#periodo-select").attr('disabled', !enabled).trigger("liszt:updated");
            }

            function imprime() {
              window.open(urlImpresion + "&division_asignatura_id=" + division_asignatura_id + "&impresion=1");
            }

            function inasistenciaChange($inasistencia) {
              $(".top-botton").button("enable");
            }

            function submitInasistencia() {
              $.ajax({
                type: "POST",
                data: $("form").serialize() + "&division_id=" + division_id,
                url: "<?php echo $this->createUrl("/inasistencia/cargaInasistenciaGraba"); ?>",
                success: function(data) {
                  if (data.errores) {
                    muestraErrores(data.errores);
                  }
                  //console.log(data);
                  //window.location = window.location;
                },
                error: function(data, status) {
                }
              });
              return false;
            }
</script>

