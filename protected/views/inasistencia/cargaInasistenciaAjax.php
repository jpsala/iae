<?php
$this->renderPartial("/inasistencia/cargaInasistencia.css");
if (count($inasistencias) == 0) {
  die;
}
//var_dump($inasistencias);die;
?>
<form id="main-form" action="#">
  <table id="inasistencia-table">
    <?php $head = false; ?>
    <?php foreach ($inasistencias as $alumnoNombre => $data): ?>
      <?php if (!$head): ?>
        <tr>
          <th>Alumno</th>
          <?php foreach (array_keys($data) as $pn): ?>
            <th class="inasistencia-th">
              <span class="inasistencia-span"><?php echo $pn; ?></span>
            </th>
          <?php endforeach; ?>
          <?php $head = true; ?>
        </tr>
      <?php endif; ?>
      <tr>
        <td class="alumno-nombre-td"><?php echo $alumnoNombre; ?></td>
        <?php foreach ($data as $td): ?>
          <?php if ($td["logica_periodo_id"] == $logica_periodo_id): ?>
            <td class="inasistencia-td"><input 
              <?php //$cnd = $td["inasistencia_id"] ? "[".$td["inasistencia_id"]."]" : "[]"?>
                name="inasistencia[<?php echo $td["alumno_id"]; ?>][<?php echo $td["logica_periodo_id"]; ?>:<?php echo $td["inasistencia_id"]; ?>]"
                onchange="inasistenciaChange($(this));
                    return false;"
                type="text" class="inasistencia-input" 
                value="<?php echo $td["cantidad"]; ?>"/>
            </td>
          <?php else: ?>
            <td class="inasistencia-td"><input disabled="disabled" type="text" class="inasistencia-input" value="<?php echo $td["cantidad"]; ?>"/></td>
            <?php endif; ?>
          <?php endforeach; ?>
        <?php endforeach; ?>
    <tr>
  </table>
</form>