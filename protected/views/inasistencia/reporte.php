<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="language" content="<?= Yii::app()->language; ?>" />     
    <link rel="stylesheet" type="text/css" href="css/screen.css" media="screen, projection" />
    <link rel="stylesheet" type="text/css" href="css/print.css" media="print" />
    <link rel="stylesheet" type="text/css" href="css/main.css" />
    <link rel="stylesheet" type="text/css" href="css/form.css" />
    <style type="text/css">
      .nombre{width: 300px;text-align: left;margin-left: 0}
      .tipo{width: 150px}
      tr td{background-color: white}
      th{font-weight:bolder}
      th{border-bottom: 1px black solid;bgcolor: #EEE}
      table{width: 100%;background-color: #eee;  border-spacing:0;border-collapse:collapse;}
      td.detalle{ width: 100%}

    </style>
  </head>
  <body style="border:0; margin: 0;">
    <table class="table">
      <tr class="th">
        <th class="nombre">Nombre</th>
        <th>Tipo</th>
        <th class="detalle">Detalle</th>
      </tr>
      <?php foreach ($inasistencias as $i): ?>
        <tr>
          <td class="nombre"><?php echo $i["nombre_completo"]; ?></td>
          <td class="tipo"><?php echo $i["inasistencia_tipo_nombre"]; ?></td>
          <td><?php echo $i["detalle"]; ?></td>
        </tr>

      <?php endforeach; ?>
    </table>
  </body>
</html>