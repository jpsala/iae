<style type="text/css">
    .inasistencia-detalle{width:200px}
    .inasistencia-total{text-align: right}
    td.linea > div { width: 100%; height: 100%; overflow:hidden; }
    td.linea { height: 1px; }
</style>
<table id="inasistencias-table">
    <tr>
        <th>Alumno</th>
        <th>Fecha</th>
        <th>Tipo</th>
        <th>Valor</th>
        <th class="inasistencia-detalle">Detalle</th>
        <th class="inasistencia-total">Total alumno</th>
    </tr>
    <?php
    $alumno_ant =    null;
    $primeraVez = true;
    ?>
    <?php foreach ($inasistencias as $i): ?>
        <tr>
            <?php
            if ($alumno_ant !== $i["alumno"]) {
                $total = 0;
                $alumno_ant = $i["alumno"];
                echo "<td>".$i["alumno"]."</td>";
            }else{
                echo "<td></td>";
            }
            ?>
            <?php $total += $i["valor"]; ?>
            <td><?php echo date("Y/m/d", mystrtotime($i["fecha"])); ?></td>
            <td><?php echo $i["tipo"]; ?></td>
            <td><?php echo $i["valor"]; ?></td>
            <td class="inasistencia-detalle"><?php echo $i["detalle"]; ?></td>
            <td class="inasistencia-total"><?php echo $total; ?></td>
        </tr>
    <?php endforeach; ?>
</table>