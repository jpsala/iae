
<script src="js/jqgrid/js/i18n/grid.locale-es.js"></script>
<script src="js/jqgrid/js/jquery.jqGrid.min.js"></script>
<script src="js/common.js"></script>
<script type="text/javascript">
  var alumno_id = null;
  var urlImpresion = "<?php echo $this->createUrl("inasistenciaXAlumno/ReporteDetallado"); ?>";
  var urlAlumnoAC = "<?php echo $this->createUrl('alumno/autoComplete'); ?>";
  var fecha = null;
  var logica_periodo_id = null;
  var inasistencia_tipo_id = null;
  var inasistencia_tipo_nombre = null;
  var $grid;
  var itemId;
  var fecha_hoy = "<?php echo $fecha_hoy ?>";
  var $itemTemplate = $('<tr id="-1" class="tr-item "></td><td class="td-tipo-inasistencia"></td><td class="td-detalle"></td><td></td><td onclick="return borraitem(-1)" class="td-borra"></td></tr>');
  $(function(){
        
    init();
        
  });
    
  function init(){
        
    creaAlumnoAutoComplete();

    $(".button").button();
        
     $("#fecha").datepicker({
      onSelect:function(){
        traeItems();
      }
    });
    
    $("#impresion_div").show().find("button").button().click(function(){
          window.location = urlImpresion;
        });
  }
    
  function creaAlumnoAutoComplete(){
    $('#alumno_ac').focus().autocomplete({autoSelect: true,autoFocus: true, minLength: 2,source:urlAlumnoAC,
      select: function( event, ui ) {
        alumno_id = ui.item.id;
        //traeItems();
      }
    });
  }

  
  function creaChosenTiposDeInasistencia(){
    $("#inasistencia-tipo-id").chosen().change(function(){
      inasistencia_tipo_id = $(this).val();
      inasistencia_tipo_nombre = $(this).find("option:selected").text();
    });
  }
  

  function traeItems(){
    if(!alumno_id){
      return;
    }
    $.ajax({
      type: "GET",
      data: {"alumno_id":alumno_id},
      url: "<?php echo $this->createUrl("inasistenciaXAlumno/traeItems"); ?>",
      success: function(data) {
        $("#errores").hide();
        $("#div-datos").html(data);
        $(".tr-item").find("td").not(".td-borra").click(function(){
          seleccionaItem($(this));
        });
        $("#fecha-item").datepicker().val(fecha_hoy);
        $("#cantidad-item").val(1);
        creaChosenTiposDeInasistencia();        
        $("#detalle-item").focus();
      },
      error: function(data,status){
      }
    });
  };

  function seleccionaItem($this){
    $("#fecha-item").val($this.find(".td-fecha").html());
    $("#cantidad-item").val($this.find(".td-cantidad").html());
    $("#detalle-item").val($this.find(".td-detalle").html());
    itemId = $this.attr("id");
  }
  
  function grabaItem(){
    var $this;
    if(itemId){
      $this = $("#"+itemId);
    }else{
      $this = $itemTemplate.clone();
    }
    $.ajax({
      type: "POST",
      dataType:"json",
      data: {
        alumno_id:alumno_id,
        logica_periodo_id:logica_periodo_id,
        item_id:itemId ? itemId : -1,
        fecha:$("#fecha").val(),
        inasistencia_tipo_id:inasistencia_tipo_id,
        detalle:$("#detalle-item").val()
      },
      url: "<?php echo $this->createUrl("inasistenciaXAlumno/grabaItem"); ?>",
      success: function(data) {
        if(data.errors){
          muestraErrores(data.errors);
          return;
        }
        $("#tabla-asignaturas").find("tr").last().before($this);
        $this.find("td").not(".td-borra").click(function(){
          seleccionaItem($(this));
        });
        
        $this.find(".td-tipo-inasistencia").html(inasistencia_tipo_nombre);
        $this.find(".td-detalle").html($("#detalle-item").val());
        $this.attr("id",data.id);
        $("#td-tipo-inasistencia").val("");
        $("#detalle-item").val("").focus();
        itemId=null;
      },
      error: function(data,status){
      }
    });    
    return false;
  }

  function borraItem(itemId){
    if(!confirm("Está seguro de borrar este registro?")){
      return;
    }
    $.ajax({
      type: "POST",
//      dataType:"json",
      data: {item_id:itemId},
      url: "<?php echo $this->createUrl("inasistenciaXAlumno/borraItem"); ?>",
      success: function(data) {
        $("#tabla-asignaturas").find("#"+itemId).remove();
      },
      error: function(data,status){
      }
    });    
    return false;
  }
  
</script>
