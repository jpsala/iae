<?php // include("/../../tcpdf/tcpdf.php"); ?>
<?php
require_once('tcpdf/config/lang/spa.php');
require_once('tcpdf/tcpdf.php');

class RegistroMatriculaPDF extends TCPDF {
	public $headerHeight;
	public function Header() {
		$margins = $this->getMargins();
		$marginsH = $margins["left"] + $margins["right"];
		$imgHeader = "http://" . $_SERVER["SERVER_NAME"] . '/' . Yii::app()->baseUrl . "/images/RegistroMatriculaHeader.jpg";
		$imgHeaderWidth = $this->getPageWidth() - $marginsH;
		$this->Image($imgHeader, $this->getX(), $this->getY(), $imgHeaderWidth);
		$this->headerHeight = $this->getY();
	}

	public function Footer() {
		
	}

	public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $thisa = false) {
		parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $thisa);
//		$this->SetFont("sourcecodepro");
		$this->SetFontSize(8);
		$this->SetMargins(0, 0, 0, true);
		$this->SetCellPaddings(.5, 1, .5,  1);
		$this->print_header = true;
		$this->print_footer = false;
		$this->SetAutoPageBreak(TRUE, 0);
	}

	public function MultiCell($w, $h, $txt, $border = 0, $align = 'J', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false) {
		$w = $w ? $w : $this->GetStringWidth($txt) + .00001;
		parent::MultiCell($w, $h, $txt, $border, $align, $fill, $ln, $x, $y, $reseth, $stretch, $ishtml, $autopadding, $maxh, $valign, $fitcell);
	}

	public function p($w, $txt) {
		$w = $w ? $w : $this->GetStringWidth($txt) + .00001;
		$x = $this->GetX();
		$y = $this->GetY();
		$this->MultiCell($w, 10,"", "LBR", "C", 0, 2, $x, $y,false,false,false,true,10, "", false);
		$this->MultiCell($w, 10, $txt, "", "L", 0, 0, $x, $y,false,false,false,true, null, "", false);
		//$this->MultiCell($w, $h, $txt, $border, $align, $fill, $ln, $x, $y, $reseth, $stretch, $ishtml, $autopadding, $maxh, $valign)
	}
	
	public function chkPageEnd($height) {
		$m = $this->getMargins();
		if (($this->getY()  + $height + $m["top"] + $m["bottom"]) >= $this->getPageHeight()) {
			return true;
		}
	}
	
	public function getMargins() {
		$m = parent::getMargins();
		$m["bottom"] = 6;
		return $m;
	}
}