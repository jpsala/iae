<?php

$ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();


$division_asignatura_id = $_GET["asignatura_id"];
$division_id = $_GET["division_id"];
$nivel_id = $_GET["nivel_id"];
list($materiaNombre, $asignatura_id, $esIntegradora) = Helpers::qryDataRow("select a.nombre , a.id, da.integradora
    from division_asignatura da
        inner join asignatura a on a.id = da.asignatura_id and da.id = $division_asignatura_id");

list($nivelNombre, $anioNombre, $divisionNombre) = Helpers::qryDataRow("
    select n.nombre, a.nombre, d.nombre 
        from division d 
            inner join anio a on a.id = d.anio_id
            inner join nivel n on n.id = a.nivel_id
        where d.id = $division_id");
$anioDivision = $anioNombre . " " . $divisionNombre;
$select = "
    select a.id, concat(a.apellido, ' ', a.nombre) as nombre, ad.id as alumno_division_id,
    a.numero_documento, a.libre
	from alumno a
		inner join alumno_division ad on ad.Alumno_id = a.id and ad.ciclo_id = $ciclo_id and ad.division_id = $division_id
		inner join alumno_division_estado ade on ade.id = ad.alumno_division_estado_id and ade.muestra_edu
		inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_edu and ! ae.ingresante
        where  a.activo = 1 /*and ad.activo*/ and ! ad.borrado
        order by a.apellido, a.nombre
";
//vd($select);
$alumnos = Helpers::qryAll($select);
$logica_id = Logica::getLogicaIdPorNivel($nivel_id);

$items = Yii::app()->db->createCommand("
            select lp.id as logica_periodo_id, li.condicion, lp.orden as logica_periodo_orden, li.logica_periodo_id,
                    li.nombre_unico as logica_item_nombre_unico, lp.nombre_unico as logica_periodo_nombre_unico,
                    li.orden as logica_item_orden, li.manual, li.id, li.formula, li.nombre, li.orden as logica_item_orden,
                    lp.fecha_inicio_ci as logica_periodo_fecha_inicio, lp.fecha_fin_ci as logica_periodo_fecha_fin, tipo_nota, li.estado
             from logica_item li
                    inner join logica_periodo lp on lp.id = li.logica_periodo_id
              where li.logica_id = $logica_id and logica_periodo_id = 3
              order by lp.orden, li.orden

        ")->queryAll();
$nroOrden = 1;
$totalAlumnos = 0;
$totalAprobados = 0;
$data = array();
foreach ($alumnos as $alumno) {
	$periodos = LogicaActiva::getNotasAlumnoAsignaturaPeriodos($alumno["alumno_division_id"], $asignatura_id, $nivel_id, $logica_id);
	//ve($periodos["CD"]["CD"]["estado"] );
//    $prom = $periodos["CD"]["CD"]["nota"];
	$aprobado = (($periodos["CD"]["CD"]["estado"] == "Aprobado") and ($alumno["libre"] === "0"));
	if (!$aprobado) {
		$data[] = array(
			"nroOrden" => $nroOrden++,
			"alumno" => $alumno["nombre"],
			"dni" => $alumno["numero_documento"],
			"escrito" => $periodos["Final"]["Final"]["nota"],
			"oral" => "",
		);
		$totalAprobados += ($aprobado) ? 1 : 0;
		$totalAlumnos++;
	}
}
//$tbl = Helpers::getTable("Alumno", $data);
//echo $tbl;

//$imgHeader = "http://" . $_SERVER["SERVER_NAME"] . '/' . Yii::app()->baseUrl . "/images/logo25.png";
$imgHeader = "http://" . $_SERVER["SERVER_NAME"] . '/' . Yii::app()->baseUrl . "/images/ISO IAE 2006.jpg";
$pdf = new PDF("P", 'mm', "legal", true, 'UTF-8', false);
$pdf->SetMargins(15, 0, 15, true);
$pdf->AddPage();
$pdf->SetFontSize(16);
$pdf->Image($imgHeader, $pdf->getPageWidth() - 30, 10, 18);
$pdf->setY(20);
$pdf->MultiCell(Null, Null, "ACTA VOLANTE DE EXÁMENES", Null, "C");
$pdf->SetFontSize(13);
$pdf->y += 5;
$pdf->SetCellPaddings(0, 0, 0, 2);
$pdf->MultiCell(Null, Null, "Lº:..................", Null, "R");
$pdf->MultiCell(Null, Null, "Fº:..................", Null, "R");
$pdf->y -= 10;
$pdf->MultiCell(50, Null, "Fecha :", Null, "l");
$pdf->MultiCell(100, Null, "Materia: " . $materiaNombre, Null, "L");
$pdf->MultiCell(null, Null, "EXÁMENES DE ALUMNOS ..........................................................................", Null, "L");
$pdf->MultiCell(null, Null, "Nivel: $nivelNombre   Año/División: $anioDivision", Null, "L");

$pdf->SetFontSize(10);
$pdf->SetCellPaddings(2, 5, 2, 0);
$pdf->x = 10;
$pdf->MultiCell(20, 31, "Nº de orden", "TLBR", "C", false, 0);
$pdf->MultiCell(22, 31, "Nº de documento", "TBR", "C", false, 0);
$pdf->MultiCell(68, 31, "APELLIDO Y  NOMBRES", "TBR", "C", false, 0);
$pdf->SetCellPaddings(2, 5, 2, 0);
$pdf->MultiCell(50, 14, "Calificación", "TBR", "C", false, 0);
$pdf->SetCellPaddings(2, 5, 2, 2);
$pdf->SetFontSize(9);
$pdf->y += 14;
$pdf->x -= 113;
$pdf->MultiCell(18, 17, "Escrito", "BR", "C", false, 0);
$pdf->MultiCell(13, 17, "Oral", "BR", "C", false, 0);
$pdf->MultiCell(21.1, 17, "Calificación definitiva", "BR", "C", false, 0);
$savex = $pdf->x;
$pdf->SetFontSize(11);
$pdf->SetCellPaddings(2, 5, 2, 0);
$pdf->y -= 14;
$pdf->x = $savex;
//$pdf->MultiCell(41, 25, "Documento de Identidad", "TBR", "C", false, 0);

$pdf->y += 31;

$pdf->SetCellPaddings(2, 1, 0, 1);
foreach ($data as $row) {
	$pdf->x = 10;
	$pdf->MultiCell(20, null, $row["nroOrden"], "LBR", "C", false, 0);
	$pdf->MultiCell(22, null, $row["dni"], "LB", "C", false, 0);
	$pdf->MultiCell(68, null, $row["alumno"], "LB", "l", false, 0);
	$pdf->MultiCell(15.9, null, /* $row["escrito"] */
		"", "LB", "C", false, 0);
	$pdf->MultiCell(13, null, "", "LB", "C", false, 0);
	$pdf->MultiCell(21.1, null, "", "LBR", "C", false, 1);
	//$pdf->MultiCell(41, null, "", "LBR", "l", false, 1);
}
$pdf->SetCellPaddings(0, 2, 0, 0);
$pdf->SetFontSize(10);
$pdf->y += 10;
$x = 8;
$pdf->MultiCell(190, null, "Profesor:..............................................................................Profesor:......................................................................................", "", "L", false, 1, $x);
$pdf->MultiCell(190, null, "Total de alumnos: $totalAlumnos", "", "L", false, 1, $x);
$pdf->MultiCell(190, null, "Aprobados: ", "", "L", false, 1, $x);
//$desAprobados = $totalAlumnos - $totalAprobados;
$pdf->MultiCell(190, null, "Desaprobados:", "", "L", false, 1, $x);
$pdf->MultiCell(190, null, "Ausentes:", "", "L", false, 1, $x);
//$dia = date("d", time());
$pdf->MultiCell(190, null, "Mar del Plata, ..... de.................... de........", "", "L", false, 1, 120);
$pdf->Output();
?>

