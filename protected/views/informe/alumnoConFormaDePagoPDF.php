<?php

include("informe.php");
$fechaSaldo = date("Y-m-d", mystrtotime($fecha_desde)-1);
$fechaSaldoMuestra = date("d/m/Y", mystrtotime($fecha_desde) - 1);
$fecha_desde = date("Y/m/d", mystrtotime($fecha_desde));
$fecha_hasta = date("Y/m/d", mystrtotime($fecha_hasta)) . " 23:59:59";
$select = "
            select  '' as comprob,  '' as Número,'Saldo' as detalle, -1 as id,  \"$fechaSaldoMuestra\" as fecha,'',
                          saldo($socio_id, \"$fechaSaldo\") as total
            union
            select  c.nombre as comprob,  concat(LPAD(d.sucursal, 4, '0'),'-',LPAD(d.numero, 8, '0')) as Número, 
                         d.detalle,d.id, d.fecha_valor as fecha, TIME(d.fecha_creacion) as hora, (d.total * c.signo_cc) as total
                from doc d
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
                    left join socio s on s.id = d.socio_id
                    left join alumno a on a.id = s.alumno_id
                where d.fecha_valor BETWEEN \"$fecha_desde\" and \"$fecha_hasta\"
                            and s.id = $socio_id and d.anulado = 0 and d.activo = 1
                order by 4,2
        ";
$data = Helpers::qryAll($select);
foreach ($data as $key => $row) {
    $doc_id = $row["id"];
//    if ($data[$key]["total"] < 0) {
//        $data[$key]["total1"] = $data[$key]["total"];
//        $data[$key]["total"] = 0;
//    }
    $fechaDoc = $row["fecha"];
    $data[$key]["detail"] = Helpers::qryAll("
        select t.nombre as tipo_valor, v.numero as numerodet, 
        case when v.fecha is null then \"$fechaDoc\" else v.fecha end as fechadet,  
        b.nombre as banco, d.nombre as destino, 
                    ch.descripcion as chequera, v.importe,v.obs
                from doc_valor v
                    inner join doc_valor_tipo t on t.id = v.tipo
                    left join banco b on b.id = v.banco_id
                    left join destino d on d.id = v.Destino_id
                    left join chequera ch on ch.id = v.chequera_id
                where v.Doc_id =  $doc_id
    ");
}
$alumno = Helpers::qryScalar("
    select concat(apellido, ', ', nombre) as alumno 
        from alumno a
            inner join socio s on s.Alumno_id = a.id
        where s.id = $socio_id
");
$colsDef = array(
        "id" => array("visible" => false),
        "sucursal" => array("visible" => false),
        "numero" => array("visible" => false),
        "fecha" => array("date"=>"d/m/Y"),
        "hora" => array("date"=>"h:i"),
        "detalle" => array("width" => 40),
        "total" => array("debeHaber"=>"dh","currency"=>true, "align" => "R", "sum"=>true),
        
);

$colsDefDetail = array(
        "tipo_valor" => array("width" => 35, "mb_convert_case"=>true, "strtolower"=>true),
        "importe" => array("currency","width"=>50, "align" => "R", "sum"=>true),
        "numerodet" => array("width"=>20, "align"=>"R"),
        "banco" => array("width"=>60),
        "obs" => array("width"=>35, "align"=>"L"),
        "fechadet" => array("date"=>"d/m/Y", "width"=>24),
);
//vd($this->encoding);
$options = array(
        "data" => $data,
        "marginY" => 4,
        "marginX" => 3,
        "title" => "Cta. Cte. con valores entre el " . date("d/m/Y", mystrtotime($fecha_desde) ) . " y el " . date("d/m/Y", mystrtotime($fecha_hasta)),
        "title2" => $alumno,
);
$i = new Informe($options, $colsDef, $colsDefDetail);
$i->render();
?>