<?php

class MYPDF extends PDF {

    public $informe, $Y, $imgHeader;

    public function header() {
        return;
        $this->Image($this->informe->imgHeader, 2, 2, 14);

        $this->setY($this->informe->marginY);

        $this->t("Alumnos Con Forma De Pago", 0, null, "C", $this->getPageWidth());

        $txt = "Impreso el";
        $this->setX($this->getPageWidth() - $this->getStringWidth($txt) - 3);
        $this->setY($this->getY() + $this->t($txt));

        $fecha = date("d/m/Y", time());
        $this->setX($this->getPageWidth() - $this->getStringWidth($fecha) - 3);
        $this->t($fecha);


        $this->setY($this->getY() + $this->t($this->informe->args["title2"], 0, null, "C", $this->getPageWidth()));

        $this->Y = $this->getY() + 2;
        $this->line(0, $this->Y, $this->getPageWidth(), $this->Y);
        $this->Y = $this->getY() + 2;

        $x = $this->informe->marginX;
        $h = "";
        foreach ($this->informe->cols as $col) {
            $h = $this->getStringHeight($this->getStringWidth($col["label"]), $col["label"]);
            if ($col["debeHaber"]) {
                $this->textBox(array(
                        "txt" => "Debe",
                        "y" => $this->Y,
                        "x" => $x,
                        "w" => $col["width"],
                        "h" => $h,
                        "align" => $col["align"],
                ));
                $x += $col["width"];
                $this->textBox(array(
                        "txt" => "Haber",
                        "y" => $this->Y,
                        "x" => $x,
                        "w" => $col["width"],
                        "h" => $h,
                        "align" => $col["align"],
                ));
                $x += $col["width"];
                $this->textBox(array(
                        "txt" => "Saldo",
                        "y" => $this->Y,
                        "x" => $x,
                        "w" => $col["width"],
                        "h" => $h,
                        "align" => $col["align"],
                ));
            } else {
                $this->textBox(array(
                        "txt" => trim($col["label"]),
                        "y" => $this->Y,
                        "x" => $x,
                        "w" => $col["width"],
                        "h" => $h,
                        "align" => $col["align"],
                ));
                $x += $col["width"];
            }
        }
        $this->Y += $h;
        $this->line(0, $this->Y, $this->getPageWidth(), $this->Y);
        $this->Y += 1;
    }

    public function footer() {
        return;
        $txt = "Pág.:" . $this->getPage();
        $w = $this->getStringWidth($txt) + 2;
        $this->textBox(array(
                "txt" => $txt,
                "y" => $this->getPageHeight() - 7,
                "x" => $this->getPageWidth() - $w,
                "w" => $w,
        ));
    }

    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
    }

}

class Informe extends stdClass {

    public $pdf, $data, $marginX, $marginY, $cols, $colsDef, $colsDefDetail, $totals = array();

    public function __construct() {
        $this->imgHeader = "http://" . $_SERVER["SERVER_NAME"] . '/' . Yii::app()->baseUrl . "/images/logo.jpg";
    }
    
    private function renderData($data, $y = null, $x = 0, $colsDef = array(), $esDetalle = false) {
        $y = $y ? $y : $this->pdf->Y;
        $left = $this->marginX + $x;
        $saldo = 0;
        foreach ($data as $row) {
            $x = $left;
            $height = 0;
            foreach ($row as $key => $col) {
                if (!is_array($col)) {
                    $col = trim($col);
                    $h = $this->pdf->getStringHeight($this->pdf->getStringWidth($col) + 2, $col, false, false, '', 0);
                    $height = ($height > $h) ? $height : $h;
                }
            }
            if ($this->chkPageEnd($height)) {
                $y = $this->pdf->Y;
                $x = $left;
            }

            foreach ($row as $key => $col) {
                if (!is_array($col)) {
                    $debeHaber = false;
                    if (isset($colsDef[$key])) {
                        if (isset($colsDef[$key]["visible"]) and !$colsDef[$key]["visible"]) {
                            continue;
                        }
                        if (isset($colsDef[$key]["date"])) {
                            $format = $colsDef[$key]["date"] ? $colsDef[$key]["date"] : "d/m/Y";
                            $col = date($format, mystrtotime($col));
                        }
                        if (isset($colsDef[$key]["debeHaber"])) {
                            $debeHaber = $colsDef[$key]["debeHaber"];
                        } elseif (isset($colsDef[$key]["currency"])) {
                            $col = number_format($col, 2);
                        }
                        if (isset($this->totals[$key])) {
                            $this->totals[$key] += str_replace(",", "", $col) + 0;
                        }
                        if (isset($colsDef[$key]["mb_convert_case"])) {
                            $col = mb_convert_case($col, MB_CASE_TITLE, "UTF-8");
                        }
                        if (isset($colsDef[$key]["strtolower"])) {
                            //$col = strtolower($col);
                        }
                    }
                    if (isset($colsDef[$key]["width"]) and $esDetalle) {
                        $w = $colsDef[$key]["width"];
                    } elseif (isset($this->cols[$key]["width"])) {
                        $w = $this->cols[$key]["width"];
                    } elseif (isset($colsDef[$key]["width"])) {
                        $w = $colsDef[$key]["width"];
                    } else {
                        $w = $this->pdf->getStringWidth($col) + 2;
                    }
                    if ($debeHaber) {
                        if ($debeHaber == "dh") {
                            $debe = ($col >= 0) ? number_format(abs($col), 2) : "";
                            $haber = ($col < 0) ? number_format(abs($col), 2) : "";
                        } else {
                            $debe = ($col < 0) ? number_format(abs($col), 2) : "";
                            $haber = ($col >= 0) ? number_format(abs($col), 2) : "";
                        }
                        $saldo += $col;
                        $this->pdf->textBox(array(
                                "txt" => $debe,
                                "y" => $y,
                                "x" => $x,
                                "w" => $w,
                                "h" => $height,
                                "align" => isset($this->cols[$key]) ? $this->cols[$key]["align"] : "L",
                        ));
                        $x+=$w;
                        $this->pdf->textBox(array(
                                "txt" => $haber,
                                "y" => $y,
                                "x" => $x,
                                "w" => $w,
                                "h" => $height,
                                "align" => isset($this->cols[$key]) ? $this->cols[$key]["align"] : "L",
                        ));
                        $x+=$w;
                        $this->pdf->textBox(array(
                                "txt" => number_format($saldo, 2),
                                "y" => $y,
                                "x" => $x,
                                "w" => $w,
                                "h" => $height,
                                "align" => isset($this->cols[$key]) ? $this->cols[$key]["align"] : "L",
                        ));
                        $x+=$w;
                    } else {
                        $this->pdf->textBox(array(
                                "txt" => $col,
                                "y" => $y,
                                "x" => $x,
                                "w" => $w,
                                "h" => $height,
                                "align" => isset($this->cols[$key]) ? $this->cols[$key]["align"] : "L",
                        ));
                        $x+=$w;
                    }
                }
            }
            $y+=$height;
            if (isset($row["detail"]) and count($row["detail"]) > 0) {
                $y = $this->renderData($row["detail"], $y, 5, $this->colsDefDetail, true) + 7;
            }
        }

        if (count($data) > 0) {
            $x = $left;
            foreach ($data[0] as $key => $col) {
                if (!is_array($col)) {
                    if (isset($colsDef[$key]["visible"]) and !$colsDef[$key]["visible"]) {
                        continue;
                    }
                    $debeHaber = isset($colsDef[$key]["debeHaber"]) ? true : false;
                    if (isset($this->cols[$key]["width"])) {
                        $w = $this->cols[$key]["width"];
                    } else {
                        $w = $this->pdf->getStringWidth($col) + 2;
                    }
                    if ($debeHaber) {
                        $x += $w;
                        $this->pdf->textBox(array(
                                "txt" => number_format($saldo, 2),
                                "y" => $y,
                                "x" => $x,
                                "w" => $w,
                                "h" => $height,
                                "align" => isset($this->cols[$key]) ? $this->cols[$key]["align"] : "L",
                        ));
                    } else {
                        $col = isset($this->totals[$key]) ? number_format($this->totals[$key], 2) : "";
                        $this->pdf->textBox(array(
                                "txt" => $col,
                                "y" => $y,
                                "x" => $x,
                                "w" => $w,
                                "h" => $height,
                                "align" => isset($this->cols[$key]) ? $this->cols[$key]["align"] : "L",
                        ));
                        $x+=$w;
                    }
                }
            }
        }
        return $y;
    }

    private function chkPageEnd($height) {
        if (($this->pdf->getY() + $height + 15) >= $this->pdf->getPageHeight()) {
            $this->pdf->AddPage();
            return true;
        } else {
            return false;
        }
    }

}

$fechaSaldo = date("Y-m-d", mystrtotime($fecha_desde) - 1);
$fecha_desde = date("Y/m/d", mystrtotime($fecha_desde));
$fecha_hasta = date("Y/m/d", mystrtotime($fecha_hasta)) . " 23:59:59";
$select = "
            select  '' as comprob,  '' as Número,'Saldo' as detalle, -1 as id,  \"$fechaSaldo\" as fecha,
                          saldo($socio_id, \"$fechaSaldo\") as total
            union
            select  c.nombre as comprob,  concat(LPAD(d.sucursal, 4, '0'),'-',LPAD(d.numero, 8, '0')) as Número, 
                         d.detalle,d.id, d.fecha_valor as fecha, (d.total * c.signo_cc) as total
                from doc d
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
                    left join socio s on s.id = d.socio_id
                    left join alumno a on a.id = s.alumno_id
                where d.fecha_valor BETWEEN \"$fecha_desde\" and \"$fecha_hasta\"
                            and s.id = $socio_id and d.anulado = 0 and d.activo = 1
                order by 4,2
        ";
$data = Helpers::qryAll($select);
foreach ($data as $key => $row) {
    $doc_id = $row["id"];
    $fechaDoc = $row["fecha"];
    $data[$key]["detailData"]["valores"] = Helpers::qryAll("
        select t.nombre as tipo_valor, v.numero as numerodet, 
        case when v.fecha is null then \"$fechaDoc\" else v.fecha end as fechadet,  
        b.nombre as banco, d.nombre as destino, 
                    ch.descripcion as chequera, v.importe,v.obs
                from doc_valor v
                    inner join doc_valor_tipo t on t.id = v.tipo
                    left join banco b on b.id = v.banco_id
                    left join destino d on d.id = v.Destino_id
                    left join chequera ch on ch.id = v.chequera_id
                where v.Doc_id =  $doc_id
    ");
}
$alumno = Helpers::qryScalar("
    select concat(apellido, ', ', nombre) as alumno 
        from alumno a
            inner join socio s on s.Alumno_id = a.id
        where s.id = $socio_id
");

        $cols["alumno"] = array(
                "fecha" => array("date"=>true, "format"=>"d/m/Y"),
                "fecha_valor" => array("date"=>true, "format"=>"d/m/Y","title"=>"fec. valor"),
                "id" => array("hidden" => true),
        );
        $cols["id"] = array(
                "hidden" => true,
        );
        $cols["valores"] = array(
                "fecha" => array("date"=>true, "format"=>"d/m/Y","title"=>"fecha"),
        );
        $cols["apps"] = array(
                "Fec. Creación" => array("date"=>true, "format"=>"d/m/Y"),
        );

$colsDefDetail = array(
        "tipo_valor" => array("width" => 35, "mb_convert_case" => true, "strtolower" => true),
        "importe" => array("currency", "width" => 50, "align" => "R", "sum" => true),
        "numerodet" => array("width" => 20, "align" => "R"),
        "banco" => array("width" => 60),
        "obs" => array("width" => 35, "align" => "L"),
        "fechadet" => array("date" => "d/m/Y", "width" => 24),
);
$style ="
    <style type=\"text/css\">
        .alumno-tablegen-fecha-th,.alumno-tablegen-fecha-td{width:80px;text-align:right}
        .alumno-tablegen-total-th,.alumno-tablegen-total-td{width:80px;text-align:right}
    </style>
    ";
        
$div = $style . Helpers::getTable4PDF("alumno", $data, $cols, true, false);

$options = array(
        "marginY" => 4,
        "marginX" => 3,
        "title" => "Cta. Cte. con valores entre el " . date("d/m/Y", mystrtotime($fecha_desde)) . " y el " . date("d/m/Y", mystrtotime($fecha_hasta)),
        "title2" => $alumno,
);
//echo $div;
//return;
$pdf = new PDF("P", "mm", "A4");
$pdf->AddPage();
$pdf->writeHTML($div);
$pdf->Output();
?>