<?php

include("informe.php");
$fechaSaldo = date("Y-m-d", mystrtotime($fecha_desde)-1);
$fecha_desde = date("Y/m/d", mystrtotime($fecha_desde));
$fecha_hasta = date("Y/m/d", mystrtotime($fecha_hasta)) . " 23:59:59";
$select = "
            select  -1 as doc_id, '' as comprob,  '' as Número,'Saldo' as detalle, -1 as id,  \"$fechaSaldo\" as fecha,
                          saldo($socio_id, \"$fechaSaldo\") as total
            union
            select d.id as doc_id, c.nombre as comprob,  concat(LPAD(d.sucursal, 4, '0'),'-',LPAD(d.numero, 8, '0')) as Número, 
                         d.detalle,d.id, d.fecha_valor as fecha, (d.total * c.signo_cc) as total
                from doc d
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
                    left join socio s on s.id = d.socio_id
                    left join alumno a on a.id = s.alumno_id
                where d.fecha_valor BETWEEN \"$fecha_desde\" and \"$fecha_hasta\"
                            and s.id = $socio_id and d.anulado = 0 and d.activo = 1
                order by 6,2
        ";
$data = Helpers::qryAll($select);
//vd($data);
foreach ($data as $key => $row) {
    $doc_id = $row["doc_id"];
    $s = "
        select dd.doc_id, dd.detalle, dd.total
	from doc_det dd
	where dd.Doc_id = $doc_id";
    $data[$key]["detail"] = Helpers::qryAll($s);
}

$alumno = Helpers::qryScalar("
    select concat(a.apellido,', ' , a.nombre) as alumno 
        from alumno a
            inner join socio s on s.Alumno_id = a.id
        where s.id = $socio_id
");
$colsDef = array(
        "doc_id" => array("visible" => false),
        "id" => array("visible" => false),
        "sucursal" => array("visible" => false),
        "numero" => array("visible" => false),
        "fecha" => array("date" => "d/m/Y"),
        "detalle" => array("width" => 50),
        "total" => array("debeHaber" => "dh", "align" => "R", "sum" => true),
);

$colsDefDetail = array(
        "doc_id" => array("visible" => false),
        "detalle" => array("width" => 136.2),
        "total" => array("debeHaber" => "dh", "align" => "R", "sum" => true, "widthh" => 8),
);

$options = array(
        "data" => $data,
        "marginY" => 4,
        "marginX" => 5,
        "title" => "Cta. Cte. con valores entre el " . date("d/m/Y", mystrtotime($fecha_desde)) . " y el " . date("d/m/Y", mystrtotime($fecha_hasta)),
        "title2" => $alumno,
);
$i = new Informe($options, $colsDef, $colsDefDetail);
$i->render();
?>