<?php

$pdf = new PDF("L", "mm", "legal", true, 'UTF-8', false);
$ciclo_id = Ciclo::getCicloIdParaInformesEducativo();

$where = "a.activo = 1 ";
$where .= ($nivel_id == "-1") ? "" : " and n.id = $nivel_id";
$where .= ($anio_id == "-1") ? "" : " and anio.id = $anio_id";
$where .= ($division_id == "-1") ? "" : " and d.id = $division_id";
$direccion = "concat (COALESCE(p1.calle, ''), ' ', COALESCE(p1.numero, ''), ' ', COALESCE(p1.piso, ''), ' ', COALESCE(p1.departamento, '')) as domicilio";
//$sql = "
//            select concat(a.apellido,' ', a.nombre) as Nombre,
//                a.matricula, n.nombre as nivel, anio.nombre as curso, d.nombre as division,
//                a.beca, a.obs_cobranza, a.obs, saldo(s.id, null) as saldo,concat(p1.apellido,' ' ,p1.nombre) as vive_con,
//                p1.telefono_casa as p1tc, p1.telefono_trabajo as p1tt, p1.telefono_celular as p1tce, 
//                concat( p2.apellido, ' ' , p2.nombre) as nombre2, 
//                p2.telefono_casa as p2tc, p2.telefono_trabajo as p2tt, p2.telefono_celular as p2tce,
//                p3.nombre as nombre3, pt.nombre as pariente1, pt1.nombre as pariente2,  $direccion
//            from alumno a
//                left  join alumno_debito deb on deb.alumno_id = a.id
//                inner join socio s on s.Alumno_id = a.id
//                left join alumno_division ad on ad.Alumno_id = a.id and ad.activo = 1
//                left join division d on d.id = ad.Division_id
//                left join anio on anio.id = d.Anio_id
//                left join articulo ar on ar.id = anio.articulo_id
//                left join nivel n on n.id = anio.Nivel_id
//                left join pariente p1 on p1.id = a.vive_con_id
//                left join pariente p2 on p2.id = (select id from pariente p2 where p2.Familia_id = a.Familia_id and p2.id <> a.vive_con_id limit 1 )
//                left join pariente p3 on p3.id = (select id from pariente p3 where p3.Familia_id = a.Familia_id and p3.id <> a.vive_con_id limit 1,2 )
//                left join pariente_tipo pt on pt.id = p1.Pariente_Tipo_Id 
//                left join pariente_tipo pt1 on pt1.id = p2.Pariente_Tipo_Id 
//            where $where
//            order by n.orden, anio.orden, d.nombre, a.apellido, a.nombre
//";
//vd($sql); 
$sql = "
            select concat(a.apellido,' ', a.nombre) as Nombre,
                a.matricula, n.nombre as nivel, anio.nombre as curso, d.nombre as division,
                a.beca, concat(p1.apellido,' ' ,p1.nombre) as vive_con,
                p1.telefono_casa as p1tc, p1.telefono_trabajo as p1tt, p1.telefono_celular as p1tce, 
                concat( p2.apellido, ' ' , p2.nombre) as nombre2, 
                saldo(s.id, null) as saldo
            from alumno a
			inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_edu = 1
			left  join alumno_debito deb on deb.alumno_id = a.id
                inner join socio s on s.Alumno_id = a.id
                left join alumno_division ad on ad.Alumno_id = a.id and ad.ciclo_id = $ciclo_id
                left join division d on d.id = ad.Division_id
                left join anio on anio.id = d.Anio_id
                left join articulo ar on ar.id = anio.articulo_id
                left join nivel n on n.id = anio.Nivel_id
                left join pariente p1 on p1.id = a.vive_con_id
                left join pariente p2 on p2.id = (select id from pariente p2 where p2.Familia_id = a.Familia_id and p2.id <> a.vive_con_id limit 1 )
                left join pariente p3 on p3.id = (select id from pariente p3 where p3.Familia_id = a.Familia_id and p3.id <> a.vive_con_id limit 1,2 )
                left join pariente_tipo pt on pt.id = p1.Pariente_Tipo_Id 
                left join pariente_tipo pt1 on pt1.id = p2.Pariente_Tipo_Id 
            where $where
            order by n.orden, anio.orden, d.nombre, a.apellido, a.nombre
";
$data = Helpers::qryFieldsAndData($sql);
$cols = array(
        "Nombre" => "max-width:10px",
);
$style = "<style>";
foreach ($data["fields"] as $fld) {
    if (isset($cols[$fld])) {
        $style .= ".$fld{" . $cols[$fld] . "}" . EOL;
    }
}
$style .= "</style>";
$table = "<table>";
foreach ($data["data"] as $row) {
    $table .= "<tr>";
    $i = 0;
    foreach ($row as $value) {
        $class = isset($data["fields"][$i]) ? "class=\"" . $data["fields"][$i] . "\"" : "";
        $table .= "<td $class>$value</td>";
        $i++;
    }
    $table .= "</tr>";
}
echo $style . $table;
// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage();
$pdf->writeHTML($style . $table, true, false, true, false, '');
$pdf->Output('example_061.pdf', 'I');
?>