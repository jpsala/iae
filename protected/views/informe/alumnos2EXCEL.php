<?php

$fileName = "Alumnos_documento-fecha_de_nacimiento_dirección_teléfono_fijo_y_ambos_celulares";
$direccion = "concat (COALESCE(p1.calle, ''), ' ', COALESCE(p1.numero, ''), ' ', COALESCE(p1.piso, ''), ' ', COALESCE(p1.departamento, '')) as domicilio";
$ciclo_id = Ciclo::getCicloIdParaInformesEducativo();

$where = ($nivel_id == "-1") ? " true " : " n.id = $nivel_id";
$where .= ($anio_id == "-1") ? "" : " and anio.id = $anio_id";
$where .= ($division_id == "-1") ? "" : " and d.id = $division_id";
$tps = Helpers::qryAllObj("select nombre from pariente_tipo");
$sql = "
            select a.numero_documento, a.Familia_id as familia_id, a.id as alumno_id, 
                concat(a.apellido,' ', a.nombre) as Nombre,
                $direccion, a.fecha_nacimiento, 
                d.nombre as division, anio.nombre as anio, n.nombre as nivel
            from alumno a
                left join pariente p1 on p1.id = a.vive_con_id
                left join alumno_estado ae on ae.id = a.estado_id and ae.activo_edu = 1
                left join alumno_division ad on ad.Alumno_id = a.id and ad.ciclo_id = $ciclo_id
                left join division d on d.id = ad.Division_id
                left join anio on anio.id = d.Anio_id
                left join nivel n on n.id = anio.Nivel_id
            where $where
            order by n.orden, anio.orden, d.orden , d.nombre, a.apellido, a.nombre
";
$rows = Helpers::qryAll($sql);
foreach ($rows as $key => $a) {
  $familia_id = $a["familia_id"];
  $parientes = Helpers::qryAll("
        select p.telefono_casa, p.telefono_trabajo, p.telefono_celular, 
               pt.nombre as pariente_tipo, concat(p.apellido, \", \", p.nombre) as nombre
            from pariente p
                inner join pariente_tipo pt on pt.id = p.Pariente_Tipo_id
            where p.Familia_id = $familia_id
        ");
  $datosParientes = "";
  foreach ($parientes as $p) {
    foreach ($tps as $tp) {
      if ($p["pariente_tipo"] == $tp->nombre) {
        $rows[$key][$tp->nombre] = $p["nombre"];
        $rows[$key][$tp->nombre . "-fijo"] = $p["telefono_casa"];
        $rows[$key][$tp->nombre . "-trabajo"] = $p["telefono_trabajo"];
        $rows[$key][$tp->nombre . "-celular"] = $p["telefono_celular"];
      } else {
        //vd($rows[$key]);
        $rows[$key][$tp->nombre] = "";
        $rows[$key][$tp->nombre . "-fijo"] = "";
        $rows[$key][$tp->nombre . "-trabajo"] = "";
        $rows[$key][$tp->nombre . "-celular"] = "";
      }
    }
  }
}
//vd($rows);
$cols = array();
if ($anio_id == -1) {
  $cols["anio"] = array("title" => "Año");
  $cols["division"] = array("title" => "División");
}
if ($division_id == -1) {
  $cols["division"] = array("title" => "División");
}
$cols["Nombre"] = array("title" => "Alumno");
$cols["numero_documento"] = array("title" => "DNI");
$cols["fecha_nacimiento"] = array("title" => "Fecha Nac.", "format" => "date");
foreach ($tps as $tp) {
  $cols[$tp->nombre] = array("title" => $tp->nombre);
  $cols[$tp->nombre . "-fijo"] = array("title" => "Fijo");
  $cols[$tp->nombre . "-trabajo"] = array("title" => "Trabajo");
  $cols[$tp->nombre . "-celular"] = array("title" => "Celular");
}

//$cols["datos"] = array("title" => "Parientes");


$excel = new Excel($rows, $cols, $fileName);
$excel->run();
$excel->output();
