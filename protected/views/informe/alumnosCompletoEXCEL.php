<?php

$fileName = "Alumnos_Completo";
$ciclo_id = Ciclo::getCicloIdParaInformesEducativo();
// vd2($ciclo_id);
$where = "true ";
$where .= ($nivel_id == "-1") ? "" : " and n.id = $nivel_id";
$where .= ($anio_id == "-1") ? "" : " and anio.id = $anio_id";
$where .= ($division_id == "-1") ? "" : " and d.id = $division_id";
$sql = "
   select n.nombre as nivel, anio.nombre as curso, d.nombre as division, ae.nombre as Estado,
      a.Matricula, a.apellido, a.nombre, a.sexo, a.familia_id, a.numero_documento,
      a.fecha_nacimiento as Fec_Nac, a.vive_con_id, a.telefono_1, a.telefono_2, a.email
   from alumno a
      left join alumno_division ad on ad.Alumno_id = a.id and ad.ciclo_id = $ciclo_id /*and ad.activo*/ and not ad.borrado /*((ad.activo or ad.promocionado or ad.egresado)*/
      left join alumno_estado ae on a.estado_id = ae.id
      left join division d on d.id = ad.Division_id /*and ad.activo*/ and not ad.borrado
      left join anio on anio.id = d.Anio_id
      left join nivel n on n.id = anio.Nivel_id
   where $where and ad.activo = 1 and a.activo = 1 and not a.ingresante and not ae.ingresante
   order by n.orden, anio.orden, d.nombre, a.apellido, a.nombre
";
//$sqlcnt = "
//   select count(*) as cant
//   from alumno a
//      left join alumno_division ad on ad.Alumno_id = a.id and ad.ciclo_id = $ciclo_id /*and ad.activo*/ and not ad.borrado /*((ad.activo or ad.promocionado or ad.egresado)*/
//      left join alumno_estado ae on a.estado_id = ae.id
//      left join division d on d.id = ad.Division_id /*and ad.activo*/ and not ad.borrado
//      left join anio on anio.id = d.Anio_id
//      left join nivel n on n.id = anio.Nivel_id
//   where $where and ad.activo = 1 and a.activo = 1 and not a.ingresante and not ae.ingresante
//   order by n.orden, anio.orden, d.nombre, a.apellido, a.nombre
//";
//vd(Helpers::qryScalar($sqlcnt));
$tps = Helpers::qryAllObj("select id, nombre from pariente_tipo order by orden");
$rows = Helpers::qryAll($sql);
ini_set('memory_limit', '-1');
$hiddens = array("familia_id", "vive_con_id");
foreach ($rows as $key => $a) {
    $familia_id = $a["familia_id"];
    foreach ($tps as $tp) {
        $p = Helpers::qry("
        select p.id, p.telefono_casa, p.telefono_trabajo, p.telefono_celular, p.email,
               pt.nombre as pariente_tipo, concat(p.apellido, \", \", p.nombre) as nombre,
               concat(p.calle, \" \",
                  case when p.numero=0 then \"\" else p.numero end, \" \", p.piso, \" \", p.departamento
					) as domicilio, concat( td.nombre,\"-\",p.numero_documento) as documento
            from pariente p
                inner join pariente_tipo pt on pt.id = p.Pariente_Tipo_id
			inner join tipo_documento td on td.id = p.tipo_documento_id
            where p.Familia_id = $familia_id and p.Pariente_Tipo_id = $tp->id
            order by pt.orden
        ");
        $nombre = $p["nombre"];
        $nombre .= (($p["id"] == $a["vive_con_id"]) ? "(V)" : "");
        $rows[$key][$tp->nombre] = $p ? $nombre : "";
        $rows[$key][$tp->nombre . "-fijo"] = $p ? $p["telefono_casa"] : "";
        $rows[$key][$tp->nombre . "-trabajo"] = $p ? $p["telefono_trabajo"] : "";
        $rows[$key][$tp->nombre . "-celular"] = $p ? $p["telefono_celular"] : "";
        $rows[$key][$tp->nombre . "-domicilio"] = $p ? $p["domicilio"] : "";
        $rows[$key][$tp->nombre . "-email"] = $p ? $p["email"] : "";
        $rows[$key][$tp->nombre . "-documento"] = $p ? $p["documento"] : "";
    }
}
$FIRST_CELL = "A1";

$objPHPExcel = new MyPHPExcel();
$as = $objPHPExcel->setActiveSheetIndex(0);

$as->getStyle("$FIRST_CELL:B1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$as->getStyle("$FIRST_CELL:B1")->getFont()->setBold(true);
$as->getStyle("$FIRST_CELL:B1")->getFill()->getStartColor()->setARGB("666666");
$as->getStyle("$FIRST_CELL:B1")->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);

if (count($rows) < 1) {
    return;
}

$styleArray = array(
      'borders' => array(
             'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
             )
      )
);

//$as->mergeCells('A1:Z1');
//$as->getStyle("A1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
//$as->SetCellValue("A1", "Reporte de Alumnos (Apellido y Nombre)");
//$as->SetCellValue("A2", "Nivel  " . $rows[0]["nivel"]);
//$as->SetCellValue("A3", "Curso " . $rows[0]["curso"] . "/" . $rows[0]["division"]);
$rowNum = 1;
Col::init();
foreach ($rows[0] as $key => $fld) {
    if (in_array($key, $hiddens)) {
        continue;
    }
    $col = Col::next();
    $as->SetCellValue($col . $rowNum, $key);
}
$rowNum++;
foreach ($rows as $row) {
    Col::init();
    foreach ($row as $key => $fld) {
        if (in_array($key, $hiddens)) {
            continue;
        }
        $col = Col::next();
        if ($key == "Fec_Nac") {
            $as->SetCellValue($col . $rowNum, date("d/m/Y", strtotime($fld)));
        } else {
            $as->SetCellValue($col . $rowNum, $fld);
        }
        if (strpos($fld, "(V)")) {
            $as->getStyle("$col$rowNum:$col$rowNum")->getFont()->setBold(true);
        }
    }
    $rowNum++;
}
Col::init();
foreach ($rows[0] as $key => $fld) {
    if (in_array($fld, $hiddens)) {
        continue;
    }
    $as->getColumnDimension(Col::next())->setAutoSize(true);
}
$as->setAutoFilter("A1:Y1");
$as->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$as->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_LEGAL);
$as->getPageSetup()->setFitToPage(true);
$as->getPageSetup()->setFitToWidth(1);
$as->getPageSetup()->setFitToHeight(0);

$objPHPExcel->output($fileName);
