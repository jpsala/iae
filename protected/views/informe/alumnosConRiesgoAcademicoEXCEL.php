<?php

// $nivel_id, $anio_id, $anio_id
$fileName = "InformeAlumnosConRiesgoAcademico.xls";
$ciclo_id = Ciclo::getCicloIdParaInformesEducativo();
$select = "
    select a.id, concat(a.apellido, ' ', a.nombre) as nombre, an.nombre as anio, di.nombre as division, ad.id as alumno_division_id
	from alumno a
		inner join alumno_division ad on ad.Alumno_id = a.id and ad.ciclo_id = $ciclo_id and ad.division_id = $division_id
                    inner join division di on di.id = ad.division_id
                    inner join anio an on an.id = di.anio_id
        where  a.activo = 1
        order by a.apellido, a.nombre
";
$alumnos = Helpers::qryAll($select);

list($anio, $division, $alumno_division_id) = Helpers::qryDataRow(
                "select an.nombre as anio, di.nombre as division, ad.id as alumno_division_id
	from alumno_division ad 
                    inner join division di on di.id = ad.division_id
                    inner join anio an on an.id = di.anio_id
          where ad.ciclo_id = $ciclo_id and ad.division_id = $division_id");

$cols = array(
        "nombre" => array("title" => "Alumnos $anio $division"),
);
$asignaturas = Notas::getAsignaturas($division_id/* , 823 */);
foreach ($asignaturas as $asignatura) {
    $cols[$asignatura["id"]] = array("title" => $asignatura["nombre"], "align" => "center", "filter" => false);
    $cols[$asignatura["id"]]["cant"] = 0;
}
$cols["cm"] = array("title" => "Cantidad de materias", "width" => 20, "align" => "center");
$cols["diciembre"] = array("title" => "Diciembre", "width" => 20, "align" => "center");

$nivel_id = Helpers::qryScalar("
                select nivel.id 
                    from nivel
                        inner join anio a on a.Nivel_id = nivel.id
                        inner join division d on d.Anio_id = a.id 
                        inner join alumno_division ad on ad.division_id = d.id and ad.id = $alumno_division_id");
$logica_id = Logica::getLogicaIdPorNivel($nivel_id);

$items = Yii::app()->db->createCommand("
            select lp.id as logica_periodo_id, li.condicion, lp.orden as logica_periodo_orden, li.logica_periodo_id,
                    li.nombre_unico as logica_item_nombre_unico, lp.nombre_unico as logica_periodo_nombre_unico,
                    li.orden as logica_item_orden, li.manual, li.id, li.formula, li.nombre, li.orden as logica_item_orden,
                    lp.fecha_inicio_ci as logica_periodo_fecha_inicio, lp.fecha_fin_ci as logica_periodo_fecha_fin, tipo_nota, li.estado
             from logica_item li
                    inner join logica_periodo lp on lp.id = li.logica_periodo_id
              where li.logica_id = $logica_id
              order by lp.orden, li.orden

        ")->queryAll();

foreach ($alumnos as $alumnoKey => $alumno) {
    $cm = 0;
    $dic = 0;
    foreach ($asignaturas as $asignatura) {
        //vd($alumno["alumno_division_id"], $asignatura["id"], $logica_periodo_id, $nivel_id, $logica_id, $items);
        $periodos = LogicaActiva::getNotasAlumnoAsignaturaPeriodos($alumno["alumno_division_id"], $asignatura["id"]
                        , $nivel_id, $logica_id, $items);
//        $nota = array_values(Notas::getNotasAlumnoAsignaturaPeriodo($alumno["alumno_division_id"], $asignatura["id"], $logica_periodo_id
//                 , $nivel_id, $logica_id, $items, Notas::TIPO_FINAL_INTERMEDIO));
        $t1 = $periodos["pt"]["t1"]["nota"];
        $t2 = $periodos["st"]["t2"]["nota"];
        $t3 = $t1 + $t2;
//				ve($periodos);
        $falta = 21 - $t3;
        if ($falta > 10) {
            $cm++;
            $nota = "dic";
            $dic++;
        } elseif ($falta > 7) {
            $nota = $falta;
            $cm++;
        } else {
            $nota = "";
        }
        $alumnos[$alumnoKey][$asignatura["id"]] = $nota;
        $cols[$asignatura["id"]]["cant"] += $nota == "" ? 0 : 1;
    }
    $alumnos[$alumnoKey]["cm"] = $cm == 0 ? "" : $cm;
    $alumnos[$alumnoKey]["diciembre"] = $dic == 0 ? "" : $dic;
}
$cant = 0;
foreach ($asignaturas as $asignatura) {
    if ($cols[$asignatura["id"]]["cant"] == 0) {
        $cols[$asignatura["id"]]["visible"] = false;
    } else {
        $cols[$asignatura["id"]]["visible"] = true;
        $cant++;
    }
}

$anchoTotal = 140;
$anchoCol = $anchoTotal / $cant;
if ($anchoCol > 20) {
    $anchoCol = 20;
}

foreach ($asignaturas as $asignatura) {
    if ($cols[$asignatura["id"]]["visible"]) {
        $cols[$asignatura["id"]]["width"] = $anchoCol;
    }
}

//vd($alumnos);
$extraDataSelect = "
    select n.nombre as nivel, anio.nombre as anio, d.nombre as division
	from alumno a
		inner join alumno_division ad on ad.Alumno_id = a.id and ad.ciclo_id = $ciclo_id
		inner join division d on d.id = ad.Division_id
		inner join anio on anio.id = d.Anio_id
		inner join nivel n on n.id = anio.Nivel_id
        where ad.division_id = $division_id
        order by n.orden, anio.orden, d.nombre, a.apellido, a.nombre
";
$extraData = Helpers::qry($extraDataSelect);
$extraData["periodo"] = $t3["nombre"];

class MyExcel extends Excel {

    public function output() {
        $styleArray = array(
                'borders' => array(
                        'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                                'color' => array('argb' => '000'),
                        ),
                        'outline' => array(
                                'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                                'color' => array('argb' => '000'),
                        ),
                ),
        );
        $this->as->getStyle("A$this->firstRow:$this->lastCol$this->lastRow")->applyFromArray($styleArray);
        parent::output();
    }

    public function printTitle() {
        $anio = date("Y", time());
        $row = $this->nextRow;
        $this->as->SetCellValue("A$row", "Alumnos con Riesgo Académico");
        $this->as->SetCellValue("H$row", $this->extraData["anio"] . " " . $this->extraData["division"] . " $anio");
        $this->as->SetCellValue("F" . ($row + 1), "Ref: Nota del " . $this->extraData["periodo"] . " necesaria para acreditar");
        $this->nextRow = 3;
        $this->as->getRowDimension(3)->setRowHeight(30);
        $this->as->getStyle('A3:A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->as->getStyle('A3:A3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->as->getStyle('B3:Z3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
        $this->as->getStyle('B3:Z3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->as->getStyle('B3:Z3')->getAlignment()->setWrapText(true);
        parent::printTitle();
    }

}

$options = array(
        "paperSize" => "legal",
        "orientation" => "L",
        "fitToPage" => true,
        "marginTop" => .5,
        "marginLeft" => .3,
        "marginBottom" => .5,
        "marginRight" => .3,
        "fitToWidth" => 1,
        "fitToHeight" => 1,
);

$excel = new MyExcel($alumnos, $cols, $fileName, $extraData, $options);
$excel->run();
$excel->output();
//Helpers::excel($rows, $cols, $fileName);


