<?php

$fileName = "alumnosConSusFamiliasInforme";
$ciclo_id = Ciclo::getCicloIdParaInformesEducativo();

$sexoAnd = $sexo ? " and a.sexo = \"$sexo\" " : "";
$row = Yii::app()->db->createCommand("
        select n.nombre as nivel, a.nombre as anio, d.nombre as division
          from division d
            inner join anio a on a.id = d.Anio_id
            inner join nivel n on n.id = a.Nivel_id
          where d.id = $division_id
      ")->query()->read();
$titulo = $row["nivel"] . " " . $row["anio"] . " " . $row["division"];
$rows = Yii::app()->db->createCommand("
     select concat(a.apellido, \", \", a.nombre) as alumno, concat(p.apellido, \", \", p.nombre) as familiar,
			 pt.nombre as tipo,
			 (select case when (a.vive_con_id = p.id) then \"Si\" else \"No\" end) as vive_con,
			 a.email as email_alumno, p.email as email_familiar
	from familia f
		inner join pariente p on p.Familia_id = f.id
		inner join pariente_tipo pt on pt.id = p.Pariente_Tipo_id
		inner join alumno a on a.Familia_id = f.id
                inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_admin = 1
                inner join alumno_division ad on ad.Alumno_id = a.id
         where ad.division_id = $division_id and ad.Ciclo_id = $ciclo_id /*and ad.activo*/ and not ad.borrado /*((ad.activo or ad.promocionado or ad.egresado)*/
        order by alumno, vive_con desc, familiar
    ")->queryAll();



$cols = array(
        "alumno" => array("title" => "Alumno"),
        "familiar" => array("title" => "Familiar"),
        "email_alumno" => array("title" => "Email Alumno"),
        "email_familiar" => array("title" => "Email Familiar"),
        "vive_con" => array("title" => "Vive Aquí"),
);


$excel = new Excel($rows, $cols, $fileName);
$excel->run();
$excel->output();


