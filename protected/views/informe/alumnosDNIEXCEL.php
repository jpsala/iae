<?php

$fileName = "InformeAlumnosConDNI.xls";
$ciclo_id = Ciclo::getCicloIdParaInformesEducativo();

$sexoAnd = $sexo ? " and a.sexo = \"$sexo\" " : "";
$row = Yii::app()->db->createCommand("
   select n.nombre as nivel, a.nombre as anio, d.nombre as division
     from division d
       inner join anio a on a.id = d.Anio_id
       inner join nivel n on n.id = a.Nivel_id
     where d.id = $division_id
 ")->query()->read();
$titulo = $row["nivel"] . " " . $row["anio"] . " " . $row["division"];
$rows = Yii::app()->db->createCommand("
   select concat(a.apellido, \", \", a.nombre) as nombre_alumno, 
      a.numero_documento as dni_alumno, pa.nombre as nacionalidad_alumno, 
      a.fecha_nacimiento as fecha_nacimiento_alumno, 
      concat(p.apellido, \", \", p.nombre) as nombre_tutor, 
      pa2.nombre as nacionalidad_tutor,
      td.nombre_corto as tipo_doc_tutor, p.numero_documento as doc_tutor, p.profesion, 
      concat(p.calle, \" \", p.numero, \" \", p.piso, \" \", p.departamento) as domicilio_tutor,
      p.telefono_casa, p.telefono_celular
   from alumno a
      inner join alumno_division ad on ad.Alumno_id = a.id
      inner join division d on d.id = ad.Division_id and ad.ciclo_id = $ciclo_id
      left join pariente p on a.Familia_id = p.Familia_id and a.vive_con_id = p.id
      left join localidad l on l.id = a.localidad_id
      left join pais pa on pa.id = l.Pais_id
      left join localidad l2 on l2.id = p.nacionalidad
      left join pais pa2 on pa2.id = l2.Pais_id
      left join tipo_documento td on td.id = p.tipo_documento_id      
   where a.activo = 1 and d.id = $division_id $sexoAnd
   order by a.apellido, a.nombre
    ")->queryAll();




$cols = array(
    "nombre_alumno" => array("title" => "Alumno"),
    "dni_alumno" => array("title" => "DNI"),
    "nacionalidad_alumno" => array("title" => "Nacionalidad"),
    "fecha_nacimiento_alumno" => array("title" => "Fecha Nac.", "format" => "date"),
    "nombre_tutor" => array("title" => "Tutor"),
);


$excel = new Excel($rows, $cols, $fileName);
$excel->run();
$excel->output();


