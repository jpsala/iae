<?php

class MyPDF extends PDF {

    public $fsize;
    public $orientacion;

    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $header = array(), $titulo = "", $fsize = null) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
        $this->fsize = $fsize;
        $this->orientacion = $orientation;
        $this->SetFontSize($fsize);
        $this->titulo = $titulo;
        $this->header = $header;
        $this->SetCreator(PDF_CREATOR);
        $this->SetAuthor('');
        $this->SetTitle($titulo);
        $this->SetSubject('IAE');
        $this->SetKeywords('TCPDF, PDF, xxx');
        $this->SetPrintHeader(true);
        $this->SetPrintFooter(false);

        $this->SetAutoPageBreak(true);
    }

    public function Header() {
        $this->SetY(4);
        $this->SetX(15);
        if ($this->titulo) {
            $this->setJPEGQuality(90);
            //$this->Image('images/logo.jpg'); 
            $this->Image('images/logo1BN.png');

            if ($this->orientacion == 'P') {
                $this->SetFontSize(11);
                $this->Cell(180, 24, $this->titulo, 1, 1, "C", false, "", "", "", "");
            } else {
                $this->SetFontSize(8);
                $this->Cell(268, 24, $this->titulo, 1, 1, "C", false, "", "", "", "");
            }
        }

        $this->SetX(15);
        ////$this->SetFontSize(11);

        foreach ($this->header as $col) {
            $this->cell($col[0], 0, $col[1], 0, "", "C");
        }
    }

    public function Footer() {
        $this->SetY(0);
    }

    public function CreateTextBox($textval, $x = 0, $y, $width = 0, $height = 16, $fontsize = 10, $fontstyle = '', $align = 'L') {


        $this->SetXY($x, $y); // 20 = margin left
        $this->SetFont(PDF_FONT_NAME_MAIN, $fontstyle, $fontsize);
        $this->Cell($width, $height, $textval, 0, false, $align);
    }

    public function renderRow($row) {
        $this->SetX(15);
        $nrow = 0;

        foreach ($this->header as $headerRow) {
            //MultiCell(55, 5, $txt, 0, '', 0, 1, '', '', true);  
            $this->cell($headerRow[0], 7, $row[$nrow], 1, $nrow + 1 == count($this->header) ? 2 : 0);
            $nrow++;
        }
    }

}

$ciclo_id = Ciclo::getCicloIdParaInformesEducativo();

$sexoAnd = $sexo ? " and a.sexo = \"$sexo\" " : "";
$row = Yii::app()->db->createCommand("
                select n.nombre as nivel, a.nombre as anio, d.nombre as division
                  from division d
                    inner join anio a on a.id = d.Anio_id
                    inner join nivel n on n.id = a.Nivel_id
                  where d.id = $division_id
              ")->query()->read();
$titulo = $row["nivel"] . " " . $row["anio"] . " " . $row["division"];
$rows = Yii::app()->db->createCommand("
                select concat(a.apellido, \", \", a.nombre) as nombre_alumno, a.numero_documento as dni_alumno, pa.nombre as nacionalidad_alumno, 
                       a.fecha_nacimiento as fecha_nacimiento_alumno, concat(p.apellido, \", \", p.nombre) as nombre_tutor, pa2.nombre as nacionalidad_tutor,
                       td.nombre_corto as tipo_doc_tutor, p.numero_documento as doc_tutor, p.profesion, concat(p.calle, \" \", p.numero, \" \", p.piso, \" \", p.departamento) as domicilio_tutor,
                       p.telefono_casa, p.telefono_celular
                  from alumno a
                    inner join alumno_division ad on ad.Alumno_id = a.id
                              inner join division d on d.id = ad.Division_id and ad.ciclo_id = $ciclo_id
                    left join pariente p on a.Familia_id = p.Familia_id and a.vive_con_id = p.id
                    left join localidad l on l.id = a.localidad_id
                    left join pais pa on pa.id = l.Pais_id
                    left join localidad l2 on l2.id = p.nacionalidad
                    left join pais pa2 on pa2.id = l2.Pais_id
                    left join tipo_documento td on td.id = p.tipo_documento_id      
                  where a.activo = 1 and d.id = $division_id $sexoAnd
                  order by a.apellido, a.nombre
              ")->queryAll();
$header = array(
        array(150, "Alumno"),
        array(30, "DNI"),
);

$pdf = new MyPDF("P", 'mm', 'A4', true, 'UTF-8', false, $header, $titulo, 11);
$pdf->SetMargins(0, 40, 0, true);
$pdf->AddPage();
foreach ($rows as $row) {
    $pdf->renderRow(array(
            substr($row["nombre_alumno"], 0, 31),
            ucwords(strtolower($row["dni_alumno"])),
    ));
}
$pdf->Output();
?>
