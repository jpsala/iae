<?php

$fileName = "InasistenciasAlumno.xls";
$fecha_desde = Helpers::date("Y/m/d", $fecha_desde);
$fecha_hasta = Helpers::date("Y/m/d", $fecha_hasta);
$ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();

$s = "
        select it.id as inasistencia_tipo_id, 
                    it.nombre as inasistencia_tipo_nombre, id.fecha, id.detalle as detalle, id.id as inasistencia_detalle_id, i.logica_periodo_id,
                    coalesce(it.valor,1) as valor, it.valor, i.cantidad
        from alumno_division ad 
                left join inasistencia i on i.alumno_division_id = ad.id
                left join inasistencia_detalle id on i.id = id.inasistencia_id
                left join inasistencia_tipo it on it.id = id.inasistencia_tipo_id 
                inner join alumno a on a.id = ad.alumno_id and a.id = $alumno_id
        where ad.ciclo_id = $ciclo_id and a.activo = 1 and inasistencia_tipo_id is not null and
                     id.fecha between \"$fecha_desde\" and \"$fecha_hasta\"
        order by id.fecha asc
      ";
//vd($s);
$rows = Yii::app()->db->createCommand($s)->queryAll();

$extraData = Yii::app()->db->createCommand("
        select concat(a.apellido, ', ', a.nombre) as alumno
            from alumno_division ad 
                inner join alumno a on a.id = ad.alumno_id and a.id = $alumno_id
      ")->queryRow();

$cols = array(
        "fecha" => array("title" => "Fecha", "format" => "date", "width" => 11),
        "inasistencia_tipo_nombre" => array("title" => "Tipo de Inas.", "width" => 19),
        "detalle" => array("title" => "Detalle", "width" => 35),
        "valor" => array("title" => "Cantidad", "total" => true),
);

class MyExcel extends Excel {

    function printTitle() {
        $this->nextRow = 3;
        $this->as->SetCellValue("A1", "Inasistencias " . $this->extraData["alumno"]);
        parent::printTitle();
    }

}

$excel = new MyExcel($rows, $cols, $fileName);
$excel->extraData = $extraData;
$excel->run();
$excel->output();


