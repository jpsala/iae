<?php

class ImprimeBoletines extends stdClass {

    private $alumnos, $nivel_id;
    private $pdf, $margenX, $margenY;
    private $numeros = array("Cero", "Uno", "Dos", "Tres", "Cuatro", "Cinco", "Seis", "Siete", "Ocho", "Nueve", "Diez");
    private $logicaPeriodos, $fecha, $cicloNombre;

    public function __construct($division_id, $fecha) {
        $this->cicloNombre = Ciclo::getActivoNombre();
        $this->alumnos = Division::getNotasPorPeriodo($division_id, null, 1);
        //vd($this->alumnos);
        //vd($this->alumnos);
        $this->nivel_id = Helpers::qryScalar("
            select a.nivel_id
            from anio a
                inner join division d on a.id = d.`Anio_id` and d.id = $division_id
            limit 1
            ");
        $logica_id = Logica::getLogicaIdPorNivel($this->nivel_id);
        $this->logicaPeriodos = Helpers::qryAll("
            select lp.abrev  from logica_periodo lp
                where lp.Logica_id = $logica_id and lp.imprime_en_boletin = 1
                order by lp.orden
          ");
        $this->fecha = $fecha;

        //echo("<pre>");
        //var_export($this->alumnos);
        //var_dump($this->alumnos);
    }

    public function imprime() {
        $this->pdf = new PDF("P", "mm", "LETTER");
        $this->pdf->SetCreator(PDF_CREATOR);
        $this->pdf->SetAuthor('JP');
        $this->pdf->SetTitle('Boletines');
        $this->pdf->SetSubject('Stickers IAE');
        $this->pdf->SetKeywords('TCPDF, PDF, Stickers');
        $this->pdf->SetPrintHeader(false);
        $this->pdf->SetPrintFooter(false);
        $this->pdf->SetAutoPageBreak(false);
        $this->margenX = 3;
        $this->margenY = 0;
        $parteNro = 1;
        $this->pdf->AddPage();
        vd($this->alumnos);
        foreach ($this->alumnos as $alumnoNombre => $alumno) {
            //vd($alumno);
            //vd($alumno);
            $this->imprimeParte(
                    1, $alumnoNombre, $alumno["notas"], $alumno["datos"], $alumno["amonestaciones"], $alumno["amonestaciones-total"], $alumno["inasistencias"], $alumno["inasistencias-total"], $alumno["desempenio"]
            );
            $this->imprimeParte(
                    2, $alumnoNombre, $alumno["notas"], $alumno["datos"], $alumno["amonestaciones"], $alumno["amonestaciones-total"], $alumno["inasistencias"], $alumno["inasistencias-total"], $alumno["desempenio"]
            );
            $this->pdf->AddPage();
        }
        $this->pdf->Output();
    }

    private function imprimeParte($parteNro, $alumno, $notasAlumno, $datosAlumno, $amonestaciones, $amonestacionesTotal
    , $inasistencias, $inasistenciasTotal, $desempenio) {
        $headerFontStyle = '';
        if ($parteNro == 1) {
            $x = $this->margenX;
            $y = $this->margenY;
            $amonestacionesY = $y + 80;
        } else if ($parteNro == 2) {
            $x = $this->margenX;
            $y = ($this->pdf->getPageHeight() / 2);
            $amonestacionesY = $y + 85;
        }

        $xNota = 60;
        $espacioEntreNotas = 19;
        $fontSizeHeader = 8;
        $fontSizeMateria = 8;
        $fontSizeNotas = 8;
        $fontSizeTitulo = 8;
        $notaWidth = 18;

        $this->pdf->textBox(array("txt" => "INFORMES", "y" => $y, "fontsize" => $fontSizeTitulo, "align" => "C"));
        $this->pdf->textBox(array("txt" => "AREAS/ASIGNATURAS", "y" => $y, "x" => $x, "fontsize" => $fontSizeHeader, "maxh" => 10, "valign" => "B"));

        $x = $xNota;
        foreach ($this->logicaPeriodos as $lp) {
            $this->pdf->textBox(array(
                    "txt" => $lp["abrev"], "x" => $x+=$espacioEntreNotas, "y" => $y,
                    "fontsize" => $fontSizeHeader, "align" => "C", "w" => $notaWidth, "maxh" => 10, "valign" => "B"
            ));
        }
        $y+=12;
        foreach ($notasAlumno as $materia => $notas) {
            $x = $this->margenX;
            $this->pdf->CreateTextBox($materia, $x, $y, null, null, $fontSizeMateria, $headerFontStyle);
            $x = $xNota;
            foreach ($notas as $nota) {
                $this->pdf->CreateTextBox($nota, $x+=$espacioEntreNotas, $y, $notaWidth, null, $fontSizeNotas, $headerFontStyle, "C");
            }
            $y+=4;
        }
        $x = $this->margenX;
        $y = $amonestacionesY;
        $this->pdf->CreateTextBox($amonestacionesTotal, $x + 140, $y, null, null, $fontSizeMateria);
        $this->pdf->CreateTextBox("CONDUCTA", $x, $y, null, null, $fontSizeMateria);
        $x = $xNota;
        foreach ($amonestaciones as $nota) {
            $nota = $nota > 0 ? $nota : "--";
            $this->pdf->CreateTextBox($nota, $x+=$espacioEntreNotas, $y, $notaWidth, null, $fontSizeNotas, $headerFontStyle, "C");
        }
        $x = $this->margenX;
        $y+=10;
        $this->pdf->CreateTextBox("INASISTENCIAS", $x, $y, null, null, $fontSizeMateria);
        $this->pdf->CreateTextBox($inasistenciasTotal, $x + 140, $y, null, null, $fontSizeMateria);
        $x = $xNota;
        foreach ($inasistencias as $nota) {
            $nota = $nota > 0 ? $nota : "--";
            $this->pdf->CreateTextBox($nota, $x+=$espacioEntreNotas, $y, $notaWidth, null, $fontSizeNotas, $headerFontStyle, "C");
        }
        $x = $this->margenX;
        $y += 10;
        $this->pdf->textBox(array("txt" => "Firma del Padre, Madre o Tutor", "x" => $x, "y" => $y, "w" => 13, "fontsize" => 7));
        $this->pdf->textBox(array(
                "txt" => "Firma del Director", "x" => $x + 47, "y" => $y, "w" => 12,
                "fontsize" => 7, "valign" => "T"
        ));
        if (($this->nivel_id == '2') || ($this->nivel_id == '3')) {
            $this->pdf->textBox(array(
                    "txt" => "Firma del Maestro", "x" => $x + 80, "y" => $y, "w" => 12,
                    "fontsize" => 7, "valign" => "T"
            ));
        }
        if ($desempenio) {
            $this->pdf->textBox(array("txt" => "Desempeño Global " . $desempenio, "x" => 160, "y" => $y + 1, "w" => 75, "fontsize" => 7));
            $this->pdf->textBox(array("txt" => "Promovido a " . $datosAlumno["siguiente_division"], "x" => 160, "y" => $y + 5, "w" => 75, "fontsize" => 7));
        }
        $y += 14;
        $x = $this->margenX;
        $this->pdf->textBox(array("txt" => "CICLO LECTIVO " . $this->cicloNombre, "x" => $x, "y" => $y, "w" => 35, "fontsize" => 7));
        $this->pdf->textBox(array("txt" => "CALIFICACIONES AL " . $this->fecha, "x" => $x + 48, "y" => $y, "w" => 45, "fontsize" => 7));
        $y += 8;
        $x = $this->margenX + 32;
        $this->pdf->CreateTextBox($alumno, $x, $y, null, null, $fontSizeHeader, $headerFontStyle);
        $this->pdf->CreateTextBox($datosAlumno["nivel"], $x + 56, $y, null, null, $fontSizeHeader, $headerFontStyle);
        $this->pdf->CreateTextBox($datosAlumno["anio"], $x + 81, $y, null, null, $fontSizeHeader, $headerFontStyle);
        $this->pdf->CreateTextBox($datosAlumno["division"], $x + 105, $y, null, null, $fontSizeHeader, $headerFontStyle);
        $this->pdf->CreateTextBox($datosAlumno["matricula"], $x + 125, $y, null, null, $fontSizeHeader, $headerFontStyle);
    }

}

$i = new ImprimeBoletines($division_id, $fecha);
$i->imprime();
?>
