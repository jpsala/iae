<?php

class ImprimeBoletines extends stdClass {

  private $alumnos, $nivel_id;
  private $pdf, $margenX, $margenY;
  private $numeros = array("Cero", "Uno", "Dos", "Tres", "Cuatro", "Cinco", "Seis", "Siete", "Ocho", "Nueve", "Diez");
  private $logicaPeriodos, $fecha, $cicloNombre;

  public function __construct($division_id, $fecha) {
    $this->alumnos = NotasAdmin::getNotasDivision($division_id); //get notas
    // vd2($this->alumnos);
    $this->logicaPeriodos = NotasAdmin::getPeriodos();
    //vd($this->alumnos);
    $items = NotasAdmin::getItems();
    $this->fecha = $fecha;
  }

  public function imprime() {
    $this->pdf = new PDF("P", "mm", "LETTER");
    $this->pdf->SetCreator(PDF_CREATOR);
    $this->pdf->SetAuthor('JP');
    $this->pdf->SetTitle('Boletines');
    $this->pdf->SetSubject('Stickers IAE');
    $this->pdf->SetKeywords('TCPDF, PDF, Stickers');
    $this->pdf->SetPrintHeader(false);
    $this->pdf->SetPrintFooter(false);
    $this->pdf->SetAutoPageBreak(false);
    $this->margenX = 4;
    $this->margenY = 2;
    $this->pdf->AddPage();
    foreach ($this->alumnos as $alumnoNombre => $alumnoData) {
      $this->imprimeParte(1, $alumnoNombre, $alumnoData);
      $this->imprimeParte(2, $alumnoNombre, $alumnoData);
      $this->pdf->AddPage();
    }
    $this->pdf->Output();
  }

  private function imprimeParte($parteNro, $alumno, $alumnoData) {
    $headerFontStyle = '';
    if ($parteNro == 1) {
      $x = $this->margenX;
      $y = $this->margenY;
      $amonestacionesY = $y + 77;
    } else if ($parteNro == 2) {
      $x = $this->margenX;
      $y = ($this->pdf->getPageHeight() / 2);
      $amonestacionesY = $y + 73;
    }

    $xNota = 60;
    $espacioEntreNotas = 18;
    $fontSizeHeader = 8;
    $fontSizeMateria = 8;
    $fontSizeNotas = 8;
    $fontSizeTitulo = 8;
    $notaWidth = 18;

    if ($parteNro == 2) {
      $y -= 4;
    }
    $this->pdf->textBox(array("txt" => "INFORMES", "y" => $y + 2, "fontsize" => $fontSizeTitulo, "align" => "C"));
    $this->pdf->textBox(array("txt" => "AREAS/ASIGNATURAS", "y" => $y, "x" => $x + 5, "fontsize" => $fontSizeHeader, "maxh" => 10, "valign" => "B"));

    $x = $xNota;
    foreach ($this->logicaPeriodos as $lp) {
      if (!$lp["imprime_en_boletin"]) {
        continue;
      }
      $this->pdf->textBox(array("txt" => $lp["abrev"], "x" => $x += $espacioEntreNotas, "y" => $y, "fontsize" => $fontSizeHeader, "align" => "C", "w" => $notaWidth, "maxh" => 10, "valign" => "B"));
    }
    $y += 12;

//			vd($alumnoData);
    //borrar
    // vd2($alumnoData["notasPeriodos"] );
    foreach ($alumnoData["notasPeriodos"] as $materia => $notas) {
      $this->pdf->CreateTextBox( trim($materia), $this->margenX + 5, $y, null, null, $fontSizeMateria, $headerFontStyle);
      $x = $xNota;
      foreach ($notas as $nota) {
        if (!$nota["imprime_en_boletin"]) {
          continue;
        }
        $this->pdf->CreateTextBox($nota["nota"], $x += $espacioEntreNotas, $y, $notaWidth, null, $fontSizeNotas, $headerFontStyle, "C");
      }
      $y += 4;
    }
    $y = $amonestacionesY;
      $nota = $alumnoData["conducta"]["amonestacionesFinal"];
      $nota = $nota > 0 ? $nota : "--";
			$this->pdf->CreateTextBox($nota, $this->margenX + 38,
					$y, null, null, $fontSizeMateria);
			$this->pdf->CreateTextBox("CONDUCTA", $this->margenX+3, $y, null, null, $fontSizeMateria);
			$x = $xNota;
			foreach ($alumnoData["conducta"]["amonestaciones"] as $nota) {
        $nota = $nota > 0 ? $nota : "--";
			  $this->pdf->CreateTextBox($nota, $x+=$espacioEntreNotas, $y, $notaWidth, null, $fontSizeNotas, $headerFontStyle, "C");
		}
    $x = $this->margenX;
    $y += 10;
    $this->pdf->CreateTextBox("INASISTENCIAS", $x + 3, $y, null, null, $fontSizeMateria);
    $this->pdf->CreateTextBox($alumnoData["inasistencia"]["totalInasistencias"], $x + 38, $y, null, null, $fontSizeMateria);
    $x = $xNota;
    foreach ($alumnoData["inasistencia"]["inasistencias"] as $nota) {
      $nota = $nota > 0 ? $nota : "--";
      $this->pdf->CreateTextBox($nota, $x += $espacioEntreNotas, $y, $notaWidth, null, $fontSizeNotas, $headerFontStyle, "C");
    }
    $x = $this->margenX;
    $y += 10;
    $this->pdf->textBox(array("txt" => "Firma del Padre, Madre o Tutor", "x" => $x + 3, "y" => $y, "w" => 13, "fontsize" => 7));
    $this->pdf->textBox(array("txt" => "Firma del Director", "x" => $x + 47, "y" => $y, "w" => 12, "fontsize" => 7, "valign" => "T"));
    if (($this->nivel_id == '2') || ($this->nivel_id == '3')) {
      $this->pdf->textBox(array("txt" => "Firma del Maestro", "x" => $x + 80, "y" => $y, "w" => 12, "fontsize" => 7, "valign" => "T"));
    }
//    vd2($alumnoData["datos"]["nivel"] );
    if ($alumnoData["datos"]["nivel"] !== 'EP 1') {
      if ($alumnoData["final"]["aprobado"]) {
        $this->pdf->textBox(array("txt" => "Desempeño Global " . $alumnoData["final"]["nota"], "x" => 160, "y" => $y + 1, "w" => 75, "fontsize" => 7));
      }
    }
    if ($alumnoData["final"]["aprobado"]) {
      $this->pdf->textBox(array("txt" => "Promovido a " . $alumnoData["datos"]["siguiente_division"], "x" => 160, "y" => $y + 5, "w" => 75, "fontsize" => 7));
    }

    $y += ($parteNro == 1) ? 14 : 14;
    $x = $this->margenX;
    $this->pdf->textBox(array("txt" => "CICLO LECTIVO " . $this->cicloNombre, "x" => $x, "y" => $y, "w" => 35, "fontsize" => 7));
    $this->pdf->textBox(array("txt" => "CALIFICACIONES AL " . $this->fecha, "x" => $x + 48, "y" => $y, "w" => 45, "fontsize" => 7));
    $y += 8;
    $x = $this->margenX + 31;
    $this->pdf->CreateTextBox($alumno, $x, $y, null, null, $fontSizeHeader, $headerFontStyle);
    $this->pdf->CreateTextBox($alumnoData["datos"]["nivel"], $x + 56, $y, null, null, $fontSizeHeader, $headerFontStyle);
    $this->pdf->CreateTextBox($alumnoData["datos"]["anio"], $x + 81, $y, null, null, $fontSizeHeader, $headerFontStyle);
    $this->pdf->CreateTextBox($alumnoData["datos"]["division"], $x + 105, $y, null, null, $fontSizeHeader, $headerFontStyle);
    $this->pdf->CreateTextBox($alumnoData["datos"]["matricula"], $x + 125, $y, null, null, $fontSizeHeader, $headerFontStyle);
  }

}

$i = new ImprimeBoletines($division_id, $fecha);
$i->imprime();
