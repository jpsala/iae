<style>
    .importe{text-align: right}
    .detalle{width:250px}
    .relleno{width:150px}
    .negrita{font-weight: bolder}
</style>
<?php
$q = Helpers::qryAll("
    select d.id as doc_id, concat(c.nombre,\"-\",d.numero) as comprob, d.fecha_creacion, d.detalle, d.total, d.saldo
        from doc d
		inner join socio s on s.id =d.Socio_id
		inner join talonario t on t.id = d.talonario_id
		inner join comprob c on c.id = t.comprob_id and c.signo_cc = 1 and d.anulado = 0
        where s.id = $socio_id and d.anulado  = 0 and d.activo = 1
         order by d.fecha_valor
");
?>
<table>
    <tr>
        <th>#</th>
        <th>Fecha</th>
        <th>Detalle</th>
        <th>Total</th>
        <th>Saldo</th>
        <th></th>
    </tr>
    <?php $total = 0; ?>
    <?php $totalGral = 0; ?>
    <?php foreach ($q as $r): ?>
        <?php
        $total += $r["total"];
        $totalGral += $r["total"];
        $doc_id = $r["doc_id"];
        $dets = Helpers::qryAll("
            select d.numero, c.nombre,  d.detalle, d.total, a.importe, d.fecha_creacion
                from doc_apl a
                    inner join doc d on d.id = a.Doc_id_origen
                    inner join doc doc2 on doc2.id = a.doc_id_destino
                    inner join socio s on s.id = d.Socio_id
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
            where a.doc_id_destino = $doc_id and d.anulado = 0 and doc2.anulado = 0
        ");
        ;
        ?>
        <tr>
            <td><?php echo $r["comprob"]; ?></td>
            <td><?php echo date("d/m/Y", strtotime($r["fecha_creacion"])); ?></td>
            <td class="detalle"><?php echo $r["detalle"]; ?></td>
            <td class="importe"><?php echo number_format($r["total"], 2); ?></td>
            <td class="negrita importe"><?php echo number_format($r["total"], 2); ?></td>
            <td class="relleno"><?php; ?></td>
        </tr>
        <?php $saldo = $r["total"]; ?>
        <?php foreach ($dets as $det): ?>
            <?php
            $saldo -= $det["importe"];
            $totalGral -= $det["importe"];
            ?>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;<?php echo $det["nombre"] . "-" . $det["numero"]; ?></td>
                <td><?php echo date("d/m/Y", strtotime($r["fecha_creacion"])); ?></td>
                <td><?php echo $det["detalle"]; ?></td>
                <td class="importe"><?php echo number_format($det["importe"] * -1, 2); ?></td>
                <td class="importe"><?php echo number_format($saldo, 2); ?></td>
                <td class="relleno"></td>
            </tr>
        <?php endforeach; ?>
        <tr>
            <td class="negrita" colspan="4">Saldo</td>
            <td class="importe negrita" colspan="1"><?php echo number_format($saldo, 2); ?></td>
            <td></td>
        </tr>
    <?php endforeach; ?>
    <?php
    $dets = Helpers::qryAll("
            select d.numero, c.nombre,  d.detalle, d.total, d.saldo, d.fecha_creacion
                from doc d
                    inner join socio s on s.id = d.Socio_id
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
            where s.id = $socio_id and d.anulado = 0 and c.signo_cc = -1 and d.saldo <> 0
        ");
    ;
    ?>
    <?php $resto = false; ?>
    <?php foreach ($dets as $det): ?>
        <?php
        $saldo -= $det["total"];
        $totalGral -= $det["total"];
        $resto = true;
        ?>
        <tr>
            <td><?php echo $det["nombre"] . "-" . $det["numero"]; ?></td>
            <td><?php echo date("d/m/Y", strtotime($r["fecha_creacion"])); ?></td>
            <td><?php echo $det["detalle"]; ?></td>
            <td class="importe"><?php echo number_format($det["total"] * -1, 2); ?></td>
            <td class="importe"><?php echo number_format($saldo, 2); ?></td>
            <td class="relleno"></td>
        </tr>
    <?php endforeach; ?>
    <?php if ($resto): ?>
        <tr>
            <td class="negrita" colspan="4">Saldo</td>
            <td class="importe negrita" colspan="1"><?php echo number_format($totalGral, 2); ?></td>
            <td></td>
        </tr>
    <?php endif; ?>
</table>


<!--
delete a.* from doc_apl a
        inner join doc d on d.id = a.Doc_id_origen
        inner join socio s on s.id = d.Socio_id
        inner join proveedor p on p.id = s.Proveedor_id and p.codigo = "90";
update doc d
        inner join socio s on s.id =d.Socio_id
        inner join proveedor p on p.id = s.Proveedor_id and p.id = 90
        inner join talonario t on t.id = d.talonario_id
        inner join comprob c on c.id = t.comprob_id and c.signo_cc = 1
    set d.saldo = d.total;
update doc d
        inner join socio s on s.id =d.Socio_id
        inner join proveedor p on p.id = s.Proveedor_id and p.id = 90
        inner join talonario t on t.id = d.talonario_id
        inner join comprob c on c.id = t.comprob_id and c.signo_cc = -1
    set d.saldo = d.total;
-->
