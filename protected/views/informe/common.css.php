<style type="text/css" media="print">
  table {border-collapse:separate;border-spacing:0;}
  tr:nth-child(2n) {
    background-color: whitesmoke !important;
  }
  .pagebreak { page-break-before: always; }
</style>
<style type="text/css">
  table{empty-cells: show !important}
  .table-border{border-collapse: collapse!important;}
  .table-border td, .table-border th{ border: thin solid black;} 
</style>