<?php

	include 'informe.php';
	$fecha_desde = date("Y/m/d", mystrtotime($fecha_desde));
	$fecha_hasta = date("Y/m/d", mystrtotime($fecha_hasta)) . " 23:59:59";
	$whereConcepto = $concepto_id ? " d.concepto_id = $concepto_id " : " true ";
	$select = "select d.fecha_creacion as fecha, c.nombre as comprob,  concat(LPAD(d.sucursal, 4, '0'),'-',LPAD(d.numero, 8, '0')) as Número,
                         (d.total * c.signo_caja) as total, 
						concat(' ', 
							case when d.detalle <> '' and d.detalle is not null then d.detalle else c2.nombre end 
						) as Detalle, d.id
                from doc d
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
				inner join concepto c2 on c2.id = d.concepto_id
                where d.fecha_creacion BETWEEN \"$fecha_desde\" and \"$fecha_hasta\"
                            and $whereConcepto and d.anulado = 0 and d.activo = 1
                order by d.fecha_creacion
           ";
//vd($select);
	$data = Helpers::qryAll($select);

	$colsDef = array(
			"id" => array("visible" => false),
			"comprob" => array("visible" => true),
			"Número" => array("visible" => true),
			"detalle" => array("visible" => true),
			"total" => array("width" => 20, "currency" => true, "align" => "R", "sum" => true),
			"fecha" => array("date" => "d/m/Y"),
	);

	$colsDefDetail = array(
	);

	$nombre_concepto = $concepto_id ? Helpers::qryScalar("
		select nombre
				from concepto
				where id = $concepto_id
	") : "Todos";

	$options = array(
			"data" => $data,
			"marginY" => 4,
			"marginX" => 10,
			"title" => "Concepto de Gastos entre el " . date("d/m/Y", mystrtotime($fecha_desde)) . " y el " . date("d/m/Y", mystrtotime($fecha_hasta)),
			"title2" => $nombre_concepto,
	);
	$i = new Informe($options, $colsDef, $colsDefDetail);
	$i->render();
	