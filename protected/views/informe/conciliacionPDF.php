<?php

include("informe.php");
set_time_limit(2000);

$select = " select IF((dv.tipo=3 or dv.tipo=6),dv.fecha,d.fecha_creacion) as fecha1,  d.detalle,
                concat(LPAD(d.sucursal, 4, '0'),'-',LPAD(d.numero, 8, '0'))  as docnumero,  dv.obs, (dv.importe * c.signo_banco) as importe, dv.numero
                from doc_valor dv
		inner join doc d on d.id = dv.Doc_id
		inner join talonario t on t.id = d.talonario_id
		inner join comprob c on c.id = t.comprob_id
               	where d.anulado = 0 and dv.conciliacion_id = $conciliacion 
                order by fecha1
           ";

$data = Helpers::qryAll($select);

$cuenta = Helpers::qryScalar("
    select concat(nombre,', ' , numero_cuenta) as cuenta 
        from destino 
        where id =$destino_id;
");
$colsDef = array(
        "fecha1" => array("date" => "d/m/Y"),
        "detalle" => array("width" => 50),
        "docnumero" => array("width" => 30),  
        "obs" => array("width" => 50, "fontSize" => 3),
        "chq_entregado_a" => array("width" => 50),
        "numero" => array("width" => 50),
        "importe" => array("debeHaber" => "dh", "align" => "R", "sum" => true),
);

$colsDefDetail = "";

$options = array(
        "data" => $data,
        "marginY" => 4,
        "marginX" => 5,
        "title" => "Conciliacion Bancaria ",
        "title2" => $cuenta,
        "fontsize"=> 3,
);
$i = new Informe($options, $colsDef, $colsDefDetail);
$i->render();
?>