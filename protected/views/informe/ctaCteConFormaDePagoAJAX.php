<style type="text/css">
        #tablegenquickfilter-div{margin-top: 8px}
</style>
<?php
$fechaSaldo = date("Y/m/d", mystrtotime($fecha_desde) - 1);
$fechaSaldoMuestra = date("d/m/Y", mystrtotime($fecha_desde) - 1);
$fecha_desde = date("Y/m/d", mystrtotime($fecha_desde));
$fecha_hasta = date("Y/m/d", mystrtotime($fecha_hasta)) . " 23:59:59";
$saldoSelect = "select coalesce(saldo($socio_id, \"$fechaSaldo\"),0) as saldo";
$select = "
           select  'Saldo' as numero,\"$fechaSaldoMuestra\" as fecha,  '' as hora, '' as comprob,  'Saldo' as detalle, -1 as id,
                          coalesce(saldo($socio_id, \"$fechaSaldo\"),0) as total, \"\" as concepto
            union
            select  concat(LPAD(d.sucursal, 4, '0'),'-',LPAD(d.numero, 8, '0')) as numero, d.fecha_valor as fecha, TIME(d.fecha_creacion) as hora, c.abreviacion as comprob,  
                         d.detalle,d.id, abs(coalesce((d.total * c.signo_cc),0)) as total, co.nombre as concepto
                from doc d
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
                    left join socio s on s.id = d.socio_id
                    left join alumno a on a.id = s.alumno_id
                    left join concepto co on co.id = d.concepto_id
                    join doc_valor dv on dv.Doc_id = d.id
                    join doc_valor_tipo dvt on dvt.id = dv.tipo
                where d.fecha_valor BETWEEN \"$fecha_desde\" and \"$fecha_hasta\"
                            and s.id = $socio_id and d.anulado = 0 and d.activo = 1
                order by 2,3
        ";
$data = Helpers::qryAll($select);

foreach ($data as $key => $row) {

    $doc_id = $row["id"];
    $fechaDoc = $row["fecha"];
    $det = Helpers::qryAll("
        select t.nombre as tipo_valor, v.numero as numerodet, 
        case when v.fecha is null then \"$fechaDoc\" else v.fecha end as fechadet,  
        b.nombre as banco, d.nombre as destino, 
                    ch.descripcion as chequera, coalesce(v.importe,0) as importe,v.obs
                from doc_valor v
                    inner join doc_valor_tipo t on t.id = v.tipo
                    left join banco b on b.id = v.banco_id
                    left join destino d on d.id = v.Destino_id
                    left join chequera ch on ch.id = v.chequera_id
                where v.Doc_id =  $doc_id
    ");
    foreach ($det as $detKey => $detRow) {
        $data[$key]["detail"][$detKey]["detalle"] = $detRow["tipo_valor"];
        $data[$key]["detail"][$detKey]["numero"] = $detRow["numerodet"];
        $data[$key]["detail"][$detKey]["importe"] = $detRow["importe"];
    }
    $data[$key]["detailData"]["valores"] = $det;
    $data[$key]["detalle"] = $data[$key]["detalle"] ? $data[$key]["detalle"] : $data[$key]["concepto"];
}

$cols["valores"] = array(
        "tipo_valor" => array("title"=>"detalle","mb_convert_case" => MB_CASE_TITLE, "width" => 120),
        "numerodet" => array("title"=>"Número","width" => 20, "align" => "R"),
        "fechadet" => array("title"=>"fecha","date"=>true, "width" => 70),
        "importe" => array("currency", "align" => "R"),
        "banco" => array("visible"=>false,),
        "destino" => array("visible"=>false,),
        "chequera" => array("visible"=>false,),
        "obs" => array("visible"=>false,),
);
$cols["ctacte"] = array(
        "id" => array("visible" => false),
        "sucursal" => array("visible" => false),
        "numero" => array("visible" => true, "width"=>60, "title"=>"Número"),
        "concepto" => array("visible" => false),
        "comprob" => array("visible" => false),
        "fecha" => array("date" => "d/m/Y", "width"=>80),
        "hora" => array(),
        "detalle" => array("width" => 110, "visible" => false),
        "total" => array("currency" => true, "align" => "R", "sum" => true, "visible" => false),
);
$div = Helpers::getTable("ctacte", $data, $cols, array("title"=>false));
echo $div;
?>