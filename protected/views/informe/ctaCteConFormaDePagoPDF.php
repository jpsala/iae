<?php

include("informePDF.php");
$fechaSaldo = date("Y/m/d", mystrtotime($fecha_desde) - 1);
$fechaSaldoMuestra = date("d/m/Y", mystrtotime($fecha_desde) - 1);
$fecha_desde = date("Y/m/d", mystrtotime($fecha_desde));
$fecha_hasta = date("Y/m/d", mystrtotime($fecha_hasta)) . " 23:59:59";
$saldoSelect = "select coalesce(saldo($socio_id, \"$fechaSaldo\"),0) as saldo";
$select = "
           select  'Saldo' as Número,\"$fechaSaldoMuestra\" as fecha,  '' as hora, '' as comprob,  'Saldo' as detalle, -1 as id,
                          coalesce(saldo($socio_id, \"$fechaSaldo\"),0) as total, \"\" as concepto
            union
            select  concat(LPAD(d.sucursal, 4, '0'),'-',LPAD(d.numero, 8, '0')) as Número, d.fecha_valor as fecha, TIME(d.fecha_creacion) as hora, c.abreviacion as comprob,  
                         d.detalle,d.id, abs(coalesce((d.total * c.signo_cc),0)) as total, co.nombre as concepto
                from doc d
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
                    left join socio s on s.id = d.socio_id
                    left join alumno a on a.id = s.alumno_id
                    left join concepto co on co.id = d.concepto_id
                    join doc_valor dv on dv.Doc_id = d.id
                    join doc_valor_tipo dvt on dvt.id = dv.tipo
                where d.fecha_valor BETWEEN \"$fecha_desde\" and \"$fecha_hasta\"
                            and s.id = $socio_id and d.anulado = 0 and d.activo = 1
                order by 2,3
        ";

//vd($select);
$data = Helpers::qryAll($select);
foreach ($data as $key => $row) {
    $doc_id = $row["id"];
    $data[$key]["detalle"] = $data[$key]["detalle"] ? $data[$key]["detalle"] : $data[$key]["concepto"];
    $fechaDoc = $row["fecha"];
    $det = Helpers::qryAll("
        select t.nombre as tipo_valor, v.numero as numerodet, 
        case when v.fecha is null then \"$fechaDoc\" else v.fecha end as fechadet,  
        b.nombre as banco, d.nombre as destino, 
                    ch.descripcion as chequera, coalesce(v.importe,0) as importe,v.obs
                from doc_valor v
                    inner join doc_valor_tipo t on t.id = v.tipo
                    left join banco b on b.id = v.banco_id
                    left join destino d on d.id = v.Destino_id
                    left join chequera ch on ch.id = v.chequera_id
                where v.Doc_id =  $doc_id
    ");
    foreach ($det as $detKey=>$detRow) {
        $data[$key]["detail"][$detKey]["detalle"] = $detRow["tipo_valor"] ;
        $data[$key]["detail"][$detKey]["numero"] = $detRow["numerodet"];
        $data[$key]["detail"][$detKey]["banco"] = $detRow["banco"];
        $data[$key]["detail"][$detKey]["fecha"] = date('d/m/Y',strtotime($detRow["fechadet"]));
        $data[$key]["detail"][$detKey]["importe"] = $detRow["importe"];
    }
}
$alumno = Helpers::qry("
    select concat(a.apellido,', ' , a.nombre) as nombre, a.beca, concat(p.apellido, ', ', p.nombre) as tutor,
           concat(coalesce(p.calle, \"\"),\" \", coalesce(p.numero,\"\"), \" \", coalesce(p.piso,\"\")) as domicilio,
           p.telefono_casa as telefono_casa, p.telefono_trabajo as telefono_trabajo, p.telefono_celular as telefono_celular,
           n.nombre as nivel, an.nombre as anio, d.nombre as division, a.matricula, f.obs as obs_cobranza
        from alumno a
            inner join socio s on s.Alumno_id = a.id
		inner join familia f on f.id = a.familia_id
            left join pariente p on p.id = a.vive_con_id
            left join alumno_division ad on ad.Alumno_id = a.id and ad.activo
            left join division d on d.id = ad.division_id
            left join anio an on an.id = d.anio_id
            left join nivel n on n.id = an.nivel_id
        where s.id = $socio_id
");

$colsDef = array(
        "id" => array("visible" => false),
        "sucursal" => array("visible" => false),
        "numero" => array("visible" => true),
        "concepto" => array("visible" => false),
        "comprob" => array("visible" => false),
        "fecha" => array("date" => "d/m/Y"),
        "hora" => array(),
        "detalle" => array("width" => 110, "visible" => false),
        "total" => array("currency" => true, "align" => "R", "sum" => true, "visible" => false),
);

$colsDefDetail = array(
        "detalle" => array("mb_convert_case" => MB_CASE_TITLE, "width" => 70),
        "importe" => array("currency", "align" => "R"),
        "numero" => array("width" => 20, "align" => "C"),
//        "banco" => array("width" => 60),
//        "obs" => array("width" => 35, "align" => "L"),
        "fechadet" => array("date" => "d/m/Y", "width" => 24),
);

$options = array(
        "data" => $data,
        "marginY" => 10,
        "marginX" => 20,
        "title" => "Cta. Cte. de " . $alumno["nombre"] . " (" . $alumno["matricula"] . ")",
        "title2" => "entre el " . date("d/m/Y", mystrtotime($fecha_desde)) . " y el " . date("d/m/Y", mystrtotime($fecha_hasta)),
        "dataExtra" => $alumno,
);
setlocale(LC_ALL,"es-AR");
//vd($data);
class MiInforme extends Informe {

    private static $saldo = 0;

    public function customHeader($pdf, $x, $y, $args) {
        /* @var $pdf PDF */
        $top = $y;
        $dataAlumno = $args["dataExtra"];

        $txt = $dataAlumno["nivel"] . " " . $dataAlumno["anio"] . " " . $dataAlumno["division"];
        if ($dataAlumno["beca"] > 0) {
            $txt .= " - Beca:" . $dataAlumno["beca"] . "%";
        }
        $h = $pdf->getStringHeight(104, $txt);
        $pdf->textBox(array("txt" => $txt,
                "y" => $y, "x" => $x, "w" => 104, "h" => null, "align" => "L",));
        $y += $h;

        $txt = "Domicilio:" . $dataAlumno["domicilio"];
        $pdf->textBox(array("txt" => $txt,
                "y" => $y, "x" => $x, "w" => null, "h" => null, "align" => "L",));
        $y += 5;
        $txt = "";
        if ($dataAlumno["telefono_casa"]) {
            $txt.= "Teléfono casa:" . $dataAlumno["telefono_casa"];
        }
        if ($dataAlumno["telefono_trabajo"]) {
            $txt.= " - Trabajo:" . $dataAlumno["telefono_trabajo"];
        }
        if ($dataAlumno["telefono_celular"]) {
            $txt.= " - Celular:" . $dataAlumno["telefono_celular"];
        }
        $pdf->textBox(array("txt" => $txt,
                "y" => $y, "x" => $x, "w" => null, "h" => null, "align" => "L",));
        $y += 5;
        if ($dataAlumno["obs_cobranza"]) {
            $fs = $pdf->setFont(PDF_FONT_NAME_MAIN, "", 11);
            $txt = $dataAlumno["obs_cobranza"];
            //$txt = "1";
            $top+=2;
            $w = 100;
            $h = $pdf->getStringHeight($w, $txt);
            $x = $pdf->getPageWidth() - $w - 1;

            $cp = $pdf->setCellPaddings(2, 2, 1, 1);
            $pdf->textBox(array("txt" => $txt,
                    "y" => $top, "x" => $x, "w" => $w, "h" => $h, "align" => "L", "fontsize" => 10,
                    "border" => array('LTRB' => array('width' => 0.1)), "autopadding" => true,
            ));
            $pdf->SetCellPadding($cp);
            $pdf->setFillColor(255, 255, 255);
            $pdf->textBox(array("txt" => "Observaciones de cobranza:", "fontsize" => 10,
                    "y" => $top - 2, "x" => $x + 1, "fill" => true, "w" => 46,
            ));

            $y = (($top + $h) > $y ? ($top + $h) + 1 : $y + $h - 3);
            $pdf->setFont(PDF_FONT_NAME_MAIN, "", $fs);
        }
        return parent::customHeader($pdf, $x, $y + 1, $args);
    }

    public function getField($fieldName, $text, $record, $esTotal = false) {
        if ($fieldName == "saldo") {
            return self::$saldo += ($record["debe"] - $record["haber"]);
        } else {
            return parent::getField($fieldName,$text, $record, $esTotal);
        }
    }

    public function getTotal($fieldName, $text) {
        if ($fieldName == "saldo") {
            return self::$saldo;
        } else {
            return parent::getTotal($fieldName, $text);
        }
    }

}

$i = new MiInforme($options, $colsDef, $colsDefDetail);
$i->render();
?>