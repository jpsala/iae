<style type="text/css">
    .importe{width:80px;text-align: right}
</style>
<table>
    <?php $comprob = null; ?>
    <?php $keyAnt = null; ?>
    <?php $primeraVez = true; ?>
    <?php $totalComprob = 0; ?>
    <?php foreach ($rows as $key => $row): ?>
        <?php $importe = $row["DEBE"] - $row["HABER"]; ?>
        <?php $comprob = $comprob ? $comprob : $row["COMPROBANTE"]; ?>
        <?php if ($comprob !== $row["COMPROBANTE"]): ?>
                <?php $primeraVez = true; ?>
            <tr><td colspan="3"></td> <td class="importe"><?php echo number_format($totalComprob,2) ?></td></tr>
        <?php endif; ?>
        <tr>
            <?php if ($primeraVez): ?>
                <td><?php echo $row["FECHA"]; ?></td>
                <td><?php echo $row["COMPROBANTE"]; ?></td>
                <td><?php echo $row["DETALLE"]; ?></td>
                <td class="importe"><?php echo number_format($importe,2); ?></td>
                <?php $primeraVez = false; ?>
            <?php else: ?>
                <td>&nbsp;</td>
                <td><?php echo $row["COMPROBANTE"]; ?></td>
                <td><?php echo $row["DETALLE"]; ?></td>
                <td class="importe"><?php echo number_format($importe,2); ?></td>
            <?php endif; ?>

            <?php if ($comprob !== $row["COMPROBANTE"]): ?>
                <?php $totalComprob = 0; ?>
            <?php else: ?>
            <?php endif; ?>
            <?php $comprob = $row["COMPROBANTE"]; ?>
            <?php $totalComprob += $importe; ?>
            <?php $keyAnt = $key; ?>
        </tr>
    <?php endforeach; ?>
</table>