<style type="text/css">
   .anulado{text-decoration: line-through;}
   .inactivo *{color:lightseagreen}
   #total-table{width: 95.7%;}
   .total-td{text-align: right}
   .fecha-td{width:65px}
   .matricula-td{width:55px}
   .nombre-td{width:180px}
</style>
<table class="tablesorter"  id="recibos-table">
   <thead>
      <tr>
         <th>Fecha</th>
         <?php
         if (in_array($comprob_id, array(
                   Comprob::NC_VENTAS,
                   Comprob::ND_VENTAS,
                   Comprob::RECIBO_BANCO,
//					Comprob::RECIBO_VENTAS,
                   Comprob::FACTURA_VENTAS))):
            ?>
            <th>Matrícula</th>
         <?php endif; ?>
         <?php
         if (in_array($comprob_id, array(
                   Comprob::NC_VENTAS,
                   Comprob::ND_VENTAS,
                   Comprob::NC_COMPRAS,
                   Comprob::ND_COMPRAS,
                   Comprob::RECIBO_BANCO,
                   Comprob::RECIBO_VENTAS,
                   Comprob::OP,
                   Comprob::FACTURA_COMPRAS,
                   Comprob::FACTURA_VENTAS))):
            ?>
            <th>Nombre</th>
         <?php endif; ?>
         <?php
         if (in_array($comprob_id, array(
                   Comprob::NC_VENTAS,
                   Comprob::ND_VENTAS,
                   Comprob::NC_COMPRAS,
                   Comprob::ND_COMPRAS,
                   Comprob::AJUSTE_CAJA_ENTRADA,
                   Comprob::AJUSTE_CAJA_SALIDA,
                   Comprob::DEPOSITO_BANCARIO_SALIDA,
                   Comprob::DEPOSITO_BANCARIO_ENTRADA,
                   Comprob::TRANSFERENCIA_ENTRADA,
                   Comprob::TRANSFERENCIA_SALIDA,
                   Comprob::MOVIMIENTO_BANCARIO_D,
                   Comprob::MOVIMIENTO_BANCARIO_H))):
            ?>
            <th>Detalle</th>
         <?php endif; ?>

         <?php
         if (in_array($comprob_id, array(
                   Comprob::AJUSTE_CAJA_ENTRADA,
                   Comprob::AJUSTE_CAJA_SALIDA,
                   Comprob::DEPOSITO_BANCARIO_SALIDA,
                   Comprob::DEPOSITO_BANCARIO_ENTRADA,
                   Comprob::TRANSFERENCIA_ENTRADA,
                   Comprob::TRANSFERENCIA_SALIDA,
                   Comprob::MOVIMIENTO_BANCARIO_D,
                   Comprob::MOVIMIENTO_BANCARIO_H,
                   Comprob::ND_VENTAS,
                   Comprob::NC_VENTAS))):
            ?>
            <th>Concepto</th>
         <?php endif; ?>

         <?php
         if (in_array($comprob_id, array(
                   Comprob::MOVIMIENTO_BANCARIO_D,
                   Comprob::MOVIMIENTO_BANCARIO_H))):
            ?>
            <th>Banco</th>
         <?php endif; ?>

         <?php
         if (in_array($comprob_id, array(
                   Comprob::RECIBO_BANCO))):
            ?>
            <th>Fecha valor</th>
         <?php endif; ?>
         <th>Número</th>
         <th>Total</th>
         <th></th>
         <!--<th></th>-->
         <th></th>
      </tr>
   </thead>
   <tbody>
      <?php $total = 0; ?>
      <?php foreach ($rows as $row): ?>
         <?php $classAnulado = $row["anulado"] == 1 ? " anulado " : ""; ?>
         <?php $classInactivo = $row["activo"] == 0 ? " inactivo " : ""; ?>
         <tr comprob="<?php echo $row["comprob"]; ?>" class=" <?php echo $classAnulado . $classInactivo; ?> doc"
             doc_id="<?php echo $row["id"]; ?>">
            <td class="fecha-td"><?php echo (($row["comprob_id"] == Comprob::MOVIMIENTO_BANCARIO_D) || ($row["comprob_id"] == Comprob::MOVIMIENTO_BANCARIO_H)) ? date("d/m/Y", mystrtotime($row["fechadv"])) : date("d/m/Y", mystrtotime($row["fecha"])); ?></td>
            <?php
            if (in_array($comprob_id, array(
                      Comprob::NC_VENTAS,
                      Comprob::NC_ELECTRONICO,
                      Comprob::ND_VENTAS,
                      Comprob::ND_ELECTRONICO,
                      Comprob::RECIBO_BANCO,
                      Comprob::RECIBO_VENTAS,
                      Comprob::RECIBO_ELECTRONICO,
                      Comprob::FACTURA_VENTAS))):
               ?>
               <td class="matricula-td filterable"><?php echo $row["matricula"]; ?></td>
            <?php endif; ?>
            <?php
            if (in_array($comprob_id, array(
                      Comprob::NC_VENTAS,
                      Comprob::NC_ELECTRONICO,
                      Comprob::ND_VENTAS,
                      Comprob::ND_ELECTRONICO,
                      Comprob::NC_COMPRAS,
                      Comprob::ND_COMPRAS,
                      Comprob::RECIBO_BANCO,
                      Comprob::RECIBO_VENTAS,
                      Comprob::RECIBO_ELECTRONICO,
                      Comprob::OP,
                      Comprob::FACTURA_COMPRAS,
                      Comprob::FACTURA_VENTAS))):
               ?>
               <td  class="filterable nombre-td"><?php echo $row["nombre"]; ?></td>
            <?php endif; ?>
            <?php
            if (in_array($comprob_id, array(
                      Comprob::NC_COMPRAS,
                      Comprob::ND_COMPRAS,
                      Comprob::NC_VENTAS,
                      Comprob::NC_ELECTRONICO,
                      Comprob::ND_VENTAS,
                      Comprob::ND_ELECTRONICO,
                      Comprob::AJUSTE_CAJA_ENTRADA,
                      Comprob::AJUSTE_CAJA_SALIDA,
                      Comprob::DEPOSITO_BANCARIO_SALIDA,
                      Comprob::DEPOSITO_BANCARIO_ENTRADA,
                      Comprob::TRANSFERENCIA_ENTRADA,
                      Comprob::TRANSFERENCIA_SALIDA,
                      Comprob::MOVIMIENTO_BANCARIO_D,
                      Comprob::MOVIMIENTO_BANCARIO_H))):
               ?>
               <td><?php echo $row["detalle"] ?></td>
            <?php endif; ?>

            <?php
            if (in_array($comprob_id, array(
                      Comprob::AJUSTE_CAJA_ENTRADA,
                      Comprob::AJUSTE_CAJA_SALIDA,
                      Comprob::DEPOSITO_BANCARIO_SALIDA,
                      Comprob::DEPOSITO_BANCARIO_ENTRADA,
                      Comprob::TRANSFERENCIA_ENTRADA,
                      Comprob::TRANSFERENCIA_SALIDA,
                      Comprob::MOVIMIENTO_BANCARIO_D,
                      Comprob::MOVIMIENTO_BANCARIO_H,
                      Comprob::ND_VENTAS,
                      Comprob::ND_ELECTRONICO,
                      Comprob::NC_VENTAS,
                      Comprob::NC_ELECTRONICO))):
               ?>
               <td><?php echo $row["concepto_nombre"] ?></td>
            <?php endif; ?>

            <?php
            if (in_array($comprob_id, array(
                      Comprob::MOVIMIENTO_BANCARIO_D,
                      Comprob::MOVIMIENTO_BANCARIO_H))):
               ?>
               <td><?php echo $row["banco"] ?></td>
            <?php endif; ?>

            <?php
            if (in_array($comprob_id, array(
                      Comprob::RECIBO_BANCO
                  ))):
               ?>
               <td><?php echo date("d/m/Y", mystrtotime($row["fecha_valor"])) ?></td>
            <?php endif; ?>
            <td><?php echo str_pad($row["sucursal"], 4, "0", STR_PAD_LEFT) . "-" . str_pad($row["numero"], 8, "0", STR_PAD_LEFT); ?></td>
            <td class="importe filtrable"><?php echo number_format($row["total"], 2) ?></td>
            <?php
//            ve($row["anulado"]);
               if ($row["anulado"] === '0' and $row["activo"] === '1') {
               $total += $row["total"];
            }
            ?>
            <td filter="false" title="Re-Imprime" class="reimpresion" onclick="return reimpresion($(this));"></td>
            <!--<td filter="false" title="Modifica" class="modifica" onclick="return modifica($(this));"></td>-->
            <td filter="false" title="Anula" class="anula" onclick="return anula($(this));"></td>
         </tr>
<?php endforeach; ?>
<!--        <tr>
<td class="importe" colspan="5"><?php //echo number_format($total, 2);                     ?></td>
</tr>-->
   </tbody>
</table>
<table id="total-table">
   <tr>
      <td>Total</td>
      <td id="total-td" class="total-td"><?php echo number_format($total, 2); ?></td>
   </tr>
</table>
<script type="text/javascript">
   comprob_id = "<?php echo $comprob_id; ?>";
</script>
