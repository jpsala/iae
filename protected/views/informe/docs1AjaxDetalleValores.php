<div class="ui-widget ui-widget-content ui-corner-all"  id="det_<?php echo $doc_id; ?>">
     <table>
            <tr>
                <th>Tipo</th>
                <th>Fecha</th>
                <th>Número</th>
                <th>Obs</th>
                <th>Banco</th>
                <th>Cuenta destino</th>
                <th>Chequera</th>
                <th>Importe</th>
            </tr>
        <?php foreach ($data as $row): ?>
            <tr>
                <td><?php echo $row["tipo_valor"]; ?></td>
                <td><?php echo date("d/m/Y",mystrtotime($row["fecha"])); ?></td>
                <td><?php echo $row["numero"];?></td>
                <td><?php echo $row["obs"];?></td>
                <td><?php echo $row["banco"];?></td>
                <td><?php echo $row["destino"];?></td>
                <td><?php echo $row["chequera"];?></td>
                <td class="importe"><?php echo $row["importe"];?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>