<?php

ini_set('memory_limit', '-1');
$fileName = "comprobantes";

$cols = array();
//vd2($rows);
$cols['fecha'] = array("title" => "Fecha");
if (in_array($comprob_id,
    [
        Comprob::NC_VENTAS,
        Comprob::ND_VENTAS,
        Comprob::RECIBO_BANCO,
//					Comprob::RECIBO_VENTAS,
        Comprob::FACTURA_VENTAS])) {
    $cols["matricula"] = array("title" => "Matrícula");
}
if (in_array($comprob_id,
    [
        Comprob::NC_VENTAS,
        Comprob::ND_VENTAS,
        Comprob::NC_COMPRAS,
        Comprob::ND_COMPRAS,
        Comprob::RECIBO_BANCO,
        Comprob::RECIBO_VENTAS,
        Comprob::OP,
        Comprob::FACTURA_COMPRAS,
        Comprob::FACTURA_VENTAS]
)) {
    $cols["nombre"] = array("title" => "Nombre");
}

if (in_array($comprob_id,
    [
        Comprob::NC_VENTAS,
        Comprob::ND_VENTAS,
        Comprob::NC_COMPRAS,
        Comprob::ND_COMPRAS,
        Comprob::AJUSTE_CAJA_ENTRADA,
        Comprob::AJUSTE_CAJA_SALIDA,
        Comprob::DEPOSITO_BANCARIO_SALIDA,
        Comprob::DEPOSITO_BANCARIO_ENTRADA,
        Comprob::TRANSFERENCIA_ENTRADA,
        Comprob::TRANSFERENCIA_SALIDA,
        Comprob::MOVIMIENTO_BANCARIO_D,
        Comprob::MOVIMIENTO_BANCARIO_H]
)) {
    $cols["detalle"] = array("title" => "Detalle");
}

if (in_array($comprob_id,
    [
        Comprob::AJUSTE_CAJA_ENTRADA,
        Comprob::AJUSTE_CAJA_SALIDA,
        Comprob::DEPOSITO_BANCARIO_SALIDA,
        Comprob::DEPOSITO_BANCARIO_ENTRADA,
        Comprob::TRANSFERENCIA_ENTRADA,
        Comprob::TRANSFERENCIA_SALIDA,
        Comprob::MOVIMIENTO_BANCARIO_D,
        Comprob::MOVIMIENTO_BANCARIO_H,
        Comprob::ND_VENTAS,
        Comprob::NC_VENTAS
    ]
)) {
    $cols["concepto_nombre"] = array("title" => "Concepto");
}

if (in_array($comprob_id,
    [
        Comprob::MOVIMIENTO_BANCARIO_D,
        Comprob::MOVIMIENTO_BANCARIO_H
    ]
)) {
    $cols["banco"] = array("title" => "Banco");
}
if (in_array($comprob_id,
    [
        Comprob::RECIBO_BANCO
    ]
)) {
    $cols["fecha_valor"] = array("fecha_valor" => "Fecha valor");
}

$cols["numero"] = array("title" => "Número");
$cols["total"] = array("title" => "Total");
$cols["anulado"] = array("title" => "Anulado");


$objPHPExcel = new MyPHPExcel();

$as = $objPHPExcel->getActiveSheet();
$as = $objPHPExcel->setActiveSheetIndex(0);

$fila = 1;
foreach ($cols as $fld => $data) {
//    vd($data);
    $title = isset($data["title"]) ? $data["title"] : ucfirst($fld);
    $sfila = chr($fila + 64);
    $as->SetCellValue($sfila . '1', $title);
    $fila++;
}
$as->setAutoFilter("A1:" . $sfila . "4");
$as->getStyle("A1:" . $sfila . "1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$as->getStyle("A1:" . $sfila . "1")->getFont()->setBold(true);
$as->getStyle("A1:" . $sfila . "1")->getFill()->getStartColor()->setARGB("666666");
$as->getStyle("A1:" . $sfila . "1")->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);


$i = 2;
foreach ($rows as $row) {
    $c = 1;
    if($row['anulado'] === '1'){
      continue;
    }
    foreach ($cols as $fld => $data) {
        $scol = chr($c + 64);
        $as->SetCellValue($scol . "$i", $row[$fld]);
        $c++;
    }
    $i++;
}
$objPHPExcel->output($fileName);