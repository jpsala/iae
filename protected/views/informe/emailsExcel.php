<?php

class MyExcel extends Excel {

//    public function output() {
//        $styleArray = array(
//                'borders' => array(
//                        'allborders' => array(
//                                'style' => PHPExcel_Style_Border::BORDER_THIN,
//                                'color' => array('argb' => '000'),
//                        ),
//                        'outline' => array(
//                                'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
//                                'color' => array('argb' => '000'),
//                        ),
//                ),
//        );
//        $this->as->getStyle("A$this->firstRow:$this->lastCol$this->lastRow")->applyFromArray($styleArray);
//        parent::output();
//    }
//
//    public function printTitle() {
//        $anio = date("Y", time());
//        $row = $this->nextRow;
//        $this->as->SetCellValue("A$row", "Alumnos con Riesgo Académico");
//        $this->as->SetCellValue("H$row", $this->extraData["anio"] . " " . $this->extraData["division"] . " $anio");
//        $this->as->SetCellValue("F" . ($row + 1), "Ref: Nota del " . $this->extraData["periodo"] . " necesaria para acreditar");
//        $this->nextRow = 3;
//        $this->as->getRowDimension(3)->setRowHeight(30);
//        $this->as->getStyle('A3:A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
//        $this->as->getStyle('A3:A3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
//        $this->as->getStyle('B3:Z3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
//        $this->as->getStyle('B3:Z3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
//        $this->as->getStyle('B3:Z3')->getAlignment()->setWrapText(true);
//        parent::printTitle();
//    }

}

$options = array(
        "paperSize" => "legal",
        "orientation" => "L",
        "fitToPage" => true,
        "marginTop" => .5,
        "marginLeft" => .3,
        "marginBottom" => .5,
        "marginRight" => .3,
        "fitToWidth" => 1,
//        "fitToHeight" => 1,
);

$excel = new MyExcel($rows, $cols, "Emails",Null , $options);
$excel->run();
$excel->output();
?>
