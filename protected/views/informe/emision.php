<?php
$paramNivel = new ParamNivel(array(
        "controller" => $this,
        "onchange" => "changeNivel",
        ));
$paramAlumno = new ParamAlumno(array(
        "controller" => $this,
        "onchange" => "changeAlumno",
        "todos" => 1,
        ));
?>
<div id="liquidacion_div">
    <?php $arr = array();?>
    <?php $data = CHtml::listData(LiquidConf::model()->findAll(), "id", "descripcion"); ?>
    <?php echo CHtml::dropDownList("liquid_conf_id", "", $data, $arr); ?>
    <?php
    $paramNivel->render();
    $paramAlumno->render();
    ?>
</div>
<script type="text/javascript">
    $(function() {
        $("#liquid_conf_id").chosen();
        setTimeout(function(){$("#liquid_conf_id_chzn").mousedown();}, 100);
    });
    function changeNivel() {
        var nivel_id = $("#nivel-select").val();
        var liquid_conf_id = $("#liquid_conf_id").val();
        window.location = "<?php echo $this->createUrl("liquidacion/emisionComprobantes"); ?>&nivel_id=" + nivel_id + "&liquid_conf_id=" + liquid_conf_id;
    }
    function changeAlumno() {
        var liquid_conf_id = $("#liquid_conf_id").val();
        var alumno_id = $("#alumno-select").val();
        window.location = "<?php echo $this->createUrl("liquidacion/emisionComprobantes"); ?>&alumno_id=" + alumno_id + "&liquid_conf_id=" + liquid_conf_id;;
    }

</script>