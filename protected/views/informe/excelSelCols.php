<style type="text/css">
	.row{display:inline-block;vertical-align: super;  margin-left: 6px;}
	.fecha {width:80px}
	#genNiAnDi-div{position: relative}
	#buttons{position: absolute; right: 0px; text-align: right; display: inline !important}
	#nivel-select{width:120px!important}
	#anio-select{width:120px!important}
	#usuario-div{display: inline;vertical-align: text-bottom;width:130px;margin-left: 0}
	#novedad-div{display: inline;margin-left: 0;vertical-align: text-bottom;width: 150px;}
	#linea2-dif { margin-right: 204px;margin-top: 4px;}
	#linea2-dif{float: right}
	#usuario_id{width:180px}
	#div-para-notas {clear: both;}
	#boletin-fecha{    display: inline-block;vertical-align: top; margin-left:3px}
	#boletin-fecha input{width: 74px}
  .cols{ margin: 50px 0 0 0 }
  .col{ margin: 5px 0 0 10px }
</style>
<div id="genNiAnDi-div">
	<?php
		$pdf = (isset($pdf) and $pdf) ? $pdf : false;
		$print = (isset($print) and $print) ? $print : false;
		$ajax = (isset($ajax) and $ajax) ? $ajax : false;
		$excel = (isset($excel) and $excel) ? $excel : false;
		$sexo = (isset($sexo) and $sexo) ? ($sexo ? "true" : "false") : "false";
		$paramNivel = new ParamNivel(array(
				"controller" => $this,
				"prompt" => "Todos",
//   "onchange" => "changeNivel",
		));
		$paramAnio = new ParamAnio(array(
				"controller" => $this,
				"prompt" => "Todos",
				//"onchange" => "anioChange",
		));
		$paramDivision = new ParamDivision(array(
				"prompt" => "Todos",
				"controller" => $this,
		));
		$paramNivel->render();
		$paramAnio->render();
		$paramDivision->render();
	?>
    <div class="cols">
      <form id="cols">
      <?php //vd2($cols); ?>
        <?php foreach ($cols as $key => $value){?>
          <?php //echo $key ; //vd2($value); ?>
          <div class="col">
            <input type="checkbox" name="<?php echo $key;?>" value="<?php echo $value['title'];?>"><?php echo $value['title'];?></input>
          </div>
        <?php } ?>
      </form>
    </div>

	<div id="buttons">
		<?php if ($excel): ?>
				<button onclick="return generaArchivo('excel');" id="generaEXCEL">Generar EXCEL</button>
			<?php endif; ?>
		<?php if ($print): ?>
				<button onclick="return generaArchivo('print');" id="ParaImprimir">Para Imprimir</button>
			<?php endif; ?>
		<?php if ($ajax): ?>
				<button onclick="visualiza();" id="visualiza">Visualiza</button>
			<?php endif; ?>
		<?php if ($pdf): ?>
				<button onclick="generaArchivo('pdf');" id="generaPDF">Generar PDF</button>
			<?php endif; ?>
	</div>
</div>


<script type="text/javascript">
	var url = "<?php echo $this->createUrl("/informe/$reporteNombre"); ?>";

	$("button").button();


	function generaArchivo(output) {
    // var cols = $('#cols').serialize();
		var params = "&output=" + output;
    var cols = new Array();
    // console.log('cols', cols);
    $("input:checked").each(function() {
      console.log($(this));

           cols.push($(this).attr('name'));
        });
    console.log('cols', cols);
    w = url + "&cols=" + cols + "&nivel_id=" + nivel_id + "&anio_id=" + anio_id + "&division_id=" + division_id + params;
    console.log('w', w);

		window.open(w);

	}

</script>