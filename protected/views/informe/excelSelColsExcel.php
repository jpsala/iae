<?php
ini_set('memory_limit', '-1');

$ciclo_id = Ciclo::getCicloIdParaInformesEducativo();
$comprobFactura = Comprob::FACTURA_VENTAS;
$fileName = "Alumnos.xls";
$direccion = "concat (COALESCE(p1.calle, ''), ' ', COALESCE(p1.numero, ''), ' ', COALESCE(p1.piso, ''), ' ', COALESCE(p1.departamento, '')) as domicilio";
$where = "a.activo = 1 ";
$where .= ($nivel_id == "-1") ? "" : " and n.id = $nivel_id";
$where .= ($anio_id == "-1") ? "" : " and anio.id = $anio_id";
$where .= ($division_id == "-1") ? "" : " and d.id = $division_id";
$sql = "
      select a.Familia_id, concat(a.apellido,' ', a.nombre) as Nombre,
        a.matricula, n.nombre as nivel, anio.nombre as curso, d.nombre as division, a.numero_documento AS DNI,
        a.activo, a.beca, f.obs, f.obs as obs_cobranza, saldo(s.id, null) as saldo,concat(p1.apellido,' ' ,p1.nombre) as padre,
        p1.telefono_casa as p1tc, p1.telefono_trabajo as p1tt, p1.telefono_celular as p1tce,
        concat( p2.apellido, ' ' , p2.nombre) as nombre2,
        p2.telefono_casa as p2tc, p2.telefono_trabajo as p2tt, p2.telefono_celular as p2tce,
        p3.nombre as nombre3, pt.nombre as pariente1, pt1.nombre as pariente2,
        $direccion, DATE_FORMAT(a.fecha_nacimiento, '%d/%m/%Y') as fecha_nacimiento,
        p1.email AS p1mail, p2.email AS p2mail, p3.email AS p3mail
      from alumno a
			inner join familia f on f.id = a.familia_id
			inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_edu = 1
                left  join alumno_debito deb on deb.alumno_id = a.id
                inner join socio s on s.Alumno_id = a.id
                left join alumno_division ad on ad.Alumno_id = a.id and ad.ciclo_id = $ciclo_id
                left join division d on d.id = ad.Division_id
                left join anio on anio.id = d.Anio_id
                left join articulo ar on ar.id = anio.articulo_id
                left join nivel n on n.id = anio.Nivel_id
                left join pariente p1 on p1.id = a.vive_con_id
                left join pariente p2 on p2.id = (select id from pariente p2 where p2.Familia_id = a.Familia_id and p2.id <> a.vive_con_id limit 1 )
                left join pariente p3 on p3.id = (select id from pariente p3 where p3.Familia_id = a.Familia_id and p3.id <> a.vive_con_id limit 1 )
                left join pariente_tipo pt on pt.id = p1.Pariente_Tipo_Id
                left join pariente_tipo pt1 on pt1.id = p2.Pariente_Tipo_Id
            where $where and ad.activo
            order by n.orden, anio.orden, d.nombre, a.sexo desc, a.apellido, a.nombre
";
// vd2($sql);
$rows = Helpers::qryAll($sql);

$objPHPExcel = new MyPHPExcel();

$as = $objPHPExcel->getActiveSheet();
$as = $objPHPExcel->setActiveSheetIndex(0);


$colsTodas = array(
  "nivel" => array("title" => "Nivel"),
  "curso" => array("title" => "Curso"),
  "division" => array("title" => "Division"),
  "matricula" => array("title" => "Matricula", "format" => "text"),
  "Nombre" => array("title" => "Nombre Alumno"),
  "fecha_nacimiento" => array("title" => "Fec. Nac."),
  "p1mail" => array("title" => "p1mail"),
  "p2mail" => array("title" => "p2mail"),
  "DNI" => array("title" => "DNI"),
  // "beca" => array("title" => "Beca"),
  "domicilio" => array("title" => "Domicilio"),
  "padre" => array("title" => "Vive Con"),
  "p1tc" => array("title" => "Tel. Fijo", "align" => "right"),
  "p1tt" => array("title" => "Tel. Trabajo", "align" => "right"),
  "p1tce" => array("title" => "Tel. Cel.", "align" => "right"),
  "nombre2" => array("title" => "Nombre Madre"),
  "p2tc" => array("title" => "Tel. Fijo", "align" => "right"),
  "p2tt" => array("title" => "Tel. Trabajo", "align" => "right"),
  "p2tce" => array("title" => "Tel. Cel.", "align" => "right"),
  // "obs_cobranza" => array("title" => "Obs. Cobranza"),
  // "saldo" => array("title" => "Saldo", "total" => true, "currency" => true),
);
$colsFinal = [];
// vd2($cols);
foreach ($colsTodas as $key => $value) {
  // vd2($key, $value);
  // ve2($key, $cols, strpos($key, $cols));
  if (strpos(strtolower($cols), strtolower($key)) > -1) {
 //   vd2($colsTodas[$key]);
    $colsFinal[$key] = $colsTodas[$key];
  }
}
$cols = $colsFinal;
// vd2($cols);

$styleArray = array(
  'borders' => array(
     'allborders' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN
     )
  )
);

$fila = 1;
foreach ($cols as $fld => $data) {
  $title = isset($data["title"]) ? $data["title"] : ucfirst($fld);
  $sfila = chr($fila + 64);
  $as->SetCellValue($sfila . '1', $title);
  $fila++;
}

$as->setAutoFilter("A1:" . $sfila . "4");
$as->getStyle("A1:" . $sfila . "1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$as->getStyle("A1:" . $sfila . "1")->getFont()->setBold(true);
$as->getStyle("A1:" . $sfila . "1")->getFill()->getStartColor()->setARGB("666666");
$as->getStyle("A1:" . $sfila . "1")->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
$i = 2;
foreach ($rows as $row) {
  $c = 1;
  foreach ($cols as $fld => $data) {
    $scol = chr($c + 64);
    $as->SetCellValue($scol . "$i", $row[$fld]);
    $c++;
  }
  $i++;
}
$c = 0;
foreach ($cols as $fld => $data) {
  $c++;
  $scol = chr($c + 64);
  $as->getColumnDimension($scol)->setAutoSize(true);
  if (isset($data["align"])) {
    if (strtoupper($data["align"]) == "RIGHT") {
      $as->getStyle($scol . "2:" . $scol . "$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    } elseif (strtoupper($data["align"]) == "LEFT") {
      $as->getStyle($scol . "2:" . $scol . "$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    }
  }
  if (isset($data["format"])) {
    if (strtoupper($data["format"]) == "TEXT") {
      $as->getStyle($scol . "2:" . $scol . "$i")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
    } elseif (strtoupper($data["format"]) == "NUMBER") {
      $as->getStyle($scol . "2:" . $scol . "$i")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
    }
  }
  if (isset($data["total"]) and $data["total"]) {
    $as->getStyle($scol . "2:" . $scol . "$i")->getNumberFormat()->setFormatCode('#,##0.00');
  }
  if (isset($data["total"]) and $data["total"]) {
    $as->setCellValue("A$i", "Totales");
    //vd("$scol$i", "=SUM(".$scol."2:".$scol.($i-1).")");
    $as->setCellValue("$scol$i", "=Sum(" . $scol . "2:" . $scol . ($i - 1) . ")");
  }
}
//$as->getStyle("I2:I$i")->getNumberFormat()->setFormatCode('#,##0.00');
//$as->getStyle("J2:J$i")->getNumberFormat()->setFormatCode('#,##0.00');
//$as->getStyle("M2:O$i")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
//$as->getStyle("E2:E$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
//$as->getStyle("M2:O$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
//$as->getStyle("Q2:S$i")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
//$as->getStyle("Q2:S$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
//$as->getStyle("T2:T$i")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
//$as->getStyle("T2:T$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
// vd2($fileName);
$objPHPExcel->getActiveSheet()->getStyle("A1:".$scol.($i-1))->applyFromArray($styleArray);
$objPHPExcel->output($fileName, true);
