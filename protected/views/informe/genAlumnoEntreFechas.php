<style type="text/css">
    #imprime {vertical-align: top; margin-top: -1px; display:inline-block; margin-left: 10px}
    /*.row{display:inline-block}*/
    #comprob_tipo_chzn{margin-bottom: 3px; vertical-align: middle}
    .importe{text-align: right}
    .fecha {
        width: 80px;
    }
    .doc:hover{cursor:pointer}
    #buttons{margin-top:15px}
    label{float: left; width: 90px; line-height: 23px; vertical-align: central}
    #div-socio{margin-bottom: 5px}
    #titulo{width: 50px}
    #div-titulo{ background-color: #C3D9FF;    border-bottom: 1px solid #CCCCCC;    border-bottom: 1px solid #EFF3FC;    border-radius: 5px 5px 5px 5px;  margin-bottom: 5px;    padding: 3px;    text-align: center;color: #555555;  font-weight: bold;}
</style>
<?php
$pdf = (isset($pdf) and $pdf) ? $pdf : false;
$print = (isset($print) and $print) ? $print : false;
$ajax = (isset($ajax) and $ajax) ? $ajax : false;
$excel = (isset($excel) and $excel) ? $excel : false;
?>  
<div class="row">
    <div id="div-titulo" >
        <?php //echo $this->pageTitle = $titulo; ?>
    </div>  
    <div id="div-socio">
        <input name="socio_id" id="socio_id" value="" placeholder="Alumno"/>
    </div>

    <div class="row">
        <label for="fecha-desde">Desde Fecha : </label>
        <input class="fecha" type="text" id="fecha_desde" value="<?php echo date("d/m/Y", time() - (3600 * 24 * 30)); ?>"/>
    </div>

    <div class="row">
        <label for="fecha-hasta">Hasta Fecha : </label>
        <input class="fecha" type="text" id="fecha_hasta" value="<?php echo date("d/m/Y", time()); ?>"/>
    </div>

    <?php if ($reporteNombre == "xxx"): ?>
    <?php endif; ?>

    <div id="buttons">
        <?php if ($excel): ?>
            <button onclick="return generaArchivo('excel');" id="generaEXCEL">Generar EXCEL</button>
        <?php endif; ?>
        <?php if ($print): ?>
            <button onclick="return generaArchivo('print');" id="ParaImprimir">Para Imprimir</button>
        <?php endif; ?>
        <?php if ($ajax): ?>
            <button onclick="visualiza();" id="visualiza">Visualizar</button>
        <?php endif; ?>
        <?php if ($pdf): ?>
            <button onclick="generaArchivo('pdf');" id="generaPDF">Generar PDF</button>
        <?php endif; ?>
            <button onclick="cancelaInforme();" id="cancela-informe">Cancela</button>
    </div>

    <div id="reporte-div"></div>

    <script type="text/javascript">
                var url = "<?php echo $this->createUrl("/informe/$reporteNombre"); ?>";
                var urlImpresion = "<?php echo $this->createUrl("informe/$reporteNombre"); ?>";
                var urlAC = "<?php echo $this->createUrl('socio/sociosAc'); ?>&socioTipo=alumno";
                var fechas = <?php echo (isset($fechas) and $fechas) ? "true" : "false" ?>;
                $("button").button();
                $(".fecha").datepicker();
                $('#socio_id').focus().autocomplete({
                    autoSelect: true, autoFocus: true,
                    minLength: 2, source: urlAC,
                    select: function(event, ui) {
                        socio_id = ui.item.id;
                    }
                });

                function visualiza() {
                    data = {output: "ajax",fecha_desde:$("#fecha_desde").val(),fecha_hasta:$("#fecha_hasta").val(),socio_id:socio_id};
                    $.ajax({
                        type: "GET",
                        data: data,
                        url: url,
                        success: function(data) {
                            $("#reporte-div").html(data);
                            $("#imprime").show();
                        },
                        error: function(data, status) {
                        }
                    });
                }

                function generaArchivo(output) {
                    var params = "&output=" + output;

                    params += "&fecha_desde=" + $("#fecha_desde").val() + "&fecha_hasta=" + $("#fecha_hasta").val() + "&socio_id="+socio_id;

                    w = url + params;
                    window.open(w);

                }

                function refresh() {
                    window.location = window.location;
                }
                
                function cancelaInforme(){
                    $("#socio_id").val("").focus();
                }
    </script>