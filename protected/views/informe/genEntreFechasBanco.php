<style type="text/css">
    #imprime {vertical-align: top; margin-top: -1px; display:inline-block; margin-left: 10px}
    .row{display:inline-block}
    #destino_chzn{margin-bottom: 3px; vertical-align: middle}
    #destino{width: 200px}
    .importe{text-align: right}
    .fecha {
        width: 80px;
    }
    .doc:hover{cursor:pointer}
</style>

<div class="row">
    <label for="fecha-desde">Desde Fecha : </label>
    <input class="fecha" type="text" id="fecha_desde" value="<?php echo date("d/m/Y", time() - (3600 * 24 * 10)); ?>"/>
</div>

<div class="row">
    <label for="fecha-hasta">Hasta Fecha : </label>
    <input class="fecha" type="text" id="fecha_hasta" value="<?php echo date("d/m/Y", time()); ?>"/>
</div>

<div class="row">
    <label for="destino">Comprobante : </label>
    <?php $ld = Destino::listData('2');?>
    <?php echo CHtml::dropDownList("destino", Null, $ld) ?>
</div>

<button onclick="return imprime();
        return false;" id="imprime">
    Imprime
</button>

<div id="reporte-div"></div>

<script type="text/javascript">
    var url = "<?php echo $this->createUrl("/informe/informeBancoPDF"); ?>";
    var urlImpresion = "<?php echo $this->createUrl("/informe/informeBancoPDF"); ?>";
    $("#imprime").button();
    $(".fecha").datepicker();
    $("#destino").chosen();


   
    function imprime() {
            window.open(urlImpresion +
                    "&destino_id=" + $("#destino").val() +
                    "&fecha_desde=" + $("#fecha_desde").val() +
                    "&fecha_hasta=" + $("#fecha_hasta").val(),
                    'newwin', 'menubar=no,width=750,height=550,toolbar=no,scrollbars=yes,screenX=300');
        }
    
    
</script>