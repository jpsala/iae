<link rel="stylesheet" type="text/css" href="js/tablesorter/themes/blue/style.css"/>
<!--<script src="js/tablesorter/jquery.tablesorter.js"></script>-->
<style type="text/css">
  #comprob-tipo {
    width: 200px
  }

  #imprime {
    vertical-align: top;
    margin-top: -1px;
    display: inline-block;
    margin-left: 10px
  }

  .row {
    display: inline-block
  }

  #comprob_tipo_chzn {
    margin-bottom: 3px;
    vertical-align: middle
  }

  .importe {
    text-align: right
  }

  .fecha {
    width: 75px;
  }

  .doc:hover {
    cursor: pointer
  }

  .reimpresion {
    background-image: url("images/print.png");
    background-repeat: no-repeat;
    background-size: 100% auto !important;
    width: 8px;
  }

  .modifica {
    background-image: url("images/edit.jpg");
    background-repeat: no-repeat;
    background-size: 100%;
    width: 7px;
    background-position: 1px 3px;
  }

  .anula {
    background-image: url("images/document-close.ico");
    background-position: 1px 2px;
    background-repeat: no-repeat;
    padding-right: 4px !important;
    width: 11px
  }

  #total-td {
    padding-right: 20px;
    text-align: right
  }

  #modifica-dialog {
    display: none
  }

  #filtro-div {
    float: right;
  }
</style>

<div class="row">
  <label for="fecha-desde">Desde : </label>
  <input class="fecha" type="text" id="fecha_desde"
         value="<?php echo date("d/m/Y", time()/* - (3600 * 24 * 1) */); ?>"/>
</div>

<div class="row">
  <label for="fecha-hasta">Hasta : </label>
  <input class="fecha" type="text" id="fecha_hasta" value="<?php echo date("d/m/Y", time()); ?>"/>
</div>

<?php if ($reporteNombre == "docs1Ajax"): ?>
  <div class="row">
    <!--<label for="comprob-tipo">Comprobante : </label>-->
    <?php //vd(CHtml::listData($comprobs, "id", "nombre"));;?>
    <?php $ld = Comprob::listData(); ?>
    <?php echo CHtml::dropDownList("comprob-tipo", Null, $ld) ?>
      <button onclick="return imprimeDoc();
      return false;" id="imprime">
          Trae los comprobantes
      </button>
      <select id="output" name="output">
          <option value="E">Excel</option>
          <option value="P" selected>Pantalla</option>
      </select>
  </div>
<?php endif; ?>



<div id="filtro-div"><input type="text" id="quickfind" placeholder="Filtro"/></div>
<div id="reporte-div"></div>

<div id="modifica-dialog" class="ui-widget ui-widget-content ui-corner-all">
</div>

<script type="text/javascript">
  var urlReporte = "<?php echo $this->createUrl("/informe/$reporteNombre"); ?>";
  var urlImpresion = "<?php echo $this->createUrl("doc/docImpresion"); ?>";
  var urlModificacion = "<?php echo $this->createUrl("doc/docModificacion"); ?>";
  var urlAnulacion = "<?php echo $this->createUrl("doc/anulaDoc"); ?>";
  var timeout = null;
  $("#quickfind").clearinput();
  $("#modifica-dialog").dialog({
    autoOpen: false,
    title: "Modificación de documento",
    width: "90%",
    height: "auto",
    modal: true,
    position: {
      my: "center, center+200",
      at: "top"
    },
    buttons: {
      Graba: function () {
        grabaDoc();
      },
      Cancela: function () {
        $(this).dialog("close");
      }
    }
  });
  $("#imprime").button();
  $(".fecha").datepicker();
  $("#comprob-tipo").chosen();

  function grabaDoc() {
    $.ajax({
        type: "POST",
        //      dataType:"json",
        data: $("#mod-form").serialize(),
        url: "<?php echo $this->createUrl("doc/docModificacionGraba"); ?>",
        success: function (data) {
          if (data) {
            alerta(data);
          } else {
            $("#modifica-dialog").dialog("close");
            $("#imprime").click();
          }
        },
        error: function (data, status) {
        }
      }
    );
  }

  function imprimeDoc() {
    var output = $("#output").val();

    if(output === 'P') {
      $.ajax({
        type: "GET",
        data: {
          fecha_desde: $("#fecha_desde").val(),
          fecha_hasta: $("#fecha_hasta").val(),
          comprob_id: $("#comprob-tipo").val(),
          paperSize: 'legal',
          output: $("#output").val()
        },
        url: urlReporte,
        success: function (data) {
          $("#reporte-div").html(data);
          $("table").on("click", "td:not('.reimpresion,.modifica,.anula')", function () {
            $obj = $(this).closest("tr");
            if ($obj.attr("doc_id")) {
              muestraDetalle($obj);
            }
          });
          //$(".tablesorter").tablesorter({headers: {
          // assign the secound column (we start counting zero)
          //3: {sorter: false}, 4: {sorter: false}, 5: {sorter: false}, 6: {sorter: false}
          // }});
          initFilter();
          $("#quickfind").focus();
        },
        error: function (data, status) {
        }
      });
    }else{
      window.location = urlReporte +
        "&fecha_desde="+ $("#fecha_desde").val()+
        "&fecha_hasta="+ $("#fecha_hasta").val()+
        "&comprob_id="+ $("#comprob-tipo").val()+
        "&paperSize="+ 'legal'+
        "&output="+ $("#output").val()
    }

    return false;
  }

  function muestraDetalle($obj) {
    $.ajax({
        type: "GET",
        dataType: "json",
        data: {doc_id: $obj.attr("doc_id")},
        url: "<?php echo $this->createUrl("informe/getDocDet"); ?>",
        success: function (data) {
          $det = $("#det_" + data.doc_id);
          $sel = $(".selected");
          if ($sel.length > 0) {
            $(".selected *").css("font-weight", "100");
            $(".selected").removeClass("selected");
          }
          if ($det.length > 0 && $det.is(':visible')) {
            $(".det").remove();
            //                 $det.closest(".det").remove();
//                    $det.closest(".det").remove();
          } else {
            $(".det").remove();
            $obj.addClass("selected");
            $obj.find("*").css("font-weight", "bolder");
            if (data.renderValores) {
              $obj.after("<tr class='det'><td style='width:50px;font-weight:bolder'>Valores</td><td colspan='5'>" + data.renderValores + "</td></tr>");
            }
            if (data.renderArticulos) {
              $obj.after("<tr class='det'><td style='width:50px;font-weight:bolder'>Detalle</td><td colspan='5'>" + data.renderArticulos + "</td></tr>");
            }
            //}
          }
        },
        error: function (data, status) {
        }
      }
    );
  }

  function reimpresion($obj) {
    var doc_id = $obj.closest("tr").attr("doc_id");
    if (comprob_id == 1 && false) { //mejor no
      $.ajax({
        type: "GET",
        //      dataType:"json",
        data: {doc_id: doc_id, imprime: 0},
        url: urlImpresion,
        success: function (data) {
          //window.location = urlImpresion;
        },
        error: function (data, status) {
        }
      });
    } else {
      console.log(urlImpresion + "&doc_id=" + doc_id, 'newwwinn', 'menubar=no,width=750,height=550,toolbar=no,scrollbars=yes,screenX=300')
      window.open(urlImpresion + "&doc_id=" + doc_id);
    }
    //window.open(urlImpresion+"&doc_id="+doc_id);
  }

  function modifica($obj) {
    doc_id = $obj.closest("tr").attr("doc_id");
    comprob = $obj.closest("tr").attr("comprob");
    console.log(comprob);
    ;
    if (comprob == "Recibo") {
      window.open("<?php echo $this->createUrl("/doc/recibo"); ?>" + "&doc_id=" + doc_id);
      return true;
    } else if (comprob == "Salida de caja") {
      window.open("<?php echo $this->createUrl("/doc/movCajaSalida"); ?>" + "&doc_id=" + doc_id);
      return true;
    } else if (comprob == "Entrada de caja") {
      window.open("<?php echo $this->createUrl("/doc/movCajaEntrada"); ?>" + "&doc_id=" + doc_id);
      return true;
    }
    $.ajax({
      type: "GET",
      //      dataType:"json",
      data: {doc_id: doc_id, imprime: 0},
      url: urlModificacion,
      success: function (data) {
        $("#modifica-dialog").html(data);
        $("#modifica-dialog").dialog("open");
      },
      error: function (data, status) {
      }
    });
  }
  //    function modifica($obj) {
  //        doc_id = $obj.closest("tr").attr("doc_id");
  //
  //        $.ajax({
  //            type: "GET",
  //            //      dataType:"json",
  //            data: {doc_id: doc_id, imprime: 0},
  //            url: urlModificacion,
  //            success: function(data) {
  //                $("#modifica-dialog").html(data);
  //                $("#modifica-dialog").dialog("open");
  //            },
  //            error: function(data, status) {
  //            }
  //        });
  //    }

  //http://localhost/iae/index.php?r=doc/anulaDoc&doc_id=14077&force=true
  function anula($obj) {
    doc_id = $obj.closest("tr").attr("doc_id");
    alerta("Anula el comprobante?", "Alerta", {
        Si: function () {
          $.ajax({
            type: "GET",
            dataType: "json",
            data: {doc_id: doc_id, force: true},
            url: urlAnulacion,
            success: function (data) {
              if (data.error) {
                alerta("Error anulando el comprobante", data.error, {
                  Ok: function () {
                    $(this).dialog("close");
                  }
                });
              } else {
                alerta("El Comprobante fué anulado", "Exito", {
                  Ok: function () {
                    $(this).dialog("close");
                    $("#imprime").click();
                  }
                });
              }
            },
            error: function (data, status) {
            }
          });
        },
        Cancela: function () {
          $(this).dialog("close");
        }
      }
    );
  }

  function initFilter() {
    $("#quickfind").val("");
    $(".indexColumn").remove();
    $(".tablesorter tr:has(td)").each(function () {
      var t = $(this).text().toLowerCase();
      $("<td class='indexColumn'></td>")
        .hide().text(t).appendTo(this);
    });//each tr
    $("#quickfind").keyup(function () {
      if (timeout) {
        clearTimeout(timeout);
      }
      timeout = setTimeout(function (o) {
        filtra(o);
      }, 400, $(this));

    });

    function filtra(o) {
      var s = o.val().toLowerCase().split(" ");
      //show all rows.
      $(".tablesorter tr:hidden").show();
      $.each(s, function () {
        $(".tablesorter tr:visible .indexColumn:not(:contains('"
        + this + "'))").parent().hide();
      });
      calcTotal();
    }
  }

  function calcTotal() {
    var total = 0;
    var importe, anulado;
    $(".importe:visible").each(function () {
      anulado = $(this).parent().hasClass("anulado");
      if (!anulado) {
        importe = $(this).html();
        importe = Number(importe.replace(",", ""));
        total += importe;
      }
    });
    $("#total-td").html(number_format(total, 2));
  }
</script>