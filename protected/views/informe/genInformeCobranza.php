<style type="text/css">
    #imprime {vertical-align: top; margin-top: -1px; display:inline-block; margin-left: 10px}
    .row{display:inline-block}
    #nivel_chzn{margin-bottom: 3px; vertical-align: middle}
    #nivel{width: 200px}
    .importe{text-align: right}
    .fecha {
        width: 80px;
    }
    .doc:hover{cursor:pointer}
    #nro_visa-div{display: inline-block}
    #nro_visa{width: 210px}
</style>


<div class="row">
    <label for="nivel">Nivel : </label>
    <?php $ld = Nivel::listData('1'); ?>
    <?php echo CHtml::dropDownList("nivel", Null, $ld) ?>
</div>
<div id="nro_visa-div">
    <input type="text" placeholder="Primer Nro. de identificador de Visa" id="nro_visa"/>
</div>
<button onclick="return imprime();
        return false;" id="imprime">
    Imprime
</button>

<div id="reporte-div"></div>

<script type="text/javascript">
    var url = "<?php echo $this->createUrl("/informe/informeCobranzaExcel"); ?>";
    var urlImpresion = "<?php echo $this->createUrl("/informe/informeCobranzaExcel"); ?>";
    $("#imprime").button();
    $("#nivel").chosen();

    function imprime() {
        window.location = urlImpresion +
                "&nivel_id=" + $("#nivel").val() + "&nro_debito=" + $("#nro_visa").val();
    }

</script>