<style type="text/css">
    #imprime {vertical-align: top; margin-top: -1px; display:inline-block; margin-left: 10px}
    /*.row{display:inline-block}*/
    #comprob_tipo_chzn{margin-bottom: 3px; vertical-align: middle}
    .importe{text-align: right}
    .fecha {
        width: 80px;
    }
    .doc:hover{cursor:pointer}
    #buttons{margin-top:15px}
    label{float: left; width: 90px; line-height: 23px; vertical-align: central}
    #div-socio{margin-bottom: 5px}
    
</style>
<?php
$pdf = (isset($pdf) and $pdf) ? $pdf : false;
$ajax = (isset($ajax) and $ajax) ? $ajax : false;
$excel = (isset($excel) and $excel) ? $excel : false;
?>
<div class="row">


    <div class="row">
        <label for="fecha-desde">Desde Fecha : </label>
        <input class="fecha" type="text" id="fecha_desde" value="<?php echo date("d/m/Y", time() - (3600 * 24 * 30)); ?>"/>
    </div>

    <div class="row">
        <label for="fecha-hasta">Hasta Fecha : </label>
        <input class="fecha" type="text" id="fecha_hasta" value="<?php echo date("d/m/Y", time()); ?>"/>
    </div>

    <?php if ($reporteNombre == "xxx"): ?>
    <?php endif; ?>

    <div id="buttons">
        <?php if ($excel): ?>
            <button onclick="return generaArchivo('excel');" id="generaEXCEL">Generar EXCEL</button>
        <?php endif; ?>
        <?php if ($ajax): ?>
            <button onclick="visualiza();" id="visualiza">Visualiza</button>
        <?php endif; ?>
        <?php if ($pdf): ?>
            <button onclick="generaArchivo('pdf');" id="generaPDF">Generar PDF</button>
        <?php endif; ?>
    </div>

    <div id="reporte-div"></div>

    <script type="text/javascript">
            var url = "<?php echo $this->createUrl("/informe/$reporteNombre"); ?>";
            var urlImpresion = "<?php echo $this->createUrl("informe/$reporteNombre"); ?>";

            $("#generaPDF").button();
            $("#generaEXCEL").button();
            $("#generaAJAX").button();
            $(".fecha").datepicker();

            function generaArchivo(output) {
                window.open(urlImpresion + "&output=" + output +
                        "&fecha_desde=" + $("#fecha_desde").val() +
                        "&fecha_hasta=" + $("#fecha_hasta").val(),
                        'newwin', 'menubar=no,width=750,height=550,toolbar=no,scrollbars=yes,screenX=300');
            }

            function informe() {
                window.open(urlImpresion +
                        "&fecha_desde=" + $("#fecha_desde").val() +
                        "&fecha_hasta=" + $("#fecha_hasta").val(),
                        'newwin', 'menubar=no,width=750,height=550,toolbar=no,scrollbars=yes,screenX=300');
            }

            function refresh() {
                window.location = window.location;
            }
    </script>