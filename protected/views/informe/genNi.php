<style type="text/css">
    .row{display:inline-block;vertical-align: super;  margin-left: 6px;}
    .fecha {width:80px}
    #genNiAnDi-div{position: relative}
    #buttons{position: absolute; right: 0px; text-align: right; display: inline !important}
    #nivel-select{width:120px!important}
    #anio-select{width:120px!important}
    #usuario-div{display: inline;vertical-align: text-bottom;width:130px;margin-left: 0}
    #novedad-div{display: inline;margin-left: 0;vertical-align: text-bottom;width: 150px;}
    #linea2-dif { margin-right: 204px;margin-top: 4px;}
    #linea2-dif{float: right}
    #usuario_id{width:180px}
    #div-para-notas {clear: both;}
</style>
<div id="genNiAnDi-div">
    <?php
    $pdf = (isset($pdf) and $pdf) ? $pdf : false;
    $print = (isset($print) and $print) ? $print : false;
    $ajax = (isset($ajax) and $ajax) ? $ajax : false;
    $excel = (isset($excel) and $excel) ? $excel : false;
    $sexo = (isset($sexo) and $sexo) ? ($sexo?"true":"false") : "false";
    $paramNivel = new ParamNivel(array(
            "controller" => $this,
            "prompt" => "Todos",
//   "onchange" => "changeNivel",
    ));
   
    $paramNivel->render();
   
    ?>

    <?php if (isset($fechas) and $fechas): ?>
        <?php $fecha_desde = isset($fecha_desde) ? $fecha_desde : date("d/m/Y", time()/* - (3600 * 24 * 1) */); ?>
        <?php $fecha_hasta = isset($fecha_hasta) ? $fecha_hasta : date("d/m/Y", time()/* - (3600 * 24 * 1) */); ?>
        <div class="row">
            <label for="fecha-desde">Desde : </label>
            <input class="fecha" type="text" id="fecha_desde" value="<?php echo $fecha_desde; ?>"/>
        </div>
        <div class="row">
            <label for="fecha-hasta">Hasta : </label>
            <input class="fecha" type="text" id="fecha_hasta" value="<?php echo $fecha_hasta; ?>"/>
        </div>
    <?php endif; ?>
    <?php if ($sexo == "true"): ?>
        <select id="sexo" name="sexo" data-placeholder="Sexo">
            <option value="-1"></option>
            <option value="">Ambos</option>
            <option value="F">Femenino</option>
            <option value="M">Masculino</option>
        </select>

    <?php endif; ?>
    <?php if ($reporteNombre == "novedades1Ajax"): ?>
        <div id="linea2-dif">
            <div id="usuario-div" class="row">
                <?php
                foreach (Helpers::qryAll("
                            select u.id, u.nombre
                                from user u
                            where (select count(*) from novedad_cab c where c.user_id = u.id) > 0") as $u) {
                    $data[$u["id"]] = $u["nombre"];
                }
                echo CHtml::dropDownList("usuario_id", Null, $data
                        , array("prompt" => "", "empty" => array("Todos los usuarios"), "data-placeholder" => "Todos los usuarios"));
                ?>
            </div>
            <div id="novedad-div" class="row">
                <?php
                $articulo_tipo_novedad = Opcion::getOpcionText("articulo_tipo_novedad", Null, "liquidacion");
                foreach (Helpers::qryAll("
                            select a.id, a.nombre
                                from articulo a
                            where (select count(*) from novedad n where n.articulo_id = a.id) > 0 and
                                         articulo_tipo_id = $articulo_tipo_novedad") as $a) {
                    $dataArt[$a["id"]] = $a["nombre"];
                }
                echo CHtml::dropDownList("articulo_id", Null, $dataArt
                        , array("prompt" => "", "empty" => array("Todas las novedades"), "data-placeholder" => "Todas las novedades"));
                ?>
            </div>
        </div>
    <?php endif; ?>
    <div id="buttons">
        <?php if ($excel): ?>
            <button onclick="return generaArchivo('excel');" id="generaEXCEL">Generar EXCEL</button>
        <?php endif; ?>
        <?php if ($print): ?>
            <button onclick="return generaArchivo('print');" id="ParaImprimir">Para Imprimir</button>
        <?php endif; ?>
        <?php if ($ajax): ?>
            <button onclick="visualiza();" id="visualiza">Visualiza</button>
        <?php endif; ?>
        <?php if ($pdf): ?>
            <button onclick="generaArchivo('pdf');" id="generaPDF">Generar PDF</button>
        <?php endif; ?>
    </div>
</div>


<div id="div-para-notas"></div>
<script type="text/javascript">
            var url = "<?php echo $this->createUrl("/informe/$reporteNombre"); ?>";
            var fechas = <?php echo (isset($fechas) and $fechas) ? "true" : "false" ?>;
            var articulo = <?php echo $reporteNombre == "novedades1Ajax" ? "true" : "false" ?>;
            var usuario = <?php echo $reporteNombre == "novedades1Ajax" ? "true" : "false" ?>;
            var sexo = <?php echo $sexo ?>;

<?php if ($reporteNombre == "novedades1Ajax"): ?>
                $("#usuario_id, #articulo_id").chosen({});
<?php endif; ?>

            $("button").button();
            $("#sexo").chosen();
            $(".fecha").datepicker();

            function visualiza() {
                data = {nivel_id: nivel_id, anio_id: anio_id, division_id: division_id, output: "ajax"};
                if (fechas) {
                    data["fecha_desde"] = $("#fecha_desde").val();
                    data["fecha_hasta"] = $("#fecha_hasta").val();
                }
                if (usuario) {
                    data["usuario_id"] = $("#usuario_id").val();
                }
                if (articulo) {
                    data["articulo_id"] = $("#articulo_id").val();
                }
                if (sexo) {
                    data["sexo"] = $("#sexo").val();
                }
                $.ajax({
                    type: "GET",
                    data: data,
                    url: url,
                    success: function(data) {
                        $("#div-para-notas").html(data);
                        $("#imprime").show();
                    },
                    error: function(data, status) {
                    }
                });
            }

            function generaArchivo(output) {
                var params = "&output=" + output;

                if (fechas) {
                    params += "&fecha_desde=" + $("#fecha_desde").val() + "&fecha_hasta=" + $("#fecha_hasta").val();
                }
                if (usuario) {
                    params += "&usuario_id=" + $("#usuario_id").val();
                }
                if (articulo) {
                    params += "&articulo_id=" + $("#articulo_id").val();
                }
                if (sexo) {
                    params += "&sexo=" + $("#sexo").val();
                }
                console.log(sexo);
                w = url + "&nivel_id=" + nivel_id + "&anio_id=" + anio_id + "&division_id=" + division_id + params;
                window.open(w);

            }

</script>