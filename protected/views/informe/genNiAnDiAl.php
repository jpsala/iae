<style type="text/css">
    .row{display:inline-block;vertical-align: super;  margin-left: 6px;}
    .fecha {width:80px}
    #genNiAnDiAl-div{position: relative}
    #buttons{position: absolute; right: 0px; text-align: right; display: inline !important}
    #nivel-select{width:120px!important}
    #anio-select{width:120px!important}
    #usuario-div{display: inline;vertical-align: text-bottom;width:130px;margin-left: 0}
    #novedad-div{display: inline;margin-left: 0;vertical-align: text-bottom;width: 150px;}
    #linea2-dif { margin-right: 204px;margin-top: 4px;}
    #linea2-dif{float: right}
    #usuario_id{width:180px}
    #div-ajax {clear: both;}
    #fechas-div{margin-top: 5px}
</style>
<div id="genNiAnDiAl-div">
    <?php
    $pdf = (isset($pdf) and $pdf) ? $pdf : false;
    $print = (isset($print) and $print) ? $print : false;
    $ajax = (isset($ajax) and $ajax) ? $ajax : false;
    $excel = (isset($excel) and $excel) ? $excel : false;
    $fechas = (isset($fechas) and $fechas) ? $fechas : false;
    $fecha_desde = (isset($fecha_desde) and $fecha_desde) ? $fecha_desde : date("d/m/Y", time());
    $fecha_hasta = (isset($fecha_hasta) and $fecha_hasta) ? $fecha_hasta : date("d/m/Y", time());
    $paramNivel = new ParamNivel(array(
            "controller" => $this,
            "prompt" => "Todos",
//   "onchange" => "changeNivel",
    ));
    $paramAnio = new ParamAnio(array(
            "controller" => $this,
            "prompt" => "Todos",
            //"onchange" => "anioChange",
    ));
    $paramDivision = new ParamDivision(array(
            "prompt" => "Todos",
            "controller" => $this,
    ));
    $paramAlumno = new ParamAlumno(array(
            "controller" => $this,
            "activos" => true,
    ));
    $paramNivel->render();
    $paramAnio->render();
    $paramDivision->render();
    $paramAlumno->render();
    ?>

    <?php if (isset($fechas) and $fechas): ?>
        <div id="fechas-div">
            <div class="row">
                <label for="fecha-desde">Desde : </label>
                <input class="fecha" type="text" id="fecha_desde" value="<?php echo $fecha_desde; ?>"/>
            </div>
            <div class="row">
                <label for="fecha-hasta">Hasta : </label>
                <input class="fecha" type="text" id="fecha_hasta" value="<?php echo $fecha_hasta; ?>"/>
            </div>
        </div>
    <?php endif; ?>
    <div id="buttons">
        <?php if ($excel): ?>
            <button onclick="return generaArchivo('excel');" id="generaEXCEL">Generar EXCEL</button>
        <?php endif; ?>
        <?php if ($print): ?>
            <button onclick="return generaArchivo('print');" id="ParaImprimir">Para Imprimir</button>
        <?php endif; ?>
        <?php if ($ajax): ?>
            <button onclick="visualiza();" id="visualiza">Visualiza</button>
        <?php endif; ?>
        <?php if ($pdf): ?>
            <button onclick="generaArchivo('pdf');" id="generaPDF">Generar PDF</button>
        <?php endif; ?>
    </div>
</div>


<div id="div-ajax"></div>
<script type="text/javascript">
            var url = "<?php echo $this->createUrl("/informe/$reporteNombre"); ?>";
            var fechas = <?php echo (isset($fechas) and $fechas) ? "true" : "false" ?>;

<?php if ($reporteNombre == "novedades1Ajax"): ?>
                $("#usuario_id, #articulo_id").chosen({});
<?php endif; ?>

            $("button").button();
            $(".fecha").datepicker();

            function visualiza() {
                data = {nivel_id: nivel_id, anio_id: anio_id, division_id: division_id, output: "ajax"};
                if (fechas) {
                    data["fecha_desde"] = $("#fecha_desde").val();
                    data["fecha_hasta"] = $("#fecha_hasta").val();
                }
                $.ajax({
                    type: "GET",
                    data: data,
                    url: url,
                    success: function(data) {
                        $("#div-ajax").html(data);
                        $("#imprime").show();
                    },
                    error: function(data, status) {
                    }
                });
            }

            function generaArchivo(output) {
                var params = "&output=" + output;
                var alumno_id = $("#alumno-select").val();
                if (fechas) {
                    params += "&fecha_desde=" + $("#fecha_desde").val() + "&fecha_hasta=" + $("#fecha_hasta").val();
                }
                w = url + "&nivel_id=" + nivel_id + "&anio_id=" + anio_id + "&division_id=" + division_id + "&alumno_id=" + alumno_id + params;
                window.open(w);

            }

</script>