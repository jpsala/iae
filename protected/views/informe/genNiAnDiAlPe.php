<style type="text/css">
    .row{display:inline-block;vertical-align: super;  margin-left: 6px;}
    .fecha {width:80px}
    #genNiAnDi-div{position: relative}
    #buttons{position: absolute; right: 0px; text-align: right; display: inline !important}
    #nivel-select{width:120px!important}
    #anio-select{width:120px!important}
    #usuario-div{display: inline;vertical-align: text-bottom;width:130px;margin-left: 0}
    #novedad-div{display: inline;margin-left: 0;vertical-align: text-bottom;width: 150px;}
    #linea2-dif { margin-right: 204px;margin-top: 4px;}
    #linea2-dif{float: right}
    #usuario_id{width:180px}
    #div-para-notas {clear: both;}
</style>
<div id="genNiAnDi-div">
    <?php
    $pdf = (isset($pdf) and $pdf) ? $pdf : false;
    $print = (isset($print) and $print) ? $print : false;
    $ajax = (isset($ajax) and $ajax) ? $ajax : false;
    $excel = (isset($excel) and $excel) ? $excel : false;
    $paramNivel = new ParamNivel(array(
            "controller" => $this,
            "prompt" => "Todos",
//   "onchange" => "changeNivel",
    ));
    $paramAnio = new ParamAnio(array(
            "controller" => $this,
            "prompt" => "Todos",
            //"onchange" => "anioChange",
    ));
    $paramDivision = new ParamDivision(array(
            "prompt" => "Todos",
            "controller" => $this,
    ));
    $paramPeriodo = new ParamPeriodo(array(
            "controller" => $this,
    ));
    $paramAlumno = new ParamAlumno(array(
            "controller" => $this,
            "activos" => true,
    ));
    $paramNivel->render();
    $paramAnio->render();
    $paramDivision->render();
    $paramAlumno->render();
    $paramPeriodo->render();
    ?>

    <div id="buttons">
        <?php if ($excel): ?>
            <button onclick="return generaArchivo('excel');" id="generaEXCEL">Generar EXCEL</button>
        <?php endif; ?>
        <?php if ($print): ?>
            <button onclick="return generaArchivo('print');" id="ParaImprimir">Para Imprimir</button>
        <?php endif; ?>
        <?php if ($ajax): ?>
            <button onclick="visualiza();" id="visualiza">Visualiza</button>
        <?php endif; ?>
        <?php if ($pdf): ?>
            <button onclick="generaArchivo('pdf');" id="generaPDF">Generar PDF</button>
        <?php endif; ?>
    </div>
</div>


<div id="div-para-notas"></div>
<script type="text/javascript">
            var url = "<?php echo $this->createUrl("/informe/$reporteNombre"); ?>";

<?php if ($reporteNombre == "novedades1Ajax"): ?>
                $("#usuario_id, #articulo_id").chosen({});
<?php endif; ?>

            $("button").button();
            $(".fecha").datepicker();

            function visualiza() {
                data = {nivel_id: nivel_id, anio_id: anio_id, division_id: division_id, logica_periodo_id: logica_periodo_id, alumno_id: alumno_id, output: "ajax"};
                $.ajax({
                    type: "GET",
                    data: data,
                    url: url,
                    success: function(data) {
                        $("#div-para-notas").html(data);
                        $("#imprime").show();
                    },
                    error: function(data, status) {
                    }
                });
            }

            function generaArchivo(output) {
                var params = "&output=" + output;
                w = url + params + "&logica_periodo_id="+ logica_periodo_id + "&alumno_id=" + alumno_id;

                window.open(w);

            }

</script>