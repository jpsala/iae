<style type="text/css">
    #genNiAnDi-div{position: relative}
    #buttons{position: absolute; right: 0px; text-align: right; display: inline !important}
</style>
<?php
$pdf = (isset($pdf) and $pdf) ? $pdf : false;
$print = (isset($print) and $print) ? $print : false;
$ajax = (isset($ajax) and $ajax) ? $ajax : false;
$excel = (isset($excel) and $excel) ? $excel : false;
$paramNivel = new ParamNivel(array(
        "controller" => $this,
//   "onchange" => "changeNivel",
        ));
$paramAnio = new ParamAnio(array(
        "controller" => $this,
        ));
$paramDivision = new ParamDivision(array(
        "controller" => $this,
        ));
$paramAsignatura = new ParamAsignatura(array(
        "controller" => $this,
        "onchange" => "asignaturaChange"
        ));
?>
<div id="genNiAnDi-div">
    <?php
    $paramNivel->render();
    $paramAnio->render();
    $paramDivision->render();
    $paramAsignatura->render();
    ?>
    <div id="buttons">
        <?php if ($excel): ?>
            <button onclick="return generaArchivo('excel');" id="generaEXCEL">Generar EXCEL</button>
        <?php endif; ?>
        <?php if ($print): ?>
            <button onclick="return generaArchivo('print');" id="ParaImprimir">Para Imprimir</button>
        <?php endif; ?>
        <?php if ($ajax): ?>
            <button onclick="visualiza();" id="visualiza">Visualiza</button>
        <?php endif; ?>
        <?php if ($pdf): ?>
            <button onclick="generaArchivo('pdf');" id="generaPDF">Generar PDF</button>
        <?php endif; ?>
    </div>
</div>
<div id="div-para-notas"></div>
<script type="text/javascript">
            var url = "<?php echo $this->createUrl("/informe/$reporteNombre"); ?>";
            $("button").button();
            $(function() {

            });
            //$("#imprime").button();
            //$("#imprime").button("disabled");

            function asignaturaChange($o) {
                division_asignatura_id = $o.val();
            }

            function visualiza() {
                var asignatura_id = $("#asignatura-select").val();
                data = {nivel_id: nivel_id, anio_id: anio_id, division_id: division_id, asignatura_id:asignatura_id,output: "ajax"};
                $.ajax({
                    type: "GET",
                    data: data,
                    url: url,
                    success: function(data) {
                        $("#div-para-notas").html(data);
                        $("#imprime").show();
                    },
                    error: function(data, status) {
                    }
                });
            }

            function generaArchivo(output) {
                var params = "&output=" + output;
                var asignatura_id = $("#asignatura-select").val();
                w = url + "&nivel_id=" + nivel_id + "&anio_id=" + anio_id + "&division_id=" + division_id + "&asignatura_id="+asignatura_id + params;
                window.open(w);

            }

</script>