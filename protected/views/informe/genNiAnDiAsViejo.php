<style type="text/css">
	#imprime {vertical-align: top; margin-top: -1px; }
</style>
<?php
	$paramNivel = new ParamNivel(array(
		"controller" => $this,
//   "onchange" => "changeNivel",
	));
	$paramAnio = new ParamAnio(array(
		"controller" => $this,
	));
	$paramDivision = new ParamDivision(array(
		"controller" => $this,
	));
	$paramAsignatura = new ParamAsignatura(array(
		"controller" => $this,
		"onchange" => "asignaturaChange"
	));
	$paramNivel->render();
	$paramAnio->render();
	$paramDivision->render();
	$paramAsignatura->render();
?>

<button onclick="return imprime();" id="imprime">Imprime</button>

<div id="div-para-notas"></div>
<script type="text/javascript">
	var url = "<?php echo $this->createUrl("/informe/$reporteNombre"); ?>";
	var esPdf = <?php echo (isset($esPdf) and $esPdf) ? "true" : "false"; ?>;
	$(function() {
		$("#imprime").button({"disabled": true});

	});
	//$("#imprime").button();
	//$("#imprime").button("disabled");

	function asignaturaChange($o) {
		$("#imprime").button({"disabled": false});
		division_asignatura_id = $o.val();
	}
	function imprime() {
		if (!esPdf) {
			$.ajax({
				type: "GET",
				data: {division_asignatura_id: division_asignatura_id},
				url: url,
				success: function(data) {
					$("#div-para-notas").html(data);
					$("#imprime").show();
				},
				error: function(data, status) {
				}
			});
			return false;
		} else {
			window.open(url + "&division_asignatura_id=" + division_asignatura_id + "&conItems=true&paperSize=" + $("#paperSize-select").val());
		}
	}

</script>