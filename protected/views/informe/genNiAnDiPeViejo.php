<style type="text/css">
    #imprime {vertical-align: top; margin-top: -1px;margin-left: 10px}
    #paperSize {
        display: inline;
        margin-left: 3px;
        vertical-align: top;
    }
    #paperSize-select{width: 130px}
    #boletin-fecha{    display: inline-block;vertical-align: top; margin-left:3px}
    #boletin-fecha input{width: 74px}
</style>
<?php
$paramNivel = new ParamNivel(array(
        "controller" => $this,
        "trae_periodos_con_conducta" => isset($trae_periodos_con_conducta),
        ));
$paramAnio = new ParamAnio(array(
        "controller" => $this,
        ));
$paramDivision = new ParamDivision(array(
        "controller" => $this,
        //"onchange" => "divisionChange"
        ));
$paramPeriodo = new ParamPeriodo(array(
        "controller" => $this,
        "onchange" => "periodoChange"
        ));

$paramNivel->render();
$paramAnio->render();
$paramDivision->render();
$paramPeriodo->render();

if ($reporteNombre == "planillaResumenFinalAjax"):
    ?>
    <div id="paperSize">
        <select id="paperSize-select" data-placeholder="Tamaño de hoja">
            <!--<option value=""></option>-->
            <option value=""></option>
            <option value="LEGAL">Oficio</option>
            <option value="A4">A4</option>
            <option value="LETTER">Carta</option>
        </select>
    </div>
<?php elseif (in_array($reporteNombre, array("boletinesAjax", "boletinesPrimariaAjax"))): ?>
    <div id="boletin-fecha">
        Fecha: <input id="boletin-fecha-input" type="text" value="<?php echo date('d/m/Y', time()); ?>"/>
    </div>


<?php endif; ?>

<button onclick="imprime();
        return false;" id="imprime">Imprime
</button>

<div id="div-para-notas"></div>
<script type="text/javascript">
    var url = "<?php echo $this->createUrl("/informe/$reporteNombre"); ?>";
    var esPdf = <?php echo (isset($esPdf) and $esPdf) ? "true" : "false"; ?>;
    $("#boletin-fecha-input").datepicker();
    $("#imprime").button();
    $("#paperSize-select").chosen();

    function periodoChange(e) {
        //$("#paperSize_select_chzn").mousedown();
        setTimeout(function() {
            $("#paperSize_select_chzn").mousedown();
        }, 100);
    }

    function imprime() {
        data = {division_id: division_id, logica_periodo_id: logica_periodo_id};
        if ($("#boletin-fecha-input").length > 0) {
            data["fecha"] = $("#boletin-fecha-input").val();
        }
        if (!esPdf) {
            $.ajax({
                type: "GET",
                data: data,
                url: url,
                success: function(data) {
                    $("#div-para-notas").html(data);
                    $("#imprime").show();
                },
                error: function(data, status) {
                }
            });
        } else {
            $fecha = $("#boletin-fecha-input");
            fecha = ($fecha.length > 0) ? $fecha.val() : null;
            window.open(url + "&division_id=" + division_id + "&logica_periodo_id=" + logica_periodo_id + "&paperSize=" + $("#paperSize-select").val() + "&fecha=" + fecha);
        }
    }
</script>