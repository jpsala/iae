<style type="text/css">
    #imprime {vertical-align: top; margin-top: -1px; display:inline-block; margin-left: 10px}
    /*.row{display:inline-block}*/
    #comprob_tipo_chzn{margin-bottom: 3px; vertical-align: middle}
    .importe{text-align: right}
    .fecha {
        width: 80px;
    }
    .doc:hover{cursor:pointer}
    #buttons{margin-top:15px}
    label{float: left; width: 90px; line-height: 23px; vertical-align: central}
    #div-socio{margin-bottom: 5px}
    #div-titulo{ background-color: #C3D9FF;    border-bottom: 1px solid #CCCCCC;    border-bottom: 1px solid #EFF3FC;    border-radius: 5px 5px 5px 5px;  margin-bottom: 5px;    padding: 3px;    text-align: center;color: #555555;  font-weight: bold;}
</style>
<div class="row">
    <div id="div-titulo" >
        <?php echo $this->pageTitle = $titulo; ?>
    </div>  
    <div id="div-socio">
        <!--            <label for="socio"></label>-->
        <input name="socio_id" id="socio_id" value="" 
               placeholder=""/>
    </div>
    <?php $fechas = !isset($fechas) or (isset($fechas) and $fechas); ?>
    <?php if ($fechas): ?>
        <div class="row">
            <label for="fecha-desde">Desde Fecha : </label>
            <input class="fecha" type="text" id="fecha_desde" value="<?php echo date("d/m/Y", time() - (3600 * 24 * 30)); ?>"/>
        </div>

        <div class="row">
            <label for="fecha-hasta">Hasta Fecha : </label>
            <input class="fecha" type="text" id="fecha_hasta" value="<?php echo date("d/m/Y", time()); ?>"/>
        </div>
    <?php endif; ?>
    <?php if ($reporteNombre == "xxx"): ?>
    <?php endif; ?>

    <div id="buttons">
        <button onclick="return informe();
                return false;" id="informe">
            Imprime
        </button>
        <button onclick="return refresh();
                return false;" id="refresh">
            cancela
        </button>
    </div>

    <div id="reporte-div"></div>

    <script type="text/javascript">
        var url = "<?php echo $this->createUrl("/informe/$reporteNombre"); ?>";
        var urlImpresion = "<?php echo $this->createUrl("informe/$reporteNombre"); ?>";
        var urlAC = "<?php echo $this->createUrl('socio/sociosAc'); ?>&socioTipo=proveedor";
        $("#informe, #refresh").button();
        $(".fecha").datepicker();
        $('#socio_id').focus().autocomplete({
            autoSelect: true, autoFocus: true,
            minLength: 2, source: urlAC,
            select: function(event, ui) {
                socio_id = ui.item.id;
                console.log(socio_id);
            }
        });

        function informe() {
            console.log(urlImpresion, socio_id, $("#fecha_desde").val(), $("#fecha_hasta").val());
            window.open(urlImpresion +
                    "&socio_id=" + socio_id +
                    "&fecha_desde=" + $("#fecha_desde").val() +
                    "&fecha_hasta=" + $("#fecha_hasta").val(),
                    '', 'menubar=no,width=750,height=550,toolbar=no,scrollbars=yes,screenX=300');
        }

        function refresh() {
            window.location = window.location;
        }
    </script>