<head>
  <meta http-equiv="X-UA-Compatible" content="IE=9" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="language" content="<?= Yii::app()->language; ?>" />        
  <title><?php echo CHtml::encode($this->pageTitle); ?></title>

  <!--[if lt IE 8]>
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
  <![endif]-->
  <?php $baseUrl = Yii::app()->request->baseUrl; ?>
  <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl . "/css/screen.css?r=2"; ?>" media="screen, projection" />
  <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl . "/css/print.css"; ?>" media="print" />
  <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl . "/css/main.css"; ?>" media="" />
  <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl . "/css/form.css"; ?>" media="" />

  <link rel="stylesheet" type="text/css" href="css/iae.css?r=6" media="screen, projection" />
</head>
