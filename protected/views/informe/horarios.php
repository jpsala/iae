<?php
$fecha_hasta = date('Y/m/d', strtotime(str_replace('/','.', $fecha_hasta)));
$fecha_desde = date('Y/m/d', strtotime(str_replace('/','.', $fecha_desde)));

$ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
$rows = Helpers::qryAll("
  SELECT e.documento, e.nombre, sum(er.minutos) as minutos 
    FROM empleado e
     INNER JOIN empleado_registro er ON e.id = er.empleado_id and
     \"$fecha_desde\" <= date_format(er.fecha, '%Y/%m/%d') and \"$fecha_hasta\" >= date_format(er.fecha, '%Y/%m/%d')
  group by e.documento
  ORDER BY e.id, er.fecha"
);

$objPHPExcel = new MyPHPExcel();

$as = $objPHPExcel->getActiveSheet();
$as->SetCellValue('A1', "Documento");
$as->SetCellValue('B1', "Nombre");
$as->SetCellValue('C1', "Trabajado");
$as->getStyle("A1:B1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$as->getStyle("A1:B1")->getFont()->setBold(true);
$as->getStyle("A1:B1")->getFill()->getStartColor()->setARGB("666666");
$as->getStyle("A1:B1")->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
$i = 2;
foreach ($rows as $row) {
  $as->SetCellValue("A$i", $row["documento"]);
  $as->SetCellValue("B$i", $row["nombre"]);
  $tiempo = convertToHoursMins($row["minutos"], '%02d horas %02d minutos');
  $as->SetCellValue("C$i", $tiempo);
  $i++;
}
$hasta = $i - 1;
$as->getStyle("A$i:B$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);

$as->getStyle("A$i:B$i")->getFont()->setBold(true);
$as->getStyle("B$i:B$i")->getFill()->getStartColor()->setARGB("666666");
$as->getStyle("C$i:B$i")->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);


$as->getColumnDimension('A')->setAutoSize(true);
$as->getColumnDimension('B')->setAutoSize(true);
$as->getColumnDimension('C')->setAutoSize(true);


$objPHPExcel->output("Horarios_" . date("Y-m-d"));

function convertToHoursMins($time, $format = '%02d:%02d') {
      if ($time < 1) {
        return;
      }
      $hours = floor($time / 60);
      $minutes = ($time % 60);
      return sprintf($format, $hours, $minutes);
    }