<?php
$fecha_hasta = date('Y/m/d', strtotime(str_replace('/','.', $fecha_hasta)));
$fecha_desde = date('Y/m/d', strtotime(str_replace('/','.', $fecha_desde)));
$ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
$select = "
  SELECT e.documento, date_format(er.fecha, '%d/%m/%Y') as fecha, er.minutos, er.tiempo 
    FROM empleado e
     INNER JOIN empleado_registro er ON e.id = er.empleado_id 
  where
     \"$fecha_desde\" <= date_format(er.fecha, '%Y/%m/%d') and \"$fecha_hasta\" >= date_format(er.fecha, '%Y/%m/%d')
  ORDER BY e.id, er.fecha";
//vd($select);
$rows = Helpers::qryAll($select);

$objPHPExcel = new MyPHPExcel();

$as = $objPHPExcel->getActiveSheet();
$as->SetCellValue('A1', "Documento");
$as->SetCellValue('B1', "Fecha");
$as->SetCellValue('C1', "Minutos");
$as->SetCellValue('D1', "Tiempo");
$as->getStyle("A1:D1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$as->getStyle("A1:D1")->getFont()->setBold(true);
$as->getStyle("A1:D1")->getFill()->getStartColor()->setARGB("666666");
$as->getStyle("A1:D1")->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
$i = 2;
foreach ($rows as $row) {
    $as->SetCellValue("A$i", $row["documento"]);
    $as->SetCellValue("B$i", $row["fecha"]);
    $as->SetCellValue("C$i", $row["minutos"]);
    $as->SetCellValue("D$i", $row["tiempo"]);
    $i++;
}
$hasta = $i - 1;
$as->getStyle("A$i:D$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);

$as->getStyle("A$i:D$i")->getFont()->setBold(true);
$as->getStyle("A$i:D$i")->getFill()->getStartColor()->setARGB("666666");
$as->getStyle("A$i:D$i")->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);


$as->getStyle("C2:C$i")->getNumberFormat()->setFormatCode('#,##0.00');

$as->getColumnDimension('A')->setAutoSize(true);
$as->getColumnDimension('B')->setAutoSize(true);
$as->getColumnDimension('C')->setAutoSize(true);
$as->getColumnDimension('D')->setAutoSize(true);

$objPHPExcel->output("Horarios_" . date("Y-m-d") );