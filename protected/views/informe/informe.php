<?php

class MYPDF extends PDF {

    public $informe, $Y, $imgHeader;

    public function header() {
        // $this->Image($this->informe->imgHeader, 2, 2, 14);

        $this->setY($this->informe->marginY);

        $this->t($this->informe->args["title"], 0, null, "C", $this->getPageWidth());

        $txt = "Impreso el";
        $this->setX($this->getPageWidth() - $this->getStringWidth($txt) - 3);
        $this->setY($this->getY() + $this->t($txt));

        $fecha = date("d/m/Y", time());
        $this->setX($this->getPageWidth() - $this->getStringWidth($fecha) - 3);
        $this->t($fecha);


        $this->setY($this->getY() + $this->t($this->informe->args["title2"], 0, null, "C", $this->getPageWidth()));

        $this->Y = $this->getY() + 2;
        $this->line(0, $this->Y, $this->getPageWidth(), $this->Y);
        $this->Y = $this->getY() + 2;

        $x = $this->informe->marginX;
        $h = "";
        foreach ($this->informe->cols as $col) {
            $h = $this->getStringHeight($this->getStringWidth($col["label"]), $col["label"]);
            if ($col["debeHaber"]) {
                $this->textBox(array(
                        "txt" => "Debe",
                        "y" => $this->Y,
                        "x" => $x,
                        "w" => $col["width"],
                        "h" => $h,
                        "align" => $col["align"],
                ));
                $x += $col["width"];
                $this->textBox(array(
                        "txt" => "Haber",
                        "y" => $this->Y,
                        "x" => $x,
                        "w" => $col["width"],
                        "h" => $h,
                        "align" => $col["align"],
                ));
                $x += $col["width"];
                $this->textBox(array(
                        "txt" => "Saldo",
                        "y" => $this->Y,
                        "x" => $x+1,
                        "w" => $col["width"]+1,
                        "h" => $h,
                        "align" => $col["align"],
                ));
            } else {
                $this->textBox(array(
                        "txt" => trim($col["label"]),
                        "y" => $this->Y,
                        "x" => $x,
                        "w" => $col["width"],
                        "h" => $h,
                        "align" => $col["align"],
                ));
                $x += $col["width"];
            }
        }
        $this->Y += $h;
        $this->line(0, $this->Y, $this->getPageWidth(), $this->Y);
        $this->Y += 1;
    }

    public function footer() {
        $txt = "Pág.:" . $this->getPage();
        $w = $this->getStringWidth($txt) + 2;
        $this->textBox(array(
                "txt" => $txt,
                "y" => $this->getPageHeight() - 7,
                "x" => $this->getPageWidth() - $w,
                "w" => $w,
        ));
    }

    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
    }

}

class Informe extends stdClass {

    public $pdf, $data, $marginX, $marginY, $cols, $colsDef, $colsDefDetail, $totals = array();

    public function __construct($options, $colsDef = array(), $colsDefDetail = array()) {
        if (!isset($options["data"])) {
            throw new Exception("Falta mandar el parámetro con los datos al informe genérico");
        }
        $this->imgHeader = "http://" . $_SERVER["SERVER_NAME"] . '/' . Yii::app()->baseUrl . "/images/logo.jpg";
        $this->data = $options["data"];
        $this->colsDef = $colsDef;
        $this->colsDefDetail = $colsDefDetail;
        $defaults = array(
                "paperSize" => "A4",
                "orientation" => "P",
                "marginX" => 0,
                "marginY" => 0,
                "fontSize" => 7,
                "showHeader" => true,
                "showFooter" => true,
                "title" => "",
                "title2" => "",
        );
        $this->args = array_merge($defaults, $options);
        $this->marginY +=$this->args["marginY"];
        $this->marginX = $this->args["marginX"];
    }

    private function initPdf() {
        $this->pdf->planilla = $this;
        $this->pdf->SetCreator(PDF_CREATOR);
        $this->pdf->SetAuthor('JP');
        $this->pdf->SetTitle($this->args["title"]);
        $this->pdf->SetSubject($this->args["title"]);
        $this->pdf->SetKeywords('TCPDF, PDF, Planilla');
        $this->pdf->SetPrintHeader(true);
        $this->pdf->SetPrintFooter(true);
        $this->pdf->SetAutoPageBreak(false);
        $this->pdf->SetMargins($this->marginX, $this->marginY);
        $this->fontSize = $this->args["fontSize"];
        $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", 11);
        $this->pdf->setCellPaddings(0, 0, 0, 0);
        $this->pdf->AddPage();
    }

    public function render() {
        $this->pdf = new MYPDF($this->args["orientation"], "mm", $this->args["paperSize"]);
        $this->pdf->informe = $this;
        $this->cols = array();
        foreach ($this->data as $row) {
            foreach ($row as $key => $col) {
                if (!is_array($col)) {
                    $debeHaber = false;
                    $align = "L";
                    if (isset($this->colsDef[$key])) {
                        if (isset($this->colsDef[$key]["visible"]) and !$this->colsDef[$key]["visible"]) {
                            continue;
                        }
                        if (isset($this->colsDef[$key]["date"])) {
                            $format = $this->colsDef[$key]["date"] ? $this->colsDef[$key]["date"] : "d/m/Y";
                            $col = date($format, mystrtotime($col));
                        }
                        if (isset($this->colsDef[$key]["width"])) {
                            //$col = date("d/m/Y", mystrtotime($col));
                        }
                        if (isset($this->colsDef[$key]["debeHaber"])) {
                            $debeHaber = true;
                            $col = number_format($col, 2);
                        } elseif (isset($this->colsDef[$key]["currency"])) {
                            $col = number_format($col, 2);
                        }
                        if (isset($this->colsDef[$key]["align"])) {
                            $align = "R";
                        }
                    }

                    if (isset($this->colsDef[$key]["width"])) {
                        $this->cols[$key]["width"] = $this->colsDef[$key]["width"];
                    } else {
                        $this->cols[$key]["width"] =
                                (isset($this->cols[$key]) and ($this->cols[$key]["width"] > $this->pdf->getStringWidth($col))) ?
                                $this->cols[$key] ["width"] :
                                $this->pdf->getStringWidth($col) + 2;
                        $this->cols[$key]["width"] =
                                ($this->cols[$key]["width"] > $this->pdf->getStringWidth($key)) ?
                                $this->cols[$key]["width"] :
                                $this->pdf->getStringWidth($key) + 2;
                    }
                    $this->cols[$key]["debeHaber"] = $debeHaber ? true : false;

                    $this->cols[$key]["align"] = $align;
                    $this->cols[$key]["label"] = mb_convert_case($key, MB_CASE_TITLE, "UTF-8");
                    if (isset($this->colsDef[$key])) {
                        if (isset($this->colsDef[$key]["label"])) {
                            $this->cols[$key]["label"] = $this->colsDef[$key]["label"];
                        }
                        if (isset($this->colsDef[$key]["fecha"]["date"])) {
                            $format = $this->colsDef[$key]["date"] ? $this->colsDef[$key]["date"] : "d/m/Y";
                            $this->cols[$key]["date"] = $format;
                        }
                        if (isset($this->colsDef[$key]["sum"])) {
                            $this->totals[$key] = 0;
                        }
                    }
                }
            }
        }
        $this->initPdf();
        //
        $this->renderData($this->data, null, null, $this->colsDef);
        //
        $this->pdf->Output();
    }

    private function renderData($data, $y = null, $x = 0, $colsDef = array(), $esDetalle = false) {
        $y = $y ? $y : $this->pdf->Y;
        $left = $this->marginX + $x;
        $saldo = 0;
        foreach ($data as $row) {
            $x = $left;
            $height = 0;
            foreach ($row as $key => $col) {
                if (!is_array($col)) {
                    $col = trim($col);
                    $h = $this->pdf->getStringHeight($this->pdf->getStringWidth($col) + 2, $col, false, false, '', 0);
                    $height = ($height > $h) ? $height : $h;
                }
            }
            if ($this->chkPageEnd($height)) {
                $y = $this->pdf->Y;
                $x = $left;
            }

            foreach ($row as $key => $col) {
                if (!is_array($col)) {
                    $debeHaber = false;
                    if (isset($colsDef[$key])) {
                        if (isset($colsDef[$key]["visible"]) and !$colsDef[$key]["visible"]) {
                            continue;
                        }
                        if (isset($colsDef[$key]["date"])) {
                            $format = $colsDef[$key]["date"] ? $colsDef[$key]["date"] : "d/m/Y";
                            $col = date($format, mystrtotime($col));
                        }
                        if (isset($colsDef[$key]["debeHaber"])) {
                            $debeHaber = $colsDef[$key]["debeHaber"];
                        } elseif (isset($colsDef[$key]["currency"])) {
                            $col = number_format($col, 2);
                        }
                        if (isset($this->totals[$key])) {
                            $this->totals[$key] += str_replace(",", "", $col)+0;
                        }
                        if (isset($colsDef[$key]["mb_convert_case"])) {
                            $col = mb_convert_case($col, MB_CASE_TITLE, "UTF-8");
                        }
                        if (isset($colsDef[$key]["strtolower"])) {
                            //$col = strtolower($col);
                        }
                    }
                    if (isset($colsDef[$key]["width"]) and $esDetalle) {
                        $w = $colsDef[$key]["width"];
                    }elseif (isset($this->cols[$key]["width"])) {
                        $w = $this->cols[$key]["width"];
                    } elseif (isset($colsDef[$key]["width"])) {
                        $w = $colsDef[$key]["width"];
                    } else {
                        $w = $this->pdf->getStringWidth($col) + 2;
                    }
                    if ($debeHaber) {
                        if ($debeHaber == "dh") {
                            $debe = ($col >= 0) ? number_format(abs($col), 2) : "";
                            $haber = ($col < 0) ? number_format(abs($col), 2) : "";
                        } else {
                            $debe = ($col < 0) ? number_format(abs($col), 2) : "";
                            $haber = ($col >= 0) ? number_format(abs($col), 2) : "";
                        }
                        $saldo += $col;
                        $this->pdf->textBox(array(
                                "txt" => $debe,
                                "y" => $y,
                                "x" => $x,
                                "w" => $w,
                                "h" => $height,
                                "align" => isset($this->cols[$key]) ? $this->cols[$key]["align"] : "L",
                        ));
                        $x+=$w;
                        $this->pdf->textBox(array(
                                "txt" => $haber,
                                "y" => $y,
                                "x" => $x,
                                "w" => $w,
                                "h" => $height,
                                "align" => isset($this->cols[$key]) ? $this->cols[$key]["align"] : "L",
                        ));
                        $x+=$w;
                        $this->pdf->textBox(array(
                                "txt" => number_format($saldo, 2),
                                "y" => $y,
                                "x" => $x+1,
                                "w" => $w+1,
                                "h" => $height,
                                "align" => isset($this->cols[$key]) ? $this->cols[$key]["align"] : "L",
                        ));
                        $x+=$w;
                    } else {
                        $this->pdf->textBox(array(
                                "txt" => $col,
                                "y" => $y,
                                "x" => $x,
                                "w" => $w,
                                "h" => $height,
                                "align" => isset($this->cols[$key]) ? $this->cols[$key]["align"] : "L",
                        ));
                        $x+=$w;
                    }
                }
            }
            $y+=$height;
            if (isset($row["detail"]) and count($row["detail"]) > 0) {
                $y = $this->renderData($row["detail"], $y, 5, $this->colsDefDetail, true) + 7;
            }
        }

        if (count($data) > 0) {
            $x = $left;
            foreach ($data[0] as $key => $col) {
                if (!is_array($col)) {
                    if (isset($colsDef[$key]["visible"]) and !$colsDef[$key]["visible"]) {
                        continue;
                    }
                    $debeHaber = isset($colsDef[$key]["debeHaber"]) ? true : false;
                    if (isset($this->cols[$key]["width"])) {
                        $w = $this->cols[$key]["width"];
                    } else {
                        $w = $this->pdf->getStringWidth($col) + 2;
                    }
                    if ($debeHaber) {
                        $x += $w;
                        $this->pdf->textBox(array(
                                "txt" => number_format($saldo, 2),
                                "y" => $y,
                                "x" => $x,
                                "w" => $w,
                                "h" => $height,
                                "align" => isset($this->cols[$key]) ? $this->cols[$key]["align"] : "L",
                        ));
                    } else {
                        $col = isset($this->totals[$key]) ? number_format($this->totals[$key], 2) : "";
                        $this->pdf->textBox(array(
                                "txt" => $col,
                                "y" => $y,
                                "x" => $x,
                                "w" => $w,
                                "h" => $height,
                                "align" => isset($this->cols[$key]) ? $this->cols[$key]["align"] : "L",
                        ));
                        $x+=$w;
                    }
                }
            }
        }
        return $y;
    }

    private function chkPageEnd($height) {
        if (($this->pdf->getY() + $height + 15) >= $this->pdf->getPageHeight()) {
            $this->pdf->AddPage();
            return true;
        } else {
            return false;
        }
    }

}

