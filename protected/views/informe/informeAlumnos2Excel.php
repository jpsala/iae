<?php
$fileName = "Alumnos_planilla_buffet.xls";
$ciclo_id = Ciclo::getCicloIdParaInformesEducativo(1);
$direccion = "concat (COALESCE(p1.calle, ''), ' ', COALESCE(p1.numero, ''), ' ', COALESCE(p1.piso, ''), ' ', COALESCE(p1.departamento, '')) as domicilio";
$where = "a.activo = 1 ";
$where .= ($nivel_id == "-1") ? "" : " and n.id = $nivel_id";
$where .= ($anio_id == "-1") ? "" : " and anio.id = $anio_id";
$where .= ($division_id == "-1") ? "" : " and d.id = $division_id";
$esCicloActual = Ciclo::esActual();
$andActivo = $esCicloActual ? " and ad.activo  " : " ";
$nivel_id = Helpers::qryScalar("SELECT n.id FROM nivel n
  INNER JOIN anio a ON n.id = a.Nivel_id
  INNER JOIN division d ON a.id = d.Anio_id AND d.id = $division_id");
$order = $nivel_id == 4 ?
"n.orden, anio.orden, d.nombre, a.apellido, a.nombre" :
"n.orden, anio.orden, d.nombre, a.sexo desc, a.apellido, a.nombre";
$sql = "
select concat(a.apellido,' ', a.nombre) as Nombre,
  a.matricula, n.nombre as nivel, anio.nombre as curso, d.nombre as division, a.sexo
from alumno a
  left join alumno_division ad on ad.Alumno_id = a.id and ad.ciclo_id = $ciclo_id
  $andActivo and not ad.borrado /*((ad.activo or ad.promocionado or ad.egresado)*/
  /*inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_edu = 1*/
  left join division d on d.id = ad.Division_id
  left join anio on anio.id = d.Anio_id
  left join nivel n on n.id = anio.Nivel_id
where $where
order by $order
";
$rows = Helpers::qryAll($sql);
ini_set('memory_limit', '-1');

$FIRST_CELL = "A1";

$objPHPExcel = new MyPHPExcel();
$as = $objPHPExcel->setActiveSheetIndex(0);

$as->getStyle("$FIRST_CELL:B1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$as->getStyle("$FIRST_CELL:B1")->getFont()->setBold(true);
$as->getStyle("$FIRST_CELL:B1")->getFill()->getStartColor()->setARGB("666666");
$as->getStyle("$FIRST_CELL:B1")->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);

if (count($rows) < 1)
	return;

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);

$as->mergeCells('A1:Z1');
$as->getStyle("A1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$as->SetCellValue("A1", "Reporte de Alumnos (Apellido y Nombre)");
$as->SetCellValue("A2", "Nivel  " . $rows[0]["nivel"]);
$as->SetCellValue("A3", "Curso " . $rows[0]["curso"] . "/" . $rows[0]["division"]);
$rowNum = 5;
foreach ($rows as $row) {
	$as->SetCellValue("A" . $rowNum, $row["Nombre"]);
	for ($col = 2; $col <= 26; $col++) {
		$scol = chr($col + 64);
		$objPHPExcel->getActiveSheet()->getStyle("$scol$rowNum")->applyFromArray($styleArray);
	}
	$rowNum++;
}
$rowNum--;
$objPHPExcel->getActiveSheet()->getStyle("A1:Z$rowNum")->applyFromArray($styleArray);
for ($col = 2; $col <= 26; $col++) {
	$scol = chr($col + 64);
	$as->getColumnDimension($scol)->setWidth(4);
}
$as->SetCellValue("A" . ($rowNum + 2), "Planilla impresa el " . date("d/m/Y", time()));

$as->getColumnDimension("A")->setAutoSize(true);

$as->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
$as->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_LEGAL);
$as->getPageSetup()->setFitToPage(true);
$as->getPageSetup()->setFitToWidth(1);
$as->getPageSetup()->setFitToHeight(0);

$objPHPExcel->output($fileName);
?>
