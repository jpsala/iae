<?php

$fileName = "Alumnos_planilla_buffet";
$ciclo_id = Ciclo::getCicloIdParaInformesEducativo(1);
$direccion = "concat (COALESCE(p1.calle, ''), ' ', COALESCE(p1.numero, ''), ' ', COALESCE(p1.piso, ''), ' ', COALESCE(p1.departamento, '')) as domicilio";
$where = "a.activo = 1 ";
$where .= ($nivel_id == "-1") ? "" : " and n.id = $nivel_id";
$where .= ($anio_id == "-1") ? "" : " and anio.id = $anio_id";
$where .= ($division_id == "-1") ? "" : " and d.id = $division_id";
$nivel_id = Helpers::qryScalar("SELECT n.id FROM nivel n
  INNER JOIN anio a ON n.id = a.Nivel_id
  INNER JOIN division d ON a.id = d.Anio_id AND d.id = $division_id");
$order = $nivel_id == 4 ? 
     "a.apellido, a.nombre" :
     "a.sexo desc, a.apellido, a.nombre";

$sql = "
	/*set lc_time_names='es_AR';*/
	SELECT nombre_completo_alumno(s.id) AS alumno/*, nombre_completo_pariente(p.id) AS vive_con,
                 domicilio_pariente(p.id) AS domicilio*/, DATE_FORMAT( a.fecha_nacimiento, '%e %b %Y') AS fecha_nac,
                 p.telefono_casa, p.telefono_trabajo, p.telefono_celular
        FROM alumno a
            INNER JOIN socio s ON a.id = s.Alumno_id
            INNER JOIN pariente p ON a.vive_con_id = p.id
            INNER JOIN pariente_tipo pt ON p.Pariente_Tipo_id = pt.id
            INNER JOIN alumno_division ad ON ad.Alumno_id = a.id and ad.Ciclo_id = $ciclo_id
              and ad.activo and not ad.borrado AND ad.Division_id = $division_id
            INNER JOIN alumno_estado ae ON ae.id = a.estado_id AND ae.activo_edu
    order by $order

";

//vd($sql);
$rows = Helpers::qryAll($sql);
ini_set('memory_limit', '-1');

$FIRST_CELL = "A1";

$objPHPExcel = new MyPHPExcel();
/* @var $as PHPExcel_Worksheet */
$as = $objPHPExcel->setActiveSheetIndex(0);

$as->getStyle("$FIRST_CELL:B1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$as->getStyle("$FIRST_CELL:B1")->getFont()->setBold(true);
$as->getStyle("$FIRST_CELL:B1")->getFill()->getStartColor()->setARGB("666666");
$as->getStyle("$FIRST_CELL:B1")->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);

if (count($rows) < 1)
	return;

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);

if (count($rows) < 1) {
	return;
}
$as->mergeCells('A1:Z1');
$as->getStyle("A1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$as->SetCellValue("A1", "Reporte de Alumnos (Apellido y Nombre)");
$as->SetCellValue("A2", "Nivel  " . $data["nivel"]);
$as->SetCellValue("A3", "Curso " . $data["curso"] . "/" . $data["division"]);
$rowNum = 5;
Col::init();
foreach (array_keys($rows[0]) as $col) {
	$scol = Col::next();
	$objPHPExcel->getActiveSheet(0)->getStyle("$scol$rowNum")->applyFromArray($styleArray);
	$objPHPExcel->getActiveSheet(0)->setCellValue($scol . $rowNum, $col);
}
$rowNum = 6;
foreach ($rows as $row) {
	Col::init();
	foreach ($row as $col) {
		$scol = Col::next();
		$objPHPExcel->getActiveSheet(0)->getStyle("$scol$rowNum")->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet(0)->setCellValue($scol . $rowNum, $col);
	}
	$rowNum++;
}

Col::init();
foreach (array_keys($rows[0]) as $col) {
	$scol = Col::next();
	$as->getColumnDimension($scol)->setAutoSize(true);
}

$rowNum--;
$as->SetCellValue("A" . ($rowNum + 2), "Planilla impresa el " . date("d/m/Y", time()));

$as->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$as->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_LEGAL);
$as->getPageSetup()->setFitToPage(true);
$as->getPageSetup()->setFitToWidth(1);
$as->getPageSetup()->setFitToHeight(0);

$objPHPExcel->output($fileName);
?>
