<?php
$ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
$rows = Helpers::qryAll("
            select concat(a.apellido, ', ', a.nombre) as nombre,
                    case when a.beca > 0 then ar.precio_neto - ((ar.precio_neto * a.beca) / 100) else ar.precio_neto end as cuota,
                    saldo(s.id, null) as saldo/*, a.beca, ar.precio_neto*/
               from alumno a
				inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_admin = 1
                    inner join socio s on s.Alumno_id = a.id
                    left join alumno_division ad on ad.Alumno_id = a.id  
												and ad.Ciclo_id = $ciclo_id 
												/*and ad.activo*/ and not ad.borrado /*((ad.activo or ad.promocionado or ad.egresado)*/
                    left join division d on d.id = ad.Division_id
                    left join anio on anio.id = d.Anio_id
                    left  join articulo ar on ar.id = anio.articulo_id
               where  a.activo = 1 and saldo(s.id, null) >= 100 /*ar.precio_neto*/
               order by a.apellido, a.nombre");

$objPHPExcel = new MyPHPExcel();

$as = $objPHPExcel->getActiveSheet();
$as->SetCellValue('A1', "Nombre");
$as->SetCellValue('B1', "Saldo");
$as->SetCellValue('C1', "Cuota");
$as->getStyle("A1:C1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$as->getStyle("A1:C1")->getFont()->setBold(true);
$as->getStyle("A1:C1")->getFill()->getStartColor()->setARGB("666666");
$as->getStyle("A1:C1")->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
$i = 2;
foreach ($rows as $row) {
    $as->SetCellValue("A$i", $row["nombre"]);
    $as->SetCellValue("B$i", $row["saldo"]);
    $as->SetCellValue("C$i", $row["cuota"]);
    $i++;
}
$hasta = $i - 1;
$as->getStyle("A$i:C$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);

$as->getStyle("A$i:C$i")->getFont()->setBold(true);
$as->getStyle("A$i:C$i")->getFill()->getStartColor()->setARGB("666666");
$as->getStyle("A$i:C$i")->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);

$as->setCellValue("A$i", "Totales");
$as->setCellValue("B$i", "=SUM(B2:B$hasta)");
$as->setCellValue("C$i", "=SUM(C2:C$hasta)");


$as->getStyle("B2:B$i")->getNumberFormat()->setFormatCode('#,##0.00');
$as->getStyle("C2:C$i")->getNumberFormat()->setFormatCode('#,##0.00');

$as->getColumnDimension('A')->setAutoSize(true);
$as->getColumnDimension('B')->setAutoSize(true);
$as->getColumnDimension('C')->setAutoSize(true);

$objPHPExcel->output("Cobranza_" . date("Y-m-d") );