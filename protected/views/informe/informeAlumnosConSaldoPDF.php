<?php

include("informe.php");
$ciclo_id = Ciclo::getCicloIdParaInformesEducativo();
$data = Helpers::qryAll("
            select concat(a.apellido, ', ', a.nombre) as nombre,
                    case when a.beca > 0 then ar.precio_neto - ((ar.precio_neto * a.beca) / 100) else ar.precio_neto end as cuota,
                    saldo(s.id,null) as saldo/*, a.beca, ar.precio_neto*/
               from alumno a
				inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_admin = 1
                    inner join socio s on s.Alumno_id = a.id
                    left join alumno_division ad on ad.Alumno_id = a.id 
											and ad.activo /*((ad.activo or ad.promocionado or ad.egresado)*/
											and ad.Ciclo_id = $ciclo_id
                    left join division d on d.id = ad.Division_id
                    left join anio on anio.id = d.Anio_id
                    left  join articulo ar on ar.id = anio.articulo_id
               where  a.activo = 1 and saldo(s.id,null) >= 100 /*ar.precio_neto*/
               order by a.apellido, a.nombre");
$colsDef = array(
        "nombre" => array("width" => 160,"visible"=>true),
        "cuota" => array("width" => 21,"currency"=>true, "align" => "R", "sum"=>true),
        "saldo" => array("width" => 21,"currency"=>true, "align" => "R", "sum"=>true),
        //"beca" => array("width" => 28,"currency"=>true, "align" => "R", "sum"=>true),
        //"precio_neto" => array("width" => 28,"currency"=>true, "align" => "R", "sum"=>true),
  
);

$colsDefDetail = array(

);

$options = array(
        "data" => $data,
        "marginY" => 2,
        "marginX" => 5,
        "title" => "Saldos al ".date("d/m/Y",time()),
);
$i = new Informe($options, $colsDef, $colsDefDetail);
$i->render();
?>