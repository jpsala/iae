<?php
//No cambiar al informe genérico!!!!!!!!!!!!!!!!!!!
include("informeBancos.php");
$fecha_desde = date("Y/m/d", mystrtotime($fecha_desde));
$fecha_hasta = date("Y/m/d", mystrtotime($fecha_hasta)) . " 23:59:59";
//IF((dv.tipo=3 or dv.tipo=6),dv.fecha,d.fecha_creacion)
$select = "     select dv.fecha as fecha,d.fecha_creacion, d.numero, d.detalle, c.signo_banco,
                concat(LPAD(d.sucursal, 4, '0'),'-',LPAD(d.numero, 8, '0')) as docnumero, dv.obs, 
                c.abreviacion, (dv.importe *  c.signo_banco) * -1 as importe , 
                dv.tipo
                from doc_valor dv
		inner join doc d on d.id = dv.Doc_id
		inner join talonario t on t.id = d.talonario_id
		inner join comprob c on c.id = t.comprob_id
                where d.anulado = 0 and dv.destino_id = $destino_id and d.activo = 1
                and dv.fecha BETWEEN \"$fecha_desde\" and \"$fecha_hasta\"
                order by fecha";

  
 //echo($select); die;                                                               
    
$data = Helpers::qryAll($select);
$saldo_ant = Helpers::qryScalar("
                select sum(dv.importe *  c.signo_banco)* -1 as importe 
                from doc_valor dv
		inner join doc d on d.id = dv.Doc_id
		inner join talonario t on t.id = d.talonario_id
		inner join comprob c on c.id = t.comprob_id
                where d.anulado = 0 and dv.destino_id = $destino_id and d.activo = 1
                and dv.fecha < \"$fecha_desde\"     
");

$cuenta = Helpers::qryScalar("
    select concat(d.nombre,', ' , d.numero_cuenta) as cuenta 
        from destino d
        where d.id = $destino_id
");
$colsDef = array(
        "fecha" => array("date"=>"d/m/Y", "label"=>"F. Acred.","width" => 25),
        "fecha_creacion" => array("date"=>"d/m/Y", "label"=>"F. Mov.","width" => 25),
        "numero" => array("visible" => true),
        "detalle" => array("visible" => true),
        "obs" => array("visible" => true),
        "docnumero" => array("visible" => false),
        "signo_banco" => array("visible" => false),
        "abreviacion" => array("visible" => true,"width" => 20),
        "importe" => array("debeHaber"=>"db", "width" => 20,"currency"=>true, "align" => "R", "sum"=>true),
  
);

$colsDefDetail = array(

);

$options = array(
        "data" => $data,
        "marginY" => 2,
        "marginX" => 5,
        "title" => "Informe de Banco entre el " . date("d/m/Y", mystrtotime($fecha_desde) ) . " y el " . date("d/m/Y", mystrtotime($fecha_hasta)),
        "title2" => $cuenta,
        "saldo_ant"=>$saldo_ant,
);
$i = new Informe($options, $colsDef, $colsDefDetail);
$i->render();
?>