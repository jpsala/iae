<?php
//No cambiar al informe genérico!!!!!!!!!!!!!!!!!!!
class MYPDF extends PDF {

    public $informe, $Y, $imgHeader;

    public function header() {

        $this->Image($this->informe->imgHeader, 2, 2, 14);

        $this->setY($this->informe->marginY);

        $this->t($this->informe->args["title"], 0, null, "C", $this->getPageWidth());

        $txt = "Impreso el";
        $this->setX($this->getPageWidth() - $this->getStringWidth($txt) - 3);
        $this->setY($this->getY() + $this->t($txt));

        $fecha = date("d/m/Y", time());
        $this->setX($this->getPageWidth() - $this->getStringWidth($fecha) - 3);
        $this->t($fecha);


        $this->setY($this->getY() + $this->t($this->informe->args["title2"], 0, null, "C", $this->getPageWidth()));

        $this->Y = $this->getY() + 2;

        $this->Y = $this->getY() + 2;

        $this->setY($this->getY() + $this->t('Saldo Anterior: ' . $this->informe->args["saldo_ant"], 0, null, "C", $this->getPageWidth()));

        $this->Y = $this->getY() + 3;
        $this->line(0, $this->Y, $this->getPageWidth(), $this->Y);
        $this->Y = $this->getY() + 3;

        $x = $this->informe->marginX;
        $h = "10";

                $this->textBox(array(
                        "txt" => 'F. Acred.',
                        "y" => $this->Y,
                        "x" => $x,
                        "w" => 20,
                        "h" => $h,
                        "align" => 'L',
                ));
                $this->textBox(array(
                        "txt" => 'F. Mov.',
                        "y" => $this->Y,
                        "x" => $x + 23,
                        "w" => 20,
                        "h" => $h,
                        "align" => 'L',
                ));
                 $this->textBox(array(
                        "txt" => 'Núm.',
                        "y" => $this->Y,
                        "x" => $x + 45,
                        "w" => 20,
                        "h" => $h,
                        "align" => 'L',
                ));

                 $this->textBox(array(
                        "txt" => 'Detalle',
                        "y" => $this->Y,
                        "x" => $x + 55,
                        "w" => 20,
                        "h" => $h,
                        "align" => 'L',
                ));
                $this->textBox(array(
                        "txt" => 'Debe',
                        "y" => $this->Y,
                        "x" => $x + 110,
                        "w" => 30,
                        "h" => $h,
                        "align" => 'C',
                ));
                $this->textBox(array(
                        "txt" => 'Haber',
                        "y" => $this->Y,
                        "x" => $x + 135,
                        "w" => 30,
                        "h" => $h,
                        "align" => 'C',
                ));
                $this->textBox(array(
                        "txt" => 'Saldo',
                        "y" => $this->Y,
                        "x" => $x + 160,
                        "w" => 30,
                        "h" => $h,
                        "align" => 'C',
                ));

        $this->Y += $h;
        $this->line(0, $this->Y, $this->getPageWidth(), $this->Y);
        $this->Y += 1;
    }

    public function footer() {
        $txt = "Pág.:" . $this->getPage();
        $w = $this->getStringWidth($txt) + 2;
        $this->textBox(array(
                "txt" => $txt,
                "y" => $this->getPageHeight() - 7,
                "x" => $this->getPageWidth() - $w,
                "w" => $w,
        ));
    }

    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
    }

}

class Informe extends stdClass {

    public $pdf, $data, $marginX, $marginY, $cols, $colsDef, $colsDefDetail, $totals, $saldo_ant = array();

    public function __construct($options, $colsDef = array(), $colsDefDetail = array()) {

        if (!isset($options["data"])) {
            throw new Exception("Falta mandar el parámetro con los datos al informe genérico");
        }
        $this->imgHeader = "http://" . $_SERVER["SERVER_NAME"] . '/' . Yii::app()->baseUrl . "/images/logo.jpg";
        $this->data = $options["data"];
        $this->saldo_ant = $options["saldo_ant"];
        $this->colsDef = $colsDef;
        $this->colsDefDetail = $colsDefDetail;
        $defaults = array(
                "paperSize" => "A4",
                "orientation" => "P",
                "marginX" => 0,
                "marginY" => 0,
                "fontSize" => 6,
                "showHeader" => true,
                "showFooter" => true,
                "title" => "",
                "title2" => "",
        );
        $this->args = array_merge($defaults, $options);
        $this->marginY +=$this->args["marginY"];
        $this->marginX = $this->args["marginX"];
    }

    private function initPdf() {
        $this->pdf->planilla = $this;
        $this->pdf->SetCreator(PDF_CREATOR);
        $this->pdf->SetAuthor('JP');
        $this->pdf->SetTitle('Planilla Bancos');
        $this->pdf->SetSubject('Planilla Bancos');
        $this->pdf->SetKeywords('TCPDF, PDF, Bancos');
        $this->pdf->SetPrintHeader(true);
        $this->pdf->SetPrintFooter(true);
        $this->pdf->SetAutoPageBreak(false);
        $this->margenX = $this->args["marginX"];
        $this->margenY = $this->args["marginY"];
        $this->fontSize = $this->args["fontSize"];
        $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", 11);
        $this->pdf->setCellPaddings(0, 0, 0, 0);
        $this->pdf->AddPage();
    }

    public function render() {
        $this->pdf = new MYPDF($this->args["orientation"], "mm", $this->args["paperSize"]);
        $this->pdf->informe = $this;
        $this->cols = array();
//        foreach ($this->data as $row) {
//            foreach ($row as $key => $col) {
//                if (!is_array($col)) {
//                    $debeHaber = false;
//                    $align = "L";
//                    if (isset($this->colsDef[$key])) {
//                        if (isset($this->colsDef[$key]["visible"]) and !$this->colsDef[$key]["visible"]) {
//                            continue;
//                        }
//                        if (isset($this->colsDef[$key]["date"])) {
//                            $format = $this->colsDef[$key]["date"] ? $this->colsDef[$key]["date"] : "d/m/Y";
//                            $col = date($format, mystrtotime($col));
//                        }
//                        if (isset($this->colsDef[$key]["width"])) {
//                            //$col = date("d/m/Y", mystrtotime($col));
//                        }
//                        if (isset($this->colsDef[$key]["debeHaber"])) {
//                            $debeHaber = true;
//                            $col = number_format($col, 2);
//                        } elseif (isset($this->colsDef[$key]["currency"])) {
//                            $col = number_format($col, 2);
//                        }
//                        if (isset($this->colsDef[$key]["align"])) {
//                            $align = "R";
//                        }
//                    }
//
//                    if (isset($this->colsDef[$key]["width"])) {
//                        $this->cols[$key]["width"] = $this->colsDef[$key]["width"];
//                    } else {
//                        $this->cols[$key]["width"] =
//                                (isset($this->cols[$key]) and ($this->cols[$key]["width"] > $this->pdf->getStringWidth($col))) ?
//                                $this->cols[$key] ["width"] :
//                                $this->pdf->getStringWidth($col) + 2;
//                        $this->cols[$key]["width"] =
//                                ($this->cols[$key]["width"] > $this->pdf->getStringWidth($key)) ?
//                                $this->cols[$key]["width"] :
//                                $this->pdf->getStringWidth($key) + 2;
//                    }
//                    $this->cols[$key]["debeHaber"] = $debeHaber ? true : false;
//
//                    $this->cols[$key]["align"] = $align;
//                    $this->cols[$key]["label"] = mb_convert_case($key, MB_CASE_TITLE, "UTF-8");
//                    if (isset($this->colsDef[$key])) {
//                        if (isset($this->colsDef[$key]["label"])) {
//                            $this->cols[$key]["label"] = $this->colsDef[$key]["label"];
//                        }
//                        if (isset($this->colsDef[$key]["fecha"]["date"])) {
//                            $format = $this->colsDef[$key]["date"] ? $this->colsDef[$key]["date"] : "d/m/Y";
//                            $this->cols[$key]["date"] = $format;
//                        }
//                        if (isset($this->colsDef[$key]["sum"])) {
//                            $this->totals[$key] = 0;
//                        }
//                    }
//                }
//            }
//        }
        $this->initPdf();
        //
        $this->renderData($this->data, null, null, $this->colsDef);
        //
        $this->pdf->Output();
    }



    private function renderData($data, $y = null, $x = 0, $colsDef = array()) {
        $y = $y ? $y : $this->pdf->Y;
        $left = $this->marginX + $x;


        $height = 0;
        foreach ($data as $row) {
            $x = $left;
            $height = 0;
            foreach ($row as $key => $col) {
                if (!is_array($col)) {
                    $col = trim($col);
                    $h = $this->pdf->getStringHeight($this->pdf->getStringWidth($col) + 2, $col, false, false, '', 0);
                    $height = ($height > $h) ? $height : $h;
                }
            }
            if ($this->chkPageEnd($height)) {
                $y = $this->pdf->Y;
                $x = $left;
            }
            $w = 100;



            $this->pdf->textBox(array(
                                "txt" => date( "d/m/Y", mystrtotime($row['fecha'])),
                                "y" => $y,
                                "x" => $x,
                                "w" => $w,
                                "h" => $height,
                                "align" => "L",
                        ));
            $fecha = ($row['tipo']==DocValor::TIPO_CHEQUE_PROPIO)?$row['fecha']:$row['fecha_creacion'];
            $this->pdf->textBox(array(
                                "txt" => date( "d/m/Y", mystrtotime($fecha)),
                                "y" => $y,
                                "x" => $x+ 23,
                                "w" => $w ,
                                "h" => $height,
                                "align" => "L",
                        ));
            $this->pdf->textBox(array(
                                "txt" => $row['numero'],
                                "y" => $y,
                                "x" => $x + 45,
                                "w" => $w,
                                "h" => $height,
                                "align" => "L",
                        ));
            $det = ($row['detalle']==$row['obs'])?trim($row['detalle']):trim($row['detalle']).' '.trim($row['obs']);
            //.' ('.$row['abreviacion'].')'
            //.' '.$row['nom_concepto']
            if (strlen($det) > 35){
              $det = substr($det, 0, 28).'[..]';
            }

            if (($row['tipo']== DocValor::TIPO_RECAUDACION_BANCARIA))
              $det1 = ' (Rec.Banc.)';
            else
             if (($row['tipo']== DocValor::TIPO_TRANSFERENCIA ))
                 $det1 =  ' (Trans.Banc.)';
                 else
                      $det1 = '';

            $this->pdf->textBox(array(
                                "txt" => ucwords(strtolower($det)). $det1,
                                "y" => $y,
                                "x" => $x + 55,
                                "w" => $w ,
                                "h" => $height,
                                "align" => "L",
                        ));

           $det = ($row['importe']>0)?110:135;
           $this->pdf->textBox(array(
                                "txt" => number_format(abs($row['importe']),2),
                                "y" => $y,
                                "x" => $x + $det,
                                "w" => 30,
                                "h" => $height,
                                "align" => "R",
                        ));
           $this->saldo_ant += $row['importe'];
          $this->pdf->textBox(array(
                                "txt" => number_format(($this->saldo_ant),2),
                                "y" => $y,
                                "x" => $x + 160,
                                "w" => 30,
                                "h" => $height,
                                "align" => "R",
                        ));
           $y+=$height;



        }


        //Imprime el pie


        $y+=$height;
        return $y;
    }

    private function chkPageEnd($height) {
        if (($this->pdf->getY() + $height + 15) >= $this->pdf->getPageHeight()) {
            $this->pdf->AddPage();
            return true;
        } else {
            return false;
        }
    }

}
