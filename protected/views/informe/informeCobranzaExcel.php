<?php
    $lc = $liquid_conf_id = LiquidConf::getActive();
    $liquid_conf_id = $lc->id;
    ini_set('memory_limit', '-1');
    $direccion = "concat (COALESCE(p1.calle, ''), ' ', COALESCE(p1.numero, ''), ' ', COALESCE(p1.piso, ''), ' ', COALESCE(p1.departamento, '')) as domicilio";
    $comprobFactura = Comprob::FACTURA_VENTAS;
    $where = " where a.activo = 1 and ad.activo ";
    $where = " where  true";
    $where .= ($nivel_id == "-1") ? "" : " and n.id = $nivel_id ";
    $sql = "
      select a.Familia_id, concat(a.apellido,' ', a.nombre) as Nombre,
        a.matricula, n.nombre as nivel, anio.nombre as curso, d.nombre as division,
        a.activo, a.beca, f.obs as obs_cobranza, saldo(s.id, null) as saldo,concat(p1.apellido,' ' ,p1.nombre) as vive_con,
        p1.telefono_casa as p1tc, p1.telefono_trabajo as p1tt, p1.telefono_celular as p1tce, 
        concat(p2.apellido,' ' ,p2.nombre) as nombre2, 
        p2.telefono_casa as p2tc, p2.telefono_trabajo as p2tt, p2.telefono_celular as p2tce,
        p3.nombre as nombre3, pt.nombre as pariente1, pt1.nombre as pariente2,  $direccion, 
        deb.cbu, deb.activo as debito_activo, a.visa_numero, a.visa_activo,
        (select max(d2.id)
            from doc d2 
                inner join doc_liquid dl2 on dl2.id = d2.doc_liquid_id 
                inner join talonario t on t.id = d2.talonario_id and t.comprob_id = $comprobFactura
            where d2.socio_id = s.id and dl2.liquid_conf_id = $liquid_conf_id
        ) as doc_id, visa_titular, ae.nombre as estado_alumno
      from alumno a
        inner join familia f on f.id = a.familia_id
        inner join alumno_estado ae on ae.id = a.estado_id
        left  join alumno_debito deb on deb.alumno_id = a.id
        inner join socio s on s.Alumno_id = a.id
        left join alumno_division ad on ad.Alumno_id = a.id and ad.activo
        left join alumno_division_estado ade on ade.id = ad.alumno_division_estado_id and ade.muestra_admin
        left join division d on d.id = ad.Division_id
        left join anio on anio.id = d.Anio_id
        left join articulo ar on ar.id = anio.articulo_id
        left join nivel n on n.id = anio.Nivel_id
        left join pariente p1 on p1.id = a.vive_con_id
        left join pariente p2 on p2.id = (select id from pariente p2 where p2.Familia_id = a.Familia_id and p2.id <> a.vive_con_id limit 1 )
        left join pariente p3 on p3.id = (select id from pariente p3 where p3.Familia_id = a.Familia_id and p3.id <> a.vive_con_id limit 1,1 )
        left join pariente_tipo pt on pt.id = p1.Pariente_Tipo_Id 
        left join pariente_tipo pt1 on pt1.id = p2.Pariente_Tipo_Id 
      $where
      order by a.apellido, a.nombre
";
    //vd($sql);
//$sql = "
//            select a.Familia_id, concat(a.apellido,' ', a.nombre) as Nombre,
//                a.matricula, n.nombre as nivel, anio.nombre as curso, d.nombre as division,
//                a.activo, a.beca, a.obs_cobranza, a.obs, saldo(s.id, null) as saldo,concat(p1.apellido,' ' ,p1.nombre) as vive_con,
//                p1.telefono_casa as p1tc, p1.telefono_trabajo as p1tt, p1.telefono_celular as p1tce,
//                concat(p2.apellido,' ' ,p2.nombre) as nombre2,
//                p2.telefono_casa as p2tc, p2.telefono_trabajo as p2tt, p2.telefono_celular as p2tce,
//                p3.nombre as nombre3, pt.nombre as pariente1, pt1.nombre as pariente2,  $direccion,
//                deb.cbu, deb.activo as debito_activo, a.visa_numero, doc.id as doc_id, a.visa_activo
//            from alumno a
//                left  join alumno_debito deb on deb.alumno_id = a.id
//                inner join socio s on s.Alumno_id = a.id
//                left join doc on doc.socio_id = s.id
//                left join talonario t on t.id = doc.talonario_id and t.comprob_id = $comprobFactura
//                left join doc_liquid dl on doc.doc_liquid_id = dl.id and dl.liquid_conf_id = (select max(id) from liquid_conf lc)
//                left join alumno_division ad on ad.Alumno_id = a.id and ad.activo = 1
//                left join division d on d.id = ad.Division_id
//                left join anio on anio.id = d.Anio_id
//                left join articulo ar on ar.id = anio.articulo_id
//                left join nivel n on n.id = anio.Nivel_id
//                left join pariente p1 on p1.id = a.vive_con_id
//                left join pariente p2 on p2.id = (select id from pariente p2 where p2.Familia_id = a.Familia_id and p2.id <> a.vive_con_id limit 1 )
//                left join pariente p3 on p3.id = (select id from pariente p3 where p3.Familia_id = a.Familia_id and p3.id <> a.vive_con_id limit 1,2 )
//                left join pariente_tipo pt on pt.id = p1.Pariente_Tipo_Id
//                left join pariente_tipo pt1 on pt1.id = p2.Pariente_Tipo_Id
//                /*left join liquid_conf lc on lc.id = dl.liquid_conf_id and lc.id = (select max(id) from liquid_conf)*/
//            where $where
//            order by a.apellido, a.nombre
//";
//vd($sql);
    $rows = Helpers::qryAll($sql);
//vd($rows);
    $objPHPExcel = new MyPHPExcel();

    $as = $objPHPExcel->getActiveSheet();
    $as = $objPHPExcel->setActiveSheetIndex(0);
    $as->SetCellValue('A1', "Familia ID");
    $as->SetCellValue('B1', "Matricula");
    $as->SetCellValue('C1', "Nombre");
    $as->SetCellValue('D1', "Nivel");
    $as->SetCellValue('E1', "Curso");
    $as->SetCellValue('F1', "Division");
    $as->SetCellValue('G1', "Domicilio");
    $as->SetCellValue('H1', "Activo");
    $as->SetCellValue('I1', "Beca");
    $as->SetCellValue('J1', "Saldo");
    $as->SetCellValue('K1', "Obs. Cobranza");
    $as->SetCellValue('L1', "Vive Con");
    $as->SetCellValue('M1', "Tel. Fijo");
    $as->SetCellValue('N1', "Tel. Trabajo");
    $as->SetCellValue('O1', "Tel. Cel.");
    $as->SetCellValue('P1', "Nombre");
    $as->SetCellValue('Q1', "Tel. Fijo");
    $as->SetCellValue('R1', "Tel. Trabajo");
    $as->SetCellValue('S1', "Tel. Cel.");
    $as->SetCellValue('T1', "CBU");
    $as->SetCellValue('U1', "Débito activo");
    $as->SetCellValue('V1', "Número de Visa");
    $as->SetCellValue('W1', "Visa Activo");
    $as->SetCellValue('X1', "Nro. de identificador de Visa");
    $as->SetCellValue('Y1', "ID facturación");
    $as->SetCellValue('Z1', "Titular Visa");
    $as->SetCellValue('AA1', "Estado Alumno");

    $as->setAutoFilter("A1:AA1");
    $as->getStyle("A1:AA1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $as->getStyle("A1:AA1")->getFont()->setBold(true);
    $as->getStyle("A1:AA1")->getFill()->getStartColor()->setARGB("666666");
    $as->getStyle("A1:AA1")->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
    $i = 2;
foreach ($rows as $row) {
    $row["visa_numero"] = ' '.trim(str_replace(' ', '', $row["visa_numero"]));
    $as->SetCellValue("A$i", $row["Familia_id"]);
    $as->SetCellValue("B$i", $row["matricula"]);
    $as->SetCellValue("C$i", $row["Nombre"]);
    $as->SetCellValue("D$i", $row["nivel"]);
    $as->SetCellValue("E$i", $row["curso"]);
    $as->SetCellValue("F$i", $row["division"]);
    $as->SetCellValue("G$i", $row["domicilio"]);
    $as->SetCellValue("H$i", $row["activo"]);
    $as->SetCellValue("I$i", $row["beca"]);
    $as->SetCellValue("J$i", $row["saldo"]);
    $as->SetCellValue("K$i", trim($row["obs_cobranza"]));
    $as->SetCellValue("L$i", trim($row["vive_con"]) . ' (' . $row["pariente1"] . ')');
    $as->SetCellValue("M$i", $row["p1tc"]);
    $as->SetCellValue("N$i", $row["p1tt"]);
    $as->SetCellValue("O$i", $row["p1tce"]);
    $as->SetCellValue("P$i", $row["nombre2"] . ' (' . $row["pariente2"] . ')');
    $as->SetCellValue("Q$i", $row["p2tc"]);
    $as->SetCellValue("R$i", $row["p2tt"]);
    $as->SetCellValue("S$i", $row["p2tce"]);
    $as->SetCellValue("T$i", $row["cbu"]);
    $as->SetCellValue("U$i", $row["debito_activo"]);
    $as->SetCellValue("V$i", "'".trim(str_replace(' ', '', $row["visa_numero"])));
    $as->SetCellValue("W$i", $row["visa_activo"] == 1 ? $row["visa_activo"] : "");
    if ($row["visa_numero"] !== "" and $row["activo"] == 1 and $row["visa_activo"] == 1) {
        $as->SetCellValue("X$i", $nro_debito);
        $nro_debito++;
    }
    $as->SetCellValue("Y$i", $row["doc_id"]);
    $as->SetCellValue("Z$i", $row["visa_titular"]);
    $as->SetCellValue("AA$i", $row["estado_alumno"]);
    $i++;
}

//$as->getStyle("A$i:U$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
//$as->getStyle("A$i:U$i")->getFont()->setBold(true);
//$as->getStyle("A$i:U$i")->getFill()->getStartColor()->setARGB("666666");
//$as->getStyle("A$i:U$i")->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);

    $hasta = $i - 1;
    $as->setCellValue("A$i", "Totales");
    $as->setCellValue("J$i", "=SUM(J2:J$hasta)");
//$as->setCellValue("C$i", "=SUM(C2:C$hasta)");
//$num_rows = $as->getHighestRow();
//$as->insertNewRowBefore($num_rows+1, 100);


    $as->getStyle("I2:I$i")->getNumberFormat()->setFormatCode('#,##0.00');
    $as->getStyle("J2:J$i")->getNumberFormat()->setFormatCode('#,##0.00');

    $as->getColumnDimension('A')->setAutoSize(true);
    $as->getColumnDimension('B')->setAutoSize(true);
    $as->getColumnDimension('C')->setAutoSize(true);
    $as->getColumnDimension('D')->setAutoSize(true);
    $as->getColumnDimension('E')->setAutoSize(true);
    $as->getColumnDimension('F')->setAutoSize(true);
    $as->getColumnDimension('G')->setAutoSize(true);
    $as->getColumnDimension('H')->setAutoSize(true);
    $as->getColumnDimension('I')->setAutoSize(true);
    $as->getColumnDimension('J')->setAutoSize(true);
    $as->getColumnDimension('K')->setWidth(61.43);
    $as->getColumnDimension('L')->setAutoSize(true);
    $as->getColumnDimension('M')->setAutoSize(true);
    $as->getColumnDimension('N')->setAutoSize(true);
    $as->getColumnDimension('O')->setAutoSize(true);
    $as->getColumnDimension('P')->setAutoSize(true);
    $as->getColumnDimension('Q')->setAutoSize(true);
    $as->getColumnDimension('R')->setAutoSize(true);
    $as->getColumnDimension('S')->setAutoSize(true);
    $as->getColumnDimension('T')->setWidth(29.43);
    $as->getColumnDimension('V')->setAutoSize(true);
    $as->getColumnDimension('W')->setAutoSize(true);
    $as->getColumnDimension('X')->setAutoSize(true);
    $as->getColumnDimension('Z')->setAutoSize(true);
    $as->getColumnDimension('AA')->setAutoSize(true);
    /*
     *
     */
    $as->getStyle("E2:E$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $as->getStyle("M2:O$i")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
    $as->getStyle("M2:O$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $as->getStyle("Q2:S$i")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
    $as->getStyle("Q2:S$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $as->getStyle("T2:T$i")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
    $as->getStyle("T2:T$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $as->getStyle("V2:V$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $as->getStyle("V2:V$i")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
//$as->getStyle("T2:T$i")->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
    $as->getColumnDimension('U')->setWidth(12);
//$objPHPExcel->output("Cobranza_" . date("Y-m-d") );
try {
    $objPHPExcel->output("Cobranza1");
} catch (Exception $e) {
    vd2($e);
}
