<style type="text/css">
    #tablegenquickfilter-div{margin-top: 8px}
</style>
<?php
$fechaSaldo = date("Y/m/d", mystrtotime($fecha_desde) - 1);
$fechaParaMostrar = date("d/m/Y", mystrtotime($fecha_desde));
$fecha_desde = date("Y/m/d", mystrtotime($fecha_desde));
$fecha_hasta = date("Y/m/d", mystrtotime($fecha_hasta)) . " 23:59:59";
$saldoSelect = "
    select coalesce(saldo_ant($socio_id, \"$fecha_desde\"),null) as saldo";
$saldo = Helpers::qryScalar($saldoSelect);
$select = "
            select  -1 as doc_id,  \"$fechaSaldo\" as fecha, '' as comprob,  '' as numero,\"Saldo al $fechaParaMostrar\"  as detalle, -1 as id,
                          \"$saldo\" as total_doc, null as total_det,
                          0 as debe, 0 as haber, 0 as saldo, 0 as doc_det_id, \"\" as concepto
            union
            select d.id as doc_id, d.fecha_valor as fecha, c.abreviacion as comprob,  concat(LPAD(d.sucursal, 4, '0'),'-',LPAD(d.numero, 8, '0')) as numero,
                         concat(d.detalle,' ', substr(coalesce(dd.detalle,''),1,30)),d.id,
                         c.signo_cc * d.total as total_doc, dd.total as total_det, 0 as debe, 0 as haber, 0 as saldo, dd.id as doc_det_id, co.nombre as concepto
                from doc d
                    left join doc_det dd on dd.doc_id = d.id
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
                    left join socio s on s.id = d.socio_id
                    left join alumno a on a.id = s.alumno_id
                    left join concepto co on co.id = d.concepto_id
                where d.fecha_valor BETWEEN \"$fecha_desde\" and \"$fecha_hasta\"
                            and s.id = $socio_id and d.anulado = 0 and d.activo = 1
                order by 6,3
        ";
$data = Helpers::qryAll($select);
$saldo = 0;
foreach ($data as $key => $val) {
    $data[$key]["detalle"] = $data[$key]["detalle"] ? $data[$key]["detalle"] : $data[$key]["concepto"];
    $total = isset($val["total_det"]) ? $val["total_det"] : $val["total_doc"];
    $data[$key]["debe"] = abs($total >= 0) ? abs($total) : 0;
    $data[$key]["haber"] = abs($total < 0) ? abs($total) : 0;
    $saldo += $total;
    $data[$key]["saldo"] = $saldo;
}

$cols["ctacte"] = array(
        "doc_id" => array("visible" => false),
        "total_doc" => array("visible" => false),
        "concepto" => array("visible" => false),
        "total_det" => array("visible" => false),
        "doc_det_id" => array("visible" => false),
        "id" => array("visible" => false),
        "comprob" => array("title" => "Compr.", "width" => 60),
        "sucursal" => array("visible" => false),
        "numero" => array("title" => "Número", "width" => 80),
        "fecha" => array("title" => "Fecha", "date" => "d/m/Y", "width" => 80),
        "detalle" => array("style" => "width:390"),
        "debe" => array("currency" => true, "style" => "text-align:right;width:80"),
        "haber" => array("currency" => true, "style" => "text-align:right;width:80"),
        "saldo" => array("currency" => true, "sum" => true, "style" => "text-align:right;width:80"),
);
$div = Helpers::getTable("ctacte", $data, $cols, array("title" => false, "filter" => true, "border" => true));
echo $div;
?>

<!---->
<!--3c64a4eba5c3415bd947f4a02630a285-->