<?php

    include("informePDF.php");
    $fechaSaldo = date("Y/m/d", mystrtotime($fecha_desde) - 1);
    $fechaParaMostrar = date("d/m/Y", mystrtotime($fecha_desde));
    $fecha_desde = date("Y/m/d", mystrtotime($fecha_desde));
    $fecha_hasta = date("Y/m/d", mystrtotime($fecha_hasta)) . " 23:59:59";
    $saldoSelect = "
        select coalesce(saldo_ant($socio_id, \"$fecha_desde\"),null) as saldo";
    $saldo = Helpers::qryScalar($saldoSelect);
    $select = "
    select  -1 as doc_id,  \"$fechaSaldo\" as fecha, '' as comprob,  '' as numero,\"Saldo al $fechaParaMostrar\"  as detalle, -1 as id,
    \"$saldo\" as total_doc, null as total_det,
    0 as debe, 0 as haber, 0 as saldo, 0 as doc_det_id, \"\" as concepto
    union
    select d.id as doc_id, d.fecha_valor as fecha, c.abreviacion as comprob,  concat(LPAD(d.sucursal, 4, '0'),'-',LPAD(d.numero, 8, '0')) as numero,
    concat(d.detalle,' ', substr(coalesce(dd.detalle,''),1,30)),d.id,
    c.signo_cc * d.total as total_doc, dd.total as total_det, 0 as debe, 0 as haber, 0 as saldo, dd.id as doc_det_id, co.nombre as concepto
    from doc d
    left join doc_det dd on dd.doc_id = d.id
    inner join talonario t on t.id = d.talonario_id
    inner join comprob c on c.id = t.comprob_id
    left join socio s on s.id = d.socio_id
    left join alumno a on a.id = s.alumno_id
    left join concepto co on co.id = d.concepto_id
    where d.fecha_valor BETWEEN \"$fecha_desde\" and \"$fecha_hasta\"
    and s.id = $socio_id and d.anulado = 0 and d.activo = 1
    order by 6,3
    ";
    // ve2($saldoSelect);
    // vd2($select);
    $data             = Helpers::qryAll($select);
    foreach ($data as $key => $val) {
        $data[$key]["detalle"] = $data[$key]["detalle"] ? $data[$key]["detalle"] : $data[$key]["concepto"];
        $total                 = isset($val["total_det"]) ? $val["total_det"] : $val["total_doc"];
        $data[$key]["debe"]    = abs($total >= 0) ? abs($total) : 0;
        $data[$key]["haber"]   = abs($total < 0) ? abs($total) : 0;
    }

    $alumno  = Helpers::qry("
    select concat(a.apellido,', ' , a.nombre) as nombre, a.beca, concat(p.apellido, ', ', p.nombre) as tutor,
           concat(coalesce(p.calle, \"\"),\" \", coalesce(p.numero,\"\"), \" \", coalesce(p.piso,\"\")) as domicilio,
           p.telefono_casa as telefono_casa, p.telefono_trabajo as telefono_trabajo, p.telefono_celular as telefono_celular,
           n.nombre as nivel, an.nombre as anio, d.nombre as division, a.matricula, f.obs
        from alumno a
            inner join socio s on s.Alumno_id = a.id
		inner join familia f on f.id = a.familia_id
            left join pariente p on p.id = a.vive_con_id
            left join alumno_division ad on ad.Alumno_id = a.id and ad.activo
            left join division d on d.id = ad.division_id
            left join anio an on an.id = d.anio_id
            left join nivel n on n.id = an.nivel_id
        where s.id = $socio_id
");
    $colsDef = array(
        "doc_id"     => array("visible" => false),
        "total_doc"  => array("visible" => false),
        "concepto"   => array("visible" => false),
        "total_det"  => array("visible" => false),
        "doc_det_id" => array("visible" => false),
        "id"         => array("visible" => false),
        "comprob"    => array("label" => "Compr."),
        "sucursal"   => array("visible" => false),
        "numero"     => array("visible" => false),
        "fecha"      => array("date" => "d/m/Y",),
        "detalle"    => array("width" => 61),
        "debe"       => array("currency" => true),
        "haber"      => array("currency" => true),
        "saldo"      => array("currency" => true, "sum" => true),
    );

    $colsDefDetail = array(
        "doc_id"  => array("visible" => false),
        "detalle" => array("width" => 80.2),
        "total"   => array(
            "debeHaber" => "dh", "align" => "R", "sum" => true, "widthh" => 8
        ),
    );

    $options = array(
        "data"      => $data,
        "marginY"   => 10,
        "marginX"   => 20,
        "title"     => "Cta. Cte. de " . $alumno["nombre"] . " (" . $alumno["matricula"] . ")",
        "title2"    => "entre el " . date("d/m/Y", mystrtotime($fecha_desde)) . " y el " . date("d/m/Y", mystrtotime($fecha_hasta)),
        "dataExtra" => $alumno,
    );

    class MiInforme extends Informe
    {

        private static $saldo = 0;

        public function customHeader($pdf, $x, $y, $args) {
            /* @var $pdf PDF */
            $top        = $y;
            $dataAlumno = $args["dataExtra"];

            $txt = $dataAlumno["nivel"] . " " . $dataAlumno["anio"] . " " . $dataAlumno["division"];
            if($dataAlumno["beca"] > 0) {
                $txt .= " - Beca:" . $dataAlumno["beca"] . "%";
            }
            $h = $pdf->getStringHeight(104, $txt);
            $pdf->textBox(array(
                              "txt"   => $txt,
                              "y"     => $y, "x" => $x, "w" => 104, "h" => null,
                              "align" => "L",
                          ));
            $y += $h;

            $txt = "Domicilio:" . $dataAlumno["domicilio"];
            $pdf->textBox(array(
                              "txt"   => $txt,
                              "y"     => $y, "x" => $x, "w" => null, "h" => null,
                              "align" => "L",
                          ));
            $y += 5;
            $txt = "";
            if($dataAlumno["telefono_casa"]) {
                $txt .= "Teléfono casa:" . $dataAlumno["telefono_casa"];
            }
            if($dataAlumno["telefono_trabajo"]) {
                $txt .= " - Trabajo:" . $dataAlumno["telefono_trabajo"];
            }
            if($dataAlumno["telefono_celular"]) {
                $txt .= " - Celular:" . $dataAlumno["telefono_celular"];
            }
            $pdf->textBox(array(
                              "txt"   => $txt,
                              "y"     => $y, "x" => $x, "w" => null, "h" => null,
                              "align" => "L",
                          ));
            $y += 5;
            if($dataAlumno["obs"]) {
                $fs  = $pdf->setFont(PDF_FONT_NAME_MAIN, "", 11);
                $txt = $dataAlumno["obs"];
                //$txt = "1";
                $top += 2;
                $w = 100;
                $h = $pdf->getStringHeight($w, $txt);
                $x = $pdf->getPageWidth() - $w - 1;

                $cp = $pdf->setCellPaddings(2, 2, 1, 1);
                $pdf->textBox(array(
                                  "txt"         => $txt,
                                  "y"           => $top, "x" => $x, "w" => $w, "h" => $h,
                                  "align"       => "L", "fontsize" => 10,
                                  "border"      => array('LTRB' => array('width' => 0.1)),
                                  "autopadding" => true,
                              ));
                $pdf->SetCellPadding($cp);
                $pdf->setFillColor(255, 255, 255);
                $pdf->textBox(array(
                                  "txt" => "Observaciones de cobranza:", "fontsize" => 10,
                                  "y"   => $top - 2, "x" => $x + 1, "fill" => true,
                                  "w"   => 46,
                              ));

                $y = (($top + $h) > $y ? ($top + $h) + 1 : $y + $h - 3);
                $pdf->setFont(PDF_FONT_NAME_MAIN, "", $fs);
            }

            return parent::customHeader($pdf, $x, $y + 1, $args);
        }

        public function getField($fieldName, $text, $record, $esTotal = false) {
            if($fieldName == "saldo") {
                return self::$saldo += ($record["debe"] - $record["haber"]);
            } else {
                return parent::getField($fieldName, $text, $record, $esTotal);
            }
        }

        public function getTotal($fieldName, $text) {
            if($fieldName == "saldo") {
                return self::$saldo;
            } else {
                return parent::getTotal($fieldName, $text);
            }
        }

    }

    $i = new MiInforme($options, $colsDef, $colsDefDetail);
    $i->render();
?>