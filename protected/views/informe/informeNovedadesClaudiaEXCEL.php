<?php
	$filename = "InformeNovedades";
	$fecha_desde = Helpers::fechaParaGrabar($_GET["fecha_desde"], "Y/m/d");
	$fecha_hasta = Helpers::fechaUltimoSegundo($_GET["fecha_hasta"], "Y/m/d");

  $where = "
    where lc.confirmada = 1 and n1.activo = 1  and  a.beca = 100
          and nc.fecha_carga between \"$fecha_desde\" and \"$fecha_hasta\"";
	$where .=$_GET["usuario_id"] == "" ? " " : " and u.id = " . $_GET["usuario_id"] . " ";
	$where .=$_GET["articulo_id"] == "" ? " " : " and a1.id = " . $_GET["articulo_id"] . " ";

	$nivel_id = in_array($_GET["nivel_id"], array("-1", "")) ? "-1" : $_GET["nivel_id"];
	$anio_id = in_array($_GET["anio_id"], array("-1", "")) ? "-1" : $_GET["anio_id"];
	$division_id = in_array($_GET["division_id"], array("-1", "")) ? "-1" : $_GET["division_id"];

	if ($nivel_id !== "-1") {
		$where .= " and n1.id =  " . $nivel_id;
	}

	if ($anio_id !== "-1") {
		$where .= " and a2.id =  " . $anio_id;
	}

	if ($division_id !== "-1") {
		$where .= " and d.id =  " . $division_id;
  }
  $select = "
    SELECT  /*DATE_FORMAT(nc.fecha_carga, '%d/%m/%Y') AS fecha_carga, */lc.descripcion AS cuota, n1.nombre AS nivel, a2.nombre AS anio, 
                    DATE_FORMAT(nc.fecha, '%d/%m/%Y') AS fecha, d.nombre AS division, u.nombre AS usuario, 
    concat(a.apellido, ', ', a.nombre) as alumno, n.detalle, n.importe 
    FROM novedad n INNER JOIN articulo a1 ON n.articulo_id = a1.id 
      INNER JOIN novedad_cab nc ON n.novedad_cab_id = nc.id 
      INNER JOIN liquid_conf lc ON nc.liquid_conf_id = lc.id 
      INNER JOIN user u ON nc.user_id = u.id 
      INNER JOIN alumno a ON n.alumno_id = a.id 
      INNER JOIN alumno_division ad ON a.id = ad.Alumno_id and ad.activo 
      INNER JOIN division d ON ad.Division_id = d.id 
      INNER JOIN anio a2 ON a2.id = d.Anio_id 
      INNER JOIN nivel n1 ON n1.id = a2.Nivel_id 
    $where
    order by n1.orden, a2.orden, d.orden, u.nombre, a1.nombre, a.apellido, a.nombre";
	// vd($select);
	$rows = helpers::qryall($select);

	$cols = array(
			"nivel" => array("title" => "nivel"),
			"anio" => array("title" => "año"),
			"division" => array("title" => "division"),
			"alumno" => array("title" => "alumno"),
			"cuota" => array("title" => "cuota"),
			"usuario" => array("title" => "usuario"),
			"detalle" => array("title" => "detalle"),
			"fecha" => array("title" => "fecha", "format" => "date"),
			"importe" => array("title" => "importe", "total" => true, "currency" => true),
	);

	helpers::excel($rows, $cols, $filename, true);


	