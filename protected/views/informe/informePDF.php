<?php

class MYPDF extends PDF {

    public $informe, $Y, $imgHeader;

    public function header() {
        $fs = $this->setFont(PDF_FONT_NAME_MAIN, "", $this->informe->headerfontsize);
        $this->Image($this->informe->imgHeader, 20, 12, 14);

        $this->setY($this->informe->marginY);

        $this->setFont(PDF_FONT_NAME_MAIN, "", $this->informe->titlefontsize);
        $this->t($this->informe->args["title"], 0, null, "C", $this->getPageWidth());
        $this->setFont(PDF_FONT_NAME_MAIN, "", $this->informe->headerfontsize);

        $this->setFont(PDF_FONT_NAME_MAIN, "", 7);
        $txt = "Impreso el";
        $this->setX($this->getPageWidth() - $this->getStringWidth($txt) - 10);
        $this->setY($this->getY() + $this->t($txt));

        $fecha = date("d/m/Y", time());
        $this->setX($this->getPageWidth() - $this->getStringWidth($fecha) - 10);
        $this->t($fecha);
        $this->setFont(PDF_FONT_NAME_MAIN, "", $this->informe->headerfontsize);

        $this->setY($this->getY() + 2);
        $this->setFont(PDF_FONT_NAME_MAIN, "", $this->informe->titlefontsize);
        $this->setY($this->getY() + $this->t($this->informe->args["title2"], 0, null, "C", $this->getPageWidth()));
        $this->setFont(PDF_FONT_NAME_MAIN, "", $this->informe->headerfontsize);

        $x = $this->informe->marginX;
        $this->Y = $this->getY() + 2;
        $this->Y = $this->informe->customHeader($this, $x, $this->Y, $this->informe->args);

        $this->setFont(PDF_FONT_NAME_MAIN, "", $this->informe->fontsize);

        $this->line(0, $this->Y, $this->getPageWidth(), $this->Y);
        $this->Y += 2;

        $h = "";
        foreach ($this->informe->cols as $col) {
            $h = $this->getStringHeight($col["width"], $col["label"]);
            $this->textBox(array(
                    "txt" => trim($col["label"]),
                    "y" => $this->Y,
                    "x" => $x,
                    "w" => $col["width"],
                    "h" => $h,
                    "align" => $col["align"],
            ));
            $x += $col["width"] + $this->informe->espacioentrecolumnas;
        }
        $this->Y += $h;
        $this->line(0, $this->Y, $this->getPageWidth(), $this->Y);
        $this->Y += 1;
    }

    public function footer() {
        $txt = "Pág.:" . $this->getPage();
        $w = $this->getStringWidth($txt);
        $this->textBox(array(
                "txt" => $txt,
                "y" => $this->getPageHeight() - 7,
                "x" => $this->getPageWidth() - $w,
                "w" => $w,
        ));
    }

    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
    }

}

class Informe extends stdClass {

    public $args, $pdf, $data, $marginX, $marginY, $cols, $colsDef, $colsDefDetail, $totals = array(),
            $fontsize, $headerfontsize, $espacioentrecolumnas;

    public function __construct($options, $colsDef = array(), $colsDefDetail = array()) {
        if (!isset($options["data"])) {
            throw new Exception("Falta mandar el parámetro con los datos al informe genérico");
        }
        $this->imgHeader = "http://" . $_SERVER["SERVER_NAME"] . '/' . Yii::app()->baseUrl . "/images/logo.jpg";
        $this->data = $options["data"];
        $this->colsDef = $colsDef;
        $this->colsDefDetail = $colsDefDetail;
        $defaults = array(
                "paperSize" => "A4",
                "orientation" => "P",
                "marginX" => 0,
                "marginY" => 0,
                "headerfontsize" => 11,
                "titlefontsize" => 12,
                "fontsize" => 10,
                "showHeader" => true,
                "showFooter" => true,
                "title" => "",
                "title2" => "",
                "espacioentrecolumnas" => 1,
                "dataExtra" => array(),
        );
        $this->args = array_merge($defaults, $options);
        $this->marginY +=$this->args["marginY"];
        $this->marginX = $this->args["marginX"];
    }

    private function initPdf() {
        $this->pdf->planilla = $this;
        $this->pdf->setCreator(PDF_CREATOR);
        $this->pdf->setAuthor('JP');
        $this->pdf->setTitle($this->args["title"]);
        $this->pdf->setSubject($this->args["title"]);
        $this->pdf->setKeywords('TCPDF, PDF, Planilla');
        $this->pdf->setPrintHeader(true);
        $this->pdf->setPrintFooter(true);
        $this->pdf->setAutoPageBreak(false);
        $this->pdf->setMargins($this->marginX, $this->marginY);
        $this->fontsize = $this->args["fontsize"];
        $this->headerfontsize = $this->args["headerfontsize"];
        $this->titlefontsize = $this->args["titlefontsize"];
        $this->espacioentrecolumnas = $this->args["espacioentrecolumnas"];
        $this->pdf->setFont(PDF_FONT_NAME_MAIN, "", $this->fontsize);
        $this->pdf->setCellPaddings(0, 0, 0, 0);
    }

    private function initColumn($key, $col, &$cols, &$colsDef) {
        $align = "L";
        $sumaAncho = 1;
        if (isset($colsDef[$key])) {
            if (isset($colsDef[$key]["visible"]) and !$colsDef[$key]["visible"]) {
                return $col;
            }
            if (isset($colsDef[$key]["date"])) {
                $format = $colsDef[$key]["date"] ? $colsDef[$key]["date"] : "d/m/Y";
                $col = date($format, mystrtotime($col));
                $sumaAncho = 1;
            }
            if (isset($colsDef[$key]["lower"])) {
                $col = strtolower($col);
            }
            if (isset($colsDef[$key]["mb_convert_case"])) {
                $col = mb_convert_case($col,$colsDef[$key]["mb_convert_case"],"utf-8");
            }
            if (isset($colsDef[$key]["currency"])) {
                if (is_numeric($col)) {
                    $col = number_format($col, 2);
                }
                $align = "R";
            }
            if (isset($colsDef[$key]["align"])) {
                $align = $colsDef[$key]["align"];
            }
        }
        $cols[$key]["align"] = $align;
        $cols[$key]["label"] = mb_convert_case($key, MB_CASE_TITLE, "UTF-8");

        if (isset($colsDef[$key]["width"])) {
            $cols[$key]["width"] = $colsDef[$key]["width"];
        } else {
            $cols[$key]["width"] =
                    (isset($cols[$key]["width"]) and ($cols[$key]["width"] > $this->pdf->getStringWidth($col))) ?
                    $cols[$key] ["width"] :
                    $this->pdf->getStringWidth($col) + $sumaAncho;
            $cols[$key]["width"] =
                    ($cols[$key]["width"] > $this->pdf->getStringWidth($key)) ?
                    $cols[$key]["width"] :
                    $this->pdf->getStringWidth($key)  + $sumaAncho;
        }

        if (isset($colsDef[$key])) {
            if (isset($colsDef[$key]["label"])) {
                $cols[$key]["label"] = $colsDef[$key]["label"];
            }
        }
        return $col;
    }

    public function render() {
        $this->pdf = new MYPDF($this->args["orientation"], "mm", $this->args["paperSize"]);
        $this->pdf->informe = $this;
        $this->initPdf();
        $this->cols = array();
        foreach ($this->data as $dataKey => $row) {
            foreach ($row as $key => $col) {
                if (!is_array($col)) {
                    $col = trim($this->getField($key, $col, $row));
                    if (isset($this->colsDef[$key]["sum"])) {
                        if (isset($this->totals[$key])) {
                            $this->totals[$key] += $col;
                        } else {
                            $this->totals[$key] = 0;
                        }
                    }
                    $this->data[$dataKey][$key] = $this->initColumn($key, $col, $this->cols, $this->colsDef);
                }
            }
        }
        foreach ($this->totals as $key => $col) {
            $this->initColumn($key, $col, $this->cols, $this->colsDef);
        }


        //vd($this->totals);
        //
        $this->pdf->AddPage();
        $this->renderData($this->data, null, null, $this->cols, $this->colsDef);
        //
        $this->pdf->Output();
    }

    private function renderData($data, $y = null, $x = 0, $cols = array(), $colsDef = array()) {
        $y = $y ? $y : $this->pdf->Y;
        $left = $this->marginX + $x;
        foreach ($data as $row) {

            $x = $left;
            $height = 0;
            foreach ($row as $key => $col) {
                if (!is_array($col)) {
                    if (isset($colsDef[$key]["visible"]) and !$colsDef[$key]["visible"]) {
                        continue;
                    }
                    $row[$key] = $col;
                    $h = $col ? $this->pdf->getStringHeight($cols["$key"]["width"], $col, false, false, '', 0) : 0;
                    $height = ($height > $h) ? $height : $h;
                }
            }

            if ($this->chkPageEnd($height)) {
                $y = $this->pdf->Y;
                $x = $left;
            }



            foreach ($row as $key => $col) {
                if (!is_array($col)) {
                    if (isset($colsDef[$key])) {
                        if (isset($colsDef[$key]["visible"]) and !$colsDef[$key]["visible"]) {
                            continue;
                        }
                    }
                    $this->printField($key, $col, $row, $y, $x, $cols[$key]["width"], $height, isset($cols[$key]) ? $cols[$key]["align"] : "L", "");
                    $x+=($cols[$key]["width"] + $this->espacioentrecolumnas);
                }
            }
            $y+=$height;
            if (isset($row["detail"]) and count($row["detail"]) > 0) {
                $detCols = array();
                foreach ($row["detail"] as $rowDetailKey => $rowDetail) {
                    foreach ($rowDetail as $detailkey => $value) {
                        $row["detail"][$rowDetailKey][$detailkey] = $this->initColumn($detailkey, $value, $detCols, $this->colsDefDetail);
                    }
                }
                //vd($detCols);
                $y = $this->renderData($row["detail"], $y, 5, $detCols, $this->colsDefDetail, true);
                $y += 1;
            }
        }
        if (count($data) > 0) {
            $x = $left;
            foreach ($data[0] as $key => $col) {
                if (!is_array($col)) {
                    if (isset($colsDef[$key]["visible"]) and !$colsDef[$key]["visible"]) {
                        continue;
                    }
                    if (isset($cols[$key]["width"])) {
                        $w = $cols[$key]["width"];
                    } else {
                        $w = $this->pdf->getStringWidth($col);
                    }
                    if (isset($this->totals[$key])) {
                        //$col = number_format($col,2);
                        $col = number_format($this->getTotal($key, $this->totals[$key]), 2);
                    } else {
                        $col = "";
                    }
                    $this->pdf->textBox(array(
                            "txt" => $col,
                            "y" => $y,
                            "x" => $x,
                            "w" => $w,
                            "h" => $height,
                            "align" => isset($cols[$key]) ? $cols[$key]["align"] : "L",
                    ));
                    $x+=($w + $this->espacioentrecolumnas);
                }
            }
        }
        return $y;
    }

    private function chkPageEnd($height) {
        if (($this->pdf->getY() + $height + 15) >= $this->pdf->getPageHeight()) {
            $this->pdf->AddPage();
            return true;
        } else {
            return false;
        }
    }

    public function printField($fieldName, $col, $row, $y, $x, $w, $height, $align, $border = "") {
        $this->pdf->textBox(array(
                "txt" => str_replace("??D", "éd", $col),
                "y" => $y,
                "x" => $x,
                "w" => $w,
                "h" => $height,
                "align" => $align,
                "border" => $border,
        ));
    }

    /**
     * Esta función es llamada cada vez que el contenido de un campo se necesita.
     * 
     * @param $fieldName (string) FieldName 
     * @param $text (string)  String to print
     * @param $record (array) Array of fields
     * @param $printing (bool) Cuando el contenido del campo se necesita para imprimir este valor es verdadero
     * @public
     */
    public function getField($fieldName, $text, $record, $esTotal = false) {
        return $esTotal ? $this->totals[$fieldName] : $text;
    }

    public function beginPrint($pdf) {
        
    }

    public function customHeader($pdf, $x, $y, $args) {
        return $y;
    }

    public function getTotal($key, $col) {
        return $col;
    }

}