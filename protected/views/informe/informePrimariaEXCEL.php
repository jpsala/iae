<?php

// $nivel_id, $anio_id, $anio_id
$fileName = "InformeAlumnos.xls";
$ciclo_id = Ciclo::getCicloIdParaInformesEducativo();
$where = " a.activo = 1  ";
$where .= $nivel_id ? " and n.id = $nivel_id" : "";
$where .= $anio_id != -1 ? " and anio.id = $anio_id" : "";
$where .= $division_id != -1 ? " and d.id = $division_id" : "";
$select = "
    select n.nombre as nivel, anio.nombre as anio, d.nombre as division,
                concat(a.apellido, ' ', a.nombre) as nombre, a.matricula
	from alumno a
		inner join alumno_division ad on ad.Alumno_id = a.id and ad.ciclo_id = $ciclo_id
		inner join division d on d.id = ad.Division_id
		inner join anio on anio.id = d.Anio_id
		inner join nivel n on n.id = anio.Nivel_id
        where $where
        order by n.orden, anio.orden, d.nombre, a.apellido, a.nombre
";

$rows = Helpers::qryAll($select);

$cols = array(
        "nivel" => array("title" => "Nivel"),
        "anio" => array("title" => "Año"),
        "division" => array("title" => "Division"),
        "nombre" => array("title" => "Nombre"),
        "matricula" => array("title" => "Matricula"),
);

$excel = new Excel($rows, $cols, $fileName);
$excel->run();
$excel->output();


//Helpers::excel($rows, $cols, $fileName);


