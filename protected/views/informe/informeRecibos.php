<?php

class MYPDF extends PDF {

    public $informe, $Y, $imgHeader;

    public function header() {

        $this->Image($this->informe->imgHeader, 2, 2, 14);

        $this->setY($this->informe->marginY);

        $this->t($this->informe->args["title"], 0, null, "C", $this->getPageWidth());

        $txt = "Impreso el";
        $this->setX($this->getPageWidth() - $this->getStringWidth($txt) - 3);
        $this->setY($this->getY() + $this->t($txt));

        $fecha = date("d/m/Y", time());
        $this->setX($this->getPageWidth() - $this->getStringWidth($fecha) - 3);
        $this->t($fecha);


        $this->setY($this->getY() + $this->t($this->informe->args["title2"], 0, null, "C", $this->getPageWidth()));

        $this->Y = $this->getY() + 2;
        $this->line(0, $this->Y, $this->getPageWidth(), $this->Y);
        $this->Y = $this->getY() + 2;

        $x = $this->informe->marginX;
        $h = "";
        foreach ($this->informe->cols as $col) {
            $h = $this->getStringHeight($this->getStringWidth($col["label"]), $col["label"]);
            if ($col["debeHaber"]) {
                $this->textBox(array(
                        "txt" => "Debe",
                        "y" => $this->Y,
                        "x" => $x,
                        "w" => $col["width"],
                        "h" => $h,
                        "align" => $col["align"],
                ));
                $x += $col["width"];
                $this->textBox(array(
                        "txt" => "Haber",
                        "y" => $this->Y,
                        "x" => $x,
                        "w" => $col["width"],
                        "h" => $h,
                        "align" => $col["align"],
                ));
                $x += $col["width"];
                $this->textBox(array(
                        "txt" => "Saldo",
                        "y" => $this->Y,
                        "x" => $x,
                        "w" => $col["width"],
                        "h" => $h,
                        "align" => $col["align"],
                ));
            } else {
                $this->textBox(array(
                        "txt" => trim($col["label"]),
                        "y" => $this->Y,
                        "x" => $x,
                        "w" => $col["width"],
                        "h" => $h,
                        "align" => $col["align"],
                ));
                $x += $col["width"];
            }
        }
        $this->Y += $h;
        $this->line(0, $this->Y, $this->getPageWidth(), $this->Y);
        $this->Y += 1;
    }

    public function footer() {
        $txt = "Pág.:" . $this->getPage();
        $w = $this->getStringWidth($txt) + 2;
        $this->textBox(array(
                "txt" => $txt,
                "y" => $this->getPageHeight() - 7,
                "x" => $this->getPageWidth() - $w,
                "w" => $w,
        ));
    }

    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
    }

}

class Informe extends stdClass {

    public $pdf, $data, $marginX, $marginY, $cols, $colsDef, $colsDefDetail, $totals = array();

    public function __construct($options, $colsDef = array(), $colsDefDetail = array()) {
        if (!isset($options["data"])) {
            throw new Exception("Falta mandar el parámetro con los datos al informe genérico");
        }
        $this->imgHeader = "http://" . $_SERVER["SERVER_NAME"] . '/' . Yii::app()->baseUrl . "/images/logo.jpg";
        $this->data = $options["data"];
        $this->colsDef = $colsDef;
        $this->colsDefDetail = $colsDefDetail;
        $defaults = array(
                "paperSize" => "A4",
                "orientation" => "P",
                "marginX" => 0,
                "marginY" => 0,
                "fontSize" => 7,
                "showHeader" => true,
                "showFooter" => true,
                "title" => "",
                "title2" => "",
        );
        $this->args = array_merge($defaults, $options);
        $this->marginY +=$this->args["marginY"];
        $this->marginX = $this->args["marginX"];
    }

    private function initPdf() {
        $this->pdf->planilla = $this;
        $this->pdf->SetCreator(PDF_CREATOR);
        $this->pdf->SetAuthor('JP');
        $this->pdf->SetTitle('Planilla Resumen Caja');
        $this->pdf->SetSubject('Planilla Resumen Caja');
        $this->pdf->SetKeywords('TCPDF, PDF, Planilla');
        $this->pdf->SetPrintHeader(true);
        $this->pdf->SetPrintFooter(true);
        $this->pdf->SetAutoPageBreak(false);
        $this->margenX = $this->args["marginX"];
        $this->margenY = $this->args["marginY"];
        $this->fontSize = $this->args["fontSize"];
        $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", 11);
        $this->pdf->setCellPaddings(0, 0, 0, 0);
        $this->pdf->AddPage();
    }

    public function render() {
        $this->pdf = new MYPDF($this->args["orientation"], "mm", $this->args["paperSize"]);
        $this->pdf->informe = $this;
        $this->cols = array();
        foreach ($this->data as $row) {
            foreach ($row as $key => $col) {
                if (!is_array($col)) {
                    $debeHaber = false;
                    $align = "L";
                    if (isset($this->colsDef[$key])) {
                        if (isset($this->colsDef[$key]["visible"]) and !$this->colsDef[$key]["visible"]) {
                            continue;
                        }
                        if (isset($this->colsDef[$key]["date"])) {
                            $format = $this->colsDef[$key]["date"] ? $this->colsDef[$key]["date"] : "d/m/Y";
                            $col = date($format, mystrtotime($col));
                        }
                        if (isset($this->colsDef[$key]["width"])) {
                            //$col = date("d/m/Y", mystrtotime($col));
                        }
                        if (isset($this->colsDef[$key]["debeHaber"])) {
                            $debeHaber = true;
                            $col = number_format($col, 2);
                        } elseif (isset($this->colsDef[$key]["currency"])) {
                            $col = number_format($col, 2);
                        }
                        if (isset($this->colsDef[$key]["align"])) {
                            $align = "R";
                        }
                    }

                    if (isset($this->colsDef[$key]["width"])) {
                        $this->cols[$key]["width"] = $this->colsDef[$key]["width"];
                    } else {
                        $this->cols[$key]["width"] =
                                (isset($this->cols[$key]) and ($this->cols[$key]["width"] > $this->pdf->getStringWidth($col))) ?
                                $this->cols[$key] ["width"] :
                                $this->pdf->getStringWidth($col) + 2;
                        $this->cols[$key]["width"] =
                                ($this->cols[$key]["width"] > $this->pdf->getStringWidth($key)) ?
                                $this->cols[$key]["width"] :
                                $this->pdf->getStringWidth($key) + 2;
                    }
                    $this->cols[$key]["debeHaber"] = $debeHaber ? true : false;

                    $this->cols[$key]["align"] = $align;
                    $this->cols[$key]["label"] = mb_convert_case($key, MB_CASE_TITLE, "UTF-8");
                    if (isset($this->colsDef[$key])) {
                        if (isset($this->colsDef[$key]["label"])) {
                            $this->cols[$key]["label"] = $this->colsDef[$key]["label"];
                        }
                        if (isset($this->colsDef[$key]["fecha"]["date"])) {
                            $format = $this->colsDef[$key]["date"] ? $this->colsDef[$key]["date"] : "d/m/Y";
                            $this->cols[$key]["date"] = $format;
                        }
                        if (isset($this->colsDef[$key]["sum"])) {
                            $this->totals[$key] = 0;
                        }
                    }
                }
            }
        }
        $this->initPdf();
        //
        $this->renderData1($this->data, null, null, $this->colsDef);
        //
        $this->pdf->Output();
    }

    

    private function renderData1($data, $y = null, $x = 0, $colsDef = array()) {
        $y = $y ? $y : $this->pdf->Y;
        $left = $this->marginX + $x;
        $saldo = 0;
        $fecha_ant = date( "d/m/Y", mystrtotime($data[0]['fecha']));
        $sub_por_dia = 0;
        $cant_por_dia = 0;
        $cant_total = 0;
        $sub_total = 0;
        foreach ($data as $row) {
            //var_dump($row);
            
            $x = $left;
            $height = 0;
            foreach ($row as $key => $col) {
                if (!is_array($col)) {
                    $col = trim($col);
                    $h = $this->pdf->getStringHeight($this->pdf->getStringWidth($col) + 2, $col, false, false, '', 0);
                    $height = ($height > $h) ? $height : $h;
                }
            }
            if ($this->chkPageEnd($height)) {
                $y = $this->pdf->Y;
                $x = $left;
            }
            $w = 100;
            $fecha_actual = date( "d/m/Y", mystrtotime($row['fecha']));
            $sub_total += $row['total'];
            $cant_total += 1;
            if ($fecha_ant == $fecha_actual) {
                  $sub_por_dia += $row['total'];
                  $cant_por_dia += 1;      
               
            } else{
                $sub_por_dia = number_format($sub_por_dia, 2);
                $this->pdf->textBox(array(
                                "txt" =>"Cantidad: $cant_por_dia Subtotal: $sub_por_dia",
                                "y" => $y,
                                "x" =>  95,
                                "w" => $w,
                                "h" => $height,
                                "align" => "R",
                        ));
                $fecha_ant = $fecha_actual;
                $sub_por_dia = $row['total']; 
                $cant_por_dia =1;
                $y+=$height;
            }
                
            
            $this->pdf->textBox(array(
                                "txt" => date( "d/m/Y", mystrtotime($row['fecha'])),
                                "y" => $y,
                                "x" => $x,
                                "w" => $w,
                                "h" => $height,
                                "align" => "L",
                        ));
            $this->pdf->textBox(array(
                                "txt" => $row['matricula'],
                                "y" => $y,
                                "x" => $x + 30,
                                "w" => $w ,
                                "h" => $height,
                                "align" => "L",
                        ));
            $this->pdf->textBox(array(
                                "txt" => $row['Nombre'],
                                "y" => $y,
                                "x" => $x + 45,
                                "w" => $w,
                                "h" => $height,
                                "align" => "L",
                        ));
            $this->pdf->textBox(array(
                                "txt" => $row['tipo'],
                                "y" => $y,
                                "x" => $x + 115,
                                "w" => $w,
                                "h" => $height,
                                "align" => "L",
                        ));
           $this->pdf->textBox(array(
                                "txt" => $row['Número'],
                                "y" => $y,
                                "x" => $x + 125,
                                "w" => $w ,
                                "h" => $height,
                                "align" => "L",
                        )); 
           
           $this->pdf->textBox(array(
                                "txt" => $row['total'],
                                "y" => $y,
                                "x" => $x + 85,
                                "w" => $w,
                                "h" => $height,
                                "align" => "R",
                        ));
           $y+=$height;
           
            
           
        }
        $sub_por_dia = number_format($sub_por_dia, 2);
        $this->pdf->textBox(array(
            "txt" => "Cantidad: $cant_por_dia Subtotal: $sub_por_dia",
            "y" => $y,
            "x" => 95,
            "w" => $w,
            "h" => $height,
            "align" => "R",
        ));
        $y+=$height;
        //Imprime el pie
        $sub_total = number_format($sub_total, 2);
        $this->pdf->textBox(array(
            "txt" => "Cantidad: $cant_total Total: $sub_total",
            "y" => $y,
            "x" => 95,
            "w" => $w,
            "h" => $height,
            "align" => "R",
        ));
        
        
        return $y;
    }

    private function chkPageEnd($height) {
        if (($this->pdf->getY() + $height + 15) >= $this->pdf->getPageHeight()) {
            $this->pdf->AddPage();
            return true;
        } else {
            return false;
        }
    }

}


