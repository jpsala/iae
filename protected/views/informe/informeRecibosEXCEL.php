<?php
//4 19 21 17
$fileName = "InformeRecibos.xls";
$fecha_desde = date("Y/m/d", mystrtotime($fecha_desde));
$fecha_hasta = date("Y/m/d", mystrtotime($fecha_hasta)) . " 23:59:59";
  // $talonarios = '25,41,61' ;
// vd2($talonarios);
$comprobs =
      Comprob::RECIBO_VENTAS .','. Comprob::RECIBO_ELECTRONICO
      .','. Comprob::NC_ELECTRONICO .','. Comprob::ND_ELECTRONICO
      .','. Comprob::NC_VENTAS .','. Comprob::ND_VENTAS 
      .','. Comprob::RECIBO_BANCO .','. Comprob::NC_VENTAS .','. Comprob::ND_VENTAS;
        // vd2($comprobs);
  $select = "   select  d.fecha_valor as fecha,
                        a.matricula,
                        concat(a.apellido, ' ', a.nombre) as Nombre,
                        c.nombre as tipo,
                        concat(LPAD(d.sucursal, 4, '0'),'-',LPAD(d.numero, 8, '0')) as Número,
                        (d.total * c.signo_cc) * -1 as total

                from doc d
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
                    left join socio s on s.id = d.socio_id
                    left join alumno a on a.id = s.Alumno_id
                    left JOIN pariente p ON a.encargado_pago_id = p.id
                where d.fecha_valor BETWEEN \"$fecha_desde\" and \"$fecha_hasta\" and
                      c.id in ($comprobs) and d.anulado = 0 and d.activo = 1 and !d.filtrado
                order by d.fecha_valor, d.numero, matricula";

/*
$comprob_id = '
(c.id = ' . Comprob::RECIBO_VENTAS . ' or c.id = ' . Comprob::RECIBO_ELECTRONICO
 . ' or c.id = ' . Comprob::NC_ELECTRONICO . ' or c.id = ' . Comprob::ND_ELECTRONICO
 . ' or c.id = ' . Comprob::RECIBO_BANCO . ')
or
  ((c.id = ' . Comprob::NC_VENTAS . ' or c.id = ' . Comprob::ND_VENTAS .')) 
 ';
$select = "   select    d.fecha_valor as fecha,
                        a.matricula,
                        concat(a.apellido, ' ', a.nombre)as Nombre,
                        c.abreviacion as tipo,
                        concat(LPAD(d.sucursal, 4, '0'),'-',LPAD(d.numero, 8, '0')) as Número,
                        concat(a.apellido, ' ', a.nombre)as Nombre,
                        (d.total * c.signo_cc) * -1 as total
                from doc d
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
                    left join socio s on s.id = d.socio_id
                    left join alumno a on a.id = s.Alumno_id
                    INNER JOIN pariente p ON a.encargado_pago_id = p.id
                where d.fecha_valor BETWEEN \"$fecha_desde\" and \"$fecha_hasta\"
                            and ($comprob_id) and d.anulado = 0 and d.activo = 1 and !d.filtrado
                order by d.fecha_valor, d.numero, matricula";

*/
                // vd($select);
                $rows = Helpers::qryAll($select);
                /* and ($comprob_id) and d.anulado = 0 and d.activo = 1 and !d.filtrado */


$cols = array(
	"fecha" => array("title" => "Fec.valor", "format" => "date"),
	"matricula" => array("title" => "Matricula", "format" => "text"),
	"Nombre" => array("title" => "Nombre"),
	"tipo" => array("title" => "Tipo"),
	"Número" => array("title" => "Número"),
	"total" => array("title" => "Total", "total" => true, "currency" => true),
);

Helpers::excel($rows, $cols, $fileName, true);


