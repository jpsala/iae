<?php

include("informeRecibos.php");
$fecha_desde = date("Y/m/d", mystrtotime($fecha_desde));
$fecha_hasta = date("Y/m/d", mystrtotime($fecha_hasta)) . " 23:59:59";
$comprob_id = '
(c.id = ' . Comprob::RECIBO_VENTAS . ' or c.id = ' . Comprob::RECIBO_ELECTRONICO
 . ' or c.id = ' . Comprob::NC_ELECTRONICO . ' or c.id = ' . Comprob::ND_ELECTRONICO
 . ' or c.id = ' . Comprob::RECIBO_BANCO . ')
or
  ((c.id = ' . Comprob::NC_VENTAS . ' or c.id = ' . Comprob::ND_VENTAS .')) 
 ';
$select = "   select    d.fecha_valor as fecha,
                        a.matricula,
                        concat(a.apellido, ' ', a.nombre)as Nombre,
                        c.abreviacion as tipo,
                        concat(LPAD(d.sucursal, 4, '0'),'-',LPAD(d.numero, 8, '0')) as Número,
                        concat(a.apellido, ' ', a.nombre)as Nombre,
                        (d.total * c.signo_cc) * -1 as total
                from doc d
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
                    left join socio s on s.id = d.socio_id
                    left join alumno a on a.id = s.Alumno_id
                    left JOIN pariente p ON a.encargado_pago_id = p.id
                where d.fecha_valor BETWEEN \"$fecha_desde\" and \"$fecha_hasta\"
                            and ($comprob_id) and d.anulado = 0 and d.activo = 1 and !d.filtrado
                order by d.fecha_valor, d.numero, matricula";

$data = Helpers::qryAll($select);

$colsDef = array(
        "fecha" => array("date"=>"d/m/Y", "label"=>"Fec.valor"),
        "Mat." => array("visible" => true),
        "Nombre" => array("visible" => true),
        "Tipo" => array("visible" => true),
        "Número" => array("visible" => true),
        "total" => array("width" => 30,"currency"=>true, "align" => "R", "sum"=>true),

);

$colsDefDetail = array(

);

$options = array(
        "data" => $data,
        "marginY" => 4,
        "marginX" => 10,
        "title" => "Recibos emitidos entre el " . date("d/m/Y", mystrtotime($fecha_desde) ) . " y el " . date("d/m/Y", mystrtotime($fecha_hasta)),
        "title2" => "",
);
$i = new Informe($options, $colsDef, $colsDefDetail);
$i->render();
?>
