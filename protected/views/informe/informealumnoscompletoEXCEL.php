<?php

// $nivel_id
$fileName = "AlumnosCompleto";

if ($division_id) {
    $nivel_id = Helpers::qryScalar("SELECT n.id FROM nivel n
	  INNER JOIN anio a ON n.id = a.Nivel_id
	  INNER JOIN division d ON a.id = d.Anio_id AND d.id = $division_id");
    $order = $nivel_id == 4 ?
         "a.apellido, a.nombre" :
         "a.sexo desc, a.apellido, a.nombre";
    $sexoAnd = isset($sexo) ? " and a.sexo = \"$sexo\" " : "";
    $row = Yii::app()->db->createCommand("
        select n.nombre as nivel, a.nombre as anio, d.nombre as division
          from division d
            inner join anio a on a.id = d.Anio_id
            inner join nivel n on n.id = a.Nivel_id
          where d.id = $division_id
      ")->query()->read();

    $titulo = $row["nivel"] . " " . $row["anio"] . " " . $row["division"];

    $direccion = "concat (COALESCE(p.calle, ''), ' ', COALESCE(p.numero, ''), ' ', COALESCE(p.piso, ''), ' ', COALESCE(p.departamento, '')) ";
    $ciclo_id = Ciclo::getCicloIdParaInformesEducativo();
    $rows = Yii::app()->db->createCommand("
        select 

          (
            SELECT pt.nombre FROM pariente p 
              INNER JOIN pariente_tipo pt ON p.Pariente_Tipo_id = pt.id
              INNER JOIN familia f ON p.Familia_id = f.id
              WHERE p.id = a.vive_con_id
          ) AS vive_con, 
          (
            SELECT GROUP_CONCAT(CONCAT(p1.apellido, ', ', p1.nombre)) FROM pariente p1 
              where p1.Familia_id = a.Familia_id AND p1.Pariente_Tipo_id = 1
          ) AS nombre_padre,
          (
            SELECT GROUP_CONCAT(p2.nombre) FROM pariente p1 
              INNER JOIN pais p2 ON p1.nacionalidad = p2.id
              where p1.Familia_id = a.Familia_id AND p1.Pariente_Tipo_id = 1
          ) AS nacionalidad_padre,
          (
            SELECT GROUP_CONCAT(p1.numero_documento) FROM pariente p1 
              where p1.Familia_id = a.Familia_id AND p1.Pariente_Tipo_id = 1
          ) AS documento_padre,
          (
            SELECT GROUP_CONCAT(p1.profesion) FROM pariente p1 
              where p1.Familia_id = a.Familia_id AND p1.Pariente_Tipo_id = 1
          ) AS profesion_padre,
          (
            SELECT GROUP_CONCAT(p1.telefono_celular) FROM pariente p1 
              where p1.Familia_id = a.Familia_id AND p1.Pariente_Tipo_id = 1
          ) AS telefono_padre,
          (
            SELECT GROUP_CONCAT(CONCAT(COALESCE(p1.calle,' '), ' ', COALESCE(p1.numero,' '), ' ', COALESCE(p1.piso,' '), ' ', COALESCE(p1.departamento,' '))) FROM pariente p1 
              where p1.Familia_id = a.Familia_id AND p1.Pariente_Tipo_id = 1
          ) AS domicilio_padre,
          (
          SELECT GROUP_CONCAT(CONCAT(p5.apellido, ', ', p5.nombre)) FROM pariente p5 
          where p5.Familia_id = a.Familia_id AND p5.Pariente_Tipo_id = 4
          ) AS nombre_madre,
          (
            SELECT GROUP_CONCAT(p9.nombre) FROM pariente p5 
              INNER JOIN pais p9 ON p5.nacionalidad = p9.id
              where p5.Familia_id = a.Familia_id AND p5.Pariente_Tipo_id = 4
          ) AS nacionalidad_madre,
          (
          SELECT GROUP_CONCAT(p5.numero_documento) FROM pariente p5 
          where p5.Familia_id = a.Familia_id AND p5.Pariente_Tipo_id = 4
          ) AS documento_madre,
          (
          SELECT GROUP_CONCAT(p5.profesion) FROM pariente p5 
          where p5.Familia_id = a.Familia_id AND p5.Pariente_Tipo_id = 4
          ) AS profesion_madre,
          (
          SELECT GROUP_CONCAT(p5.telefono_celular) FROM pariente p5 
          where p5.Familia_id = a.Familia_id AND p5.Pariente_Tipo_id = 4
          ) AS telefono_madre,
          (
          SELECT GROUP_CONCAT(COALESCE(p5.calle,' '), ' ',COALESCE(p5.numero,' '), ' ',COALESCE(p5.piso,' '), ' ',COALESCE(p5.departamento,' ')) FROM pariente p5 
          where p5.Familia_id = a.Familia_id AND p5.Pariente_Tipo_id = 4
          ) AS domicilio_madre,
        
              concat(a.apellido, ', ', ', ', a.nombre) as nombre_alumno, a.numero_documento as dni_alumno, 
              pa.nombre as nacionalidad_alumno, 
              a.fecha_nacimiento as fecha_nacimiento_alumno, l.nombre as localidad_alumno, pr.nombre as provincia_alumno
          from alumno a
            inner join alumno_division ad on ad.Alumno_id = a.id and ad.Ciclo_id = $ciclo_id
            inner join division d on d.id = ad.Division_id /*and ad.activo*/ and not ad.borrado /*((ad.activo or ad.promocionado or ad.egresado)*/
            inner join alumno_division_estado ade on ade.id = ad.alumno_division_estado_id and ade.muestra_edu
            left join localidad l on l.id = a.localidad_id
            left join pais pa on pa.id = l.Pais_id
            left join provincia pr on l.provincia_id = pr.id
          where a.activo = 1 and d.id = $division_id $sexoAnd
        order by $order
       ")->queryAll();
    
    foreach ($rows as $key => $row) {
        $rows[$key]["edad"] = Helpers::edad($row["fecha_nacimiento_alumno"]);
    }
}
$cols = array(
    "nombre_alumno" => array("title" => "Nombre"),
    "dni_alumno" => array("title" => "DNI"),
    "nacionalidad_alumno" => array("title" => "Nacionalidad"),
    "provincia_alumno" => array("title" => "Provincia"),
    "localidad_alumno" => array("title" => "Localidad"),
    "dni_alumno" => array("title" => "DNI"),
    "edad" => array("title" => "Edad"),
    "fecha_nacimiento_alumno" => array("format" => "date", "title" => "Fecha Nac."),
    "nombre_padre" => array("title" => "padre"),
    "nacionalidad_padre" => array("title" => "Nacionalidad padre"),
    "documento_padre" => array("title" => "Tipo Doc. padre"),
    "profesion_padre" => array("title" => "Profesión padre"),
    "domicilio_padre" => array("title" => "Domicilio padre"),
    "telefono_padre" => array("title" => "Tel. Celular padre"),
    "nombre_madre" => array("title" => "madre"),
    "nacionalidad_madre" => array("title" => "Nacionalidad madre"),
    "documento_madre" => array("title" => "Tipo Doc. madre"),
    "profesion_madre" => array("title" => "Profesión madre"),
    "domicilio_madre" => array("title" => "Domicilio madre"),
    "telefono_madre" => array("title" => "Tel. Celular madre"),
);

$excel = new Excel($rows, $cols, $fileName);
$excel->run();
$excel->output();


//Helpers::excel($rows, $cols, $fileName);


