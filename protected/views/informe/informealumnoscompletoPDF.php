<?php

class MyPDF extends PDF {

    public $fsize;
    public $orientacion;

    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $header = array(), $titulo = "", $fsize = null) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
        $this->fsize = $fsize;
        $this->orientacion = $orientation;
        $this->SetFontSize($fsize);
        $this->titulo = $titulo;
        $this->header = $header;
        $this->SetCreator(PDF_CREATOR);
        $this->SetAuthor('');
        $this->SetTitle($titulo);
        $this->SetSubject('IAE');
        $this->SetKeywords('TCPDF, PDF, xxx');
        $this->SetPrintHeader(true);
        $this->SetPrintFooter(false);

        $this->SetAutoPageBreak(true);
    }

    public function Header() {
        $this->SetY(4);
        $this->SetX(15);
        if ($this->titulo) {
            $this->setJPEGQuality(90);
            //$this->Image('images/logo.jpg'); 
            $this->Image('images/logo1BN.png');

            if ($this->orientacion == 'P') {
                $this->SetFontSize(11);
                $this->Cell(180, 24, $this->titulo, 1, 1, "C", false, "", "", "", "");
            } else {
                $this->SetFontSize(8);
                $this->Cell(268, 24, $this->titulo, 1, 1, "C", false, "", "", "", "");
            }
        }

        $this->SetX(15);
        ////$this->SetFontSize(11);

        foreach ($this->header as $col) {
            $this->cell($col[0], 0, $col[1], 0, "", "C");
        }
    }

    public function Footer() {
        $this->SetY(0);
    }

    public function CreateTextBox($textval, $x = 0, $y, $width = 0, $height = 16, $fontsize = 10, $fontstyle = '', $align = 'L') {


        $this->SetXY($x, $y); // 20 = margin left
        $this->SetFont(PDF_FONT_NAME_MAIN, $fontstyle, $fontsize);
        $this->Cell($width, $height, $textval, 0, false, $align);
    }

    public function renderRow($row) {
        $this->SetX(15);
        $nrow = 0;

        foreach ($this->header as $headerRow) {
            //MultiCell(55, 5, $txt, 0, '', 0, 1, '', '', true);  
            $this->cell($headerRow[0], 7, $row[$nrow], 1, $nrow + 1 == count($this->header) ? 2 : 0);
            $nrow++;
        }
    }

}

$ciclo_id = Ciclo::getCicloIdParaInformesEducativo();

if ($division_id) {
  $nivel_id = Helpers::qryScalar("SELECT n.id FROM nivel n
	INNER JOIN anio a ON n.id = a.Nivel_id
	INNER JOIN division d ON a.id = d.Anio_id AND d.id = $division_id");
  $order = $nivel_id == 4 ? 
	   "a.apellido, a.nombre" :
	   "a.sexo desc, a.apellido, a.nombre";
    $sexoAnd = isset($sexo) ? " and a.sexo = \"$sexo\" " : "";
    $row = Yii::app()->db->createCommand("
        select n.nombre as nivel, a.nombre as anio, d.nombre as division
          from division d
            inner join anio a on a.id = d.Anio_id
            inner join nivel n on n.id = a.Nivel_id
          where d.id = $division_id
      ")->query()->read();
    $titulo = $row["nivel"] . " " . $row["anio"] . " " . $row["division"];
    $direccion = "concat (COALESCE(p.calle, ''), ' ', COALESCE(p.numero, ''), ' ', COALESCE(p.piso, ''), ' ', COALESCE(p.departamento, '')) ";
    $rows = Yii::app()->db->createCommand("
      select concat(a.apellido, \", \", a.nombre) as nombre_alumno, 
						a.numero_documento as dni_alumno, pa.nombre as nacionalidad_alumno, 
            a.fecha_nacimiento as fecha_nacimiento_alumno, 
						concat(p.apellido, \", \", p.nombre) as nombre_tutor, 
						pa2.nombre as nacionalidad_tutor, td.nombre_corto as tipo_doc_tutor, 
						p.numero_documento as doc_tutor, p.profesion, $direccion as domicilio_tutor,
            p.telefono_casa, p.telefono_celular
        from alumno a
          inner join alumno_division ad on ad.Alumno_id = a.id
					inner join division d on d.id = ad.Division_id and ad.ciclo_id = $ciclo_id
          left join pariente p on  a.vive_con_id = p.id
          left join localidad l on l.id = a.localidad_id
          left join pais pa on pa.id = l.Pais_id
          left join pais pa2 on pa2.id = p.nacionalidad
          left join tipo_documento td on td.id = p.tipo_documento_id      
        where a.activo = 1 and d.id = $division_id $sexoAnd
        order by $order
    ")->queryAll();
    $header = array(
            array(40, "Alumno"),
            array(15, "DNI"),
            array(20, "Nacionalidad"),
            array(5, "Edad"),
            array(17, "Fecha Nac."),
            array(40, "Tutor"),
            array(20, "Nacionalidad"),
            array(21, "Doc."),
            array(20, "Profesión"),
            array(37, "Domicilio"),
            array(18, "Tel.Fijo"),
            array(20, "Tel.Celular"),
    );

    $pdf = new MyPDF("L", 'mm', 'A4', true, 'UTF-8', false, $header, $titulo, 7);
    $pdf->SetMargins(0, 40, 0, true);
    $pdf->AddPage();
    foreach ($rows as $row) {
        $pdf->renderRow(array(
                substr($row["nombre_alumno"], 0, 31),
                $row["dni_alumno"],
                $row["nacionalidad_alumno"],
                helpers::edad(date("Y-m-d", mystrtotime($row["fecha_nacimiento_alumno"]))),
                date("d/m/Y", mystrtotime($row["fecha_nacimiento_alumno"])),
                substr($row["nombre_tutor"], 0, 31),
                $row["nacionalidad_tutor"],
                $row["tipo_doc_tutor"] . " " . $row["doc_tutor"],
                ucwords(strtolower(substr($row["profesion"], 0, 15))),
                ucwords(strtolower($row["domicilio_tutor"])),
                substr($row["telefono_casa"], 0, 10),
                $row["telefono_celular"],
        ));
    }
    $pdf->Output();
}
?>
