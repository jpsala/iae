<?php


$fileName = "InformeAlumnosASR1";
$ciclo_id = Ciclo::getCicloIdParaInformesAdmin();
$where = " a.activo = 1  ";
$where .= $nivel_id != -1 ? " and n.id = $nivel_id" : "";
$where .= $anio_id != -1 ? " and anio.id = $anio_id" : "";
$where .= $division_id != -1 ? " and d.id = $division_id" : "";
//vd($where);
$select = "
    select concat(a.apellido, ' ', a.nombre) as nombre, a.matricula, a.beca, f.obs, concat(n.nombre,' ', anio.nombre, ' ', d.nombre ) as asignacion,
        concat(p.apellido, \" \", a.nombre) as viveCon, saldo(s.id, null) as saldo

		from alumno a
			inner join familia f on f.id = a.familia_id
			inner join socio s on s.Alumno_id = a.id
			inner join pariente p on p.id = a.vive_con_id
			inner join alumno_division ad on ad.Alumno_id = a.id and ad.ciclo_id = $ciclo_id
			inner join division d on d.id = ad.Division_id
			inner join anio on anio.id = d.Anio_id
			inner join nivel n on n.id = anio.Nivel_id
		where $where
		order by n.orden, anio.orden, d.nombre, a.apellido, a.nombre
";

$rows = Helpers::qryAll($select);
//vd($rows);
$cols = array(
//	"nivel" => array("title" => "Nivel"),
//	"anio" => array("title" => "Año"),
//	"division" => array("title" => "Division"),
	"nombre" => array("title" => "Nombre"),
	"matricula" => array("title" => "Matricula"),
	"beca" => array("title" => "Beca"),
	"obs" => array("title" => "Obs"),
	"asignacion" => array("title" => "Asignacion"),
	"viveCon" => array("title" => "ViveCon"),
	"saldo" => array("title" => "Saldo")
);

$excel = new Excel($rows, $cols, $fileName);
$excel->run();
$excel->output();


//Helpers::excel($rows, $cols, $fileName);


