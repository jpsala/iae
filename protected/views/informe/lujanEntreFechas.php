<?php
$fecha_hasta = date('Y/m/d', strtotime(str_replace('/','.', $fecha_hasta)));
$fecha_desde = date('Y/m/d', strtotime(str_replace('/','.', $fecha_desde)));

$ciclo_id = Ciclo::getCicloIdParaCargaDeNotas();
$rows = Helpers::qryAll("
  SELECT t.id, c.nombre as comprob, dvt.nombre as valor, format(sum(dv.importe),2) as total
    FROM doc d
      INNER JOIN doc_valor dv ON d.id = dv.Doc_id
      INNER JOIN talonario t ON d.talonario_id = t.id
      INNER JOIN comprob c ON t.comprob_id = c.id
      LEFT JOIN destino d1 ON dv.Destino_id = d1.id
      LEFT JOIN doc_valor_tipo dvt ON dv.tipo = dvt.id
  where t.id in(25,61) and dvt.id in (0,1) and d.fecha_valor BETWEEN '$fecha_desde' and '$fecha_hasta'
  group by t.id, dvt.id
"
);

$objPHPExcel = new MyPHPExcel();

$as = $objPHPExcel->getActiveSheet();
$as->SetCellValue('A1', "Comprobante");
$as->SetCellValue('B1', "Valor");
$as->SetCellValue('C1', "Total");
$as->getStyle("A1:B1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$as->getStyle("A1:B1")->getFont()->setBold(true);
$as->getStyle("A1:B1")->getFill()->getStartColor()->setARGB("666666");
$as->getStyle("A1:B1")->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
$i = 2;
foreach ($rows as $row) {
  $as->SetCellValue("A$i", $row["comprob"]);
  $as->SetCellValue("B$i", $row["valor"]);
  $as->SetCellValue("C$i", $row['total']);
  $i++;
}
$hasta = $i - 1;
$as->getStyle("A$i:B$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);


$as->getColumnDimension('A')->setAutoSize(true);
$as->getColumnDimension('B')->setAutoSize(true);
$as->getColumnDimension('C')->setAutoSize(true);


$objPHPExcel->output("Entradas_" . date("Y-m-d"));
