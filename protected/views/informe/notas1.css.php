<?php $this->renderPartial("/informe/head"); ?>
<?php $this->renderPartial("/informe/common.css"); ?>
<style type="text/css">
  #imprime {vertical-align: top; margin-top: -1px; display:none}
  .td-nota{ width:20px}
  .td-alumno{width:250px}
  .rec-img{display:none}
  #informe{width: 680px; margin:auto}
  table {width: 100%;margin-top: 3px}
  tr,body{background-color: white !important;
          background:white !important;}
  th, td, caption {padding: 1px 1px 4px 3px !important;font-size: 90%}
  .td-nota, .right-align{text-align: center !important}
  .td-alumno {width: 180px !important;}

  .datos-label {    display: inline-block;  min-width: 72px;
  }
  #datos{text-align: left; margin-bottom: 8px}
  #datos *{font-size: 93%}
  #notas1-header{ height: 60px;
                  margin-top: 10px;
                  text-align: center;}
  #notas1-hearder *{font-size: 13px; font-weight: bolder}
  #notas1-header #logo{        clear: both;margin-top: -8px;
                               display: inline-block;
                               height: 43px;
                               text-align: right;
                               width: 73px;}
  #texto-izquierda{    display: inline-block;
                       float: left;
                       text-align: left;}
  #texto-centrado{    display: inline-block;
                      text-align: center;}
  #logo-div{   background-image: url("images/logo1BN.png");
               background-position: left center;
               background-repeat: no-repeat;
               background-size: 44px 66px;
               display: inline-block;
               float: right;
               height: 63px;
               text-align: right;
               width: 43px;}
  #datos2{text-align: center}
  #nivel{float:left; display: inline-block}
  #anio-y-div{text-align: center; display: inline-block}
  #datos-profesor{float: right;display: inline-block}
  .rrotado{-webkit-transform: rotate(90deg);
           -moz-transform: rotate(90deg);   
           -o-transform: rotate(90deg);}
  #firma-aclaracion{    float: right;
    margin-top: -92px;
    position: absolute;
    right: 247px;
    text-align: center;
    width: 300px;}
  #firmas{    border-collapse: separate;
    border-spacing: 5px 46px;
    float: right;
    margin-top: 82px;
    max-width: 323px;
    position: absolute;
    right: 0;
    text-align: center;}
  #firmas td{border-top: black 1px solid; text-align: center;padding-top: 20px}
  #firmas tr{padding-bottom: 0px}
  #firmas .firma{width:180px}
  #firmas .aclaracion{width:200px}
  #desc-table {
    width: 431px;
  }
</style>
