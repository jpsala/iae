<?php
if ($impresion) {
  $this->renderPartial("/informe/notas1.css");
}
?>
<div id="informe">
  <div id="notas1-header">
    <div id="texto-izquierda">
      INSTITUTO ALBERT EINSTEIN
      <br>
      EDUCACION SECUNDARIA 
    </div>
    <div id="texto-centrado">
      PLANILLA DE CALIFICACIONES
      <br>
      CICLO LECTIVO 2013
    </div>
    <div id="logo-div">
    </div>
  </div>
  <div id="datos">
    <div id="datos-asignatura"><span class="datos-label">MATERIA:</span><?php echo $asignaturaNombre; ?></div>
    <div id="fecha">Fecha: <?php echo date("d/m/Y", time()); ?></div>
  </div>
  <div id="datos2">
    <div id="nivel">
      Nivel:<?php echo $divisionAsignatura->division->anio->nivel->nombre; ?>
    </div>
    <div id="anio-y-div">
      Año y Div.:<?php echo $divisionAsignatura->division->anio->nombre . " " . $divisionAsignatura->division->nombre; ?>
    </div>
    <div id="datos-profesor">
      Profesor:<?php echo $profesorNombre; ?></div>
  </div>
  <table id="main-table" class="table-border" border="1">
    <tr class="tr-header">
      <th class="rotado">Alumno</th>
      <?php foreach ($logicaItems as $li): ?>
        <th class="right-align"><?php echo $li->abrev; ?></th>
        <?php //if ($li->tiene_conducta): ?>
          <!--<th class="right-align" style="border-right: none"><?php echo "Inas."; ?></th>-->
        <?php //endif; ?>
      <?php endforeach; ?>
    </tr>
    <?php foreach ($notas as $alumnoDivision_id => $notasAlumno): ?>
      <?php $alumno = AlumnoDivision::model()->with("alumno")->findByPk($alumnoDivision_id)->alumno; ?>
      <tr calls="notas-alumno">
        <td class="td-alumno"><?php echo $alumno->nombreCompleto; ?>
          <input type="hidden" name="alumnos[]" value ="<?php echo $alumno->id; ?>"
        </td>
        <?php foreach ($notasAlumno as $nota): ?>
          <td class="right-align td-nota ">
            <?php echo $nota["nota"]; ?>
          </td>
        <?php endforeach; ?>
      </tr>
    <?php endforeach; ?>

  </table>
  <div id="firma-aclaracion">
    <table id="firmas" CELLPADDING="14" CELLSPACING="12" >
      <tr>
        <td class="firma"><div class="firma">Firma</div></td>
        <td class="aclaracion"><div class="aclaracion">Aclaración</div></td>
      </tr>

    </table>
  </div>
  <table id="desc-table" class="pagebreak">
    <?php /* @var $division Division */; ?>
    <tr>
      <th>Abrev.</th>
      <th>Descripción</th>
    </tr>
    <?php $row = 0; ?>
    <?php foreach ($logicaItems as $li): ?>
      <tr class="<?php echo ($row++ % 2) ? "tr-even" : ""; ?>">
        <td><?php echo $li->abrev; ?></td><td><?php echo $li->nombre; ?></td>
      </tr>
    <?php endforeach; ?>
  </table>
</div>

<script type="text/javascript">
  var impresion = <?php echo $impresion ? "false" : "true"; ?>;
  if (impresion) {
    $("#imprime").show();
  }
</script>
