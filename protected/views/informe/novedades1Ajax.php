<style type="text/css">
	.titulo {
		background-color: slategrey;
		text-align: left;
		margin-left: 4px;
		padding: 6px;
		color: white
	}

	.anio-table {
		padding-left: 7px
	}

	.division-table {
		padding-left: 7px
	}

	.novedad-table {
		padding-left: 0px
	}

	table {
		margin-bottom: 0px !important;
	}

	.anio-td, .division-td {
		padding: 0 !important
	}

	.sinpadding {
		padding: 0 !important
	}

	.titulo:hover {
		font-weight: 600;
		text-decoration: underline;
		width: 300px;
		cursor: pointer
	}

	.titulo {
		background-image: url("images/expanded.png");
		background-position: left center;
		background-repeat: no-repeat;
		color: #D3D3D3;
		padding-left: 18px;
	}

	.oculto {
		background-image: url("images/collapsed.png");
		background-position: left center;
		background-repeat: no-repeat;
		color: #D3D3D3;
		padding-left: 18px;
	}

	.elimina-td a {
		width: 5px;
		cursor: pointer;
		color: red;
		text-align: right
	}

	.elimina-td {
		text-align: right
	}

	.elimina-td:hover {
		text-decoration: #000
	}

	.detalle-td {
		width: 250px
	}

	.fecha-td {
		width: 60px
	}

	.alumno-td {
		width: 250px
	}
</style>
<table id="nivel-table">
	<?php $total = 0; ?>
	<?php $cantidad = 0; ?>
	<?php foreach ($rows as $nivel => $anios): ?>
		<tr>
			<td class="titulo"><?php echo $nivel; ?></td>
		</tr>
		<tr>
			<td class="anio-td">
				<table class="anio-table">
					<?php $totalAnio = 0; ?>
					<?php $cantidadAnio = 0; ?>
					<?php foreach ($anios as $anio => $divisiones): ?>
						<tr>
							<td class="titulo"><?php echo $anio; ?></td>
						</tr>

						<tr>
							<td class="division-td">
								<table class="division-table">
									<?php $totalAnio = 0; ?>
									<?php $cantidadAnio = 0; ?>
									<?php foreach ($divisiones as $division => $novedadesDivision): ?>
									<tr>
										<td class="titulo"><?php echo $division; ?></td>
									</tr>


									<tr>
										<td class="sinpadding">
											<table>
												<tr>
													<td class="division-td">
														<table class="division-table">
															<?php foreach ($novedadesDivision as $novedadNombre => $novedades): ?>
																<tr>
																	<td class="titulo"><?php echo $novedadNombre; ?></td>
																</tr>
																<tr>
																	<td class="novedad-td">
																		<table id="novedad-table">
																			<tr>
																				<!--<th>Usuario</th>-->
																				<th>Fec. Nov.</th>
																				<th>Fec. Carga</th>
																				<th>Alumno</th>
																				<th>Detalle</th>
																				<th class="importe">Importe</th>
																				<!--<td class=""></td>-->
																			</tr>
																			<?php $totalDivision = 0; ?>
																			<?php $cantidadDivision = 0; ?>
																			<?php foreach ($novedades as $novedad): ?>
																				<?php $total += $novedad["importe"]; ?>
																				<?php $totalDivision += $novedad["importe"]; ?>
																				<?php $totalAnio += $novedad["importe"]; ?>
																				<?php $cantidad++; ?>
																				<?php $cantidadDivision++; ?>
																				<?php $cantidadAnio++; ?>
																				<tr novedad_id="<?php echo $novedad["id"]; ?>">
																					<!--<td><?php //echo $novedad["usuario"]; ?></td>-->
																					<td
																						class="fecha-td"><?php echo date("d/m/Y", mystrtotime($novedad["fecha"])); ?></td>
																					<td
																						class="fecha-td"><?php echo date("d/m/Y", mystrtotime($novedad["fecha_carga"])); ?></td>
																					<td class="alumno-td"><?php echo $novedad["alumno"]; ?></td>
																					<!--<td><?php // echo $novedad["articulo"];                                                     ?></td>-->
																					<td class="detalle-td"><?php echo $novedad["detalle"]; ?></td>
																					<td class="importe"><?php echo number_format($novedad["importe"], 2); ?></td>
																					<?php if ($output !== "print"): ?>
																						<td class="elimina-td"><a
																								onclick="return eliminaNovedad($(this));">Borra</a></td>
																					<?php endif; ?>
																				</tr>
																			<?php endforeach; ?>
																			<tr>
																				<td colspan="4">
																					Total <?php echo "$nivel $anio $division $novedadNombre"; ?></td>
																				<td class="importe">
																					<?php echo number_format($totalDivision, 2); ?>
																				</td>
																			</tr>
																			<tr>
																				<td colspan="4">Cantidad</td>
																				<td class="importe">
																					<?php echo $cantidadDivision; ?>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															<?php endforeach; ?>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>

								</table>
								<?php endforeach; ?>
							</td>
						</tr>

						<tr>
							<td>
								<table>
									<tr>
										<td>Total <?php echo " $nivel $anio"; ?></td>
										<td class="importe">
											<?php echo number_format($totalAnio, 2); ?>
										</td>
									</tr>
									<tr>
										<td>Cantidad</td>
										<td class="importe">
											<?php echo $cantidadAnio; ?>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					<?php endforeach; ?>

				</table>
			</td>
		</tr>
	<?php endforeach; ?>
</table>
<table>
	<tr>
		<td colspan="">Total General</td>
		<td class="importe">
			<?php echo number_format($total, 2); ?>
		</td>
	</tr>
	<tr>
		<td colspan="">Cantidad</td>
		<td class="importe">
			<?php echo $cantidad; ?>
		</td>
	</tr>
</table>

<script type="text/javascript">
	<?php if ($output == "print"): ?>
	$(function () {
		$("#header").remove();
	});
	<?php endif; ?>
	$(".titulo").click(function () {
		if ($(this).hasClass("oculto")) {
			$(this).removeClass("oculto");
			$(this).closest("tr").next("tr").show();
		} else {
			$(this).addClass("oculto");
			$(this).closest("tr").next("tr").hide();
			console.log("no");
		}
	});
	function eliminaNovedad($this) {
		if (!confirm("Está seguro?")) {
			return false;
		}
		var $tr = $this.closest("tr");
		var novedad_id = $tr.attr("novedad_id");
		$.ajax({
				type: "POST",
				//      dataType:"json",
				data: {novedad_id: novedad_id},
				url: "<?php echo $this->createUrl("novedad/borraNovedad"); ?>",
				success: function (data) {
					if (data === "ok") {
						$tr.remove();
					} else {
						alerta(data, "error");
					}
				},
				error: function (data, status) {
				}
			}
		);

	}
</script>