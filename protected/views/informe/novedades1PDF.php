<?php

class Novedades1PDF extends PDF {

    public $informe, $Y, $imgHeader;

    public function header() {

        $this->Image($this->informe->imgHeader, 2, 2, 14);

        $this->setY($this->informe->marginY);

        $this->t($this->informe->args["title"], 0, null, "C", $this->getPageWidth());

        $txt = "Impreso el";
        $this->setX($this->getPageWidth() - $this->getStringWidth($txt) - 3);
        $this->setY($this->getY() + $this->t($txt));

        $fecha = date("d/m/Y", time());
        $this->setX($this->getPageWidth() - $this->getStringWidth($fecha) - 3);
        $this->t($fecha);


        $this->setY($this->getY() + $this->t($this->informe->args["title2"], 0, null, "C", $this->getPageWidth()));

        $this->Y = $this->getY() + 2;
        $this->line(0, $this->Y, $this->getPageWidth(), $this->Y);
        $this->Y = $this->getY() + 2;

        $h = "";

        $this->Y += $h;
        $this->line(0, $this->Y, $this->getPageWidth(), $this->Y);
        $this->Y += 1;
    }

    public function footer() {
        $txt = "Pág.:" . $this->getPage();
        $w = $this->getStringWidth($txt) + 2;
        $this->textBox(array(
                "txt" => $txt,
                "y" => $this->getPageHeight() - 7,
                "x" => $this->getPageWidth() - $w,
                "w" => $w,
        ));
    }

    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
    }

}

class Informe extends stdClass {

    public $pdf, $data, $marginX, $marginY, $cols, $colsDef, $colsDefDetail, $totals = array();

    public function __construct($options = array(), $colsDef = array(), $colsDefDetail = array()) {
        $this->imgHeader = "http://" . $_SERVER["SERVER_NAME"] . '/' . Yii::app()->baseUrl . "/images/logo.jpg";
        $this->colsDef = $colsDef;
        $this->colsDefDetail = $colsDefDetail;
        $defaults = array(
                "paperSize" => "A4",
                "orientation" => "P",
                "marginX" => 0,
                "marginY" => 0,
                "fontSize" => 7,
                "showHeader" => true,
                "showFooter" => true,
                "title" => "",
                "title2" => "",
        );
        $this->args = array_merge($defaults, $options);
        $this->marginY +=$this->args["marginY"];
        $this->marginX = $this->args["marginX"];
    }

    private function initPdf() {
        $this->pdf->planilla = $this;
        $this->pdf->SetCreator(PDF_CREATOR);
        $this->pdf->SetAuthor('JP');
        $this->pdf->SetTitle($this->args["title"]);
        $this->pdf->SetSubject($this->args["title"]);
        $this->pdf->SetKeywords('TCPDF, PDF, Planilla');
        $this->pdf->SetPrintHeader(true);
        $this->pdf->SetPrintFooter(true);
        $this->pdf->SetAutoPageBreak(false);
        $this->margenX = $this->args["marginX"];
        $this->margenY = $this->args["marginY"];
        $this->fontSize = $this->args["fontSize"];
        $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", 11);
        $this->pdf->setCellPaddings(0, 0, 0, 0);
        $this->pdf->AddPage();
    }

    public function render() {
        $this->pdf = new Novedades1PDF($this->args["orientation"], "mm", $this->args["paperSize"]);
        $this->pdf->informe = $this;
        $this->cols = array();
        $this->initPdf();
        //
        $height = 5;
        $y = $this->marginY = 14;
        $this->pdf->setY($y + $this->marginY);
        $total = 0;
        foreach ($this->data as $nivel => $dataNivel) {
//            $col = date($format, mystrtotime($col));
//            $col = number_format($col, 2);
//            $this->totals[$key] += str_replace(",", "", $col);
            $x = $this->marginX = 5;
            $w = $this->pdf->getStringWidth($nivel) + 1;
            //$saldo += $col;
            $this->pdf->textBox(array(
                    "txt" => $nivel,
                    "y" => $y,
                    "x" => $x,
                    "w" => 100,
                    "h" => $height,
                    "align" => "L",
            ));
            $y+=$height;
            $xNivel = $this->marginX + 5;
            $xAnio = $xNivel + 5;
            $xNov = $xAnio + 5;
            $xDivision = $xNov + 5;
            $totalNivel = 0;
            foreach ($dataNivel as $anio => $dataAnio) {
                $x = $xNivel;
                $this->pdf->textBox(array(
                        "txt" => $anio,
                        "y" => $y,
                        "x" => $x,
                        "w" => 100,
                        "h" => $height,
                        "align" => "L",
                ));
                $y+=$height;
                $totalAnio = 0;
                foreach ($dataAnio as $division => $dataDivision) {
                    $x = $xAnio;
                    $this->pdf->textBox(array(
                            "txt" => $division,
                            "y" => $y,
                            "x" => $x,
                            "w" => 100,
                            "h" => $height,
                            "align" => "L",
                    ));

                    $y+=$height;
                    $totalDivision = 0;
                    foreach ($dataDivision as $novedad => $dataNov) {
                        $x = $xNov;
                        $this->pdf->textBox(array(
                                "txt" => $novedad,
                                "y" => $y,
                                "x" => $x,
                                "w" => 100,
                                "h" => $height,
                                "align" => "L",
                        ));

                        $totalNovedad = 0;
                        $y+=$height;
                        $dataPrint = array("alumno", "fecha", "usuario", "importe");
                        foreach ($dataNov as $data) {
                            foreach ($dataPrint as $field) {
                                $value = $field == "fecha" ? date("d/m/Y_", mystrtotime($data[$field])) : $data[$field];
                                $value = $field == "usuario" ? substr($value, 0, 4) : $value;
                                $w = $this->pdf->getStringWidth($value) + 1;
                                $colsW[$field] = isset($colsW[$field]) ? $colsW[$field] : 0;
                                $colsW[$field] = ($w > $colsW[$field]) ? $w : ($colsW[$field]);
                            }
                        }
                        foreach ($dataNov as $data) {
                            $x = $xDivision;
                            $totalNovedad += $data["importe"];
                            if ($this->chkPageEnd($height + 2)) {
                                $y = $this->marginY + 14;
                                $x = $xDivision;
                            }
                            foreach ($dataPrint as $field) {
                                $value = $field == "fecha" ? date("d/m/Y ", mystrtotime($data[$field])) : $data[$field];
                                $value = $field == "usuario" ? substr($value, 0, 4) : $value;
                                $this->pdf->textBox(array(
                                        "txt" => $value,
                                        "y" => $y,
                                        "x" => $x,
                                        "w" => $colsW[$field],
                                        "h" => $height,
                                        "align" => in_array($field, array("importe", "fecha")) ? "R" : "L",
                                ));
                                $x+=$colsW[$field];
                            }
                            $y+=$height;
                            $x = $xDivision;
                        }
                        $this->pdf->textBox(array(
                                "txt" => "Total Novedad: " . number_format($totalNovedad, 2),
                                "y" => $y + 4,
                                "x" => $x - 5,
                                "w" => 100,
                                "h" => $height,
                                "align" => "L",
                                "fontstyle" => "B",
                        ));
                        $y+=$height + 8;
                        $total+=$totalNovedad;
                        $totalDivision += $totalNovedad;
                        $totalAnio += $totalNovedad;
                        $totalNivel += $totalNovedad;
                    }
                    $this->pdf->textBox(array(
                            "txt" => "Total división: " . number_format($totalDivision, 2),
                            "y" => $y,
                            "x" => $x - 10,
                            "w" => 100,
                            "h" => $height,
                            "align" => "L",
                            "fontstyle" => "B",
                    ));
//                    $y+=$height + 4;
                    $this->pdf->addPage();
                    $y = $this->marginY + 14;
                    $x = $xDivision;
                }
                $this->pdf->textBox(array(
                        "txt" => "Total Año: " . number_format($totalAnio, 2),
                        "y" => $y,
                        "x" => $x - 15,
                        "w" => 100,
                        "h" => $height,
                        "align" => "L",
                        "fontstyle" => "B",
                ));
//                $y+=$height + 4;
                $this->pdf->addPage();
                $y = $this->marginY + 14;
                $x = $xAnio;
            }
            $this->pdf->textBox(array(
                    "txt" => "Total Nivel: " . number_format($totalNivel, 2),
                    "y" => $y,
                    "x" => $x - 20,
                    "w" => 100,
                    "h" => $height,
                    "align" => "L",
                    "fontstyle" => "B",
            ));
            $y+=$height + 4;
//            $this->pdf->addPage();
//            $y = $this->marginY + 14;
//            $x = $xNivel;
        }
        $this->pdf->Output();
    }

    private function renderData($data, $y = null, $x = 0, $colsDef = array()) {
        $y = $y ? $y : $this->pdf->Y;
        $left = $this->marginX + $x;
        $saldo = 0;
        foreach ($data as $row) {
            $x = $left;
            $height = 0;
            foreach ($row as $key => $col) {
                if (!is_array($col)) {
                    $col = trim($col);
                    $h = $this->pdf->getStringHeight($this->pdf->getStringWidth($col) + 2, $col, false, false, '', 0);
                    $height = ($height > $h) ? $height : $h;
                }
            }
            if ($this->chkPageEnd($height)) {
                $y = $this->pdf->Y;
                $x = $left;
            }

            foreach ($row as $key => $col) {
                if (!is_array($col)) {
                    $debeHaber = false;
                    if (isset($colsDef[$key])) {
                        if (isset($colsDef[$key]["visible"]) and !$colsDef[$key]["visible"]) {
                            continue;
                        }
                        if (isset($colsDef[$key]["date"])) {
                            $format = $colsDef[$key]["date"] ? $colsDef[$key]["date"] : "d/m/Y";
                            $col = date($format, mystrtotime($col));
                        }
                        if (isset($colsDef[$key]["debeHaber"])) {
                            $debeHaber = $colsDef[$key]["debeHaber"];
                        } elseif (isset($colsDef[$key]["currency"])) {
                            $col = number_format($col, 2);
                        }
                        if (isset($this->totals[$key])) {
                            $this->totals[$key] += str_replace(",", "", $col);
                        }
                        if (isset($colsDef[$key]["mb_convert_case"])) {
                            $col = mb_convert_case($col, MB_CASE_TITLE, "UTF-8");
                        }
                        if (isset($colsDef[$key]["strtolower"])) {
                            //$col = strtolower($col);
                        }
                    }
                    if (isset($this->cols[$key]["width"])) {
                        $w = $this->cols[$key]["width"];
                    } elseif (isset($colsDef[$key]["width"])) {
                        $w = $colsDef[$key]["width"];
                    } else {
                        $w = $this->pdf->getStringWidth($col) + 2;
                    }
                    if ($debeHaber) {
                        if ($debeHaber == "dh") {
                            $debe = ($col >= 0) ? number_format(abs($col), 2) : "";
                            $haber = ($col < 0) ? number_format(abs($col), 2) : "";
                        } else {
                            $debe = ($col < 0) ? number_format(abs($col), 2) : "";
                            $haber = ($col >= 0) ? number_format(abs($col), 2) : "";
                        }
                        $saldo += $col;
                        $this->pdf->textBox(array(
                                "txt" => $debe,
                                "y" => $y,
                                "x" => $x,
                                "w" => $w,
                                "h" => $height,
                                "align" => isset($this->cols[$key]) ? $this->cols[$key]["align"] : "L",
                        ));
                        $x+=$w;
                        $this->pdf->textBox(array(
                                "txt" => $haber,
                                "y" => $y,
                                "x" => $x,
                                "w" => $w,
                                "h" => $height,
                                "align" => isset($this->cols[$key]) ? $this->cols[$key]["align"] : "L",
                        ));
                        $x+=$w;
                        $this->pdf->textBox(array(
                                "txt" => number_format($saldo, 2),
                                "y" => $y,
                                "x" => $x,
                                "w" => $w,
                                "h" => $height,
                                "align" => isset($this->cols[$key]) ? $this->cols[$key]["align"] : "L",
                        ));
                        $x+=$w;
                    } else {
                        $this->pdf->textBox(array(
                                "txt" => $col,
                                "y" => $y,
                                "x" => $x,
                                "w" => $w,
                                "h" => $height,
                                "align" => isset($this->cols[$key]) ? $this->cols[$key]["align"] : "L",
                        ));
                        $x+=$w;
                    }
                }
            }
            $y+=$height;
            if (isset($row["detail"]) and count($row["detail"]) > 0) {
                $y = $this->renderData($row["detail"], $y, 5, $this->colsDefDetail) + 7;
            }
        }

        if (count($data) > 0) {
            $x = $left;
            foreach ($data[0] as $key => $col) {
                if (!is_array($col)) {
                    if (isset($colsDef[$key]["visible"]) and !$colsDef[$key]["visible"]) {
                        continue;
                    }
                    $debeHaber = isset($colsDef[$key]["debeHaber"]) ? true : false;
                    if (isset($this->cols[$key]["width"])) {
                        $w = $this->cols[$key]["width"];
                    } else {
                        $w = $this->pdf->getStringWidth($col) + 2;
                    }
                    if ($debeHaber) {
                        $x+=$w * 2;
                        $this->pdf->textBox(array(
                                "txt" => number_format($saldo, 2),
                                "y" => $y,
                                "x" => $x,
                                "w" => $w,
                                "h" => $height,
                                "align" => isset($this->cols[$key]) ? $this->cols[$key]["align"] : "L",
                        ));
                    } else {
                        $col = isset($this->totals[$key]) ? number_format($this->totals[$key], 2) : "";
                        $this->pdf->textBox(array(
                                "txt" => $col,
                                "y" => $y,
                                "x" => $x,
                                "w" => $w,
                                "h" => $height,
                                "align" => isset($this->cols[$key]) ? $this->cols[$key]["align"] : "L",
                        ));
                        $x+=$w;
                    }
                }
            }
        }
        return $y;
    }

    private function chkPageEnd($height) {
        if (($this->pdf->getY() + $height + 15) >= $this->pdf->getPageHeight()) {
            $this->pdf->AddPage();
            return true;
        } else {
            return false;
        }
    }

}

$i = new Informe();
$i->data = $rows;
$i->render();
?>
