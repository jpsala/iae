<?php
$fileName = "Alumnos_planilla_buffet.xls";

$sql = "
  select * from alumno limit 1   
";
$rows = Helpers::qryAll($sql);
ini_set('memory_limit', '-1');

$FIRST_CELL = "A1";

$objPHPExcel = new MyPHPExcel();
$as = $objPHPExcel->setActiveSheetIndex(0);

$as->getStyle("$FIRST_CELL:B1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$as->getStyle("$FIRST_CELL:B1")->getFont()->setBold(true);
$as->getStyle("$FIRST_CELL:B1")->getFill()->getStartColor()->setARGB("666666");
$as->getStyle("$FIRST_CELL:B1")->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);

if (count($rows) < 1)
	return;

$styleArray = array(
	  'borders' => array(
			 'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
			 )
	  )
);

$as->mergeCells('A1:Z1');
$as->getStyle("A1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$as->SetCellValue("A1", "Reporte de Alumnos (Apellido y Nombre)");
$rowNum = 5;
foreach ($rows as $row) {
	$as->SetCellValue("A" . $rowNum, $row["nombre"]);
//  for ($col = 2; $col <= 26; $col++) {
//    $scol = chr($col + 64);
//    $objPHPExcel->getActiveSheet()->getStyle("$scol$rowNum")->applyFromArray($styleArray);
//  }
	$rowNum++;
}
$rowNum--;
$objPHPExcel->getActiveSheet()->getStyle("A1:Z$rowNum")->applyFromArray($styleArray);
for ($col = 2; $col <= 26; $col++) {
	$scol = chr($col + 64);
	$as->getColumnDimension($scol)->setWidth(5);
}

$as->getColumnDimension("A")->setAutoSize(true);

$as->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$as->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_LEGAL);
$as->getPageSetup()->setFitToPage(true);
$as->getPageSetup()->setFitToWidth(1);
$as->getPageSetup()->setFitToHeight(0);

$objPHPExcel->output($fileName);
