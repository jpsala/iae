<?php

class PlanDeCuentasPDF extends PDF {

    public $informe, $Y, $imgHeader;

    public function header() {
        $this->Image($this->informe->imgHeader, 2, 2, 14);

        $this->setY($this->informe->marginY);

        $this->t($this->informe->args["title"], 0, null, "C", $this->getPageWidth());

        $txt = "Impreso el";
        $this->setX($this->getPageWidth() - $this->getStringWidth($txt) - 3);
        $this->setY($this->getY() + $this->t($txt));

        $fecha = date("d/m/Y", time());
        $this->setX($this->getPageWidth() - $this->getStringWidth($fecha) - 3);
        $this->t($fecha);


        $this->setY($this->getY() + $this->t($this->informe->args["title2"], 0, null, "C", $this->getPageWidth()));

        $this->Y = $this->getY() + 2;
        $this->line(0, $this->Y, $this->getPageWidth(), $this->Y);
        $this->Y = $this->getY() + 2;

        $this->line(0, $this->Y, $this->getPageWidth(), $this->Y);
    }

    public function footer() {
        $txt = "Pág.:" . $this->getPage();
        $w = $this->getStringWidth($txt) + 2;
        $this->textBox(array(
                "txt" => $txt,
                "y" => $this->getPageHeight() - 7,
                "x" => $this->getPageWidth() - $w,
                "w" => $w,
        ));
    }

    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
    }

}

class PlanDeCuentas extends stdClass {

    public $pdf, $data, $marginX, $marginY, $cols, $colsDef, $colsDefDetail, $totals = array(), $plan;

    public function __construct($plan) {
        $options=array();
        $this->plan = $plan;
        $this->imgHeader = "http://" . $_SERVER["SERVER_NAME"] . '/' . Yii::app()->baseUrl . "/images/logo.jpg";
        $defaults = array(
                "paperSize" => "A4",
                "orientation" => "P",
                "marginX" => 0,
                "marginY" => 0,
                "fontSize" => 7,
                "showHeader" => true,
                "showFooter" => true,
                "title" => "Plan de cuentas",
                "title2" => "",
        );
        $this->args = array_merge($defaults, $options);
        $this->marginY +=$this->args["marginY"];
        $this->marginX = $this->args["marginX"];
    }

    private function initPdf() {
        $this->pdf->planilla = $this;
        $this->pdf->SetCreator(PDF_CREATOR);
        $this->pdf->SetAuthor('JP');
        $this->pdf->SetTitle($this->args["title"]);
        $this->pdf->SetSubject($this->args["title"]);
        $this->pdf->SetKeywords('TCPDF, PDF, Planilla');
        $this->pdf->SetPrintHeader(true);
        $this->pdf->SetPrintFooter(true);
        $this->pdf->SetAutoPageBreak(true);
        $this->margenX = $this->args["marginX"];
        $this->margenY = $this->args["marginY"];
        $this->fontSize = $this->args["fontSize"];
        $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", 11);
        $this->pdf->setCellPaddings(0, 0, 0, 0);
        $this->pdf->SetMargins(0, 16);
        $this->pdf->AddPage();
    }

    public function render() {
        $this->pdf = new PlanDeCuentasPDF($this->args["orientation"], "mm", $this->args["paperSize"]);
        $this->pdf->informe = $this;
        $this->initPdf();
        $this->pdf->writeHTML($this->plan);
        $this->pdf->Output();
    }

}

$informe = new PlanDeCuentas($plan);
$informe->render();