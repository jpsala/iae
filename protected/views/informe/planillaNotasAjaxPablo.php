<?php

	class MYPDF extends PDF {

		public $planilla;

		public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false) {
			parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
		}

		public function Header() {
			parent::Header();
			$headerHeight = 20;
			$fontSize = $this->getFontSize();
			$fontFamily = $this->getFontFamily();
			$fontStyle = $this->getFontStyle();
			$paddings = $this->getCellPaddings();
			$this->setCellPaddings(0, 0, 0, 0);
			$this->SetFont(PDF_FONT_NAME_MAIN, "", $this->planilla->fontSizeDatos);
			$col1Width = 38;
			$col1 = $this->marginX; // + 20;
			$this->setXY($col1, $this->marginY);
			$currY = $this->marginY;
			$height = $this->getStringHeight("Ciclo Lectivo", $col1Width);
			$this->textBox("Ciclo Lectivo:", $col1Width);
			$this->textBox(":  " . $this->planilla->notas["datos"]["ciclo"]);
			$this->setXY($col1, $currY+=$height);

			$this->textBox("Nivel", $col1Width);
			$this->textBox(":  " . $this->planilla->notas["datos"]["nivel"]);
			$this->setXY($col1, $currY+=$height);

			$this->textBox("Fecha de impresión", $col1Width);
			$this->textBox(":  " . date("d/mY", time()));
			$this->setXY($col1, $currY+=$height);

			$col1+= 80;
			$currY = $this->marginY;
			$this->setXY($col1, $this->marginY - 2);

			$this->textBox(array("txt" => "INSTITUTO ALBERT EINSTEIN", "fontsize" => 12, "align" => "C", "x" => 0, "w" => $this->getPageWidth()));
			$this->y += 4;
			$this->textBox(array("txt" => "PLANILLA DE CALIFICACIONES", "align" => "C", "x" => 0, "fontsize" => 12, "w" => $this->getPageWidth()));
			$this->y += 4;
			$cicloNombre = Ciclo::getActivoNombre();
			$this->textBox(array("txt" => "ES-POLIMODAL", "align" => "L", "fontsize" => 8));
			$this->textBox(array("txt" => "CICLO LECTIVO " . $cicloNombre, "align" => "C", "fontsize" => 8));
			$this->y += 4;
			$this->textBox(array("txt" => "ÁREA/ESPACIO CURRICULAR " . $this->planilla->asignaturaNombre, "align" => "L", "fontsize" => 8));
			$this->y += 4;
			$this->textBox(array("txt" => "Fecha: " . date("d/m/Y", time()), "align" => "L", "fontsize" => 8));
			$this->setXY($col1, $currY+=$height);
			$this->y += 5;

			$divisionY = $this->y; //lo imprimo mas abajo sino me corre los encabezados


			$cd = "Curso/División :  " . $this->planilla->notas["datos"]["anio"] . "/" . $this->planilla->notas["datos"]["division"];
			$this->textBox(array("txt" => $cd, "fontsize" => 12));
			$this->setXY($col1 + $col1Width + 8, $currY);

			$this->planilla->currY = $this->getY() + $height + 3;

			$this->SetFont(PDF_FONT_NAME_MAIN, "", $this->planilla->fontSizeNotasHeader);
			$this->setCellPaddings(2, 2, 0, 0);
			$this->setCellHeightRatio(1.1);

//$this->planilla->currX =$this->marginX;
			$this->planilla->currY += 35;
//        vd($this->planilla->currX);
			;
			$this->SetFont(PDF_FONT_NAME_MAIN, "", $this->planilla->fontSizeMateria);

			$i = 0;
			$this->startTransform();
			$this->rotate(-90);
			$this->MultiCell(30, ($this->planilla->anchoNotas)
					, $this->planilla->periodos[$i]["texto"], "LTBR", "C", false
					, 0, 141, 135.3 - $this->planilla->anchoNotas
					, true, 0, false, false, $this->planilla->anchoNotas, 'M', false);
			$this->rotate(90);
			$this->stopTransform();

			$i++;
			$this->MultiCell($this->planilla->anchoNotas * $this->planilla->periodos[$i]["colspan"]
					, 30, $this->planilla->periodos[$i]["texto"], "LTBR", "C", false
					, 0, 11.8, 22.3
					, true, 0, false, true, 0, 'B', false);

			$i++;
			$this->startTransform();
			$this->rotate(-90);
			$this->MultiCell(30, ($this->planilla->anchoNotas)
					, $this->planilla->periodos[$i]["texto"], "LTBR", "C", false
					, 0, 60.8, 12.6
					, true, 0, false, false, $this->planilla->anchoNotas, 'M', false);
			$this->rotate(90);
			$this->stopTransform();

			$i++;
			$this->MultiCell($this->planilla->anchoNotas * 4 + .1
					, 30, $this->planilla->periodos[$i]["texto"], "LTBR", "C", false
					, 0, 60.7 + 9.9, 22.3
					, true, 0, false, true, 0, 'B', false);

			$i++;
			$this->MultiCell($this->planilla->anchoNotas * 4 + .1
					, 30, $this->planilla->periodos[$i]["texto"], "LTBR", "C", false
					, 0, 11.1 * 9 + 9.9, 22.3
					, true, 0, false, true, 0, 'B', false);

			$i++;
			$this->MultiCell($this->planilla->anchoNotas * 4 + .1
					, 30, $this->planilla->periodos[$i]["texto"], "LTBR", "C", false
					, 0, 11.1 * 12 + 5.8 + 9.9, 22.3
					, true, 0, false, true, 0, 'B', false);

			$i++;
			$this->startTransform();
			$this->rotate(-90);
			$this->MultiCell(30, ($this->planilla->anchoNotas)
					, $this->planilla->periodos[$i]["texto"], "LTBR", "C", false
					, 0, 188.2, 12.5
					, true, 0, false, false, $this->planilla->anchoNotas, 'M', false);
			$this->rotate(90);
			$this->stopTransform();

			$i++;
			$this->startTransform();
			$this->rotate(-90);
			$this->MultiCell(30, ($this->planilla->anchoNotas)
					, $this->planilla->periodos[$i]["texto"], "", "C", false
					, 0, 225, 20
					, true, 0, false, false, $this->planilla->anchoNotas, 'M', false);
			$this->rotate(90);
			$this->stopTransform();

			$this->MultiCell($this->planilla->anchoNotas
					, 30, "", "LTBR", "C", false
					, 0, 198, 22.3
					, true, 0, false, true, 0, 'B', false);

			$this->setXY($col1 + $col1Width, $divisionY);
			$cd = "Asignatura:  " . $this->planilla->asignaturaNombre . "   ";
			$this->textBox(array("txt" => $cd, "fontsize" => 12, "align" => "R"));

			$this->setXY($col1 + $col1Width, $divisionY - $height);
			$cd = "Profesor:  " . Yii::app()->user->model->nombre . "   ";
			$this->textBox(array("txt" => $cd, "fontsize" => 12, "align" => "R"));


			$this->planilla->currX = $this->marginX;

			$this->SetFont(PDF_FONT_NAME_MAIN, "", $fontSize);
			$this->setCellPaddings($paddings);

			$this->setFont($fontFamily, $fontStyle, $fontSize);
			$this->getCellPaddings($paddings);
		}

		public function Footer() {
			$this->SetY(0);
			$this->SetX(0);
		}

	}

	class Informe extends stdClass {

		public $pdf;
		public $numeros = array("Cero", "Uno", "Dos", "Tres", "Cuatro", "Cinco", "Seis", "Siete", "Ocho", "Nueve", "Diez");
		public $fontSize, $fontSizeNota, $fontSizeAlumno, $fontSizeDatos, $fontSizeNotasHeader;
		public $periodo_nombre, $paperSize;
		public $col1, $col2, $asignaturas, $alumnoWidth, $CondAsisWidth, $currY, $currX;
		public $headerHeight, $anchoColumnas;
		public $asignaturasHeaderHeight, $MatDesWidth;
		public $anchoNotas, $asignaturaNombre;

		public function __construct($notas, $paperSize = "legal") {
			$this->paperSize = $paperSize;
			$this->notas = $notas;
			//vd($notas);
		}

		public function imprime() {
			$this->pdf = new MYPDF("P", "mm", $this->paperSize);
			$this->pdf->marginY = 4;
			$this->pdf->marginX = 2;
			$this->pdf->planilla = $this;
			$this->pdf->SetCreator(PDF_CREATOR);
			$this->pdf->SetAuthor('JP');
			$this->pdf->SetTitle('Planilla Resumen Final');
			$this->pdf->SetSubject('Planilla Resumen Final IAE');
			$this->pdf->SetKeywords('TCPDF, PDF, Planilla');
			$this->pdf->SetPrintHeader(true);
			$this->pdf->SetPrintFooter(false);
			$this->pdf->SetAutoPageBreak(false);
			$this->fontSize = 8;
			$this->fontSizeNota = $this->fontSize + 1;
			$this->fontSizeNotasHeader = $this->fontSize + 1;
			$this->fontSizeDatos = $this->fontSize + 4;
			$this->fontSizeAlumno = $this->fontSize + 1;
			$this->fontSizeMateria = $this->fontSize;
			$this->preparaPdf();
			$this->pdf->Output();
		}

		private function preparaPdf() {

			/*
			 * Cálculo del ancho de la columna alumnos
			 */
//    vd($this->notas);
			$this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", $this->fontSizeAlumno);
			$this->pdf->setCellPaddings(1, 2, 0, 0);
			$this->anchoNotas = 9.8;
			$this->periodos = array(
					array("texto" => "Nro. de orden", "colspan" => 1, "rotated" => true, "footer" => ""),
					array("texto" => "Apellido y Nombre del Alumno", "colspan" => 5, "rotated" => false, "footer" => ""),
					array("texto" => "1ER. BIMESTRE NOTA INFORMATIVA", "colspan" => 1, "rotated" => true, "footer" => ""),
					array("texto" => "PRIMER TRIMESTRE", "colspan" => 4, "rotated" => false, "footer" => "Firma y aclaración"),
					array("texto" => "SEGUNDO TRIMESTRE", "colspan" => 4, "rotated" => false, "footer" => "Firma y aclaración"),
					array("texto" => "TERCER TRIMESTRE", "colspan" => 5, "rotated" => false, "footer" => "Firma y aclaración", "integradora"=>true),
					array("texto" => "Promedio Anual", "colspan" => 1, "rotated" => true, "footer" => ""),
					array("texto" => "Calificación Final", "colspan" => 1, "rotated" => true, "footer" => ""),
			);

			$this->currX = $this->pdf->marginX;

			/*
			 * Cálculo de las diferentes columnas de materias
			 */
			$this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", $this->fontSizeDatos);
			$this->pdf->setCellPaddings(0, 2, 0, 0);
			$this->pdf->setCellHeightRatio(1.1);
			$height = 0;

			$this->pdf->setCellPaddings(0, 0, 0, 0);
			$this->pdf->setCellHeightRatio(1);
			$this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", $this->fontSize);

			$this->currX = $this->pdf->marginX;


			$this->pdf->AddPage();
			$orden = 1;
                        if(!isset($this->notas["alumnos"])){
                            $this->notas["alumnos"] = array();
                        }
			foreach ($this->notas["alumnos"] as $alumnoNombre => $notas) {
				/*
				 * Imprimo el alumno
				 */

				$this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", $this->fontSizeAlumno);
				$this->pdf->setCellPaddings(1, 1, 0, 1);
				$height = $this->pdf->getStringHeight($this->anchoNotas * 5, $alumnoNombre, false, true, '', 0);
				$this->chkPageEnd($height);
				$this->currX = $this->pdf->marginX;
				$this->pdf->MultiCell($this->anchoNotas, $height, $orden++, "LBR", "L", false, 1, $this->currX, $this->currY
						, true, 0, false, true, 0, 'C', false);
				$this->currX = $this->pdf->marginX + $this->anchoNotas;
				$this->pdf->MultiCell($this->anchoNotas * 5, $height, "$alumnoNombre", "LBR", "L", false, 1, $this->currX, $this->currY
						, true, 0, false, true, 0, 'C', false);
				/*
				 * Imprimo las notas
				 */
				$this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", $this->fontSizeNota);
				$this->currX+=$this->anchoNotas * 5;
				foreach (array_keys($this->notas["items"]) as $item) {
					$fnt = $this->pdf->getFontStyle();
//					vd2($notas["notas"][$item]);
					$txt = isset($notas["notas"][$item]) ? $notas["notas"][$item]["nota"] : "";
					$conIntegradora = $notas["notas"][$item]["logica_periodo_id"] == "98";
					$ancho = $conIntegradora ? $this->anchoNotas -2: $this->anchoNotas;
					//if ($txt < 7) {
					if ($notas["notas"][$item]["estado"] !== "Aprobado") {
						$this->pdf->setFont(null, "BI");
					}
					$this->pdf->MultiCell($ancho, $height, $txt, "BR", "C", false, 1, $this->currX, $this->currY
							, true, 0, false, true, 0, 'M', false);
					$this->pdf->setFont(null, $fnt);
					$this->currX+=$ancho;
				}
				$this->currY+=$height;
			}
			$this->currX = $this->pdf->marginX;

			foreach ($this->periodos as $periodo) {
				$this->pdf->MultiCell($this->anchoNotas * $periodo["colspan"]
						, 10, "", "LTBR", "C", false
						, 0, $this->currX, $this->currY
						, true, 0, false, true, 0, 'B', false);
				$this->currX+=($this->anchoNotas * $periodo["colspan"]);
			}

			$this->currX = $this->pdf->marginX;
			$this->currY += 9.8;
			foreach ($this->periodos as $periodo) {
				$this->pdf->MultiCell($this->anchoNotas * $periodo["colspan"]
						, 7, $periodo["footer"], "LTBR", "C", false
						, 0, $this->currX, $this->currY
						, true, 0, false, true, 0, 'B', false);
				$this->currX+=($this->anchoNotas * $periodo["colspan"]);
			}

			$this->currY += 12;
			$this->currX = $this->pdf->marginX;


			$rowsCats = array("10y7", "6y4", "3y1", "noCalif");

			for ($i = 0; $i < 4; $i++) {
				$this->currX = $this->pdf->marginX;
				$this->pdf->MultiCell($this->anchoNotas * 6
						, 7, $this->notas["statsLeyendas"][$i], "LTBR", "L", false
						, 0, $this->currX, $this->currY
						, true, 0, false, true, 0, 'B', false);
				$this->currX+=($this->anchoNotas * $periodo["colspan"] * 6);

				$this->pdf->MultiCell($this->anchoNotas
						, 7, "", "LTBR", "C", false
						, 0, $this->currX, $this->currY
						, true, 0, false, true, 0, 'B', false);
				$this->currX+=($this->anchoNotas * $periodo["colspan"]);
				$this->pdf->MultiCell($this->anchoNotas * 2
						, 7, $this->notas["stats"][$rowsCats[$i]]["1er."]["cant"], "LTBR", "C", false
						, 0, $this->currX, $this->currY
						, true, 0, false, true, 0, 'B', false);
				$this->currX+=($this->anchoNotas * $periodo["colspan"] * 2);
				$this->pdf->MultiCell($this->anchoNotas * 2
						, 7, $this->notas["stats"][$rowsCats[$i]]["1er."]["porcentaje"], "LTBR", "C", false
						, 0, $this->currX, $this->currY
						, true, 0, false, true, 0, 'B', false);
				$this->currX+=($this->anchoNotas * $periodo["colspan"] * 2);
				$this->pdf->MultiCell($this->anchoNotas * 2
						, 7, $this->notas["stats"][$rowsCats[$i]]["2do."]["cant"], "LTBR", "C", false
						, 0, $this->currX, $this->currY
						, true, 0, false, true, 0, 'B', false);
				$this->currX+=($this->anchoNotas * $periodo["colspan"] * 2);
				$this->pdf->MultiCell($this->anchoNotas * 2
						, 7, $this->notas["stats"][$rowsCats[$i]]["2do."]["porcentaje"], "LTBR", "C", false
						, 0, $this->currX, $this->currY
						, true, 0, false, true, 0, 'B', false);
				$this->currX += ($this->anchoNotas * $periodo["colspan"] * 2);
				$this->pdf->MultiCell($this->anchoNotas * 2
						, 7, $this->notas["stats"][$rowsCats[$i]]["3ro."]["cant"], "LTBR", "C", false
						, 0, $this->currX, $this->currY
						, true, 0, false, true, 0, 'B', false);
				$this->currX += ($this->anchoNotas * $periodo["colspan"] * 2);
				$this->pdf->MultiCell($this->anchoNotas * 2
						, 7, $this->notas["stats"][$rowsCats[$i]]["3ro."]["porcentaje"], "LTBR", "C", false
						, 0, $this->currX, $this->currY
						, true, 0, false, true, 0, 'B', false);
				$this->currX += ($this->anchoNotas * $periodo["colspan"]) * 2;

				$this->pdf->MultiCell($this->anchoNotas
						, 7, "", "LTBR", "C", false
						, 0, $this->currX, $this->currY
						, true, 0, false, true, 0, 'B', false);
				$this->currX += ($this->anchoNotas * $periodo["colspan"]);
				$this->pdf->MultiCell($this->anchoNotas
						, 7, "", "LTBR", "C", false
						, 0, $this->currX, $this->currY
						, true, 0, false, true, 0, 'B', false);
				$this->currX += ($this->anchoNotas * $periodo["colspan"]);
				$this->currY += 7;
			}
		}

		private function chkPageEnd($height) {
			if (($this->currY + $height + $this->pdf->marginY + 2) >= $this->pdf->getPageHeight()) {
				$this->pdf->AddPage();
				$this->currY = $this->pdf->marginY + $this->headerHeight;
			}
		}

	}

	$i = new Informe($notas, $paperSize);
	$i->asignaturaNombre = $asignaturaNombre;

	$i->imprime();
?>