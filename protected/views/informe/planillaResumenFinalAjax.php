<?php

class MYPDF extends PDF
{

  public $planilla;

  public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false)
  {
    parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
  }

  public function Header()
  {
    $fontSize = $this->getFontSize();
    $fontFamily = $this->getFontFamily();
    $fontStyle = $this->getFontStyle();
    $paddings = $this->getCellPaddings();
    parent::Header();
    $this->setCellPaddings(0, 0, 0, 0);
    $this->SetFont(PDF_FONT_NAME_MAIN, "", $this->planilla->fontSizeDatos);
    $col1Width = 38;
    $col1 = $this->marginX; // + 20;
    $this->setXY($col1, $this->marginY);
    $currY = $this->marginY;
    $height = $this->getStringHeight("Ciclo Lectivo", $col1Width);
    $this->textBox("Ciclo Lectivo:", $col1Width);
    $this->textBox(":  " . $this->planilla->notas["datos"]["ciclo"]);
    $this->setXY($col1, $currY += $height);

    $this->textBox("Nivel", $col1Width);
    $this->textBox(":  " . $this->planilla->notas["datos"]["nivel"]);
    $this->setXY($col1, $currY += $height);

//    $this->textBox("Fecha de impresión", $col1Width);
//    $this->textBox(":  " . date("d/mY", time()));
//    $this->setXY($col1, $currY+=$height);

    $col1 += 80;
    $currY = $this->marginY;
    $this->setXY($col1, $this->marginY);

    $this->textBox("Tipo de Calificación", $col1Width);
    $this->textBox(":  " . $this->planilla->periodo_nombre);
    $this->setXY($col1, $currY += $height);

    $this->textBox("Curso/División", $col1Width);
    $this->textBox(":  " . $this->planilla->notas["datos"]["anio"]);
    $this->setXY($col1 + $col1Width + 8, $currY);
    $this->textBox($this->planilla->notas["datos"]["division"]);

    $this->planilla->currY = $this->getY() + $height + 3;

    $this->SetFont(PDF_FONT_NAME_MAIN, "", $this->planilla->fontSizeNotasHeader);
    $this->setCellPaddings(2, 2, 0, 0);
    $this->setCellHeightRatio(1.1);

//$this->planilla->currX =$this->marginX;
    $this->planilla->currY += 3;
    $this->planilla->currX = $this->marginX;;
    $this->SetFont(PDF_FONT_NAME_MAIN, "", $this->planilla->fontSizeMateria);
    $this->MultiCell($this->planilla->anchoAlumno, $this->planilla->asignaturasHeaderHeight
     , "Alumno", "TBLR", "L", false, 0, $this->planilla->currX, $this->planilla->currY
     , true, 0, false, true, 0, "B", false);
    $this->planilla->currX += $this->planilla->anchoAlumno;
    $this->setCellPaddings(0, 2, 0, 0);
    $i = 0;
    foreach ($this->planilla->asignaturas as $asignatura) {
      $this->MultiCell($this->planilla->anchoColumnas[$i]
       , $this->planilla->asignaturasHeaderHeight, trim($asignatura), "TBR", "C", false
       , 0, $this->planilla->currX, $this->planilla->currY
       , true, 0, false, true, 0, 'B', false);
      $this->planilla->currX += $this->planilla->anchoColumnas[$i++];
    }

    $this->MultiCell($this->planilla->anchoCondAsis, $this->planilla->asignaturasHeaderHeight, "Inasistencia", "TB", "C", false, 1, $this->planilla->currX, $this->planilla->currY
     , true, 0, false, true, 0, 'B', false);
    $this->planilla->currX += $this->planilla->anchoCondAsis;

    $this->MultiCell($this->planilla->anchoCondAsis, $this->planilla->asignaturasHeaderHeight, "Conducta", "TBLR", "C", false, 1, $this->planilla->currX, $this->planilla->currY
     , true, 0, false, true, 0, 'B', false);
    $this->planilla->currX += $this->planilla->anchoCondAsis;

    $this->MultiCell($this->planilla->anchoMatDes, $this->planilla->asignaturasHeaderHeight, "Materias\Desapr.", "TBLR", "C", false, 1, $this->planilla->currX, $this->planilla->currY
     , true, 0, false, true, 0, 'B', false);

    $this->planilla->currY += $this->planilla->asignaturasHeaderHeight;
    $this->planilla->currX = $this->marginX;
    $this->planilla->headerHeight = $this->planilla->currY - 1;

    $this->SetFont(PDF_FONT_NAME_MAIN, "", $fontSize);
    $this->setCellPaddings($paddings);

    $this->setFont($fontFamily, $fontStyle, $fontSize);
    $this->getCellPaddings($paddings);
  }

  public function Footer()
  {
    $this->SetY(0);
    $this->SetX(0);
  }

}

class Informe extends stdClass
{

  public $pdf;
  public $numeros = array("Cero", "Uno", "Dos", "Tres", "Cuatro", "Cinco", "Seis", "Siete", "Ocho", "Nueve", "Diez");
  public $fontSize, $fontSizeNota, $fontSizeAlumno, $fontSizeDatos, $fontSizeNotasHeader;
  public $periodo_nombre, $paperSize;
  public $col1, $col2, $asignaturas, $alumnoWidth, $CondAsisWidth, $currY, $currX;
  public $headerHeight, $anchoColumnas;
  public $asignaturasHeaderHeight, $MatDesWidth;

  public function __construct($division_id, $logica_periodo_id, $paperSize = "legal")
  {
    $this->paperSize = $paperSize;
    $this->notas = Division::getNotasPorPeriodo2($division_id, $logica_periodo_id);
    // vd2($this->notas);
    $this->asignaturas = $this->notas["asignaturas"];
    $this->periodo_nombre = Helpers::qryScalar("select nombre from logica_periodo where id = $logica_periodo_id");
  }

  public function imprime()
  {
    $this->pdf = new MYPDF("L", "mm", $this->paperSize);
    $this->pdf->marginY = 4;
    $this->pdf->marginX = 2;
    $this->pdf->planilla = $this;
    $this->pdf->SetCreator(PDF_CREATOR);
    $this->pdf->SetAuthor('JP');
    $this->pdf->SetTitle('Planilla Resumen Final');
    $this->pdf->SetSubject('Planilla Resumen Final IAE');
    $this->pdf->SetKeywords('TCPDF, PDF, Planilla');
    $this->pdf->SetPrintHeader(true);
    $this->pdf->SetPrintFooter(false);
    $this->pdf->SetAutoPageBreak(false);
    $this->fontSize = 8;
    $this->fontSizeNota = $this->fontSize + 1;
    $this->fontSizeNotasHeader = $this->fontSize + 1;
    $this->fontSizeDatos = $this->fontSize + 4;
    $this->fontSizeAlumno = $this->fontSize + 1;
    $this->fontSizeMateria = $this->fontSize;
    $this->preparaPdf();
    $this->pdf->Output('Planilla-resumen-final.pdf');
  }

  private function preparaPdf()
  {

    /*
     * Cálculo del ancho de la columna alumnos
     */

    $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", $this->fontSizeAlumno);
    $this->pdf->setCellPaddings(1, 2, 0, 0);
    $this->anchoAlumno = 0;
    if (!isset($this->notas["alumnos"])) {
      $this->notas["alumnos"] = array();
    }
    foreach (array_keys($this->notas["alumnos"]) as $alumnoNombre) {
      $w = $this->pdf->getStringWidth($alumnoNombre);
      $this->anchoAlumno = (($w > $this->anchoAlumno) ? $w : $this->anchoAlumno);
    }
    $this->anchoAlumno += 4; //arreglar y quitar esto
    $this->anchoCondAsis = 15;
    $this->anchoMatDes = 15;
    $this->currX = $this->pdf->marginX;

    /*
     * Cálculo de las diferentes columnas de materias
     */
    $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", $this->fontSizeNotasHeader);
    $this->pdf->setCellPaddings(0, 2, 0, 0);
    $this->pdf->setCellHeightRatio(1.1);
    $height = 0;
    $anchoMaximo = 0;
    $this->anchoColumnas = array();
    $minimo = 14;
    $anchoTotal = 0;
    $i = 0;
    foreach ($this->asignaturas as $asignatura) {
      $p = explode(" ", $asignatura);
      $wm = 0;
      foreach ($p as $word) {
        $w = $this->pdf->getStringWidth(trim($word));
        $w = ($w < $minimo) ? $minimo : $w;
        $wm = ($wm < $w) ? $w : $wm;
        $anchoMaximo = ($w > $anchoMaximo) ? $w : $anchoMaximo;
      }
      $anchoTotal += $wm;
      $this->anchoColumnas[$i++] = $wm;
    }
// vd2($this->anchoColumnas);
    /*
     * Si sobre espacio lo distribuyo entre las columnas de materias
     */
    $dif = (($this->pdf->getPageWidth() -
      $anchoTotal -
      $this->anchoAlumno -
      $this->anchoMatDes -
      $this->pdf->marginX -
      ($this->anchoCondAsis * 2) - $this->pdf->marginX) / count($this->asignaturas));


    $anchoMaximo += $dif;
    /*
     * Distribuyo la diferencia entre las columnas
     * y reemplazo los espacios con CR así queda una palabra por linea
     * en la cabezera de las materias
     */
    for ($index = 0; $index < count($this->asignaturas); $index++) {
      $this->anchoColumnas[$index] += $dif;
      $this->asignaturas[$index] = str_replace(" ", "\n", $this->asignaturas[$index]);
    }

    /*
     * Cálculo de la altura de las cabeceras de las materias
     */
    foreach ($this->asignaturas as $asignatura) {
      $h = $this->pdf->getStringHeight($anchoMaximo, trim($asignatura), false, false, '', 0);
      $height = ($h > $height) ? $h : $height;
    }
    $this->asignaturasHeaderHeight = $height += (($dif > 0) ? 3 : 1);
    $this->pdf->setCellPaddings(0, 0, 0, 0);
    $this->pdf->setCellHeightRatio(1);
    $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", $this->fontSize);

    $this->currX = $this->pdf->marginX;

//    $this->currY += $this->asignaturasHeaderHeight;

    $this->pdf->AddPage();
    foreach ($this->notas["alumnos"] as $alumnoNombre => $notas) {
      /*
       * Imprimo el alumno
       */
      $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", $this->fontSizeAlumno);
      $this->pdf->setCellPaddings(1, 1, 0, 1
      );
      $height = $this->pdf->getStringHeight($this->anchoAlumno, $alumnoNombre, false, true, '', 0);
      $this->chkPageEnd($height);
      $this->currX = $this->pdf->marginX;
      $this->pdf->MultiCell($this->anchoAlumno, $height, "$alumnoNombre", "LBR", "L", false, 1, $this->currX, $this->currY
       , true, 0, false, true, 0, 'C', false);
      /*
       * Imprimo las notas
       */
      $this->pdf->SetFont(PDF_FONT_NAME_MAIN, "", $this->fontSizeNota);
      $this->currX += $this->anchoAlumno;
      $i = 0;
      $notas = array_values($notas["notas"]);
      foreach ($notas as $nota) {
        $fnt = $this->pdf->getFontStyle();
//@todo: que la nota en negrita sea dinámica
        if (
         (is_numeric($nota) and $nota < 4) or (is_string($nota) and strtoupper($nota) == "AU") and
         in_array($this->periodo_nombre, array("Diciembre", "Marzo"))
        ) {
          $this->pdf->SetFillColor(100, 100, 100);
          $this->pdf->setFont(null, "BI");
        } elseif (
         ((is_numeric($nota) and $nota < 7 and !($nota === 0)) or (is_string($nota) and strtoupper($nota) == "AU")) and !in_array($this->periodo_nombre, array("Diciembre", "Marzo"))
        ) {
          $this->pdf->SetFillColor(150, 150, 150);
          $this->pdf->setFont(null, "BI");
        } else {
           $this->pdf->SetFillColor(255, 255, 255);
        }
        $this->pdf->MultiCell($this->anchoColumnas[$i], $height, $nota === 0 ? '':$nota, "BR"
         , "C", true, 1, $this->currX, $this->currY
         , true, 0, false, true, 0, 'M', false);
        $this->pdf->setFont(null, $fnt);
        $this->currX += $this->anchoColumnas[$i++];
      }
      /*
       * Imprimo Asistencia y Conducta
       */
//vd2($this->notas);
//      vd2($this->notas[$alumnoNombre]["inasistencias"]);
      $this->pdf->MultiCell($this->anchoCondAsis, $height
       , $this->notas[$alumnoNombre]["inasistencias"], "B", "C", false
       , 1, $this->currX, $this->currY, true, 0, false, true, 0, 'M', false);
      $this->currX += $this->anchoCondAsis;

      $this->pdf->MultiCell($this->anchoCondAsis, $height
       , $this->notas[$alumnoNombre]["amonestaciones"], "BLR", "C", false
       , 1, $this->currX, $this->currY, true, 0, false, true, 0, 'M', false);
      $this->currX += $this->anchoCondAsis;
      /*
       * SetFont 	( 	$  	family,
        $  	style = '',
        $  	size = null,
        $  	fontfile = '',
        $  	subset = 'default',
        $  	out = true
        )
       */
      $fnt = $this->pdf->getFontStyle();
      $this->pdf->setFont(null, "B");
      $this->pdf->MultiCell($this->anchoMatDes, $height
       , $this->notas[$alumnoNombre]["sumaAsignaturasDesaprobadas"], "BLR", "C"
       , false, 1, $this->currX, $this->currY, true, 0, false, true, 0, 'M', false);
      $this->pdf->setFont(null, $fnt);

      $this->currX += $this->anchoCondAsis;
      $this->currY += $height;
    }
    $this->currX = $this->pdf->marginX;
    $this->chkPageEnd($height);

    $this->currY += 2;
    $this->pdf->MultiCell($this->anchoAlumno, $height, "Cantidad Desaprobados", "TLBR", "L", false, 1, $this->currX, $this->currY
     , true, 0, false, true, 0, 'C', false);
    $this->currX += $this->anchoAlumno;
    $fnt = $this->pdf->getFontStyle();
    $this->pdf->setFont(null, "B");
    $i = 0;
    foreach ($this->notas["asignaturas-desaprobadas"] as $nota) {
      $this->pdf->MultiCell($this->anchoColumnas[$i], $height, $nota, "TBR", "C", false, 1, $this->currX, $this->currY
       , true, 0, false, true, 0, 'M', false);
      $this->currX += $this->anchoColumnas[$i++];
    }
    $this->pdf->setFont(null, $fnt);
    $this->pdf->MultiCell($this->anchoCondAsis, $height, "", "TB", "C", false, 1, $this->currX, $this->currY
     , true, 0, false, true, 0, 'M', false);
    $this->currX += $this->anchoCondAsis;

    $this->pdf->MultiCell($this->anchoCondAsis, $height, "", "TBLR", "C", false, 1, $this->currX, $this->currY
     , true, 0, false, true, 0, 'M', false);
    $this->currX += $this->anchoCondAsis;

    $this->pdf->MultiCell($this->anchoMatDes, $height, "", "TBLR", "C", false, 1, $this->currX, $this->currY
     , true, 0, false, true, 0, 'M', false);
    $this->currX = $this->pdf->marginX;
    $this->chkPageEnd($height);
    $this->currY += $height;
    $this->pdf->MultiCell($this->anchoAlumno, $height, "Porcentaje por materia", "LBR", "L", false, 1, $this->currX, $this->currY
     , true, 0, false, true, 0, 'C', false);
    $this->currX += $this->anchoAlumno;
    $fnt = $this->pdf->getFontStyle();
    $this->pdf->setFont(null, "B");

    $i = 0;
    foreach ($this->notas["asignaturas-desaprobadas"] as $nota) {
      $cantAlu = count($this->notas["alumnos"]);

      $nota = round(($nota * 100) / (($cantAlu == 0) ? 1 : $cantAlu));
      $this->pdf->MultiCell($this->anchoColumnas[$i], $height, $nota . "%", "BR", "C", false, 1, $this->currX, $this->currY
       , true, 0, false, true, 0, 'M', false);
      $this->currX += $this->anchoColumnas[$i++];
    }
    $this->pdf->MultiCell($this->anchoCondAsis, $height, "", "B", "C", false, 1, $this->currX, $this->currY
     , true, 0, false, true, 0, 'M', false);
    $this->currX += $this->anchoCondAsis;

    $this->pdf->MultiCell($this->anchoCondAsis, $height, "", "BLR", "C", false, 1, $this->currX, $this->currY
     , true, 0, false, true, 0, 'M', false);
    $this->currX += $this->anchoCondAsis;

    $this->pdf->MultiCell($this->anchoMatDes, $height, "", "BLR", "C", false, 1, $this->currX, $this->currY
     , true, 0, false, true, 0, 'M', false);
    $this->pdf->setFont(null, $fnt);
  }

  private function chkPageEnd($height)
  {
    if (($this->currY + $height + $this->pdf->marginY + 2) >= $this->pdf->getPageHeight()) {
      $this->pdf->AddPage();
      $this->currY = $this->pdf->marginY + $this->headerHeight;
    }
  }

}

$i = new Informe($division_id, $logica_periodo_id, $paperSize);
$i->imprime();
?>