<?php

  include("informe.php");
  $fechaSaldo = date("Y-m-d", mystrtotime($fecha_desde) - 1);
  $fecha_desde = date("Y/m/d", mystrtotime($fecha_desde));
  $fecha_hasta = date("Y/m/d", mystrtotime($fecha_hasta)) . " 23:59:59";
  $select = "
            select  '' as comprob,  '' as Numero,'Saldo' as detalle, -1 as id,  \"$fechaSaldo\" as fecha,
                          saldo($socio_id, \"$fechaSaldo\") as total
            union
            select  c.nombre as comprob,  concat(LPAD(d.sucursal, 4, '0'),'-',LPAD(d.numero, 8, '0')) as Numero, 
                         d.detalle,d.id, d.fecha_valor as fecha, (d.total * c.signo_cc) as total
                from doc d
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
                    left join socio s on s.id = d.socio_id
                    left join proveedor a on a.id = s.proveedor_id
                where d.fecha_valor BETWEEN \"$fecha_desde\" and \"$fecha_hasta\"
                            and s.id = $socio_id and d.anulado = 0
                order by 4,2
        ";
//vd(str_replace("\\", "", $select));
  $data = Helpers::qryAll($select);
//foreach ($data as $key => $row) {
//    $doc_id = $row["id"];
//    $data[$key]["detail"] = Helpers::qryAll("
//        select t.nombre as tipo_valor, v.numero as NÃºmero,  v.obs, b.nombre as banco, d.nombre as destino, 
//                    ch.descripcion as chequera, v.importe
//                from doc_valor v
//                    inner join doc_valor_tipo t on t.id = v.tipo
//                    left join banco b on b.id = v.banco_id
//                    left join destino d on d.id = v.Destino_id
//                    left join chequera ch on ch.id = v.chequera_id
//                where v.Doc_id =  $doc_id
//    ");
//}
  $proveedor = Helpers::qryScalar("
    select razon_social as proveedor 
        from proveedor a
            inner join socio s on s.Proveedor_id = a.id
        where s.id = $socio_id
");
  $colsDef = array(
      "id" => array("visible" => false),
      "sucursal" => array("visible" => false),
      "numero" => array("visible" => false),
      "fecha" => array("date" => "d/m/Y", "width" => 20),
      "detalle" => array("width" => 48),
      "total" => array("debeHaber" => "db", "align" => "R", "sum" => true),
  );

  $colsDefDetail = array(
      //"tipo_valor" => array("width" => 50, "mb_convert_case"=>true, "strtolower"=>true),
      //"importe" => array("currency", "align" => "R"),
  );

  $options = array(
      "data" => $data,
      "marginY" => 4,
      "marginX" => 2,
      "title" => "Cta. Cte. con valores entre el " . date("d/m/Y", mystrtotime($fecha_desde)) . " y el " . date("d/m/Y", mystrtotime($fecha_hasta)),
      "title2" => $proveedor,
  );
  $i = new Informe($options, $colsDef, $colsDefDetail);
  $i->render();
  