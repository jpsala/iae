<?php
  $anioMasMes = date("Ym",mystrtotime($fecha_desde));
  $dia = date("d",mystrtotime($fecha_desde));
  $quincena = $dia == 1 ? 1:2;
  $archivo = "AR-30639390469-$anioMasMes$quincena-6-LOTE1_.txt";
  header('Content-Type: application/text');
  header('Content-Disposition: filename=' . $archivo);
  $fecha_desde = date("Y/m/d", mystrtotime($fecha_desde));
  $fecha_hasta = date("Y/m/d", mystrtotime($fecha_hasta)) . " 23:59:59";
  $select = "
    select case
          when s.id then p.cuit
            else dv.cuit
          end as cuit, '0001' as sucursal, 'O' as tipo, 'C' as letra, d.numero, d.total,
          dv.importe as retencion, date_format(d.fecha_valor, '%d/%m/%Y') as fecha
    from  doc_valor dv
          inner join doc d on d.id = dv.doc_id
          left join socio s on s.id = d.socio_id
          left join proveedor p on p.id = s.proveedor_id
    where d.fecha_valor between '$fecha_desde' and '$fecha_hasta'
          and dv.tipo = 7 AND d.activo AND ! d.anulado and (dv.retencion_estado is null
          OR dv.retencion_estado = 2)
  ";
  $lineas = '';
  // $EOL = '<br>';
  $data = Helpers::qryAll($select);
  foreach ($data as $row) {
      $linea = trim(cuit($row["cuit"]))
              . $row["fecha"]
              . $row["sucursal"]
              . str_pad($row["numero"], 8, "0", STR_PAD_LEFT)
              . str_pad($row["retencion"], 11, "0", STR_PAD_LEFT)
              . 'a'
              . EOL;
      $lineas.=$linea;
  }
echo trim($lineas);

//27-31186689-9/27311866899
//20-20372876-0/20203728760
function cuit($cuit){
  $pos = strpos($cuit,'-');
  $retCuit = $cuit;
  if(!$pos){
    $retCuit = substr($cuit, 0, 2)
      .'-'
      . substr($cuit, 2, 8)
      .'-'
      . substr($cuit, 10, 1);
  }
  return $retCuit;
}
