<?php

include("informe.php");
$fecha_desde = date("Y/m/d", mystrtotime($fecha_desde));
$fecha_hasta = date("Y/m/d", mystrtotime($fecha_hasta)) . " 23:59:59";

if ($solo_saldo == "checked") {
    $select = "
            select  a.codigo, a.nombre_fantasia as nombre , s.id
                from doc d
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
                    left join socio s on s.id = d.socio_id
                    left join proveedor a on a.id = s.proveedor_id
                where d.anulado = 0 and s.tipo = 'P'
                group by a.codigo, a.nombre_fantasia, s.id 
                having Sum(d.total * c.signo_cc) <> 0 order by nombre";
} else {
    $select = "
            select  p.id as codigo, p.nombre_fantasia as nombre , s.id
                from proveedor p
                left join socio s on s.Proveedor_id = p.id
                order by nombre";
}

$data = Helpers::qryAll($select);

foreach ($data as $key => $row) {
    $socio_id = $row["id"];
    $proveedor_nombre = $row["codigo"];
    $proveedor_codigo = $row["nombre"];
    $data[$key]["saldo_anterior"] = Yii::app()->db->createCommand("                  
                    select Sum(d.total * c.signo_cc) as saldo_anterior
                    from doc d
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
                    left join socio s on s.id = d.socio_id
                    left join proveedor a on a.id = s.proveedor_id
                    where d.fecha_creacion <= \"$fecha_desde\" and 
                    s.id = $socio_id and d.anulado = 0 and d.activo = 1")->queryScalar();

    $data[$key]["total_debitos"] = Yii::app()->db->createCommand("
                    select Sum(d.total * c.signo_cc) as total_debitos
                    from doc d
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
                    left join socio s on s.id = d.socio_id
                    left join proveedor a on a.id = s.proveedor_id
                    where d.fecha_creacion BETWEEN \"$fecha_desde\" and \"$fecha_hasta\" and 
                    s.id = $socio_id and d.anulado = 0 and c.signo_cc = 1 and d.activo = 1")->queryScalar();

    $data[$key]["total_creditos"] = Yii::app()->db->createCommand("
                    select Sum(d.total * c.signo_cc) as total_debitos
                    from doc d
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
                    left join socio s on s.id = d.socio_id
                    left join proveedor a on a.id = s.proveedor_id
                    where d.fecha_creacion BETWEEN \"$fecha_desde\" and \"$fecha_hasta\" and 
                    s.id = $socio_id and d.anulado = 0 and c.signo_cc = -1 and d.activo = 1")->queryScalar();

    $data[$key]["saldo"] = Yii::app()->db->createCommand("select Sum(d.total * c.signo_cc)*-1 as saldo
                from doc d
                    inner join talonario t on t.id = d.talonario_id
                    inner join comprob c on c.id = t.comprob_id
                    left join socio s on s.id = d.socio_id
                    left join proveedor a on a.id = s.proveedor_id
                where d.anulado = 0 and s.id = $socio_id  and d.activo = 1")->queryScalar();
}

$colsDef = array(
        "id" => array("visible" => false),
        "proveedor_codigo" => array("visible" => true),
        "proveedor_nombre" => array("visible" => true),
        "saldo_anterior" => array("width" => 30, "currency" => true, "align" => "R", "sum" => true),
        "total_debitos" => array("width" => 30, "currency" => true, "align" => "R", "sum" => true),
        "total_creditos" => array("width" => 30, "currency" => true, "align" => "R", "sum" => true),
        "saldo" => array("width" => 30, "currency" => true, "align" => "R", "sum" => true),
);

$colsDefDetail = array(
);

$options = array(
        "data" => $data,
        "marginY" => 4,
        "marginX" => 10,
        "title" => "Balance de Saldos Proveedores entre el " . date("d/m/Y", mystrtotime($fecha_desde)) . " y el " . date("d/m/Y", mystrtotime($fecha_hasta)),
        "title2" => "",
);
$i = new Informe($options, $colsDef, $colsDefDetail);
$i->render();
?>