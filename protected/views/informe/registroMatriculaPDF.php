<?php

include("RegistroMatriculaPDF.Class.php");
$pdf = new RegistroMatriculaPDF("L");
$pdf->SetMargins(4, 24, 5, true);
$pdf->SetAutoPageBreak(true, 4);

if(count($data) == 0){
	echo "No hay datos";
	return false;
}
//vd($data);
$pdf->AddPage();
$pdf->setY(23.6);
$margins = $pdf->getMargins();
$orden = 1;
//vd($data);
$pdf->writeHTML($data);
$pdf->output();
