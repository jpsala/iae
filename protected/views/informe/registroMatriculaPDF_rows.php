<?php
$data = Helpers::qryAll("
	SELECT '' as fecha_inscripcion, a.matricula, 
			left(CONCAT(a.apellido , ', ', a.nombre),20) AS nombres, date_format(a.fecha_nacimiento,'%d/%m/%y') AS fecha_nacimiento, 
			DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(a.fecha_nacimiento)), '%Y')+0 AS edad,
			a.sexo, left(p2.nombre,4) AS nacionalidad, a.numero_documento, 
			'' AS vacuna_fecha, '' AS vacuna_result, '' AS buco_fecha, '' AS buco_result,
			CONCAT(p1.apellido, ', ', p1.nombre) AS pariente_nombre, left(p.nombre,4) AS nacionalidad_pariente,
			lower(left(p1.profesion,10)) AS pariente_profesion, pariente_domicilio(p1.id) as pariente_domicilio, 
			p1.telefono_casa AS telefono, '' AS egreso, '' AS observaciones
	 FROM alumno a 
		INNER JOIN alumno_division ad ON a.id = ad.Alumno_id /*and ad.activo*/ 
		INNER JOIN alumno_estado ae ON a.estado_id = ae.id AND ae.activo_edu
		INNER JOIN pariente p1 ON a.vive_con_id = p1.id
		INNER JOIN localidad l ON p1.lugar_nacimiento = l.id
		INNER JOIN pais p ON l.Pais_id = p.id
		INNER JOIN localidad l1 ON a.localidad_id = l1.id
		INNER JOIN pais p2 ON l1.Pais_id = p2.id
	where ad.division_id = $division_id
	");
$colsModel = array(
	"fecha_inscripcion" => array("title" => "", "width" => "42", "format" => null, "align"=>"left"),
	"matricula" => array("title" => "", "width" => "37", "format" => null, "align"=>"center"),
	"nombres" => array("title" => "", "width" => "33mm", "format" => null, "align"=>"left"),
	"fecha_nacimiento" => array("title" => "", "width" => "12mm", "format" => null, "align"=>"left"),
	"edad" => array("title" => "", "width" => "5mm", "format" => null, "align"=>"left"),
	"sexo" => array("title" => "", "width" => "5mm", "format" => null, "align"=>"left"),
	"nacionalidad" => array("title" => "", "width" => "5mm", "format" => null, "align"=>"left"),
	"numero_documento" => array("title" => "", "width" => "15mm", "format" => null, "align"=>"left"),
	"vacuna_fecha" => array("title" => "", "width" => "12mm", "format" => null, "align"=>"left"),
	"vacuna_result" => array("title" => "", "width" => "8mm", "format" => null, "align"=>"left"),
	"buco_fecha" => array("title" => "", "width" => "12mm", "format" => null, "align"=>"left"),
	"buco_result" => array("title" => "", "width" => "9.5mm", "format" => null, "align"=>"left"),
	"pariente_nombre" => array("title" => "", "width" => "41mm", "format" => null, "align"=>"left"),
	"nacionalidad_pariente" => array("title" => "", "width" => "6mm", "format" => null, "align"=>"left"),
	"pariente_profesion" => array("title" => "", "width" => "16.4mm", "format" => null, "align"=>"left"),
	"pariente_domicilio" => array("title" => "", "width" => "17mm", "format" => null, "align"=>"left"),
	"telefono" => array("title" => "", "width" => "13mm", "format" => null, "align"=>"left"),
	"egreso" => array("title" => "", "width" => "13mm", "format" => null, "align"=>"left"),
	"observaciones" => array("title" => "", "width" => "26.5mm", "format" => null, "align"=>"left"),
);
$fields = array_keys($data[0]);
$orden = 1;
?>
<style>
	tr, td{max-height:10px}
	.alto{max-height: 10px; height: 10px}
	.borde{border: #000 solid thin;}
	.orden{width: 30px; text-align: center}
	.cell{font-size: 7px}
<?php foreach ($fields as $fldName): ?>
.<?php echo $fldName;?>{
	width:<?php echo $colsModel[$fldName]["width"];?>;
	text-align:<?php echo isset($colsModel[$fldName]["align"])?$colsModel[$fldName]["align"]:"left";?>;
}
<?php endforeach; ?>
</style>
<table cellpadding="1.5">
	<?php foreach ($data as $row):
		?>
		<tr>
			<td class="orden borde"><?php echo $orden++;?></td>
			<?php foreach ($fields as $fldName): ?>
				<td class="<?php echo $fldName; ?> borde alto cell">
					<span><?php echo $row[$fldName]; ?></span>
					<!--		if (!isset($colsModel[$fldName])) {
								vd($fldName);
							}-->
					<!--$w = $colsModel[$fldName]["width"] !== "" ? $colsModel[$fldName]["width"] : null;-->
					<!--$pdf->p($w, $row[$fldName]);-->
				</td>
			<?php endforeach; ?>
		</tr>
	<?php endforeach; ?>
</table>