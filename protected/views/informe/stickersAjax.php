<?php

class ImprimeEtiquetas extends stdClass {

    private $alumnos;
    private $pdf, $margenX, $margenY, $logica_item_nombre_unico;
    private $numeros = array("Cero", "Uno", "Dos", "Tres", "Cuatro", "Cinco", "Seis", "Siete", "Ocho", "Nueve", "Diez");

    public function __construct($division_id, $logica_periodo_id) {
//$this->alumnos = Division::getNotasPorPeriodo($division_id, $logica_periodo_id);
        $this->alumnos = LogicaActiva::getNotasDivision($division_id, $logica_periodo_id);

        //vd($this->alumnos);
        $this->logica_item_nombre_unico = Helpers::qryScalar("
            select i.nombre_unico
                from logica_item i
                    inner join logica_periodo p on p.id = i.Logica_Periodo_id
                where i.nota_del_periodo = 1 and p.id = $logica_periodo_id
        ");
    }

    public function imprime($logica_periodo_id) {
        $this->pdf = new PDF();
        $this->pdf->SetCreator(PDF_CREATOR);
        $this->pdf->SetAuthor('JP');
        $this->pdf->SetTitle('Stickers');
        $this->pdf->SetSubject('Stickers IAE');
        $this->pdf->SetKeywords('TCPDF, PDF, Stickers');
        $this->pdf->SetPrintHeader(false);
        $this->pdf->SetPrintFooter(false);
        $this->pdf->SetAutoPageBreak(false);
        $this->margenX = 6;
        $this->margenY = 30;
        $parteNro = 1;
        $this->pdf->AddPage();
//        echo "<pre>";
//        var_export($this->alumnos);
//        echo "</pre>";
//        die;
        foreach ($this->alumnos as $alumnoNombre => $alumno) {
//            vd(
//                    $parteNro++, $alumnoNombre, $alumno["datos"], $alumno["conducta"]["amonestacionesFinal"], $alumno["inasistencias"]["totalInasistencias"], $alumno["inasistencias"]["totalInasistencias"]);
//            $conducta = $alumno["conducta"]["amonestacionesFinal"];
//            $conducta = (($alumno['datos']["nivel"]==='EP 1') or ($alumno['datos']["nivel"]==='EP 2'))
//              ?"":$alumno["conducta"]["amonestacionesFinal"];
            $conducta = (($alumno['datos']["nivel"]==='EP 1') or ($alumno['datos']["nivel"]==='EP 2'))
              ?$alumno["conducta"]["amonestaciones"][$this->logica_item_nombre_unico]:$alumno["conducta"]["amonestacionesFinal"];

            if($alumnoNombre ==  "Bedoya, Juan Cruz"){
//              vd2($alumno['inasistencias']['inasistencias'][$logica_periodo_id]);
                // vd($this->logica_item_nombre_unico);
                // vd($this->alumnos);
                // vd($alumno);
                // vd($alumno["conducta"]["amonestaciones"][$this->logica_item_nombre_unico]);
                // vd($alumno);
            }
            $this->imprimeEtiqueta(
                $parteNro++, $alumnoNombre, $alumno["notas"], $alumno["datos"], $conducta,
                $alumno['inasistencias']['inasistencias'][$logica_periodo_id], $alumno["inasistencias"]["totalInasistencias"]
            );
            if ($parteNro == 5) {
                $this->pdf->AddPage();
                $parteNro = 1;
            }
        }
        $this->pdf->Output();
    }

    private function imprimeEtiqueta($parteNro, $alumno, $notasAlumno, $datosAlumno, $amonestaciones
    , $inasistencias, $inasistenciasTotal) {
        $ep = (($datosAlumno["nivel"]=='EP 1') or ($datosAlumno["nivel"]=='EP 2'));
        $headerFontStyle = '';
        $fontSize = 10;
        if ($parteNro == 1) {
            $x = $this->margenX;
            $y = $this->margenY + 5;
            $amonestacionesY = $y + 80;
        } else if ($parteNro == 2) {
            $x = ($this->pdf->getPageWidth() / 2) + 8;
            $y = $this->margenY + 5;
            $amonestacionesY = $y + 80;
        } else if ($parteNro == 3) {
            $y = ($this->pdf->getPageHeight() / 2) + 38;
            $x = $this->margenX;
            $amonestacionesY = $y + 80;
        } else if ($parteNro == 4) {
            $x = ($this->pdf->getPageWidth() / 2) + 8;
            $y = ($this->pdf->getPageHeight() / 2 + 38);
            $amonestacionesY = $y + 80;
        }
        $datosAlumnoStr = $datosAlumno["nivel"]. " ". $datosAlumno["anio"] . " " .
                $datosAlumno["division"] . "     " . $datosAlumno["matricula"]  ;
        $this->pdf->CreateTextBox($alumno, $x, $y, null, null, $fontSize, $headerFontStyle);
        $this->pdf->CreateTextBox($datosAlumnoStr, $x + 50, $y, null, null, $fontSize, $headerFontStyle);
        $y += 10;

        $xNota = $x + (($this->pdf->getPageWidth() / 2) - 40);
        foreach ($notasAlumno as $materia => $nota) {
            $y+=3.7;
            $this->pdf->CreateTextBox(trim($materia), $x, $y, null, null, $fontSize, $headerFontStyle);
            $n = $nota[$this->logica_item_nombre_unico]["nota"];
            $this->pdf->CreateTextBox($n, $xNota, $y, 12, null, $fontSize, $headerFontStyle, "R");
            if ($nota > 0 and $nota < 11) {
                $this->pdf->CreateTextBox("(" . $this->numeros[$nota] . ")", $xNota + 12, $y, 16, null, $fontSize, $headerFontStyle, "L");
            }
        }
        $y = $amonestacionesY + 4;
        $textoConducta = $ep?"":"Amonestaciones";
        $this->pdf->CreateTextBox($textoConducta, $x, $y, null, null, $fontSize, $headerFontStyle);
        $this->pdf->CreateTextBox($amonestaciones === 0 ? "" : $amonestaciones, $xNota, $y, 12, null, $fontSize, $headerFontStyle, "R");
        $y+=4;
        $this->pdf->CreateTextBox("Inasistencias", $x, $y - ($ep?5:0), null, null, $fontSize, $headerFontStyle);
        if($datosAlumno["nivel"]=='ES'){
          $this->pdf->CreateTextBox(($inasistenciasTotal ? $inasistenciasTotal : ""), $xNota, $y - ($ep?5:0), 12, null, $fontSize, $headerFontStyle, "R");
        }  else {
          $this->pdf->CreateTextBox(($inasistencias ? $inasistencias : ""), $xNota, $y - ($ep?5:0), 12, null, $fontSize, $headerFontStyle, "R");
        }
        if($ep){
//      $this->pdf->CreateTextBox(date("d/m/Y", time()), $xNota - 9, $y, 21, null, $fontSize, $headerFontStyle, "R");
          $this->pdf->CreateTextBox('20/12/2017', $xNota - 9, $y, 21, null, $fontSize, $headerFontStyle, "R");
        }
    }

}

$i = new ImprimeEtiquetas($division_id, $logica_periodo_id);
$i->imprime($logica_periodo_id);
?>
