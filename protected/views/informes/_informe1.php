<style type="text/css">
  .datos-label {
    display: inline-block;
    min-width: 72px;
  }
  #datos{text-align: left; margin-bottom: 8px}
</style>
<?php
if ($impresion) {
  $this->renderPartial("/informes/informe.css");
}
?>
<div id="informe">
  <div id="datos">
    <div id="datos-profesor"><span class="datos-label">Profesor:</span><?php echo $profesorNombre; ?></div>
    <div id="datos-asignatura"><span class="datos-label">Materia:</span><?php echo $asignaturaNombre; ?></div>
  </div>
  <table id="main-table">
    <tr class="tr-header">
      <th>Alumno</th>
      <?php foreach ($logicaItems as $li): ?>
        <th class="right-align"><?php echo $li->abrev; ?></th>
      <?php endforeach; ?>
    </tr>
    <?php foreach ($notas as $alumnoDivision_id => $notasAlumno): ?>
      <?php $alumno = AlumnoDivision::model()->with("alumno")->findByPk($alumnoDivision_id)->alumno; ?>
      <tr calls="notas-alumno">
        <td class="td-alumno"><?php echo $alumno->nombreCompleto; ?><input type="hidden" name="alumnos[]" value ="<?php echo $alumno->id; ?>"</td>
        <?php foreach ($notasAlumno as $nota): ?>
          <td class="right-align td-nota ">
            <?php echo $nota->nota; ?>
          </td>
        <?php endforeach; ?>
      </tr>
    <?php endforeach; ?>

  </table>

  <table id="desc-table" class="pagebreak">
    <?php /* @var $division Division */; ?>
    <tr>
      <th>Abrev.</th>
      <th>Descripción</th>
    </tr>
    <?php $row = 0; ?>
    <?php foreach ($logicaItems as $li): ?>
      <tr class="<?php echo ($row++ % 2) ? "tr-even" : ""; ?>">
        <td><?php echo $li->abrev; ?></td><td><?php echo $li->nombre; ?></td>
      </tr>
    <?php endforeach; ?>
  </table>
</div>

<script type="text/javascript">
  var impresion = <?php echo $impresion ? "false" : "true"; ?>;
  if (impresion) {
    $("#imprime").show();
  }
</script>
