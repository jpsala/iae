<?php $this->renderPartial("/informes/head"); ?>
<style type="text/css">
  #imprime {vertical-align: top; margin-top: -1px; display:none}
  .td-nota{ width:20px}
  .td-alumno{width:250px}
  .rec-img{display:none}
  #informe{width: 680px; text-align: center; margin:auto}
  table {width: 100%}
  tr,body{background-color: white !important;
          background:white !important;}
  th, td, caption {padding: 1px 1px 4px 3px !important;font-size: 80%}
  .td-nota, .right-align{text-align: center !important}
  .td-alumno {width: 180px !important;}
  tr:nth-child(2n) {
    background-color: whitesmoke !important;
  }
  .tr-header th{border-bottom: #000 solid thin}
  .pagebreak { page-break-before: always; }
  @page {
    size: auto;   /* auto is the initial value */
    margin: 12px 0 0 0;  /* this affects the margin in the printer settings */
  }
</style>
