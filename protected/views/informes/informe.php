<style type="text/css">
  #imprime {vertical-align: top; margin-top: -1px; display:none}
</style>
<?php
$paramNivel = new ParamNivel(array(
    "controller" => $this,
//   "onchange" => "changeNivel",
        ));
$paramAnio = new ParamAnio(array(
    "controller" => $this,
        ));
$paramDivision = new ParamDivision(array(
    "controller" => $this,
        ));
$paramAsignatura = new ParamAsignatura(array(
    "controller" => $this,
    "onchange" => "asignaturaChange"
        ));
$paramNivel->render();
$paramAnio->render();
$paramDivision->render();
$paramAsignatura->render();
?>
<button onclick="imprime();
      return false;" id="imprime">Imprime</button>

<div id="div-para-notas"></div>
<script type="text/javascript">
  var url = "<?php echo $this->createUrl("nota/$reporteNombre"); ?>";


  function asignaturaChange($o) {
    division_asignatura_id = $o.val();
    $.ajax({
      type: "GET",
      data: {division_asignatura_id: division_asignatura_id},
      url: url,
      success: function(data) {
        $("#div-para-notas").html(data);
        $("#imprime").show();
      },
      error: function(data, status) {
      }
    });
  }
  $("#imprime").button();

  function imprime() {
    window.open("<?php echo $this->createUrl("nota/$reporteNombre"); ?>&division_asignatura_id=" + division_asignatura_id + "&impresion=1");
  }
</script>