<script src="js/knockout.my.js"></script>
<script type="text/javascript">
var main = (function () {
    var urlAreas = "<?php echo $this->createAbsoluteUrl("inicial/areasAjax"); ?>";
    var urlContenidos = "<?php echo $this->createAbsoluteUrl("inicial/contenidosAjax"); ?>";
    var urlSubmit = "<?php echo $this->createAbsoluteUrl("inicial/grabaContenidosAjax"); ?>";
    var urlBorraContenido = "<?php echo $this->createAbsoluteUrl("inicial/borraContenidosAjax"); ?>";
    var urlBorraArea = "<?php echo $this->createAbsoluteUrl("inicial/borraAreaAjax"); ?>";
    var urlSecciones = "<?php echo $this->createAbsoluteUrl("inicial/seccionesAjax"); ?>";
    //var urlDocentes = "<?php //echo $this->createAbsoluteUrl("inicial/docentesJardinAjax");   ?>";
    var recordarGrabar = true;
    var areasAjax, seccionesAjax/*, docentesAjax*/;
    var cambiandoArea = false;
    var vm = {};
    vm.data = ko.observable(new Data());
    vm.dataChanged = ko.observable(false);
    vm.areas = [];
    //vm.docentes = ko.observableArray();
    vm.secciones = ko.observableArray();
    vm.contenidos = ko.observableArray();
    vm.docente = ko.observable();
    vm.areaNueva = ko.observable(new Area({id: -1, nombre: "Area Nueva"}));
    vm.contenidoNuevo = ko.observable(new Contenido({}));
    vm.areaActiva = ko.observable("");
    vm.areaSeleccionada = ko.observable("");
    /*
     * Cambio de Área
     */
    vm.areaActiva.subscribe(function (area) {
        cambiandoArea = true;
        recordarGrabar = true;
        vm.data().areaNombre(area.nombre);
        //vm.data().docente_id(Number(area.docente_id));
        vm.data().docente(area.docente);
        vm.data().seccion_id(Number(area.seccion_id));
        vm.data().area_id(Number(area.id));
        vm.data().especial(area.especial());
        console.log(area);
        getContenidos().done(function () {
            vm.dataChanged(false);
            cambiandoArea = false;
        });
    });

    vm.getChangedContenidos = vm.contenidos.filter(function (contenido) {
        return contenido.changed();
    });

    vm.selectArea = function (area) {
        vm.areaSeleccionada(area);
    };

    vm.templateName = function (area) {
        return vm.areaSeleccionada() === area ? "editTemplate" : "itemTemplate";
    };

    vm.graba = function () {
        var data = ko.toJS({data: vm.data, contenidos: vm.getChangedContenidos});
        $.post(urlSubmit, {
            data: ko.toJS(data),
            contentType: 'application/json',
            type: 'POST'
        }, function (a) {
            if (vm.dataChanged()) {
                //window.location = window.location;
            } else {
                getContenidos(vm.data().area_id);
            }
        });
    };

    vm.getContenidoNuevoOk = function () {
        return vm.contenidoNuevo().nombre() !== "" && vm.contenidoNuevo().texto() !== "";
    };

    vm.addContenidoNuevo = function (o) {
        if (!vm.getContenidoNuevoOk()) {
            return;
        }
        var newContenido = new Contenido({nombre: o.nombre(), texto: o.texto()});
        newContenido.changed(true);
        o.changed(false);
        o.nombre("");
        o.texto("");
        vm.contenidos.push(newContenido);
        getRecordarGrabar();
    };

    vm.setContenidoChanged = function (o) {
        getRecordarGrabar();
        o.changed(true);
    };

    vm.getPuedeGrabar = function () {
        return (vm.getChangedContenidos().length > 0) || vm.dataChanged();
    };

    vm.borraContenido = function (o, el) {
        var force = false;
        var borra = function (o, force) {
            $.get(urlBorraContenido, {contenido_id: o.id(), force: force});
            vm.contenidos.remove(o);
        };
        console.log(o.notas(), Number(o.notas()));
        if (o.notas() > 0 && confirm("El contenido tiene notas cargadas, lo borra junto con ellas")) {
            borra(o, true);
        }
        ;
        if (Number(o.notas()) === 0 && confirm("Borra este contenido?")) {
            borra(o);
        }
    };

    vm.borraArea = function (o, el) {
        if (vm.contenidos().length > 0) {
            alert("Primero debe borrar todos los contenidos");
            return;
        }
        if (confirm("Borra esta área?")) {
            $.get(urlBorraArea, {area_id: vm.data().area_id()}).done(function () {
                window.location = window.location;
            });
        }
    };

    getAreas();
    getSecciones();
    getDocentes();

    /*
     * subscriptions for all vm.data propertyes
     */
    for (key in vm.data()) {
        vm.data()[key].subscribe(function (val) {
            dataChanged(this.toString(), val);
        }, key);
    }
    /*
     * Some property of data has changed
     */
    function dataChanged(propName, val) {
        if (vm.areaActiva() && !cambiandoArea) {
            if (propName === "docente_id" || propName === "seccion_id") {
                vm.areaActiva()[propName] = val;
            }
            vm.dataChanged(true);
            getRecordarGrabar();
        }
    }

    function getAreas() {
        areasAjax = $.getJSON(urlAreas);
        $.when(areasAjax).then(function (areas) {
            vm.areas.pop();
//                vm.areas.removeAll();
            $.each(areas, function (i, o) {
                vm.areas.push(new Area(o));
                //console.log(o);
            });
            vm.areas.push(vm.areaNueva());
        });
        //console.log(vm.areas);
    }

    function getSecciones() {
        seccionesAjax = $.getJSON(urlSecciones);
        $.when(seccionesAjax).then(function (secciones) {
            $.each(secciones, function (i, o) {
                vm.secciones.push(new Seccion(o));
            });
        });
    }

    function getDocentes() {
        //docentesAjax = $.getJSON(urlDocentes);
        //$.when(docentesAjax).then(function(docentes) {
//				$.each(docentes, function(i, o) {
        //vm.docentes.push(new Docente(o));
//				});
        //});
    }

    function getContenidos() {
        contenidosAjax = $.getJSON(urlContenidos, {area_id: vm.areaActiva().id});
        $.when(contenidosAjax).then(function (contenidos) {
            vm.contenidos.removeAll();
            $.each(contenidos, function (i, o) {
                vm.contenidos.push(new Contenido(o));
            });
        });
        return contenidosAjax;
    }

    function Data() {
        this.areaNombre = ko.observable("");
        this.area_id = ko.observable();
        this.seccion_id = ko.observable();
        //this.docente_id = ko.observable();
        this.docente = ko.observable('');
        this.especial = ko.observable();
    }

    function Area(o) {
//            console.log(o);
        this.text = o.nombre || "";
        this.id = o.id || "";
        this.nombre = o.nombre || "";
        this.seccion_id = o.seccion_id || "";
        //this.docente_id = o.docente_id || "";
        this.docente = o.docente || "";
        this.abreviacion = o.abreviacion || "";
        this.especial = ko.observable(o.especial === '1');
        this.materia = ko.observable(o.materia === '1');
    }

    function Seccion(o) {
        this.id = ko.observable(o.id || "");
        this.nombre = ko.observable(o.nombre || "");
    }

    function Docente(o) {
        this.id = o.id || "";
        this.nombre = o.nombre || "";
    }

    function Contenido(o) {
        this.id = ko.observable(o.id || "");
        this.nombre = ko.observable(o.nombre || "");
        this.texto = ko.observable(o.texto || "");
        this.notas = ko.observable(o.notas || "");
        this.changed = ko.observable(false);
    }

    function getRecordarGrabar() {
        if (recordarGrabar) {
            recordarGrabar = false;
            toastr.warning('Recuerde grabar los cambios antes de salir', 'Importante', {timeOut: 6000});
        }
    }

    return {vm: vm};
})();

$("#seccion-select").select2({placeholder: "", width: 200});
//$("#docente-select").select2({placeholder: "Seleccione un docente", width:200});

ko.applyBindings(main.vm);

$(function () {
//        setTimeout(function() {
//            $("#area-select").select2("open");
//        }, 600);
});

$("#contenidos-div").on({
    mouseenter: function () {
        $(this).find(".borrar-contenido").css("display", "inline-block");
    },
    mouseleave: function () {
        $(this).find(".borrar-contenido").css("display", "none");
    }
}, "table tbody tr");


</script>