<link rel="stylesheet" href="js/select2/select2.css"/>
<link rel="stylesheet" href="js/select2/select2-bootstrap.css"/>
<script type="text/javascript" src="js/select2/select2.min.js"></script>
<!--<script src="js/knockout-deferred-updates.min.js"></script>-->
<script src="js/knockout.mapping.js"></script>
<script src="js/knockout-projections.min.js"></script>
<style>
    .nombre-td { width: 200px }
    .nombre-td input { width: 150px; display: inline-block }
    .borrar-contenido { display: none }
    #graba-area { vertical-align: text-top; }
    .add-disable { text-decoration: line-through !important; }
    /*.center{margin-left:auto!important;margin-right: auto !important; float:none !important}*/
    #area-nombre-input, .borra-area { display: inline-block; width: 200px }
    #docente-input{width: 330px; display: inline-block;vertical-align: baseline;}
    #contenidos-div h2{margin-top: 0px}
</style>
<div class="container">
    <h2>Áreas y contenidos</h2>

    <div class="row top10">
        <div class="col-xs-12">
            <div id="area-select" class="form-control select2"
                 data-bind="obj:areaActiva, select2:{ data: areas, placeholder:'Seleccione un Área', width:300}">
            </div>
            <select id="seccion-select" class="form-control"
                    data-bind="value:data().seccion_id, enable: areaActiva() ">
                <option value=''>Para todas las Secciones</option>
                <!-- ko foreach: secciones -->
                <option data-bind="value: id, text: nombre "></option>
                <!-- /ko -->
            </select>
            </div>
            <div class="col-xs-12">
            <input id="area-nombre-input" class="form-control input-sm"
                   data-bind="value: data().areaNombre, valueUpdate: ['input'], enable: areaActiva()"/>
            <textarea id="docente-input" class="form-control input-sm" rows="4"
                      data-bind="value: data().docente, valueUpdate: ['input'], enable: areaActiva()"
                      placeholder="Docentes"></textarea>
            <a style="width:30px"
               data-bind="click: borraArea, visible: data().area_id() && data().area_id() !== '' && data().area_id() !== '-1'"
               class="borra-area btn btn-danger btn-sm" href="#">
                <i class=" fa fa-trash-o"></i></a>

            <div class="col-xs-12">
                <div class="row" style="margin-top: -60px">
                    <label for="especial">especial: </label>
                    <input type="checkbox" data-bind="checked: data().especial" id="especial"/>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <!--{{ko.toJSON(data,0,2)}}-->
        </div>
        <div id="contenidos-div" class="top10 col-xs-12">
            <!-- ko if:data().area_id() !== "" -->
            <h2>Contenidos</h2>

            <div class="row top10">
                <div class="col-xs-10">
                    <table
                        class="table table-bordered table-condensed table-hover table-striped col-xs-6">
                        <thead>
                        </thead>
                        <tbody>
                            <!-- ko foreach:contenidos-->
                            <tr data-bind="event:{click:$root.selectArea}">
                                <td class="nombre-td">
                                    <input class="form-control input-sm"
                                           data-bind="value:nombre, valueUpdate: 'input', event: { input: $parent.setContenidoChanged }"/>
                                    <a data-bind="
									click: $parent.borraContenido, anchorenable: id()"
                                       class="borrar-contenido btn btn-danger btn-sm pull-right"
                                       href="#">
                                        <i class=" fa fa-trash-o"></i></a>
                                </td>
                                <td><textarea rows="6" class="form-control input-sm"
                                              data-bind="value:texto, valueUpdate: 'input', event: { input: $parent.setContenidoChanged }"></textarea>
                                </td>
                            </tr>
                            <!-- /ko -->
                            <tr data-bind="with: contenidoNuevo ">
                                <td class="nombre-td"><input class="form-control input-sm"
                                                             data-bind="enable: $parent.areaActiva(),value:nombre, valueUpdate: 'input', event: { change: $parent.contenidoNuevoChanged }"
                                                             placeholder="Nombre"/></td>
                                <td>
                                    <textarea
                                        rows="5"
                                        class="form-control input-sm"
                                        data-bind="
									enable: $parent.areaActiva(),
									value:texto, valueUpdate: 'input', 
									event: { change: $parent.contenidoNuevoChanged } "
                                        placeholder="Texto">
                                    </textarea>
                                    <a data-bind="click: $parent.addContenidoNuevo,
									anchorenable:$parent.getContenidoNuevoOk()"
                                       class="btn btn-link btn-sm pull-right" href="#">
                                        <i class="fa fa-lg fa-plus-circle pull-left fa-border"></i>Agregar</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row top10">
                <div class="col-xs-10">
                    <button
                        id="graba-btn" type="button" class="btn btn-primary pull-right"
                        disabled="true"
                        data-bind="enable:getPuedeGrabar(),click:graba">
                        Grabar
                    </button>
                </div>
            </div>
            <!-- /ko -->
        </div>
    </div>
</div>
<!--<pre data-bind="text: ko.toJSON($data, null, 2)"></pre>-->
<?php
include("areaIndex.js.php");
