<?php
if (!$area_id) {
    return;
}

$ciclo_id = Ciclo::getActivoId();

$alumnos = Helpers::qryAll("
		SELECT ad.alumno_id, ad.id AS alumno_division_id, CONCAT(a.apellido, ', ', a.nombre) AS alumno,
					 a.matricula
			 FROM alumno_division ad
				 INNER JOIN alumno a ON ad.Alumno_id = a.id and a.activo
                 INNER JOIN alumno_estado ae ON a.estado_id = ae.id
			 WHERE ad.activo and !ad.borrado and ad.division_id = $division_id and ad.ciclo_id = $ciclo_id
			       and ! ae.ingresante AND ae.activo_edu
			 ORDER BY a.apellido, a.nombre
    ");

Helpers::qry("select ia.nombre, u.nombre as docente
		from inicial_area ia
				left join user u on u.id = ia.docente_id
		where ia.id = $area_id");

list($area_nombre, $especial, $docente_nombre) = Helpers::qryDataRow("
		select ia.nombre as area_nombre, ia.especial, case when u.id is not null then u.nombre else 'No asignado' end as docente_nombre
				from inicial_area ia
						left join user u on u.id = ia.docente_id
		where ia.id = $area_id");
// $wherePeriodos = $especial ? " where tipo <> 'f'" : "";
$wherePeriodos = " where tipo <> 'f'";
$periodos = Helpers::qryAll("select id, nombre, tipo from inicial_periodo $wherePeriodos");
$subPeriodos = Helpers::qryAll("SELECT id, nombre FROM inicial_sub_periodo");
$contenidos = Helpers::qryAll("
        select ia.id, ia.texto from inicial_materia ia where ia.area_id = $area_id"
);
foreach ($alumnos as $key => $a) {
    $alumno_id = $a["alumno_id"];
    $select = "
    SELECT in1.id as nota_id, in1.tildado, in1.periodo_id, in1.sub_periodo_id, in1.materia_id,
           p.tipo, in1.texto
        FROM inicial_materia im
        INNER JOIN inicial_nota in1 ON in1.materia_id = im.id
        INNER JOIN alumno_division ad on ad.id = in1.alumno_division_id and
                    ad.division_id = $division_id and ad.alumno_id = $alumno_id and ad.activo
        INNER JOIN alumno a ON ad.Alumno_id = a.id
        INNER JOIN alumno_estado ae ON a.estado_id = ae.id
        inner join inicial_periodo p on p.id = in1.periodo_id
        left join inicial_area ar on ar.id = im.area_id
    WHERE NOT ae.ingresante AND ae.activo_edu
			";
    $tmpNotas = Helpers::qryAll($select);
    $notas = array();
    foreach ($tmpNotas as $n) {
        $notas[$n["periodo_id"]][$n["materia_id"]]["texto"] = $n["texto"];
        $notas[$n["periodo_id"]][$n["sub_periodo_id"]][$n["materia_id"]]["nota"] = $n["texto"];
        $notas[$n["periodo_id"]][$n["sub_periodo_id"]][$n["materia_id"]]["nota_id"] = $n["nota_id"];
    }

    $tmpObservaciones = Helpers::qryAll("
			SELECT o.id as nota_id, o.texto, o.periodo_id, o.area_id
				FROM inicial_observacion o
					INNER JOIN alumno_division ad on ad.id = o.alumno_division_id and
										ad.division_id = $division_id and ad.alumno_id = $alumno_id
				where o.area_id = $area_id
			");
    foreach ($tmpObservaciones as $n) {
        $notas[$n["periodo_id"]]["observaciones"] = $n["texto"];
        $notas[$n["periodo_id"]]["obs_id"] = $n["nota_id"];
    }
    foreach ($periodos as $p) {
        if (!isset($notas[$p["id"]]["observaciones"])) {
            $notas[$p["id"]]["observaciones"] = "";
            $notas[$p["id"]]["obs_id"] = "-1";
        }
        if (!isset($notas[$p["id"]]["texto"])) {
            $notas[$p["id"]]["texto"] = "";
            $notas[$p["id"]]["nota_id"] = "-1";
        }
    }
    $tmpfirmas = Helpers::qryAll("
			SELECT f.id as firma_id, f.docente, f.firmado, f.periodo_id, f.area_id
				FROM inicial_firma f
					INNER JOIN alumno_division ad on ad.id = f.alumno_division_id and
                               ad.division_id = $division_id and ad.alumno_id = $alumno_id
				where f.area_id = $area_id
			");
    foreach ($tmpfirmas as $n) {
        $notas[$n["periodo_id"]]["firma"][$n["docente"]]["firmado"] = $n["firmado"];
        $notas[$n["periodo_id"]]["firma"][$n["docente"]]["firma_id"] = $n["firma_id"];
    }
    foreach ($periodos as $p) {
        foreach (array("paola", "lorena") as $docente) {
            if (!isset($notas[$p["id"]]["firma"][$docente]) or $notas[$p["id"]]["firma"][$docente] == 0) {
                $notas[$p["id"]]["firma"][$docente]["firmado"] = "0";
                $notas[$p["id"]]["firma"][$docente]["firma_id"] = "-1";
            }
        }
    }
    foreach ($periodos as $p):
        foreach ($subPeriodos as $sp):
            foreach ($contenidos as $cont):
                if (!isset($notas[$p["id"]][$sp["id"]][$cont["id"]]["nota"])) {
                    $notas[$p["id"]][$sp["id"]][$cont["id"]]["nota"] = "";
                    $notas[$p["id"]][$sp["id"]][$cont["id"]]["nota_id"] = "-1";
                }
            endforeach;
        endforeach;
    endforeach;
    $alumnos[$key]["notas"] = $notas;
}
// vd2($alumnos);
?>
<h2 style="display:inline-block" class="top10"><?php echo $area_nombre; ?></h2>
<h4 style="display: inline; margin-left:8px">(Docente: <?php echo $docente_nombre; ?>
	<h4 style="display: inline; margin-left:8px">(Especial : <?php echo $especial === '1' ? 'Si' : 'No'; ?>
		)</h4>
	<?php foreach ($alumnos as $alumno): ?>
		<a class="subir" href="#inicio-section">Subir</a>
		<h3><?php echo $alumno["alumno"]; ?></h3>
		<table class="table table-bordered table-condensed table-hover table-striped top10">
			<thead>
			<tr>
				<th class="nombre-td">Se ha trabajado con el alumno en:</th>
				<?php foreach ($periodos as $p): ?>
					<th colspan="<?php echo count((!$especial and $p['tipo'] != 'f') ? 1 : $subPeriodos); ?>">
						<?php echo $p["nombre"]; ?>
					</th>
				<?php endforeach;?>
			</tr>
			<tr>
				<th class="nombre-td"></th>
				<?php foreach ($periodos as $p): ?>
					<?php if (!$especial and ($p['tipo'] == 'b' or $p['tipo'] == 't')): ?>
						<th colspan="1"></th>
					<?php endif;?>
				<?php endforeach;?>
			</tr>
			</thead>
			<?php foreach ($contenidos as $cont): ?>
				<tr>
					<td class="nombre-td"><?php echo $cont["texto"]; ?></td>
					<?php foreach ($periodos as $p): ?>
						<?php if (!$especial and $p['tipo'] == "t"): ?>
							<td class="nota-td">
				<textarea cols="20" rows="4" class="nota" tipo="nota"
									nota_id="<?php echo $alumno["notas"][$p["id"]][$sp["id"]][$cont["id"]]["nota_id"]; ?>"
									periodo_id="<?php echo $p["id"]; ?>"
									area_id="<?php echo $area_id; ?>"
									alumno_division_id="<?php echo $alumno["alumno_division_id"]; ?>"
									materia_id="<?php echo $cont["id"]; ?>"
									periodo_id="<?php echo $p["id"]; ?>"
									sub_periodo_id="<?php echo $sp["id"]; ?>"
				><?php echo isset($alumno["notas"][$p["id"]][$cont["id"]]["texto"]) ? $alumno["notas"][$p["id"]][$cont["id"]]["texto"] : ""; ?></textarea>
							</td>
						<?php elseif (!$especial and $p['tipo'] == "b"): ?>
							<td class="nota-td">
								<?php $checked = $alumno["notas"][$p["id"]][$sp["id"]][$cont["id"]]["nota"] == "1" ? " checked " : "";?>
								<input type="checkbox" class="nota" tipo="nota"
											 nota_id="<?php echo $alumno["notas"][$p["id"]][$sp["id"]][$cont["id"]]["nota_id"]; ?>"
											 periodo_id="<?php echo $p["id"]; ?>"
											 sub_periodo_id="<?php echo $sp["id"]; ?>"
											 materia_id="<?php echo $cont["id"]; ?>"
											 area_id="<?php echo $area_id; ?>"
											 alumno_division_id="<?php echo $alumno["alumno_division_id"]; ?>"
									<?php echo $checked; ?>/>
							</td>
						<?php elseif ($especial or $p['tipo'] == "f"): ?>
							<!--<?php foreach ($subPeriodos as $sp): ?>
								<td class="nota-td">
									<?php $checked = $alumno["notas"][$p["id"]][$sp["id"]][$cont["id"]]["nota"] == "1" ? " checked " : "";?>
									<input type="checkbox" class="nota" tipo="nota"
												 nota_id="<?php echo $alumno["notas"][$p["id"]][$sp["id"]][$cont["id"]]["nota_id"]; ?>"
												 periodo_id="<?php echo $p["id"]; ?>"
												 sub_periodo_id="<?php echo $sp["id"]; ?>"
												 materia_id="<?php echo $cont["id"]; ?>"
												 area_id="<?php echo $area_id; ?>"
												 alumno_division_id="<?php echo $alumno["alumno_division_id"]; ?>"
										<?php echo $checked; ?>/>
								</td>
							<?php endforeach;?>-->
						<?php endif;?>
					<?php endforeach;?>
				</tr>
			<?php endforeach;?>
			<tr>
				<th>Firmas</th>
				<?php foreach ($periodos as $p): ?>
					<th colspan="<?php echo count((!$especial and $p['tipo'] != 'f') ? 1 : $subPeriodos); ?>">
						<?php foreach (array("paola", "lorena") as $docente): ?>
							<div style="width:48%;display:inline-block">
								<?php $checked = $alumno["notas"][$p["id"]]["firma"][$docente]["firmado"] == "1" ? " checked " : "";?>
								<?php echo $docente; ?>:&nbsp;
								<input type="checkbox" class="nota"
											 tipo="firma"
											 firma_id="<?php echo $alumno["notas"][$p["id"]]["firma"][$docente]["firma_id"]; ?>"
											 periodo_id="<?php echo $p["id"]; ?>"
											 area_id="<?php echo $area_id; ?>"
											 alumno_division_id="<?php echo $alumno["alumno_division_id"]; ?>"
											 firma=<?php echo $docente; ?>
											 <?php echo $checked; ?>
											 style="vertical-align: sub;"/>
							</div>
						<?php endforeach;?>
					</th>
				<?php endforeach?>

			</tr>

			</tbody>
		</table>
	<?php endforeach;?>
	<script>
		var especial = <?php echo $especial; ?>;
	</script>
