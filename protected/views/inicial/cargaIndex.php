<?php
    $nivel_id         = Nivel::NIVEL_INICIAL;
    $secciones        = Anio::model()->findAll("nivel_id=$nivel_id");
    $seccionesOptions = array(
        "class"       => "form-control",
        "empty"       => "",
        "placeholder" => "Sección"
    );
    $seccionesData    = CHtml::listData($secciones, "id", "nombre");
    $selSecciones     = CHtml::dropDownList("seccion-select", null, $seccionesData, $seccionesOptions);
?>

<style>
    .subir {
        float: right
    }

    textArea {
        width: 100%
    }

    .nombre-td {
        width: 150px !important;
    }

    .nota-td {
        text-align: center;
        vertical-align: middle
    }

    ;
    .nota {
        width: 100%;
        vertical-align: middle
    }

    #secciones-select {
        width: 250px;
    }

    /*.select2-container.form-control {height: auto !important;}*/
    .table > thead > tr > th {
        background-color: #F5F5F5;
        text-align: center;
        vertical-align: middle;
    }
</style>

<!--<link href="js/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>-->
<link rel="stylesheet" href="js/select2/select2.css"/>
<link rel="stylesheet" href="js/select2/select2-bootstrap.css"/>
<script type="text/javascript" src="js/select2/select2.min.js"></script>
<section id="inicio-section"></section>
<div class="container-fluid">
    <h2>Evaluación narrada</h2>

    <div class="row top10">
        <div class="col-xs-12">
            <?php echo $selSecciones; ?>
            <select id="division-select">
                <option></option>
            </select>
            <select id="area-select">
                <option></option>
            </select>
            <button id="graba-btn" type="button" class="btn btn-default" disabled="true"
                    onclick="return grabaNotas();">Grabar
            </button>
        </div>
    </div>
    <div id="carga-div"></div>
</div>

<script type="text/javascript">
    var division_id, area_id;
    var recordarGrabar = true;

    //toastr.options = {"closeButton": true, "timeOut": "10000"};

    $("#carga-div")
        .on("change", ".nota", function (e) {
            $(this).addClass("changed");
            $("#graba-btn").removeAttr("disabled");
            recordar_grabar();
        });

    $("#seccion-select").select2({allowClear: true, width: 200})
        .on("change", function (e) {
            var anio_id = $(e.target).val();
            $.ajax({
                type: "GET",
                data: {anio_id: anio_id},
                url: "<?php echo $this->createUrl("inicial/divisionesAjax"); ?>",
                success: function (data) {
                    $("#division-select").html(data);
                    $("#division-select").select2("val", "");
                    if (anio_id) {
                        $("#division-select").select2("open");
                    }
                },
                error: function (data, status) {
                }
            });
            $.ajax({
                type: "GET",
                data: {anio_id: anio_id},
                url: "<?php echo $this->createUrl("inicial/areasSelectAjax"); ?>",
                success: function (data) {
                    $("#area-select").html(data);
                    console.log(data);
                    //$("#area-select").select2("val", "");
                },
                error: function (data, status) {
                }
            });
        });

    $("#division-select").select2({allowClear: true, placeholder: "División", width: 200})
        .on("change", function (e) {
            division_id = $(e.target).val();
            $("#area-select").select2("val", "");
            if (division_id) {
                $("#area-select").select2("open");
            }
        });

    $("#area-select").select2({placeholder: "Área", allowClear: true, width: 200})
        .on("change", function (e) {
            area_id = $(e.target).val();
            traeNotas();
        });

    /*
     * Init
     */
    $("#seccion-select").select2("open");
    toastr.info('Usar la opción control-f para buscar palabras o frases dentro del sistema (apellidos, nombres, notas, períodos, etc)', 'Recuerden...', {timeOut: 6000});

    function traeNotas() {
        $.ajax({
                type: "GET",
                //      dataType:"json",
                data: {division_id: division_id, area_id: area_id},
                url: "<?php echo $this->createUrl("inicial/notasAjax"); ?>",
                success: function (data) {
                    $("#carga-div").html(data);
                },
                error: function (data, status) {
                }
            }
        );
    }

    function grabaNotas() {
        var data = new Array();
        $("table tbody tr  .changed").each(function (i, e) {
            var o = {};
            o.area_id = $(e).attr("area_id");
            o.periodo_id = $(e).attr("periodo_id");
            o.sub_periodo_id = $(e).attr("sub_periodo_id") || "";
            o.materia_id = $(e).attr("materia_id") || -1;
            o.nota_id = $(e).attr("nota_id") || -1;
            o.obs_id = $(e).attr("obs_id") || -1;
            o.alumno_division_id = $(e).attr("alumno_division_id");
            o.nota = $(e).is(":checked") ? 1 : 0;
            o.tipo = $(e).attr("tipo");
            o.firma = $(e).attr("firma");
            o.firma_id = $(e).attr("firma_id");
            o.obs = $(e).val() || "";
			o.texto = $(e).val() || "";
            data.push(o);
        });
        jQuery.ajax({
            type: 'POST',
            url: "<?php echo $this->createUrl("inicial/grabaNotas"); ?>",
            data: {notas: data, especial: especial},
            success: function (data) {
                toastr.success(data + ' notas fueron grabadas', 'Correcto');
                $("#graba-btn").attr("disabled", "");
                traeNotas();
            }
        });
    }

    recordar_grabar = function () {
        if (recordarGrabar) {
            recordarGrabar = false;
            toastr.warning('Recuerde grabar los cambios antes de salir', 'Importante', {timeOut: 6000});
        }
    }

</script>