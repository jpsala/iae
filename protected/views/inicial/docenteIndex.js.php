<script  type="text/javascript">
    var main = (function() {
        var urlDocentes = "<?php echo $this->createAbsoluteUrl("inicial/docentesAjax"); ?>";
        var urlDocenteCambia = "<?php echo $this->createAbsoluteUrl("inicial/docenteCambiaAjax"); ?>";
        var vm = {};
        var docentesAjax;
        vm.docenteActivo;
        vm.docentes = ko.observableArray();
        vm.docente_id = ko.observable();

        traeDocentes();

        function traeDocentes() {
            docentesAjax = getDocentesAjax();
            $.when(docentesAjax).then(function(docentes) {
                docentesReady(docentes);
            });
        }

        function docentesReady(docentes) {
            vm.docentes.removeAll();
            $.each(docentes, function(i, o) {
                var docente = new Docente(o);
                docente.jardin.subscribe(function(val) {
                    jardinChange(val, docente);
                });
                vm.docentes.push(docente);
            });
        }

        function getDocentesAjax() {
            return $.getJSON(urlDocentes);
        }

        function Docente(o) {
            this.id = ko.observable(o.id || "");
            this.nombre = ko.observable(o.nombre || "");
            this.login = ko.observable(o.login || "");
            this.jardin = ko.observable(o.jardin ? true : false);
        }

        function jardinChange(val, docente) {
            docente.jardin(val);
            console.log(docente);
            $.get(urlDocenteCambia, {docente_id: docente.id(), val: val}).done(function(data) {
                console.log(data);
            });
        }

        vm.changeDocenteJardinCheckbox = function(o, e) {
            o.jardin($(e.target).is(":checked"));
        };

        return {vm: vm};

    })();

    $("#docente-select").select2({placeholder: "Seleccione un Área"});


    ko.applyBindings(main.vm);

    $(function() {
        setTimeout(function() {
            $("#docente-select").select2("open");
        }, 600);
    });

</script>