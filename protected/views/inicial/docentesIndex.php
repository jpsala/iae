<link rel="stylesheet" href="js/select2/select2.css"/>
<link rel="stylesheet" href="js/select2/select2-bootstrap.css"/>
<script type="text/javascript" src="js/select2/select2.min.js"></script>
<!--<script src="js/knockout-deferred-updates.min.js"></script>-->
<script src="js/knockout.mapping.js"></script>
<script src="js/knockout-projections.min.js"></script>
<div class="container">
    <h2>Docentes</h2>

    <div class="row top10">
        <div class="col-xs-6">
            <table class="table table-condensed table-hover table-striped">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Login</th>
                    <th>Docente del Jardín</th>
                </tr>
                </thead>
                <tbody>
                <!-- ko foreach: docentes -->
                <tr>
                    <td data-bind="text: nombre "></td>
                    <td data-bind="text: login "></td>
                    <td>
                        <input
                            data-bind="checked: jardin(),event:{change:$parent.changeDocenteJardinCheckbox}"
                            class="pull-right" type="checkbox"/>
                    </td>
                </tr>
                <!-- /ko -->

                </tbody>
            </table>
            </select>
        </div>
    </div>
</div>
<?php
    include("docenteIndex.js.php");
    