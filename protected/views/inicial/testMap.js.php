<script  type="text/javascript">
    var vm = (function Vm() {
        var urlAreas = "<?php echo $this->createAbsoluteUrl("inicial/testGetAreas"); ?>";
        var vm = {};
        vm.areas = ko.observableArray();
        vm.areasMap = ko.observable();
        vm.areaActiva = ko.observable("");
        vm.area_id = ko.observable(-1);
        vm.area_activa;
        vm.areas;
        var areaMapping = {
            key: function(item) {
                return ko.utils.unwrapObservable(item.id);
            },
            create: function(options) {
                return new Area(options.data);
            }};
        $.getJSON(urlAreas).done(function(areas) {
            //vm.areasMap(ko.mapping.fromJS(areas, areaMapping));
            ko.mapping.fromJS(areas, areaMapping, vm.areas);
            console.log(1);
        });
        function Area(area) {
            ko.mapping.fromJS(area, {}, this);
            this.text = ko.computed(function() {
                return this.nombre();
            }.bind(this));
        }
        vm.areaActiva = ko.computed(function() {
            var area = ko.utils.arrayFirst(vm.areas(), function(child) {
                return child.id() === vm.area_id();
            });            //console.log(vm.areas.indexOf(vm.area_id));
            return area;
        });
        
        return vm;
        
    })();
</script>