<link  rel="stylesheet" href="js/select2/select2.css"/>
<link  rel="stylesheet" href="js/select2/select2-bootstrap.css"/>
<script src="js/knockout.mapping.js"></script>		
<script src="js/knockout-projections.min.js"></script>		
<script src="js/knockout.my.js"></script>		
<script  type="text/javascript" src="js/select2/select2.min.js"></script>

<?php include("testMap.js.php"); ?>

<div class="container">
    <div class="row-fluid">
        <!--        <div id="area-select" class="form-control select2" 
                     data-bind="obj:areaActiva, select2:{ data: areas, placeholder:'Seleccione un Área'}"
                     style="width:200px">
                     
                </div>-->
        <select id="area-select" class="form-control select2" 
                data-bind="value: area_id,foreach:areas"
                style="width:200px">
            <option data-bind="value:id, text: nombre"></option>
        </select>
        <pre data-bind="text:ko.toJSON(vm.areaActiva.nombre, 0,20)"></pre>
        <pre data-bind="text:ko.toJSON(vm.area_id,0,20)"></pre>
        <!--<pre data-bind="text:ko.toJSON(vm.areasMap,0, 3)"></pre>-->
    </div>
</div>

<script  type="text/javascript">
    ko.applyBindings(vm);
    $(function(){
    $("#area-select2").select2({placeholder:'Seleccione un Área'});
        
    })
</script>