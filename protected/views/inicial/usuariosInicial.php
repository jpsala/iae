<link  rel="stylesheet" href="js/select2/select2.css"/>
<script  type="text/javascript" src="js/select2/select2.min.js"></script>

<div class="container">
		<div class="top20 form-group">
			<label for="concepto_id">Orden</label>
			<select data-bind="value: orden" name="concepto_id" id="concepto_id">
				<option value="0" default>Apellido</option>
				<option value="1">Correo</option>
			</select>
		</div>

  <table class="table stripped">
    <thead>
    <tr>
      <th>Apellido</th>
      <th>Nombre</th>
      <th>Hijos</th>
      <th>Correo</th>
      <th>Password</th>
    </tr>
    </thead>
    <tbody>
      <!-- ko foreach: data -->
        <tr>
          <td data-bind="text:apellido"></td>
          <td data-bind="text:nombre"></td>
          <td data-bind="text:hijos"></td>
          <td data-bind="text:email"></td>
          <td data-bind="text:password"></td>
        </tr>
    	<!-- /ko -->
    </tbody>
  </table>
</div>

<script>
  	function ViewModel() {
			var self = this;
			self.data = ko.observable(<?php echo json_encode($data);?>);
      self.orden = ko.observable(0);
      console.log('data', self);
  	}

		viewModel = new ViewModel;
		ko.applyBindings(viewModel);
    		$("#concepto_id").select2({width:200});

</script>