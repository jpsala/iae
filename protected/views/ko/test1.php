<?php include('test1_item.html');?>
<?php include('test1.html');?>


<script type=text/javascript>
    function AppData() {
        var self = this;
        self.items=ko.observableArray();
        self.items.push({nombre:ko.observable('JP1')});
        self.items.push({nombre:ko.observable('JP2')});
        self.items.push({nombre:ko.observable('JP3')});
        self.afterAddItem = function(item){
            if(item.nodeType === 1){
                $(item).hide().slideDown();
            }
        };
        self.beforeRemoveItem = function(item){
            if(item.nodeType === 1){
                $(item).slideUp(function(){
                    $(item).remove();
                });
            }
        };
        return self;
    }
    var f = new AppData();
    ko.applyBindings(f);
</script>