<div class="container top30 form">
    <div class="row">
        <div class="form-group col-xs-10">
            <label for="nombre">Nombre</label>
            <input data-bind="instantValue:nombre" type="text" class="form-control"
                   placeholder="Ingrese su nombre..." id="nombre"/>
        </div>
    </div>
    <div class="well row">
        <div data-bind="toJSON:nombre"></div>
        <div data-bind="currency:nombre"></div>
        <div class="" id="edit" contentEditable="true"
             data-bind="editableText:nombre"></div>
    </div>
</div>

<script>
    function Vm() {
        this.nombre = ko.observable("");
    }
    ko.applyBindings(new Vm());
</script>