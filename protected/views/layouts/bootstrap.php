<?php header ('Content-type: text/html; charset=utf-8');;?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="content-type" content="text/html; charset=windows-1252">
		<meta name=”viewport” content=”width=device-width, initial-scale=1>
		<meta name="description" content="Test">
		<meta name="author" content="JP">
		<link rel="shortcut icon" href="">
		<!--Yii, no metas el jquery!!!!-->
		<?php Yii::app()->clientScript->scriptMap = array('jquery.js' => false); ?>
		<?php Yii::app()->clientScript->scriptMap = array('jquery.min.js' => false); ?>
		<title> <?php echo CHtml::encode($this->pageTitle); ?> </title>
		<!--jquery-->
		<!--<script src="js/jquery-1.10.1.min.js"></script>-->
		<script src="js/jquery-2.1.0.min.js"></script>

		<!--Bootstrap-->
		<?php Yii::app()->libs->includeBootStrap(); ?>

		<!-- my styles -->
		<link href="css/navbar.css" rel="stylesheet">
		<link href="css/nuevas.css" rel="stylesheet">
		<?php renderLibs(); ?>
	<div class="container">
		<nav class="navbar navbar-default" role="navigation">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo $this->createUrl("/"); ?>">IAE</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav">
					<?php include("bootstrapmenu.php"); ?>
				</ul>
			</div><!-- /.navbar-collapse -->
		</nav>
	</div>
</head>

<body  role="document">
	<p id="width"></p>
	<?php //include(Yii::app()->basePath . "/views/layouts/menu.php"); ?>
	<div id="content" class="" role="main">
		<?php echo $content; ?>			
	</div>

</body>
</html>
<?php

	function renderLibs() { ?>
		<link rel="stylesheet" href="js/font-awesome/css/font-awesome.min.css">
		<script src="js/common-nueva.js"></script>		
		<?php if (Yii::app()->libs->active("knockout")): ?>
			<script src="js/knockout-3.1.0.js"></script>		
			<script src="js/knockout-projections.min.js"></script>		
			<script src="js/knockout-deferred-updates.min.js"></script>		
			<!--<script src="js/knockout-bootstrap.min.js"></script>-->		
			<?php if (Yii::app()->libs->active("knockout.mapping")): ?>
				<script src="js/knockout.mapping.js"></script>		
			<?php endif; ?>
		<?php endif; ?>
		<?php if (Yii::app()->libs->active("knockout-debug")): ?>
			<!--<script src="js/knockout-3.1.0.debug.js"></script>-->		
			<script src="js/knockout-3.1.0.js"></script>		
			<script src="js/knockout-bootstrap.min.js"></script>		
			<?php if (Yii::app()->libs->active("knockout.mapping")): ?>
				<script src="js/knockout.mapping.js"></script>		
			<?php endif; ?>
		<?php endif; ?>
		<?php if (Yii::app()->libs->active("paginator")): ?>
			<script src="js/bootstrap-paginator.min.js"></script>		
		<?php endif; ?>
		<?php if (Yii::app()->libs->active("searchable")): ?>
			<script src="js/searchable/jquery.searchable.min.js"></script>
		<?php endif; ?>
		<?php if (Yii::app()->libs->active("datepicker")): ?>
			<link rel="stylesheet" href="js/bootstrap-datepicker/datepicker3.css"/>
			<script src="js/bootstrap-datepicker/bootstrap-datepicker.js"></script>
			<script src="js/bootstrap-datepicker/locales/bootstrap-datepicker.es.js"></script>
		<?php endif; ?>
		<?php if (Yii::app()->libs->active("jquery-sortable")): ?>
			<script src='js/jquery-sortable-min.js'></script>
		<?php endif; ?>
		<?php if (Yii::app()->libs->active("html5-sortable")): ?>
			<script src='js/html5.sortable.min.js'></script>
		<?php endif; ?>
		<?php if (Yii::app()->libs->active("setImmediate")): ?>
			<script src='js/setImmediate.min.js'></script>
			<script src='js/setImmediateDeferred.js'></script>
		<?php endif; ?>
		<?php
	}
	