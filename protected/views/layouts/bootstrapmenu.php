<?php ?>
<?php menu(null, $this); ?>
<?php

function menu($menu_id, $controller) {
	$where = $menu_id ? "m.menu_id = $menu_id" : "m.menu_id IS null";
	$select = "
		SELECT m.id, 
					case when (m.texto!='') then m.texto else mi.texto end as texto, m.menu_id,
					(SELECT COUNT(*) FROM menu m1 WHERE m1.menu_id = m.id) AS children,
					mi.url
		FROM menu m
			LEFT JOIN menuitem mi ON mi.id = m.menuitem_id
		WHERE $where and m.activo = 1 
		order by orden
	";
	$qry = Helpers::qryAllObj($select, $controller);
	foreach ($qry as $menu) {
		$url = $controller->createUrl($menu->url);
		if ($menu->children > 0) { 
			if (!$menu->menu_id) { //root menu
				echo "<li class=\"dropdown\">";
				echo "<a href=\"$url\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">$menu->texto <b class=\"caret\"></b></a>";
			} else {
				echo "<li class=\"dropdown-submenu\">";
				echo "<a href=\"$url\" > $menu->texto</a>";
			}
			echo "<ul class=\"dropdown-menu\">";
			menu($menu->id, $controller);
			echo "</ul>";
			echo "</li>";
		} else {
			echo "<li>";
			echo "<a href=\"$url\" > $menu->texto</a>";
			echo "</li>";
		}
	}
}
