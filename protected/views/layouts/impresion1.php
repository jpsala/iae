<head>
  <meta http-equiv="X-UA-Compatible" content="IE=9" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="language" content="<?= Yii::app()->language; ?>" />        
  <title><?php echo CHtml::encode($this->pageTitle); ?></title>

  <!--[if lt IE 8]>
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
  <![endif]-->
  <?php
  $baseUrl = Yii::app()->request->baseUrl;
  $cs = Yii::app()->clientScript;
  $cs->registerCssFile($baseUrl . "/css/screen.css", "screen, projection");
  $cs->registerCssFile($baseUrl . "/css/print.css", "print");
  $cs->registerCssFile($baseUrl . "/css/main.css");
  $cs->registerCssFile($baseUrl . "/css/form.css");
  $img = "url(\"http://".$_SERVER["SERVER_NAME"].'/'.Yii::app()->baseUrl."/images/logo.jpg\")";
  ?>
  <style type="text/css">
    .header-tr{vertical-align: middle}
    #header-table{margin: 0}
    .logo{
      background: <?php echo $img;?>  white 0 no-repeat;
      line-height: 51px;padding-left: 65px; font-weight: bolder; font-size: 120%;
      }
    #content{padding-top: 10px; margin: 5px}
  </style>
</head>
<body style="margin: 0px">
  <div class="container" id="page">

    <div class="container">
      <table id="header-table" style="border-bottom: 1px solid black; width: 100%; ">
        <tr class="header-tr">
          <td class="logo section" style="vertical-align: top"><?php echo $this->pageTitle; ?></td>
        </tr>
      </table>
      <div id="content">
        <?php echo $content; ?>
      </div><!-- content -->
    </div>
  </div>
</body>
</html>