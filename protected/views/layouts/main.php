<?php
	header('Content-type: text/html; charset=utf-8');
//	vd($this->action->id);
	if (!Yii::app()->user->isGuest and
			Yii::app()->user->model->cambiar_password and
			$this->action->id !== "cambioPassword"
	) {
		$this->redirect($this->createUrl("/site/cambioPassword"));
	} else if (Yii::app()->user->isGuest and $this->action->id == "alta") {
		//ok
	} else if ((Yii::app()->user->isGuest and $this->action->id !== "login") and ! ($this->action->id == "alta") and ! ($this->action->id == "test")) {
		$this->redirect($this->createUrl("/site/login"));
	}
	$lc_id = Opcion::getOpcionText("liquid_conf_id", Null, "liquidacion");
	$lc_nombre = $lc_id ? Helpers::qryScalar("select descripcion from liquid_conf where id = $lc_id") : null;
?>
<head>
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=9" />
  <meta name="language" content="<?= Yii::app()->language; ?>" />
  <title onclick="return serfeliz();"><?php echo CHtml::encode($this->pageTitle); ?></title>

  <!--[if lt IE 8]>
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
  <![endif]-->
	<?php $baseUrl = Yii::app()->request->baseUrl; ?>
  <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl . "/css/screen.css?r=2"; ?>" media="screen, projection" />
  <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl . "/css/print.css"; ?>" media="print" />
  <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl . "/css/main.css"; ?>" media="" />
  <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl . "/css/form.css"; ?>" media="" />

  <!--<link rel="stylesheet" type="text/css" href="js/jquery-ui/development-bundle/themes/base/minified/jquery-ui.min.css" media="screen, projection" />-->
  <link rel="stylesheet" type="text/css" href="js/jquery-ui/development-bundle/themes/smoothness/jquery.ui.all.css" media="screen, projection" />
  <link rel="stylesheet" type="text/css" href="js/jquery.ui.menubar.css" media="screen, projection" />

  <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl . "/js/clearinput/clearinput.css?r=1"; ?>" media="screen, projection" />
  <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl . "/js/chosen/chosen/chosen.css"; ?>" media="screen, projection" />

  <link rel="stylesheet" type="text/css" href="css/iae.css?r=12" media="screen, projection" />

  <script src="js/jquery-1.10.1.min.js"></script>
  <script src="js/jquery.hotkeys.js"></script>
	<?php if (!(isset($this->notjstree) and $this->notjstree)): ?>
			<script src="js/jstree/jquery.jstree.js"></script>
			<script src="js/jstree/_lib/jquery.hotkeys.js"></script>
		<?php endif; ?>
  <script src="js/jquery-ui/js/jquery-ui-1.10.3.custom.min.js"></script>
  <script src="js/jquery-migrate-1.0.0.min.js"></script>
  <script src="js/jquery.ui.menubar.js"></script>
  <script src="js/chosen/chosen/chosen.jquery.js"></script>
  <script src="js/clearinput/clearinput.js?a=2"></script>
  <script src="js/jquery-ui/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
  <script src="js/session.warning.js"></script>
  <script src="js/idle-timer.min.js"></script>

  <script src="js/jquery.numeric.js"></script>
  <script src="js/autoNumeric/autoNumeric.js"></script>
	<script type="text/javascript" src="js/knockout-3.2.0.js"></script>

  <link rel="stylesheet" type="text/css" href="js/jstree/themes/apple/style.css" media="screen, projection" />
  <!--<script src="js/jquery.hotkeys-0.7.9.min.js"></script>-->
  <script src="js/common.js?a=21"></script>

	<?php
		$contexto = $this->getId() . "_" . $this->getAction()->getId();
		/* @var $this Controller */
	?>
  <script type="text/javascript">
		var escape_key = false;
		var muestraSpin = true;
		var contexto = "<?php echo $contexto; ?>";
		var liq = "<?php echo $lc_nombre ? "<span style='float:left;position:absolute;top:20px;left:63%;font-size:14px;color:red'>Cuidado! Liquidación $lc_nombre</span>" : ""; ?>";
		//var comentarios = "<?php //echo $this->comentarios ? true : false;                          ?>";
		$(function() {
//			if (contexto !== "site_login") {
//				controlSession.init({
//					urlKeep: "<?php echo $this->createUrl("/varios/keepSession"); ?>",
//					urlIndex: "<?php echo $this->createUrl("/site/logout"); ?>",
//					urlSessionSecondsLeft: "<?php echo $this->createUrl("/varios/sessionSecondsLeft"); ?>",
//					warningSeconds: 60
//				});
//			}
			jQuery(this).ajaxStart(function() {
				//$('body').css('cursor', 'wait');
				if (muestraSpin) {
					$('#spin').show();
				}
			});
			jQuery(this).ajaxStop(function() {
				$('#spin').hide();
			});
			jQuery(".table").on("click", "tr", function() {
				$(this).closest("table").find(".selected").toggleClass("selected");
				$(this).toggleClass("selected");
			});
			$("#menubar").append("<a id=\"tickets\" href=\"http://iae.dyndns.org/mantis\" target=\"_blank\">Tickets</a>");
			$("input:visible, body").bind("keydown.esc.main", "esc", function(e) {
				var $cancel;
				if (escape_key) {
					$cancel = $("#cancela");
					if ($cancel.length > 0) {
						$cancel.click();
					} else {
						$cancel = $("#cancel");
						if ($cancel.length > 0) {
							$cancel.click();
						} else {
						}
					}
				} else {
					escape_key = true;
					setTimeout(function() {
						escape_key = false;
					}, 400);
				}
				return false;
			});
			$("body").prepend(liq);
		});

		$.datepicker.setDefaults({
			showOn: 'both',
			buttonImageOnly: true,
			buttonImage: 'images/calendar.gif',
			changeMonth: true,
			changeYear: false,
			showButtonPanel: true,
			numberOfMonths: 2,
			showCurrentAtPos: 1,
			stepMonths: 1,
			firstDay: 1,
			dateFormat: "dd-mm-yyyy",
			autoSize: true,
			showOptions: {direction: "up"}
		});

		function serfeliz() {
			var feliz = 'Volviendo a revisar mis notas encontr algo bello:<br/>\n\
                      Podes tener defectos, vivir ansioso y estar irritado algunas veces, pero no te olvides de que tu vida es la mayor empresa del mundo.\n\
                      Solo vos podes evitar que ella vaya en decadencia.<br/>\n\
                      Hay muchas personas que te precisan, admiran y te quieren.<br/>\n\
                      Me gustaría que siempre recordaras de que ser feliz no es tener un cielo sin tempestades, caminos sin accidentes, trabajos sin cansancio, relaciones sin decepciones<br/>\n\
                      Ser feliz es encontrar fuerza en el perdón, esperanza en las batallas, seguridad en el palco del miedo, amor en los desencuentros.<br/>\n\
                      Ser feliz no es solo valorizar la sonrisa, sino también reflexionar sobre la tristeza.<br/>\n\
                      No es apenas conmemorar el suceso, sino aprender lecciones en los fracasos.<br/>\n\
                      No es apenas tener alegría con los aplausos, sino encontrar alegría en el anonimato.<br/>\n\
                      Ser feliz es reconocer que vale la pena vivir la vida, a pesar de todos los desafíos, incomprensiones y períodos de crisis.<br/>\n\
                      Ser feliz no es una fatalidad del destino, sino una conquista de quien sabe viajar para dentro de su propio ser.<br/>\n\
                      Ser feliz es dejar de ser victima de los problemas y volverse un actor de la propia historia.<br/>\n\
                      Es atravesar desiertos fuera de si, mas ser capaz de encontrar un oasis en lo recóndito de nuestra alma.<br/>\n\
                      Es agradecer a DIOS cada mañana por el milagro de la vida.<br/>\n\
                      Ser feliz es no tener miedo de los propios sentimientos.<br/>\n\
                      Es saber hablar de si mismo.<br/>\n\
                      Es tener coraje para oír un “NO".<br/>\n\
                      Es tener seguridad para recibir una critica, aunque sea injusta.<br/>\n\
                      Es besar a los hijos, mimar a los padres y tener momentos poéticos con los amigos, aunque ellos nos hieran.<br/>\n\
                      Ser feliz es dejar vivir a la criatura libre, alegre y simple que vive dentro de cada uno de nosotros.<br/>\n\
                      Es tener madurez para decir “ME EQUIVOQUE".<br/>\n\
                      Es tener la osadía para decir “PERDÓNAME".<br/>\n\
                      Es tener sensibilidad para expresar “TE NECESITO".<br/>\n\
                      Es tener capacidad de decir "TE AMO".<br/>\n\
                      Deseo que tu vida se vuelva un jardín de oportunidades<br/>\n\
                      para ser feliz...<br/>\n\
                      Que en tus primaveras seas amante de la alegría.<br/>\n\
                      Que en tus inviernos seas amigo de la sabiduría.<br/>\n\
                      Y, cuando te equivoques en el camino, comiences todo de nuevo.<br/>\n\
                      Pues así serás cada vez mas apasionado por la vida.<br/>\n\
                      Y descubrirás que...<br/>\n\
                      Ser feliz no es tener una vida perfecta.<br/>\n\
                      Sino usar las lágrimas para regar la tolerancia.<br/>\n\
                      Usar las perdidas para refinar la paciencia.<br/>\n\
                      Usar las fallas para esculpir la serenidad.<br/>\n\
                      Usar el dolor para lapidar el placer.<br/>\n\
                      Usar los obstáculos para abrir las ventanas de la inteligencia.<br/>\n\
                      Jamás desistas.<br/>\n\
                      Jamás desistas de las personas que amas.<br/>\n\
                      Jamás desistas de ser feliz,<br/>\n\
                      pues la vida es un espectáculo imperdible<br/>\n\
                      Y SOS UN SER HUMANO ESPECIAL! <br/> ';
			alerta(feliz, "Feliz");
		}

  </script>
</head>
<body>

  <div class="container" id="page">

    <div id="header">
      <div id="logo" ondblclick="return serfeliz();"><span id="page-title"><?php //echo $this->pageTitle ? $this->pageTitle : "";                 ?></span></div>
			<?php include('menu.php'); ?>
    </div><!-- header -->


		<?php
			echo $content;
		?>
    <!--<div id="comentarios"></div>-->
    <div id="footer">
    </div><!-- footer -->

  </div><!-- page -->

</body>
</html>
<div id="spin" style="display: none">Esperando datos...</div>
