<?php

// áéíóúñ
$itemsFondos = array();
$itemsBancos = array();
//var_dump(Yii::app()->user);die;
$caja = Destino::getActivo();
if ($caja) {
  $itemsFondos["Cierra la caja"] = array('href' => 'fondos/cierraCaja');
} else {
  $itemsFondos["Abre la caja"] = array('href' => 'fondos/AbreCaja');
}

$itemsFondos["Consulta de caja"]       = array('href' => 'fondos/consultaCaja',);
$itemsFondos["Transferencia"]          = array('href' => 'doc/transferencia',);
$itemsFondos["Depósito"]               = array('href' => 'doc/deposito',);
$itemsBancos["Movimiento Bancario"]    = array('href' => 'doc/movBanco',);
$itemsFondos["Entradas"]               = array('href' => 'doc/movCajaEntrada',);
$itemsFondos["Salidas"]                = array('href' => 'doc/movCajaSalida',);
$itemsBancos["Conciliación"]           = array("href" => "conciliacion");
$itemsBancos["Arregla Cheques Nación"] = array("href" => "varios/arreglachequesnacion");
$itemsBancos["Chequeras"]              = array("href" => "chequera/chequera");
$itemsBancos["Cartera Cheques"]        = array("href" => "Cartera");
$itemsBancos["Libro de Bancos"]        = array("href" => "informe/informeBanco");
$items                                 = array(
  array(
    'Administrativo' => array(
      //'disabled' => Yii::app()->user->checkAccess('Administración'),
      'visible' => Yii::app()->user->checkAccess('Administración'),
      'items' => array(
        'Parámetros' => array(
          'disabled' => Yii::app()->user->isGuest,
          'items' => array(
            'Administración de Alumnos' => array('href' => 'alumno/admin'),
            'Administración de Alumnos 2' => array('href' => 'alumno/admin2'),
            'Números de comprobantes' => array(
              'enabled' => Yii::app()->user->isGuest,
              'href' => "talonario/numeracion"
            ),
            'Bancos' => array(
              'enabled' => Yii::app()->user->isGuest,
              'href' => 'crud/banco'
            ),
            'Artículos' => array(
              'disabled' => Yii::app()->user->isGuest,
              'items' => array(
                'Artículos' => array(
                  'enabled' => Yii::app()->user->isGuest,
                  'href' => 'articulo/articulo'
                ),
                'Tipo de Artículos' => array(
                  'enabled' => Yii::app()->user->isGuest,
                  'href' => 'articulo/tipoarticulo'
                )
              )
            ),
            'Destinos' => array(
              'enabled' => Yii::app()->user->isGuest,
              'href' => 'crud/destino'
            ),
            //'Tipo de Comprobantes' => array('enabled' => Yii::app()->user->isGuest, 'href' => 'crud/comprob_tipo'),
            'Comprobantes' => array(
              'enabled' => Yii::app()->user->isGuest,
              'href' => 'crud/comprob'
            ),
            'Perfiles Trabajo' => array(
              'enabled' => Yii::app()->user->isGuest,
              'href' => 'puestoTrabajo/admin'
            ),
            'Talonarios' => array(
              'enabled' => Yii::app()->user->isGuest,
              'href' => 'crud/Talonario'
            ),
            'Tarjetas' => array(
              'enabled' => Yii::app()->user->isGuest,
              'href' => 'crud/Tarjeta'
            ),
            'Planes de Tarjetas' => array(
              'enabled' => Yii::app()->user->isGuest,
              'href' => 'crud/adminplan'
            ),
            'Tipos de Documentos' => array(
              'enabled' => Yii::app()->user->isGuest,
              'href' => 'crud/tipodoc'
            ),
            'Tipo de Parientes' => array(
              'enabled' => Yii::app()->user->isGuest,
              'href' => 'crud/parientetipo'
            ),
            'Informes' => array(
              'items' => array(
                'Plan de cuentas' => array('href' => 'informe/planDeCuentas'),
              )
            ),
          )
        ),
        'Ventas' => array(
          'disabled' => Yii::app()->user->isGuest,
          //                    'visible' => Yii::app()->user->checkAccess('Administración'),
          'items' => array(
            'Factura Electrónica' => array(
              'items' => array(
                'Control de responsables de pago' => array('href' => 'alumno/parientes'),
                'No vistos' => array('href' => 'alumno/parientesNoVistos'),
                'Recibo' => array('href' => '/doc/reciboElectronico'),
                'NC' => array('href' => '/doc/ncVentasElectronico'),
                'ND' => array('href' => '/doc/ndVentasElectronico'),
              )
            ),
            'Recibo' => array('href' => '/doc/recibo'),
            'Recibo Banco' => array('href' => '/doc/reciboBanco'),
            'Nota de Crédito' => array('href' => '/doc/ncVentas'),
            'Nota de Débito' => array('href' => '/doc/ndVentas'),
            'Aplicaciones' => array('href' => '/doc/aplicacionAlumno'),
            'Recargos' => array('href' => '/liquidacion/recargos'),
            'Carga de Becas' => array(
              'href' => '/alumno/becas',
              'visible' => Yii::app()->user->checkAccess('Carga de Becas')
            ),
            'Informes' => array(
              'items' => array(
                'Con Forma de Pago' => array('href' => 'informe/AlumnoConFormaDePago'),
                'Cuenta Corriente' => array('href' => 'informe/CtaCteNuevo'),
                'Alumnos con saldo' => array('href' => 'informe/AlumnosConSaldo'),
                'Recibos Entre Fechas' => array('href' => 'informe/informeRecibos'),
                'Notas de crédito' => array('href' => 'informe/NC'),
              )
            ),
            'Excel' => array(
              'items' => array(
                "Para armar" => array('href' => 'informe/alumnosSelCols'),
                "Alumno Completo en Excel" => array('href' => 'informe/alumnosCompletoExcel'),
                'Alumnos Fede' => array('href' => 'informe/AlumnosFede1'),
                'Alumnos con saldo' => array('href' => 'informe/AlumnosConSaldoExcel'),
                'Informe de Cobranza Excel' => array('href' => 'informe/informecobranza'),
                'Entradas efectivo y cheque entre fechas' => array('href' => 'informe/lujanEntreFechas'),
                'Nombre/Matr/Beca/Obs/viveCon/Saldo' => array('href' => 'Informe/informeAlumnosASR1'),
              )
            )
          )
        ),
        'Liquidación' => array(
          'disabled' => Yii::app()->user->isGuest,
          //                    'visible' => Yii::app()->user->checkAccess('Administración'),
          'items' => array(
            'Liquidación' => array('href' => '/liquidacion/liquidacion'),
            'Resumen' => array('href' => '/liquidacion/resumen&impresion=1'),
            'Emisión' => array('href' => '/liquidacion/emisionComprobantes'),
            'Emisión nivel o alumno' => array('href' => '/informe/emision'),
            "Rendición Banelco" => array('href' => '/liquidacion/rendicionBanelco'),
            "Rendición Visa" => array('href' => '/liquidacion/rendicionVisa'),
            "Rendición Débitos Visa" => array('href' => '/liquidacion/rendicionDebitosVisa'),
            "Rendición Débitos Visa Superville" => array('href' => '/liquidacion/rendicionDebitosVisa&banco=superville'),
            "Rendición Link" => array('href' => '/liquidacion/rendicionLink'),
            "Débitos" => array('href' => '/liquidacion/debitos'),
            'Generar créditos' => array('href' => '/bancos/archivos'),
            'Carga de novedades' => array('href' => '/novedad/index'),
            'Novedades 100%' => array('href' => '/informe/novedadesClaudia'),
            'Informes' => array(
              'items' => array(
                'Control para rendir a los bancos' => array('href' => '/liquidacion/controlParaElBanco1')
              )
            )
          )
        ),
        'Fondos' => array(
          //                    'disabled' => Yii::app()->user->isGuest,
          'visible' => true,
          'items' => $itemsFondos
        ),
        'Bancos' => array(
          //                    'disabled' => Yii::app()->user->isGuest,
          'visible' => true,
          'items' => $itemsBancos
        ),
        'Compras' => array(
          'disabled' => Yii::app()->user->isGuest,
          //                    'visible' => Yii::app()->user->checkAccess('admin'),
          'items' => array(
            'Proveedores' => array('href' => 'proveedor/proveedor'),
            'Proveedores/retenciones' => array('href' => 'socio/retenciones'),
            'Orden de pago' => array('href' => 'doc/op'),
            'Nota de Crédito' => array('href' => '/doc/ncCompras'),
            'Nota de Débito' => array('href' => '/doc/ndCompras'),
            'Factura' => array('href' => '/doc/facturaCompra'),
            'Aplicaciones' => array('href' => '/doc/aplicacionProveedor'),
            'Informes' => array(
              'items' => array(
                'Cuenta Corriente' => array('href' => 'informe/ProveedorConFormaDePago'),
                'Saldos' => array('href' => 'informe/ProveedoresSaldo'),
                'Movimientos por Concepto' => array('href' => 'informe/ConceptoResumen'),
                'Retenciones' => array('href' => 'informe/ProveedorRetenciones'),
                'archivo-retenciones' => array('href' => 'informe/ProveedorRetencionesArchivo'),
                'Chequear cuenta completa' => array('href' => 'informe/chkSaldo'),
              )
            )
          )
        ),
        'Comprobantes' => array(
          //                    'disabled' => Yii::app()->user->isGuest,
          //                    'visible' => Yii::app()->user->checkAccess('admin'),
          'items' => array(
            //'Anulación' => array('href' => 'doc/anulaDoc'),
            'Consulta/Anula/Modifica' => array('href' => 'informe/docs1'),
            "Comparación de saldos" => array('href' => 'import/SaldosClientes2')
          )
        ),
        'Claudia' => array(
          //                    'disabled' => Yii::app()->user->isGuest,
          //                    'visible' => Yii::app()->user->checkAccess('admin'),
          'items' => array(
            //'Anulación' => array('href' => 'doc/anulaDoc'),
            'ReAplicacion' => array('href' => 'socio/reaplica'),
            'Promoción' => array('href' => 'promocion/promocionPreview'),
          )
        ),
        'Lujan' => array(
          //                    'disabled' => Yii::app()->user->isGuest,
          //                    'visible' => Yii::app()->user->checkAccess('admin'),
          'items' => array(
            //'Anulación' => array('href' => 'doc/anulaDoc'),
            'Chequeo de saldos alumnos y proveedores' => array('href' => 'socio/chequeaCta'),
            'Procesa archivo' => array('href' => 'informe/horarios'),
            'Horarios' => array('href' => 'informe/horariosEntreFechas'),
            'Horarios con detalle' => array('href' => 'informe/horariosEntreFechasDetalle'),
          )
        ),
         'Pedro' => array(
          //                    'disabled' => Yii::app()->user->isGuest,
          //                    'visible' => Yii::app()->user->checkAccess('admin'),
          'items' => array(
            //'Anulación' => array('href' => 'doc/anulaDoc'),
            'Excel cierre de balance' => array('href' => 'peter/cierre'),
          )
        ),
      )
    )
  ),
  array(
    'Educativo' => array(
      'disabled' => Yii::app()->user->isGuest,
      'visible' => Yii::app()->user->checkAccess('Preceptor') or Yii::app()->user->checkAccess('Profesor'),
      'items' => array(
        'Inicial' => array(
          'enabled' => true,
          'items' => array(
            "Areas y Contenidos" => array('href' => '/inicial/areaIndex'),
            "Evaluación Narrada" => array('href' => '/inicial/cargaIndex'),
            "Docentes" => array('href' => '/inicial/docentesIndex'),
          )
        ),
        'Ciclos por defecto' => array(
          'enabled' => Yii::app()->user->isGuest,
          'href' => '/ciclo/index'
        ),
        'Profesores' => array(
          'enabled' => true,
          'items' => array(
            "Datos personales" => array('href' => '/profes/admin'),
            "Datos personales(preceptor)" =>
              array('href' => '/profes/admin&isAdmin=true',
                'visible' => Yii::app()->user->checkAccess('Preceptor'))

          )
        ),
        'Alumnos' => array(
          'enabled' => true,
          //'disabled' => Yii::app()->user->isGuest,
          'visible' => true,
          'items' => array(
            'Administración de Alumnos' => array('href' => 'alumno/admin'),
            'Observaciones' => array('href' => 'alumno/observaciones'),
            'Notas' => array(
              'disabled' => false,
              'visible' => true,
              'items' => array(
                'Carga' => array('href' => '/nota/cargaXAsignatura'),
                'Carga cualitativa' => array('href' => '/nota/cargaXAsignatura&cuali=true'),
               'Stickers' => array('href' => '/informe/stickers'),
               'Stickers EP' => array('href' => '/informe/stickersPatricia'),
                'Planilla resumen final' => array('href' => '/informe/planillaResumenFinal'),
                'Planilla Notas' => array('href' => '/informe/planillaNotas'),
                'Planilla Notas Integradora' => array('href' => '/informe/planillaNotasPablo'),
                'Lógica' => array('href' => '/nota/logica'),
                'Boletines primaria' => array('href' => '/informe/boletinesPrimaria'),
                'Boletines secundaria' => array('href' => '/informe/boletines'),
                'Alumnos con riesgo académico' => array('href' => '/informe/AlumnosConRiesgoAcademico'),
                'Actas volantes de exÃƒÂ¡menes' => array('href' => '/informe/actasVolantes'),
                'Desempeño Global' => array('href' => '/conducta/desempenio'),
              )
            ),
            'Informes' => array(
              'enabled' => true,
              'disabled' => Yii::app()->user->isGuest,
              'visible' => true,
              'items' => array(
                "Registro matrícula" => array('href' => 'informe/registroMatricula'),
                "Alumno Completo en Excel" => array('href' => 'informe/alumnosCompletoExcel'),
                "Para armar" => array('href' => 'informe/alumnosSelCols'),
                'Planilla Buffet' => array('href' => 'informe/informeAlumnos2'),
                'Nombre, Apellido, Fecha de Nac. y Teléfono' => array('href' => 'informe/alumnos3'),
                //								'Nivel/Anio/Divsion/Nombre' => array('href' => 'informe/informeAlumnos1'),
                'Completo' => array('href' => 'Informe/AlumnosCompleto1'),
                'Resumido' => array('href' => 'reportes/alumnosResumido1'),
                'Resumido c/ Dirección y Teléfono' => array('href' => 'reportes/alumnosResumido2'),
                'Alumnos Solo (Apellido y Nombre)' => array('href' => 'reportes/alumnosSolo'),
                //								'Alumnos DNI (Apellido y Nombre, DNI)' => array('href' => 'informe/alumnosDni'),
                //								'Alumnos DNI - Fecha Nacimiento - Nacionalidad ' => array('href' => 'reportes/alumnosDniFnac&tipo=1'),
                'Ingresantes' => array('href' => 'informe/InformeIngresantes1'),
                'Listado para certificados' => array('href' => 'reportes/alumnosDniFnac&tipo=2'),
                //								'Alumnos con sus familias' => array('href' => "informe/alumnosConSusFamilias"),
                //								'DNI Fecha_Nac Dir Tel' => array('href' => '/informe/alumnos2'),
              )
            ),
            //                              'Excel' => array(
            //                                   'enabled' => true,
            //                                   'disabled' => Yii::app()->user->isGuest,
            //                                   'visible' => true,
            //                                   'items' => array(
            //                                        "Alumno Completo en Excel" => array('href' => 'informe/alumnosCompletoExcel'),
            //                                        'Alumnos Fede' => array('href' => 'informe/AlumnosFede1'),
            //                                        'Alumnos con saldo' => array('href' => 'informe/AlumnosConSaldoExcel'),
            //                                        'Informe de Cobranza Excel' => array('href' => 'informe/informecobranza'),
            //                                   )
            //                              ),
          )
        ),
        'Estructura Escolar' => array(
          'disabled' => Yii::app()->user->isGuest,
          'visible' => true,
          'items' => array(
            'Ciclos' => array(
              'enabled' => Yii::app()->user->isGuest,
              'href' => 'crud/ciclo'
            ),
            'Niveles' => array('href' => 'crud/nivel'),
            'Años' => array('href' => 'anio/admin'),
            'Orientaciones' => array('href' => 'orientacion/admin'),
            'Tipos de Asignaturas Según Nivel' => array('href' => 'asignaturatipo/admin'),
            'Fórmulas para cálculo de notas y conducta' => array('href' => '/nota/logica'),
          )
        ),
        'Novedades' => array(
          'disabled' => Yii::app()->user->isGuest,
          'visible' => true, //Yii::app()->user->checkAccess('Novedades'),
          'items' => array(
            'Carga de novedades' => array('href' => '/novedad/index'),
            'Modificación' => array('href' => 'novedad/crudNovedades'),
            'Informes' => array(
              'enabled' => true,
              'visible' => true,
              'items' => array(
                'Carga Entre Fechas' => array('href' => 'novedad/crudNovedades'),
                'General' => array('href' => 'informe/novedades1'),
              )
            ),
          )
        ),
        'Conducta/Inasistencias' => array(
          'disabled' => Yii::app()->user->isGuest,
          'visible' => true,
          'items' => array(
            'Conducta EP' => array('href' => 'conducta/cargaConducta'),
            'Conducta ES' => array('href' => 'conducta'),
            'Tipos de Inasistencia' => array(
              'enabled' => Yii::app()->user->isGuest,
              'href' => 'crud/inasistipo'
            ),
            "Inasistencia Secundaria" => array('href' => 'inasistencia'),
            "Inasistencia Por Alumno" => array('href' => '/informe/alumnoInasistencias'),
            'Inasistencia Primaria' => array('href' => 'inasistencia/cargaInasistencia'),
            //                        'Inasistencia por alumno' => array('href' => 'inasistenciaXAlumno'),
          )
        ),
        'Admin' => array(
          'disabled' => Yii::app()->user->isGuest,
          'visible' => true,
          'items' => array(
            'Usuarios' => array(
              "visible" => Yii::app()->user->checkAccess('PreceptorAdmin'),
              'href' => '/user/admin'
            ),
          )
        ),
        //                'Asignación materias' => array('href' => '/user/asignacionMaterias', 'disabled' => !Yii::app()->user->checkAccess('Directivo')),
        //                'Orden de las materias' => array('href' => '/educativo/materias', 'disabled' => !Yii::app()->user->checkAccess('Directivo')),
      )
    )
  ),
  array(
    'Sistema' => array(
      'disabled' => Yii::app()->user->isGuest,
      'visible' => Yii::app()->user->checkAccess('admin') or Yii::app()->user->checkAccess('Directivo') or Yii::app()->user->checkAccess('Preceptor'),
      'items' => array(
        'Usuarios' => array(
          'disabled' => Yii::app()->user->isGuest,
          'visible' => true,
          'items' => array(
            'Alta' => array('href' => '/user/alta'),
            'Asignaciones' => array('href' => '/user/asignaciones'),
            'Accesos' => array('href' => '/rights')
          )
        ),
        'Varios' => array(
          'disabled' => Yii::app()->user->isGuest,
          'visible' => true,
          'items' => array(
            'Cant. de Familias Activas' => array('href' => 'exec&action=cantidadDeFamiliasActivas'),
            'Menu nuevo' => array('href' => '/menu'),
          )
        ),
        'Comentarios' => array(
          'enabled' => Yii::app()->user->isGuest,
          'href' => 'comentario/general'
        ),
        'Ciclos por defecto' => array(
          'enabled' => Yii::app()->user->isGuest,
          'href' => '/ciclo/index'
        ),
        'nuevo' => array('href' => '/btTest/fontAwesome'),
      )
    )
  ),
);
if (!Yii::app()->user->isGuest) {
  $items[] = array(
    "Perfil (" . Yii::app()->user->model->nombre . ")" => array(
      'items' => array(
        'Cambio de password' => array('href' => 'site/cambioPassword'),
        'Ver/Act. datos' => array('href' => '/educativo/profesor'),
        'Salir' => array('href' => '/site/logout')
      )
    )
  );
} else {
  $items[] = array(
    'Invitado' => array(
      'items' => array(
        'Ingresar' => array('href' => 'site/login'),
        'Registrarse' => array('href' => 'user/alta',)
      )
    )
  );
}

jMenuBar('menubar', $items);

function jMenuBar($id, $menu)
{
  echo <<<EOF
<ul id="$id" style="display:none" class="menubar">
EOF;
  foreach ($menu as $mi) {
    renderMenuBar($mi);
  }
  echo '</ul>';
  echo <<<EOF
<script type="text/javascript">
    $("#$id").show().menubar({
  select: function(event, ui) {
       console.log($(ui.item.context).find('a').attr());
      // window.location = ui.item.context.href;
       event.preventDefault();
      }
    });
    $(".mmm").click(function(){
      if($(this).hasClass("ui-menubar-link")){
        return;
      }
      return false;
  });
</script>

EOF;
}

function renderMenuBar($menu)
{

  foreach ($menu as $key => $val) {

    $visible = isset($val['visible']) ? $val['visible'] : true;

    if ($visible) {

      $disabledMenu  = (isset($val['disabled']) and $val['disabled']) ? true : false;
      $href          = (isset($val['href']) and !$disabledMenu) ? (Yii::app()->urlManager->getBaseUrl() . '?r=' . $val['href']) : '#';
      $disabledClass = ($disabledMenu) ? "ui-state-disabled" : "";

      if (!isset($val['items'])) {

        $a = "
        <a href=\"$href\"
              class=\"$disabledClass ui-button ui-widget ui-button-text-only ui-menubar-link\"
              tabindex=\"-1\"
              role=\"menuitem\" aria-haspopup=\"true\">
          <span class=\"ui-button-text\">$key</span>
        </a>
      ";
        echo $a;
      } else {


        echo '<li>';
        $class = $href == '#' ? "mmm" : "";
        echo "<a class=\"$disabledClass $class \" href=\"$href\">$key</a>";
        echo '<ul>';

        foreach ($val['items'] as $k => $v) {
          if (isset($v['items'])) {
            renderMenuBar(array($k => $v));
          } else {
            $visible = isset($v['visible']) ? $v['visible'] : true;
            if ($visible) {
              $disabledItem  = ((isset($v['disabled']) and $v['disabled']) or $disabledMenu) ? true : false;
              $href          = (isset($v['href']) and !$disabledItem) ? (Yii::app()->urlManager->getBaseUrl() . '?r=' . $v['href']) : '#';
              $disabledClass = ($disabledItem) ? "ui-state-disabled" : "";
              $li            = "<li class=\"$disabledClass\"><a href=\"$href\">$k</a></li>";
              echo $li;
            }
          }
        }

        echo '</ul>';

        echo '</li>';
      }
    }
  }
}
