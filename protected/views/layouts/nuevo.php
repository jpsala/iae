<?php
header('Content-type: text/html; charset=utf-8');
?>
<!DOCTYPE html>
<html lang="es">
  <head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="assets/ico/favicon.png">
	<style>
	  #menubar{margin-left:40px}
	  .menubar.ui-menubar.ui-widget-header.ui-helper-clearfix {
		font-size: 12px;
	  }
	  .ui-menubar .ui-menu {width:220px !important;min-width: 200px !important;max-width: 220px !important;}
	  #logo {
		background: url("images/logo.jpg") no-repeat scroll 1px 0px / 62% auto rgba(0, 0, 0, 0);
		float: left;
		height: 62px;
		margin-top: 0;
		padding: 0;
		position: absolute;
		width: 63px;
	  }
	  #header {
		border-top: 3px solid #C9E0ED;
		margin: 0;
		padding: 0;
	  }
	  .top5{margin-top: 5px}
	  .top10{margin-top: 10px}
	  .top15{margin-top: 15px}
	  .top20{margin-top: 20px}
	  .top25{margin-top: 25px}
	  .top30{margin-top: 30px}
	</style>
	<?php $title = $this->pageTitle ? CHtml::encode($this->pageTitle) : $this->action->id; ?>
	<title>GE - <?php echo $title; ?></title>

	<!--Yii-->

	<?php $baseUrl = Yii::app()->request->baseUrl; ?>
	<script  type="text/javascript">var baseUrl = "<?php echo $baseUrl; ?>"</script>
<!--	<link rel="stylesheet" type="text/css" href="js/jquery-ui/nuevo/development-bundle/themes/smoothness/jquery.ui.all.css" media="screen, projection" />-->
	<link rel="stylesheet" type="text/css" href="js/jquery-ui-nuevo/css/smoothness/jquery-ui-1.10.4.custom.min.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="js/jquery-ui-nuevo/development-bundle/themes/smoothness/minified/jquery-ui.min.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="js/jquery-ui-nuevo/development-bundle/themes/smoothness/minified/jquery.ui.menu.min.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="js/jquery-ui-nuevo/development-bundle/themes/smoothness/minified/jquery.ui.theme.min.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="js/jquery-ui-nuevo/development-bundle/themes/base/minified/jquery-ui.min.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="js/jquery.ui.menubar.css" media="screen, projection" />

	<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl . "/js/clearinput/clearinput.css?r=1"; ?>" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl . "/js/chosen/chosen/chosen.css"; ?>" media="screen, projection" />

	<link href="js/toastr/toastr.min.css" rel="stylesheet"/>


	<script src="js/jquery-2.1.0.min.js"></script>
	<script src="js/jquery.hotkeys.js"></script>
	<script src="js/jquery-ui-nuevo/js/jquery-ui-1.10.4.custom.min.js"></script>
	<script src="js/jquery.ui.menubar.js"></script>

	<script src="js/toastr/toastr.min.js"></script>

	<!-- Custom styles for this template -->
	<link href="css/mines.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="js/font-awesome/css/font-awesome.min.css">
	<script type="text/javascript" src="js/knockout-3.3.0.js"></script>
	<!--<script  type="text/javascript" src="js/knockout-3.1.0.js"></script>-->
	<script type="text/javascript" src="js/knockout.plugins.js"></script>
	<script type="text/javascript" src="js/knockout.dirtyFlag.js"></script>
	<!--<script  type="text/javascript" src="js/knockout-deferred-updates.min.js"></script>-->
	<script type="text/javascript" src="js/knockout.wrap.js"></script>
	<script type="text/javascript" src="js/knockout.mapping.js"></script>
	<script type="text/javascript" src="js/knockout-projections.min.js"></script>
	<script type="text/javascript" src="js/knockout.punches.min.js"></script>
	<script type="text/javascript" src="js/knockout.my.js"></script>
	<script>ko.punches.enableAll()</script>

	<script src="js/common.js?a=22"></script>

	<!-- Bootstrap core CSS -->
	<!--        <link href="bootstrap/css/bootstrap.css" rel="stylesheet">-->
<!--	<link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
	<link href="css/mines.css" rel="stylesheet" type="text/css"/>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
					<script src="assets/js/html5shiv.js"></script>
					<script src="assets/js/respond.min.js"></script>
	<![endif]-->
		<!--<script type="text/javascript" data-main="js/main.js" src="js/require.min.js"></script>-->
  </head>
  <body data-spy="scroll" data-offset="0" data-target="#theMenu">
	<!-- ========== HEADER SECTION ========== -->
	<section id="home"></section>
	<div class="container">
	  <div id="header">
		<div id="logo"><span id="page-title"><?php //echo $this->pageTitle ? $this->pageTitle : "";                                       ?></span></div>
		<?php include('menu.php'); ?>
	  </div><!-- header -->
	</div>

	<div id ="content">
	  <?php echo $content; ?>
	</div>
  </body>
  <div id="spin"><i class="fa fa-spinner fa-spin"></i></div>
</html>
