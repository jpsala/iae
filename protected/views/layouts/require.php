<?php
	header('Content-type: text/html; charset=utf-8');
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="shortcut icon" href="assets/ico/favicon.png">
		
		<?php $title = $this->pageTitle ? CHtml::encode($this->pageTitle) : $this->action->id; ?>
		
		<title>GE - <?php echo $title; ?></title>

		<!--Yii-->

		<?php $baseUrl = Yii::app()->request->baseUrl; ?>
		<script  type="text/javascript">var baseUrl = "<?php echo $baseUrl; ?>"</script>

		<link href="js/toastr/toastr.min.css" rel="stylesheet"/>

		<!-- Custom styles for this template -->
		<link href="css/mines.css" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" href="js/font-awesome/css/font-awesome.min.css">

		<!-- Bootstrap core CSS -->
		<link href="bootstrap/css/bootstrap.css" rel="stylesheet">

		<script type="text/javascript" data-main="scripts/main.js" src="js/require.min.js"></script>
		
	</head>
	<body data-spy="scroll" data-offset="0" data-target="#theMenu">
		<!-- ========== HEADER SECTION ========== -->
		<section id="home"></section>
		<div class="container">
			<div id="header">
				<div id="logo"><span id="page-title"><?php //echo $this->pageTitle ? $this->pageTitle : "";                                    ?></span></div>
				<?php include('menu.php'); ?>
			</div><!-- header -->
		</div>

		<div id ="content">
			<?php echo $content; ?>
		</div>
	</body>
	<div id="spin"><i class="fa fa-spinner fa-spin"></i></div>
</html>
