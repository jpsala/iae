<style>
	#lc1-div,#lc2-div{line-height: 20px; padding: 3px}
	#liquid_conf_ant_id{width: 230px}
	.label{width: 120px; display: inline-block; text-align: right; margin-right: 5px;vertical-align: middle}
	#liquid_conf_ant_id_chzn{vertical-align: middle}
	#lc-nombre{vertical-align: middle; display: inline-block; margin-left: 8px}
	#ajax-div{height: 400px; margin-top: 5px; padding: 5px}
</style>
<?php
$lc = $liquid_conf_id = LiquidConf::getActive();
$liquid_conf_id = $lc->id;
$liquid_conf_ant_id = $lc->id - 1;
$lc_nombre = Helpers::qryScalar("select descripcion from liquid_conf where id = $lc->id");
$ld = CHtml::listData(LiquidConf::model()->findAll("id <> $lc->id"), "id", "descripcion");
$dd = CHtml::dropDownList("liquid_conf_ant_id", $liquid_conf_ant_id, $ld);
?>
<div id="lc1-div">
	<span class="label">Liquidación actual: </span>
	<span id="lc-nombre"><?php echo $lc_nombre; ?></span>
</div>
<div id="lc2-div">
	<span class="label">Liquidación Anterior: </span>
	<?php echo $dd; ?>
</div>
<div id ="ajax-div" class="ui-widget-content ui-corner-all">

</div>
<script>
	var liquid_conf_ant_id = <?php echo $liquid_conf_ant_id; ?>;
	$("#liquid_conf_ant_id").chosen().change(function(){
		traeDatos();
	});
	traeDatos();

	function traeDatos() {
		liquid_conf_ant_id = $("#liquid_conf_ant_id").val();
		$.ajax({
			type: "GET",
			//      dataType:"json",
			data: {liquid_conf_id:<?php echo $liquid_conf_id; ?>,liquid_conf_ant_id:liquid_conf_ant_id},
			url: "<?php echo $this->createUrl("liquidacion/controlAjax"); ?>",
			success: function(data) {
				$("#ajax-div").html(data);
			},
			error: function(data, status) {
			}
		}
		);
	}
</script>

