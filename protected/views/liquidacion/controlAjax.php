<style>
	table{width: 49.8%}
	#data0{float: left}
	#data1{float: right}
	.importe-td ,.cant-td{text-align: right; width:50px}
</style>
<?php foreach ($general as $key=>$data): ?>
	<table id="data<?php echo $key;?>">
		<tr>
			<th>Nivel</th>
			<th>Cantidad</th>
			<th class="importe-td">Cuota</th>
			<th class="importe-td">Beca</th>
			<th class="importe-td">Saldo</th>
			<th class="importe-td">Total</th>
		</tr>
		<?php foreach ($data as $row) : ?>
			<tr>
				<td class="nivel-td"><?php echo $row->nivel; ?></td>
				<td class="cant-td"><?php echo $row->cant; ?></td>
				<td class="importe-td"><?php echo number_format($row->cuota, 0); ?></td>
				<td class="importe-td"><?php echo number_format($row->beca * -1, 0); ?></td>
				<td class="importe-td"><?php echo number_format($row->saldo, 0); ?></td>
				<td class="importe-td"><?php echo number_format($row->cuota + $row->saldo - $row->beca, 0); ?></td>
			</tr>
		<?php endforeach; ?>
		<tr></tr>
	</table>
<?php endforeach; ?>
<?php foreach ($novedades as $key=>$data): ?>
	<table id="data<?php echo $key;?>">
		<tr>
			<th>Nivel</th>
			<th class="importe-td">Detalle</th>
			<th class="importe-td">Cant</th>
			<th class="importe-td">Total</th>
		</tr>
		<?php foreach ($data as $row) : ?>
			<tr>
				<td class="nivel-td"><?php echo $row->nivel; ?></td>
				<td class=""><?php echo $row->detalle; ?></td>
				<td class="cant-td"><?php echo $row->cant; ?></td>
				<td class="importe-td"><?php echo number_format($row->total, 0); ?></td>
			</tr>
		<?php endforeach; ?>
		<tr></tr>
	</table>
<?php endforeach; ?>