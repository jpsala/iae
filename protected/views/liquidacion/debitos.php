<style>
    .alumno-td{width:200px}
    .total-td{width:70px;text-align: right}
    .fecha-td{width:50px;text-align: right}
    .inactivo-tr td{text-decoration: line-through}
</style>
<button id="genera-button" onclick="return generaArchivo();">Genera Archivo</button>
<table id="debito-table">
    <tr>
        <th class="alumno-td">Alumno</th>
        <th>Matrícula</th>
        <th>CBU</th>
        <th class="fecha-td">1er. Vto</th>
        <th class="fecha-td">2do. Vto</th>
        <th></th>
        <th class="total-td"></th>
        <th class="total-td">Total</th>
    </tr>
    <?php $total = 0; ?>
    <?php $cant = 0;?>
    <?php foreach ($data as $row): ?>
        <?php $total =  $total + (($row["activo"] == 0) ? 0:$row["total"] ); ?>
        <?php $cant = $cant + (($row["activo"] == 0) ? 0:1 ); ?>
        <?php $class = ($row["activo"] == 0) ? "class=\"inactivo-tr\"": "";?>
        <tr <?php echo $class;?>>
            <td class="alumno-td"><?php echo $row["alumno"]; ?></td>
            <td><?php echo $row["matricula"]; ?></td>
            <td><?php echo $row["cbu"]; ?></td>
            <td class="fecha"><?php echo $row["fecha_venc_1"]; ?></td>
            <td class="fecha"><?php echo $row["fecha_venc_2"]; ?></td>
            <td></td>
            <td></td>
            <td class="total-td"><?php echo number_format($row["total"],2); ?></td>
        </tr>
    <?php endforeach; ?>
        <tr>
            <td colspan="6">Total</td>
            <td class="total-td" colspan="1"><?php echo $cant;?></td>
            <td class="total-td"><?php echo number_format($total,2); ?></td>
        </tr>
</table>

<script type="text/javascript">
    $("button").button();
    function generaArchivo(){
        window.location="<?php echo $this->createUrl("liquidacion/debitosArchivo");?>";
    }
</script>