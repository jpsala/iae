<?php

$archivo = "debitos.txt";
header('Content-Type: application/download');
header('Content-Disposition: filename=' . $archivo);
$cant = 0;
$total = 0;
$lineas = "";
foreach ($data as $row) {
    $linea = "0370"
            . $row["matricula"] . str_repeat(" ", 18)
            . $row["cbu"]
            . str_pad(strtoupper(trim(substr($row["descripcion"], 0, 15))), 15, " ", STR_PAD_RIGHT)
            . date("Ymd", mystrtotime($row["fecha_venc_1"]))
            . importe($row["total"])
            . date("Ymd", mystrtotime($row["fecha_venc_2"]))
            . importe($row["total"] + Opcion::getOpcionText("recargo", Null, "liquidacion"))
            . "00000000000000000000000   000000000000000                      0000000000000000000000000000000000000000                                                                                                                                        "
            . EOL;
    $lineas.=$linea;
    $total += $row["total"];
    $cant++;
}
$linea = "00004804C"
        . date("Ymd", time())
        . "1EMPRESA"
        . importe($total)
        . str_pad($cant, 7, "0", STR_PAD_LEFT)
        . str_repeat(" ", 304) . EOL;

$lineas = $linea . $lineas;

$linea = '99994804C'
        . date("Ymd", time())
        . '1EMPRESA'
        . importe($total)
        . str_pad($cant, 7, "0", STR_PAD_LEFT)
        . str_repeat(" ", 304) . EOL;

$lineas = $lineas . $linea;

/*
 * sl.Add('99994804C'+
  FormatDateTime('yyyymmdd', now) +
  '1EMPRESA'+
  importeComoString(total) +
  StringOfChar('0',7-length(scant)) + scant +
  StringOfChar(' ',304));
 */

echo $lineas;

return;

function importe($importe) {
    $partes = explode(".", $importe);
    $entero = $partes[0];
    $decimal = (count($partes) == 1) ?
            "00" :
            str_pad($partes[1], 2, "0", STR_PAD_RIGHT);
    return str_pad($entero . $decimal, 14, "0", STR_PAD_LEFT);
}

?>
