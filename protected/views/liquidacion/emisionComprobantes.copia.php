<?php

//chau
//class PDFParaEmision extends PDF {
//
//    public $marginX = 0, $marginY = 0;
//
//    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false) {
//        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
//    }
//
//    public function Header() {
//        
//    }
//
//    public function Footer() {
//        
//    }
//
//}

/*
 * Empresa:= '0001';

  Fecha01:= [FORMATDATETIME('DDMMYYYY', [fecha1])];

  Imp1:= [Saldo_Regularice] + [Acumulador];
  Importe1Punto:= [FORMATFLOAT('000000.00', Imp1)];
  Importe1:= [COPY(Importe1Punto, 1, 6)] + [COPY(Importe1Punto, 8, 2)];

  Fecha02:= [FORMATDATETIME('DDMMYYYY', [fecha2])];

  Imp2:= [[total]+40];
  Importe2Punto:= [FORMATFLOAT('000000.00', Imp2)];
  Importe2:= [COPY(Importe2Punto, 1, 6)] + [COPY(Importe2Punto, 8, 2)];

  Matricula := [FORMATFLOAT('0000', [DialogForm.qryCabecera."Matricula"])];

  NroComprob := [FORMATFLOAT('000000', [DialogForm.qryCabecera."OID_LIQUID_CAB"])];

  Digito:= '0';

  Cad:= Empresa + Fecha01 + Importe1 + Fecha02 + Importe2 + Matricula + NroComprob;

  CadFinal:= '(';
  for i:= 1 to 46 do
  begin
  if ((i mod 2) <> 0) then
  begin
  Par:= [COPY(Cad, i, 2)];

  if ([STRTOFLOAT(Par)] <= 49) then
  valor:= [STRTOFLOAT(Par)] + 48
  else
  valor:= [STRTOFLOAT(Par)] + 142;

  CadFinal:= CadFinal + [CHR(valor)];
  end;
  end;
  CadFinal:= CadFinal + ')';


  end
 * 
 * 
 *   (0178D=0DÆ0>8D=0Dî0]1ÓÊ4)
 *   0001070820130020560014082013002096004501696004
 */
$pdf = new PDF(PDF_PAGE_ORIENTATION, PDF_UNIT, "letter", true, 'UTF-8', false);
$pdf->SetMargins(0, 0);
$pdf->SetAutoPageBreak(false);
$style = array('text' => true, 'font' => 'helvetica', 'fontsize' => 9);
$cabY = 39;
$detalleY = 60;
$il = 4.5;
$pdf->SetFontSize(10);
$mx = 10;
foreach ($alumnos as $a) {
	$totalComprob = $a["doc"]["total"] + $a["doc"]["saldo_anterior"];
	if ($totalComprob < 0) {
		//continue;
	}
	$totalComprobMasRecargo = $totalComprob + Opcion::getOpcionText("recargo", Null, "liquidacion");
	$beca = $a["beca"] > 0 ? str_replace(".00", "", $a["beca"]) . "%" : "";
	//vd($a);
	$pdf->SetMargins(0, 0);
	$pdf->addPage();
	$y = $cabY;
	$pdf->textBox(array("txt" => $a["alumno"], "x" => $mx + 8, "y" => $y));
	$pdf->textBox(array("txt" => $a["nivel"], "x" => $mx + 133, "y" => $y - 5));
	$y+=$il;
	$domicilio = mb_convert_case(trim($a["domicilio"]), MB_CASE_TITLE, "UTF-8");
	$pdf->textBox(array("txt" => $domicilio, "x" => $mx + 8, "y" => $y));
	$y+=$il * 2;
	$pdf->textBox(array("txt" => $a["anio"] . " " . $a["division"], "x" => $mx + 132, "y" => $y - 5));
	$pdf->textBox(array("txt" => $a["matricula"], "x" => $mx + 165, "y" => $y - 5));
	$pdf->textBox(array("txt" => $beca, "x" => $mx + 180, "y" => $y - 5));

	$doc = $a["doc"];
	$y = $detalleY;
	$x = 15;
	$totalX = 106;
	$saldo_ant = $doc["saldo_anterior"] !== 0 ? number_format($doc["saldo_anterior"], 2) : "";
	//$saldo_ant = $saldo_ant < 0 ? $saldo_ant ."xx" : $saldo_ant;
	$pdf->textBox(array("txt" => date("d/m/Y", strtotime($a["fecha_liquidacion"])), "x" => $x, "y" => $y + 6));
	$pdf->textBox(array("txt" => $saldo_ant, "x" => 132, "y" => $y + 6, "align" => "R", "w" => 50, "align" => "R", "w" => 50));
	$y = 72;
	foreach ($doc["det"] as $det) {
		$x = 36;
		$pdf->textBox(array("txt" => $det["nombre"], "x" => $x, "y" => $y));
		$pdf->textBox(array("txt" => number_format($det["total"], 2), "x" => $mx + $totalX, "y" => $y, "align" => "R", "w" => 50));
		$y += $il;
	}
	$y = 103;
	$pdf->textBox(array("txt" => number_format($totalComprob, 2), "x" => 138, "y" => $y - 7, "align" => "R", "w" => 50));
	$y += $il * 2.5;
	if ($a["debito_activo"]) {
		$x = 5;
		$pdf->textBox(array("txt" => "DÉBITO AUTOMÁTICO", "x" => $mx + $x, "y" => $y - 10));
	}
	if ($doc["saldo_anterior"] >= $doc["total"]) {
		$x = 5;
		$pdf->textBox(array("txt" => "Registra saldos pendientes. Regularice su situación.", "x" => $mx + $x, "y" => $y));
	}
	if ($a["visa"]) {
		$x = 5;
		$pdf->textBox(array("txt" => "VISA - ACREDITACIÓN CUOTA", "x" => $mx + $x, "y" => $y - 10));
		$pdf->textBox(array("txt" => "sujeta a confirmación", "x" => $x + 12, "y" => $y - 10 + 5));
	}
	$x = 138;
	$y = 115;
	$pdf->textBox(array("txt" => date("d/m/Y", mystrtotime($doc["fecha_vto1"])), "x" => $mx + $x - 5, "y" => $y - 3));
	$pdf->textBox(array("txt" => number_format($totalComprob, 2), "x" => 138, "y" => $y - 3, "align" => "R", "w" => 50));
	$y += $il * 3;
	$x = 5;
	$link = str_pad($a["matricula"], 8, "0", STR_PAD_LEFT);
	$banelco = str_pad($a["matricula"], 10, "0", STR_PAD_LEFT);
	$pdf->textBox(array("txt" => "Cód. Link: $link - Cód. Banelco: $banelco", "x" => $mx + $x, "y" => $y - 3));
	$x = 138;
	$pdf->textBox(array("txt" => date("d/m/Y", mystrtotime($doc["fecha_vto2"])), "x" => $mx + $x - 5, "y" => $y - 3));
	$pdf->textBox(array("txt" => number_format($totalComprobMasRecargo, 2), "x" => 138, "y" => $y - 3, "align" => "R", "w" => 50));

	$margen = 5;
	$barWidth = ($pdf->getPageWidth() / 2) - ($margen + ($margen / 2)) - 8;
	$cols[0] = $margen;
	$cols[1] = ($pdf->getPageWidth() / 2) + ($margen / 2) - 6;
	$topY = 167;
	for ($parte = 0; $parte < 2; $parte++) {
		$y = $topY;
		$x = $cols[$parte];

		$pdf->textBox(array("txt" => $a["nivel"], "x" => $mx + $x, "y" => $y));
		$y += $il * 3;
		$pdf->textBox(array("txt" => $a["anio"] . " " . $a["division"], "x" => $mx + $x + 1.5, "y" => $y));
		$pdf->textBox(array("txt" => $a["matricula"], "x" => $mx + $x + 43, "y" => $y));
		if ($beca) {
			$pdf->textBox(array("txt" => $beca, "x" => $mx + $x + 70, "y" => $y));
		}
		$y += $il * 3.5;
		$pdf->textBox(array("txt" => $a["alumno"], "x" => $mx + $x + 2, "y" => $y - 2));
		$y += $il * 1.5;
		$pdf->textBox(array("txt" => $domicilio, "x" => $mx + $x + 2, "y" => $y - 2));
		$y = $topY + 63;
		$pdf->textBox(array("txt" => date("d/m/Y", mystrtotime($doc["fecha_vto1"])), "x" => $mx + $x + 12, "y" => $y - 3));
		$pdf->textBox(array("txt" => number_format($totalComprob, 2), "x" => $mx + $x + 26, "y" => $y - 3, "align" => "R", "w" => 50));
		$y += $il * 3;
		$pdf->textBox(array("txt" => date("d/m/Y", mystrtotime($doc["fecha_vto2"])), "x" => $mx + $x + 12, "y" => $y - 5));
		$pdf->textBox(array("txt" => number_format($totalComprobMasRecargo, 2), "x" => $mx + $x + 26, "y" => $y - 5, "align" => "R", "w" => 50));

		/*
		 * Código de barras
		 */
		if ($doc["saldo_anterior"] + $doc["total"] > 0) {
			$cadena = calculaCadena($a, $doc);
			$y = $pdf->getPageHeight() - 28;
			if ($parte == 0) {
				$pdf->write1DBarcode($cadena, 'I25', 11, $y, $barWidth, 16, 0.4, $style, 'N');
				//$pdf->textBox(array("txt" => $cadena, "x" => $margen, "y" => $y));
			} else {
				$pdf->write1DBarcode($cadena, 'I25', $pdf->getPageWidth() - $barWidth - $margen - 5, $y, $barWidth, 16, 0.4, $style, 'N');
				//$pdf->textBox(array("txt" => $cadena, "x" => $pdf->getPageWidth() - $barWidth - $margen, "y" => $y));
			}
		}
	}
}
$pdf->Output('liquidacion.pdf', 'I');

function calculaCadena($a, $doc) {
	$empresa = '0001';
	$fecha1 = date("dmY", mystrtotime($doc["fecha_vto1"]));
	$fecha2 = date("dmY", mystrtotime($doc["fecha_vto2"]));
	$total = $doc["saldo_anterior"] + $doc["total"];
	if ($total <= 0) {
		return "";
	}
	$partes = explode(".", $total);
	if (count($partes) == 1) {
		$partes[1] = "00";
	}
	$importe1 = str_pad($partes[0], 6, "0", STR_PAD_LEFT) . str_pad($partes[1], 2, "0", STR_PAD_RIGHT);

	$partes = explode(".", $total + Opcion::getOpcionText("recargo", 40, "liquidacion"));
	if (count($partes) == 1) {
		$partes[1] = "00";
	}
	$importe2 = str_pad($partes[0], 6, "0", STR_PAD_LEFT) . str_pad($partes[1], 2, "0", STR_PAD_RIGHT);
	$matricula = str_pad($a["matricula"], 4, "0", STR_PAD_LEFT);
	$nroComprob = str_pad($doc["id"], 6, "0", STR_PAD_LEFT);
//    $NroComprob : = FORMATFLOAT('000000', [DialogForm . qryCabecera . "OID_LIQUID_CAB"]);
	return $empresa . $fecha1 . $importe1 . $fecha2 . $importe2 . $matricula . $nroComprob;
}

function calculaCodigo($cadena) {
	/*
	 *   
	  CadFinal:= '(';
	  for i:= 1 to 46 do
	  begin
	  if ((i mod 2) <> 0) then
	  begin
	  Par:= [COPY(Cad, i, 2)];

	  if ([STRTOFLOAT(Par)] <= 49) then
	  valor:= [STRTOFLOAT(Par)] + 48
	  else
	  valor:= [STRTOFLOAT(Par)] + 142;

	  CadFinal:= CadFinal + [CHR(valor)];
	  end;
	  end;
	  CadFinal:= CadFinal + ')';
	 */
	$ret = "(";
	for ($i = 1; $i <= 46; $i++) {
		if (($i % 2) !== 0) {
			$par = substr($cadena, $i - 1, 2);
			if ($par <= 49) {
				$valor = $par + 48;
			} else {
				$valor = $par + 142;
			}
			//$ret .= str_pad(chr($valor),2,"0",STR_PAD_LEFT);
			$ret .= chr($valor);
		}
	}
	$ret .= ")";
	vd($ret, "(0178D=0DÆ0>8D=0Dî0]1ÓÊ4)");
}

?>
