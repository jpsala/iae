<?php

//chau
//class PDFParaEmision extends PDF {
//
//    public $marginX = 0, $marginY = 0;
//
//    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false) {
//        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
//    }
//
//    public function Header() {
//
//    }
//
//    public function Footer() {
//
//    }
//
//}

/*
 * Empresa:= '0001';

  Fecha01:= [FORMATDATETIME('DDMMYYYY', [fecha1])];

  Imp1:= [Saldo_Regularice] + [Acumulador];
  Importe1Punto:= [FORMATFLOAT('000000.00', Imp1)];
  Importe1:= [COPY(Importe1Punto, 1, 6)] + [COPY(Importe1Punto, 8, 2)];

  Fecha02:= [FORMATDATETIME('DDMMYYYY', [fecha2])];

  Imp2:= [[total]+40];
  Importe2Punto:= [FORMATFLOAT('000000.00', Imp2)];
  Importe2:= [COPY(Importe2Punto, 1, 6)] + [COPY(Importe2Punto, 8, 2)];

  Matricula := [FORMATFLOAT('0000', [DialogForm.qryCabecera."Matricula"])];

  NroComprob := [FORMATFLOAT('000000', [DialogForm.qryCabecera."OID_LIQUID_CAB"])];

  Digito:= '0';

  Cad:= Empresa + Fecha01 + Importe1 + Fecha02 + Importe2 + Matricula + NroComprob;

  CadFinal:= '(';
  for i:= 1 to 46 do
  begin
  if ((i mod 2) <> 0) then
  begin
  Par:= [COPY(Cad, i, 2)];

  if ([STRTOFLOAT(Par)] <= 49) then
  valor:= [STRTOFLOAT(Par)] + 48
  else
  valor:= [STRTOFLOAT(Par)] + 142;

  CadFinal:= CadFinal + [CHR(valor)];
  end;
  end;
  CadFinal:= CadFinal + ')';


  end
 *
 *
 *   (0178D=0DÆ0>8D=0Dî0]1ÓÊ4)
 *   0001070820130020560014082013002096004501696004
 */
$pdf = new PDF("L", PDF_UNIT, "A5", true, 'UTF-8', false);
$pdf->SetMargins(0, 0);
$pdf->SetAutoPageBreak(false);
$style = array('text' => true, 'font' => 'helvetica', 'fontsize' => 9);
$cabY = 10;
$detalleY = 60;
$il = 4.5;
$pdf->SetFontSize(10);
$mx = 0;
$numero = $primero - 1;
foreach ($alumnos as $a) {
	$numero++;
	$totalComprob = $a["doc"]["total"] + $a["doc"]["saldo_anterior"];
	if ($totalComprob < 0) {
		//continue;
	}
	$totalComprobMasRecargo = $totalComprob + Opcion::getOpcionText("recargo", Null, "liquidacion");
	$beca = $a["beca"] > 0 ? str_replace(".00", "", $a["beca"]) . "%" : "";
	//vd($a);
	$pdf->SetMargins(0, 0);
	$pdf->addPage();
	$y = $cabY+1;
	$pdf->SetFontSize(13);
	$pdf->textBox(array("txt" => "Reserva de Vacante 2020", "x" => $mx + 130, "y" => $y - 5));
	$pdf->textBox(array("txt" => "Nivel Inicial", "x" => $mx + 130, "y" => $y + 4));
	$pdf->textBox(array("txt" => "Nro. " . str_pad($numero, 4, "0", STR_PAD_LEFT) , "x" => $mx + 130, "y" => $y + 13));

	$pdf->SetFontSize(11);

	$doc = $a["doc"];
	$y = $detalleY - 18;
	$x = 12;
	$totalX = 103;
//	$pdf->Line ($x, $y-2, $x+$pdf->getPageWidth()-34, $y-2, $style=array());
	$saldo_ant = $doc["saldo_anterior"] !== 0 ? number_format($doc["saldo_anterior"], 2) : "";
	//$saldo_ant = $saldo_ant < 0 ? $saldo_ant ."xx" : $saldo_ant;
	// $pdf->textBox(array("txt" => "Saldo al " . date("d/m/Y", strtotime($a["fecha_liquidacion"])), "x" => $x, "y" => $y + 7));
	// $pdf->textBox(array("txt" => $saldo_ant, "x" => 144, "y" => $y + 7, "align" => "R", "w" => 50, "align" => "R", "w" => 50));
	$pdf->textBox(array("txt" => "Detalle", "x" => $x, "y" => $y + 1));
	$pdf->textBox(array("txt" => 'Importe', "x" => $mx + $totalX +14, "y" => $y+1, "align" => "R", "w" => 50));
	$pdf->textBox(array("txt" => 'Saldo', "x" => 144, "y" => $y+1, "align" => "R", "w" => 50));
	$y += 8;
	$x = 15;
	$pdf->textBox(array("txt" => "Arancel en concepto Reserva de Vacante 2020", "x" => $x-3, "y" => $y));
	$pdf->textBox(array("txt" => number_format(7580, 2), "x" => $mx + $totalX +14, "y" => $y, "align" => "R", "w" => 50));
	$y += $il;
	$y += 17;
	/*
	 *
TCPDF::SetFont	(	 	$family,
 	$style = '',
 	$size = null,
 	$fontfile = '',
 	$subset = 'default',
 	$out = true
)
	 */
	$pdf->SetFont('', 'B', 11);
	$pdf->textBox(array("txt" => "Total", "x" => $x-3, "y" => $y - 16));
	$pdf->textBox(array("txt" => number_format(7580, 2), "x" => 144, "y" => $y - 16, "align" => "R", "w" => 50));


	$pdf->SetFont('', '', 10.5);
	$pdf->textBox(array("txt" => "Se deja expresamente establecido que una vez efectivizado no se reintegrará su valor debido a la naturaleza de", "x" => $x-6, "y" => $y-8));
	$pdf->textBox(array("txt" => "la misma, sirviendo el presente comprobante de suficiente recibo con sello o ticket de la entidad recaudatoria.", "x" => $x-6, "y" => $y-3));
	$pdf->textBox(array("txt" => "Lugares de pago: Ripsa Pagos - Administración IAE", "x" => $x-6, "y" => $y+16));
	$pdf->textBox(array("txt" => "Próximo vencimiento: Cuota 1 - 07-02-2020", "x" => $x-6, "y" => $y+26));

	$pdf->SetFont('', '', 10);

//	$pdf->Line ($x-3, $y-9, $x+$pdf->getPageWidth()-37, $y-9, $style=array());
	$box_height = $y - $detalleY + 8;
	$pdf->setXY(10, $detalleY - 18);
	$pdf->Cell(187, $box_height, '', 1, 2, 'C', 0, '', 0);
	$pdf->setXY(10, $detalleY - 18);
	$pdf->Cell(187, 6, '', 1, 2, 'C', 0, '', 0);
	$pdf->setXY(10, $detalleY - 18);
	// $pdf->Cell(130, $box_height, '', 1, 2, 'C', 0, '', 0);
	$pdf->setXY(10, $detalleY - 18);
	// $pdf->Cell(160, $box_height, '', 1, 2, 'C', 0, '', 0);
	$pdf->setXY(140, $detalleY - 18);
	$pdf->Cell(30, $y - $detalleY + 8 , '', 1, 2, 'C', 0, '', 0);

	$y = 110;
	$x-=6;
	$x = 138;
	//borde
//	$pdf->textBox(array("border"=>"1", "width"=> 10, "txt" => '', "x" => $mx + $x - 8, "y" => $y - 5));
//	$pdf->Cell($y - 5, $mx + $x - 8, 'TEST CELL STRETCH: no stretch', 1, 2, 'C', 0, '', 0);
	//fin forde

	$y = 116;
	// $pdf->Line ($mx + $x - 9, $y-5, $mx + $x + 54, $y-5, $style=array());
	// Recuadro / borde caja
	$pdf->setXY(140, $detalleY - 18);
	// $pdf->setXY($mx + $x - 9, $y-5);
	// $pdf->Cell(57, 93, '', 1, 2, 'C', 0, '', 0);
	$pdf->setXY($mx + $x + 2, $y-5);
	$pdf->Cell(57, 8, '', 1, 2, 'C', 0, '', 0);
	$pdf->setXY($mx + $x - 2 + 34, $y-5);
	$pdf->Cell(27, 24, '', 1, 2, 'C', 0, '', 0);
	$pdf->setXY($mx + $x - 2 + 4, $y-5);
	$pdf->Cell(30, 24, '', 1, 2, 'C', 0, '', 0);
	// $pdf->setXY($mx + $x - 4 + 35, $y-5);
	// $pdf->Cell(28, 8, '', 1, 2, 'C', 0, '', 0);
	$pdf->setXY($mx + $x - 9, $y-5);
	// $pdf->Cell(68, 17, '', 1, 2, 'C', 0, '', 0);
	$pdf->textBox(array("txt" => 'Vencimiento', "x" => $mx + $x + 8, "y" => $y - 3));
	$pdf->textBox(array("txt" => 'Importe', "x" => 144, "y" => $y - 3, "align" => "R", "w" => 50));
	$y += 8;
	$pdf->textBox(array("txt" => "30/06/2019", "x" => $mx + $x + 9, "y" => $y));
	$pdf->textBox(array("txt" => number_format(7580, 2), "x" => 144, "y" => $y, "align" => "R", "w" => 50));
	$y += $il * 3;
	$x = 5;
	$link = str_pad($a["matricula"], 8, "0", STR_PAD_LEFT);
	$banelco = str_pad($a["matricula"], 10, "0", STR_PAD_LEFT);
	// $pdf->textBox(array("txt" => "Cód. Link: $link - Cód. Banelco: $banelco", "x" => $mx + $x + 11, "y" => $y - 19));
	$x = 138;
	$y -= 3;
	// $y += 5;

	$margen = 5;
	$barWidth = ($pdf->getPageWidth() / 2) - ($margen + ($margen / 2)) - 8;
	$cols[0] = $margen;
	$cols[1] = ($pdf->getPageWidth() / 2) + ($margen / 2) - 6;
	$topY = 167;
	for ($parte = 0; $parte < 1; $parte++) {
//		$y = $topY;
//		$x = $cols[$parte];
//
//		$pdf->textBox(array("txt" => $a["nivel"], "x" => $mx + $x, "y" => $y));
//		$y += $il * 3;
//		$pdf->textBox(array("txt" => $a["anio"] . " " . $a["division"], "x" => $mx + $x + 1.5, "y" => $y));
//		$pdf->textBox(array("txt" => $a["matricula"], "x" => $mx + $x + 43, "y" => $y));
//		if ($beca) {
//			$pdf->textBox(array("txt" => $beca, "x" => $mx + $x + 70, "y" => $y));
//		}
//		$y += $il * 3.5;
//		$pdf->textBox(array("txt" => $a["alumno"], "x" => $mx + $x + 2, "y" => $y - 2));
//		$y += $il * 1.5;
//		$pdf->textBox(array("txt" => $domicilio, "x" => $mx + $x + 2, "y" => $y - 2));
//		$y = $topY + 63;
//		$pdf->textBox(array("txt" => date("d/m/Y", mystrtotime($doc["fecha_vto1"])), "x" => $mx + $x + 12, "y" => $y - 3));
//		$pdf->textBox(array("txt" => number_format($totalComprob, 2), "x" => $mx + $x + 26, "y" => $y - 3, "align" => "R", "w" => 50));
//		$y += $il * 3;
//		$pdf->textBox(array("txt" => date("d/m/Y", mystrtotime($doc["fecha_vto2"])), "x" => $mx + $x + 12, "y" => $y - 5));
//		$pdf->textBox(array("txt" => number_format($totalComprobMasRecargo, 2), "x" => $mx + $x + 26, "y" => $y - 5, "align" => "R", "w" => 50));
//
		/*
		 * Código de barras
		 */
			$cadena = calculaCadena($a, $doc, $numero);
			$y = $pdf->getPageHeight() - 28;
			if ($parte == 0) {
				$pdf->write1DBarcode($cadena, 'I25', 12, $y, $barWidth, 16, 0.4, $style, 'N');
//				$pdf->textBox(array("txt" => $cadena, "x" => $margen+1, "y" => $y));
			} else {
//				$pdf->write1DBarcode($cadena, 'I25', $pdf->getPageWidth() - $barWidth - $margen - 5, $y, $barWidth, 16, 0.4, $style, 'N');
				//$pdf->textBox(array("txt" => $cadena, "x" => $pdf->getPageWidth() - $barWidth - $margen, "y" => $y));
			}
	}
}
$pdf->Output('liquidacion.pdf', 'I');

function calculaCadena($a, $doc, $numero)
{
	$empresa = '0001';
	$fecha1 = "30062019";
	$fecha2 = "30062019";
	$total = 7580;
	if ($total <= 0) {
		return "";
	}
	$partes = explode(".", $total);
	if (count($partes) == 1) {
		$partes[1] = "00";
	}
	$importe1 = str_pad($partes[0], 6, "0", STR_PAD_LEFT) . str_pad($partes[1], 2, "0", STR_PAD_RIGHT);

	$partes = explode(".", $total);
	if (count($partes) == 1) {
		$partes[1] = "00";
	}
	$importe2 = str_pad($partes[0], 6, "0", STR_PAD_LEFT) . str_pad($partes[1], 2, "0", STR_PAD_RIGHT);
	$matricula = str_pad('9999', 4, "0", STR_PAD_LEFT);
	$nroComprob = str_pad($numero, 6, "0", STR_PAD_LEFT);
//    $NroComprob : = FORMATFLOAT('000000', [DialogForm . qryCabecera . "OID_LIQUID_CAB"]);
	return $empresa . $fecha1 . $importe1 . $fecha2 . $importe2 . $matricula . $nroComprob;
}

function calculaCodigo($cadena)
{
	/*
	 *
	  CadFinal:= '(';
	  for i:= 1 to 46 do
	  begin
	  if ((i mod 2) <> 0) then
	  begin
	  Par:= [COPY(Cad, i, 2)];

	  if ([STRTOFLOAT(Par)] <= 49) then
	  valor:= [STRTOFLOAT(Par)] + 48
	  else
	  valor:= [STRTOFLOAT(Par)] + 142;

	  CadFinal:= CadFinal + [CHR(valor)];
	  end;
	  end;
	  CadFinal:= CadFinal + ')';
	 */
	$ret = "(";
	for ($i = 1; $i <= 46; $i++) {
		if (($i % 2) !== 0) {
			$par = substr($cadena, $i - 1, 2);
			if ($par <= 49) {
				$valor = $par + 48;
			} else {
				$valor = $par + 142;
			}
			//$ret .= str_pad(chr($valor),2,"0",STR_PAD_LEFT);
			$ret .= chr($valor);
		}
	}
	$ret .= ")";
//	vd($ret, "(0178D=0DÆ0>8D=0Dî0]1ÓÊ4)");
}

?>
