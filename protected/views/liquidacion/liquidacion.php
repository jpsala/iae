<style type="text/css">
	#form{width:550px; margin:0 auto}
	#registro-nuevo{}
	input[name='descripcion']{width: 300px;display: inline}
	form{margin-top: 10px; padding: 5px 0px 8px 5px; overflow: visible}
	#nivel_id{width:150px}
	#nivel_id-div{display: block}
	label{margin-bottom: 5px}
	.row{margin-top: 8px; clear:both}
	.hcenter{text-align: center}
	#form-title{padding: 5px}
	#form-buttons{margin: 15px 0px 0px 0;}
	label{float:left; width: 110px; margin-top: 3px }
	input[type="checkbox"]{   margin-top: 7px;vertical-align: middle;}
	#resumen{display:none; margin: 0 auto; width: 500px; margin-top: 3px;margin-bottom: 3px}
	#resumen-inner{padding:5px}
	.total{text-align: right; max-width: 80px;}
	.totalH1{font-weight:  bold; color: black}
	.totalNum{font-weight:  bold; color: black;}
	#resumen-table .right{ text-align: right}
	.nivel-td{padding:0; margin: 0; text-align: center}
	.nivel-div{padding: 3px; font-weight:  bold; text-align: center}
	.negrita{font-weight: 900}
	#interes-tr{margin-top: 5px}
</style>
<div id="form" class="ui-widget ui-widget-content ui-corner-all">
	<?php $fecha_venc_1 = $lc->fecha_venc_1 ? date("d/m/Y", mystrtotime($lc->fecha_venc_1)) : ""; ?>
	<?php $fecha_venc_2 = $lc->fecha_venc_2 ? date("d/m/Y", mystrtotime($lc->fecha_venc_2)) : ""; ?>
	<?php if ($lc->isNewRecord): ?>
			<div id="form-title" class="hcenter ui-state-default ui-corner-tl ui-corner-tr" id="registro-nuevo">Nueva configuración</div>
		<?php else: ?>
			<div id="form-title" class="hcenter ui-state-default ui-corner-tl ui-corner-tr" id="registro-nuevo">Modificando la configuración</div>
	<?php endif; ?>
	<form method="POST" action="submit">
		<input type="hidden" id="id" name="id" value="<?php echo $lc->id; ?>"/>
		<input type="hidden" id="nro-cuota" name="nro_cuota" value="<?php echo $lc->nro_cuota; ?>"/>
		<div class="row">
			<label class="row" for="descripcion">
				Descripción:
			</label>
			<input type="text" id="descripcion" name="descripcion" value="<?php echo $lc->descripcion; ?>"/>
		</div>
		<div class="row">
			<label class="row" for="fecha1">
				Primer Vto.:
			</label>
			<input type="text" id="fecha1" name="fecha_venc_1" value="<?php echo $fecha_venc_1; ?>"/>
		</div>
		<div id="nivel_id-div" class="row">
			<label class="row" for="nivel_id">
				Nivel:
			</label>
			<?php $data = CHtml::listData($niveles, "id", "nombre"); ?>
			<?php echo CHtml::dropDownList("nivel_id", Null, $data, array("prompt" => "Todos")); ?>
		</div>
		<div class="row">
			<label class="row" for="fecha2">
				Segundo Vto.:
			</label>
			<input id="fecha2" type="text" name="fecha_venc_2" value="<?php echo $fecha_venc_2; ?>"/>
		</div>
		<div class="row">
			<label class="row" for="matricula">
				<?php $checked = $lc->es_matricula ? "checked" : ""; ?>
				Es matrícula:
			</label>
			<input type="checkbox" id="matricula" name="matricula" <?php echo $checked; ?>/>
		</div>
		<div class="row">
			<label class="row" for="conel100">
				<?php $checked = $lc->conel100 ? "checked" : ""; ?>
				Liquida los 100%?:
			</label>
			<input type="checkbox" id="conel100" name="conel100" <?php echo $checked; ?>/>
		</div>
		<div id="form-buttons" class="hcenter">
			<?php $style = ($lc->fecha_liquidacion) ? "" : "display:none" ?>
			<input id="emision-button" type="button" value="Emitir" onclick="return emision();" style="<?php echo $style; ?>"/>
			<input id="resumen-button" type="button" value="Ver Resumen" onclick="return resumen();" style="<?php echo $style; ?>"/>
			<input id="resumen-button" type="button" value="Impresión" onclick="return resumenImpresion();" style="<?php echo $style; ?>"/>
			<input id="control-button" type="button" value="Comparar" onclick="return comparar();" style="<?php echo $style; ?>"/>
			<input id="cierra-button" type="button" value="Cierra la liquidación" onclick="return cierra()" style="<?php echo $style; ?>"/>
			<?php if (!$lc->isNewRecord): ?>
					<input type="button" value="Liquida" onclick="return liquida();"/>
				<?php endif; ?>
			<input type="button" value="Graba" onclick="return grabaForm();"/>
		</div>
	</form>
</div>
<div id="resumen" class="ui-widget ui-widget-content ui-corner-all">
	<div id="resumen-inner">
	</div>  
</div>
<div id="error-dialog">
</div>
<script type="text/javascript">
	$("#nivel_id").chosen();
	$("input[type='button']").button();
	$("input[name='fecha_venc_1']").datepicker();
	$("input[name='fecha_venc_2']").datepicker();

	function grabaForm() {
		var data = $("form").serialize();
		$.ajax({
			type: "POST",
			dataType: "json",
			data: data,
			url: "<?php echo $this->createUrl("liquidacion/graba"); ?>",
			success: function(data) {
				if (data) {
					muestraErroresDoc(data);
					return false;
				} else {
					window.location = window.location;
				}
			},
			error: function(data, status) {
			}
		});
	}

	function liquida() {
		var conel100 = $("#conel100").is(":checked");
		var nivel_id = $("#nivel_id").val();
		$.ajax({
			type: "GET",
			//      dataType:"json",
			data: {conel100: conel100, nivel_id: nivel_id},
			url: "<?php echo $this->createUrl("liquidacion/liquida"); ?>",
			success: function(data) {
				if (data !== "ok") {
					//          alert("Hubo un error en el proceso de liquidación")
					//          alert(data);
					$("#error-dialog").dialog({
						width: "auto",
						height: "auto",
						position: "center",
						buttons: {
							ok: function() {
								$(this).dialog("close");
							}
						}
					});
					$("#error-dialog").html(data);
					return;
				}
				$("#resumen-button").show();
				$("#cierra-button").show();
				//        alert(data);
				//        $("#resumen").html(data);
				resumen();
			},
			error: function(data, status) {
				alerta(data.responseText, status);
			}
		});
	}

	function resumen() {
		$.ajax({
			type: "GET",
			//      dataType:"json",
			data: {
			},
			url: "<?php echo $this->createUrl("liquidacion/resumen"); ?>",
			success: function(data) {
				$("#resumen-inner").html(data).parent().show();
			},
			error: function(data, status) {
			}
		});
	}
	
	function resumenImpresion() {
		var url = "<?php echo $this->createUrl("liquidacion/resumen"); ?>&impresion=true";
		window.open(url);
	}

	function cierra() {
		$.ajax({
			type: "GET",
			dataType: "json",
			data: {
			},
			url: "<?php echo $this->createUrl("liquidacion/cierraLiq"); ?>",
			success: function(data) {
				if (data.status == "ok") {
					alert("Liquidación confirmada, registros:" + data.registros);
					window.location = window.location;
				} else {
					alert(data);
					window.location = windows.location;
				}
				;
			},
			error: function(data, status) {
			}
		});
	}

	function emision() {
		var nivel_id = $("#nivel_id").val();
		var url = "<?php echo $this->createUrl("/liquidacion/emisionComprobantes"); ?>&nivel_id=" + nivel_id;
		window.open(url);
	}

	function comparar() {
		var url = "<?php echo $this->createUrl("/liquidacion/control"); ?>";
		window.open(url);
	}

</script>
