<style type="text/css">
    .importe{text-align: right}
</style>
<!--
drop function saldo;
CREATE FUNCTION saldo(socio_id int)
 RETURNS DECIMAL(10,2)
READS SQL DATA not deterministic
BEGIN

declare saldo DECIMAL(10,2);
select sum(case when c.signo_cc = 1 then d.total else d.total * -1 end) as total
	into saldo
	from socio s
		inner join doc d on d.Socio_id = s.id
		inner join talonario t on t.id = d.talonario_id
		inner join comprob c on c.id = t.comprob_id
	where s.id = socio_id and d.activo = 1 and d.anulado = 0;
	RETURN round(saldo,2);
END
;-->
<?php $total = 0;?>
<?php $cant = 0;?>
<button id="" onclick="return grabaRecargos();">Graba los recargos</button>
<table>
    <tr>
        <th>Alumno</th>
        <th>Saldo</th>
        <!--<th>Cuota</th>-->
    </tr>
    <?php $importeRecargo = Opcion::getOpcionText("recargo", null, "liquidacion"); ?>
<?php  foreach ($alumnos as $a):;?>
    <tr socio_id="<?php echo $a["socio_id"];?>">
        <td><?php echo $a["nombre"];?></td>
        <td class="importe"><?php echo number_format($a["saldo"],2);?></td>
        <!--<td class="importe"><?php //echo number_format($a["cuota"],2);?></td>-->
    </tr>
    <?php $total += $importeRecargo;?>
    <?php $cant ++;?>
<?php endforeach;?>
    <tr><td>Total recargos</td><td colspan="2" class="importe"><?php echo number_format($total,2);?></td></tr>
    <tr><td>Cantidad recargos</td><td colspan="2" class="importe"><?php echo number_format($cant,0);?></td></tr>
</table>

<script type="text/javascript">
    $("button").button();
    function grabaRecargos(){
        $.ajax({
      type: "GET",
      //      dataType:"json",
      data: {},
      url: "<?php echo $this->createUrl("liquidacion/recargosGraba"); ?>",
      success: function(data) {
        if(data == "ok"){
            alert("Listo...");
        }else{
            alerta(data);
        }
      },
      error: function(data,status){
      }
    }
    );
        return false;
    }
    </script>