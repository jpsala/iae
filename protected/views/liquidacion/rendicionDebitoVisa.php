<?php
$rows = Alumno::getLiquidacionData();
$totalConNegativos = number_format(Alumno::getLiquidacionDataTotalConNegativos(),2);
if ($preview) {
    $cols["main"] = array(
            "doc_id" => array("visible" => false),
            "beca" => array("visible" => false),
            "saldo" => array("visible" => false),
            "monto_novedades" => array("visible" => false),
    );
    $div = Helpers::getTable("main", $rows, $cols);
    $div = "<span>Total para claudia: $totalConNegativos</span>"
            . "<button id=\"submit\" onclick=\"return archivo();\">&nbsp;Bajar Archivo&nbsp;</button>" . $div;
    $this->renderPartial("/layouts/main", array("content" => $div));
} else {
    if ($banco == "Banelco") {
        $archivo = "FAC2057." . date("dmy", time());
    } else {
        $archivo = "V0021555768" . date("ymd", time()) . "_DEUDA.txt";
    }
    header('Content-Type: application/download');
    header('Content-Disposition: filename=' . $archivo);
    $cant = 0;
    $total = 0;
    $cr = "\r\n";
    $lineas = '0' . '400' . '2057' . date("Ymd") . str_repeat("0", 184) . $cr;

    foreach ($rows as $row) {
        /*
          1	CodRegistro	02	9(1)	1	1	Código de Registro.   5 = Detalle
          2	NroReferencia	02	X(19)	2	20	Identificación del cliente en la empresa
          3	IdFactura	02	X(20)	21	40	Identificación de la factura
          4	CodMoneda	02	9(1)	41	41	Código de moneda de los importes informados. 0 = Pesos.
          5	Fecha1erVto	02	9(8)	42	49	Fecha del 1er vencimiento de la factura
          6	Importe1erVto	02	9(9)v(2)	50	60	Importe de la factura para el 1er vencimiento
          7	Fecha2doVto	02	9(8)	61	68	Fecha del 2do vencimiento de la factura
          8	Importe2doVto	02	9(9)v(2)	69	79	Importe de la factura para el 2do vencimiento
          9	Fecha3erVto	02	9(8)	80	87	Fecha del 3er vencimiento de la factura
          10	Importe3erVto	02	9(9)v(2)	88	98	Importe de la factura para el 3er vencimiento
          11	ImporteMínimo	02	9(9)v(2)	99	109	Importe mínimo adeudado.
          12	FechaProxVto	02	9(8)	110	117	Fecha de Próximo vencimiento
          13	NroReferenciaAnt	02	X(19)	118	136	En caso de cambio de la identificación del cliente, se deberá informar la identificación anterior. Caso contrario, se informará la identificación actual.
          14	MensajeATM	02	X(40)	137	176	Datos a informar en el ticket de pago emitido por el cajero automático.
          15	IdentifParticular	02	X(15)	177	191	Datos a informar en la pantalla del cajero en la selección de la factura a pagar.
          16	Filler	02	X(9)	192	200	Campo para uso futuro. Valor fijo: espacios.
         */
        $matricula = str_pad($row["matricula"], 10, "0", STR_PAD_LEFT) . "         "; //NroReferencia	02	X(19)	2	20	Identificación del cliente en la empresa
        $doc_id = str_pad($row["doc_id"], 20, " ", STR_PAD_RIGHT); //IdFactura	02	X(20)	21	40	Identificación de la factura
        $importe1 = importe($row["total"]);
        $importe2 = importe($row["total"] + Opcion::getOpcionText("recargo", Null, "liquidacion"));
        $detalle = strtoupper(str_pad(substr($row["detalle"], 0, 40), 40, " ", STR_PAD_RIGHT));
        $detalle2 = strtoupper(str_pad(substr($row["detalle"], 0, 15), 15, " ", STR_PAD_RIGHT));
        $linea = "5" //Código de Registro.   5 = Detalle
                . $matricula //Identificación del cliente en la empresa
                . $doc_id //IdFactura	02	X(20)	21	40	Identificación de la factura
                . "0" //CodMoneda	02	9(1)	41	41	Código de moneda de los importes informados. 0 = Pesos.
                . date("Ymd", mystrtotime($row["fecha_venc_1"]))
                . $importe1
                . date("Ymd", mystrtotime($row["fecha_venc_2"]))
                . $importe2
                . str_repeat('0', 11) //3er vto fecha
                . str_repeat('0', 8) //3er vto importe
                . $importe1 //importe mínimo
                . str_repeat('0', 8) //fecha próx vto
                . $matricula
                . $detalle
                . $detalle2
                . str_repeat(' ', 9); //Filler	02	X(9)	192	200	Campo para uso futuro. Valor fijo: espacios.
        $lineas.=$linea . $cr;
        $total += $row["total"];
        $cant++;
    }

    $lineas .= '9' . '400' . '2057' . date("Ymd", time())
            . str_pad($cant, 7, "0", STR_PAD_LEFT)
            . str_repeat("0", 7) . importe($total)
            . str_repeat("0", 12)
            . str_repeat("0", 147) . $cr;

    echo $lineas;
    return;
}

function importe($importe) {
    $partes = explode(".", $importe);
    $entero = $partes[0];
    $decimal = (count($partes) == 1) ?
            "00" :
            str_pad($partes[1], 2, "0", STR_PAD_RIGHT);
    return str_pad($entero . $decimal, 11, "0", STR_PAD_LEFT);
}
?>
<script type="text/javascript">
    function archivo() {
        window.location = " <?php echo $this->createUrl("/liquidacion/rendicionBanelcoVisa",array("preview"=>false,"banco"=>$banco)); ?>";
    }
</script>