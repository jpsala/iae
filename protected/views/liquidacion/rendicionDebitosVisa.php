<?php if($preview): ?>
    <style>
        .totales-div { margin: 5px }
        .total-title, .cant-title { font-weight: bold; display: inline-block; width: 150px }
        .total-data, .cant-data { font-weight: bold; }
    </style>
<?php endif; ?>
<?php
    /*
     * Nº       Nivel     Descripción del campo                                                                       Tipo        Posición        Tamaño       Dec.      Red.
                                                                                                                                                         inicial
    1           01        Registro de Resumen                                                                          AN	              1	               100
    2           03        Tipo de Registro (Fijo "0")  			                          AN	              1	                   1
    3           03        Constante  "DEBLIQC"    y un blanco 		                            AN	              2	                   8
    4           03        Número del Establecimiento que generó el archivo                           AN	            10	                 10
    5           03        Constante “900000” 	u cuatro blancos                                      AN	            20	                 10
    6           03        Fecha de generación del archivo (AAAAMMDD)	                             N	            30	                   8
    7           03        Hora de generación del archivo (HHMM)	                             N	            38	                   4
    8           03        Tipo de Archivo. Débitos a liquidar
                             “0”= Altas  			                                               A	            42	                   1
    9           03        Estado archivo - Constante espacios   		                            AN	            43	                   2
    10         03        Reservado – Constante espacios		                            AN	            45	                 55
    11         03        Marca de fin de registro – Constante “*”        	                            AN	           100	                   1

     */

    if($preview) {
        $rows         = Alumno::getLiquidacionDebitosVisaRows(false, $banco);
        $total        = Alumno::getLiquidacionDebitosVisaTotal(false, $banco);
        $cant         = count($rows);
        $cols["main"] = array(
            "doc_id"                  => array(
                "visible" => true, 'title' => '#Facturación'
            ),
            "visa_numero"             => array("visible" => true, 'title'=>'Número'),
            "familia_id"             => array("visible" => true, 'title'=>'Flia. ID'),
            "total"                    => array("visible" => true, 'currency'=>'true'),
            "beca"                    => array("visible" => false),
            "fecha_venc_1"            => array("visible" => false),
            "fecha_venc_2"            => array("visible" => false),
            "nivel"                   => array("visible" => false),
            "anio"                    => array("visible" => false),
            "movimientos_posteriores" => array("visible" => false),
            "division"                => array("visible" => false),
            "saldo"                   => array("visible" => false),
            "saldo_actual"            => array("visible" => false),
            "monto_novedades"         => array("visible" => false),
        );
        $total_str    = number_format($total, 2);
        $div = '<style>';
        $div .= '.main-tablegen-familia_id-td{width:50px}';
        $div .= '.main-tablegen-matricula-td{width:50px}';
        $div .= '.main-tablegen-total-td{text-align:right; width:45px}';
        $div .= '.main-tablegen-alumno-td{width:180px}';
        $div .= '.main-tablegen-visa_numero-td{width:100px}';
        $div .= '.main-tablegen-detalle-td{width:60px}';
        $div .= '</style>';
        $div .= Helpers::getTable("main", $rows, $cols);
        $div .= "<div class='totales-div'>";
        $div .= "<span class='total-title'>Total:</span>";
        $div .= "<span class='total-data'>$total_str</span><br/>";
        $div .= "<span class='cant-title'>Cantidad de registros:</span>";
        $div .= "<span class='cant-data'>$cant</span>";
        $div .= "</div>";
        $div .= "<button class='btn btn-small' onclick='return archivo();'>Bajar archivo</button>";
        $this->renderPartial("/layouts/main", array("content" => $div));
    } else {
        $rows                  = Alumno::getLiquidacionDebitosVisaRows(true, $banco);
        $numeroEstablecimiento = $banco == "superville"? "0031552136":"0018228940";
        $archivo               = "DEBLIQC.TXT";
        header('Content-Type: application/download');
        header('Content-Disposition: filename=' . $archivo);
        $cant   = 0;
        $total  = 0;
        $cr     = "\r\n";
        $lineas = '0'
            . 'DEBLIQC '
            . $numeroEstablecimiento
            . "900000    "
            . date("YmdHi")
            . "0"
            . "  "
            . str_repeat(" ", 55)
            . "*" . $cr;

        foreach ($rows as $row) {
            $doc_id = $row["doc_id"];
            $linea  = "1"
                . str_replace(" ", "", $row["visa_numero"])
                . "   "
                . str_pad($doc_id, 8, "0", STR_PAD_LEFT)
                . date("Ymd", mystrtotime($row["fecha_venc_1"]))
                . "0005"
                . importe($row["total"], 15)
                . str_pad($doc_id, 15, "0", STR_PAD_LEFT)
                . "E"
                . "  "
                . str_repeat(' ', 26)
                . "*";
            $lineas .= $linea . $cr;
            $total += $row["total"];
            $cant++;
        }

        $lineas .= "9"
            . "DEBLIQC "
            . $numeroEstablecimiento
            . "900000    "
            . date("YmdHi")
            . str_pad($cant, 7, "0", STR_PAD_LEFT)
            . importe($total, 15)
            . str_repeat(' ', 36)
            . "*"
            . $cr;


        echo $lineas;

        return;
    }

    function importe($importe, $len = 11) {
        $partes  = explode(".", $importe);
        $entero  = $partes[0];
        $decimal = (count($partes) == 1) ?
            "00" :
            str_pad($partes[1], 2, "0", STR_PAD_RIGHT);

        return str_pad($entero . $decimal, $len, "0", STR_PAD_LEFT);
    }

?>
<script type="text/javascript">
    function archivo() {
        window.location = " <?php echo $this->createUrl("/liquidacion/rendicionDebitosVisa",array("preview"=>false, "banco"=>$banco)); ?>";
    }
</script>