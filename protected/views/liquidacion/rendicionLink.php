<?php if ($accion == ""): ?>
    <style type="text/css">
        button{width: 80px; margin: 10px}
    </style>
    <div class="ui-widget ui-widget-default ui-corner-all">
        <div class="titulo">Rendición Link</div>
        <div id="buttons" class="ui-widget ui-widget-default ui-corner-all">
            <button onclick="return submitAccion('cabecera');">Archivo Cabecera</button>
            <div>
                <button onclick="return submitAccion('detalle');">Archivo Detalle</button>
            </div>
        </div>
    </div>
    <?php
else:
    define('CRLF', "\r\n");
    /*
     * DecodeDate(fecha, Year, Month, Day);
      if month <= 9 then
      mesParaBanco := trim(IntToStr(month))
      else if month = 10 then
      mesParaBanco :=  'A'
      else if month = 11 then
      mesParaBanco :=  'B'
      else if month = 12 then
      mesParaBanco :=  'C';
      refreshFileName := 'P7940' + MesParaBanco + FormatDateTime('DD', fecha);
      end;
     */
    // cab:C7940628
    $mes = date("m", time()) + 0;
    switch ($mes) {
        case $mes <= 9:
            $mesParaBanco = $mes;
            break;
        case 10:
            $mesParaBanco = 'A';
            break;
        case 11:
            $mesParaBanco = 'B';
            break;
        case 12:
            $mesParaBanco = 'C';
            break;
    }
    $cant = 0;
    $total = 0;
//    $lineas = '0' . '400' . '2057' . date("Ymd") . str_repeat("0", 184) . CRLF;
    if ($accion == "cabecera") {
        $archivo = 'C7940' . $mesParaBanco . date('d', time());
        $archivoDetalle = 'P7940' . $mesParaBanco . date('d', time());
        $linea1 = "HRPASCTRL"
                . date("Ymd", time())
                . "794"
                . $archivoDetalle
                . str_pad(($row["cant"]+2) * 133, 10, "0", STR_PAD_LEFT)
                . str_repeat(" ", 37)
                . CRLF;
        $linea2 = "LOTES"
                . "00000"
                . str_pad($row["cant"] + 2, 8, "0", STR_PAD_LEFT)
                . "000000" . importe($row["total"])
                . "000000" . importe($row["total"] + (Opcion::getOpcionText("recargo", Null, "liquidacion") * $row["cant"]))
                . str_repeat("0", 18)
                . str_repeat("0", 3)
                . CRLF;
        $linea3 = "FINAL"
                . str_pad(($row["cant"] + 2), 8, "0", STR_PAD_LEFT)
                . "000000" . importe($row["total"])
                . "000000" . importe($row["total"] + (Opcion::getOpcionText("recargo", Null, "liquidacion") * $row["cant"]))
                . str_repeat("0", 18)
                . date("Ymd", mystrtotime($row["fecha_vto2"]));
        $lineas = $linea1 . $linea2 . $linea3;
        header('Content-Type: application/download');
        header('Content-Disposition: filename=' . $archivo);
        echo $lineas;
    } elseif ($accion == "detalle") {
        $lineas = 'HRFACTURACION'
                . '794'
                . date('ymd', time())
                . '00001'
                . str_repeat(' ', 104)
                . CRLF;
        foreach ($rows as $row) {
            $lineas .= str_pad(substr($row["doc_id"], -5), 5, "0", STR_PAD_LEFT)
                    . "001"
                    . str_pad("0000" . $row["matricula"], 19, " ", STR_PAD_RIGHT)
                    . date("ymd", mystrtotime($row["fecha_venc_1"]))
                    . importe($row["total"])
                    . date("ymd", mystrtotime($row["fecha_venc_2"]))
                    . importe($row["total"] + Opcion::getOpcionText("recargo", Null, "liquidacion"))
                    . str_repeat("0", 18)
                    . str_pad($row["doc_id"], 50, "0", STR_PAD_LEFT)
                    . CRLF;
            $total += $row["total"];
            $cant++;
        }
        $lineas .= "TRFACTURACION"
                . str_pad($cant + 2, 8, "0", STR_PAD_LEFT)
                . "000000" . importe($total)
                . "000000" . importe($total+(Opcion::getOpcionText("recargo", Null, "liquidacion")*$cant))
                . str_repeat("0", 18)
                . str_repeat(" ", 56);
        //vd($total,$cant,$total+(Opcion::getOpcionText("recargo", Null, "liquidacion")*$cant));

        $archivo = 'P7940' . $mesParaBanco . date('d', time());
        header('Content-Type: application/download');
        header('Content-Disposition: filename=' . $archivo);
        echo $lineas;
    }


endif;

function importe($importe) {
    $partes = explode(".", $importe);
    $entero = $partes[0];
    $decimal = (count($partes) == 1) ?
            "00" :
            str_pad($partes[1], 2, "0", STR_PAD_RIGHT);
    return str_pad($entero . $decimal, 12, "0", STR_PAD_LEFT);
}
?>

<?php if ($accion == ""): ?>
    <script type="text/javascript">
                $("button").button();
                function submitAccion(accion) {
                    window.location = "<?php echo $this->createUrl("/liquidacion/rendicionLink") . "&accion="; ?>" + accion;
                }
    </script>
<?php endif; ?>
