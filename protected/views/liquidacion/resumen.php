<?php $totalGral = 0; ?>
<style>
	/*.detalle-td{width:200px}*/
</style>
<?php if ($impresion): ?>
		<!DOCTYPE html>
		<html lang="es">
			<head>
				<meta charset="utf-8">
			<h2> Liquidación <?php echo $descripcion; ?></h2>
			<style>
				table{width:100%}
				.ui-widget{font-weight: bolder;border:#000 solid thin; padding: 5px; display:inline-block; width:100%}
				th,td{text-align: left}
				.total, .alignright{width:50mm;text-align: right}
				.totalH1{font-weight: bold}
				.totalNum{font-weight: bold}
				/*				tr>td{min-width:40mm}
								tr>th{min-width:40mm}*/
				tr>td{min-width:200px}
			</style>
		</head>
	<?php endif; ?>
<table>
	<tr>
		<td colspan="4" class="nivel-td"><div class="ui-widget ui-widget-content ui-corner-all nivel-div">Cuota</div></td>
	</tr>
	<tr>
		<th>Detalle</th>
		<th></th>
		<th class="alignright">Cantidad</th>
		<th class="alignright">Importe</th>
	</tr>
	<?php $total = 0; ?>
	<?php $cant = 0; ?>
	<?php $total_nivel = 0; ?>
	<?php foreach ($cuotas as $row): ?>
			<tr>
				<td><?php echo $row["detalle"]; ?></td>
				<td></td>
				<td class="total"><?php echo $row["cant"]; ?></td>
				<td class="total"><?php echo number_format($row["total"], 2, ",", "."); ?></td>
			</tr>
			<?php $total+= $row["total"]; ?>
			<?php $cant+= $row["cant"]; ?>
		<?php endforeach; ?>
	<tr>
		<td class="totalH1">Total Cuota</td>
		<td></td>
		<td class="totalNum alignright"><?php echo $cant; ?></td>
		<td class="totalNum alignright"><?php echo number_format($total, 2, ",", "."); ?></td>
	</tr>
	<tr><td colspan="4" >&nbsp;</td></tr>
	<?php $totalGral += $total; ?>
	<?php $total = 0; ?>
	<?php $cant = 0; ?>
	<tr>
		<td colspan="4" class="nivel-td"><div class="ui-widget ui-widget-content ui-corner-all nivel-div">Beca</div></td>
	</tr>
	<tr>
		<th>Detalle</th>
		<th></th>
		<th class="alignright">Cantidad</th>
		<th class="alignright">Importe</th>
	</tr>
	<?php foreach ($becas as $row): ?>
			<tr>
				<td><?php echo $row["detalle"]; ?></td>
				<td></td>
				<td class="total"><?php echo $row["cant"]; ?></td>
				<td class="total"><?php echo "(" . number_format($row["total"], 2, ",", ".") . ")"; ?></td>
			</tr>
			<?php $total+= $row["total"]; ?>
			<?php $cant+= $row["cant"]; ?>
		<?php endforeach; ?>
	<?php $totalGral -= $total; ?>
	<tr>
		<td class="totalH1">Total Becas</td>
		<td></td>
		<td class="totalH1"></td>
		<td class="totalNum alignright"><?php echo number_format($total, 2, ",", "."); ?></td>
	</tr>
	<tr><td colspan="3">&nbsp;</td></tr>
	<tr id="interes-tr">
		<td colspan="4" class="nivel-td"><div class="ui-widget ui-widget-content ui-corner-all nivel-div">Intereses</div></td>
	</tr>
	<tr>
		<td class="totalH1">Total Intereses</td>
		<td class=""></td>
		<td class="totalH1"></td>
		<td class="totalNum alignright"><?php echo number_format($intereses, 2, ",", "."); ?></td>
	</tr>
	<?php $totalGral += $intereses; ?>
	<?php
		$nivel = "";
		$totalNivel = 0
	?>

	<tr><td colspan="4" >&nbsp;</td></tr>
	<tr>
		<td colspan="4" class="nivel-td"><div class="ui-widget ui-widget-content ui-corner-all nivel-div">Total Novedades</div></td>
	</tr>
	<tr>
		<th>Detalle</th>
		<th></th>
		<th class="alignright">Cantidad</th>
		<th class="alignright">Importe</th>
	</tr>
	<?php $totalNovedades = 0; ?>
	<?php $cantNovedades = 0; ?>
	<?php foreach ($novedades as $row): ?>
			<tr>
				<td><?php echo $row["detalle"]; ?></td>
				<td></td>
				<td class="alignright total"><?php echo $row["cant"]; ?></td>
				<td class="alignright total"><?php echo number_format($row["total"], 2, ",", "."); ?></td>
			</tr>
			<?php $totalNovedades+= $row["total"]; ?>
			<?php $cantNovedades+= $row["cant"]; ?>
		<?php endforeach; ?>
	<tr>
		<td class="totalH1"></td>
		<td></td>
		<td class="totalNum alignright"><?php echo $cantNovedades; ?></td>
		<td class="totalNum alignright"><?php echo number_format($totalNovedades, 2, ",", "."); ?></td>
		<?php $totalGral+=$totalNovedades; ?>
	</tr>

	<!--Acá van los saldos anteriores-->

	<tr>
		<td colspan="4" class="nivel-td"><div class="ui-widget ui-widget-content ui-corner-all nivel-div">Total General</div></td>
	<tr>
		<td colspan="4" class="totalNum alignright"><?php echo number_format($totalGral, 2, ",", "."); ?></td>
	</tr>
	<!--Aca va lo cobrado por adelantado
		Está abajo
	-->
</table>

<tcpdf method="AddPage" />

<table>
	<?php foreach (array_keys($novedadesPorNivel) as $nivel): ?>

			<tr>
				<td colspan="4" class="nivel-td"><div class="ui-widget ui-widget-content ui-corner-all nivel-div">Novedades <?php echo $nivel; ?></div></td>
			</tr>
			<?php $total = 0; ?>
			<?php foreach ($novedadesPorNivel[$nivel] as $n): ?>
				<?php //bzx720;   ?>
				<tr>
					<td><?php echo $n["nivel"]; ?></td>
					<td><?php echo $n["detalle"]; ?></td>
					<?php $abre = $n["total"] < 0 ? "(" : ""; ?>
					<?php $cierra = $n["total"] < 0 ? ")" : ""; ?>
					<?php $total += $n["total"]; ?>
					<?php $totalGral += $n["total"]; ?>
					<td class=" alignright"><?php echo $n["cant"]; ?></td>
					<td class=" alignright"><?php echo $abre . number_format($n["total"], 2, ",", ".") . $cierra; ?></td>
				</tr>
			<?php endforeach; ?>
			<tr>
				<td colspan="3" class="totalH1">Total</td>
				<td class="totalNum alignright"><?php echo number_format($total, 2, ",", "."); ?></td>
			</tr>
		<?php endforeach; ?>
</table>
<?php
	$nivel = "";
	$totalNivel = 0
?>

<!--Detalle de beca-->
<table id="resumen-table" style="display:none">
	<tr>
		<td colspan="3" class="nivel-td"><div class="ui-widget ui-widget-content ui-corner-all nivel-div">Detalle Beca</div></td>
	</tr>
	<tr>
		<th>Alumno</th>
		<th>Beca</th>
		<th class="alignright">Importe</th>
	</tr>
	<?php $total = 0; ?>
	<?php $cant = 0; ?>
	<?php foreach ($becasDetalle as $row): ?>
			<tr>
				<td><?php echo $row["alumno"]; ?></td>
				<td class="total"><?php echo $row["beca"]; ?></td>
				<td class="total"><?php echo number_format($row["importeBeca"], 2, ",", "."); ?></td>
			</tr>
			<?php $total+= $row["importeBeca"]; ?>
			<?php $cant++ ?>
		<?php endforeach; ?>
	<tr>
		<td class=""></td>
		<td class="totalNum alignright"><?php echo $cant; ?></td>
		<td class="totalNum alignright"><?php echo number_format($total, 2, ",", "."); ?></td>
	</tr>
</table>




<table style="display:nonee">
	<tr><td colspan="4" >&nbsp;</td></tr>
	<tr>
		<td colspan="4" class="nivel-td"><div class="ui-widget ui-widget-content ui-corner-all nivel-div">Total Gral. sin saldos anteriores</div></td>
	</tr>
	<tr>
		<td class="totalH1" colspan="3"></td>
		<td class="totalNum alignright"><?php echo number_format($totalGral, 2, ",", "."); ?></td>
	</tr>
	<tr><td colspan="4" >&nbsp;</td></tr>
	<tr>
		<td colspan="4" class="nivel-td"><div class="ui-widget ui-widget-content ui-corner-all nivel-div">Cobrado x Adelantado</div></td>
	</tr>
	<tr><td colspan="4" >&nbsp;</td></tr>

	<tr>
		<th>Detalle</th>
		<th></th>
		<th class="alignright">Cantidad</th>
		<th class="alignright">Importe</th>
	</tr>
	<?php $cant = 0; ?>
	<?php $total = 0; ?>
	<?php $total_nivel = 0; ?>
	<?php foreach ($saldosNegativos as $row): ?>
			<?php $cant += $row["cant"]; ?>
			<tr>
				<td><?php echo $row["detalle"]; ?></td>
				<td class="detalle-td"></td>
				<td class="alignright total"><?php echo $row["cant"]; ?></td>
				<td class="alignright total"><?php echo number_format($row["total"], 2, ",", "."); ?></td>
			</tr>
			<?php $totalGral+= $row["total"]; ?>
			<?php $total+= $row["total"]; ?>
		<?php endforeach; ?>
	<tr>
		<td class=""></td>
		<td class=""></td>
		<td class="totalNum alignright"><?php echo $cant; ?></td>
		<td class="totalNum alignright"><?php echo number_format($total, 2, ",", "."); ?></td>
	</tr>

	<tr><td colspan="4" >&nbsp;</td></tr>
	<tr>
		<td colspan="4" class="nivel-td"><div class="ui-widget ui-widget-content ui-corner-all nivel-div">Total General menos adelantado</div></td>
	<tr>
		<td colspan="4" class="totalNum alignright"><?php echo number_format($totalGral, 2, ",", "."); ?></td>
	</tr>

</table> <!-- adelantado -->

<table style="display:nonee">
	<tr>
		<td colspan="4" class="nivel-td"><div class="ui-widget ui-widget-content ui-corner-all nivel-div">Saldos Anteriores</div></td>
	</tr>

	<tr>
		<th>Detalle</th>
		<th></th>
		<th class="alignright">Cantidad</th>
		<th class="alignright">Importe</th>
	</tr>
	<?php $cant = 0; ?>
	<?php $total = 0; ?>
	<?php $total_nivel = 0; ?>
	<?php foreach ($saldos as $row): ?>
			<?php $cant += $row["cant"]; ?>
			<tr>
				<td><?php echo $row["detalle"]; ?></td>
				<td class="detalle-td"></td>
				<td class="alignright total"><?php echo $row["cant"]; ?></td>
				<td class="alignright total"><?php echo number_format($row["total"], 2, ",", "."); ?></td>
			</tr>
			<?php $totalGral+= $row["total"]; ?>
			<?php $total+= $row["total"]; ?>
		<?php endforeach; ?>
	<tr>
		<td class=""></td>
		<td class=""></td>
		<td class="totalNum alignright"><?php echo $cant; ?></td>
		<td class="totalNum alignright"><?php echo number_format($total, 2, ",", "."); ?></td>
	</tr>
</table>