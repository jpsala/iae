<style type="text/css">
  #doc-div{}
  #doc-table{width: 700px}
  #doc-table .alumno{width:200px}
  #det-table{margin-right: 10px;width: auto}
  #det-table .articulo{width: 200px}
  #det-table .cantidad{width: 20px}
  #det-table .total{width: 80px; text-align: right}
  .nivel-tr{ color: lightsalmon; font-size: 120%; font-weight: 900; padding-left: 10px}
  .anio-tr{ color: lightsalmon; font-size: 120%; font-weight: 400; padding-left: 50px}
  .division-tr{ color: lightsalmon; font-size: 120%; font-weight: 400; padding-left: 90px}
</style>
<?php $docs = Yii::app()->db->createCommand("
      select concat(al.apellido,\", \",al.nombre) as alumno,d.id as doc_id, d.detalle, d.total+d.liquid_saldo_ant as total_doc, d.saldo, d.fecha_creacion,
          liquid_saldo_ant, ni.nombre as nivel, ni.orden as orden_nivel, an.nombre as anio, an.orden as anio_orden,
                  di.nombre as division
        from doc d
            inner join socio s on s.id = d.Socio_id
            inner join alumno al on al.id = s.Alumno_id
						  inner join alumno_estado ae on ae.id = al.estado_id and ae.activo_admin = 1
						  inner join alumno_estado ae on ae.id = al.estado_id and ae.activo_admin = 1
                          inner join alumno_division ad on ad.Alumno_id = al.id
                          inner join division di on di.id = ad.Division_id
                          inner join anio an on an.id = di.Anio_id
                          inner join nivel ni on ni.id = an.Nivel_id
        where d.liquid_conf_id = (select max(id) from liquid_conf)
        order by ni.orden, an.orden, di.nombre, al.apellido, al.nombre
      ")->queryAll();?>

<div id="doc-div">
  <table id="doc-table" class="ui-widget-content ui-corner-all">
    <tr>
      <th class="alumno">Alumno</th>
      <th>Detalle</th>
      <th>Total</th>
      <th>Saldo</th>
      <th>Fecha de creación</th>
    </tr>
    <?php
    $nivelAnt = null;
    $divisionAnt = null;
    $anioAnt = null;
    $totalDiv = 0;
    ;
    ?>
    <?php foreach ($docs as $doc):
      ?>
      <?php if ($nivelAnt <> $doc["nivel"]): ?>
        <tr><td class="nivel-tr" colspan="3"><?php echo $doc["nivel"]; ?></td></tr>
        <?php $nivelAnt = $doc["nivel"]; ?>
        <tr><td class="anio-tr" colspan="3"><?php echo $doc["anio"]; ?></td></tr>
        <?php $anioAnt = $doc["anio"]; ?>
        <tr><td class="division-tr" colspan="3"><?php echo $doc["division"]; ?></td></tr>
        <?php $divisionAnt = $doc["division"]; ?>
      <?php endif; ?>
      <?php if ($anioAnt <> $doc["anio"]): ?>
        <tr><td class="anio-tr" colspan="3"><?php echo $doc["anio"]; ?></td></tr>
        <?php $anioAnt = $doc["anio"]; ?>
        <tr><td class="division-tr" colspan="3"><?php echo $doc["division"]; ?></td></tr>
        <?php $divisionAnt = $doc["division"]; ?>
      <?php endif; ?>
      <?php if ($divisionAnt <> $doc["division"]): ?>
        <tr>
          <td class="division-tr" colspan="2"><?php echo "Total 12345 " . $divisionAnt; ?></td>
          <td><?php echo number_format($totalDiv, 2, ",", "."); ?></td>
        </tr>
        <?php $totalDiv = 0; ?>
        <tr>
          <td class="division-tr" colspan="1"><?php echo $doc["division"]; ?></td>
          <td><?php echo number_format($totalDiv, 2, ",", "."); ?></td>
          <td>12345</td>
        </tr>
        <?php $divisionAnt = $doc["division"]; ?>
      <?php endif; ?>
      <tr>
        <td class="alumno"><?php echo $doc["alumno"]; ?></td>
        <td><?php echo $doc["detalle"]; ?></td>
        <td><?php echo $doc["total_doc"]; ?></td>
        <td><?php echo $doc["saldo"]; ?></td>
        <td><?php echo $doc["fecha_creacion"]; ?></td>
      </tr>
      <tr>
        <td colspan="5">
          <div id="det-div"  class="ui-corner-all ui-widget-content">
            <table id="det-table">
              <tr>
                <td class="cantidad"><?php echo ""; ?></td>
                <td class="articulo"><?php echo "Saldo anterior"; ?></td>
                <td class="total"><?php echo $doc["liquid_saldo_ant"]; ?></td>
              </tr>
              <?php
              $doc_id = $doc["doc_id"];
              $dets = Yii::app()->db->createCommand("
      select d.detalle, a.nombre as articulo, a.afectacion as afectacion, det.cantidad, det.importe
          from doc d
              inner join doc_det det on det.Doc_id = d.id
              inner join articulo a on a.id = det.articulo_id
              inner join socio s on s.id = d.Socio_id
              inner join alumno al on al.id = s.Alumno_id
						  inner join alumno_estado ae on ae.id = al.estado_id and ae.activo_admin = 1
          where det.Doc_id = $doc_id;
          order by d.id desc
      ")->queryAll();
              foreach ($dets as $det):
                ?>
                <tr>
                  <td class="cantidad"><?php echo $det["cantidad"]; ?></td>
                  <td class="articulo"><?php echo $det["articulo"]; ?></td>
                  <td class="total"><?php echo $det["importe"]; ?></td>
                </tr>
              <?php endforeach; ?>
            </table>
          </div></td>
      </tr>
      <?php $totalDiv += $doc["total_doc"]; ?>
      <tr>
        <td class="division-tr" colspan="2"><?php echo "subtotal "; ?></td>
        <td><?php echo number_format($totalDiv, 2, ",", "."); ?></td>
      </tr>
    <?php endforeach; ?>

  </table>
</div>
