<style>
    #logica_id{width:250px}
</style>
<?php
$li = Ciclo::getActivoId();
$logicas = Logica::model()->findAllBySql("
        select l.id, l.nombre
	from logica l
		inner join logica_ciclo lc on lc.Logica_id = l.id and lc.Ciclo_id = $li
    ");
$opt = array("prompt" => "");
$options = CHtml::listOptions(null, CHtml::listData($logicas, "id", "nombre"), $opt);
?>
<select id="logica_id" name="logica_id" data-placeholder="Seleccione una lógica">
    <?php echo $options; ?>
</select>
<div id="items"></div>

<script>
    $("#logica_id").chosen().change(function(x) {
        $s = $(x.currentTarget);
        $.ajax({
            type: "GET",
            //      dataType:"json",
            data: {logica_id: $s.val()},
            url: "<?php echo $this->createUrl("logica/itemsAjax"); ?>",
            success: function(data) {
                $("#items").html(data);
                init();
            },
            error: function(data, status) {
            }
        }
        );
    }
    );
    $("#logica_id_chzn").mousedown();

    function init() {
        $("tr").draggable({
            appendTo: "body",
            helper: "clone"
        });
        var items = $("tr");
        items.droppable({
            activeClass: "ui-state-default",
            hoverClass: "ui-state-active",
            accept: ":not(.ui-sortable-helper)",
            drop: function(event, ui) {
                $('.placeholder').remove();
                row = ui.draggable;
                $(this).before(row);
                setTimeout(function() {
                    grabaNuevoOrden();
                }, 400);
            }
        });
    }
    function grabaNuevoOrden() {
        items = $("tr");
        var linkItems = [items.size()];
        var index = 0;
        items.each(
                function() {
                    var logica_item_id = $(this).attr("logica_item_id");
                    if (logica_item_id && Number(logica_item_id)) {
                        linkItems[index++] = logica_item_id;
                    }
                }
        );
        var logica_items = linkItems.join(",");
        $.ajax({
            type: "POST",
            data: {logica_items: logica_items, logica_id: logica_id},
            url: "<?php echo $this->createUrl("logica/itemsGrabaOrden"); ?>",
            success: function(data) {
            },
            error: function(data, status) {
            }
        });
    }

</script>