<script src="js/jquery.ajaxmock.min.js"></script>
<?php include("index_bindings.js.php"); ?>

<style>
	.sortable li span{cursor:pointer}
	.test{color:red}
	.active{background-color: red;}
	body.dragging, body.dragging * {
		cursor: move !important;
	}
	.dragged {
		position: absolute;
		opacity: 0.5;
		z-index: 2000;
		min-height: 40px;

	}
	ol li.placeholder {
		position: relative;
		list-style-type: circle;
		list-style-position: inside;
		content: "Chapter: ";
		display: inline;
		background-color: red;
		min-height: 40px;
		/** More li styles **/
	}
	ol li.placeholder:before {
		content: " ";
		border-radius: 5px;
		display: block;
		height: 5px;
		width: 5px;
		background-color: lightcoral;
		z-index: 1000;
		/*position: absolute;*/
		/** Define arrowhead **/
	}
	submit{width:20%}
</style>

<div class="container-fluid">
	<div id="tree-div" class="tree col-xs-6">
		<ol class="sortable divider-vertical" data-bind="template: { name: 'nodeTmpl', foreach: tree }"></ol>
		<script id="nodeTmpl" type="text/html">
			<li data-bind="attr:{id:id}" class=" divider-vertical" >
				<span>
					<i  data-bind="click:function(data, event) { $root.borraMenu(data, event, $parent); }" class="fa fa-trash-o borra"></i>
						<!--<span class="borra" data-bind="click:function(data, event) { $root.borraMenu(data, event, $parent); }"></span>-->
					<span data-bind="text:texto"></span>
				</span>
				<ol class="divider-vertical" data-bind="template: { name: 'nodeTmpl', foreach: children }"></ol>
			</li>
			</script>
			<div  class="liast-group-menu divider-vertical" >
				<form class="form row" role="form" data-bind="submit: addMenu">
					<div class="col-xs-10">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-plus fa-fw"></i></span>
							<input id="nuevo-menu" class="form-control" type="text" placeholder="Nuevo menu" data-bind="value: nuevoMenu, valueUpdate: 'afterkeydown'" placeholder="Nuevo menu">
						</div>
					</div>
					<div class="col-xs-2 row">
						<button type="submit" class="btn btn-default" data-bind="enable: nuevoMenu().length > 0">Add Item</button>
					</div>
				</form>
			</div>
		</div>
		<div id="items-div" class="items col-xs-6">
			<ul id="items-li" class="lisat-group sortable  divider-vertical" data-bind="template: { name: 'miTmpl', foreach: items }"></ul>
			<script id="miTmpl" type="text/html">
				<li data-bind="attr:{menuitem_id:id}" class="lisat-group-item divider-vertical" >
					<span><i  data-bind="click:$parent.borraItem" class="fa fa-trash-o borra"></i></span>
					<span  data-bind="text:texto"></span>
				</li>
				</script>
				<div class="container-fluid">
					<div  class="liast-group-item divider-vertical" >
						<form class="form" role="form" data-bind="submit: addItem">
							<div class="col-xs-10 row">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-plus fa-fw"></i></span>
									<input id="nuevo-item" class="form-control" type="text" placeholder="Nuevo Item" data-bind="value: nuevoItem, valueUpdate: 'afterkeydown'" placeholder="Nuevo item">
								</div>
							</div>
							<div class="col-xs-2 row pull-right row">
								<button type="submit" class="btn btn-default pull-right" data-bind="enable: nuevoItem().length > 0">Add Item</button>
							</div>
						</form>
					</div>
				</div>
				<div class="col-xs-12">
					<div class=" top10">
						<div class="alert alert-success text-center ">
							Items Nuevos:<strong><!--ko text: itemsNuevos().length --><!--/ko--></strong>
						</div>
					</div>				
				</div>				
			</div>
		</div>

		<!--Drag and Drop-->
		<script  type="text/javascript" >
			var treeData = <?php echo json_encode($tree); ?>;
			var menuItems = <?php echo json_encode($menuItems); ?>;
			vmTree = initTree(treeData, document.getElementById("tree-div"));
			vmItems = initMenuItems(menuItems, document.getElementById("items-div"));
			init();

			function init() {
				$('.sortable').sortable({
					group: 'nested',
					onDrop: function(item, container, _super) {
						var parent_id = $(container.el).closest("li").attr("id");
						var menuitem_id = $(item).attr("menuitem_id");
						if (menuitem_id) {
							$.post("<?php echo $this->createUrl("menu/guardaMenuItem"); ?>", {parent_id: parent_id, menuitem_id: menuitem_id});
						} else {
							var pars = "&parent_id="+parent_id;
							$(container.el).children("li").each(function(e,obj){
								pars += "&id[]="+$(obj).attr("id");
							});
							$.get("<?php echo $this->createUrl("menu/guardaMenu"); ?>"+pars).then(function(a){console.log(a);});
						}
						_super(item);
					}
				});
			}
		</script>