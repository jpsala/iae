<script>
	//	Test Deferred
//	doDef().done(function(a) {
//		console.log("done", a);
//	});
//
//	function doDef() {
//		var def = $.Deferred();
//		setTimeout(function() {
//			def.resolve(5);
//		}, 2000);
//		return def;
//	}

	//Knockout
	function initTree(treeData, el) {

		this.parseTree = function(tree, node) {
			var nodes = [];
			$.each(tree, function(index, value) {
				var nodo = new Node(value.texto, value.id);
				nodo.children = ko.observableArray(parseTreeChildren(value.children, node));
				nodes.push(nodo);
			});
			node.children = ko.observableArray(nodes);
		};
		this.parseTreeChildren = function(tree) {
			var nodes = [];
			$.each(tree, function(index, value) {
				var nodec = new Node(value.texto, value.id);
				nodec.children = ko.observableArray(parseTreeChildren(value.children));
				nodec.children.subscribe(function(a, b, c, d) {
					console.log(99, a, b, c, d);
				});
				nodes.push(nodec);
			});
			return nodes;
		};
		var Node = function(texto, id, children) {
			var self = this;
			self.texto = ko.observable(texto);
			self.id = ko.observable(id);
			self.children = ko.observableArray(children);
			self.children.subscribe(function(a, b, c) {
				console.log("subs", a, b, c, d);
			});
		};
		var node = new Node("Menu", 0, []);
		this.parseTree(treeData, node);
		function ViewModel() {
			var self = this;
			self.tree = ko.observable(node);
			self.nuevoMenu = ko.observable("");
			this.addMenu = function(b) {
				$.get("<?php echo $this->createUrl("addMenu"); ?>", {texto: self.nuevoMenu()})
						.then(function(id) {
							var node = new Node(self.nuevoMenu(), id, []);
							console.log(self);
							self.tree().children.push(node);
							self.nuevoMenu("");
						});
			};
			this.borraMenu = function(el, event, parent) {
				$.get("<?php echo $this->createUrl("borraMenu"); ?>", {id: el.id()})
						.then(function() {
							console.log(self);
							parent.children.remove(el);
						});
			};
			self.borra = function(a, b, c, d) {
				var node = ko.dataFor(b.target);
				var parent = ko.contextFor(b.target).$parent;
				parent.children.remove(node);
				return;
			};
		}

		viewModel = new ViewModel;
		ko.applyBindings(viewModel, el);
		return viewModel;
	}

	function initMenuItems(menuItems, el) {
		var items = [];
		$.each(menuItems, function(i, e) {
			items.push(new Item(e.id, e.texto, e.activo, false));
		});

		function Item(id, texto, activo, nuevo) {
			this.texto = ko.observable(texto);
			this.id = ko.observable(id);
			this.activo = ko.observable(activo);
			this.nuevo = ko.observable(nuevo === undefined ? false : nuevo);
		}

		function VmItems(items) {
			self = this;
			this.items = ko.observableArray(items);
			this.nuevoItem = ko.observable("");
			this.itemsNuevos = this.items.filter(function(a) {
				return a.nuevo();
			});
			this.testcomp = ko.computed(function() {
				return self.itemsNuevos().length;
			});
			this.addItem = function() {
				var partes = self.nuevoItem().split(",");
				var texto = partes[0];
				var detalle = partes[1] || "";
				var url = partes[2] || "";
				$.get("<?php echo $this->createUrl("addItem"); ?>", {texto: texto, detalle: detalle, url:url})
						.then(function(id) {
							self.items.push(new Item(id, detalle !== "" ? detalle : texto, undefined, true));
							self.nuevoItem("");
						});
			};
			this.borraItem = function() {
				var e = this;
				$.get("<?php echo $this->createUrl("borraMenuItem"); ?>", {id: e.id()})
						.then(function() {
							self.items.remove(e);
						});
			};
		}

		vmItems = new VmItems(items);
		ko.applyBindings(vmItems, el);
		return vmItems;
	}


</script>