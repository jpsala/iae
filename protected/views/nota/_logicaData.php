<style type="text/css">
  .formula{width: 100%; height: 500px}
  .logica-item-div{padding: 5px; overflow: auto}
  .logica-item-div .formula{height: 350px}
  #item-nuevo{ cursor:pointer}
  .item-nombre{width:250px}
  .periodo-fecha{width:100px; margin-right: 10px}
  .periodo-abreviatura{width:100px; margin-right: 10px}
  .periodo-orden{width:25px}
  .label-periodo-fecha{width:95px; display: inline-block}
  .label-periodo-abreviatura{width:91px; display: inline-block}
  .label-periodo-orden{width:48px; display: inline-block}
  .logica-item-periodos{display: inline-block}
  .logica-item-periodos-label{display: inline-block; margin-left: 10px}
  .logica-item-abreviatura-label{width:80px; margin-left: 10px; display: inline-block}
  .logica-item-manual-label{width: 50px; display: inline-block}
  .logica-item-manual{width: 20px; display: inline-block}
  .logica-item-tipo-nota-label, .logica-item-requerida-label{margin-left: 10px;width: 70px; display: inline-block}
  .logica-item-tipo-nota, .logica-item-requerida{width: 20px; display: inline-block}
  .logica-item-imprime-en-boletin{width: 20px; display: inline-block}
  .logica-item-imprime-en-boletin-label{width: 114px; display: inline-block}
  .logica-item-orden-label{width: 50px; display: inline-block}
  .logica-item-orden{width: 25px; display: inline-block}
  .logica-item-nombre-unico-label{width: 90px; display: inline-block}
  .logica-item-nombre-unico{width: 80px; display: inline-block}
  .item-borra{float: right; cursor: pointer}
  #links{padding:3px 3px 5px 3px; overflow: auto}
  /*.item-a{padding: 1px 5px 1px 5px; margin-left: 3px; float: left; margin-top: 5px; text-decoration: none}*/
  .item-a .ui-button-text-only{padding:0}
  .item-a .ui-button-text{padding:1px 5px 1px 4px; margin: 1px 1px  0px 0px}
  .item-li {margin-bottom: 5px;}
  .ui-dialog{position:fixed !important}
  .logica-item-tipo-nota-label{width:68px}
  #div-item-tipo-tiene-conducta{    display: inline;    width: 114px;}
  #item-tipo-tiene-conducta{vertical-align: top}
  /*.items-nombre-td{width:350px}*/
</style>

<script type="text/javascript">
  function quitaEditores() {
    $(".edit").each(function() {
      editAreaLoader.delete_instance($(this).attr("id"));
      $(this).removeClass("edit");
    })
  }
  quitaEditores();
  $("#links").html("");
</script>

<?php $logica_id = $logica->id ? $logica->id : "nueva"; ?>

<input type="hidden" name="logica_id" value="<?php echo $logica_id; ?>"/>

<div id="items-dialog">
  <table id="items-dialog-table">
  </table>
</div>
<script type="text/javascript">
</script>
<div id="logica-data-div-temp">
  <div class="item-borra"><img src="images/delete.jpg" logica_id="<?php echo $logica_id; ?>" onclick="logicaBorra(this);
    return false"/></div>
  <div class="row">
    <input type="text" name="nombre" value="<?php echo $logica->nombre; ?>" placeholder="Nombre"/>
  </div>
  <div class="row">
    <textarea onfocus="formulaFocus(this);
    return false" id="formula" class="formula" name="formula" placeholder="Fórmula"><?php echo $logica->formula; ?></textarea>
  </div>
</div>

<div id="logica-items-div-temp">
  <div id="links" class="ui-widget ui-widget-content"></div>
  <?php
  $logicaItems = LogicaItem::model()
          ->with("logicaPeriodo")
          ->findAll(array("condition" => "t.logica_id=\"$logica_id\"",
      "order" => "logicaPeriodo.orden,t.orden"));
  ?>
  <?php foreach ($logicaItems as $li): ?>
    <?php nuevoItem($li, null, $logica_id); ?>
  <?php endforeach; ?>
  <?php nuevoItem($liNuevo, null, $logica_id); ?>
</div>

<div id="logica-periodo-items-temp">
  <?php
  $logicaPeriodos = LogicaPeriodo::model()
          ->findAll(array("condition" => "logica_id=\"$logica_id\"", "order" => "orden"));
  ?>
  <?php foreach ($logicaPeriodos as $lp): ?>
    <?php nuevoPeriodo($lp, null, $logica_id); ?>
  <?php endforeach; ?>
  <?php nuevoPeriodo($lpNuevo, null, $logica_id); ?>
</div>

<?php

function nuevoItem($li, $titulo = "Ingrese un nombre para este item", $logica_id) { ?>
  <?php $id = !$li->id ? "nuevo" : $li->id; ?>
  <div id="item_<?php echo $id; ?>" class="logica-item-div ui-widget ui-widget-content ui-corner-all">
    <div class="item-borra"><img src="images/delete.jpg" item_id="<?php echo $id; ?>" onclick="itemBorra(this);
      return false"/></div>
    <div class="row">
      <input name='logica-item[<?php echo $id; ?>][nombre]'  class="item-nombre"  value="<?php echo $li->nombre; ?>" placeholder="<?php echo $titulo; ?>"/>
      <?php /* @var $li LogicaItem */; ?>
      <div class="logica-item-periodos-label">Período: </div>
      <select class="logica-item-periodos" name='logica-item[<?php echo $id; ?>][periodo_id]'>
        <?php
        $x = array();
        if ($id == "nuevo") {
          echo "<option value=''></option>";
        }
        $logica_id = $li->Logica_id ? $li->Logica_id : "-1";
        echo CHtml::listOptions(
                $li->Logica_Periodo_id
                , CHtml::listData(LogicaPeriodo::model()->findAll(
                                array(
                                    "condition" => "logica_id = $logica_id",
                                    "order" => "orden")
                        ), 'id', 'nombre'
                ), $x
        );

        ;
        ?>
      </select>
      <div class="logica-item-abreviatura-label">Abreviatura:</div><input name='logica-item[<?php echo $id; ?>][abrev]'  class="item-abrev"  value="<?php echo $li->abrev; ?>" placeholder="Abreviatura"/>
    </div>
    <div class="row">
      <div class="logica-item-manual-label">Manual:</div><input name='logica-item[<?php echo $id; ?>][manual]'  class="logica-item-manual"  value="<?php echo $li->manual; ?>" placeholder=""/>
      <div class="logica-item-requerida-label">Requerida:</div><input name='logica-item[<?php echo $id; ?>][requerida]'  class="logica-item-requerida"  value="<?php echo $li->requerida; ?>" placeholder=""/>
      <div class="logica-item-tipo-nota-label">Tipo nota?:</div>
      <input 
        name='logica-item[<?php echo $id; ?>][tipo-nota]'  
        class="logica-item-requerida"  
        value="<?php echo $li->tipo_nota; ?>" 
        placeholder=""/>
      <div id="div-item-tipo-tiene-conducta">Tiene conducta?</div>
      <input 
        type="checkbox"
        name='logica-item[<?php echo $id; ?>][tiene-conducta]'  
        id="item-tipo-tiene-conducta"  
        <?php echo $li->tiene_conducta ? "checked=\"checked\"" : ""; ?> 
        />
      <div class="logica-item-imprime-en-boletin-label">Imprime en boletín:</div><input name='logica-item[<?php echo $id; ?>][imprime-en-boletin]'  class="logica-item-imprime-en-boletin"  value="<?php echo $li->imprime_en_boletin; ?>" placeholder=""/>
      <div class="logica-item-orden-label">Orden:</div><input name='logica-item[<?php echo $id; ?>][orden]'  class="logica-item-orden"  value="<?php echo $li->orden; ?>" placeholder=""/>
      <div class="logica-item-nombre-unico-label">Nombre único:</div><input id="logica-item-nombre-unico" type="text" name='logica-item[<?php echo $id; ?>][nombre-unico]' value="<?php echo $li->nombre_unico; ?>" placeholder="Nombre único"/>
    </div>
    <div class="row">
      <?php $formula_id = "formula" . $id; ?>
      <textarea id="<?php echo $formula_id; ?>" class="formula"
                onfocus="formulaFocus(this);
      return false"
                name='logica-item[<?php echo $id; ?>][formula]' placeholder="Ingrese una Fórmula"><?php echo $li->formula; ?></textarea>
      <button type="button"name="graba" class="graba item-a" onclick="return grabaLogica();">Graba</button>
    </div>
    <button
      id="a-<?php echo $id; ?>"
      class="item-a"
      type="button"
      onclick="$('html, body').animate({
        scrollTop: $('#item_<?php echo $id; ?>').offset().top
      }, 900);">
        <?php echo ($id !== "nuevo") ? $li->abrev : "Nuevo"; ?>
    </button>
    <button
      type="button"
      class="item-a"
      onclick="$('html, body').animate({scrollTop: 0}, 1400);">
      Arriba
    </button>
    <button
      type="button"
      class="item-a"
      onclick="ayudaItems()">
      Ayuda
    </button>
    <table id="temp-table">
      <tr class="item-li" id="tr-<?php echo $id; ?>">
        <td class="items-nombre-td"><?php echo $li->nombre; ?></td>
        <td><?php echo $li->nombre_unico; ?></td>
      </tr>
    </table>
  </div>
  <script type="text/javascript">
  $("#tr-<?php echo $id; ?>").appendTo("#items-dialog-table");
  $("#temp-table").remove();
  $("#a-<?php echo $id; ?>").appendTo("#logica-items-div-temp #links");
  //    $("#logica-items-div-temp #links").append("<a class=\"item-a ui-widget ui-widget-content ui-corner-all\" href=\"#item_<?php echo $id; ?>\"><?php echo ($id !== "nuevo") ? $li->abrev : "Nuevo"; ?></a>");
  </script>
  <?php
}

function nuevoPeriodo($lp, $titulo = "Ingrese un nombre para este período", $logica_id) {
  ?>
  <?php $id = !$lp->id ? "nuevo" : $lp->id; ?>
  <div class="logica-item-div ui-widget ui-widget-content ui-corner-all">
    <div class="item-borra"><img src="images/delete.jpg" periodo_id="<?php echo $id; ?>"
                                 onclick="periodoBorra(this);
      return false"/></div>
    <div class="row">
      <input name='logica-periodo[<?php echo $id; ?>][nombre]'  class="periodo-nombre"
             value="<?php echo $lp->nombre; ?>" placeholder="<?php echo $titulo; ?>"/>
    </div>
    <div class="row">
      <div class="label-periodo-fecha">Desde fecha: </div>
      <input name='logica-periodo[<?php echo $id; ?>][fecha_inicio]'
             class="periodo-fecha" value="<?php echo date("d/m/Y", mystrtotime($lp->fecha_inicio)); ?>"
             placeholder="Fecha de inicio"/>
      <div class="label-periodo-fecha">Hasta fecha: </div>
      <input name='logica-periodo[<?php echo $id; ?>][fecha_fin]'
             class="periodo-fecha" value="<?php echo date("d/m/Y", mystrtotime($lp->fecha_fin)); ?>"
             placeholder="Fecha de finalización"/>
    </div>
    <div class="row">
      <div class="label-periodo-fecha">Desde fecha ci: </div>
      <input name='logica-periodo[<?php echo $id; ?>][fecha_inicio_ci]'
             class="periodo-fecha" value="<?php echo date("d/m/Y", mystrtotime($lp->fecha_inicio_ci)); ?>"
             placeholder="Fecha de inicio ci"/>
      <div class="label-periodo-fecha">Hasta fecha ci: </div>
      <input name='logica-periodo[<?php echo $id; ?>][fecha_fin_ci]'
             class="periodo-fecha" value="<?php echo date("d/m/Y", mystrtotime($lp->fecha_fin_ci)); ?>"
             placeholder="Fecha de finalización ci"/>
    </div>
    <div class="row">
      <div class="label-periodo-abreviatura">Abreviatura</div>
      <input name='logica-periodo[<?php echo $id; ?>][abreviatura]'  class="periodo-abreviatura"
             value="<?php echo $lp->abrev; ?>" placeholder="Ingrese la abreviatura"/>
      <div class="label-periodo-orden">Orden</div>
      <input name='logica-periodo[<?php echo $id; ?>][orden]' class="periodo-orden"
             value="<?php echo $lp->orden; ?>" placeholder="Ingrese el Órden"/>
    </div>
  </div>
  <?php
}
?>

<script type="text/javascript">
  var logica_id = "<?php echo $logica_id; ?>";
  $("#items-dialog").dialog({
    title: "Items",
    autoOpen: false,
    width: "auto",
    position: [700, 100]
  });
  $(".graba").show();
  if (logica_id === "nueva") {
    $("#tabs").tabs({disabled: [2, 3, 4]});
    $("#tabs").tabs('select', 1);
  } else {
    $("#tabs").tabs({disabled: []});
  }
  $(".graba").button();
  $("button").button();
  $("#logica-tab").html("");
  $("#logica-items-tab").html("");
  $("#logica-periodo-items-tab").html("");
  $("#logica-data-div-temp").appendTo("#logica-tab");
  $("#logica-items-div-temp").appendTo("#logica-items-tab");
  $("#logica-periodo-items-temp").appendTo("#logica-periodo-items-tab");
  // initialisation
  $(".formula").each(function() {

  })

  function formulaFocus(obj) {
    quitaEditores();
    id = $(obj).attr("id");
    agregaEditor($(obj).attr("id"));
    return;
  }

  function agregaEditor(id) {
    $("#" + id).addClass("edit");
    editAreaLoader.init({
      id: id	// id of the textarea to transform		
              , start_highlight: true	// if start with highlight
              , allow_resize: "both"
              , is_editable: true
              , allow_toggle: false
              , word_wrap: false
              , language: "es"
              , syntax: "php"
              //, display: "later"
    });
  }

  function ayudaItems() {
    $("#items-dialog").dialog("open");
    //$("#items-dialog").closest(".ui-dialog").css("top",10);
  }

  function formulaBlur(obj) {
    //editAreaLoader.delete_instance($(obj).attr("id"));
  }

  function itemBorra(obj) {
    item_id = $(obj).attr("item_id");
    $.ajax({
      type: "GET",
//      dataType:"json",
      data: {item_id: item_id},
      url: "<?php echo $this->createUrl("nota/logicaItemBorra"); ?>",
      success: function(data) {
        logicaChange();
      },
      error: function(data, status) {
      }
    });
  }

  function periodoBorra(obj) {
    periodo_id = $(obj).attr("periodo_id");
    $.ajax({
      type: "GET",
//      dataType:"json",
      data: {periodo_id: periodo_id},
      url: "<?php echo $this->createUrl("nota/logicaPeriodoBorra"); ?>",
      success: function(data) {
        logicaChange();
      },
      error: function(data, status) {
      }
    });
  }

  function logicaBorra(obj) {
    logica_id = $(obj).attr("logica_id");
    $.ajax({
      type: "GET",
//      dataType:"json",
      data: {logica_id: logica_id},
      url: "<?php echo $this->createUrl("nota/logicaBorra"); ?>",
      success: function(data) {
        asignaturaTipoChange();
      },
      error: function(data, status) {
      }
    });
  }

</script>
