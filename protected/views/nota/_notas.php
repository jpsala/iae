<?php //vd2($cuali,$cuali?'si':'no');?>
<?php $notaWidth = 350; //700 / count($logicaItems);             ?>
<style type="text/css">
	textarea.nota{
		width:<?php echo $notaWidth; ?>
	}
	.nombre-asignatura-div{
		padding: 10px;
	}
</style>
<form class="" id="main-form" name="main-form" method="POST" action="#">
	<input name="division_asignatura_id" type="hidden" value="<?php echo $divisionAsignatura->id; ?>"/>
	<input name="asignatura_id" type="hidden" value="<?php echo $asignatura->id; ?>"/>
	<div class="nombre-asignatura-div ui-widget ui-widget-content ui-widget-default ui-corner-all">
		<?php echo $asignatura->nombre;?>
	</div>
	<table id="main-table">
		<?php /* @var $division Division */; ?>
		<tr>
			<th></th>
			<th>Alumno</th>
			<?php foreach ($logicaItems as $li): ?>
					<th class="right-align"><?php echo $li->abrev; ?></th>
				<?php endforeach; ?>

		</tr>
		<?php $row = 0; ?>
		<?php $col = 0; ?>
		<?php foreach ($notas as $alumnoDivision_id => $notasAlumno): ?>
				<?php $alumno = AlumnoDivision::model()->with("alumno")->findByPk($alumnoDivision_id)->alumno; ?>
				<tr 
					row="<?php echo ++$row; ?>"
					class="<?php echo ($row % 2) ? "tr-even" : ""; ?> notas-alumno" 
					id="<?php echo $alumno->id; ?>" 
					alumnoDivision_id="<?php echo $alumnoDivision_id; ?>"
					>
					<td class="rec-img"></td>
					<td class="td-alumno"><?php echo $alumno->nombreCompleto; ?><input type="hidden" name="alumnos[]" value ="<?php echo $alumno->id; ?>"</td>
					<?php $col = 0; ?>
					<?php foreach ($notasAlumno as $nota): ?>
						<?php //vd($nota); ?>
						<?php if ($nota["manual"] and $nota["logica_periodo_id"] == $logicaPeriodoSeleccionada_id): ?>
							<td class="right-align td-nota">
								<?php if ($asignaturaTipo_input == "input" and $cuali==-1): ?>
									<input 
										id="row_<?php echo $row; ?>"
										col="<?php echo ++$col; ?>"
										type="text" 
										class="nota <?php echo $nota["error"] != '' ? 'error' : '' ?>" 
										logica_item_id="<?php echo $nota["logica_item_id"]; ?>" 
										value="<?php echo $nota["nota"]; ?>" 
										name="notas[<?php echo $alumno->id; ?>][<?php echo $nota["logica_item_id"]; ?>]"
										/>
									<?php else: ?>
									<textarea 
										class="nota <?php echo $nota["error"] != '' ? 'error' : '' ?>" 
										logica_item_id="<?php echo $nota["logica_item_id"]; ?>" 
										name="notas[<?php echo $alumno->id; ?>][<?php echo $nota["logica_item_id"]; ?>]"
										rows="10" cols="40"><?php echo $nota["nota"]; ?></textarea>
							<?php endif; ?>
							</td>
			<?php else: ?>
							<td class="right-align td-nota">
								<input 
									type="hidden" class="nota" 
									logica_item_id="<?php echo $nota["logica_item_id"]; ?>" 
									value="<?php echo $nota["nota"]; ?>" 
									name="notas[<?php echo $alumno->id; ?>][<?php echo $nota["logica_item_id"]; ?>]"
									/>
				<?php if ($asignaturaTipo_input == "input"  and $cuali==-1): ?>
									<div type="text" logica_item_id="<?php echo $nota["logica_item_id"]; ?>" 
											 class="nota-no-editable nota <?php echo $nota["error"] != '' ? 'error' : '' ?>">
										<div><?php echo $nota["nota"]; ?></div>
									</div>
				<?php else: ?>
									<textarea 
										class="nota <?php echo $nota["error"] != '' ? 'error' : '' ?>" 
										logica_item_id="<?php echo $nota["logica_item_id"]; ?>" 
										name="notas[<?php echo $alumno->id; ?>][<?php echo $nota["logica_item_id"]; ?>]"
										rows="10"
										disabled="disabled"
										><?php echo $nota["nota"]; ?></textarea>
							<?php endif; ?>
							</td>
						<?php endif; ?>
		<?php endforeach; ?>
				</tr>
				<tr class="error"><td></td><td class="td-error"  colspan="<?php echo count($logicaItems) + 1; ?>"></td></tr>
	<?php endforeach; ?>

	</table>
	<div class="buttons-div">
		<button class="top-botton ok" type="submit" name="ok" id="">Graba</button>
		<button class="top-botton" type="button" name="cancela" id="cancela" onclick="cancelaNotas();
				return false">Cancela</button>
	</div>
</form>
<table id="desc-table">
<?php /* @var $division Division */; ?>
	<tr>
		<th>Abrev.</th>
		<th>Descripción</th>
	</tr>
	<?php $rowLi = 0; ?>
<?php foreach ($logicaItems as $li): ?>
			<tr class="<?php echo ($rowLi++ % 2) ? "tr-even" : ""; ?>">
				<td><?php echo $li->abrev; ?></td><td><?php echo $li->nombre; ?></td>
			</tr>
	<?php endforeach; ?>
</table>
<script type="text/javascript">
	var maxCol = Number(<?php echo $col; ?>);
	var maxRow = Number(<?php echo $row; ?>);
  var integradora = <?php echo $integradora ? $integradora : 'false'; ?>;
  console.log('integradora:', integradora);
	function inputAbajo($obj) {
		var $tr = $($obj).closest("tr");
		var col = Number($($obj).attr("col"));
		var row = Number($tr.attr("row"));
		if (row === maxRow) {
			row = 1;
		} else {
			row++;
		}
		$n = $("tr[row=%row]".replace("%row", row));
		return $n.find("input[col=%col]".replace("%col", col));
	}

	function inputArriba($obj) {
		var $tr = $($obj).closest("tr");
		var col = Number($($obj).attr("col"));
		var row = Number($tr.attr("row"));
		if (row === 1) {
			row = maxRow;
		} else {
			row--;
		}
		$n = $("tr[row=%row]".replace("%row", row));
		return $n.find("input[col=%col]".replace("%col", col));
	}

	function inputSiguiente($obj) {
		var $tr = $($obj).closest("tr");
		var col = Number($($obj).attr("col"));
		var row = Number($tr.attr("row"));
		var $input;
		if (col === maxCol && row === maxRow) {
			col = 1;
			row = 1;
		} else if (col === maxCol) {
			row++;
			col = 1;
		} else if (row === maxRow) {
			row = 1;
		} else {
			col++;
		}
		$tr = $("tr[row=%row]".replace("%row", row));
		$input = $tr.find("input[col=%col]".replace("%col", col));
		return $input;
	}

	function inputAnterior($obj) {
		var $tr = $($obj).closest("tr");
		var col = Number($($obj).attr("col"));
		var row = Number($tr.attr("row"));
		var $input;
		if (col === 1 && row === 1) {
			col = maxCol;
			row = maxRow;
		} else if (col === 1) {
			row--;
			col = maxCol;
		} else if (row === maxRow) {
			row = maxRow;
		} else {
			col = 1;
		}
		$tr = $("tr[row=%row]".replace("%row", row));
		$input = $tr.find("input[col=%col]".replace("%col", col));
		return $input;
	}

</script>            