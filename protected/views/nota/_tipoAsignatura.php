<style type="text/css">
  #asignatura-tipo-nombre-div{display: inline}
  #asignatura-tipo-borra{display: inline;vertical-align: bottom;}
  #asignatura-tipo-tipo-input-div{display: inline;;}
</style>
<div class="row" id="asignatura-tipo-nombre-div">
  Nombre: <input name="asignatura-tipo-nombre" value="<?php echo $ta->nombre; ?>"/>
</div>
<div class="row" id="asignatura-tipo-tipo-input-div">
  Tipo de entrada: 
  <select id="asignatura-tipo-tipo-input-select" name="asignatura-tipo-tipo-input">
    <option value="input" <?php echo $ta->tipo_input != "textarea" ? "selected" : ""; ?>>Input</option>
    <option value="textarea" <?php echo $ta->tipo_input == "textarea" ? "selected" : ""; ?>>Textarea</option>
  </select>
</div>
<div id="asignatura-tipo-borra"><img src="images/delete.jpg" asignatura-tipo-id="<?php echo $ta->id; ?>" onclick="asignaturaTipoBorra($(this));
    return false"/>
</div>


<input type="button" value="Graba" name="graba" class="graba" onclick="return grabaLogica()"/>
<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<script type="text/javascript">
  function asignaturaTipoBorra(obj) {
    $.ajax({
      type: "GET",
//      dataType:"json",
      data: {asignatura_tipo_id: obj.attr("asignatura-tipo-id")},
      url: "<?php echo $this->createUrl("nota/borraAsignaturaTipo"); ?>",
      success: function(data) {
        if (data == "ok") {
          changeNivel();
        }
      },
      error: function(data, status) {
      }
    });
  }
</script>
