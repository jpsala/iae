<style type="text/css">

    #pedido-de-datos .row { display: inline-block }
    #pedido-de-datos #nivel-select { width: 115px; }
    #pedido-de-datos #anio-select { width: 70px }
    #pedido-de-datos #division-select { width: 110px }
    #pedido-de-datos #asignatura-select { width: 200px }
    #pedido-de-datos #periodo-select { width: 175px }
    #div-para-notas { margin-top: 8px }
    input.nota { width: 33px; text-align: right }
    textarea.nota { }
    .nota-no-editable { border: 1px solid #CECECE;
        border-radius: 2px 2px 2px 2px;
        box-shadow: 1px 1px 8px 0px rgba(0, 0, 0, 0.1) inset, 0 1px 0 rgba(255, 255, 255, 0.2);
        color: #999999;
        float: right;
        height: 21px;
        margin-right: 0;
        outline: medium none;
        width: 30px; }
    .nota-no-editable > div { padding: 3px }
    table#main-table { background-color: #C3D9FF; border: #0066A4 solid 1px; border-radius: 4px; padding: 2px; width: 100% }
    table#main-table tr { background-color: #fff }
    table#main-table .td-nota { width: 35px; }
    table#main-table td.td-alumno { width: 250px }
    table#main-table th { padding-left: 3px }
    /*    table#main-table th:last-child{min-width:2px}*/
    table#main-table * { font-size: 98% }
    table#main-table input { padding-left: 2px; padding-right: 2px }
    table#main-table tr.error { display: none }
    table#main-table tr.error .td-error { padding: 0 }
    table#main-table tr td input.error { box-shadow: 0 1px 3px rgba(200, 20, 0, 0.8) inset, 0 1px 0 rgba(55, 55, 0, 0.1) }
    table#desc-table { background-color: #C3D9FF; border: 1px solid #0066A4; border-radius: 4px 4px 4px 4px; margin: 10px 0 0; padding: 4px; width: 400px; }
    table#desc-table tr { background-color: #fff }
    table#desc-table td { font-size: 90%; padding: 1px 0px 2px 6px }
    table#desc-table th { border-bottom: 1px solid #777777; padding: 2px 0 2px 5px; }
    th, td, caption { padding: 1px 0px 2px 0px }
    th { background-color: #C3D9FF; color: #555555 }
    td.center, th.center { text-align: center }
    td.rec-img { background-position: center center; background-repeat: no-repeat; height: 15px; padding: 0; width: 26px; }
    .error-span { background-color: LightYellow; border: 2px solid #C3D9FF; border-radius: 6px 6px 6px 6px; color: SlateGrey; display: inline-block; margin: 2px 2px 6px 0; padding: 4px 5px 4px 5px; }
    .error-span strong { color: Tomato; }
    .buttons-div { text-align: right }
    /*.tr-even { background-color:#edf1fe !important}*/
    .right-align { text-align: right }
    #top-botton-div { float: right }
    /*#imprime{margin-left: 1px}*/
</style>
<?php /* @var $nota Nota */; ?>
<?php
  $cuali = (isset($cuali) and $cuali) ? 1 : '-1';
?>
<div id="pedido-de-datos">
    <div class="row">
        <select id="nivel-select" data-placeholder="Nivel" class="chzn-select">
            <?php
                $x = array('prompt' => '');
                echo CHtml::listOptions(null, CHtml::listData(Nivel::model()->ordenados()->activos()->findAll(), 'id', 'nombre'), $x)
            ?>
        </select>
    </div>
    <div class="row">
        <select id="anio-select" data-placeholder="Año" class="chzn-select">
            <?php
                $x = array('prompt' => '');
                echo CHtml::listOptions(null, CHtml::listData(Nivel::model()->findAll("id=-1"), 'id', 'nombre'), $x)
            ?>
        </select>
    </div>
    <div class="row">
        <select id="division-select" data-placeholder="Division" class="chzn-select">
            <?php
                $x = array('prompt' => '');
                echo CHtml::listOptions(null, CHtml::listData(Division::model()->findAll("id=-1"), 'id', 'nombre'), $x)
            ?>
        </select>
    </div>
    <div class="row">
        <select id="asignatura-select" data-placeholder="Asignatura" class="chzn-select">
            <?php
                $x = array('prompt' => '');
                echo CHtml::listOptions(null, CHtml::listData(Asignatura::model()->findAll("id=-1"), 'id', 'nombre'), $x)
            ?>
        </select>
    </div>
    <div class="row">
        <select id="periodo-select" data-placeholder="Período" class="chzn-select">
            <?php
                $x = array('prompt' => '');
                echo CHtml::listOptions(null, CHtml::listData(LogicaPeriodo::model()->findAll("id=-1"), 'id', 'nombre'), $x)
            ?>
        </select>
    </div>
    <div id="top-botton-div">
        <button class="top-botton ok" type="submit" name="ok" id="ok">Graba</button>
        <button class="top-botton" type="button" name="cancela" id="cancela" onclick="cancelaNotas();
				return false">Cancela
        </button>
        <button class="" onclick="imprime();
				return false;" id="imprime">Imprime
        </button>
    </div>
</div>
<div id="div-para-notas"></div>
<script type="text/javascript">
var division_asignatura_id, logica_periodo_id;
var totalErrors = new Array();
var modificado = false;
var urlImpresion = "<?php echo $this->createUrl("/informe/planillaNotasAjax"); ?>";
var urlImpresionIntegradora = "<?php echo $this->createUrl("/informe/planillaNotasAjaxIntegradora"); ?>";
var procesando = false;

$(function () {
    init();
});

function init() {
    $(document).bind('keydown', 'ctrl+o', function (e) {
        console.log(e);
    });

    $(".top-botton").attr("DISABLED", "DISABLED");

    $("button").button();


    $(".ok").click(function () {
        $("form").submit();
    });

    $("#nivel-select").chosen().change(function () {
        changeNivel($(this));
    });

    $("#anio-select").chosen().change(function () {
        changeAnio($(this));
    });

    $("#division-select").chosen().change(function () {
        changeDivision($(this));
    });

    $("#asignatura-select").chosen().change(function () {
        changeAsignatura($(this));
    });

    $("#periodo-select").chosen().change(function () {
        traeNotas($(this));
    });

    $(".clearinput").clearinput();

    $("#nivel_select_chzn").mousedown();

    $("#imprime").button("disable");
}

function changeNivel($this) {
    $.ajax({
        type: "GET",
        //dataType: "json",
        data: {nivel_id: $this.val()},
        url: "<?php echo $this->createUrl("anio/options"); ?>",
        success: function (data) {
            $("#anio-select").html(data);
            $("#anio-select").trigger("liszt:updated");
            $("#anio_select_chzn").mousedown();

        },
        error: function (data, status) {
        }
    });
    return false;
}

function changeAnio($this) {
    $.ajax({
        type: "GET",
        //dataType: "json",
        data: {anio_id: $this.val()},
        url: "<?php echo $this->createUrl("division/options"); ?>",
        success: function (data) {
            $("#division-select").html(data);
            $("#division-select").trigger("liszt:updated");
            $("#division_select_chzn").mousedown();

        },
        error: function (data, status) {
        }
    });
    return false;

}

function changeDivision($this) {
    $.ajax({
        type: "GET",
        //dataType: "json",
        data: {division_id: $this.val()},
        url: "<?php echo $this->createUrl("asignatura/options"); ?>",
        success: function (data) {
            $("#asignatura-select").html(data);
            $("#asignatura-select").trigger("liszt:updated");
            $("#asignatura_select_chzn").mousedown();
        },
        error: function (data, status) {
            division_asignatura_id = null;
        }
    });
    return false;

}

function changeAsignatura($this) {
    division_asignatura_id = $this.val();
    $.ajax({
        data: {division_asignatura_id: division_asignatura_id, seleccionaActiva: true},
        type: "GET",
        url: "<?php echo $this->createUrl("logicaPeriodo/options"); ?>",
        success: function (data) {
            $("#periodo-select").html(data);
            $("#periodo-select").trigger("liszt:updated");
            $("#div-para-notas").html("");
            //$("#periodo_select_chzn").mousedown();
            $("#periodo-select").focus().change();
            $("#imprime").button("enable");

        },
        error: function (data, status) {
            division_asignatura_id = null;
        }
    });

}

function traeNotas($this) {
    logica_periodo_id = $this.val();
    $.ajax({
        type: "GET",
        data: {
            logica_periodo_id: logica_periodo_id,
            division_asignatura_id: division_asignatura_id,
            cuali: <?php echo $cuali;?>
        },
        url: "<?php echo $this->createUrl("nota/traeNotasParaCargaXAsignatura"); ?>",
        success: function (data) {
            $("#div-para-notas").html(data);
            afterTraeNotas();
        },
        error: function (data, status) {
        }
    });

}

function afterTraeNotas() {
    $(".top-botton").attr("DISABLED", "DISABLED");

    $("button").button();

    $inputAnt = $(".nota:not(:disabled):not(:hidden)").not(".nota-no-editable").first().focus();

    $(".notas-alumno").on("change", "input,textarea", function () {
        procesaNotas($(this));
    });

    $(".notas-alumno").on("blur", "input,textarea", function (e) {
    });

    $(".notas-alumno").on("keydown", "input,textarea", function (e) {
        if (procesando) {
            e.preventDefault();
            return false;
        }
        if (e.keyCode === 9 && e.shiftKey) {
            console.log(1);
            $nextInput = inputAnterior($(this));
            setFocusAndSelect($nextInput, 1);
            e.preventDefault();
        } else if (e.keyCode === 13 || e.keyCode === 9) {
            $nextInput = inputSiguiente($(this));
            setFocusAndSelect($nextInput, 1);
            e.preventDefault();
        } else if (e.keyCode === 40) {
            $nextInput = inputAbajo($(this));
            setFocusAndSelect($nextInput, 1);
            e.preventDefault();
        } else if (e.keyCode === 38) {
            $nextInput = inputArriba($(this));
            setFocusAndSelect($nextInput, 1);
            e.preventDefault();
        } else {
            $nextInput = null;
        }
    });

    $("#main-form").submit(function () {
        if (!procesando) {
            submit();
        }
        return false;
    });
}

function procesaNotas($this) {
    var cuali = "<?php echo $cuali;?>" === "1";
    var $rec = $this.closest("tr");
    if(cuali){
      $rec.find(".rec-img").css("background-image", 'url("images/ok.gif")');
      $(".top-botton").button("enable");
      return;
    };
    toggle("off");
    procesando = true;
    muestraSpin = false;
    var alumno_division_id = $rec.attr("alumnoDivision_id");
    var data = $rec.find(".nota").serialize();
    $rec.find(".rec-img").css("background-image", 'url("images/spinner.gif")');
    $(".nota").css("enabled", "false");
    $rec.find(".nota").prop("disabled", true);
    $.ajax({
        type: "POST",
        dataType: "json",
        data: "alumno_division_id=" + alumno_division_id + "&logica_periodo_id=" + logica_periodo_id + "&division_asignatura_id=" + division_asignatura_id + "&" + data,
        url: "<?php echo $this->createUrl("nota/cargaXAsignaturaProcesa"); ?>",
        success: function (data) {
            procesaProceso(data, $rec);
            $rec.find(".nota").prop("disabled", false);
            if ($nextInput)
                setFocus($nextInput, 1);
        },
        error: function (data, status) {
            $rec.find(".rec-img").css("background-image", 'url("images/error.gif")');
        },
        complete: function () {
            muestraSpin = true;
            procesando = false;
            $(".spinner").removeClass("spinner");
        }
    });
}

function procesaProceso(data, $rec) {
    var nota, $destino, hayError;
    $rec.next().find(".error-span").remove();
    $rec.find("td input.error").removeClass("error");
    for (var i in data) {
        nota = data[i];
        $destino = $rec.find("[logica_item_id='%id']".replace("%id", nota.logica_item_id));
        if (nota.error) {
            $rec.next().find(".td-error").append('<div class="error-span">' + nota.nombre + " <strong>" + nota.error + "</strong></div>").end().animate({"opacity": "show"});
            hayError = true;
        }
        $destino.each(function () {
            if (nota.error) {
                $destino.addClass("error");
            }
            if ($(this)[0].tagName == "INPUT") {
                $(this).val(nota.nota);
            } else {
                $(this).find("div").html(nota.nota);
            }
        });
    }
    if (hayError) {
        totalErrors["'" + $rec.attr("id") + "'"] = true;
        $rec.find(".rec-img").css("background-image", 'url("images/error.gif")');
    } else {
        if (totalErrors["'" + $rec.attr("id") + "'"]) {
            delete totalErrors["'" + $rec.attr("id") + "'"];
        }
        $rec.find(".rec-img").css("background-image", 'url("images/ok.gif")');
    }
}

function cancelaNotas() {
    toggle("on");
    traeNotas({
        val: function () {
            return logica_periodo_id
        }
    });
}

function submit($this) {
    if (Object.keys(totalErrors).length) {
        alert("Antes de grabar corrija las notas con error");
        return;
    }
    var data = $("#main-form").serialize();
    data = data + '&cuali=<?php echo $cuali?>';
    $.ajax({
        type: "POST",
        data: data,
        url: "<?php echo $this->createUrl("nota/cargaXAsignaturaGraba"); ?>",
        success: function (data) {
            toggle("on");
            alert("Las notas fueron grabadas");
        },
        error: function (data, status) {
        }
    });
    return false;
}

function toggle(onoff) {
    enabled = onoff == "on" ? true : false;
    if (!enabled) {
        $(".top-botton").button("enable");
    } else {
        $(".top-botton").button("disable");
    }
    $("#nivel-select").attr('disabled', !enabled).trigger("liszt:updated");
    $("#anio-select").attr('disabled', !enabled).trigger("liszt:updated");
    $("#division-select").attr('disabled', !enabled).trigger("liszt:updated");
    $("#asignatura-select").attr('disabled', !enabled).trigger("liszt:updated");
    $("#periodo-select").attr('disabled', !enabled).trigger("liszt:updated");
}

function imprime() {
    // window.open(urlImpresion + "&division_asignatura_id=" + division_asignatura_id + "&impresion=1");
    // window.open(urlImpresionIntegradora + "&division_asignatura_id=" + division_asignatura_id + "&impresion=1");
    console.log('integradora', integradora);
    if (integradora) {
        window.open(urlImpresionIntegradora + "&division_asignatura_id=" + division_asignatura_id + "&impresion=1");
    } else {
        window.open(urlImpresion + "&division_asignatura_id=" + division_asignatura_id + "&impresion=1");
    }
}

function nextOnTabIndex(element) {
    //var element = document.activeElement;
    var fields = $($('form')
        .find('a[href], button, input, select, textarea')
        .filter(':visible').filter(':enabled')
        .toArray()
        .sort(function (a, b) {
            return ((a.tabIndex > 0) ? a.tabIndex : 1000) - ((b.tabIndex > 0) ? b.tabIndex : 1000);
        }));


    return fields.eq((fields.index(element) + 1) % fields.length);
}

</script>

