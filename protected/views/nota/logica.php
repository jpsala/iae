<script language="Javascript" type="text/javascript" src="js/editarea/edit_area/edit_area_full.js"></script>

<style type="text/css">
  .inline-row{display:inline}
  #logica-data-div{margin-top: 10px; padding: 5px}
  #nivel-select{width:200px}
  #asignatura-tipos-select{width: 200px}
  #logica-select{width: 200px}
  .graba{float: right; display: none}
</style>

<div class="inline-row">
  <?php $paramNivel->render(); ?>
</div>
<div id="asignatura-tipos-select-div" class="inline-row">
</div>
<div id="logicas-select-div" class="inline-row">
</div>

<form>
  <input type="hidden" name="nivel-id" id="nivel-id"/>
  <div id="tabs">
    <ul>
      <li><a href="#asignatura-tipos-tab"><span>Tipo de asignatura</span></a></li>
      <li><a href="#logica-tab"><span>Lógica</span></a></li>
      <li><a href="#logica-periodo-items-tab"><span>Períodos</span></a></li>
      <li><a href="#logica-items-tab"><span>Ítems</span></a></li>
      <input type="button" value="Graba" name="graba" class="graba" onclick="return grabaLogica()"/>
    </ul>
    <div id="asignatura-tipos-tab">
    </div>
    <div id="logica-tab">
    </div>
    <div id="logica-items-tab">
    </div>
    <div id="logica-periodo-items-tab">
    </div>
  </div>
</form>



<script type="text/javascript">
        var logica_id;
        $("#tabs").tabs();

        $("#nivel-select_chzn").mousedown();

        function changeNivel(obj) {
          if (obj) {
            nivel_id = obj.val();
          }
          $("#nivel-id").val(nivel_id);
          $.ajax({
            type: "GET",
            //      dataType:"json",
            data: {nivel_id: nivel_id},
            url: "<?php echo $this->createUrl("nota/traeAsignaturaTipos"); ?>",
            success: function(data) {
              $("#asignatura-tipos-select-div").html(data);
              $("#asignatura-tipos-select-div select").chosen().change(function() {
                asignatura_tipo_id = $(this).val();
                $.ajax({
                  type: "GET",
                  //      dataType:"json",
                  data: {asignatura_tipo_id: asignatura_tipo_id},
                  url: "<?php echo $this->createUrl("nota/traeTipoAsignatura"); ?>",
                  success: function(data) {
                    $("#asignatura-tipos-tab").html(data);
                    $(".graba").show();
                    if (asignatura_tipo_id == -1) {
                      $("#tabs").tabs({disabled: [1, 2, 3]});
                      $("#tabs").tabs('select', 0);
                      $("#asignatura-tipo-nombre-div input").focus();
                    } else {
                      $("#tabs").tabs({disabled: []});
                      asignaturaTipoChange();
                    }
                  },
                  error: function(data, status) {
                  }
                });
              }
              );
              $("#asignatura-tipos-select_chzn").mousedown();
            },
            error: function(data, status) {
            }
          });
        }

        function asignaturaTipoChange() {
//          $("#tabs").tabs('select', 1);
          $.ajax({
            type: "GET",
            //      dataType:"json",
            data: {asignatura_tipo_id: asignatura_tipo_id},
            url: "<?php echo $this->createUrl("nota/traeLogicas"); ?>",
            success: function(data) {
              $("#logicas-select-div").html(data);
              $("#logicas-select-div select").chosen().change(function() {
                logica_id = $(this).val();
                logicaChange();
              });
              $("#logica-select_chzn").mousedown();
            },
            error: function(data, status) {
            }
          });
        }

        function logicaChange() {
          //$('.ui-dialog:has("#items-dialog")').empty().remove();
          $("#items-dialog").remove();
          $('.ui-dialog').remove();
          $.ajax({
            type: "GET",
            //      dataType:"json",
            data: {logica_id: logica_id, asignatura_tipo_id: asignatura_tipo_id},
            url: "<?php echo $this->createUrl("nota/traeLogicaData"); ?>",
            success: function(data) {
              //$("#links").html("");
              $("body").append(data);
              $(".periodo-fecha").datepicker();

            },
            error: function(data, status) {
            }
          });
        }

        function grabaLogica() {
          var editorId = $(".edit").attr("id");
          quitaEditores();
          $.ajax({
            type: "POST",
//      dataType:"json",
            data: $("form").serialize() + "&logica_id=" + logica_id + "&asignatura_tipo_id=" + asignatura_tipo_id,
            url: "<?php echo $this->createUrl("nota/grabaLogica"); ?>",
            success: function(data) {
              if (logica_id == "nueva") {
                asignaturaTipoChange();
              } else if (asignatura_tipo_id == -1) {
                changeNivel();
              }
              if (editorId) {
                agregaEditor(editorId);
              }
            },
            error: function(data, status) {
            }
          });
          return false;
        }
</script>