
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>EditArea - the code editor in a textarea</title>
    <script language="Javascript" type="text/javascript" src="js/editarea/edit_area/edit_area_full.js"></script>
    <script language="Javascript" type="text/javascript">
      // initialisation
      editAreaLoader.init({
        id: "example_1"	// id of the textarea to transform		
                , start_highlight: true	// if start with highlight
                , allow_resize: "both"
                , allow_toggle: true
                , word_wrap: false
                , language: "es"
                , syntax: "php"
      });

      // callback functions
      function my_save(id, content) {
        alert("Here is the content of the EditArea '" + id + "' as received by the save callback function:\n" + content);
      }

      function my_load(id) {
        editAreaLoader.setValue(id, "The content is loaded from the load_callback function into EditArea");
      }


      function toogle_editable(id)
      {
        editAreaLoader.execCommand(id, 'set_editable', !editAreaLoader.execCommand(id, 'is_editable'));
      }

    </script>
  </head>
  <body>
    <form action='' method='post'>
      <fieldset>
        <textarea id="example_1" style="height: 350px; width: 100%;" name="test_1">
          <?php
          ?>
        </textarea>
        <p>Custom controls:<br />
          <input type='button' onclick='alert(editAreaLoader.getValue("example_1"));' value='get value' />
          <input type='button' onclick='editAreaLoader.setValue("example_1", "new_value");' value='set value' />
          <input type='button' onclick='toogle_editable("example_1");' value='Toggle readonly mode' />
        </p>
      </fieldset>
    </form>
  </body>
</html>
