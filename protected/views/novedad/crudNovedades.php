<style type="text/css">
  .importe-novedad{width:50px}
  .novedad_borra{max-width:30px;cursor:pointer}
  #selecciona_todos{cursor:pointer}
  #fecha-desde,#fecha-hasta{width: 92px}
  #filtro{padding: 10px; overflow: auto}
  #filtro-fecha{width:250px; display: inline; float: left}
  #filtro-novedad{width :250px; display: inline; float: left; margin-top: -1px}
</style>
<?php /* @var $n Novedad */; ?>
<?php $style = $novedad_cab_id == null ? "" : "style=\"display:none\""; ?>
<div id="filtro" <?php echo $style; ?> class="ui-widget-content ui-widget ui-corner-all">
  <?php $checked = ($fechaDesde and $fechaHasta) ? "checked=\"CHECKED\"" : ""; ?>
  <div id="filtro-fecha" class="ui-widget-default">
    <input id="filtra_fecha" type="checkbox" <?php echo $checked; ?> name="filtra_fecha" onchange="filtraFechaChange(this)"/>
    <input id="fecha-desde" type="text" value="<?php echo $fechaDesde; ?>" name="fecha-desde" onchange="cambiaFecha(this)" placeholder="Desde"/>
    <input id="fecha-hasta" type="text" value="<?php echo $fechaHasta; ?>" name="fecha-hasta" onchange="cambiaFecha(this)" placeholder="Hasta"/>
  </div>
  <div id="filtro-novedad" class="ui-widget-default">
    <?php $data = CHtml::listData($articulos, "id", "nombre"); ?>
    <?php echo CHtml::dropDownList("articulos", $articulo_id, $data, array("prompt" => " - Todas las novedades - ", "id" => "articulos-select", "onchange" => "articulosChange(this)")); ?>
  </div>
  <div id="filtro-novedad" class="ui-widget-default">
    <label for="filtra_usuario">Solo mis novedades</label>
    <?php $checked = $filtra_usuario == 1 ? "checked=\"CHECKED\"" : ""; ?>
    <input id="filtra_usuario" type="checkbox"  name="filtra_usuario" <?php echo $checked;?> onchange="filtro()"/>
  </div>
</div>

<form>
  <table>
    <tr>
      <?php if (!$fechaDesde): ?>
        <th>Fecha</th>
      <?php endif; ?>
      <th>Alumno</th>
      <th>Novedad</th>
      <th>Importe</th>
      <th></th>
      <th><a id="selecciona_todos" onclick="seleccionaTodos();"/>Todos</th>
    </tr>
    <?php foreach ($novedades as $n): ?>
      <tr novedad_id="<?php echo $n['novedad_id']; ?>">
        <td><?php echo $n["fecha"]; ?></td>
        <td><?php echo $n["alumno"]; ?></td>
        <td><?php echo $n["articulo_nombre"]; ?></td>
        <td><input onchange="updNovedad(novedad_id=<?php echo $n["novedad_id"]; ?>,this)" class="importe-novedad" type="text" value="<?php echo $n["importe"]; ?>"</td>
        <td class="novedad_borra"><a onclick="borraNovedad(novedad_id=<?php echo $n["novedad_id"]; ?>,this)">Borra</a></td>
        <td><input  class="selec" name="selec[<?php echo $n["novedad_id"]; ?>]" type="checkbox"/></td>
      </tr>
    <?php endforeach; ?>
  </table>
</form>
<script type="text/javascript">
  //  $fecha
  $("#fecha-desde").datepicker();
  $("#fecha-hasta").datepicker();

  //  $("#articulos-select").chosen().change(function(){
  //    changeNivel($(this));
  //  });

  function cambiaFecha(){
    $("#filtra_fecha").attr("checked","checked")
    filtro();
  }
  
  function seleccionaTodos(){
    $(".selec").each(function(){
      $(this).attr("checked",! $(this).attr("checked"))
    })
  }
  
  function borraNovedad(novedad_id,obj){
    var $novedades = $(".selec:checked");
    var cant = $novedades.length;
    var data;
    if(cant == 0){
      if(!confirm("Borra esta novedad?")){
        return;
      }
      data={"novedad_id":novedad_id};
    }else{
      if(cant == 1){
        if(!confirm("Borra esta novedad?")){
          return;
        }
        novedad_seleccionada = $novedades.closest("tr").attr("novedad_id");
        if(novedad_id != novedad_seleccionada){
          alert("No puede tener seleccionada una novedad y borrar otra");
          return;
        }
      }else  if(cant >0){
        if(!confirm("Borra estas "+cant+" novedades?")){
          return;
        }
      }
      data=$("form").serialize()
    }
    
    $.ajax({
      type: "POST",
      //      dataType:"json",
      data: data,
      url: "<?php echo $this->createUrl("novedad/borraNovedad"); ?>",
      success: function(data) {
        if($novedades.length > 0){
          $novedades.each(function(){
            $(this).closest("tr").remove();
          })
        }else{
          $(obj).closest("tr").remove();
        }
      },
      error: function(data,status){
      }
    });
  }
  
  function updNovedad(novedad_id,obj){
    $input = $(obj);
    $.ajax({
      type: "GET",
      //      dataType:"json",
      data: {novedad_id:novedad_id,importe:$input.val()},
      url: "<?php echo $this->createUrl("novedad/updNovedad"); ?>",
      success: function(data) {
      },
      error: function(data,status){
      }
    });
  }

  function filtraFechaChange(obj){
    if(! $(obj).attr("checked")){
      $("#fecha-desde").attr("disabled","disabled");
      $("#fecha-hasta").attr("disabled","disabled");
    }else{
      $("#fecha-desde").removeAttr("disabled");
      $("#fecha-hasta").removeAttr("disabled");
    }
    filtro();
  }

  function articulosChange(){
    filtro();
  }
  
  function filtro(){
    var filtro, fechaDesde, fechaHasta, articulo_id;
    if(!$("#filtra_fecha").attr("checked")){
      filtro = "";
    }else{
      fechaDesde = $("#fecha-desde").val();
      fechaHasta = $("#fecha-hasta").val();
      filtro = "&fechaDesde="+fechaDesde+"&fechaHasta="+fechaHasta;
    }
    articulo_id = $("#articulos-select").val();
    if(articulo_id){
      filtro = filtro + "&articulo_id="+articulo_id
    }
    if($("#filtra_usuario").attr("checked")){
      filtro += '&filtra_usuario=1'
    }else{
      filtro += '&filtra_usuario=0'
    }

    if((fechaDesde != "" & fechaHasta != "") | articulo_id)
      window.location="<?php echo $this->createUrl("novedad/crudNovedades"); ?>"+filtro;
  }
</script>
