<!--<script src="js/jquery-ui/development-bundle/ui/jquery.ui.draggable.js"></script>
<script src="js/jquery-ui/development-bundle/ui/jquery.ui.droppable.js"></script>-->
<style type="text/css">
    #content{overflow: visible; min-height: 710px}
    #izq,#der{min-height: 700px;margin-bottom: 20px; overflow: auto}
    #izq{float:left;width: 35%}
    #der{float:right;width:64%}
    #niveles{width: 99%;overflow: auto; min-width: 50px}
    .nivel, .anio, .division{cursor:pointer}
    #anios{width: 49%;min-height: 50px; float: left}
    #divisiones{width: 49%;min-height: 50px;float: left}
    #articulos{padding: 3px; overflow: auto}
    #alumnos{padding: 3px; overflow: auto; min-height: 50px}
    .p{cursor:move}
    .articulo, .anio, .division, .nivel, .alumno{padding: 2px; margin: 3px;}
    .articulo, .alumno{ float:left}
    .over {
        background-color: #cedae3;
    }
    .out {
        background-color: #a6bcce;
    }
    #datos-dialog{display:none}
    .header{ padding: 5px; margin-bottom: 5px}
    .selected-red{color:red !important;background-color: lightseagreen !important}
    .importe-novedad{width:40px}
    .alumno-seleccionado{ color:red !important}
    #alumnos-drop{ padding: 10px; display: none}
</style>
<?php
//$baseUrl = Yii::app()->request->baseUrl;
//$cs = Yii::app()->clientScript;
//$cs->registerScriptFile($baseUrl . "/js/jquery-1.7.2.min.js", CClientScript::POS_HEAD);
//$cs->registerScriptFile($baseUrl . "/js/jquery-ui-1.8.22.custom.min.js", CClientScript::POS_HEAD);
//$cs->registerScriptFile($baseUrl . "/js/jquery-ui/ui/jquery.ui.draggable.js", CClientScript::POS_HEAD);
//$cs->registerScriptFile($baseUrl . "/js/jquery-ui/ui/jquery.ui.droppble.js", CClientScript::POS_HEAD);
?>
<div id="izq" class="ui-widget ui-corner-all ui-widget-content">
    <div id="niveles" class="ui-widget-content ui-corner-all">
        <div class="header ui-widget-content ui-state-active ui-corner-all">Niveles</div>
        <?php foreach ($niveles as $nivel): ?>
            <div tipo="nivel" nivel_id="<?php echo $nivel->id; ?>" class="nivel ui-state-focus ui-corner-all droppable"><?php echo $nivel->nombre; ?></div>
            <?php
        endforeach;
        ;
        ?>
    </div>
    <div id="anios-divisiones">
        <div id="anios" class="ui-widget-content ui-corner-all">
            <div class="header ui-widget-content ui-state-active ui-corner-all">Años</div>
        </div>
        <div id="divisiones" class="ui-widget-content ui-corner-all">
            <div class="header ui-widget-content ui-state-active ui-corner-all">Divisiones</div>
        </div>
    </div>
</div>
<div id="der" class="ui-widget ui-corner-all ui-widget-content">
    <div id="articulos" class="ui-widget-content ui-corner-all">
        <div class="header ui-widget-header ui-state-active ui-corner-all">Artículos</div>
        <?php foreach ($articulos as $articulo): ?>
            <div class="articulo p draggable ui-widget-header ui-corner-all" 
                 articulo_id="<?php echo $articulo->id; ?>"
                 detalle="<?php echo $articulo->nombre; ?>"
                 importe="<?php echo $articulo->precio_neto; ?>">
                     <?php echo $articulo->nombre; ?>
            </div>
            <?php
        endforeach;
        ;
        ?>
    </div>
    <div id="alumnos" class="ui-widget-content ui-corner-all">
        <div class="header ui-widget-content ui-state-active ui-corner-all">Alumnos</div>
        <div id="alumnos-drop" tipo="alumno" class=" ui-widget-content ui-corner-all droppable">Arrastre aquí para aplicar sobre los alumnos seleccionados</div>
    </div>
</div>

<div id="ret-dialog" >
    <form id="ret-dialog-form">
        <input id="ret-dialog-liquid_conf_id" name="ret-dialog-liquid_conf_id" value="" type="hidden"/>
        <div class="row">
            <span id="ret-dialog-cant"></span>
            <span id="ret-dialog-leyenda"> novedades fueron grabadas</span>
        </div>
        <div class="row">
            <span id="ret-dialog-confirma">Desea verlas/modificarlas?</span>
        </div>
    </form>
</div>

<div id="datos-dialog" >
    <form id="datos-form">
        <input id="articulo_id" name="articulo_id" value="" type="hidden"/>
        <input id="tipo" name="tipo" value="" type="hidden"/>
        <input id="id" name="id" value="" type="hidden"/>
        <div class="row">
            <label for="detalle">Detalle:</label>
            <input id="detalle" name="detalle" value="" class=""/>
        </div>
        <div class="row">
            <label for="importe">Importe:</label>
            <input id="importe" name="importe" value=""/>
        </div>
    </form>
</div>

<script type="text/javascript">
    var liquid_conf_id = "<?php echo $liquid_conf_id ?>";
    var novedad_cab_id;
    $("#niveles").on("click", ".nivel", function() {
        var nivel_id = $(this).attr("nivel_id");
        $("#niveles .nivel").removeClass("selected-red");
        $(this).addClass("selected-red");
        $.ajax({
            type: "GET",
            //      dataType:"json",
            data: {nivel_id: nivel_id},
            url: "<?php echo $this->createUrl("novedad/traeAnios"); ?>",
            success: function(data) {
                $("#anios .anio").remove();
                $("#divisiones .division").remove();
                $("#alumnos .alumno").remove();
                $("#anios").find(".header").after(data);
                makeDroppable();
            },
            error: function(data, status) {
            }
        });
    })

    $("#anios").on("click", ".anio", function() {
        var anio_id = $(this).attr("anio_id");
        $("#anios .anio").removeClass("selected-red");
        $(this).addClass("selected-red");
        $.ajax({
            type: "GET",
            //      dataType:"json",
            data: {anio_id: anio_id},
            url: "<?php echo $this->createUrl("novedad/traeDivisiones"); ?>",
            success: function(data) {
                $("#divisiones .division").remove();
                $("#alumnos .alumno").remove();
                $("#divisiones").find(".header").after(data);
                makeDroppable();
            },
            error: function(data, status) {
            }
        });
    })

    $("#divisiones").on("click", ".division", function() {
        var division_id = $(this).attr("division_id");
        $("#divisiones .division").removeClass("selected-red");
        $(this).addClass("selected-red");
        $.ajax({
            type: "GET",
            //      dataType:"json",
            data: {division_id: division_id},
            url: "<?php echo $this->createUrl("novedad/traeAlumnos"); ?>",
            success: function(data) {
                $("#alumnos .alumno").remove();
                $("#alumnos").find("#alumnos-drop").after(data);
                makeDroppable();
            },
            error: function(data, status) {
            }
        });
    });

    $('.draggable').draggable({
        helper: "clone",
        revert: true
    });

    $(".p").disableSelection();

    $("#datos-dialog").dialog({
        autoOpen: false,
        buttons: {
            Ok: function() {
                $(this).dialog("close");
                sube();
            },
            Cancela: function() {
                $(this).dialog("close");
            }
        }
    })

    $("#ret-dialog").dialog({
        autoOpen: false,
        buttons: {
            Si: function() {
                $(this).dialog("close");
                crudNovedades(novedad_cab_id);
            },
            No: function() {
                $(this).dialog("close");
            }
        },
        close: function() {
            $(".alumno").removeClass("alumno-seleccionado");
            $("#alumnos-drop").hide();
        }
    })

    makeDroppable();

    function makeDroppable() {
        var $target, tipo, id;
        $(".droppable").droppable({
            //    tolerance: 'touch',
            accept: ".draggable",
            //activeClass: "ui-state-hover",
            hoverClass: "ui-state-active",
            drop: function(event, ui) {
                $target = $(event.target);
                tipo = $target.attr("tipo");
                id = $target.attr(tipo + "_id");
                $("#datos-form #articulo_id").val($(ui.draggable).attr("articulo_id"));
                $("#datos-form #detalle").val($(ui.draggable).attr("detalle"));
                $("#datos-form #importe").val($(ui.draggable).attr("importe"));
                $("#datos-form #tipo").val(tipo);
                $("#datos-form #id").val(id);
                $("#datos-dialog").dialog("open");
            }
        });
    }

    function sube() {
        var data = $("#datos-form").serialize() + "&liquid_conf_id=" + liquid_conf_id;
        var cant = $(".alumno-seleccionado").length;
        if (cant) {
            $(".alumno-seleccionado").each(function() {
                data += "&alumnos[]=" + $(this).attr("alumno_id");
            });
        }
        $.ajax({
            type: "POST",
            dataType: "json",
            data: data,
            url: "<?php echo $this->createUrl("novedad/sube"); ?>",
            success: function(data) {
                if (data.errores) {
                    alert(data.errores);
                } else {
                    $("#ret-dialog-cant").html(data.cant);
                    $("#ret-dialog-liquid_conf_id").html(data.liquid_conf_id);
                    $("#ret-dialog").dialog("open");
                    novedad_cab_id = data.novedad_cab_id;
                }
            },
            error: function(data, status) {
            }
        });
    }

    function crudNovedades(novedad_cab_id) {
        var url = "<?php echo $this->createUrl('novedad/crudNovedades'); ?>" + "&novedad_cab_id=" + novedad_cab_id;
        window.location = url;
    }

    function alumnoClick(obj) {
        var cant;
        $alu = $(obj);
        $alu.toggleClass("alumno-seleccionado");
        cant = $(".alumno-seleccionado").length;
        if (cant > 0) {
            $("#alumnos-drop").show();
            $(".alumno").droppable({accept: ""});
        } else {
            $("#alumnos-drop").hide();
            makeDroppable();
        }
    }
</script>
