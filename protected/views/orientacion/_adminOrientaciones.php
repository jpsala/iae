<form id="form-orientacion-nueva" method="POST" action="#">

    <table class="ui-widget ui-widget-content" id="tabla-orientaciones">
        <tr class="ui-widget-header">
            <th>Nombre</th>
            <th>&nbsp</th>
        </tr>
        <?php $row = 0;?>
        <?php foreach (Orientacion::model()->with(array("anio","anio.nivel"))->findAll("Anio_id = $anio_id") as $orientacion): ?>
            <tr class="tr-orientacion <?php echo ($row++ % 2) ? "tr-even" : ""; ?>" orientacion_id="<?php echo $orientacion->id; ?>">
                <td class="td-nombre"><?php echo $orientacion->nombre; ?></td>
                <td class="td-borra" onclick="return borraOrientacion(<?php echo $orientacion->id; ?>)"></td>
            </tr>
        <?php endforeach; ?>
        <tr class="tr-orientacion <?php echo ($row++ % 2) ? "tr-even" : ""; ?>" id="tr-orientacion-nueva">
            <td class="td-nombre"><input id="nombre-orientacion" name="nombre" onchange="return changeNombreOrientacion(this)"/></td>
            <td ><input type="button" id="graba-orientacion-nueva-button" value="Graba" onclick="return grabaOrientacion(this)"/></td>
        </tr>
    </table>
</form>