
<script type="text/javascript">
    
    var nivel_id, anio_id, $tr_orientacion_nueva,  division_id;
    var $tr_orientacion = $('<tr orientacion_id="8" class="tr-orientacion ">  <td class="td-nombre">ddvs</td> <td onclick="return borraOrientacion(8)" class="td-borra"></td> </tr>');
    
    $(function(){
        init();
    });
    
    function init(){
        
        $("button").button();
        
        $("#nivel-select").chosen().change(function(){
            changeNivel($(this));
        });

        $("#anio-select").chosen().change(function(){
            changeAnio($(this));
        });

       
        $(".clearinput").clearinput();
        
        $("#nivel-select_chzn").mousedown();
        
    }
    
    function changeNivel($this){
        nivel_id = $this.val();
        $.ajax({
            type: "GET",
            //dataType: "json",
            data: {nivel_id:$this.val()},
            url: "<?php echo $this->createUrl("anio/options"); ?>",
            success: function(data) {
                $("#anio-select").html(data);
                $("#anio-select").trigger("liszt:updated");
                $("#anio-select_chzn").mousedown();

            },
            error: function(data,status){
            }
        });
        return false;
    }

    function changeAnio($this){
        //        pedidoDeDatosVisible(false);
        anio_id = $this.val();
        traeOrientaciones($this);
        return false;
    }
    
   function changeNombreOrientacion($this){
        if( ! $("#nombre-orientacion").val()){
            $("#nombre-orientacion").val($($this).val());
        }
    }
    
    
    
    function pedidoDeDatosVisible(visible){
        $("#pedido-de-datos").animate({"opacity": visible ? "show":"hide"},"slow");    
    }
    
    function traeOrientaciones($this){
        anio_id = $this.val();
        $.ajax({
            type: "GET",
            //dataType: "json",
            data: {nivel_id:nivel_id,anio_id:anio_id},
            url: "<?php echo $this->createUrl("anio/adminOrientaciones"); ?>",
            success: function(data) {
                $("#orientaciones").html(data);
                $("#graba-orientacion-nueva-button").button();
                $(".tr-orientacion input:not(:hidden)").first().focus();
                //$tr_orientacion = $(".tr-orientacion").first().clone();
                $(".tr-orientacion:not('#tr-orientacion-nueva')").find("td:not('.td-borra')").click(function(){clickOrientacion($(this).parent())});
            },
            error: function(data,status){
            }
        });
    }
    
    function clickOrientacion($this){
        
        $rec = $("#tr-orientacion-nueva");
        $rec.find("#nombre-orientacion").val($this.find(".td-nombre").html());
        $rec.attr("orientacion_id",$this.attr("orientacion_id"));
        $rec.find("input:not(:hidden)").first().focus();
    }
    
    function grabaOrientacion(input){
        var $tr;
        $rec = $(input).closest("tr");
        orientacion_id = $rec.attr("orientacion_id");
        var data = "Anio_id=" + anio_id + "&" + "orientacion_id=" + orientacion_id + "&" + $rec.find("*").serialize();;
        $.ajax({
            type: "POST",
            dataType: "json",
            data: data,
            url: "<?php echo $this->createUrl("anio/adminOrientacionesGraba"); ?>",
            success: function(data) {
                if(data.error){
                    alert("Error en los datos, verifique")
                }else{
                    if(!data.nuevo){
                        $tr = $('[orientacion_id="%id%"]'.replace("%id%",data.orientacion_id)).first();
                        $rec.removeAttr("orientacion_id");
                    }else{
                        //$tr = $tr_orientacion.clone();
                        $tr = $tr_orientacion;
                        $tr.find(".td-borra").attr("onclick","return borraOrientacion("+data.orientacion_id+")");
                    }
                    $tr.attr("orientacion_id",data.orientacion_id);
                    $tr.find(".td-nombre").html($rec.find("#nombre-orientacion").val());
                    if(data.nuevo){
                        $rec.before($tr);
                        $tr.find("td:not('.td-borra')").click(function(){clickOrientacion($(this).parent())});
                    }
                    $rec.find("#nombre-orientacion").val("");
                    $rec.find("input:not(:hidden)").first().focus();
                }
            },
            error: function(data,status){
            }
        });

    }
    
    function cancela(){
        pedidoDeDatosVisible(true);
        limpiaTodo();
        return false;
    }
    
       
    function borraOrientacion(orientacion_id){
        $.ajax({
            type: "GET",
            //dataType: "json",
            data: {orientacion_id:orientacion_id},
            url: "<?php echo $this->createUrl("anio/adminOrientacionesBorra"); ?>",
             
            success: function(data) {
                if (!data){
                $('[orientacion_id="%id%"]'.replace("%id%",orientacion_id)).remove();
                }else{alert('La orientación se encuentra en uso');}
            },
            error: function(data,status){
            }
        });

        return false;
    }    
    
    function limpiaTodo(){
        
    }
    
</script>
