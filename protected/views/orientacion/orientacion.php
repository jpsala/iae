<?php include("orientacion.css.php"); ?>
<?php include("orientacion.js.php"); ?>
<div id="pedido-de-datos">
    <div class="row">
        <select id="nivel-select" data-placeholder="Nivel" class="chzn-select">
            <?php
            $x = array('prompt' => '');
            echo CHtml::listOptions(null, CHtml::listData(Nivel::model()->findAll(array("order"=>"orden")), 'id', 'nombre'), $x)
            ?>
        </select>
    </div>
    <div class="row">
        <select id="anio-select" data-placeholder="Año" class="chzn-select">
            <?php
            $x = array('prompt' => '');
            echo CHtml::listOptions(null, CHtml::listData(Anio::model()->findAll("id=-1"), 'id', 'nombre'), $x)
            ?>
        </select>
    </div>
</div>
<form id="form-nivel" method="POST" action ="#" name="form-nivel">
    
</form>
<div id="orientaciones">
</div>

<!--    <div class="buttons-div">
        <button type="button" name="cancela" id="cancela" onclick="return cancela()">Cancela</button>
        <button type="button" name="ok" id="ok">Graba</button>
    </div>-->
<!--</div>-->

