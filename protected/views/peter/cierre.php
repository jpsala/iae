<div class="container top30">
    <div class="col-xs-6">
        <div class="panel panel-default">
            <div class="panel-heading">Informe cierre de período</div>
            <div class="panel-body">
                <form class="form-inline">
                    <div class='row'>
                        <div class="col-xs-24">
                            <div class='form-group'>
                                <div class="input-group">
                    <span class="input-group-btn">
                            <button class="btn btn-default"
                                    onclick="$('#desde').focus()"
                                    type="button">
                                <i class="fa fa-calendar fa-fw"></i>
                            </button>
                    </span>
                                    <input type="text" placeholder="Desde Fecha"
                                           class="form-control"
                                           name="desde" id="desde"
                                           data-bind="datepicker: desde"/>
                                </div>
                                <!--                <input type="text" data-bind="datepicker: birthdayMoment, datepickerOptions: { format: 'D/M/YY' }" class="span2">-->
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="col-xs-24">
                            <div class='form-group'>
                                <div class="input-group">
                    <span class="input-group-btn">
                            <button class="btn btn-default"
                                    onclick="$('#hasta').focus()"
                                    type="button">
                                <i class="fa fa-calendar fa-fw"></i>
                            </button>
                    </span>
                                    <input type="text" placeholder="Hasta Fecha"
                                           class="form-control"
                                           name="hasta" id="hasta"
                                           data-bind="datepicker: hasta"/>
                                </div>
                                <!--                <input type="text" data-bind="datepicker: birthdayMoment, datepickerOptions: { format: 'D/M/YY' }" class="span2">-->
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="col-xs-24 top10">
                            <button class="btn btn-sm pull-right btn-primary"
                                    data-bind="click:submit">Ok
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row" id="data">

</div>

<script>
    Vm = function () {
        var url="<?php echo $this->createUrl('/peter/cierrePDF');?>";
        var self = this;
        self.desde = ko.observable('');
        self.hasta = ko.observable('');
        self.submit = function () {
            window.location = url + '&desde='+self.desde()+'&hasta='+self.hasta()+'&empresa=iae';
//            return;
//            $.ajax({
//                data:{desde:self.desde(),hasta:self.hasta(),empresa:'iae'},
//                url:url,
//                type:'GET',
//                success:function(data){
//                    $("#data").html(data);
//                    console.log(data);
//                }
//            });
        };
    }
    ko.applyBindings(new Vm());
</script>