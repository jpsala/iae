<?php

	ini_set('memory_limit', '-1');
    $sql = "
        SELECT d.fecha_valor as fecha,
          c.id AS concepto_id, d.total,
          case WHEN s.Proveedor_id THEN p.razon_social ELSE d.detalle END AS detalle,
          c1.abreviacion AS comprob, d.numero
        FROM doc d
            LEFT JOIN concepto c ON d.concepto_id = c.id
            INNER JOIN talonario t ON d.talonario_id = t.id
            INNER JOIN comprob c1 ON t.comprob_id = c1.id
            LEFT JOIN socio s ON d.Socio_id = s.id
            LEFT JOIN proveedor p ON s.Proveedor_id = p.id
        WHERE
            d.talonario_id IN (33,34,32) AND not d.anulado and not d.filtrado and d.activo and
            d.fecha_valor BETWEEN '$desde' AND '$hasta'
        order by d.fecha_valor, d.id
    ";
    //vd($sql);
    //vd($desde,$hasta);
	$rows = Helpers::qryAll($sql);
    //vd($rows[0]);die;
    if(!$rows){
        return;
    }
    foreach ($rows as $key=>$row) {
        $rows[$key]['concepto'] = $row['concepto_id']?Concepto::path($row['concepto_id']):'';
        //$rows[$key]['fecha'] = date('d/m/Y', strtotime($row['fecha']));
    }
    //vd($rows);
    $sheetIndex = 0;

    $objPHPExcel = new MyPHPExcel();

    $mesMasAnio = date('M-Y',strtotime($rows[0]['fecha']));
    $as = createSheet($objPHPExcel, $sheetIndex++, $mesMasAnio);
    $primerHoja = strftime('%B-%Y', strtotime($row['fecha']));
	$i = 2;
	foreach ($rows as $row) {
        if($mesMasAnio !== date('M-Y',strtotime($row['fecha']))){
            $mesMasAnio = date('M-Y',strtotime($row['fecha']));
            $as =  createSheet($objPHPExcel, $sheetIndex++, strftime('%B-%Y', strtotime($row['fecha'])));
            $i = 2;
        }
        $as->SetCellValue('A'.$i, date('d/m/Y',strtotime($row['fecha'])));
        $as->SetCellValue('b'.$i, $row['detalle']);
        $as->SetCellValue('c'.$i, $row['concepto']);
        $as->SetCellValue('d'.$i, $row['comprob']);
        $as->SetCellValue('e'.$i, $row['numero']);
        $as->SetCellValue('f'.$i, $row['total']);
        $i++;
    }

//    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//    $objWriter->save('php://output');
    $objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->output("peterExportation");

    function createSheet($objPHPExcel, $sheetId, $titulo){
        $objPHPExcel->createSheet($sheetId);
        $objPHPExcel->setActiveSheetIndex($sheetId);
        $as = $objPHPExcel->getActiveSheet();
        $as->setTitle($titulo);
        $as->SetCellValue('A1', "Fecha");
        $as->SetCellValue('B1', "Descripción");
        $as->SetCellValue('C1', "Concepto");
        $as->SetCellValue('D1', "Comprob.");
        $as->SetCellValue('E1', "Número");
        $as->SetCellValue('F1', "Importe");
        //$as->setAutoFilter("A1:d1");
        $as->getColumnDimension('A')->setAutoSize(true);
        $as->getColumnDimension('b')->setAutoSize(true);
        $as->getColumnDimension('c')->setAutoSize(true);
        $as->getColumnDimension('d')->setAutoSize(true);
        $as->getColumnDimension('e')->setAutoSize(true);
        $as->getColumnDimension('f')->setAutoSize(true);
        return $as;
    }