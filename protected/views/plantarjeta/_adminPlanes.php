<form id="form-plan-nuevo" method="POST" action="#">

    <table class="ui-widget ui-widget-content" id="tabla-orientaciones">
        <tr class="ui-widget-header">
            <th>Nombre</th>
            <th>Cuotas</th>
            <th>Activo</th>
            <th>&nbsp</th>
        </tr>
        <?php $row = 0;?>
        
        <?php foreach (TarjetaPlan::model()->findAll("tarjeta_id = $tarjeta_id") as $plan): //with(array("anio","anio.nivel"))->?>
            <tr class="tr-plan <?php echo ($row++ % 2) ? "tr-even" : ""; ?>" plan_id="<?php echo $plan->id; ?>">
                <td class="td-nombre"><?php echo $plan->nombre; ?></td>
                <td class="td-cuotas"><?php echo $plan->cuotas; ?></td>
                <td class="td-activo"><?php echo CHtml::checkBox("activo", $plan->activo); ?></td>
                <td class="td-borra" onclick="return borraPlan(<?php echo $plan->id; ?>)"></td>
            </tr>
        <?php endforeach; ?>
        <tr class="tr-plan <?php echo ($row++ % 2) ? "tr-even" : ""; ?>" id="tr-plan-nuevo">
            <td class="td-nombre"><input id="nombre-plan" name="nombre" onchange="return changeNombrePlan(this)"/></td>
            <td class="td-cuotas"><input id="cuota-plan" name="cuota" /></td>
<!--            <td class="td-activo"><?php echo CHtml::checkBox("activo_new", 0); ?></td>-->
            <td><input type="button" id="graba-plan-nuevo-button" value="Graba" onclick="return grabaPlan(this)"/></td>
        </tr>
    </table>
</form>