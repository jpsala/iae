
<script type="text/javascript">
    
    var plan_id, tarjeta_id, $tr_orientacion_nueva,  division_id;
    var $tr_plan = $('<tr plan_id="8" class="tr-plan "><td class="td-nombre">ddvs</td><td class="td-cuotas">3</td> <td class="td-activo"><input id="activo" type="checkbox" name="activo" value="1"></td><td onclick="return borraplan(8)" class="td-borra"></td> </tr>');
    $(function(){
        init();
    });
    
    function init(){
        
        $("button").button();
        
        $("#tarjeta-select").chosen().change(function(){
            changeTarjeta($(this));
        });

       
        $(".clearinput").clearinput();
        
        $("#tarjeta-select_chzn").mousedown();
        
    }
    
   
    function changeTarjeta($this){
        //        pedidoDeDatosVisible(false);
        tarjeta_id = $this.val();
        traePlanes($this);
        return false;
    }
    
   function changeNombrePLan($this){
        if( ! $("#nombre-plan").val()){
            $("#nombre-plan").val($($this).val());
        }
    }
    
    
    
    function pedidoDeDatosVisible(visible){
        $("#pedido-de-datos").animate({"opacity": visible ? "show":"hide"},"slow");    
    }
    
    function traePlanes($this){
        tarjeta_id = $this.val();
        $.ajax({
            type: "GET",
            //dataType: "json",
            data: {tarjeta_id:tarjeta_id},
            url: "<?php echo $this->createUrl("crud/adminPlanes"); ?>",
            success: function(data) {
                $("#planes").html(data);
                $("#graba-plan-nuevo-button").button();
                $(".tr-plan input:not(:hidden)").first().focus();
                //$tr_orientacion = $(".tr-orientacion").first().clone();
                $(".tr-plan:not('#tr-plan-nuevo')").find("td:not('.td-borra')").click(function(){clickPlan($(this).parent())});
            },
            error: function(data,status){
            }
        });
    }
    
    function clickPlan($this){
        $rec = $("#tr-plan-nuevo");
        $rec.find("#nombre-plan").val($this.find(".td-nombre").html());
        $rec.find("#cuota-plan").val($this.find(".td-cuotas").html());
        $rec.attr("plan_id",$this.attr("plan_id"));
        $rec.find("input:not(:hidden)").first().focus();
    }
    
    function grabaPlan(input){
        var $tr;
        $rec = $(input).closest("tr");
        plan_id = $rec.attr("plan_id");
        var data = "tarjeta_id=" + tarjeta_id + "&" + "plan_id=" + plan_id + "&" + $rec.find("*").serialize();;
        $.ajax({
            type: "POST",
            dataType: "json",
            data: data,
            url: "<?php echo $this->createUrl("crud/adminPlanesGraba"); ?>",
            success: function(data) {
                if(data.error){
                    alert("Error en los datos, verifique")
                }else{
                    if(!data.nuevo){
                        $tr = $('[plan_id="%id%"]'.replace("%id%",data.plan_id)).first();
                        $rec.removeAttr("plan_id");
                    }else{
                        //$tr = $tr_orientacion.clone();
                        $tr = $tr_plan;
                        $tr.find(".td-borra").attr("onclick","return borraPlan("+data.plan_id+")");
                    }
                    $tr.attr("plan_id",data.plan_id);
                    $tr.find(".td-nombre").html($rec.find("#nombre-plan").val());
                    $tr.find(".td-cuotas").html($rec.find("#cuota-plan").val());
                    //$tr.find(".td-activo").html($rec.find("#activo_new").val());
                    if(data.nuevo){
                        $rec.before($tr);
                        $tr.find("td:not('.td-borra')").click(function(){clickPlan($(this).parent())});
                    }
                    $rec.find("#nombre-plan").val("");
                    $rec.find("input:not(:hidden)").first().focus();
                }
            },
            error: function(data,status){
            }
        });

    }
    
    function cancela(){
        pedidoDeDatosVisible(true);
        limpiaTodo();
        return false;
    }
    
       
    function borraPlan(plan_id){
        $.ajax({
            type: "GET",
            //dataType: "json",
            data: {plan_id:plan_id},
            url: "<?php echo $this->createUrl("crud/adminPlanesBorra"); ?>",
             
            success: function(data) {
                if (!data){
                $('[plan_id="%id%"]'.replace("%id%",plan_id)).remove();
                }else{alert('El plan se encuentra en uso');}
            },
            error: function(data,status){
            }
        });

        return false;
    }    
    
    function limpiaTodo(){
        
    }
    
</script>

