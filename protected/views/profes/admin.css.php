<style>
    .row-domicilio input { width: auto; display: inline }
    .row-domicilio .form-group { width: inherit; display: inline }
    label { width: inherit /*145px*/; /*text-align: right*/ }
    label.label-sm { width: 100px }
    label.label-xsm { width: inherit /*65px*/ }
    .label-lg { width: 171px }
    .row-domicilio label { width: inherit; text-align: inherit }
    #profes-div{font-size: 90%}
    #profes-div form{font-size: 90%}
    #profes-div .row{margin-bottom: 1px}
</style>