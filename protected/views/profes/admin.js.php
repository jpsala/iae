<script>

    var admin = (function () {
        var self = this;
        var pub = {};
        pub.isAdmin = <?php echo $isAdmin ? 'true':'false';?>;
        pub.showLog = false;
        pub.datos = ko.observable(ko.wrap.fromJS(<?php echo json_encode($datos);?>));
        pub.isDirty = ko.observable(false);
        pub.niveles = ko.wrap.fromJS(<?php echo json_encode($niveles);?>);
        pub.usuarios = ko.wrap.fromJS(<?php echo json_encode($usuarios);?>);
        pub.selectedTr = null;
        pub.selectedUser = ko.observable(null);
        pub.filter = ko.observable("");
//        pub.urlGraba;
//        pub.urlDatosUsuario;
        pub.clearFilter = function () {
            pub.filter('');
            pub.filter.valueHasMutated();
        };
        pub.usuariosFiltrados = ko.computed(function () {
            var filter = pub.filter().toLowerCase();
            if (!filter) {
                return pub.usuarios();
            } else {
                return ko.utils.arrayFilter(pub.usuarios(), function (item) {
                    var found = false;
                    if (item.apellido() && (item.apellido().toLowerCase().indexOf(filter) !== -1)) {
                        found = true;
                    } else if (item.nombre() && (item.nombre().toLowerCase().indexOf(filter) !== -1)) {
                        found = true;
                    }
                    return found;
                });
            }

        }).extend({rateLimit: {timeout: 500, method: "notifyWhenChangesStop"}});
        pub.nombreCompleto = function (user) {
            return (user.apellido() || '') + ', ' + user.nombre();
        };
        pub.antiguedad_iae = ko.computed(function () {
            return getAntiguedad(pub.datos().fecha_ingreso_iae());
        });
        pub.antiguedad_docencia = ko.computed(function () {
            return getAntiguedad(pub.datos().fecha_ingreso_docencia());
        });
        // computed for isDirty
        ko.computed(function () {
            ko.toJSON(pub.datos);
            pub.isDirty(true)
        });
        pub.selectUser = function (user, e) {
            pub.selectedTr = e.target;
            pub.selectedUser(user);
            $.ajax({
                url: pub.urlDatosUsuario,
                data: {isAdmin: true, user_id: user.id()},
                method: 'get',
                dataType: 'json',
                success: function (data) {
                    ko.wrap.updateFromJS(pub.datos, data);
                    pub.isDirty(false);
                }
            })

        };
        pub.graba = function (a, b, c) {
            if (!pub.urlGraba) {
                alert('falta asignar el url para grabar los datos');
                return;
            }
            var data = pub.datos();
            $.ajax({
                url: pub.urlGraba,
                data: data,
                method: 'post',
                dataType: 'json',
                success: function (data) {
                    toastr.success('Los datos fueron grabados...');
                    updateSelectedTr();
//                    pub.dirtyFlag().reset();
                    pub.isDirty(false);
                }
            })
        };
        pub.afterRender = function (a, b, c) {
            console.log(a, b, c);
        }
        function updateSelectedTr() {
            if (pub.selectedTr) {
                $(pub.selectedTr).text(pub.datos().apellido() + ', ' + pub.datos().nombre());
            }
        };
        function getAntiguedad(fecha) {
            var m = moment(fecha, "YYYY-MM-DD"),
                h = moment(new Date()),
                diff = h.diff(m),
                duration = moment.duration(diff),
                durationText = '';
            if(duration.years() > 0){
                durationText += duration.years() + ' años ';
            }
            if(duration.months() > 0){
                durationText += duration.months() + ' meses ';
            }
            if(duration.days() > 0){
                durationText += duration.days() + ' dias';
            }
            return durationText;
        }
        setTimeout(function () {
            pub.isDirty(false)
        });
        return pub;

    }())
</script>