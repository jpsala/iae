<?php include('admin.css.php'); ?>
<?php include('admin.js.php'); ?>
<div id="profes-div" class="container top30" data-bind="afterRender: afterRender">
    <div class="row">
        <div class="col-xs-8"
             data-bind="visible: isAdmin">
            <?php include('admin_usuarios.php'); ?>
        </div>
        <div data-bind="css:{ 'col-xs-16': isAdmin, 'col-xs-24': !isAdmin }">
            <div class="panel panel-default">
                <div class="panel-body">
                    {{#ifnot: !isAdmin || (isAdmin && selectedUser)}}
                        <strong>Seleccione un usuario...</strong>
                    {{/ifnot}}
                    {{#if: !isAdmin || (isAdmin && selectedUser)}}
                        <?php include('admin_form.php'); ?>
                    {{/if}}
                </div>
                <div class="panel-footer" style="overflow: auto">
                    <input type="button"
                           class="btn btn-sm btn-info pull-right"
                           data-bind="click:graba, enable:isDirty"
                           value="Graba"/>
                </div>
            </div>
        </div>
    </div>
</div>
<div data-bind="visible:showLog" class="top30">
    <div class="col-xs-12">
        <pre data-bind="text: ko.toJSON(datos,null,2)"></pre>
    </div>
    <div class="col-xs-12">
        <pre data-bind="text: ko.toJSON(niveles,null,2)"></pre>
    </div>
</div>
<script>
    admin.urlGraba = "<?php echo $this->createUrl('/profes/adminGraba');?>";
    admin.urlDatosUsuario = "<?php echo $this->createUrl('/profes/adminGetDatosUsuario');?>";
    ko.applyBindings(admin);
</script>