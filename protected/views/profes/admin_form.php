<form class="form">
<fieldset>
    <legend data-bind="text:'FICHA DATOS PERSONALES'"></legend>
</fieldset>
<fieldset>
    <legend>Datos personales</legend>
    <div class='row'>
        <div class='col-xs-3 col-sm-3 col-md-2'>
            <div class='form-group'>
                <label class="label-sm" for="profe">Profe
                    <input type="checkbox" class="form-control" id="profe" name="profe"
                           data-bind="checked:datos().profe() == 1, valueUpdate:'input',
                                value:datos().profe"
                           size="15" type="text"/>
                </label>
            </div>
        </div>
        <div class='col-xs-11 col-sm-6 col-md-8'>
            <div class='form-group'>
                <label class="label-sm" for="login">Login</label>
                <input class="form-control" id="login" name="login"
                       data-bind="value:datos().login, valueUpdate:'input'"
                       size="15" type="text"/>
            </div>
        </div>
        <div class='col-xs-10 col-sm-7 col-md-7'>
            <div class='form-group'>
                <label class="label-sm" for="legajo">Legajo Nro.</label>
                <input class="form-control" id="legajo" name="legajo"
                       data-bind="value:datos().legajo, valueUpdate:'input'"
                       size="15" type="text"/>
            </div>
        </div>
        <div class="clearfix visible-xs-block"></div>
        <div class='col-xs-12 col-sm-8 col-md-6'>
            <div class='form-group'>
                <label class="label-sm" for="nivel_id">Nivel</label>
                <select class="form-control" id="nivel_id"
                        name="nivel_id"
                        data-bind="options: niveles(),
                                                optionsText:'nombre',
                                                optionsValue: 'id',
                                                value:datos().nivel_id, valueUpdate:'input'">
                </select>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class='col-xs-12 col-sm-12 col-md-10'>
            <div class='form-group'>
                <label class="label-sm" for="nombre">Nombre</label>
                <input class="form-control" id="nombre" name="nombre"
                       size="20" type="text"
                       data-bind="value:datos().nombre, valueUpdate:'input'"/>
            </div>
        </div>
        <div class='col-xs-12 col-sm-12 col-md-10'>
            <div class='form-group fright'>
                <label class="label-sm" for="apellido">Apellido</label>
                <input class="form-control" id="apellido"
                       name="apellido" required="true" size="20"
                       type="text"
                       data-bind="value:datos().apellido, valueUpdate:'input'"/>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class='col-xs-12 col-sm-12 col-md-10'>
            <div class='form-group'>
                <label class="label-sm" for="doc">Documento</label>
                <input class="form-control required" id="doc"
                       name="documento" required="true" size="18"
                       type="text" placeholder="Tipo y Nro."
                       data-bind="value:datos().documento, valueUpdate:'input'"/>
            </div>
        </div>
        <div class='col-xs-12 col-sm-12 col-md-10'>
            <div class='form-group fright'>
                <label class="label-lg" for="lugar_nacimiento">Lugar de
                    Nacimiento</label>
                <input class="form-control" id="lugar_nacimiento"
                       name="lugar_nacimiento" required="true" size="20"
                       type="text"
                       data-bind="value:datos().lugar_nacimiento, valueUpdate:'input'"/>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class='col-xs-24 col-sm-12'>
            <div class='form-group'>
                <label class="label-sm" for="estado_civil">Estado civil</label>
                <input class="form-control required" id="estado_civil"
                       name="estado_civil" required="true" size="15"
                       type="text" placeholder=""
                       data-bind="value:datos().estado_civil, valueUpdate:'input'"/>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Datos domicilio</legend>
    <div class='row'>
        <div class="col-xs-24 col-sm-12 col-md-10">
            <div class='form-group'>
                <!--                        <label class="label-sm" for="domicilio_calle">Domicilio</label>-->
                <input class="form-control" id="domicilio_calle"
                       name="domicilio_calle" placeholder="Calle"
                       size="50" type="text"
                       data-bind="value:datos().domicilio_calle, valueUpdate:'input'"/>
            </div>
        </div>
        <div class="col-xs-8 col-sm-4 col-md-3">
            <div class='form-group'>
                <input class="form-control" id="domicilio_numero"
                       data-bind="value:datos().domicilio_numero, valueUpdate:'input'"
                       name="domicilio_numero"
                       placeholder="Nro."
                       size="" type="text"/>
            </div>
        </div>
        <div class="col-xs-8 col-sm-4 col-md-3">
            <div class='form-group'>
                <input class="form-control" id="domicilio_piso"
                       name="domicilio_piso" placeholder="Piso"
                       size="" type="text"
                       data-bind="value:datos().domicilio_piso, valueUpdate:'input'"/>
            </div>
        </div>
        <div class="col-xs-8 col-sm-4 col-md-3">
            <div class='form-group'>
                <input class="form-control" id="domicilio_depto"
                       name="domicilio_depto" placeholder="Depto."
                       size="" type="text"
                       data-bind="value:datos().domicilio_depto, valueUpdate:'input'"/>
            </div>
        </div>
    </div>
    <div class="row form">
        <div class="col-xs-12 col-sm-12 col-md-8">
            <div class="form-group">
                <label class="label-sm" for="telefono">Teléfonos</label>
                <input class="form-control" id="telefono"
                       name="telefono"
                       data-bind="value:datos().telefono, valueUpdate:'input'"
                       size="" type="text" placeholder="Casa"/>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-8">
            <div class="form-group">
                <label class="visible-x" for="celular">&nbsp;</label>
                <input class="form-control" id="celular"
                       name="celular"
                       data-bind="value:datos().celular, valueUpdate:'input'"
                       size="12" type="text" placeholder="Celular"/>
            </div>
        </div>
        <div class="col-xs-12 col-sm-10 col-md-10">
            <div class="form-group">
                <label class="label-sm" class="" for="email">Email</label>
                <input class="form-control" id="email"
                       name="email" data-bind="value:datos().email, valueUpdate:'input'"
                       size="" type="text"/>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Otros Datos</legend>
    <div class='row'>
        <div class="col-xs-12 col-sm-8">
            <div class='form-group'>
                <label class="label-sm" for="titulo">Título</label>
                <input class="form-control" id="titulo"
                       name="titulo" placeholder=""
                       size="20" type="text"
                       data-bind="value:datos().titulo, valueUpdate:'input'"/>
            </div>
        </div>
        <div class="col-xs-12 col-sm-8">
            <div class='form-group'>
                <label class="label-sm" for="expedido_por">Expedido Por</label>
                <input class="form-control" id="expedido_por"
                       name="expedido_por" placeholder=""
                       size="19" type="text"
                       data-bind="value:datos().expedido_por, valueUpdate:'input'"/>
            </div>
        </div>
        <div class="col-xs-12 col-sm-8">
            <div class='form-group'>
                <label class="label-sm" for="registro_numero">Registro Nro.</label>
                <input class="form-control" id="registro_numero"
                       name="registro_numero" placeholder=""
                       size="19" type="text"
                       data-bind="value:datos().registro_numero, valueUpdate:'input'"/>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class="col-xs-12 col-sm-8">
            <div class='form-group'>
                <label class="label-sm" for="cargo">Cargo</label>
                <input class="form-control" id="cargo"
                       name="cargo" placeholder=""
                       size="15" type="text"
                       data-bind="value:datos().cargo, valueUpdate:'input'"/>
            </div>
        </div>
        <div class="col-xs-12 col-sm-8">
            <div class='form-group'>
                <label class="label-xsm" for="function">Función</label>
                <input class="form-control" id="funtion"
                       name="funcion" placeholder=""
                       size="15" type="text"
                       data-bind="value:datos().funcion, valueUpdate:'input'"/>
            </div>
        </div>
        <div class="col-xs-12 col-sm-8">
            <div class='form-group'>
                <label class="label-xsm" for="categoria">Categoría</label>
                <input class="form-control" id="categoria"
                       name="categoria" placeholder=""
                       size="15" type="text"
                       data-bind="value:datos().categoria, valueUpdate:'input'"/>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class="col-xs-12">
            <div class='form-group'>
                <label for="fecha_ingreso_iae">Fecha ingreso al IAE</label>

                <div class="input-group">
                          <span class="input-group-btn">
                            <button class="btn btn-default"
                                    onclick="$('#fecha_ingreso_iae').focus()"
                                    type="button">
                                <i class="fa fa-calendar fa-fw"></i>
                            </button>
                          </span>
                    <input type="text" placeholder="" class="form-control"
                           name="fecha_ingreso_iae" id="fecha_ingreso_iae"
                           data-bind="datepicker: datos().fecha_ingreso_iae"/>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class='form-group'>
                <label for="antiguadad_iae">Antiguedad</label>
                <input class="form-control" id="antiguadad_iae"
                       name="antiguadad_iae" placeholder=""
                       size="12" type="text"
                       data-bind="value:antiguedad_iae, valueUpdate:'input'"/>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class="col-xs-12">
            <div class='form-group'>
                <label class="visible-sm visible-md visible-lg visible-xl"
                       for="fecha_ingreso_docencia">Fecha ingreso a la
                    docencia</label>
                <label class="visible-xs" for="fecha_ingreso_docencia">Fec. ing. a la
                    docencia</label>

                <div class="input-group">
                    <span class="input-group-btn">
                            <button class="btn btn-default"
                                    onclick="$('#fecha_ingreso_docencia').focus()"
                                    type="button">
                                <i class="fa fa-calendar fa-fw"></i>
                            </button>
                    </span>
                    <input type="text" placeholder="" class="form-control"
                           name="fecha_ingreso_docencia" id="fecha_ingreso_docencia"
                           data-bind="datepicker: datos().fecha_ingreso_docencia"/>
                </div>
                <!--                <input type="text" data-bind="datepicker: birthdayMoment, datepickerOptions: { format: 'D/M/YY' }" class="span2">-->
            </div>
        </div>
        <div class="col-xs-12">
            <div class='form-group'>
                <label for="antiguadad_docencia">Antiguedad</label>
                <input class="form-control" id="antiguadad_docencia"
                       name="antiguadad_docencia" placeholder=""
                       size="12" type="text"
                       data-bind="value:antiguedad_docencia, valueUpdate:'input'"/>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class="col-xs-12">
            <div class='form-group'>
                <label for="obra_social">Obra Social</label>
                <input class="form-control" id="obra_social"
                       name="obra_social" placeholder=""
                       size="12" type="text"
                       data-bind="value:datos().obra_social, valueUpdate:'input'"/>
            </div>
        </div>
        <div class="col-xs-12">
            <div class='form-group'>
                <label for="obra_social_numero">Nro. de Afiliado</label>
                <input class="form-control" id="obra_social_numero"
                       name="obra_social_numero" placeholder=""
                       size="10" type="text"
                       data-bind="value:datos().obra_social_numero_afiliado, valueUpdate:'input'"/>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class="col-xs-12">
            <div class='form-group'>
                <label for="afiliado_sindicato">Afiliado a algún sindicato</label>
                <input class="form-control" id="afiliado_sindicato"
                       name="afiliado_sindicato" placeholder=""
                       size="12" type="text"
                       data-bind="value:datos().afiliado_sindicato, valueUpdate:'input'"/>
            </div>
        </div>
        <div class="col-xs-12">
            <div class='form-group'>
                <label for="afiliado_sindicato_numero">Nro. de afiliado al
                    sindicato</label>
                <input class="form-control" id="afiliado_sindicato_numero"
                       name="afiliado_sindicato_numero" placeholder=""
                       size="10" type="text"
                       data-bind="value:datos().afiliado_sindicato_numero, valueUpdate:'input'"/>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Actividades Docentes en otras Instituciones</legend>
</fieldset>
</form>

