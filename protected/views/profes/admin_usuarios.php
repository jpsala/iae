<div class="row">
    <div class="col-xs-24">
        <input type="text" data-bind="value:filter, valueUpdate:'input'"
               class="form-control" placeholder="Filtro"/>
            <span data-bind="click:clearFilter,visible: filter().length"
                  id="searchclear" class="inputclear glyphicon glyphicon-remove-circle">
            </span>
    </div>
</div>
<div class="row">
    <div class="col-xs-24">
        <table
            class="table table-hover table-striped table-bordered table-condensed">
            <tr>
                <th>Usuarios</th>
            </tr>
            <tbody data-bind="foreach: {data:usuariosFiltrados, as:'user'}">
            <tr data-bind="
                        click: $parent.selectUser, attr:{id:user.id},
                        css:{'success': user == $parent.selectedUser()}">
                <td data-bind="text: $parent.nombreCompleto(user)"></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
