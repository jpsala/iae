<?php

$tr = Yii::app()->db->beginTransaction();

$sAlumnosRotos = "
    select a.matricula, a.id
        from alumno a
       where (select count(*) from alumno_division ad where ad.alumno_id = a.id and ad.activo = 1 ) > 1
       order by a.matricula
    ";
$alumnos = Helpers::qryAllObj($sAlumnosRotos);
foreach ($alumnos as $a) {
    $sAlumnosDup = "
        select a.apellido, a.matricula, ad.id, an.nombre as anio, d.nombre as division, n.nombre as nivel,
                    ad.id as alumno_division_id
           from  alumno_division ad
                inner join alumno a on a.id = ad.alumno_id
                inner join division d on d.id = ad.division_id
                inner join anio an on an.id = d.anio_id
                inner join nivel n on n.id = an.nivel_id
           where ad.Alumno_id = $a->id
            order by ad.id asc
        ";
    $ads = Helpers::qryAllObj($sAlumnosDup);
//    $al = Alumno::model()->findByPk($a->id);
//    $al->alumno_division_id_anterior_borrar = $a->alumno_division_id;
    ve($ads[0]->matricula, $ads[0]->alumno_division_id."/".$ads[0]->nivel."/".$ads[0]->anio."/".$ads[0]->division, $ads[1]->alumno_division_id."/".  $ads[1]->nivel."/".$ads[1]->anio."/".$ads[1]->division);
    //d($alumnosDup[1]);
//    if(!$al->save()){
//        vd($al->errors);
//    }
}

dd("fin");

$ciclo_viejo = Ciclo::getActivo();
/* @var $ciclo_viejo Ciclo */
$ciclo_nuevo = new Ciclo();
$ciclo_nuevo->attributes = $ciclo_viejo->attributes;
$ciclo_viejo->activo = 0;
$ciclo_viejo->fecha_fin = date("Y/m/d", time() - (3600 * 24));
$ciclo_nuevo->fecha_inicio = date("d.m.Y", time());
$ciclo_nuevo->fecha_fin = date("d.m.Y", time() + (3600 * 24 * 363));
$ciclo_nuevo->nombre = $nombreCicloNuevo;
//$ciclo_nuevo->activo = 1;
if (!$ciclo_viejo->save()) {
    vd($ciclo_viejo->errors);
}
if (!$ciclo_nuevo->save()) {
    vd($ciclo_nuevo->errors);
}
$ciclo_viejo->refresh();
$ciclo_nuevo->refresh();

//vd($ciclo_actual->attributes, $ciclo_nuevo->attributes);
$ls = Helpers::qryAll("
    select *
	from logica l
		inner join logica_ciclo lc on lc.Logica_id = l.id and lc.Ciclo_id = $ciclo_viejo->id
    ");
foreach ($ls as $l) {
    $ln = new Logica();
    $ln->attributes = $l;
    if (!$ln->save()) {
        vd($ln->errors);
    }
    $lcs = Helpers::qryAll("
        select * from logica_ciclo lc where lc.ciclo_id = $ciclo_viejo->id and lc.Logica_id = :logica_id
    ", array("logica_id" => $l["Logica_id"]));
    foreach ($lcs as $lc) {
        $lcn = new LogicaCiclo();
        $lcn->attributes = $lc;
        $lcn->Ciclo_id = $ciclo_nuevo->id;
        $lcn->Logica_id = $ln->id;
        if (!$lcn->save()) {
            vd($lcn->errors);
        }
    }

    $lps = Helpers::qryAll("
            select * from logica_periodo lp where lp.logica_id = :logica_id
            ", array("logica_id" => $l["Logica_id"])
    );
    foreach ($lps as $lp) {
        $lpn = new LogicaPeriodo();
        $lpn->attributes = $lp;
        $lpn->fecha_inicio = date("Y/m/d", strtotime($lp["fecha_inicio"]) + 3600 * 24 * (365 + 1));
        $lpn->fecha_inicio_ci = date("Y/m/d", strtotime($lp["fecha_inicio_ci"]) + 3600 * 24 * (365 + 1));
        $lpn->fecha_fin = date("Y/m/d", strtotime($lp["fecha_fin"]) + 3600 * 24 * (365 + 1));
        $lpn->fecha_fin_ci = date("Y/m/d", strtotime($lp["fecha_fin_ci"]) + 3600 * 24 * (365 + 1));
        $lpn->fecha_inicio = date("Y/m/d", time() + 3600 * 24 * 365);
        $lpn->Logica_id = $ln->id;
        if (!$lpn->save()) {
            vd($lpn->errors);
        }
        $lis = Helpers::qryAll("
                select * from logica_item li where li.Logica_Periodo_id = :logica_periodo_id
                ", array("logica_periodo_id" => $lp["id"])
        );
        foreach ($lis as $li) {
            $lin = new LogicaItem();
            $lin->attributes = $li;
            $lin->Logica_id = $ln->id;
            $lin->Logica_Periodo_id = $lpn->id;
            if (!$lin->save()) {
                vd($lin->errors);
            }
        }
    }
}
$sIngresantes = "
                select  ad.id as alumno_division_id
                      from alumno_division ad 
                          inner join division d on d.id = ad.Division_id and d.activo = 1
                          inner join anio a on a.id = d.Anio_id
                          inner join nivel n on n.id = a.Nivel_id
                          inner join alumno al on al.id = ad.Alumno_id
                          inner join socio s on s.Alumno_id = al.id
                      where ad.Ciclo_id = $ciclo_viejo->id and al.activo = 0 and al.ingresante = 1
            ";
$ingresantes = Helpers::qryAllObj($sIngresantes);
foreach ($ingresantes as $i) {
    $ad = AlumnoDivision::model()->findByPk($i->alumno_division_id);
    /* @var $ad AlumnoDivision */
    $ad->Ciclo_id = $ciclo_nuevo->id;
    $ad->activo = 1;
    if (!$ad->save()) {
        vd($ad->errors);
    }
    $a = Alumno::model()->findByPk($ad->Alumno_id);
    /* @var $a Alumno */
    $a->activo = 1;
    if (!$a->save()) {
        vd($a->errors);
    }
}
$sAlumnos = "
                select al.matricula, concat(al.nombre, ', ', al.apellido) as alumno_nombre, dmat.id as division_siguiente_id,
                            ad.id as alumno_division_id
                      from alumno_division ad 
                          inner join division d on d.id = ad.Division_id and d.activo = 1
                          inner join division dmat on dmat.id = d.division_id_siguiente
                          inner join anio a on a.id = d.Anio_id
                          inner join anio amat on amat.id = dmat.Anio_id
                          inner join nivel n on n.id = a.Nivel_id
                          inner join nivel nmat on nmat.id = amat.Nivel_id
                          inner join alumno al on al.id = ad.Alumno_id
                          inner join socio s on s.Alumno_id = al.id
                      where ad.Ciclo_id = $ciclo_viejo->id and ad.activo = 1 and al.activo = 1 and al.ingresante = 0 
                      order by al.apellido, al.nombre
            ";
$rows = Helpers::qryAll($sAlumnos);
foreach ($rows as $row) {
    $cicloActivo = Ciclo::getActivo()->id;
    $ad = AlumnoDivision::model()->findByPk($row["alumno_division_id"]);
    /* @var $ad AlumnoDivision */
    $nad = new AlumnoDivision();
    $nad->Division_id = $ad->siguiente_division_id;
    $nad->fecha_alta = new CDbExpression("now()");
    $nad->activo = 1;
    $division_id = Helpers::qryScalar("
        select d.division_id_siguiente
	from division d
	where d.id = :division_id
        ", array("division_id" => $ad->Division_id));
    $nad->Division_id = $division_id;
    $siguiente_division_id = Helpers::qryScalar("
        select d.division_id_siguiente
	from division d
	where d.id = :division_id
        ", array("division_id" => $division_id));
    $nad->siguiente_division_id = $siguiente_division_id;
    $ad->activo = 0;
    $ad->fecha_baja = new CDbExpression("now()");
    $nad->Alumno_id = $ad->Alumno_id;
    $nad->Ciclo_id = $ciclo_nuevo->id;
    $ad->save();
    $nad->save();
}
$tr->commit();

