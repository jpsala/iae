<?php

	$tr = Yii::app()->db->beginTransaction();
	/*
	 * si se pasó el año se usa la siguiente variable
	 * y $diasParaAtras con los días necesarios para volver al año anterior
	 */
 $estamosEnElSiguienteAnio = true;
 $diasParaAtras = (3600*24*4);

	$time = $estamosEnElSiguienteAnio ? (time() - $diasParaAtras) : time();
	$nombreCicloNuevo = date("Y", $time);
	$existe = Helpers::qryScalar("
        select count(*) from ciclo where nombre = \"$nombreCicloNuevo\"
  ");
	if ($existe) {
		throw new Exception("Ya existe el ciclo $nombreCicloNuevo");
	}

	$ciclo_viejo = Ciclo::getActivo();
	/* @var $ciclo_viejo Ciclo */
	$ciclo_nuevo = new Ciclo();
	$ciclo_nuevo->attributes = $ciclo_viejo->attributes;
	$ciclo_viejo->activo = 0;
	//throw new Exception("antes que nada mirar como quedan las fecha abajo!!!!!");
	/* Esto parece andar bien!
	  update logica_periodo lc
	  inner join logica_ciclo c on c.Logica_id = lc.Logica_id and c.Ciclo_id = 2
	  inner join asignatura_tipo a on a.id = c.Asignatura_Tipo_id
	  SET lc.fecha_inicio_ci = date_add(lc.fecha_inicio_ci, INTERVAL 1 YEAR),
	  lc.fecha_fin_ci = date_add(lc.fecha_fin_ci, INTERVAL 1 YEAR),
	  lc.fecha_inicio = date_add(lc.fecha_inicio, INTERVAL 1 YEAR),
	  lc.fecha_fin = date_add(lc.fecha_fin, INTERVAL 1 YEAR)
	 */

	$ciclo_viejo->fecha_fin = date("Y/m/d", $time - (3600 * 24));
	$ciclo_nuevo->fecha_inicio = date("Y/m/d", $time);
	$ciclo_nuevo->fecha_fin = date("Y/m/d", $time + (3600 * 24 * 364));
	$ciclo_nuevo->nombre = $nombreCicloNuevo;
	// vd2($ciclo_viejo->attributes, $ciclo_nuevo->attributes);
//$ciclo_nuevo->activo = 1;
// vd2($ciclo_viejo, $ciclo_nuevo);
	if (!$ciclo_viejo->save()) {
		vd($ciclo_viejo->errors);
	}
	if (!$ciclo_nuevo->save()) {
		vd($ciclo_nuevo->errors);
	}
	$ciclo_viejo->refresh();
	$ciclo_nuevo->refresh();
//vd($ciclo_actual->attributes, $ciclo_nuevo->attributes);
	$ls = Helpers::qryAll("
    select *
     from logica l
     inner join logica_ciclo lc on lc.Logica_id = l.id and lc.Ciclo_id = $ciclo_viejo->id
    ");
	foreach ($ls as $l) {
		$ln = new Logica();
		$ln->attributes = $l;
		if (!$ln->save()) {
			vd($ln->errors);
		}
		$lcs = Helpers::qryAll("
        select * from logica_ciclo lc where lc.ciclo_id = $ciclo_viejo->id and lc.Logica_id = :logica_id
    ", array("logica_id" => $l["Logica_id"]));
		foreach ($lcs as $lc) {
			$lcn = new LogicaCiclo();
			$lcn->attributes = $lc;
			$lcn->Ciclo_id = $ciclo_nuevo->id;
			$lcn->Logica_id = $ln->id;
			if (!$lcn->save()) {
				vd($lcn->errors);
			}
		}

		$lps = Helpers::qryAll("
            select * from logica_periodo lp where lp.logica_id = :logica_id
            ", array("logica_id" => $l["Logica_id"])
		);
		foreach ($lps as $lp) {
			$lpn = new LogicaPeriodo();
			$lpn->attributes = $lp;
			$lpn->fecha_inicio = date("Y/m/d", strtotime($lp["fecha_inicio"]) + 3600 * 24 * (365 + 1));
			$lpn->fecha_inicio_ci = date("Y/m/d", strtotime($lp["fecha_inicio_ci"]) + 3600 * 24 * (365 + 1));
			$lpn->fecha_fin = date("Y/m/d", strtotime($lp["fecha_fin"]) + 3600 * 24 * (365 + 1));
			$lpn->fecha_fin_ci = date("Y/m/d", strtotime($lp["fecha_fin_ci"]) + 3600 * 24 * (365 + 1));
			$lpn->fecha_inicio = date("Y/m/d", $time + 3600 * 24 * 365);
			$lpn->Logica_id = $ln->id;
			if (!$lpn->save()) {
				vd($lpn->errors);
			}
			$lis = Helpers::qryAll("
                select * from logica_item li where li.Logica_Periodo_id = :logica_periodo_id
                ", array("logica_periodo_id" => $lp["id"])
			);
			foreach ($lis as $li) {
				$lin = new LogicaItem();
				$lin->attributes = $li;
				$lin->Logica_id = $ln->id;
				$lin->Logica_Periodo_id = $lpn->id;
				if (!$lin->save()) {
					vd($lin->errors);
				}
			}
		}
	}
	$tr->commit();

	