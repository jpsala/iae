<?php
	// estoy promocionando en el año anterior, si no es así restar 1 de la linea de abajo
 $time = time() - (3600*24*10);
	$nombreCicloViejo = date("Y", $time)-1;
  $nombreCicloNuevo = date("Y", $time);
  // vd2($nombreCicloViejo, $nombreCicloNuevo);
$ciclo_nuevo = Ciclo::model()->find("nombre = '$nombreCicloNuevo'");
$ciclo_viejo = Ciclo::model()->find("nombre = '$nombreCicloViejo'");
//	vd2($ciclo_viejo->nombre,$ciclo_nuevo->nombre);
	if ($ciclo_viejo->nombre == $ciclo_nuevo->nombre) {
		throw new Exception("Primero genere las lógicas, no hay ciclo nuevo");
	}
	$sPromocionados = "
    select count(*)
        from alumno_division ad
        where ad.Ciclo_id = $ciclo_nuevo->id
    ";
	$promocionados = Helpers::qryScalar($sPromocionados);
	if ($promocionados) {
		throw new Exception("Los alumnos fueron promocionados - " . $promocionados);
	}

	$tr = Yii::app()->db->beginTransaction();


	/*
	 * Grabo el estado anterior
	 */
	$sAlumnos = "
    select a.id, ad.id as alumno_division_id, a.ingresante
        from alumno a
            inner join alumno_division ad on ad.alumno_id = a.id and ad.activo
    ";
	$alumnos = Helpers::qryAll($sAlumnos);
	foreach ($alumnos as $a) {
		$al = Alumno::model()->findByPk($a['id']);
		$al->alumno_division_id_anterior_borrar = $a['alumno_division_id'];
    $al->estado_id_anterior = $al->estado_id;
    $al->visa_numero = "";
		$al->ex_ingresante = $al->ingresante;
		if (!$al->save()) {
			vd($al->errors);
		}
	}


	/*
	 * Los que egresan
	 */
	$sAlumnosEgresan = "
		select al.id as alumno_id, al.matricula, concat(al.nombre, ', ', al.apellido) as alumno_nombre,
					ad.id as alumno_division_id
			from alumno_division ad
					inner join division d on d.id = ad.Division_id and d.activo = 1
					inner join anio a on a.id = d.Anio_id
					inner join nivel n on n.id = a.Nivel_id
					inner join alumno al on al.id = ad.Alumno_id
					inner join socio s on s.Alumno_id = al.id
					inner join alumno_estado ae on ae.id = al.estado_id and !ae.ingresante and ae.activo_edu
			where ad.Ciclo_id = $ciclo_viejo->id and ad.activo = 1 and al.activo = 1
										and d.division_id_siguiente is null
			order by al.apellido, al.nombre
	";
	$rows = Helpers::qryAllObj($sAlumnosEgresan);
//	vd($rows);
	$estado_egresado = AlumnoEstado::model()->find('nombre = "egresado"');
	foreach ($rows as $row) {
		/* @var $a Alumno */
		$a = Alumno::model()->findByPk($row->alumno_id);
		$a->estado_id = $estado_egresado->id;
		$a->egresado = 1;
		if (!$a->save()) {
			vd($a->errors);
		}
		/* @var $adEgresado AlumnoDivision */
		$adEgresado = AlumnoDivision::model()->findByPk($row->alumno_division_id);
		$adEgresado->activo = 0;
		$adEgresado->egresado = 1;
		if (!$adEgresado->save()) {
			vd($adEgresado->errors);
		}
	}

	/*
	 * Promocion
	 */
	$estado_regular = AlumnoEstado::model()->find('nombre = "regular"');
	$estado_regular_id = $estado_regular['id'];
	$sAlumnos = "
	select al.matricula, concat(al.nombre, ', ', al.apellido) as alumno_nombre,
				dmat.id as division_siguiente_id, ad.id as alumno_division_id,
				al.id as alumno_id
		from alumno_division ad
				inner join division d on d.id = ad.Division_id and d.activo = 1
				inner join division dmat on dmat.id = d.division_id_siguiente
				inner join anio a on a.id = d.Anio_id
				inner join anio amat on amat.id = dmat.Anio_id
				inner join nivel n on n.id = a.Nivel_id
				inner join nivel nmat on nmat.id = amat.Nivel_id
				inner join alumno al on al.id = ad.Alumno_id
				inner join alumno_estado ae on ae.id = al.estado_id and ae.id = $estado_regular_id
				inner join socio s on s.Alumno_id = al.id
		where ad.Ciclo_id = $ciclo_viejo->id and ad.activo = 1 and al.activo = 1
		order by al.apellido, al.nombre
  ";
	$rows = Helpers::qryAll($sAlumnos);
	foreach ($rows as $row) {
		$a = Alumno::model()->findByPk($row['alumno_id']);
		/* @var $a Alumno */
		$a->estado_id = $estado_regular->id;
		if (!$a->save()) {
			vd('alumno estado a egresado', $a->attributes, $a->errors);
		}

		// nueva alumno_division
		$nad = new AlumnoDivision();
		$nad->Division_id =$row['division_siguiente_id'];
		$nad->fecha_alta = new CDbExpression("now()");
		$nad->activo = 1;
		$nad->Alumno_id = $row['alumno_id'];
		$nad->Ciclo_id = $ciclo_nuevo->id;
		/* @var $ad AlumnoDivision */
		$ad = AlumnoDivision::model()->findByPk($row["alumno_division_id"]);
		$nad->siguiente_division_id = $ad->siguiente_division_id;

		// modifico la division actual (anterior)
		$ad->activo = 0;
		$ad->promocionado = 1;
		$ad->fecha_baja = new CDbExpression("now()");
		if(!$ad->save()){
			vd('alumno_division_anterior desactivada',$ad->attributes,$ad->errors);
		}
		if(!$nad->save()){
			vd('alumno_division_nueva activada',$nad->attributes,$nad->errors);
		}
	}

	/*
	 * Ingresantes a Regulares
	 */
	$sIngresantes = "
		select ad.id as alumno_division_id, al.id as alumno_id, al.matricula, a.nombre, d.nombre
			from alumno_division ad
        INNER JOIN alumno_division_estado ade ON ad.alumno_division_estado_id = ade.id AND ade.id = 1
        inner join division d on d.id = ad.Division_id and d.activo = 1
        inner join anio a on a.id = d.Anio_id
        inner join nivel n on n.id = a.Nivel_id
        inner join alumno al on al.id = ad.Alumno_id
        inner join alumno_estado ae on ae.id = al.estado_id and ae.ingresante
        inner join socio s on s.Alumno_id = al.id
			where ad.Ciclo_id = $ciclo_viejo->id  AND !ad.borrado
  ";
	$ingresantes = Helpers::qryAllObj($sIngresantes);
//	vd2($ingresantes);
	$estado_regular = AlumnoEstado::model()->find('nombre = "regular"');
	foreach ($ingresantes as $i) {
		$ad = AlumnoDivision::model()->findByPk($i->alumno_division_id);
		$alu = Alumno::model()->findByPk($i->alumno_id);
		$alu->ingresante = 0;
		$alu->estado_id = 1;
		if(!$alu->save()){
		  vd('error alumnos save()', $alu->errors);
  }
		/* @var $ad AlumnoDivision */
		$ad->Ciclo_id = $ciclo_nuevo->id;
		$ad->alumno_division_estado_id = 1;
		$ad->activo = 1;

		if (!$ad->save()) {
			vd($ad->errors);
		}
		$a = Alumno::model()->findByPk($ad->Alumno_id);
		/* @var $a Alumno */
		$a->activo = 1;
		$a->estado_id = $estado_regular->id;
		if (!$a->save()) {
			vd($a->errors);
		}
	}

	$tr->commit();





	//
//// arrebla 5, C y D pasaron a C, borro los 5 C y D del ciclo nuevo y los regenero
//$ingresantesMal = Helpers::qryAllObj("
//    select id from alumno a
//    where a.matricula in (2346,2373,2682,2926,2927,3057,3095,3136,3306,3329,3543,3553,3561,3587,3599,4047,3526,3527,3528,3532,3533,3534,3535,3536,3537,3538,3623,3684,3711,3825,3831,3833,3912,3914,3918,3923,3988,4142,3672,4538,3141,3216,3218,3227,3370,3371,3374,3524,3525,3530,3541,3572,3710,3824,3911,3915,3916,4112,4228,4331)
// ");
//$tr = Yii::app()->db->beginTransaction();
//foreach ($ingresantesMal as $a) {
//    echo Helpers::qryExec("
//        delete from alumno_division where alumno_id = $a->id and Ciclo_id = 2
// ");
//}
//$sAlumnos = "
//                select al.matricula, concat(al.nombre, ', ', al.apellido) as alumno_nombre, dmat.id as division_siguiente_id,
//                            ad.id as alumno_division_id
//                      from alumno_division ad
//                          inner join division d on d.id = ad.Division_id and d.activo = 1
//                          inner join division dmat on dmat.id = d.division_id_siguiente
//                          inner join anio a on a.id = d.Anio_id
//                          inner join anio amat on amat.id = dmat.Anio_id
//                          inner join nivel n on n.id = a.Nivel_id
//                          inner join nivel nmat on nmat.id = amat.Nivel_id
//                          inner join alumno al on al.id = ad.Alumno_id
//                          inner join socio s on s.Alumno_id = al.id
//                      where al.matricula in (2346,2373,2682,2926,2927,3057,3095,3136,3306,3329,3543,3553,3561,3587,3599,4047,3526,3527,3528,3532,3533,3534,3535,3536,3537,3538,3623,3684,3711,3825,3831,3833,3912,3914,3918,3923,3988,4142,3672,4538,3141,3216,3218,3227,3370,3371,3374,3524,3525,3530,3541,3572,3710,3824,3911,3915,3916,4112,4228,4331)
//                      order by al.apellido, al.nombre
//            ";
//$rows = Helpers::qryAll($sAlumnos);
//foreach ($rows as $row) {
//    $cicloActivo = $ciclo_ant_id;
//    $ad = AlumnoDivision::model()->findByPk($row["alumno_division_id"]);
//    /* @var $ad AlumnoDivision */
//    $nad = new AlumnoDivision();
//    $nad->Division_id = $ad->siguiente_division_id;
//    $nad->fecha_alta = new CDbExpression("now()");
//    $nad->activo = 1;
//    $division_id = Helpers::qryScalar("
//        select d.division_id_siguiente
//	from division d
//	where d.id = :division_id
//        ", array("division_id" => $ad->Division_id));
//    $nad->Division_id = $division_id;
//    $siguiente_division_id = Helpers::qryScalar("
//        select d.division_id_siguiente
//	from division d
//	where d.id = :division_id
//        ", array("division_id" => $division_id));
//    $nad->siguiente_division_id = $siguiente_division_id;
//    $ad->activo = 0;
//    $ad->fecha_baja = new CDbExpression("now()");
//    $nad->Alumno_id = $ad->Alumno_id;
//    $nad->Ciclo_id = $ciclo_nuevo->id;
//    $ad->save();
//    $nad->save();
//}
//$tr->commit();
//return;
//arregla egresados
//$tr = Yii::app()->db->beginTransaction();
//$sAlumnosEgresan = "
//                select al.id as alumno_id, al.matricula, concat(al.nombre, ', ', al.apellido) as alumno_nombre,
//                            ad.id as alumno_division_id
//                      from alumno_division ad
//                          inner join division d on d.id = ad.Division_id and d.activo = 1
//                          inner join anio a on a.id = d.Anio_id
//                          inner join nivel n on n.id = a.Nivel_id
//                          inner join alumno al on al.id = ad.Alumno_id
//                          inner join socio s on s.Alumno_id = al.id
//                      where ad.Ciclo_id = $ciclo_ant_id and ad.activo = 0 and al.activo = 1 and al.ingresante = 0
//                                    and d.division_id_siguiente is null
//                      order by al.apellido, al.nombre
//            ";
//$rows = Helpers::qryAllObj($sAlumnosEgresan);
//$divEgresados_id = Helpers::qryScalar("select id from division where egresados = 1");
//foreach ($rows as $a) {
//    $ad = new AlumnoDivision();
//    $ad->Ciclo_id = $ciclo_nuevo->id;
//    $ad->Division_id = $divEgresados_id;
//    $ad->activo = 1;
//    $ad->Alumno_id = $a->alumno_id;
//    $ad->fecha_alta =date("d.m.Y", $time);
//    if(!$ad->save()){
//        vd($ad->errors);
//    }
//    ve($ad->attributes);
//}
//$tr->commit();
//return;
//$ingresantesMal = Helpers::qryAll("
//        select a.apellido, a.matricula, ad.id, an.nombre as anio, d.nombre as division, n.nombre as nivel,
//                        ad.id as alumno_division_id, ad.activo, a.ingresante
//               from  alumno_division ad
//                    inner join alumno a on a.id = ad.alumno_id
//                    inner join division d on d.id = ad.division_id
//                    inner join anio an on an.id = d.anio_id
//                    inner join nivel n on n.id = an.nivel_id
//               where (select count(*) from alumno_division ad2 where ad2.Alumno_id = ad.Alumno_id) > 1 and a.ingresante = 1
//                order by a.id, ad.id asc
//    ");
//if ($ingresantesMal) {
//    //vd("Alumnos mal ingresados, revisar primero...",$ingresantesMal);
//}

