<style>
    .actual-td{width:300px}
</style>
<?php $cant = 0;?>
<button <?php echo $existe?'DISABLED=DISABLED':''?> onclick="return submit();"><?php echo !$existe?"Crea ciclo nuevo y demás":"Ciclo nuevo ya fué creado";?></button>
<button <?php echo $promocionados?'DISABLED=DISABLED':''?> onclick="return promocionaAlumnos();"><?php echo $promocionados?'Alumnos ya promocionados':'Promociona los alumnos'?></button>
<table>
    <tr>
        <th>Actual</th>
        <th>Promociona a</th>
        <th>Cantidad</th>
    </tr>
    <?php foreach ($divs as $div) : ?>
        <td class="actual-td"><?php echo $div["nivel"] . " " . $div["anio"] . " " . $div["division"]; ?></td>
        <td><?php echo $div["nivel_sig"] . " " . $div["anio_sig"] . " " . $div["division_sig"]; ?></td>
        <td><?php echo $div["count"]; ?></td>
        <tr>
            <td>
                <table>
                    <tr>
                        <th>Alumno</th>
                    </tr>
                    <?php foreach ($div["rows"] as $row) : ?>
                        <?php $cant++; ?>
                        <tr>
                            <td><?php echo $row["alumno_nombre"]; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </td>
        </tr>
        <?php
        ?>
    <?php endforeach; ?>
    <tr>
        <td colspan="2">Cantidad promocionados</td>
        <td><?php echo $cant; ?></td>
    </tr>
</table>

<script>
    function submit() {
        window.location = "<?php echo $this->createUrl("promocion"); ?>";
    }
    function promocionaAlumnos() {
      window.location = "<?php echo $this->createUrl("promocionAlumnos"); ?>";
    }
</script>
