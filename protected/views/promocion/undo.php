<?php

$ciclo_viejo = Ciclo::getActivo();

$tr = Yii::app()->db->beginTransaction();

if ($ciclo_viejo->nombre !== date("Y", time())) {
    throw new Exception("Primero genere las lógicas");
}
$nombreCicloAnt = date("Y", time()) - 1;
$ciclo_viejo_id = Helpers::qryScalar("select id from ciclo where nombre = \"$nombreCicloAnt\"");
if (!$ciclo_viejo_id) {
    vd("Ciclo anterior ($nombreCicloAnt) no encontrado");
}

$sPromocionados = "
    select count(*)
        from alumno_division ad
        where ad.Ciclo_id = $ciclo_viejo->id
    ";
$promocionados = Helpers::qryScalar($sPromocionados);

if (!$promocionados) {
    throw new Exception("Los alumnos no fueron promocionados, no hay nada que deshacer");
}

$sAlumnos = "
                select al.id, al.matricula, concat(al.nombre, ', ', al.apellido) as alumno_nombre,
                            ad.id as alumno_division_id, al.alumno_division_id_anterior_borrar as ant
                      from alumno_division ad 
                          inner join division d on d.id = ad.Division_id and d.activo = 1
                          inner join anio a on a.id = d.Anio_id
                          inner join nivel n on n.id = a.Nivel_id
                          inner join alumno al on al.id = ad.Alumno_id
                          inner join socio s on s.Alumno_id = al.id
                      where ad.Ciclo_id = $ciclo_viejo_id and al.activo = 1
                      order by n.orden, a.orden, d.orden, al.apellido, al.nombre, ad.id
            ";
$rows = Helpers::qryAllObj($sAlumnos/* , null , null, $dbBK */);

/* @var $dbBK CDbConnection */
$total = 0;
$totalNo = 0;
$no = array();
foreach ($rows as $a) {
    $alumno = Alumno::model()->findBySql("select * from alumno a where a.id = $a->id");
    /* @var $alumno Alumno */
    $ant = AlumnoDivision::model()->findByPk($a->alumno_division_id);
    $sAdAct = "
        select * from alumno_division ad
            where ad.Alumno_id = $a->id and ad.Ciclo_id = $ciclo_viejo->id
     ";
    $act = AlumnoDivision::model()->findBySql($sAdAct);
    /* @var $act AlumnoDivision */
    /* @var $ant AlumnoDivision */
    if (!$act and !$alumno->ex_ingresante) {
        //ve("error", $alumno->attributes, $ant->attributes, $sAdAct);
        $totalNo++;
        $mal = Helpers::qryAll("
                select a.matricula, an.nombre as año, n.nombre as nivel  from alumno a
                    inner join alumno_division ad on ad.Alumno_id = a.id
                    inner join division d on d.id = ad.Division_id
                    inner join anio an on an.id = d.Anio_id
                    inner join nivel n on n.id = an.Nivel_id
                where a.id = $alumno->id
            ");
        $no[] = $mal;
    } else {
        $total++;
        if ($alumno->ex_ingresante) {
            ve("ingresante?");
        }
        $act->delete();
        if (!$act->save()) {
            vd($act->errors);
        }
        $ant->Ciclo_id = $ciclo_viejo_id;
        $ant->activo = 1;
        if (!$ant->save()) {
            vd($ant->errors);
        }
        $alumno->ingresante = $alumno->ex_ingresante;
        if ($alumno->ingresante) {
            $alumno->activo = 0;
        }
        if (!$alumno->save()) {
            vd($alumno->errors);
        }
//        ve($a->alumno_nombre, $ant->division->anio->nivel->nombre . " " . $ant->division->anio->nombre . " " . $ant->division->nombre . " - " . $act->division->anio->nivel->nombre . " " . $act->division->anio->nombre . " " . $act->division->nombre);
    }
}
vd("Total undo: $total, no tocados: $totalNo", $no);
//$tr->commit();

