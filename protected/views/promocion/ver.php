<?php
$nombreCicloAnt = date("Y", time()) - 1;
$ciclo_viejo_id = Helpers::qryScalar("select id from ciclo where nombre = \"$nombreCicloAnt\"");
$ciclo_nuevo_id = Ciclo::getActivoId();

$ingresantesCicloAnt = Helpers::qryAllObj("
        select a.id as alumno_id, ad.id, a.matricula, nivel.nombre as nivel, anio.nombre as anio, d.nombre as division, 
                                a.activo, ad.activo, c.nombre, a.obs_cobranza, saldo(s.id,null), a.ex_ingresante
		from alumno_division ad
			inner join ciclo c on c.id = ad.Ciclo_id
			inner join alumno a on a.id = ad.Alumno_id
			inner join socio s on s.Alumno_id = a.id
			inner join division d on d.id = ad.Division_id
			inner join anio on anio.id = d.Anio_id
			inner join nivel on nivel.id = anio.Nivel_id
	where ad.ciclo_id = $ciclo_viejo_id and a.activo = 1 or a.ingresante = 1
          order by nivel.orden, anio.orden, d.orden, a.apellido
    ");
$alumnos = array();
foreach ($ingresantesCicloAnt as $im) {
    $act = Helpers::qryObj("
            select ad.id, nivel.nombre as nivel, anio.nombre as anio, d.nombre as division, c.nombre as ciclo
		from alumno_division ad
			inner join ciclo c on c.id = ad.Ciclo_id
			inner join alumno a on a.id = ad.Alumno_id
			inner join socio s on s.Alumno_id = a.id
			inner join division d on d.id = ad.Division_id
			inner join anio on anio.id = d.Anio_id
			inner join nivel on nivel.id = anio.Nivel_id
	where ad.ciclo_id = $ciclo_nuevo_id and a.id = $im->alumno_id and ad.activo = 1
    ");
    $alumnos[$im->alumno_id]["anterior"] = $im;
    $alumnos[$im->alumno_id]["nuevo"] = $act;
}
?>
<table>
    <?php $total = 0; ?>
    <?php foreach ($alumnos as $i): ?>
        <tr>
            <td><?php echo $i["anterior"]->matricula; ?></td>
            <td><?php echo $i["anterior"]->nivel . " " . $i["anterior"]->anio . " " . $i["anterior"]->division; ?></td>
            <?php if (isset($i["nuevo"]->nivel)): ?>
                <?php $total++; ?>
                <td><?php echo $i["nuevo"]->nivel . " " . $i["nuevo"]->anio . " " . $i["nuevo"]->division; ?></td>
                <td><?php echo $i["anterior"]->ex_ingresante == 1? "Ing.":"";?></td>
            <?php else: ?>
                <td><?php echo "Ninguno"; ?></td>
            <?php endif; ?>
            <td></td>
            <td></td>
        </tr>
    <?php endforeach; ?>
    <tr>
        <td><?php echo "total"; ?></td>
        <td><?php echo $total; ?></td>
    </tr>
</table>

