  <div class="form">
    <div class="row">
      <select id="proveedor" data-placeholder="Puede seleccionar un proveedor para modificar" class="chzn-select">
        <?php
        $x = array('prompt' => '');
        echo CHtml::listOptions($model->id, CHtml::listData(Proveedor::model()->findAll(), 'id', 'razon_social'), $x)
        ?>
      </select>
    </div>
<form id="carga" method="post">
  <input type="hidden" id="id" name="id" value="<?php echo $model->id; ?>"/>

  <fieldset id="nombre-fieldset" class="ui-widget-content">
    <legend>Razón Social</legend>
    <input maxlength="45" id="razon_social" name="razon_social" title="Razón Social" value="<?php echo $model->razon_social; ?>" placeholder="Razón Social"/>
    <input maxlength="45" id="nombre_fantasia" name="nombre_fantasia" title="Nombre Fantasía" value="<?php echo $model->nombre_fantasia; ?>" placeholder="Nombre Fantasía"/>
  </fieldset>

  <fieldset id="codigo-fieldset" class="ui-widget-content">
    <legend>Código</legend>
    <input maxlength="30" id="codigo" name="codigo" title="Código" value="<?php echo $model->codigo; ?>" placeholder="Código"/>
  </fieldset>
  
  <fieldset id="datosfiscales-fieldset" class="ui-widget-content">
    <legend>Datos Fiscales</legend>
    <input maxlength="13" id="cuit" name="cuit" title="C.U.I.T." value="<?php echo $model->cuit; ?>" placeholder="C.U.I.T."/>
    <input maxlength="20" id="nro_ingresos_brutos" name="nro_ingresos_brutos" title="II.BB." value="<?php echo $model->nro_ingresos_brutos; ?>" placeholder="II.BB."/>
  </fieldset>
 
  <fieldset id="domicilio-fieldset" class="ui-widget-content">
    <legend>Domicilio</legend>
    <input maxlength="25" id="calle" name="calle" title="Calle" value="<?php echo $model->calle; ?>" placeholder="Calle"/>
    <input maxlength="7" id="numero" name="calle_numero" title="Núm." value="<?php echo $model->calle_numero; ?>" placeholder="Núm."/>
    <input maxlength="4" id="piso" name="calle_piso" title="Piso" value="<?php echo $model->calle_piso; ?>" placeholder="Piso"/>
    <input maxlength="4" id="departamento" name="calle_departamento" title="Depto." value="<?php echo $model->calle_departamento; ?>" placeholder="Depto."/>
    <input maxlength="20" id="codigo_postal" name="codigo_postal" title="Código Postal" value="<?php echo $model->codigo_postal; ?>" placeholder="Código Postal"/>
  </fieldset>

  <fieldset id="telefono-fieldset" class="ui-widget-content">
    <legend>Teléfonos</legend>
    <input maxlength="30" id="telefono1" name="telefono1" title="Teléfono 1" value="<?php echo $model->telefono1; ?>" placeholder="Teléfono 1"/>
    <input maxlength="30" id="telefono2" name="telefono2" title="Teléfono 2" value="<?php echo $model->telefono2; ?>" placeholder="Teléfono 2"/>
    <input maxlength="30" id="telefono3" name="telefono3" title="Teléfono 3" value="<?php echo $model->telefono3; ?>" placeholder="Teléfono 3"/>
  </fieldset>

  <fieldset id="correo-fieldset" class="ui-widget-content">
    <legend>Correo Electrónico</legend>
    <input maxlength="40" id="correo_electronico1" name="correo_electronico1" value="<?php echo $model->correo_electronico1; ?>" placeholder="Correo Electrónico 1"/>
    <input maxlength="40" id="correo_electronico2" name="correo_electronico2" value="<?php echo $model->correo_electronico2; ?>" placeholder="Correo Electrónico 2"/>
    <input maxlength="40" id="correo_electronico3" name="correo_electronico3" value="<?php echo $model->correo_electronico3; ?>" placeholder="Correo Electrónico 3"/>
  </fieldset>

  <fieldset id="contacto-fieldset" class="ui-widget-content">
    <legend>Contacto</legend>
    <input maxlength="50" id="contacto" name="contacto" value="<?php echo $model->contacto; ?>" placeholder="Contacto"/>
  </fieldset>

  <fieldset id="observaciones-fieldset" class="ui-widget-content">
    <legend>Observaciones</legend>
    <textarea maxlength="255" id="observaciones" name="observaciones" rows="5" cols="120" placeholder="Observaciones" ><?php echo $model->observaciones; ?></textarea>
  </fieldset>

  <div class="botones">
    <input type="submit" name="submit" value="Grabar" id="submit"/>
    <?php if (!$model->getIsNewRecord()): ?>
      <input type="button" name="borrar" value="Borra" id="borra"/>
    <?php endif; ?>
    <input type="button" onclick="return refresh()" name="button" value="Nuevo" id="button"/>
  </div>
</form>
<div id="errores">
</div>
 </div>      
