<style  type="text/css">
    label{float: left; width: 160px;line-height: 2em; margin-right: 14px; text-align: right; padding-top: 4px;}
    #errores{border: 1px #09f solid; color:#09f; background-color: #f2dada; display: none; padding: 10px; margin: 0 auto; width: 500px; text-align: center}
    form{margin: 10px 0px 10px; border: 1px #75C2F8 solid; padding: 19px 20px 11px 27px;  border-radius: 8px; border-shadow: 5px}
    .botones{text-align: right} 
    #proveedor {width: 350px}
    #razon_social, #nombre_fantasia {width: 400px}
    #nro_ingresos_brutos {width: 150px}
    #telefono1,#telefono2,#telefono3 {width: 250px}
    #piso, #departamento {width: 50px}
    #numero {width: 70px}
    #codigo_postal {width: 100px}
    #correo_electronico1, #correo_electronico2, #correo_electronico3 {width: 250px}
    #contacto {width: 400px}
    #codigo{width:60px}
    #codigo-fieldset{width:17%; display: inline}
    #datosfiscales-fieldset{width: 77.4%; display: inline}
    input[type=text] {
        color: #444;
        letter-spacing: 1px;
        word-spacing: 2px; 
        padding: 6px; 
    }   
    .ui-widget { font-size: 12px; }
</style>

<?php $this->breadcrumbs = array(Proveedor::label(2),); ?>

<div id="_form">

    <div id="form-div">
        <?php include "_proveedorForm.php"; ?>
    </div>
</div>
</div>

<script type="text/javascript">

    init();


    function init() {
        $("button, #submit, #borra").button();
        $(".chzn-select").chosen();
        $(".clearinput").clearinput();
        $("#proveedor").focus();

        $("#proveedor").chosen().change(function() {
            ajax($(this).val());
        });

        $('#borra').click(function() {
            if (!confirm("¿Borra este Proveedor?"))
                return;

            $.ajax({
                type: "GET",
                data: {'id': $('#id').val()},
                url: "<?php echo $this->createUrl('proveedor/proveedorBorra'); ?>",
                success: function(data) {
                    refresh();
                }
            });
        });

        $('form').submit(function() {
            $.ajax({
                type: "POST",
                data: $('form').serialize(),
                dataType: 'json',
                url: "<?php echo $this->createUrl('proveedor/proveedorGraba'); ?>",
                success: function(data) {
                    if (data) {
                        $('#errores').html("");
                        for (var i in data)
                        {
                            $('#errores').append(data[i][0] + "<br/>");
                        }
                        $('#errores').show();
                    } else {
                        refresh();
                    }
                }
            });
            return false;
        });

    }
    function refresh() {
        ajax(null);
        return false;
    }
    function ajax(id) {
        $.ajax({
            type: "GET",
            data: {"id": id},
            url: "<?php echo $this->createUrl("proveedor/proveedorForm"); ?>",
            success: function(data) {
                $("#form-div").html(data);
                init();
            },
            error: function(data, status) {
            }
        });
    }
</script>