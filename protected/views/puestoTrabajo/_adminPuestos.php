<form id="form-puesto-nuevo" method="POST" action="#">
     <?php //var_dump(Talonario::Model()->findAll()); die;?>
    <table class="ui-widget ui-widget-content" id="tabla-puestos">
        <tr class="ui-widget-header">
            <th>Nombre</th>
            <th>Destino</th>
            <th>&nbsp</th>
        </tr>
        <?php $row = 0;?>
        
        <?php foreach (PuestoTrabajoPerfil::model()->findAll("puesto_trabajo_id = $puesto_trabajo_id") as $itempuesto): //with(array("anio","anio.nivel"))->?>
            <tr class="tr-puesto <?php echo ($row++ % 2) ? "tr-even" : ""; ?>" item_id="<?php echo $itempuesto->id; ?>">
                <td class="td-talonario"><?php echo Talonario::Model()->findByPk($itempuesto->talonario_id)->nombre;?></td>
                <td class="td-destino"><?php echo Destino::model()->findByPk($itempuesto->destino_id)->nombre;?></td>
                <td class="td-borra" onclick="return borraItemPuesto(<?php echo $itempuesto->id; ?>)"></td>
            </tr>
        <?php endforeach; ?>
            <tr class="tr-puesto <?php echo ($row++ % 2) ? "tr-even" : ""; ?>" id="tr-item-nuevo">
            <td class="td-comprob-nuevo"><select id="comprob-select"  data-placeholder="Comprobantes" class="chzn-select">
            <?php
            $x = array('prompt' => '');
            echo CHtml::listOptions(null, CHtml::listData(Talonario::getTalonario(), 'id', 'nombre_talonario'), $x)
            ?>
            </select></td>
            <td class="td-destino-nuevo"><select id="destino-select" data-placeholder="Destinos" class="chzn-select">
            <?php
            $y = array('prompt' => '');
            echo CHtml::listOptions(null, CHtml::listData(Destino::model()->findAll(), 'id', 'nombre'), $y)
            ?>
            </select></td>
            <td><input type="button" id="graba-item-nuevo-button" value="Graba" onclick="return grabaItem(this)"/></td>
            </tr>
    </table>
</form>