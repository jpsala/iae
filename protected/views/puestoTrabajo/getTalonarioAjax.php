<?php
$selectPT = "
	select * from puesto_trabajo pt where pt.id = $puesto_trabajo_id
	";
$pt = Helpers::qryObj($selectPT);
if (!$pt) {
	$nuevo = true;
	$pt = new PuestoTrabajo();
	$pt->nombre = "";
	$pt->punto_venta_numero = "";
	$pt->nombre_pc = "";
} else {
	$nuevo = false;
}

$select = "
			select t.id, c.nombre, t.letra, t.numero, t.activo, t.impresora, 
					 t.imprimeAuto, t.numeracion_manual, t.comprob_id, p.destino_id
				from puesto_trabajo p
					inner join talonario t on t.punto_venta_numero = p.punto_venta_numero
					inner join comprob c on c.id = t.comprob_id
			where p.id = $puesto_trabajo_id";
$rows = Helpers::qryAllObj($select);
?>

<div id="talanario-div">
	<form name="puesto-trabajo-form" method="POST" class="ui-widget ui-widget-content ui-corner-all">
		<input type="hidden" name="puesto_trabajo_id" value="<?php echo $pt->id; ?>"/>
		<div class="row">
			<input class="largo" name="nombre" value="<?php echo $pt->nombre; ?>" placeholder="Descripción"/>
		</div>
		<div class="row">
			<input class="mediano" name="punto_venta_numero" value="<?php echo $pt->punto_venta_numero; ?>" placeholder="Punto de venta"/>
		</div>
		<div class="row">
			<?php $arr = array("class" => "mediano", "prompt" => "Destino"); ?>
			<?php $destinos = Destino::model()->findAll("tipo=1"); ?>
			<?php $listData = CHtml::listData($destinos, "id", "nombre"); ?>
			<?php echo CHtml::dropDownList("destino_id", $pt->destino_id, $listData, $arr); ?>
		</div>
		<div class="row">
			<input class="largo" name="nombre_pc" value="<?php echo $pt->nombre_pc; ?>" placeholder="Nombre de la PC" />
		</div>
		<div class="botones">
			<button id="graba-button" onclick="return grabaPuestoTrabajo();">Graba</button>
			<button id="borra-button" onclick="return borraPuestoTrabajo(<?php echo $pt->id; ?>);">Borra comprobante!</button>
		</div>
	</form>
</div>
<?php if (!$nuevo): ?>
	<table id="talonarios-table">
		<tr>
			<th class="td-corto">Activo</th>
			<th class="td-largo">Comprob.</th>
			<th class="td-corto">Letra</th>
			<th class="td-largo">Número</th>
			<th class="td-corto">Num. Manual</th>
			<th class="td-largo">Impresora</th>
			<th class="td-corto">Impr. Auto.</th>
			<!--<th></th>-->
		</tr>
		<?php foreach ($rows as $row): ?>
			<?php $this->getTalnoarioTr($row); ?>
		<?php endforeach; ?>
	</table>
	<?php comprobantesSelect($puesto_trabajo_id); ?>
<?php endif; ?>

<?php

function comprobantesSelect($puestro_trabajo_id) {
	$select = "select comprob.id, comprob.nombre from comprob where comprob.id not in(
		select t.comprob_id
			from puesto_trabajo p
				inner join talonario t on t.punto_venta_numero = p.punto_venta_numero
				inner join comprob c on c.id = t.comprob_id
			where p.id = $puestro_trabajo_id)";
	$rows = Helpers::qryAllObj($select);
	if (!$rows) {
		echo "Todos los comprobantes fueron asignados...";
		return;
	}
	?>

	<select id="comprobante_id" data-placeholder="Seleccione un comprobante">
		<option value=""></option>
		<?php foreach ($rows as $row): ?>
			<option value="<?php echo $row->id; ?>"><?php echo $row->nombre; ?></option>
		<?php endforeach; ?>
	</select>
	<?php
}
?>
<script>
	$(function() {
		$("#graba-button, #borra-button").button();
		$("#destino_id").chosen();
		$("table").on("change", "input", function(o) {
			var talonario_id = $(this).closest("tr").attr("id");
			var campo = $(this).attr("name");
			var type = $(this).attr("type");
			var val;
			if (type === "checkbox") {
				val = $(this).is(":checked") ? 1 : 0;
			} else {
				val = $(this).val();
			}
			$.ajax({
				type: "GET",
				//      dataType:"json",
				data: {talonario_id: talonario_id, campo: campo, val: val},
				url: "<?php echo $this->createUrl("puestoTrabajo/modifica"); ?>",
				success: function(data) {
					if (data !== "ok") {
						alert(data);
					}
				},
				error: function(data, status) {
				}
			}
			);
		});
	});
	$("#comprobante_id").chosen().change(function() {
		agregaComprobante($(this).val());
	});

	function agregaComprobante(comprobante_id) {
		var puesto_trabajo_id = $("#puesto_trabajo_id").val();
		$.ajax({
			type: "GET",
//			dataType: "json",
			data: {comprobante_id: comprobante_id, puesto_trabajo_id: puesto_trabajo_id},
			url: "<?php echo $this->createUrl("puestoTrabajo/agregaComprobante"); ?>",
			success: function(data) {
				$("table tbody").append(data);
			},
			error: function(data, status) {
			}
		}
		);
	}

	function grabaPuestoTrabajo() {
		$.ajax({
			type: "POST",
			dataType: "json",
			data: $("form").serialize(),
			url: "<?php echo $this->createUrl("puestoTrabajo/graba"); ?>",
			success: function(data) {
				if (data.state === "error") {
					muestraErrores(data.error);
				} else {
					window.location = window.location;
				}
			},
			error: function(data, status) {
			}
		}
		);
		return false;
	}

	function borraPuestoTrabajo(puesto_trabajo_id) {
		$.ajax({
			type: "GET",
			dataType: "json",
			data: {puesto_trabajo_id: puesto_trabajo_id},
			url: "<?php echo $this->createUrl("puestoTrabajo/borra"); ?>",
			success: function(data) {
				if (data.state == "error") {
					muestraErrores(data.error);
				} else {
					window.location = window.location;
				}
			},
			error: function(data, status) {
			}
		});
		return false;
	}

	function borraTalonario(talonario_id) {
		$.ajax({
			type: "GET",
			dataType:"json",
			data: {talonario_id:talonario_id},
			url: "<?php echo $this->createUrl("puestoTrabajo/borraTalonario"); ?>",
			success: function(data) {
				if (data.state == "error") {
					muestraErrores(data.error);
				} else {
					$("#"+talonario_id).remove();
				}
			},
			error: function(data, status) {
			}
		}
		);
	}

</script>