<style>
	#puesto_trabajo_id{width:220px}
	#comprobante_id{width:220px}
	table .td-corto{width:20px}
	table .td-largo{width:180px}
	table .td-medio{width:110px}
	.hidden{display:none}
	.largo{width:200px}
	.mediano{width:120px}
	.corto{width:50px}
	.row {display: inline-block;vertical-align: top; margin-top:5px }
	.chzn-container {margin-top: -1px; vertical-align: middle}
	form{margin: 10px 0 10px 0; padding: 5px}
	.botones{margin: 10px 0 0 0; text-align: right}
</style>
<?php
$puesto_trabajo_id = 1;
$select = "select * from puesto_trabajo";
$rows = Helpers::qryAllObj($select);
?>

<select id="puesto_trabajo_id" data-placeholder="Seleccione un Puesto de Trabajo">
	<option value=""></option>
	<?php foreach ($rows as $row): ?>
		<option value="<?php echo $row->id; ?>"><?php echo $row->nombre; ?></option>
	<?php endforeach; ?>
	<option value="-1"> - Nuevo -</option>
</select>
<div id="talonarios-div"></div>

<script>
	$(function() {
		$("#puesto_trabajo_id_chzn").mousedown();
	});

	$("#puesto_trabajo_id").chosen().change(function() {
		puestoTrabajoChange();
	});

	function puestoTrabajoChange() {
		var puesto_trabajo_id = $("#puesto_trabajo_id").val();
		$.ajax({
			type: "GET",
			//      dataType:"json",
			data: {puesto_trabajo_id: puesto_trabajo_id},
			url: "<?php echo $this->createUrl("puestoTrabajo/getTalonariosAjax"); ?>",
			success: function(data) {
				$("#talonarios-div").html(data);
			},
			error: function(data, status) {
			}
		}
		);
	}
</script>

