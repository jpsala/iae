<script type="text/javascript">
    

    
                
    var $tr_item=$('<tr item_id="0000000013" class="tr-puesto tr-even"> ' +
                ' <td class="td-talonario">Factura venta talonario 1</td> ' +
                ' <td class="td-destino">Caja JP</td> ' +
                ' <td onclick="return borraItemPuesto(0000000013)" class="td-borra"></td> ' +
                ' </tr>');  
            
    var $perfil_id;
    var $puesto_trabajo_id;
    var $destino_id;
    var $talonario_id;
    var item_id;

   $(function(){
        init();
    });
    
    function init(){
        
        $("button").button();
        
        $("#puesto-select").chosen().change(function(){
            changePuesto($(this));
        });


       
        $(".clearinput").clearinput();
        
        
    }
    
   
    function changePuesto($this){
        $perfil_id = $this.val();
        traeItems($this);

        return false;
    }
    function changeDestino($this){
        $destino_id = $this.val();
        return false;
    }

    function changeTalonario($this){
        $talonario_id = $this.val();
        return false;
    }
    
    function traeItems($this){
        $puesto_trabajo_id = $this.val();
        $.ajax({
            type: "GET",
            //dataType: "json",
            data: {puesto_trabajo_id:$puesto_trabajo_id},
            url: "<?php echo $this->createUrl("PuestoTrabajo/adminItems"); ?>",
            success: function(data) {
                $("#puestos").html(data);
                $("#graba-item-nuevo-button").button();
                $(".tr-puesto input:not(:hidden)").first().focus();
                $(".tr-puesto:not('#tr-puesto-nuevo')").find("td:not('.td-borra')").click(function(){clickItem($(this).parent())});
                $("#comprob-select").chosen().change(function(){
                    changeTalonario($(this));
                });
                $("#destino-select").chosen().change(function(){
                     changeDestino($(this));
                });
            },
            error: function(data,status){
                 
            }
        });
    }

   
    
    
    
    function pedidoDeDatosVisible(visible){
        $("#pedido-de-datos").animate({"opacity": visible ? "show":"hide"},"slow");    
    }
    
    
    function clickItem($this){
        //console.log('click');
        
        $rec = $("#tr-comprob-nuevo");
        $rec.find("#talonario-item").val($this.find(".td-talonario").html());
        $rec.find("#destino-item").val($this.find(".td-destino").html());
        $rec.attr("puesto_trabajo_id",$this.attr("puesto_trabajo_id"));
        $rec.attr("item_id",$this.attr("item_id"));
        $rec.find("input:not(:hidden)").first().focus();
    }
    
    function grabaItem(input){
        var $tr,$rec;
        $rec = $(input).closest("tr");
        $item_id = $rec.attr("item_id");
        var data = "destino_id=" + $destino_id + "&" + "talonario_id=" + $talonario_id + "&" + "puesto_trabajo_id=" + $puesto_trabajo_id + "&" + $rec.find("*").serialize();
        $.ajax({
            type: "POST",
            dataType: "json",
            data: data,
            url: "<?php echo $this->createUrl("puestoTrabajo/AdminItemPerfilGraba"); ?>",
            success: function(data) {
                if(data.error){
                    alert(data.error);
                }else{
                    $tr = $tr_item;
                    $tr.attr("item_id",data.id);
                    $tr.find(".td-talonario").html($rec.find("#comprob-select_chzn").find("span").html());
                    $tr.find(".td-destino").html($rec.find("#destino-select_chzn").find("span").html());
                    $tr.find(".td-borra").attr("onclick","return borraItemPuesto("+data.id+")");
                    if(data.nuevo){
                        $rec.before($tr);
                        $tr.find("td:not('.td-borra')").click(function(){clickItem($(this).parent())});
                    }
                    $rec.find("#comprob-select").first().focus();
                }
            },
            error: function(data,status){
              
            }
        });

    }
    
    function cancela(){
        pedidoDeDatosVisible(true);
        limpiaTodo();
        return false;
    }
    
       
    function borraItemPuesto(id){
        $.ajax({
            type: "GET",
            data: {id:id},
            url: "<?php echo $this->createUrl("puestotrabajo/AdminItemPerfilBorra"); ?>",
             
            success: function(data) {
                $('[item_id="%id%"]'.replace("%id%",id)).remove();
            },
            error: function(data,status){
                alert('error');
            }
        });

        return false;
    }    
    
    function limpiaTodo(){
        
    }
    
</script>
