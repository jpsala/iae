<style type="text/css">
  #reportes-div{overflow: auto; padding: 5px}
  #reportes-table{width: 23%; border : #999 1px solid; float: left;}
  #reportes-table table{padding-top: 3px}
  #reportes-table-header{border-bottom: #999 1px solid}
  #reportes-table tbody tr{cursor: pointer}
  #reportes-table tbody tr:hover{background-color: lightsteelblue}
  #reportes-crud{width: 74%; border : #999 1px solid; min-height: 400px; float: left; margin-left: 5px}
</style>
<div id="reportes-div" class="ui-widget ui-widget-content ui-corner-all">
  <div id="reportes-table" class="ui-corner-all">
    <table>
      <tr class="ui-widget-header">
        <th id="reportes-table-header">Reporte</th>
      </tr>
      <?php foreach ($reportes as $reporte): ?>
        <tr>
          <td onclick="reporteSelec(<?php echo $reporte->id; ?>)"><?php echo $reporte->nombre; ?></td>
        </tr>
      <?php endforeach; ?>
    </table>
  </div>
  <div id="reportes-crud" class="ui-corner-all">

  </div>
</div>
<script type="text/javascript">
  function reporteSelec(reporte_id){
    $.ajax({
      type: "GET",
      //      dataType:"json",
      data: {
        reporte_id:reporte_id
      },
      url: "<?php echo $this->createUrl("/reporte/_adminForm"); ?>",
      success: function(data) {
        $("#reportes-crud").html(data);
      },
      error: function(data,status){
      }
    });
  }
</script>