<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<head>
  <html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en"></html>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <style type="text/css">
    #header { border-radius: 5px;position: fixed; left: 0px; top: -10px; right: 0px; height: 50px; background-color:  lightblue; text-align: center; }
    #footer { position: fixed; left: 0px; bottom: -120px; right: 0px; height: 150px; background-color: lightblue; }
    #footer .page:after {content: counter(page, upper-roman); }
    #footer .page{ margin-left: 10px}
    table{margin-top: 60px;background-color: 
            #C3D9FF;
          border: 
            #0066A4 solid 1px;
          border-radius: 4px;
          padding: 2px;
          border-collapse: separate;
          border-spacing: 0;
          white-space: normal;
          line-height: normal;
          font-weight: normal;
          font-variant: normal;
          font-style: normal;
          text-align: -webkit-auto;
          width: 100%
    }
    table tr{color: #222;background-color:lightyellow ;border-bottom: #000 1px solid}
    td, th{text-align:left}
    th{background-color: #C3D9FF;
       color: #222;
    }
    .odd{background-color: #EEE}
  </style>
</head>

<body>
  <div id="header">
    <p>Título</p>
  </div>
  <div id="footer">
    <p class="page">Page </p>
  </div>  
  <?php
  $rows = $cmd->queryAll();
  if (count($rows) < 1) {
    echo "No hay datos para mostrar";
    return;
  }
  $row = $rows[0];
  $fields = array();
  /* @var $tr CDbTransaction */
  $tr = Yii::app()->db->beginTransaction();
  foreach ($row as $field => $value) {
    $rc = ReporteCampo::model()->find("reporte_id=$reporte_id and nombre = \"$field\"");
    if (!$rc) {
      $rc = new ReporteCampo();
      $rc->reporte_id = $reporte_id;
      $rc->nombre = $field;
      $rc->visible = 1;
      $rc->save();
    }
    $fld = null;
    $fld->nombre = $field;
    $fld->visible = $rc->visible;
    $fld->style = $rc->style;
    $fields[$field] = $fld;
  }
  $tr->commit();

  echo "<table>";

  echo "<tr>";
  foreach ($row as $field => $value) {
    if ($fields[$field]->visible) {
      $style = $fields[$field]->style;
      echo "<th style=\"$style\">$field</th>";
    }
  }
  echo "</tr>";

  $i = 0;
  foreach ($rows as $row) {
    $class = ($i++ & 1) ? "odd" : "";
    echo "<tr class=\"$class\">";
    foreach ($row as $field => $value) {
      if ($fields[$field]->visible) {
        $style = $fields[$field]->style;
        echo "<td  style=\"$style\">$value</td>";
      }
    }
    echo "</tr>";
  }
  ?>
</body>
