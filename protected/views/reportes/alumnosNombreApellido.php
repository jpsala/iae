<?php
$paramNivel = new ParamNivel(array(
    "controller" => $this,
//   "onchange" => "changeNivel",
        ));
$paramAnio = new ParamAnio(array(
    "controller" => $this,
//   "onchange" => "changeAnio",
        ));
$paramDivision = new ParamDivision(array(
    "controller" => $this,
//    "onchange" => "changeDivision",
        ));

$paramNivel->render();
$paramAnio->render();
$paramDivision->render();
?>
<select id="sexo-select" data-placeholder="Sexo">
  <option value="-1"></option>
  <option value="">Ambos</option>
  <option value="F">Femenino</option>
  <option value="M">Masculino</option>
</select>
<select id="output-select" data-placeholder="Destino" style="width: 95px">
  <option value="-1"></option>
  <option value="PDF">PDF</option>
  <option value="EXCEL">Excel</option>
</select>

<script type="text/javascript">
  var sexo, output;
  $("#output-select").chosen().change(function() {
    output = $("#output-select").val();
    changeOutput();
  });
  $("#sexo-select").chosen().change(function() {
    changeSexo($(this));
  });

  function changeSexo() {
    sexo = $("#sexo-select").val();
  }

  function changeOutput() {
    window.location = "<?php echo $this->createUrl("reportes/alumnosNombreApellido"); ?>&division_id=" + division_id + "&sexo=" + sexo + "&output=" + output;
  }

</script>