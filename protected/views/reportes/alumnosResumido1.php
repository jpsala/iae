<?php
$paramNivel = new ParamNivel(array(
    "controller" => $this,
//   "onchange" => "changeNivel",
        ));
$paramAnio = new ParamAnio(array(
    "controller" => $this,
//   "onchange" => "changeAnio",
        ));
$paramDivision = new ParamDivision(array(
    "controller" => $this,
//    "onchange" => "changeDivision",
        ));

$paramNivel->render();
$paramAnio->render();
$paramDivision->render();
?>
<select id="sexo-select" data-placeholder="Sexo">
  <option value="-1"></option>
  <option value="">Ambos</option>
  <option value="F">Femenino</option>
  <option value="M">Masculino</option>
</select>
<script type="text/javascript">
  $("#sexo-select").chosen().change(function() {
    changeSexo($(this));
  });

  function changeSexo() {
    var sexo = $("#sexo-select").val();
    window.location = "<?php echo $this->createUrl("reportes/alumnosResumido1"); ?>&division_id=" + division_id + "&sexo=" + sexo;
  }

</script>