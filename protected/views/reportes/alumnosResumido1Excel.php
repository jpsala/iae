<?php

	$fileName = "Alumnos_Resumido";
	$select = "
		select concat(a.apellido, \", \", a.nombre) as nombre_alumno, a.numero_documento as dni_alumno, 
					pa.nombre as nacionalidad_alumno, 
					a.fecha_nacimiento as fecha_nacimiento_alumno
			from alumno a
				inner join alumno_estado ae on ae.id = a.estado_id and ae.activo_edu = 1
				inner join alumno_division ad on ad.Alumno_id = a.id
				inner join division d on d.id = ad.Division_id and ad.activo = 1
				left join localidad l on l.id = a.localidad_id
				left join pais pa on pa.id = l.Pais_id
			where a.activo = 1 and d.id = $division_id $sexoAnd
			order by a.sexo desc, a.apellido, a.nombre";

	$rows = Helpers::qryAll($select);


	$cols = array(
			"nombre_alumno" => array("title" => "Alumno"),
			"dni_alumno" => array("title" => "DNI", "format" => "text"),
			"nacionalidad_alumno" => array("title" => "Nacionalidad"),
			"fecha_nacimiento_alumno" => array("title" => "Fec.Nac.", "format" => "date"),
	);

	Helpers::excel($rows, $cols, $fileName, true);


	