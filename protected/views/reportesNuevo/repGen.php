<?php

	if (count($data) == 0) {
		return false;
	}

	$pdf = isset($opt["ouput"]["pdf"]) ? $opt["ouput"]["pdf"] : true;
	$html = isset($opt["ouput"]["pdf"]) ? $opt["ouput"]["html"] : true;
	$excel = isset($opt["ouput"]["pdf"]) ? $opt["ouput"]["excel"] : true;

	$colModel = isset($colModel) ? $colModel : $this->createColModel($data);

	if (in_array($output, array("html", "pdf"))) {
		$tableStyle = $this->renderPartial("repGenTableStyle"
				, array("data" => $data, "colModel" => $colModel), true
		);
		$tableHeader = $this->renderPartial("repGenTableHeader"
				, array("data" => $data, "colModel" => $colModel), true
		);
		$tableData = $this->renderPartial("repGenTableData"
				, array("data" => $data, "colModel" => $colModel), true
		);
		$tableTagBegin = '<table class="table table-striped table-bordered table-condensed">';
		$tableTagEnd = '</table>';
		if ($output == "html") {
			echo $tableStyle . $tableTagBegin . $tableHeader . $tableData . $tableTagEnd;
		} elseif ($output == "pdf") {
			$this->pdf($colModel, $tableStyle . $tableTagBegin . $tableData . $tableTagEnd, $tableStyle . $tableTagBegin . $tableHeader . $tableTagEnd);
		}
	} else {
		$this->excel($data, $colModel, $output);
	}


