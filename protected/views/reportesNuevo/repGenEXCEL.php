<?php

	ini_set('memory_limit', '-1');
	$fields = array_keys($colModel);
	$rows = $data;
	$hiddens = array("familia_id", "vive_con_id");
	$FIRST_CELL = "A1";

	$objPHPExcel = new MyPHPExcel();
	$as = $objPHPExcel->setActiveSheetIndex(0);

	$rowNum = 1;
	Col::init();
	foreach ($fields as $fld) {
		$fldData = isset($colModel[$fld]) ? $colModel[$fld] : array();
		$visible = !isset($fldData["visible"]) or ( isset($fldData["visible"]) and $fldData["visible"]);
		if ($visible) {
			$col = Col::next();
			$as->SetCellValue($col . $rowNum, $fld);
		}
	}
	$rowNum++;
	foreach ($rows as $row) {
		Col::init();
		foreach ($row as $key => $fld) {
			$fldData = isset($colModel[$key]) ? $colModel[$key] : array();
			$visible = !isset($fldData["visible"]) or ( isset($fldData["visible"]) and $fldData["visible"]);
			if ($visible) {
				$col = Col::next();
				$as->SetCellValue($col . $rowNum, $fld);
			}
		}
		$rowNum++;
	}
	Col::init();
	foreach ($fields as $fld) {
		$fldData = isset($colModel[$fld]) ? $colModel[$fld] : array();
		$visible = !isset($fldData["visible"]) or ( isset($fldData["visible"]) and $fldData["visible"]);
		if ($visible) {
			$col = Col::next();
			$as->getColumnDimension($col)->setAutoSize(true);
		}
	}
	$as->setAutoFilter("A1:$col" . "1");
	$as->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$as->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_LEGAL);
	$as->getPageSetup()->setFitToPage(true);
	$as->getPageSetup()->setFitToWidth(1);
	$as->getPageSetup()->setFitToHeight(0);

	$objPHPExcel->output($filename);
	