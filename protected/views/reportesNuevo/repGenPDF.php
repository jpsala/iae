<?php

	require_once('tcpdf/config/lang/spa.php');
	require_once('tcpdf/tcpdf.php');

	class RepGenPDF extends TCPDF {

		public $headerHeight, $colModel, $header;

		public function Header() {
			$margins = $this->getMargins();
			$marginsH = $margins["left"] + $margins["right"];
			$imgHeader = "http://" . $_SERVER["SERVER_NAME"] . '/' . Yii::app()->baseUrl . "/images/logo1BN.png";
			$imgHeaderWidth = 22;
			$this->setXY(0, 2);
			$this->Image($imgHeader, $this->getX(), $this->getY(), $imgHeaderWidth);
			$this->writeHTMLCell($this->getPageWidth(), 1, 0, 10, "", "B");
			$this->setXY(5, 17);
			$this->writeHTML($this->header);
			$this->writeHTMLCell($this->getPageWidth(), 1, 0, 17, "", "B");
		}

		public function Footer() {
			
		}

		public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $thisa = false) {
			parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $thisa);
			$this->print_header = true;
			$this->print_footer = false;
//		$this->SetFont("sourcecodepro");
		}

		public function MultiCell($w, $h, $txt, $border = 0, $align = 'J', $fill = false, $ln = 1, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = true, $maxh = 0, $valign = 'T', $fitcell = false) {
			$w = $w ? $w : $this->GetStringWidth($txt) + .00001;
			parent::MultiCell($w, $h, $txt, $border, $align, $fill, $ln, $x, $y, $reseth, $stretch, $ishtml, $autopadding, $maxh, $valign, $fitcell);
		}

		public function p($w, $txt) {
			$w = $w ? $w : $this->GetStringWidth($txt) + .00001;
			$x = $this->GetX();
			$y = $this->GetY();
			$this->MultiCell($w, 10, "", "LBR", "C", 0, 2, $x, $y, false, false, false, true, 10, "", false);
			$this->MultiCell($w, 10, $txt, "", "L", 0, 0, $x, $y, false, false, false, true, null, "", false);
			//$this->MultiCell($w, $h, $txt, $border, $align, $fill, $ln, $x, $y, $reseth, $stretch, $ishtml, $autopadding, $maxh, $valign)
		}

		public function chkPageEnd($height) {
			$m = $this->getMargins();
			if (($this->getY() + $height + $m["top"] + $m["bottom"]) >= $this->getPageHeight()) {
				return true;
			}
		}

		public function getMargins() {
			$m = parent::getMargins();
			$m["bottom"] = 6;
			return $m;
		}

	}

	$pdf = new RepGenPDF("L");
	$pdf->SetFontSize(11);
	$pdf->SetMargins(0, 0, 0, true);
	$pdf->SetCellPaddings(.5, 1, .5, 1);
	$pdf->SetAutoPageBreak(TRUE, 0);
	$pdf->colModel = $colModel;
	$pdf->header = $header;
	$pdf->SetMargins(4, 24, 5, true);
	$pdf->AddPage();
//$pdf->setY(29);
	$margins = $pdf->getMargins();
	$pdf->writeHTML($html);
	$pdf->output();
	