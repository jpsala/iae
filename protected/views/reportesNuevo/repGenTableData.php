<?php
	$fields = array_keys($colModel);
?>
<?php foreach ($data as $row): ?>
		<tr>
			<?php
			foreach ($fields as $fld):
				$fldData = isset($colModel[$fld]) ? $colModel[$fld] : array();
				$visible = !isset($fldData["visible"]) or ( isset($fldData["visible"]) and $fldData["visible"]);
				?>
				<?php if ($visible): ?>
					<td class="<?php echo $fld; ?>"> <?php echo $row[$fld]; ?> </td>
				<?php endif; ?>
			<?php endforeach; ?>
		</tr>
	<?php endforeach; 
