<?php
	$fields = array_keys($colModel);
?>
<tr>
	<?php
		foreach ($fields as $fld):
			$fldData = isset($colModel[$fld]) ? $colModel[$fld] : array();
			$visible = !isset($fldData["visible"]) or ( isset($fldData["visible"]) and $fldData["visible"]);
			$title = isset($fldData["title"]) ? $fldData["title"] : $fld;
			?>
			<?php if ($visible): ?>
				<th class="<?php echo $fld; ?>"><?php echo $title; ?></th>
			<?php endif; ?>
		<?php endforeach; ?>
</tr>
