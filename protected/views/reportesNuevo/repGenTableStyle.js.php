<?php
	$fields = array_keys($colModel);
?>
<style>
	th{font-weight: bold}
<?php
	foreach ($fields as $fld) {
		$fldData = isset($colModel[$fld]) ? $colModel[$fld] : array();
		if (isset($fldData["css"])) {
			echo "." . $fld . "{" . $fldData["css"] . "}";
		}
	}
?>
</style>
