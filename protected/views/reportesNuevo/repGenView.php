<link rel="stylesheet" href="js/select2/select2.css"/>
<link rel="stylesheet" href="js/select2/select2-bootstrap.css"/>
<script  src="js/select2/select2.min.js"></script>	
<script  src="js/select2/select2_locale_es.js"></script>
<?php include("repGenViewModel.js.php"); ?>
<div class="container top5" data-bind="visible:listo()">
	<form class="form-inline col-xs-12">
		<h2><?php echo $title; ?></h2>
		<div class="form-group top10" data-bind="foreach: params">
			<span data-bind="text: params().length"></span>
			<select  autofocus="" class="" style="width:150px" 
							 data-bind="
								value: value,
								valueUpdate: 'input',
								options: options, 
								optionsCaption: optionsCaption,
								optionsText: optionsText,
								optionsValue: optionsValue">
			</select>
		</div>
		<div class="form_foot pull-right">
			<!--			<button id="pdf-button" class="btn btn-default" data-bind="enable:pdf && division_id(), click:function(){self.output('pdf');}">PDF</button>
						<button id="html-button" class="btn btn-default" data-bind="enable:html && division_id(), click:function(){self.output('html');}">HTML</button>
						<button id="excel-button" class="btn btn-default" data-bind="value:output,enable:excel && division_id(), click:function(){self.output('excel');}">EXCEL</button>-->
		</div>
	</form>
</div>
<div id="ajax-div" class="container top30">

</div>

<script  type="text/javascript">
	var init = rgvm.init(<?php echo json_encode($params); ?>).then(function(m) {
		m =new Date();
		m = m.getMilliseconds();
		ko.applyBindings();
		$("select").select2();
		m2 =new Date();
		m2 = m2.getMilliseconds();
		console.log(m2-m);
	});
</script>

