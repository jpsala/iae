<script  type="text/javascript">
	var rgvm = (function() {

		self = this;
		self.params = ko.observableArray([]);
		
		self.listo = ko.observable(false);
		
		self.init = function(params) {
			var deferred = $.Deferred();
			createParams(params);
			getAllData().done(function() {
				self.listo(true);
				deferred.resolve("ok");
			});
			return deferred.promise();
		};
		
		function createParams(data) {
			var parent_id, param, params = [];
			$.each(data, function(i, o) {
				switch (i) {
					case  "nivel_id":
						param = {
							nombre: "nivel_id",
							value: ko.observable(o.value),
							optionsCaption: "nivel...",
							url: "<?php echo $this->createUrl("reportesNuevo/getNiveles"); ?>",
							optionsText: o.optionText || 'nombre',
							optionsValue: o.optionsValue || 'id',
							options: ko.observableArray(o.options || []),
							dirty: ko.observable(false)
						};
						parent_id = o.value;
						break;
					case  "anio_id":
						param = {
							nombre: "anio_id",
							value: ko.observable(o.value),
							optionsCaption: "Año...",
							url: "<?php echo $this->createUrl("reportesNuevo/getAnios"); ?>",
							data: {parent_id: parent_id},
							optionsText: o.optionText || 'nombre',
							optionsValue: o.optionsValue || 'id',
							options: ko.observableArray(o.options || []),
							parent: "nivel_id",
							dirty: ko.observable(false)
						};
						parent_id = null;
						break;
					case  "division_id":
						param = {
							nombre: "division_id",
							value: ko.observable(o.value),
							optionsCaption: "Division...",
							url: "<?php echo $this->createUrl("reportesNuevo/getDivisiones"); ?>",
							data: {parent_id: parent_id},
							optionsText: o.optionText || 'nombre',
							optionsValue: o.optionsValue || 'id',
							options: ko.observableArray(o.options || []),
							parent: "anio_id",
							dirty: ko.observable(false)
						};
						parent_id = null;
						break;
				}
				param = ko.observable(param);
				self.params.push(param);
				//console.log(param);
				var p = self.params.indexOf(param);
				param().value.subscribe(function(a) {
					valorChange(a, p, false);
				});
				param().dirty.subscribe(function(a) {
					valorChange(a, p, true);
				});
			});
		}

		function getAllData() {
			var requests = new Array();
			var deferred = $.Deferred();
			$.each(self.params(), function(i, p) {
				if (p().value()) {
					requests.push(getOptions(p));
				}
			});
			$.when.apply($, requests).done(function() {
				$.each(arguments, function(i, data) {
				});
				deferred.resolve("ok");
			});
			return deferred.promise();
		}

		function valorChange(id, paramIndex, dirty) {
///			console.log(id,self.params()[indexOf(index)], index);
			var param = self.params()[paramIndex];
			var dependants = getDependants(param().nombre);
		$.each(dependants, function(i, o) {
				getOptions(o);
				return;
//				console.log("dep", o().nombre);
				o().data[o().parent] = id;
//				o().dirty(true);
				return;
				var data = {parent_id: id};
				$.getJSON(targetParam().url, data, function(data) {
					targetParam().options.removeAll();
					$.each(data, function(i, o) {
	//					console.log(i, o);
						targetParam().options.push(o);
					});
				});
			});

		}

		function getDependants(parent) {
			var dependants = [];
			$.each(self.params(), function(i, o) {
				if (o().parent && o().parent === parent) {
					dependants.push(o);
				}
			});
			return dependants;
		}

		function getParam(nombre){
			var param;
			$.each(self.params(), function(i, o) {
				if (o().nombre === nombre) {
					param = o;
				}
			});
			return param;
		}

		function getOptions(param) {
			var parentParam = param().parent ? getParam(param().parent) : undefined;
			var data = parentParam ? {parent_id: parentParam().value()} : undefined;
			return $.getJSON(param().url, data, function(data) {
				param().options.removeAll();
				$.each(data, function(i, o) {
					param().options.push(o);
				});
			});
		}

		return {
			init: init
		};
		
	})();
</script>