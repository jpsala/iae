<style type="text/css">
  #cp{padding: 10px}
  #cp label{float: left; width: 190px; text-align: right ; margin-right: 5px}
  #submit{margin-top: 5px}
  #errores{padding: 10px; margin: 10px}
  #msg{padding:10px; margin: 10px}
  .error{background-color: lightcoral; background-image: none}
</style>
<div id="cp" class="ui-widget ui-widget-content ui-corner-all">
  <?php if ($error): ?>
    <div id="errores" class="error ui-widget ui-widget-content error ui-corner-all">
      <?php echo $error; ?>
    </div>
  <?php endif; ?>
  <div id="msg" class="ui-widget ui-widget-content ui-state-highlight ui-corner-all">
    El password es el mismo que el login...
  </div>
  <form method="post" name="form" action="">
    <div class="row">
      <label>Password Actual:</label><input type="password" name="password-actual"/>
    </div>
    <div class="row">
      <label>Password Nuevo:</label><input type="password" name="password"/>
    </div>
    <div class="row">
      <label>Password Nuevo verificación:</label><input type="password" name="password-verificacion"/>
    </div>
    <input type="submit" name="submit" id="submit" value="Cambia password"/>
  </form>
</div>