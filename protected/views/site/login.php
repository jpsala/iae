<?php
$this->pageTitle.= ' Ingreso ';
?>

<div class="form">
  <?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'login-form',
      'enableClientValidation' => true,
      'clientOptions' => array(
          'validateOnSubmit' => true,
      ),
  ));
  ?>

  <div class="row">
    <?php echo $form->labelEx($model, 'Usuario'); ?>
    <?php echo $form->textField($model, 'username'); ?>
<?php echo $form->error($model, 'username'); ?>
  </div>

  <div class="row">
    <label for="password">Contraseña:</label>
    <?php echo $form->passwordField($model, 'password'); ?>
<?php echo $form->error($model, 'password'); ?>
  </div>

  <div class="row rememberMe">
    <?php echo $form->checkBox($model, 'rememberMe'); ?>
    <label for="rememberMe">Recuérdeme en esta PC</label>
<?php echo $form->error($model, 'rememberMe'); ?>
  </div>

  <div class="row buttons">
<?php echo CHtml::submitButton('Login'); ?>
  </div>

<?php $this->endWidget(); ?>
</div><!-- form -->
<script type="text/javascript">
  $("#LoginForm_username").focus();
</script>