<?php
$sociosSelect = "
  select 
      s.id, s.tipo,
      case when a.id then a.matricula when p.id then p.codigo end as codigo,
      case when a.id 
        then concat(a.apellido, ', ', a.nombre)
        when s.id then p.nombre_fantasia
      end as nombre,
      saldo(s.id, '2020/01/01') as saldo
    from socio s 
      left join alumno a on a.id = s.Alumno_id
      left join proveedor p on p.id = s.Proveedor_id
    order by upper(s.tipo)
";
$socios = Helpers::qryAllObj($sociosSelect);
foreach ($socios as $key=>$socio){
  $saldoCalcSelect = "
    SELECT c.nombre, d.numero, SUM(d.saldo*c.signo_cc) AS sumaSaldo,  saldo(d.Socio_id, NULL) AS saldo
    FROM doc d
      INNER JOIN talonario t ON d.talonario_id = t.id
      INNER JOIN comprob c ON t.comprob_id = c.id
    WHERE d.activo AND !d.anulado AND d.Socio_id = $socio->id AND d.saldo != 0
  ";
  $saldoCalc = Helpers::qryObj($saldoCalcSelect);
  $socio->calc = $saldoCalc;
}
?>
<table>
  <tr>
    <th>Tipo</th>
    <th>ID</th>
    <th>Código</th>
    <th>Alumno</th>
    <th>Saldo()</th>
    <th>SaldoCalc</th>
  </tr>
  <?php foreach($socios as $socio): ?>
   <?php if($socio->saldo*1 != $socio->calc->sumaSaldo*1):?>
      <tr>
        <td><?php echo $socio->tipo; ?></td>
        <td><?php echo $socio->id; ?></td>
        <td><?php echo $socio->codigo; ?></td>
        <td><?php echo $socio->nombre; ?></td>
        <td><?php echo $socio->saldo;?></td>
        <td><?php echo $socio->calc->sumaSaldo;?></td>
      </tr>
    <?php endif;?>
  <?php endforeach; ; ?>
</table>
