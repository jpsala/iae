<style>
    .num{width:50px;text-align: right}
    .comprob{width:160px}
    .fecha{width:70px}
    .appl-tr{background-color: lemonchiffon !important}
    /*.indent{width:1px}*/
</style>
<table>
    <tr>
        <th></th>
    </tr>
    <?php foreach ($rows as $socio_nombre => $row): ?>
    <?php if($row->ok) continue;?>
        <tr>
            <th colspan="2"><?php echo "$socio_nombre ($row->codigo/$row->id)"; ?></th>
            <th colspan="3"><?php echo $row->ok ? "Ok" : "$row->saldo_calc \ Bien $row->saldo_bien \ Saldos $row->suma_saldos"; ?></th>
        </tr>
        <?php $saldo_prov = 0; ?>
        <?php $saldo_guard_prov = 0; ?>
        <?php $saldo_calc_prov = 0; ?>
        <tr>
            <th>Fecha</th>
            <th>Comprob</th>
            <th></th>
            <th>Total doc</th>
            <th>Saldo guard.</th>
            <th>Saldo calc</th>
            <th>Saldo Socio</th>
        </tr>
        <?php foreach ($row->docs as $doc): ?>
            <?php $saldo_prov+=$doc->total; ?>
            <?php $saldo_guard_prov+=$doc->saldo; ?>
            <?php $saldo_calc_prov+=$doc->saldo_calc; ?>
            <tr>
                <td class="fecha"><?php echo date("d/m/Y", strtotime($doc->fecha)); ?></th>
                <td class="comprob"><?php echo $doc->comprob . " - " . str_pad($doc->numero, 6, "0", STR_PAD_LEFT); ?></th>
                <td colspan="1"><?php echo $doc->ok ? "Ok" : "Mal"; ?></td>
                <td class="num"><?php echo number_format($doc->total, 2); ?></td>
                <td class="num"><?php echo number_format($doc->saldo, 2); ?></td>
                <td class="num"><?php echo number_format($doc->saldo_calc, 2); ?></td>
                <td class="num"><?php echo number_format($saldo_prov, 2); ?></td>
            </tr>
            <?php $saldo_doc = 0; ?>
            <?php foreach ($doc->appls as $appl): ?>
                <?php $saldo_doc -= $appl->importe_aplicacion; ?>
                <?php $saldo_prov-=$appl->importe_aplicacion; ?>
                <tr class="appl-tr">
                    <td class="fecha"><?php echo "&nbsp;&nbsp;" . date("d/m/Y", strtotime($appl->fecha)); ?></th>
                    <td class="comprob"><?php echo $appl->comprob . " - " . str_pad($appl->numero, 6, "0", STR_PAD_LEFT); ?></th>
                    <td></td>
                    <td class="num"><?php echo number_format($appl->importe_aplicacion * -1, 2); ?></td>
                    <td class="num"></td>
                    <td class="num"></td>
                    <td class="num"><?php echo number_format($saldo_prov, 2); ?></td>
                </tr>
            <?php endforeach; ?>
        <?php endforeach; ?>
        <?php foreach ($row->docsPend as $doc): ?>
            <?php $saldo_prov-=$doc->saldo; ?>
            <?php $saldo_guard_prov-=$doc->saldo; ?>
            <tr class="">
                <td class="fecha"><?php echo "..".date("d/m/Y", strtotime($doc->fecha)); ?></th>
                <td class="comprob"><?php echo $doc->comprob . " - " . str_pad($doc->numero, 6, "0", STR_PAD_LEFT); ?></th>
                <td colspan="1"><?php echo $doc->ok ? "Ok" : "Mal"; ?></td>
                <td class="num"><?php echo number_format($doc->total, 2); ?></td>
                <td class="num"><?php echo number_format($doc->saldo, 2); ?></td>
                <td class="num"></td>
                <td class="num"><?php echo number_format($saldo_prov, 2); ?></td>
            </tr>
            <?php $saldo_doc = 0; ?>
            
        <?php endforeach; ?>
        <tr>
            <td colspan="4">Saldo Socio</td>
            <td class="num"><?php echo number_format($saldo_guard_prov, 2); ?></td>
            <td class="num"><?php echo number_format($saldo_calc_prov, 2); ?></td>
            <td class="num"><?php echo number_format($saldo_prov, 2); ?></td>
        </tr>
        <tr><td></td></tr>
        <tr><td></td></tr>
    <?php endforeach; ?>
</table>
