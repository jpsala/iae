<style>
    label{vertical-align: top;}
</style>
<div class="container top20">
    <h1>Re-Aplicación de documentos, cuidado!</h1>
    </br>
    <form id="f2" class="form">
        <div class="control-group">
            <label for="matriculas">Matrículas</label>
            <textarea id="matriculas" name="matriculasStr" rows="10" cols="80"></textarea>
        </div>
        <div class="buttons">
            <input type="submit" value="re-aplica-auto, cuidado!"/>
        </div>
    </form>
  </br>
  <form id="f1" class="form">
    <div class="control-group">
      <label for="matriculas">Matrículas</label>
      <textarea id="matriculas" name="matriculasStr" rows="10" cols="80"></textarea>
    </div>
    <div class="buttons">
      <input type="submit" value="Desaplica todo, cuidado!"/>
    </div>
  </form>
</div>
<script>
  $('#f1').submit(function(){
    var data = $('#f1').serialize();
    console.log(data);
    $.ajax({
      url: "<?php echo $this->createUrl('socio/desAplicaAjax');?>",
      data:data,
      dataType: 'json',
      type: 'POST',
      success:function(data){
        console.log(data);
      }
    });
    return false;
  });
  $('#f2').submit(function(){
    var data = $('#f2').serialize();
    console.log(data);
    $.ajax({
      url: "<?php echo $this->createUrl('socio/reAplicaAjax');?>",
      data:data,
      dataType: 'json',
      type: 'POST',
      success:function(data){
        console.log(data);
      }
    });
    return false;
  });
</script>