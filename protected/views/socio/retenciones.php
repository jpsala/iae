<style>
    .input-retencion { width: 80px; text-align: right }
    .td-retencion { text-align: right; width: 80px }
    .td-error { width: 480px }
    .td-fecha { width: 80px; text-align: right }
    .input-cuit { width: 120px;}
    .td-cuit { width: 120px;}
</style>
<div class="container">
    <div class="nav nav-default">
        <button class="btn btn-primary btn-small top10"
                data-bind="click:actualizaRetenciones,
                           text: actualizando() ? 'Actualizando...':'Actualiza Retenciones',
                           enable:!actualizando()">
            Actualiza Retenciones
        </button>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <table
                class="table table-striped table-bordered table-hover table-condensed table-bordered top10">
                <thead>
                <tr>
                    <th>Proveedor</th>
                    <th>CUIT</th>
                    <th>Retención</th>
                    <th>Último Error!</th>
                    <th>Última Actualización</th>
                </tr>
                </thead>
                <tbody data-bind="foreach:{data:proveedores, as:'p'}">
                <tr>
                    <td data-bind="text: razon_social"></td>
                    <td class="td-cuit">
                        <input class="input-cuit" data-bind="value: cuit,
                event:{change:function(a,b){$root.cambiaCuit(a,b,$data)}}"/>
                    </td>
                    <td class="td-retencion">
                        <input class="input-retencion" data-bind="value: retencion,
                event:{change:function(a,b){$root.cambiaRetencion(a,b,$data)}}"/>
                    </td>
                    <td class="td-error" data-bind="text: retencion_error"></td>
                    <td class="td-fecha"
                        data-bind="text: retencion_ultima_actualizacion"></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    var params = {
        urlProveedores: "<?php echo $this->createUrl('/socio/proveedores');?>",
        urlActualizaRetenciones: "<?php echo $this->createUrl('/socio/actualizaRetenciones');?>",
        urlCambiaRetencion: "<?php echo $this->createUrl('/socio/cambiaRetencion');?>",
        urlCambiaCuit: "<?php echo $this->createUrl('/socio/cambiaCuit');?>"
    };

    var ViewModel = function (params) {
        var self = this;
        self.actualizando = ko.observable(false);
        self.proveedores = ko.observableArray();
        self.init = function () {
            $.ajax({
                dataType: 'json',
                url: params.urlProveedores,
                success: function (data) {
                    ko.wrap.updateFromJS(self.proveedores, data);
                }
            });
        };
        self.actualizaRetenciones = function () {
            self.actualizando(true);
            $.ajax({
                dataType: 'json',
                url: params.urlActualizaRetenciones,
                success: function (data) {
                    console.log(data);
                    //ko.wrap.updateFromJS(self.proveedores, data);
                },
                complete: function () {
                    self.actualizando(false);
                    //location.reload();
                }
            });
        };
        self.cambiaRetencion = function (a, b, c) {
            var id = a.id();
            $.getJSON(params.urlCambiaRetencion,
                {
                    id: a.id(),
                    retencion: a.retencion()
                }).done(function (data) {
                    a.retencion_error('');
                    a.retencion_ultima_actualizacion(data.ahora);
                    a.retencion_error(data.error);
                })
        };
        self.cambiaCuit = function (a) {
            var id = a.id();
            $.getJSON(params.urlCambiaCuit,
                {
                    id: a.id(),
                    cuit: a.cuit()
                }).done(function (data) {
                    a.retencion_ultima_actualizacion(data.ahora);
                })
        };
        self.init(params.urlProveedores);
    };
    var vm = new ViewModel(params);
    ko.applyBindings(vm);
</script>