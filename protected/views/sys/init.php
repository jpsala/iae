<?php

$tr = Yii::app()->db->beginTransaction();

$tipos = array(
    "1" => "Regular",
    "2" => "Novedad",
    "3" => "Novedad interna",
    "4" => "Movimiento bancario",
);
echo "********<br/>Tipos de artículos<br/>********<br/>";
foreach ($tipos as $id => $nombre) {
  $at = ArticuloTipo::model()->find("nombre = \"$nombre\"");
  if (!$at) {
    $at = new ArticuloTipo();
    $at->id = $id;
    $at->nombre = $nombre;
  } else {
    if ($at->id !== $id) {
      $at->id = $id;
    }
  }
  if (!$at->save()) {
    echo "No se pudo grabar $nombre<br/>";
    ve($at->errors);
    return;
  } else {
    echo "ok $id->$nombre<br/>";
  }
}

$tipos = array(
    "0" => "EFECTIVO",
    "1" => "CHEQUE TERCEROS",
    "2" => "TARJETA CREDITO",
    "3" => "CHEQUE PROPIO",
    "4" => "RECAUDACION BANCARIA",
    "5" => "TRANSFERENCIA",
);
echo "********<br/>Tipos de valor<br/>********<br/>";
foreach ($tipos as $id => $nombre) {
  $at = DocValorTipo::model()->find("nombre = \"$nombre\"");
  if (!$at) {
    $at = new DocValorTipo();
    $at->id = $id;
    $at->nombre = $nombre;
  } else {
    if ($at->id !== $id) {
      $at->id = $id;
    }
  }
  if (!$at->save()) {
    echo "No se pudo grabar $nombre<br/>";
    ve($at->errors);
    return;
  } else {
    echo "ok $id->$nombre<br/>";
  }
}
/*
 * ID_NIVEL	NOMBRE	DIEGEP	CANTIDADMATERIAS	ORDEN

  1	NIVEL INICIAL	3984	0	2
  2	EP 1	3984	10	3
  3	EP 2	3984	10	4
  4	ES	3984	11	5
  5	POLIMODAL	2574	11	6
  6	EGRESADO	0	0	7

 */
echo "********<br/>Tipos de Asignatura<br/>********<br/>";
$tipos = array(
    "1" => array("Nivel Inicial", 1, 3984, 1),
    "2" => array("EP 1", 3984, 1),
    "3" => array("EP 2", 3984, 1),
    "4" => array("ES", 3984, 1),
    "5" => array("POLIMODAL", 2574, 1),
    "6" => array("EGRESADO", 0, 1),
);
foreach ($tipos as $id => $value) {
  $nombre = $value[0];
  $nro = $value[1];
  $orden = $value[2];
  $at = Nivel::model()->find("nombre = \"$nombre\"");
  if (!$at) {
    $at = new Nivel();
    $at->id = $id;
  } else {
    if ($at->id !== $id) {
      $at->id = $id;
    }
  }
  $at->Logica_id = 1;
  $at->nombre = $nombre;
  $at->orden = $orden;
  $at->activo = 1;
  if (!$at->save()) {
    echo "No se pudo grabar $nombre<br/>";
    ve($at->errors);
    return;
  } else {
    echo "ok $id->$nombre<br/>";
  }
}

//echo "********<br/>Niveles<br/>********<br/>";
//$tipos = array(
//    "1" => array("Normal EP1", 1),
//    "2" => array("Normal EP2", 2),
//    "3" => array("Normal Sec", 3),
//);
//foreach ($tipos as $id => $value) {
//  $nombre = $value[0];
//  $nivel_id = $value[1];
//  $at = AsignaturaTipo::model()->find("nombre = \"$nombre\"");
//  if (!$at) {
//    $at = new AsignaturaTipo();
//    $at->id = $id;
//  } else {
//    if ($at->id !== $id) {
//      $at->id = $id;
//    }
//  }
//  $at->Nivel_id = $nivel_id;
//  $at->nombre = $nombre;
//  if (!$at->save()) {
//    echo "No se pudo grabar $nombre<br/>";
//    ve($at->errors);
//    return;
//  } else {
//    echo "ok $id->$nombre<br/>";
//  }
//}
echo "********<br/>Tipos de Comprobante<br/>********<br/>";
$rows = array(
    array(
        "id" => "1",
        "nombre" => "Recibo",
        "tipo" => "",
        "compra_venta" => "V",
        "afecta_caja" => "1",
        "signo_cc" => "-1",
        "abreviacion" => "recv",
        "signo_caja" => "1",
        "signo_stock" => "",
    ),
    array(
        "id" => "2",
        "nombre" => "Nota de débito",
        "tipo" => "",
        "compra_venta" => "V",
        "afecta_caja" => "1",
        "signo_cc" => "-1",
        "abreviacion" => "ndv",
        "signo_caja" => "0",
        "signo_stock" => "",
    ),
    array(
        "id" => "3",
        "nombre" => "Nota de crédito",
        "tipo" => "",
        "compra_venta" => "V",
        "afecta_caja" => "0",
        "signo_cc" => "-1",
        "abreviacion" => "ncv",
        "signo_caja" => "0",
        "signo_stock" => "",
    ),
    array(
        "id" => "4",
        "nombre" => "Factura",
        "tipo" => "",
        "compra_venta" => "V",
        "afecta_caja" => "1",
        "signo_cc" => "1",
        "abreviacion" => "facv",
        "signo_caja" => "1",
        "signo_stock" => "",
    ),
    array(
        "id" => "5",
        "nombre" => "Factura",
        "tipo" => "",
        "compra_venta" => "C",
        "afecta_caja" => "",
        "signo_cc" => "-1",
        "abreviacion" => "facc",
        "signo_caja" => "1",
        "signo_stock" => "",
    ),
    array(
        "id" => "6",
        "nombre" => "Nota de débito",
        "tipo" => "",
        "compra_venta" => "C",
        "afecta_caja" => "0",
        "signo_cc" => "1",
        "abreviacion" => "ndc",
        "signo_caja" => "0",
        "signo_stock" => "",
    ),
    array(
        "id" => "7",
        "nombre" => "Nota de crédito",
        "tipo" => "",
        "compra_venta" => "C",
        "afecta_caja" => "0",
        "signo_cc" => "-1",
        "abreviacion" => "ncc",
        "signo_caja" => "0",
        "signo_stock" => "",
    ),
    array(
        "id" => "8",
        "nombre" => "Orden de pago",
        "tipo" => "",
        "compra_venta" => "C",
        "afecta_caja" => "-1",
        "signo_cc" => "-1",
        "abreviacion" => "opc",
        "signo_caja" => "-1",
        "signo_stock" => "",
    ),
    array(
        "id" => "9",
        "nombre" => "Entrada de caja",
        "tipo" => "",
        "compra_venta" => "F",
        "afecta_caja" => "1",
        "signo_cc" => "",
        "abreviacion" => "ec",
        "signo_caja" => "1",
        "signo_stock" => "",
    ),
    array(
        "id" => "10",
        "nombre" => "Salida de caja",
        "tipo" => "",
        "compra_venta" => "F",
        "afecta_caja" => 1,
        "signo_cc" => "",
        "abreviacion" => "sc",
        "signo_caja" => -1,
        "signo_stock" => "",
    ),
    array(
        "id" => "11",
        "nombre" => "Depósito Bancario Salida",
        "tipo" => "",
        "compra_venta" => "B",
        "afecta_caja" => 1,
        "signo_cc" => "",
        "abreviacion" => "db",
        "signo_caja" => -1,
        "signo_stock" => "",
    ),
    array(
        "id" => "12",
        "nombre" => "Transferencia Entrada",
        "tipo" => "",
        "compra_venta" => "F",
        "afecta_caja" => 1,
        "signo_cc" => "",
        "abreviacion" => "te",
        "signo_caja" => 1,
        "signo_stock" => "",
    ),
    array(
        "id" => "13",
        "nombre" => "Transferencia Salida",
        "tipo" => "",
        "compra_venta" => "F",
        "afecta_caja" => 1,
        "signo_cc" => "",
        "abreviacion" => "ts",
        "signo_caja" => -1,
        "signo_stock" => "",
    ),
    array(
        "id" => "14",
        "nombre" => "Depósito Bancario Entrada",
        "tipo" => "",
        "compra_venta" => "F",
        "afecta_caja" => 1,
        "signo_cc" => "",
        "abreviacion" => "dbe",
        "signo_caja" => 1,
        "signo_stock" => "",
    ),
    array(
        "id" => "15",
        "nombre" => "Movimiento Bancario",
        "tipo" => "",
        "compra_venta" => "F",
        "afecta_caja" => 0,
        "signo_cc" => "",
        "abreviacion" => "mb",
        "signo_caja" => 0,
        "signo_stock" => "",
    ),
);
foreach ($rows as $row) {
  $nombre = $row["nombre"];
  $compra_venta = $row["compra_venta"];
  $at = Comprob::model()->find("nombre = \"$nombre\" and compra_venta = \"$compra_venta\"");
  if (!$at) {
    $at = new Comprob();
  }
  foreach ($row as $fld => $value) {
    if ($fld == "id" and $at->$fld !== $value and !$at->isNewRecord) {
      ve("mal el id de $nombre -> $value");
      return;
    }
    $at->$fld = $value;
  }
  try {
    if (!$at->save()) {
      echo "No se pudo grabar $nombre<br/>";
      ve($at->attributes);
      ve($at->errors);
      return;
    } else {
      echo "ok $id->$nombre<br/>";
    }
  } catch (Exception $e) {
    echo "<pre>";
    ve($at->attributes);
    ve($at->errors);
    ve("Error:" . $e->getCode() . "</pre></br>");
//      if ($e->getCode() == 42) {
//        echo 'Error ejecutando query:</br>';
    ve($e->getMessage());
//      }
    echo $e->getTraceAsString();
    echo "</pre>";
    return;
  }
}



echo "<br/>****************Caja***************<br/>";
if ($c = Destino::model()->find("tipo = 1")) {
  ve("Ya existe una caja");
  ve($c->attributes);
} else {
  $c = new Destino();
  $c->tipo = 1;
  $c->nombre = "Caja";
  if (!$c->save()) {
    ve("error grabando la caja");
    ve($c->attributes);
    ve($c->errors);
    return;
  }
}

echo "<br/>****************Puesto de trabajo***************<br/>";
if ($pt = PuestoTrabajo::model()->find()) {
  ve("Ya existe una Puesto de Trabajo");
  ve($pt->attributes);
} else {
  $pt = new PuestoTrabajo();
  $pt->id = 0;
  $pt->nombre = "Puesto de trabajo 1";
  $pt->destino_id = Destino::model()->find("tipo = 1")->id;
  if (!$pt->save()) {
    ve("error grabando Puesto de trabajo");
    ve($pt->attributes);
    ve($pt->errors);
    return;
  }
  echo "<br/>Ok<br/>";
}

echo "<br/>****************Ciclo***************<br/>";
$c = new Ciclo();
if (Ciclo::model()->find()) {
  ve("Ya existe un ciclo");
  ve($c->attributes);
} else {
  $c->activo = 1;
  $c->nombre = date("Y", time());
  $c->fecha_inicio = date("Y/m/d", time());
  $c->fecha_fin = date("Y/m/d", time() + 365 * 3600 * 24);
  if (!$c->save()) {
    echo "<br/>No se pudo grabar el ciclo activo<br/>";
    ve($c->attributes);
    ve($c->errors);
    return;
  }
}

echo "<br/>****************Talonarios***************<br/>";
//$tr = Yii::app()->db->beginTransaction();
foreach (Comprob::model()->findAll() as $c) {
  if ($t = Talonario::model()->find("comprob_id = $c->id")) {
    ve("Ya existe un puesto de trabajo asociado con $c->nombre");
  } else {
    $ptt = new Talonario();
    $ptt->comprob_id = $c->id;
    $ptt->letra = "X";
    $ptt->numero = 0;
    $ptt->punto_venta_numero = 1;
    $ptt->numeracion_manual = 0;
    $ptt->activo = 1;
    if (!$ptt->save()) {
      echo "<br/>No se pudo grabar el talonario para el comprob. $c->nombre<br/>";
      ve($ptt->attributes);
      ve($ptt->errors);
      return;
    } else {
      ve("ok -> talonario para el comprob. $c->nombre");
    }
  }
}
//$tr->commit();

echo "<br/>****************Puestos de trabajo <-> talonarios***************<br/>";
$pt = PuestoTrabajo::model()->find();
if (!$pt) {
  ve("no se importó puesto de trabajo");
  return;
}

//$tr = Yii::app()->db->beginTransaction();
foreach (Talonario::model()->findAll() as $t) {
  if ($ptt = PuestoTrabajoTalonario::model()->with(array("talonario", "talonario.comprob"))->find("talonario_id = $t->id")) {
    $comprob = $ptt->talonario->comprob->nombre;
    ve("Puesto de trabajo $pt->nombre ya está asociado con $comprob");
  } else {
    $ptt = new PuestoTrabajoTalonario();
    $ptt->talonario_id = $t->id;
    $ptt->puesto_trabajo_id = $pt->id;
    if (!$ptt->save()) {
      echo "<br/>No se pudo grabar Puestos de trabajo: $pt->id <-> talonario. $t->id<br/>";
      ve($ptt->attributes);
      ve($ptt->errors);
      return;
    } else {
      $comprob = $ptt->talonario->comprob->nombre;
      ve("Ok -> Puesto de trabajo $pt->nombre está asociado con $comprob");
    }
  }
}
//$tr->commit();

echo "<br/>****************Logica_ciclo por cada asignaturaTipo***************<br/>";
$ciclo_id = Ciclo::getActivo()->id;
$logica_id = Logica::model()->find()->id;

//$tr = Yii::app()->db->beginTransaction();
foreach (AsignaturaTipo::model()->findAll() as $at) {
  if ($lc = LogicaCiclo::model()->find("ciclo_id = $ciclo_id 
      and logica_id = $logica_id
      and Asignatura_Tipo_id = $at->id")) {
    ve("ok: Logica ciclo para ciclo $ciclo_id, lógica $logica_id y tipo de asignatura $at->id ya existe");
  } else {
    $lc = new LogicaCiclo();
    $lc->Logica_id = $logica_id;
    $lc->Ciclo_id = $ciclo_id;
    $lc->Asignatura_Tipo_id = $at->id;
    if (!$lc->save()) {
      echo "<br/>No se pudo grabar Logica ciclo para ciclo $ciclo_id, lógica $logica_id y tipo de asignatura $at->id<br/>";
      ve($lc->attributes);
      ve($lc->errors);
      return;
    } else {
      $comprob = $ptt->talonario->comprob->nombre;
      ve("ok: Logica ciclo para ciclo $ciclo_id, lógica $logica_id y tipo de asignatura $at->id");
    }
  }
}
$tr->commit();
?>
