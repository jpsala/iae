<script>

  var vm = (function(){
    var pub = {};
    pub.comprobantes = ko.observableArray();
    ko.mapping.fromJS(<?php echo json_encode($comprobantes);?>,{
      create:function(o){
        return new Comprobante(o.data);
      }
    },pub.comprobantes);
    pub.talonarios = <?php echo json_encode($talonarios);?>;
    //console.log(pub)
    return pub;
  })()
  function Comprobante(data){
    ko.mapping.fromJS(data,{},this);
  }
</script>