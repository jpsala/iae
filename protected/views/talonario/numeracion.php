<style type="text/css">
    .puesto-td{width:150px;}
    .punto-td{width:30px; text-align: center}
    .numero-td input{width:70px;text-align: right}
    .comprobante-td{width:250px}
</style>
<table id="talonario-table">
    <tr>
        <th class="puesto-td">Puesto de trabajo</th>
        <th class="comprobante-td">Comprobante</th>
        <th class="punto-td">Punto de venta</th>
        <th class="numero-td">Numero</th>
    </tr>
    <?php foreach ($data as $r): ?>
        <tr talonario_id="<?php echo $r["talonario_id"]; ?>">
            <td class="puesto-td"><?php echo $r["puesto"]; ?></td>
            <td class="comprobante-td"><?php echo $r["comprobante"]; ?></td>
            <td class="punto-td"><?php echo $r["punto_venta_numero"]; ?></td>
            <td class="numero-td">
                <input 
                    name="data[<?php echo $r["talonario_id"]; ?>][numero]" 
                    value="<?php echo $r["numero"]; ?>"
                    onchange="return numeroChange($(this));"/>
            </td>
            <td></td>
        </tr>
    <?php endforeach; ?>
</table>
<script type="text/javascript">
                    function numeroChange($obj) {
                        var $tr=$obj.closest("tr");
                        var talonario_id = $tr.attr("talonario_id");
                        var numero = $tr.find("input").val();
                        data = {numero:numero,talonario_id:talonario_id};
                        $.ajax({
                            type: "GET",
                            //      dataType:"json",
                            data: data,
                            url: "<?php echo $this->createUrl("talonario/indexGraba"); ?>",
                            success: function(data) {
                            },
                            error: function(data, status) {
                            }
                        }
                        );
                    }
</script>
