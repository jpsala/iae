<?php if ($action == ""): ?>
    <style type="text/css">
        #test-div a{cursor: pointer;margin-left: 10px}
        #test-div{padding: 5px}
        #ajax{margin-top: 15px; min-height: 100px; width: 100%;padding: 10px}
    </style>

<?php endif; ?>
<?php
if ($action == "") {
    echo '<div id="test-div" class="ui-widget ui-widget-content ui-corner-all">';
}

class test extends stdClass {

    public function __construct($action, $controller, $exec) {
        /* @var $controller CController */
        if ($action == "") {
            foreach (get_class_methods("test") as $m) {
                if ($m !== "__construct") {
                    $url = $controller->createUrl("test/index", array("action" => $m, "exec" => 1));
                    echo "<button onclick=\"return irA('" . $url . "');\">$m</button>";
                }
            }
        } else {
            if (!method_exists($this, $action)) {
                throw new Exception("No existe el metodo $action en el view test");
            } else {
                try {
                    $this->$action();
                } catch (Exception $exc) {
                    ve($exc->getTraceAsString());
                }
            }
        }
    }

    private function docLiquiqPerdidos() {
        $cf = Comprob::FACTURA_VENTAS;
        $rows = Yii::app()->db->createCommand("
                select doc.*, l.*
                    from doc 
                        inner join doc_liquid l on l.id = doc.doc_liquid_id
                        inner join talonario t on t.id = doc.talonario_id
                    where l.liquid_conf_id = (select max(id) from liquid_conf) and t.comprob_id = $cf
            ")->queryAll();
        vd($rows);
    }

    private function verUltimaLiquidacion() {
        $cf = Comprob::FACTURA_VENTAS;
        $rows = Yii::app()->db->createCommand("
                select concat(a.apellido, ', ', a.nombre) as alumno, doc.*, l.*
                    from doc 
                        inner join doc_liquid l on l.id = doc.doc_liquid_id
                        inner join socio s on s.id = doc.`Socio_id`
                        inner join alumno a on a.id = s.`Alumno_id`
                        inner join talonario t on t.id = doc.talonario_id
                    where l.liquid_conf_id = (select max(id) from liquid_conf) and t.comprob_id = $cf
            ")->queryAll();
        vd($rows);
    }

        private function becas() {
        $cf = Comprob::FACTURA_VENTAS;
        $cantRecargos = Yii::app()->db->createCommand("
                select count(*)
                    from doc 
                        inner join doc_liquid l on l.id = doc.doc_liquid_id
                        inner join talonario t on t.id = doc.talonario_id
                    where l.liquid_conf_id = (select max(id) from liquid_conf) and t.comprob_id = $cf and
                                l.beca > 0
            ")->queryScalar();
        echo "Cantidad de recargos : $cantRecargos<br/>";
        }
        
    private function statusUltimaLiquidacion() {
        $cf = Comprob::FACTURA_VENTAS;
        $cantRecargos = Yii::app()->db->createCommand("
                select count(*)
                    from doc 
                        inner join doc_liquid l on l.id = doc.doc_liquid_id
                        inner join talonario t on t.id = doc.talonario_id
                    where l.liquid_conf_id = (select max(id) from liquid_conf) and t.comprob_id <> $cf
            ")->queryScalar();
        echo "Cantidad de recargos : $cantRecargos<br/>";
        $cantLiquidados = Yii::app()->db->createCommand("
                select count(*)
                    from doc 
                        inner join doc_liquid l on l.id = doc.doc_liquid_id
                        inner join talonario t on t.id = doc.talonario_id
                    where l.liquid_conf_id = (select max(id) from liquid_conf) and t.comprob_id = $cf
            ")->queryScalar();
        echo "Cantidad de liquidados : $cantLiquidados<br/>";
        $cantDocLiquid = Yii::app()->db->createCommand("
                select count(*)
                    from doc_liquid l
                    where l.liquid_conf_id = (select max(id) from liquid_conf)
            ")->queryScalar();
        echo "Cantidad de docLiquid : $cantDocLiquid<br/>";
        $cantDocLiquidSueltos = Yii::app()->db->createCommand("
                select count(*) from doc_liquid l
                    where l.id not in
                        (select d.doc_liquid_id from doc d where d.doc_liquid_id = l.id)
            ")->queryScalar();
        echo "Cantidad de docLiquid Sueltos: $cantDocLiquidSueltos<br/>";
    }
    
    private function borraDocLiquidSuelgos() {
        $docLiquidSueltos = Yii::app()->db->createCommand("
                select * from doc_liquid l
                    where l.id not in
                        (select d.doc_liquid_id from doc d where d.doc_liquid_id = l.id)
            ")->queryAll();
        ve($docLiquidSueltos);
        return;
        $cantDocLiquidSueltos = Yii::app()->db->createCommand("
                delete l.* from doc_liquid l
                    where l.id not in
                        (select d.doc_liquid_id from doc d where d.doc_liquid_id = l.id)
            ")->execute();
        echo "Cantidad de docLiquid Sueltos Borrados: $cantDocLiquidSueltos<br/>";
    }

}

new test($action, $this, $exec);

if ($action == "") {
    echo "</div>";
}
?>

<?php if ($action == ""): ?>
    <div id="ajax" class="ui-widget ui-widget-content ui-corner-all"></div>

    <script type="text/javascript">

        $("button").button();

        function irA(url) {
            $.ajax({
                type: "GET",
                url: url,
                success: function(data) {
                    $("#ajax").html(data);
                },
                error: function(data, status) {
                }
            }
            );
            return false;
        }
    </script>
<?php endif; ?>