<div class="container">
    <div class="panel panel-default top30">
            <div class="panel-body">
                Alumno: <?php echo $alumno['nombres'];?>
            </div>
    </div>
    <table class="table table-bordered table-stripped">
        <thead>
        <tr>
            <th>Fecha de carga</th>
            <th>Detalle</th>
            <th>Importe</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($data as $row):?>
            <tr>
                <td><?php echo $row['fecha_carga'] ;?></td>
                <td><?php echo $row['detalle'] ;?></td>
                <td><?php echo $row['importe'] ;?></td>
            </tr>
        <?php endforeach;?>
        </tbody>
    </table>
</div>