<style type="text/css">
    .label{margin-right: 8px}
</style>

<link rel="stylesheet" href="js/chosen/chosen/chosen.css"/>
<script src="js/chosen/chosen/chosen.jquery.min.js"></script>

<div>
    <form class="form">
        <?php foreach ($users as $u): ?>
            <span class="label"> <?php echo $u->nombre; ?> </span>
            <select id="select" data-placeholder="Roles" style="width:350px" multiple tabindex="3">
                <option value=""></option> 
                <?php foreach ($u->roles as $i): ?>
                    <?php $sel = $u->checkAccess($i->name) ? 'selected="selected"' : ''; ?>
                    <option <?php echo $sel;   ?> value="<?php echo $i->name;   ?>"><?php echo $i->name;   ?></option> 
                <?php endforeach; ?>
            </select>
        <?php endforeach; ?>
    </form>
</div>

<script type="text/javascript">
    $('select').chosen();
</script>




