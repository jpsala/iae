<form id="form-orientacion-nueva" method="POST" action="#">

    <table class="ui-widget ui-widget-content" id="tabla-tipoasignaturas">
        <tr class="ui-widget-header">
            <th>Tipo de Asignaturas en el Nivel</th>
            <th>&nbsp</th>
        </tr>
        <?php $row = 0;?>
        <?php foreach (AsignaturaTipo::model()->findAll("Nivel_id = $nivel_id") as $asignatura_tipo): ?>
            <tr class="tr-asignaturatipo <?php echo ($row++ % 2) ? "tr-even" : ""; ?>" asignatura_tipo_id="<?php echo $asignatura_tipo->id; ?>">
                <td class="td-nombre"><?php echo $asignatura_tipo->nombre; ?></td>
                <td class="td-borra" onclick="return borraAsigTipo(<?php echo $asignatura_tipo->id; ?>)"></td>
            </tr>
        <?php endforeach; ?>
        <tr class="tr-asignaturatipo <?php echo ($row++ % 2) ? "tr-even" : ""; ?>" id="tr-asigtipo-nueva">
            <td class="td-nombre"><input id="nombre-asigtipo" name="nombre" onchange="return changeNombreAsigTipos(this)"/></td>
            <td ><input type="button" id="graba-asigtipo-nueva-button" value="Graba" onclick="return grabaAsigTipo(this)"/></td>
        </tr>
    </table>
</form>