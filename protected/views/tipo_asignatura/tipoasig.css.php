<style type="text/css">
    #pedido-de-datos{ margin: 0px 0 9px 2px}
    #pedido-de-datos .row{display:inline-block}
    #pedido-de-datos #nivel-select{width: 150px;}

    #pedido-de-datos #division-select{width: 150px}
    #pedido-de-datos #asignatura-select{width: 150px}
    #pedido-de-datos #periodo-select{width: 190px}

    #tabs{height: 525px;}
    #tab-asignaturas, #tab-divisiones{height: 100%}

    table.table{background-color: #C3D9FF;    border: 1px solid #0066A4;    border-radius: 4px 4px 4px 4px;    margin: 10px 0 0;    padding: 4px; }
    table.table tr{background-color: #fff}
    table.table td{ font-size: 90%; padding: 3px 5px 3px 5px}
    table.table th{border-bottom: 1px solid #777777;  padding: 1px 4px 2px 2px; background-color: #C3D9FF; color: #555555}
    table.table td.center,th.center{text-align: center}
    table.table tr#tr-asignatura-nueva td{padding: 15px 3px}
    table.table{width: 300px}
    table tr *{cursor:pointer}
    table tr input{cursor:text}
    table tr select, table tr input[type="button"]{cursor:auto}
    table td.td-borra{background-image: url("images/delete.jpg");background-repeat: no-repeat; cursor: pointer; text-align: right;background-position: right center;}
    #tab-asignaturas #nombre-asignatura{width: 250px}
    #graba-asigtipo-nueva-button{font-size: 90%;    margin-top: -2px;    padding: 2px;}    
    .buttons-div{ bottom: -20px;    position: absolute;    right: 13px;    text-align: right;}
    #tabla-tipoasignaturas{margin-top: 12px}
    #div-asignatura-edita input[type="button"]{ padding: 1px 0}
    #div-asignatura-edita #division-nombre{width:90px}
    #div-asignatura-edita #division-orden{width:25px;}
    #div-asignatura-edita #label-for-orden{margin-left: 6px}
    #div-asignatura-edita #button-graba-division{margin-left: 10px}
    
    .right-align{text-align: right}

    #footer{margin:10px 23px}
</style>
