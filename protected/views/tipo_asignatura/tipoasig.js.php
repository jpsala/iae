
<script type="text/javascript">
    
    var nivel_id, anio_id, $tr_orientacion_nueva,  division_id;
    var $tr_asignaturatipo = $('<tr><asignatura_tipo_id="8" class="tr-asignaturatipo">  <td class="td-nombre">ddvs</td> <td onclick="return borraAsigTipo(8)" class="td-borra"></td> </tr>');
    
    $(function(){
        init();
    });
    
    function init(){
        
        $("button").button();
        
        $("#nivel-select").chosen().change(function(){
            changeNivel($(this));
        });

        $(".clearinput").clearinput();
        
        $("#nivel-select_chzn").mousedown();
        
    }
    
    
    function changeNivel($this){
        nivel_id = $this.val();
        traeAsigTipos($this);
        return false;
    }
    
   function changeNombreAsigTipos($this){
        if( ! $("#nombre-asigtipo").val()){
            $("#nombre-asigtipo").val($($this).val());
        }
    }
    
    
    
    function pedidoDeDatosVisible(visible){
        $("#pedido-de-datos").animate({"opacity": visible ? "show":"hide"},"slow");    
    }
    
    function traeAsigTipos($this){
        anio_id = $this.val();
        $.ajax({
            type: "GET",
            //dataType: "json",
            data: {nivel_id:nivel_id},
            url: "<?php echo $this->createUrl("asignaturatipo/AdminAsigTipos"); ?>",
            success: function(data) {
                $("#asignaturatipo").html(data);
                $("#graba-asigtipo-nueva-button").button();
                $(".tr-asignaturatipo input:not(:hidden)").first().focus();
                //$tr_orientacion = $(".tr-orientacion").first().clone();
                $(".tr-asignaturatipo:not('#tr-asigtipo-nueva')").find("td:not('.td-borra')").click(function(){clickAsigTipo($(this).parent())});
            },
            error: function(data,status){
            }
        });
    }
    
    function clickAsigTipo($this){
        
        $rec = $("#tr-asigtipo-nueva");
        $rec.find("#nombre-asigtipo").val($this.find(".td-nombre").html());
        $rec.attr("asignatura_tipo_id",$this.attr("asignatura_tipo_id"));
        $rec.find("input:not(:hidden)").first().focus();
    }
    
    function grabaAsigTipo(input){
        var $tr;
        $rec = $(input).closest("tr");
        
        asignatura_tipo_id = $rec.attr("asignatura_tipo_id");
        var data = "Nivel_id=" + nivel_id + "&" +"asignatura_tipo_id=" + asignatura_tipo_id + "&" + $rec.find("*").serialize();
        $.ajax({
            type: "POST",
            dataType: "json",
            data: data,
            url: "<?php echo $this->createUrl("asignaturatipo/AdminAsigTiposGraba"); ?>",
            success: function(data) {
                if(data.error){
                    alert("Error en los datos, verifique")
                }else{
                    if(!data.nuevo){
                        $tr = $('[asignatura_tipo_id="%id%"]'.replace("%id%",data.asignatura_tipo_id)).first();
                        $rec.removeAttr("asignatura_tipo_id");
                    }else{
                        $tr = $tr_asignaturatipo.clone();
                        //$tr = $tr_asignaturatipo;
                        $tr.find(".td-borra").attr("onclick","return borraAsigTipo("+data.asignatura_tipo_id+")");
                    }
                    $tr.attr("asignatura_tipo_id",data.asignatura_tipo_id);
                    $tr.find(".td-nombre").html($rec.find("#nombre-asigtipo").val());
                    if(data.nuevo){
                        $rec.before($tr);
                        $tr.find("td:not('.td-borra')").click(function(){clickAsigTipo($(this).parent())});
                    }
                    $rec.find("#nombre-asigtipo").val("");
                    $rec.find("input:not(:hidden)").first().focus();
                }
            },
            error: function(data,status){
            }
        });

    }
    
    function cancela(){
        pedidoDeDatosVisible(true);
        limpiaTodo();
        return false;
    }
    
       
    function borraAsigTipo(asignatura_tipo_id){
        $.ajax({
            type: "GET",
            //dataType: "json",
            data: {asignatura_tipo_id:asignatura_tipo_id},
            url: "<?php echo $this->createUrl("asignaturatipo/AdminAsigTiposBorra"); ?>",
             
            success: function(data) {
                if (!data){
                $('[asignatura_tipo_id="%id%"]'.replace("%id%",asignatura_tipo_id)).remove();
                }else{alert('El tipo de asignatura se encuentra en uso');}
            },
            error: function(data,status){
            }
        });

        return false;
    }    
    
    function limpiaTodo(){
        
    }
    
</script>
