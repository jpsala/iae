<?php $todos = true; ?>
<?php foreach ($usuarios as $u): ?>
  <?php if (!$u["asignado"]) $todos = false; ?>
  <tr class="rec">
    <td class="nombre"><?php echo $u["nombre"]; ?></td>
    <td class="asignado <?php echo $u["asignado"] ? "asignado-verdadero" : ""; ?>"
        onclick="userClick(<?php echo $u["id"]; ?>)">
      <span id="usuario_<?php echo $u["id"] ?>" ><?php echo $u["asignado"] ? "Si" : "No"; ?></span></td>
  </tr>
<?php endforeach; ?>
<script type="text/javascript">
    $("#asignados-check").attr("checked",eval("<?php echo $todos ? "true":"false";?>"));
</script>