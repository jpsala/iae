<style type="text/css">
  .users-table{width: 700px; margin:auto; padding-left: 0; margin-left: 0}
  .password-reset-td, borra-td{padding-right: 0; margin-right: 0; text-align: right}
  .password-reset, .borra-td{text-align: right}
  td{position: relative}
  #pr{color: red !important;
      margin-left: 12px;
      margin-top: 2px;
      position: absolute;}
    .user-admin-doc{
        max-width: 110px
    }
  </style>
  <?php $r = Yii::app()->authManager; ?>
  <table class="users-table">
      <tr>
          <th>Nombre</th>
          <th>Documento</th>
          <th>Login</th>
      </tr>
    <?php foreach (User::model()->findAllBySql("select id, nombre, documento, login from user order by nombre") as $user): ?>
      <?php
      if ($r->checkAccess("Admin", $user->id) or
              $r->checkAccess("Administracion", $user->id))
        continue;
      ?>

    <tr id="<?php echo $user->id; ?>">
      <td><?php echo $user->nombre; ?></td>
      <td><input id="<?php echo $user->id;?>" class="user-admin-doc" type="text" value="<?php echo $user->documento   ; ?>"></td>
      <td><?php echo $user->login; ?></td>
      <td class="borra-td">
        <input type="button" 
               value="Elimina" 
               class="password-reset"
               onclick="borra($(this));
                     return false;"/>
      </td>
      <td class="password-reset-td">
        <input type="button" 
               value="Reinicia Password" 
               class="password-reset"
               onclick="passwordReset($(this));
                     return false;"/>
      </td>
    </tr>
  <?php endforeach; ?>
  <tr id="nuevo">
    <td><input type="text" placeholder="Nombre"/></td>
    <td></td>
    <td class="password-reset-td">
      <input type="button" 
             value="Nuevo" 
             class="nuevo"
             onclick="usuarioNuevo($(this));
                   return false;"/>
    </td>
  </tr>
</table>

<script type="text/javascript">
                 $(".password-reset").button();
                 $(".nuevo").button();
                 
                 function passwordReset($o) {
                   var user_id = $o.closest("tr").attr("id");
                   $.ajax({
                     type: "GET",
                     dataType: "json",
                     data: {user_id: user_id},
                     url: "<?php echo $this->createUrl("/user/resetPassword"); ?>",
                     success: function(data) {
                       if (data.errores && muestraErroresDoc(data.errores)) {
                         return false;
                       } else {
                         $o.closest("tr").find("td").last().after().append("<span id=\"pr\">   Password reseteado</span>");
                         colorize($("#pr"));
                       }
                     },
                     error: function(data, status) {
                     }
                   });

                 }
                 function borra($o) {
                   var user_id = $o.closest("tr").attr("id");
                   $.ajax({
                     type: "GET",
                     dataType: "json",
                     data: {user_id: user_id},
                     url: "<?php echo $this->createUrl("/user/borra"); ?>",
                     success: function(data) {
                       if (data.errores && muestraErroresDoc(data.errores)) {
                         return false;
                       } else {
                         console.log(data);
                         if(data.error){
                           alert(data.error);
                           return false;
                         }
                         $o.closest("tr").remove();
                       }
                     },
                     error: function(data, status) {
                       alert('error intentando borrar usuario');
                     }
                   });

                 }
                 $('.user-admin-doc').on('change', function () {
                   console.log($(this));
                   var id = $(this).attr('id')
                   var value = $(this).val();
                   $.ajax({
                     type: "POST",
                     url: "<?php echo $this->createUrl("/user/grabaUserDocumento"); ?>",
                     data: {'id':id,'documento':value },
                     success: function (json) {
                       // do things
                     }
                   });
                 })

                 function usuarioNuevo($o) {
                   var $tr = $o.closest("tr");
                   var nombre = $tr.find("input").eq(0).val();
                   $.ajax({
                     type: "GET",
                     dataType: "json",
                     data: {nombre: nombre},
                     url: "<?php echo $this->createUrl("/user/usuarioNuevo"); ?>",
                     success: function(data) {
                       if (data.errores && muestraErroresDoc(data.errores)) {
                         return false;
                       } else {
                         $trCloned = $(".users-table").find("tr").eq(0).before().clone();
                         $trCloned.attr("id", data.id);
                         $trCloned.find("td").eq(0).html(nombre);
                         $trCloned.find("td").eq(1).html(data.login);
                         $(".users-table").find("tr").eq(-1).before($trCloned);
                         $(".users-table").find("tr").eq(-2).find("td").last().append("<span id=\"pr\">   El Usuario fué dado de alta</span>");
                         $tr.find("input").eq(0).val("");
                         colorize($("#pr"));
                       }
                     },
                     error: function(data, status) {
                     }
                   });

                 }

</script>
