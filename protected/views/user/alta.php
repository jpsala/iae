<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'user-form',
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'validateOnType' => true,
            )));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->label($model, 'nombre'); ?>
        <?php echo $form->textField($model, 'nombre') ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'login'); ?>
        <?php echo $form->textField($model, 'login') ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'password'); ?>
        <?php echo $form->passwordField($model, 'password') ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'password_repeat'); ?>
        <?php echo $form->passwordField($model, 'password_repeat') ?>
    </div>
    
    <div class="row">
        <?php echo $form->label($model, 'Puesto Trabajo'); ?>
        <select id="Puesto" data-placeholder="Seleccione un puesto de trabajo" class="chzn-select">
            <?php
            $x = array('prompt' => '');
            echo CHtml::listOptions($model->puesto_trabajo_id, CHtml::listData(PuestoTrabajo::Model()->findAll(), 'id', 'nombre'), $x)
            ?>
        </select>
    </div>


    <div class="row submit">
        <?php echo CHtml::submitButton('Alta'); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->