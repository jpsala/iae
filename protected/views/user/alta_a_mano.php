<style type="text/css">
    label{width:150px}
    #errores{border: red 1px solid; padding: 5px; width: 200px; margin: 0; text-align: center}
    #leyenda{border: green 1px solid; padding: 5px; width: 200px; margin: 0; text-align: center}
    .error{font-size: 80%; color: red}
</style>

<?php if ($leyenda): ?>
    <div id="leyenda">
        <span> <?php echo $leyenda; ?></span>
    </div>
<?php endif; ?>

<?php if (count($errors)): ?>
    <div id="errores">
        <?php foreach ($errors as $e): ?>
            <div id="error">
                <span class="error"> <?php echo $e[0]; ?></span>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
<div  class="form">
    <form method="post" name="alta-usuario">
        <div class="row">
            <label for="nombre">Nombre:
                <input id="nombre" type="text" name="nombre" value="<?php echo $user->nombre; ?>"/>
            </label>
        </div>
        <div class="row">
            <label for="login">Login:
                <input id="login" type="text" name="login" value="<?php echo $user->login; ?>"/>
            </label>
        </div>
        <div class="row">
            <label for="password">Password:
                <input id="password" type="password" name="password" value="<?php echo $user->password; ?>"/>
            </label>
        </div>
        <div class="row">
            <label for="password_repeat">Repita el password:
                <input id="password_repeat" type="password" name="password_repeat" value="<?php echo $user->password_repeat; ?>"/>
            </label>
        </div>
        <div class="row">
            <input name="submit" type="submit" value="Alta"/>
        </div>
    </form>
</div>

<script type="text/javascript">
    setTimeout(function(){
        $('#leyenda').hide('slow');
    }, 3000);
    
</script>