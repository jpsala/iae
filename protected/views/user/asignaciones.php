<style>
  #roles-div, #usuarios-div{; float:left; margin-right: 5px; min-height: 400px; max-height: 600px; overflow: auto}
  #roles-div{width:28%}
  #usuarios-div{width:60%}
  .asignado:hover{text-decoration: underline}
  .usuarios .nombre, .nombre-header{width: 70%}
  .usuarios .asignado, .asignado-header{width:28%}
  .asignado-verdadero * {color:green; font-weight: bold}

</style>
<div id="roles-div" class="ui-widget ui-widget-content ui-corner-all">
  <table id="roles-table" class="table">
    <thead>
      <tr class="ui-widget-header">
        <th >Roles</th>
      </tr>
    </thead>
    <?php foreach ($authItems as $ai): ?>
      <tr>
        <td onclick="roleClick('<?php echo $ai->name; ?>')"><?php echo $ai->name; ?></td>
      </tr>
    <?php endforeach; ?>
  </table>
</div>
<div id="usuarios-div" class="ui-widget ui-widget-content ui-corner-all">
  <table id="usuarios-table" class="table">
    <thead>
      <tr class="ui-widget-header">
        <th class="nombre-header">Nombre</th>
        <th id="" class="asignado-header">Asignado <input id="asignados-check" type="checkbox" onclick="userClick('*',this.checked)"/></th>
      </tr>
    </thead>
  </table>
</div>
<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<script type="text/javascript">
  var role;
  function roleClick(_role){
    role = _role;
    traeUsuarios();
  }
  
  function traeUsuarios(){
    $("#usuarios-table .rec").remove();
    $.ajax({
      type: "GET",
      data: {role:role},
      url: "<?php echo $this->createUrl("roleUsuarios"); ?>",
      success: function(data) {
        $("#usuarios-table").append(data);
      },
      error: function(data,status){
      }
    });
  }
  
  function userClick(user, checked){
    toggleUserRole(user,checked);
  }
  
  function toggleUserRole(user,todos){
    var ant;
    $.ajax({
      type: "GET",
      //      dataType:"json",
      data: {user:user,role:role,todos:todos},
      url: "<?php echo $this->createUrl("toggleUserRole"); ?>",
      success: function(data) {
        if(user=="*"){
          traeUsuarios();
        }else{
          $td=$("#usuario_"+user);
          $td.parent().toggleClass("asignado-verdadero")
          ant = $td.html();
          if(ant=="Si"){
            $td.html("No");
            $("#asignados-check").attr("checked",false);
          }else{
            $td.html("Si");
          }
        }
      },
      error: function(data,status){
      }
    });
  }
  $(".usuarios").on("click", ".asignado",function(){
  })
</script>