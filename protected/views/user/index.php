<style>
	#izq,#der{height:500px; max-height: 500px}
	#izq{float:left;width: 26.5%;overflow-x: hidden;overflow-y: scroll}
	#der{float:right;width:73.1%}
	#der>div{padding:5px}
	#izq tr *{cursor: default}
	#izq .rec:hover{background-color: lemonchiffon}
	#izq tr.selected{background-color: lemonchiffon}
</style>
<div id="izq" class="ui-widget-content ui-corner-all">
	<table>
		<tr>
			<th>Nombre</th>
			<th>Login</th>
			<th>Login</th>
			<th>Documento</th>
		</tr>
		<?php foreach ($users as $user): ?>
			<tr class="rec" user_id="<?php echo $user->id; ?>">
				<td class="nombre-td"><?php echo $user->nombre; ?></td>
				<td class="login-td"><?php echo $user->login; ?></td>
				<td class="doc-td"><?php echo $user->documento; ?></td>
			</tr>
		<?php endforeach; ?>
	</table>
</div>
<div id="der" class="ui-widget">
	<div id="user-form-div" class="ui-widget-content ui-corner-all">
	</div>
	<div id="user-roles" class="ui-widget-content ui-corner-all">
	</div>
</div>

<script>
	var $selected = null;

	$(function() {
		$("tr:nth-child(3)").click();
	});

	$("#izq .rec").on("click",
		function() {
			if ($selected) {
				$selected.removeClass("selected");
			}
			$selected = $(this).addClass("selected");
			$.ajax({
				type: "GET",
				//      dataType:"json",
				data: {user_id: $selected.attr("user_id")},
				url: "<?php echo $this->createUrl("user/getUserForm"); ?>",
				success: function(data) {
					$("#user-form-div").html(data);
					$("#nombre").focus().select();
					$("#graba").button();
				},
				error: function(data, status) {
				}
			});
			$.ajax({
				type: "GET",
				//      dataType:"json",
				data: {user_id: $selected.attr("user_id")},
				url: "<?php echo $this->createUrl("user/getUserAsignaciones"); ?>",
				success: function(data) {
					$("#user-roles").html(data);
					$("#nombre").focus().select();
					$("#graba").button();
					$("#tabs").tabs();
					$("#items-div").on("dblclick", ".item", function() {
						moveItem($(this));
					});
				},
				error: function(data, status) {
				}
			});
		}
	);

	function moveItem($this) {
		var parent_id = $this.parent().attr("id");
		if (parent_id === "operaciones-libres" || parent_id === "tareas-libres" || parent_id === "roles-libres") {
			$this.appendTo("#items-asignados");
		} else if (parent_id === "items-asignados") {
			var type = $this.attr("tipo-item");
			$target = $("fieldset[tipo=\"" + type + "\"]");
			$this.appendTo($target);
		}
		$.ajax({
			type: "GET",
			//      dataType:"json",
			data: {user_id:$("#user_id").val(),item:$this.html()},
			url: "<?php echo $this->createUrl("user/asignaItem"); ?>",
			success: function(data) {
			},
			error: function(data, status) {
			}
		}
		);
	}

	function grabaUsuario(user_id) {
		$.ajax({
			type: "POST",
			dataType: "json",
			data: $("#user-form").serialize(),
			url: "<?php echo $this->createUrl("user/grabaUserForm"); ?>",
			success: function(data) {
				if (data.status === "error" && muestraErrores(data.errores)) {
					return false;
				} else {
					return true;
				}
			},
			error: function(data, status) {
			}
		});
		return false;
	}


</script>
