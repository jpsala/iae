<style>
	#tabs{overflow: auto}
	#tab-roles{padding: 1em 0.4em !important;}
	#user-roles{overflow: auto; max-height: 64.8%}
	.muy-corto{width:10px}
	.corto{width:80px}
	.mediano{width:180px}
	.largo{width:250px}
	.item{padding: 5px; display: inline-block; float: left; margin: 2px; 
			cursor: pointer; border-radius: 8px}
	#items-asignados{float: left;width: 27.9%;}
	#items-libres{float: right;width:70.2%;height: 54%}
	#roles-libres,#tareas-libres,#operaciones-libres{float: right;width:97.4%;margin-bottom: 2px;}
	#items-asignados,#roles-libres,#tareas-libres,#operaciones-libres{max-height: 100%; overflow: auto; padding: 4px}
	#items-asignados{float: left}
	#roles-libres{float: right}
	#items-asignados-childs{clear: both}
</style>
<div id="tabs">
	<ul>
		<li><a href="#tab-roles">Roles</a></li>
	</ul>
	<div id="tab-roles">
		<div id="items-div">
			<fieldset id="items-asignados" class="ui-widget-content ui-corner-all">
				<legend>Roles asignados</legend>
				<?php $style = ""; ?>
				<?php foreach ($userRoles as $r): ?>
					<div class="item ui-state-default" tipo-item="<?php echo $r->type; ?>">
						<?php echo $r->name; ?>
					</div>
				<?php endforeach; ?>
			</fieldset>

			<div id="items-libres">

				<fieldset tipo="2" id="roles-libres" class="ui-widget-content ui-corner-all">
					<legend>Roles Disponibles</legend>
					<?php foreach ($freeRoles as $r): ?>
						<?php if ($r->type !== "2") continue; ?>
						<div class="item ui-state-default" tipo-item="<?php echo $r->type; ?>"><?php echo $r->name; ?></div>
					<?php endforeach; ?>
				</fieldset>

				<fieldset tipo="1" id="tareas-libres" class="ui-widget-content ui-corner-all">
					<legend>Tareas Disponibles</legend>
					<?php foreach ($freeRoles as $r): ?>
						<?php if ($r->type !== "1") continue; ?>
						<div class="item ui-state-default" tipo-item="<?php echo $r->type; ?>"><?php echo $r->name; ?></div>
					<?php endforeach; ?>
				</fieldset>

				<fieldset tipo="0" id="operaciones-libres" class="ui-widget-content ui-corner-all">
					<legend>Operaciones Disponibles</legend>
					<?php foreach ($freeRoles as $r): ?>
						<?php if ($r->type !== "0") continue; ?>
						<div class="item ui-state-default" tipo-item="<?php echo $r->type; ?>"><?php echo $r->name; ?></div>
					<?php endforeach; ?>
				</fieldset>
			</div>
		</div>
	</div>
	<script>

	</script>
	<?php

	function itemColor($type) {
		return ";background-color:red;";
	}

	;
	?>