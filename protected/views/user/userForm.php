<style>
	#user-form{overflow: visible}
	#user-form input{margin: 3px}
	.muy-corto{width:10px}
	.corto{width:80px}
	.mediano{width:180px}
	.largo{width:250px}
	#user-datos{margin: 4px}
	legend{margin-left: 5px}
	.row{margin: 5px}
	.row input{display: block}
	.inline-block{display: inline-block}
	.row label{margin-left: 4px}
	#password-div{}
	#login-div{}
	#cambiar_password-div{margin-top: -25px}
	#cambiar_password-div label{float: left; margin-right: 5px}
	#botones{float: right}
	#graba{margin: 20px 10px 5px 0; clear: both}
	#puesto_trabajo_id{width:200px}
	#puesto_trabajo_id_chzn{vertical-align: middle}
</style>
<form id="user-form" name="user-form">
	<input type="hidden" name="user_id" id="user_id" value="<?php echo $user->id; ?>"/>
	<fieldset id="user-datos" class="ui-widget-content ui-corner-all">
		<legend>Datos</legend>
		<div class="row inline-block">
			<label>Nombre</label>
			<input class="mediano" id="nombre" name="nombre" placeholder="Nombre" value="<?php echo $user->nombre; ?>"/>
		</div>
		<div id="login-div" class="row inline-block">
			<label>Login</label>
			<input class="mediano" id="login" name="login" placeholder="Login" value="<?php echo $user->login; ?>"/>
		</div>
		<div id="password-div" class="row inline-block">
			<label>Password</label>
			<input type="password" class="mediano" id="password" name="password" placeholder="Password" value="<?php echo $user->password; ?>"/>
			<input type="password" class="mediano" id="password_repeat" name="password_repeat" placeholder="Repeat Password" value="<?php echo $user->password; ?>"/>
		</div>
		<div id="cambiar_password-div" class="row">
			<label>Cambiar Pass.</label>
			<?php $cp_checked = $user->cambiar_password ? "checked" : ""; ?>
			<input class="muy-corto" type="hidden" name="cambiar_password" />
			<input class="muy-corto" type="checkbox" id="cambiar_password"  <?php echo $cp_checked; ?> name="cambiar_password" placeholder="Cambiar Pass." value="<?php echo $user->cambiar_password; ?>"/>
		</div>
		<div id="puesto_trabajo-div" class="row inline-block">
			<label>Puesto de trabajo</label>
			<?php
			$data = CHtml::listData(PuestoTrabajo::model()->findAll(), "id", "nombre");
			echo CHtml::dropDownList("puesto_trabajo_id", $user->puesto_trabajo_id, $data)
			?>
		</div>
		<div id="botones">
			<button id="graba" onclick="return grabaUsuario(<?php echo $user->id; ?>)">Graba</button>
		</div>
	</fieldset>
</form>

<script>
	$("#puesto_trabajo_id").chosen();
	function grabaUsuario() {
		$.ajax({
			type: "POST",
			dataType: "json",
			data: $("#user-form").serialize(),
			url: "<?php echo $this->createUrl("user/grabaUserForm"); ?>",
			success: function(data) {
				if (data.status === "error" && muestraErrores(data.errores)) {
					return false;
				} else {
					return true;
				}
			},
			error: function(data, status) {
			}
		});
		return false;
	}
</script>