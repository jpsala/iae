<div class="container">
   <H1>Arregla Cheques Nación</h1>
   <button data-bind="click: arregla">Arregla</button>
   <table class="table table-bordered table-condensed table-stripped">
      <tbody data-bind="foreach:{data: data, as: 'chq' }">
         <tr>
            <td data-bind="text: chq.comprob"></td>
            <td data-bind="text: chq.numero"></td>
            <td data-bind="text: chq.fecha"></td>
            <td data-bind="text: chq.importe"></td>
            <td data-bind="text: chq.destino"></td>
         </tr>
      </tbody>
   </table>

</div>

<script>
function ViewModel(){
   this.data = <?php echo $qry; ?>;
   this.test = "hola";
   this.arregla = function(){
     $.ajax({
        type:'post',
        url:'<?php echo $this->createUrl("varios/arreglaChequesNacionAjax"); ?>',
        success: function(data){
           console.log(data);
           window.location = window.location;
     }
   });
   };
}
ko.applyBindings(new ViewModel());
</script>