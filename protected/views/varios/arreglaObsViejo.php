<div class="container">
	<div class="row">
		<?php
			$rowsFams = Helpers::qryAllObj("
				select f.id
					from familia f
						inner join alumno a on a.Familia_id = f.id
					where a.obs is not null or a.obs_cobranza is not null
				");
			$tr = Yii::app()->db->beginTransaction();
			foreach ($rowsFams as $rowFam) {
				$rowsAlus = Helpers::qryAllObj("
					select a.obs, a.matricula, a.obs_cobranza, a.id, a.familia_id
						from alumno a 
						where a.familia_id = $rowFam->id
				");
				$obs = null;
				foreach ($rowsAlus as $row) {
					$sep = trim($obs) ? "<br/>" : "";
					if ($row->obs_cobranza and $row->obs_cobranza != "null") {
						$obs .= $sep . $row->matricula . ":" . trim($row->obs_cobranza);
					}
					if ($row->obs and $row->obs != "null") {
						$obs .= "/" . trim($row->obs);
					}
					//$obs .= $sep . CHtml::encode($row->matricula . ":". ($row->obs ? $row->obs : "\r\n") . $row->obs_cobranza);
				}
				if ($obs) {
					ve(Helpers::qryExec("
					update familia a
						set a.obs =  \"$obs\"
						where a.id = $row->familia_id
					"), $obs);
				}
			}
			$tr->commit();
		?>
	</div>
</div>