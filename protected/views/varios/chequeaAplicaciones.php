<?php
    $data    = array();
    $alumnos = Helpers::qryAllObj("
        SELECT s.id AS socio_id, a.matricula, CONCAT(a.apellido, ', ', a.nombre) AS nombres,
           saldo(s.id, null) AS saldo
        FROM alumno a
          INNER JOIN socio s ON a.id = s.Alumno_id
        /*WHERE a.matricula = 2833*/");
    foreach ($alumnos as $a) {
        $saldo  = Helpers::qry("select
            (SELECT saldo($a->socio_id, null)) AS saldo_bien,
            (SELECT COALESCE(SUM(d.saldo),0) AS saldo_docs_contra
              FROM doc d
                  INNER JOIN talonario t ON d.talonario_id = t.id
                  INNER JOIN comprob c ON t.comprob_id = c.id AND c.signo_cc = 1
              WHERE !d.anulado and d.Socio_id = $a->socio_id AND d.saldo > 0) as saldo_docs_contra,
            (SELECT COALESCE(SUM(d.saldo)) AS saldo_docs FROM doc d
                  INNER JOIN talonario t ON d.talonario_id = t.id
                  INNER JOIN comprob c ON t.comprob_id = c.id AND c.signo_cc = -1
                WHERE !d.anulado and d.Socio_id = $a->socio_id AND d.saldo > 0) as saldo_docs_favor
        ");
        $data[] = array(
            "alumno" => $a,
            "saldos" => $saldo
        );
    }
?>
<div class="container top30">
    <table class="table table-condensed table-striped">
        <tr>
            <td>socio_id</td>
            <td></td>
            <td></td>
            <td>Saldo(Bien)</td>
            <td>Saldo Docs favor</td>
            <td>Saldo Docs contra</td>
            <td>Diff</td>
        </tr>
        <?php foreach ($data as $d): ?>
            <?php //$resta = $d['saldos']['positivo'] - $d['saldos']['negativo'];?>
            <?php $saldo = $d['alumno']->saldo; ?>
            <?php $saldo_docs_favor = $d['saldos']['saldo_docs_favor']; ?>
            <?php $saldo_docs_contra = $d['saldos']['saldo_docs_contra']; ?>
            <?php $saldo_calc = $d['saldos']['saldo_docs_contra'] - $d['saldos']['saldo_docs_favor']; ?>
            <?php if(abs($saldo_calc - $saldo) > 1): ?>
                <tr>
                    <td><?php echo $d['alumno']->socio_id; ?></td>
                    <td><?php echo $d['alumno']->matricula; ?></td>
                    <td><?php echo $d['alumno']->nombres; ?></td>
                    <td><?php echo $d['alumno']->saldo; ?></td>
                    <td><?php echo $d['saldos']['saldo_docs_favor']; ?></td>
                    <td><?php echo $d['saldos']['saldo_docs_contra']; ?></td>
                    <td><?php echo $saldo_calc - $saldo; ?></td>
                </tr>
            <?php endif; ?>
        <?php endforeach ?>
    </table>
</div>
