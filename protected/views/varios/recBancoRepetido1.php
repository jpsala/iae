<ul><li><?php echo $this->actions[$this->action->id]["obs"] ?></li></ul>

<?php
$comps = Helpers::qryAll("
            select d.fecha_valor, fecha_creacion, concat(c.nombre,\"-\",LPAD(d.numero, 6, \"0\")) as comprob, d.detalle, d.total, 
			 d.saldo, case when d.anulado then \"Si\" else \"No\" end as anulado, de.nombre, 
                               v.importe, case when v.conciliado then \"Si\" else \"No\" end as conciliado, t.nombre, u.nombre as usuario
	from doc d
		inner join doc_valor v on v.Doc_id = d.id
		inner join destino de on de.id = v.Destino_id
		inner join doc_valor_tipo t on t.id = v.tipo
		inner join talonario ta on ta.id = d.talonario_id
		inner join comprob c on c.id = ta.comprob_id
		inner join user u on u.id = d.user_id
where d.numero in (2566 ,2567) and d.talonario_id = 41
");
$cols["Transferencias"] = array(
        "fecha_valor" => array("date" => "d/m/Y", "title"=>"Fecha Valor", "visible"=>false),
        "fecha_creacion" => array("date" => "d/m/Y", "title"=>"Fecha Carga"),
        "total" => array("currency"=>true),
        "importe" => array("currency"=>true, "title"=>"Valor", "total"=>true),
);
$opt = array(
        "title" => false,
        "filter" => false,
);
echo Helpers::getTable("Transferencias", $comps, $cols, $opt);
