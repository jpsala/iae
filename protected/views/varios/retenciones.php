<div class="container">
<?php
$phpExcelPath = Yii::getPathOfAlias('ext');
spl_autoload_unregister(array('YiiBase', 'autoload'));
include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel/IOFactory.php');
spl_autoload_register(array('YiiBase', 'autoload'));
$rows = [];
$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$archivos = [
  "plan_sublegajos_comparada1.xlsx",
  "plan_sublegajos_comparada2.xlsx",
  "plan_sublegajos_comparada3.xlsx",
  "plan_sublegajos_comparada4.xlsx",
  "plan_sublegajos_comparada5.xlsx",
  "plan_sublegajos_comparada6+sac.xlsx",
  "plan_sublegajos_comparada7.xlsx",
  "plan_sublegajos_comparada8.xlsx",
  "plan_sublegajos_comparada9.xlsx",
];
/*
    [A] => 174
    [B] => 1
    [C] => ALBAYTERO, MARIA AGUSTINA
    [D] => 5/1/2011
    [E] => 
    [F] => 002-INICIAL-7953 - PF NO SUBV.
    [G] => ACTIVO
    [H] => TIT
    [I] => J
    [J] => PR
    [K] => 00006-PRECEP/INI. PR(006)
    [L] => 1
    [M] => 
    [N] => 0
    [O] => No
    [P] => 17
    [Q] => IPS
    [R] => 0
    [S] => 0
    [T] => 0
    [U] => 0
    [V] => 0
    [W] => 0.00
    [X] => 15890.62
    [Y] => 0.00
    [Z] => 14598.29
    [AA] => 0.00
    [AB] => 1292.33
 */
foreach ($archivos as $archivo) {
  $objPHPExcel = $objReader->load("/mnt/shares/zoot/RETROACTIVO/" . $archivo);
  $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
  $numero = substr($archivo, 25, 1);
  foreach ($sheetData as $key => $row) {
    if (is_numeric($row["A"])) {
      $id = $row['A'] . '-' . $row['B'];
      $legajo = $row['A'];
      $subLegajo = $row['B'] ;
      $nombre = $row['C'];
      $dif = $row['AB'];
      $rows[$id]['planillas'][$numero] = $dif;
      if (!isset($rows[$id]['diff'])) {
        $rows[$id]['diff'] = 0;
      }
      $rows[$id]['diff'] += $dif;
      $rows[$id]["id"] = $id;
      $rows[$id]["nombre"] = $nombre;
      $rows[$id]["legajo"] = $legajo;
      $rows[$id]["subLegajo"] = $subLegajo;
    }
  }
}
// vd2($rows);
?> 
<table class="table">
  <tr>
    <th>legajo;</th><th>sub-legajo;</th><th>Nombre;</th>
    <?php
    for ($i = 0; $i < 9; $i++) {
      echo "<th>" . ($i + 1) . ";</th>";
    }
    ?>
    <th>Diferencia</th>
  </tr>
<?php
foreach ($rows as $row) {
  echo "<tr>";
  echo "<td>" . $row['legajo'] . ";</td>";
  echo "<td>" . $row['subLegajo'] . ";</td>";
  echo "<td>" . $row['nombre'] . ";</td>";
  if (!isset($row["planillas"])) {
    $row["planillas"] = [];
  }
  for ($i = 0; $i < 9; $i++) {
    $dif = isset($row["planillas"][$i+1]) ? $row["planillas"][$i+1] : '-';
    echo "<th>" . $dif . ";</th>";
  }
  echo "<td>" . $row['diff'] . ";</td>";
  echo "</tr>";
}