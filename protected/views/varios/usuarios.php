<di class="container">
<?php
  $fileName = '/mnt/shares/zoot/FRANCISCO/plantel.xlsx';
  $phpExcelPath = Yii::getPathOfAlias('ext');
  spl_autoload_unregister(array('YiiBase', 'autoload'));
  include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
  include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel/IOFactory.php');
  spl_autoload_register(array('YiiBase', 'autoload'));
  $objReader = PHPExcel_IOFactory::createReader('Excel2007');
  $objPHPExcel = $objReader->load($fileName);
  $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
  $usuarios = [];
  $agregados = 0;
  $actualizados = 0;
  foreach ($sheetData as $data) {
    $cuitParts = split('-', $data['A']);
    if($data['A'] !== '' and count($cuitParts) === 3 ){
      $doc = $cuitParts[1];
      $nombres = split( ',', $data['B'] );
      $usuario = new stdClass();
      $usuario->documento = $doc;
      $usuario->nombre = $nombres[0];
      $usuario->apellido = $nombres[1];
      $usuarios[] = $usuario;
    }
  }

echo '<h4 style="color: red; display: inline">Rojo - Agregando</h4>&nbsp/&nbsp;';
echo '<h4 style="color: green; display: inline">Verde - Actualizando</h4>';
echo '<hr>';
$tr = Yii::app()->db->beginTransaction();
  foreach ($usuarios as $usuario) {
    $usuario->login = $usuario->documento;
    $usuario->password = $usuario->documento;
    $userId = Helpers::qryScalar("select id from user where documento = " . $usuario->documento);
    if($userId){
      $actualizados++;
      $params = [
        "apellido" => $usuario->apellido,
        "nombre" => $usuario->nombre,
      ];
      $updateUser = "
      update  user set apellido = :apellido, nombre = :nombre where id = $userId;
      ";
      $insertado = Helpers::qryExec($updateUser, $params);
      echo '<label style="color: green">'. $usuario->apellido . ' ' . $usuario->nombre . '</label> - ';
    }else{
      $agregados++;
      $params = [
        "documento" => $usuario->documento,
        "login" => $usuario->documento,
        "apellido" => $usuario->apellido,
        "nombre" => $usuario->nombre,
        "password" => $usuario->documento
      ];
      $insertUser = "
      insert into user(documento,login,apellido,nombre,password) values (:documento,:login,:apellido,:nombre,:password);
      ";
      $insertados = Helpers::qryExec($insertUser, $params);
      $last_id = Helpers::qryscalar('SELECT LAST_INSERT_ID() as last_id');
      echo '<label style="color: red">'. $usuario->apellido . ' ' . $usuario->nombre . '</label> - ';
      // $existeAssignment = Helpers::qryScalar("select count(*) from authassignment where itemnamem = 'Profesor' and userid = ". $u)
      $insertAsignment = "
        INSERT INTO authassignment(userid,itemname,data) values( $last_id, 'Profesor', 'N;' )
      ";

      Helpers::qryExec($insertAsignment);
    }
    // echo '</pre>';

  }
  // $tr->rollback();
  $tr->commit();
?> 
</label>
<?php
  echo '<H1>Actualizados ' . $actualizados . '</H1>';
  echo '<H1>Agregados    ' . $agregados . '</H1>';