SELECT n.nombre AS nivel, a.nombre AS anio, d.nombre AS division, a1.apellido AS alumno, ad.Ciclo_id, n1.nota, n1.texto, p.apellido, p.email
FROM alumno_division ad
  INNER JOIN alumno a1 ON ad.Alumno_id = a1.id
  INNER JOIN familia f ON a1.Familia_id = f.id
  INNER JOIN pariente p ON f.id = p.Familia_id
  INNER JOIN division d ON ad.Division_id = d.id AND d.activo
  INNER JOIN anio a ON d.Anio_id = a.id
  INNER JOIN nivel n ON a.Nivel_id = n.id
  INNER JOIN alumno_division_estado ade ON ad.alumno_division_estado_id = ade.id AND ade.muestra_admin
  INNER JOIN division_asignatura da ON d.id = da.Division_id
  INNER JOIN nota n1 ON ad.id = n1.Alumno_Division_id
WHERE ad.activo AND ad.Ciclo_id = (SELECT MAX(id) FROM ciclo) AND texto <> ""