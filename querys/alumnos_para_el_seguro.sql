SELECT a.apellido, a.nombre, a.numero_documento, DATE_FORMAT(a.fecha_nacimiento, '%d/%m/%Y') AS fecha_nacim FROM alumno a
  INNER JOIN alumno_estado ae ON a.estado_id = ae.id AND ae.activo_admin AND ae.activo_edu
  INNER JOIN alumno_division ad ON a.id = ad.Alumno_id
  INNER JOIN alumno_division_estado ade ON ad.alumno_division_estado_id = ade.id AND ade.muestra_admin AND ade.muestra_edu
  INNER JOIN ciclo c ON ad.Ciclo_id = c.id AND c.id = 8