SELECT sv.id, sv.nombreCompleto as nombre, 
         saldo_buffet(sv.id, null) * -1 AS total,
         (select count(bd2.borrado) from buffet_doc bd2 where bd2.socio_id = sv.id and bd2.borrado and bd2.motivo_borrado != 'xxx') as con_borrados, 
         (SELECT DATE_FORMAT(MAX(bd.fecha), '%d/%m/%Y') FROM buffet_doc bd WHERE bd.socio_id = sv.id) AS ultima_fecha
    FROM buffet_doc d
      INNER JOIN socio_view sv ON sv.id = d.socio_id AND sv.id != 42593
    where (select count(*) from buffet_doc_det det where det.doc_id = d.id) > 0
    GROUP BY sv.id
    order by saldo_buffet(sv.id, null) desc