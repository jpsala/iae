select  s1.id AS socio_id_pariente, s.id as socio_id, upper(s.tipo) as tipo, s.password, p1.apellido AS parienteApellido, p1.nombre AS parienteNombre, p1.email AS parienteMail,
            case
              when a.id then concat(a.apellido,', ', a.nombre)
              when e.id then e.nombre
              else concat(p.apellido, ', ', p.nombre)
            end as nombre,
            case
              when a.id then a.email
              when e.id then e.email
              when p.id then p.email
            end as email,
            case
              when a.id then 'alumno'
              when e.id then 'empleado'
              when p.id then 'pariente'
            end as tipo,
            case
                when a.id then 'a'
                when p.id then 'p'
                when e.id then 'e'
              end as tipo_socio,
            case
              when a.id then
              concat(
                  an.nombre, ' ', d.nombre,
                  ' DNI:', a.numero_documento
                )
              when e.id then 'Empleado'
              when p.id then t.nombre
            end as description, saldo_buffet(s.id, null) as saldo
          from socio s
            left join alumno a on a.id = s.alumno_id
            left join alumno_division ad on ad.alumno_id = a.id and ad.activo
            left join division d on d.id = ad.division_id
            left join anio an on an.id = d.anio_id
            left join pariente p on p.id = s.pariente_id
            left join pariente_tipo t on p.pariente_tipo_id = t.id
            left join user e on e.id = s.empleado_id
            left JOIN familia f ON a.Familia_id = f.id
            left JOIN pariente p1 ON f.id = p1.Familia_id
            left JOIN socio s1 ON p1.id = s1.Pariente_id
          WHERE saldo_buffet(s.id, null) > 0