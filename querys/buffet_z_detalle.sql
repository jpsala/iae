SELECT d.total, d.descuento,
  (
   SELECT GROUP_CONCAT(CONCAT(bdd.detalle, ' - ', bdd.cantidad, ' - ', bdd.importe, ' - ', bdd.saldo)) FROM buffet_doc_det bdd WHERE bdd.doc_id = d.id
  ) AS detalle
  FROM buffet_doc d
    INNER JOIN socio_view sv ON sv.id = d.socio_id  
  WHERE d.tipo = 't' AND sv.Empleado_id = 18 AND d.fecha = '2019/09/02'