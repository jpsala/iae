SELECT a.apellido, a.nombre, CONCAT(p.calle, ' ', p.numero, ' ', p.piso,' ',  p.departamento) AS domicilio, 
                a.telefono_1 AS telefono_alumno, p.telefono_celular AS telefono_padre_celular, p.telefono_casa AS telefono_padre_casa,
                (
                      SELECT c.nombre FROM alumno_division ad
                          INNER JOIN ciclo c ON ad.Ciclo_id = c.id
                        WHERE ad.Alumno_id = a.id ORDER BY ad.id DESC LIMIT 1
                 ) AS anio_egreso
  FROM alumno a 
    inner JOIN pariente p ON a.vive_con_id = p.id
    INNER JOIN alumno_estado ae ON a.estado_id = ae.id
  WHERE (a.egresado OR ae.egresado)
  ORDER BY a.apellido, a.nombre