SELECT c.nombre, n1.nombre AS nivel, a1.nombre AS anio, d.nombre AS division, a.apellido, a.nombre, lp.nombre, li.nombre, n.nota
  FROM nota n
    INNER JOIN alumno_division ad ON n.Alumno_Division_id = ad.id
    INNER JOIN division d ON ad.Division_id = d.id
    INNER JOIN anio a1 ON d.Anio_id = a1.id
    INNER JOIN nivel n1 ON a1.Nivel_id = n1.id AND n1.id = 4
    INNER JOIN alumno a ON ad.Alumno_id = a.id
    INNER JOIN inasistencia i ON ad.id = i.Alumno_Division_id
    INNER JOIN logica_item li ON n.Logica_item_id = li.id
    INNER JOIN logica_periodo lp ON i.logica_periodo_id = lp.id
    INNER JOIN division_asignatura da ON d.id = da.Division_id
    INNER JOIN ciclo c ON ad.Ciclo_id = c.id AND c.id = (SELECT MAX(id) FROM ciclo)
ORDER BY n1.orden, a1.orden, d.orden/*, lp.orden, li.orden;*/