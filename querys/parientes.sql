SELECT p.id, s.id AS socio_id, p.email, p.email_ant, p.password, s.password
  FROM alumno a
    INNER JOIN pariente p ON a.vive_con_id = p.id
    INNER JOIN socio s ON p.id = s.Pariente_id
    INNER JOIN alumno_division ad ON a.id = ad.Alumno_id
    INNER JOIN alumno_division_estado ade ON ad.alumno_division_estado_id = ade.id AND ade.muestra_edu
    INNER JOIN division d ON ad.Division_id = d.id
    INNER JOIN anio a1 ON d.Anio_id = a1.id
  WHERE  ad.activo AND a.activo AND p.email = 'damiancid@hotmail.com'