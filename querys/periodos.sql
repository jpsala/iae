SELECT * FROM logica_periodo lp
  INNER JOIN logica l ON lp.Logica_id = l.id
  INNER JOIN logica_ciclo lc ON l.id = lc.Logica_id
  INNER JOIN asignatura_tipo at ON lc.Asignatura_Tipo_id = at.id
  INNER JOIN nivel n ON at.Nivel_id = n.id
  INNER JOIN ciclo c ON lc.Ciclo_id = c.id AND c.nombre = '2019' AND n.id = 4
ORDER BY n.orden, lp.orden